
"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Graficas = function (_React$Component) {
    _inherits(Graficas, _React$Component);

    function Graficas(props) {
        _classCallCheck(this, Graficas);

        var _this = _possibleConstructorReturn(this, (Graficas.__proto__ || Object.getPrototypeOf(Graficas)).call(this, props));

        _this.echarts = window.echarts;
        _this.charts = {};
        _this.state = {
            responsive: false,
            options: _this.props.options
        };
        return _this;
    }

    _createClass(Graficas, [{
        key: "generateSettings",
        value: function generateSettings() {
            if (this.props.hasOwnProperty("height")) this.setState({ height: this.props.height });
            if (this.props.hasOwnProperty("options")) this.setState({ options: this.props.options });else throw "Options Requerido!!";

            if (this.props.hasOwnProperty("responsive") && this.props.responsive != undefined) this.setState({ responsive: this.props.responsive });
        }
    }, {
        key: "getSizeWindows",
        value: function getSizeWindows() {
            var w = window;
            var d = document;
            var documentElement = d.documentElement;
            var body = d.getElementsByTagName('body')[0];
            var size = {
                width: w.innerWidth || documentElement.clientWidth || body.clientWidth,
                height: w.innerHeight || documentElement.clientHeight || body.clientHeight
            };

            return size;
        }
    }, {
        key: "chartResize",
        value: function chartResize() {
            setTimeout(() => {
                this.charts.resize();
            }, 100)
            
            var options = this.charts.getOption();
            var position = 0;
            if (this.state.responsive && this.getSizeWindows().width <= 755) {
                options.legend[position].top = null;
                options.legend[position].bottom = "8%";
                options.legend[position].left = "10%";
                options.grid[position].bottom = "30%";
                options.grid[position].top = 28;
                if (options.hasOwnProperty("dataZoom") && options.dataZoom.length > 0) {
                    options.dataZoom[position].bottom = -1;
                }
            } else {
                options.legend[position].top = "top";
                options.legend[position].bottom = null;
                options.legend[position].left = null;
                if(!options.grid) options.grid = [];
                options.grid[position].bottom = 60;
                options.grid[position].top = 60;
                if (options.hasOwnProperty("dataZoom") && options.dataZoom.length > 0) {
                    options.dataZoom[position].bottom = null;
                }
            }
            this.setState({ options: options });
            this.charts.setOption(this.state.options);
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(newProps) {
            this.setState({
                options: newProps.options,
                responsive: newProps.responsive
            });
        }
    }, {
        key: "componentDidUpdate",
        value: function componentDidUpdate() {
            this.charts.setOption(this.state.options);
        }
    }, {
        key: "componentDidMount",
        value: function componentDidMount() {
            if (!this.props.hasOwnProperty("id") && this.props.id == undefined) {
                throw 'ID es Requerido';
            }
            this.generateSettings();
            this.charts = this.echarts.init(document.getElementById(this.props.id), this.props.theme);
            this.charts.setOption(this.state.options);
            window.addEventListener("resize", this.chartResize.bind(this));
            window.addEventListener("orientationchange", () => this.chartResize.bind(this))
        }
    }, {
        key: "render",
        value: function render() {
            return React.createElement("div", { ref: "grafica", id: this.props.id, className: this.props.height });
        }
    }]);

    return Graficas;
}(React.Component);

Graficas.propTypes = {
    options: React.PropTypes.object,
    height: React.PropTypes.number
};
Graficas.defaultProps = {
    options: {},
    height: "charts-500"
};

var Historica = function (_React$Component2) {
    _inherits(Historica, _React$Component2);

    function Historica(props) {
        _classCallCheck(this, Historica);

        var _this2 = _possibleConstructorReturn(this, (Historica.__proto__ || Object.getPrototypeOf(Historica)).call(this, props));

        _this2.state = {
            series: _this2.props.series,
            legend: _this2.props.legend,
            mode: _this2.props.mode,
            umbral: _this2.props.umbral,
            type : _this2.props.type,
            zoom : _this2.props.zoom != undefined ? _this2.props.zoom : true,
            legendBottom : _this2.props.legendBottom,
            min : _this2.props.min,
            max : _this2.props.max,
            actions : _this2.props.hasOwnProperty("actions") ? _this2.props.actions : true,
            grid : _this2.props.hasOwnProperty("grid") ? _this2.props.grid : false,
            showLegends : _this2.props.hasOwnProperty("showLegends") ? _this2.props.showLegends : true
        };
        return _this2;
    }

    _createClass(Historica, [{
        key: "getLegendSeries",
        value: function getLegendSeries() {
            var legend = [];
            if (this.props.hasOwnProperty("legend") && this.props.legend.length > 0) {
                return this.props.legend;
            } else {
                var _legend = this.props.data.map(function (e) {
                    return e.label;
                });
                return _legend;
            }

            return legend;
        }
    }, {
        key: "getTitulo",
        value: function getTitulo() {
            var encabezado = {
                titulo: "",
                subtitulo: ""
            };
            if (this.props.hasOwnProperty("titulo") && this.props.titulo != "") {
                encabezado.titulo = this.props.titulo;
            }
            if (this.props.hasOwnProperty("subtitulo") && this.props.subtitulo != "") {
                encabezado.subtitulo = this.props.subtitulo;
            }

            return encabezado;
        }
    }, {
        key: "getLegend",
        value: function getLegend() {
            if (this.props.hasOwnProperty("series")) {
                if (this.props.series.length > 0) {
                    return this.props.series;
                } else {
                    var serie = this.props.series;
                    var series = Object.keys(serie).map(function (e) {
                        return serie[e].name;
                    });
                    return series;
                }
            } else {
                throw "Sin series..";
            }
        }
    }, {
        key: "getSeries",
        value: function getSeries() {
            if (this.props.hasOwnProperty("series")) {
                if (this.props.series.length > 0) {
                    let series = { series : this.props.series, selected : {} }
                    if(parseFloat(this.state.umbral)){
                        let markLine = {
                            name: "Umbral",
                            value: parseFloat(this.state.umbral),
                            xAxis: -1,
                            yAxis: parseFloat(this.state.umbral)
                        };
                        for(let i in series.series){
                            let s = series.series[i]
                            s.markLine = {
                                data: [],
                                lineStyle : {
                                    color : 'red'
                                }
                            };
                            s.markLine.data.push(markLine);
                        }
                        /*series.series[0].markLine = {
                            data: []
                        };
                        series.series[0].markLine.data.push(markLine);*/
                    }
                    return series
                } else {
                    var serie = this.props.series;
                    var series = {};
                    var selected = {};
                    var markLine = {
                        name: "Umbral",
                        value: parseFloat(this.state.umbral),
                        xAxis: -1,
                        yAxis: parseFloat(this.state.umbral)
                    };
                    if (this.state.mode) {
                        series = Object.keys(serie).map(function (e) {
                            if(serie[e].hasOwnProperty('selected')){
                                selected[serie[e].name] = serie[e].selected
                            }
                            return serie[e];
                        });
                    } else {
                        var type = this.state.type
                        series = Object.keys(serie).map(function (e) {
                            if(serie[e].hasOwnProperty('selected')){
                                selected[serie[e].name] = serie[e].selected
                            }

                            var s = {
                                connectNulls: true,
                                name: serie[e].name,
                                type: type || "bar",
                                data: serie[e].data,
                                markLine: null,
                                label : serie[e].label || {},
                                itemStyle : serie[e].itemStyle
                            };

                            if(serie[e].hasOwnProperty('yAxisIndex')){
                                s.yAxisIndex = serie[e].yAxisIndex
                            }
                            return s
                        });
                        if (this.state.umbral > 0 && series.length > 0) {
                            series[0].markLine = {
                                data: []
                            };
                            series[0].markLine.data.push(markLine);
                        }
                    }

                    return { series, selected };
                }
            } else {
                throw "Sin series..";
            }
        }
    }, {
        key: "getMargen",
        value: function getMargen() {
            if (this.props.hasOwnProperty("margen")) {
                return this.props.margen;
            } else {
                return { min: 80, max: 100 };
            }
        }
    }, {
        key: "getOptions",
        value: function getOptions() {
            var titulo = this.getTitulo().titulo;
            var subtitulo = this.getTitulo().subtitulo;
            var legendSeries = this.getLegendSeries();
            var legend = this.getLegend();
            if (this.state.mode) {
                legend = legendSeries;
            }
            var { series, selected } = this.getSeries();
            var margen = this.getMargen();
        
            var options = {
                title: {
                    text: titulo,
                    x: 'center',
                    subtext: subtitulo,
                    align: 'right'
                },
                grid: (this.state.grid) ? this.state.grid : { bottom: 80 },
                toolbox: {
                    feature: (this.state.actions) ? {
                        dataZoom: {
                            yAxisIndex: 'none'
                        },
                        magicType: {
                            type: ['line', 'bar']
                        },
                        restore: {},
                        saveAsImage: {}
                    } : {}
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        animation: false
                    }
                },
                legend: {
                    data: legend,
                    x: 'left',
                    y: this.state.legendBottom ? 'bottom' : 'top',
                    selected : selected || null,
                    orient : this.props.legendOrient || 'horizontal',
                    show : this.state.showLegends
                },
                dataZoom: [{
                    show: this.state.zoom,
                    realtime: true
                }, {
                    type: 'inside',
                    realtime: true
                }],
                xAxis: [{
                    type: 'category',
                    boundaryGap: true,
                    axisLine: { onZero: true },
                    data: legendSeries,
                }],
                yAxis: [{
                    type: 'value',
                    min : this.state.min || null,
                    max : this.state.max || null
                }],
                plotOptions: {
                    series: {
                        connectNulls: true
                    }
                },
                series: series
            };
            return options;
        }
    }, {
        key: "validParams",
        value: function validParams() {
            if (!this.props.hasOwnProperty("series")) throw 'Data no encontrada';
        }
    }, {
        key: "componentWillReceiveProps",
        value: function componentWillReceiveProps(newProps) {
            this.setState({
                series: newProps.series,
                legend: newProps.legend,
                umbral: newProps.umbral
            });
        }
    }, {
        key: "render",
        value: function render() {
            this.validParams();
            var options = this.getOptions();
            return React.createElement(Graficas, { options: options, id: this.props.id, height: this.props.height, responsive: this.props.responsive });
        }
    }]);

    return Historica;
}(React.Component);

var Barras = function (_React$Component3) {
    _inherits(Barras, _React$Component3);

    function Barras(props) {
        _classCallCheck(this, Barras);

        var _this3 = _possibleConstructorReturn(this, (Barras.__proto__ || Object.getPrototypeOf(Barras)).call(this, props));

        _this3.state = {
            umbral: _this3.props.umbral
        };
        return _this3;
    }

    _createClass(Barras, [{
        key: "getLegend",
        value: function getLegend() {
            var legend = [];
            if (this.props.hasOwnProperty("legend") && this.props.legend.length > 0) {
                return this.props.legend;
            } else {
                var _legend2 = this.props.data.map(function (e) {
                    return e.label;
                });
                return _legend2;
            }

            return legend;
        }
    }, {
        key: "getTitulo",
        value: function getTitulo() {
            var encabezado = {
                titulo: "",
                subtitulo: ""
            };
            if (this.props.hasOwnProperty("titulo") && this.props.titulo != "") {
                encabezado.titulo = this.props.titulo;
            }
            if (this.props.hasOwnProperty("subtitulo") && this.props.subtitulo != "") {
                encabezado.subtitulo = this.props.subtitulo;
            }

            return encabezado;
        }
    }, {
        key: "getEjeX",
        value: function getEjeX() {
            var ejeX = [];
            if (this.props.hasOwnProperty("ejeX") && this.props.ejeX) {
                var _ejeX = this.props.data.map(function (e) {
                    return e.label;
                });
                return _ejeX;
            }
            return ejeX;
        }
    }, {
        key: "getSeries",
        value: function getSeries() {
            if (this.props.hasOwnProperty("series") && this.props.series.length > 0) {
                return this.props.series;
            } else {
                var markLine = {
                    name: "Umbral",
                    value: parseFloat(this.state.umbral),
                    xAxis: -1,
                    yAxis: parseFloat(this.state.umbral)
                };
                var series = this.props.data.map(function (e) {
                    return {
                        name: e.label,
                        type: 'bar',
                        data: [new Number(e.value).toFixed(2)],
                        markLine: null
                    };
                });

                if (this.state.umbral > 0 && series.length > 0) {
                    series[0].markLine = {
                        data: []
                    };
                    series[0].markLine.data.push(markLine);
                }

                return series;
            }
        }
    }, {
        key: "validParams",
        value: function validParams() {
            if (!this.props.hasOwnProperty("data")) throw 'Data no encontrada';
            if (this.props.data.length <= 0) throw 'Data no encontrada';
        }
    }, {
        key: "getOptions",
        value: function getOptions() {
            var titulo = this.getTitulo().titulo;
            var subtitulo = this.getTitulo().subtitulo;
            var legend = this.getLegend();
            var ejeX = this.getEjeX();
            var series = this.getSeries();
            var option = {
                title: {
                    text: titulo,
                    subtext: subtitulo
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: legend,
                    bottom: '1%'
                },
                toolbox: {
                    show: true,
                    feature: {
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                calculable: true,
                xAxis: [{
                    type: 'category',
                    data: ejeX
                }],
                yAxis: [{
                    type: 'value'
                }],
                series: series
            };
            return option;
        }
    }, {
        key: "render",
        value: function render() {
            this.validParams();
            var option = this.getOptions();
            return React.createElement(Graficas, { options: option, id: this.props.id });
        }
    }]);

    return Barras;
}(React.Component);

var Pastel = function (_React$Component4) {
    _inherits(Pastel, _React$Component4);

    function Pastel(props) {
        _classCallCheck(this, Pastel);

        return _possibleConstructorReturn(this, (Pastel.__proto__ || Object.getPrototypeOf(Pastel)).call(this, props));
    }

    _createClass(Pastel, [{
        key: "getLegend",
        value: function getLegend() {
            var legend = [];
            if (this.props.hasOwnProperty("legend") && this.props.legend.length > 0) {
                return this.props.legend;
            } else {
                var _legend3 = this.props.data.map(function (e) {
                    return e.label;
                });
                return _legend3;
            }

            return legend;
        }
    }, {
        key: "getTitulo",
        value: function getTitulo() {
            var encabezado = {
                titulo: "",
                subtitulo: ""
            };
            if (this.props.hasOwnProperty("titulo") && this.props.titulo != "") {
                encabezado.titulo = this.props.titulo;
            }
            if (this.props.hasOwnProperty("subtitulo") && this.props.subtitulo != "") {
                encabezado.subtitulo = this.props.subtitulo;
            }

            return encabezado;
        }
    }, {
        key: "getSeries",
        value: function getSeries() {
            if (this.props.hasOwnProperty("series") && this.props.series.length > 0) {
                return this.props.series;
            } else {
                var series = this.props.data.map(function (e) {
                    return {
                        name: e.label,
                        value: new Number(e.value).toFixed(2),
                        itemStyle: {
                            normal: {
                                color : e.color || null,
                                label: {
                                    formatter: "{c} ({d}%)"
                                }
                            }
                        },
                        label : {
                            formatter: "{c} ({d}%)",
                            normal: {
                                color : e.color || null,
                            }
                        }
                    };
                });

                return series;
            }
        }
    }, {
        key: "validParams",
        value: function validParams() {
            if (!this.props.hasOwnProperty("data")) throw 'Data no encontrada';
            if (this.props.data.length <= 0) throw 'Data no encontrada';
        }
    }, {
        key: "getOptions",
        value: function getOptions() {
            var titulo = this.getTitulo().titulo;
            var subtitulo = this.getTitulo().subtitulo;
            var legend = this.getLegend();
            var series = this.getSeries();
            var option = {
                title: {
                    text: titulo,
                    subtext: subtitulo
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    show: true,
                    // orient : 'vertical',
                    x: 'center',
                    y: 'bottom',
                    data: legend
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: { show: false },
                        dataView: { show: false, readOnly: false },
                        magicType: {
                            show: true,
                            type: ['pie', 'funnel'],
                            option: {
                                funnel: {
                                    x: '25%',
                                    width: '50%',
                                    funnelAlign: 'left',
                                    max: 1548
                                }
                            }
                        },
                        restore: { show: true },
                        saveAsImage: { show: true, name: this.props.nameseries }
                    }
                },
                calculable: true,
                series: [{
                    name: this.props.nameseries,
                    type: 'pie',
                    radius: '40%',
                    center: ['50%', '40%'],
                    data: series
                }]
            };

            return option;
        }
    }, {
        key: "render",
        value: function render() {
            this.validParams();
            var option = this.getOptions();
            return React.createElement(Graficas, { options: option, id: this.props.id, height: this.props.height });
        }
    }]);

    return Pastel;
}(React.Component);

var Brujula = function (_React$Component5) {
    _inherits(Brujula, _React$Component5);

    function Brujula(props) {
        _classCallCheck(this, Brujula);

        return _possibleConstructorReturn(this, (Brujula.__proto__ || Object.getPrototypeOf(Brujula)).call(this, props));
    }

    _createClass(Brujula, [{
        key: "getLegend",
        value: function getLegend() {
            var legend = [];
            if (this.props.hasOwnProperty("legend") && this.props.legend.length > 0) {
                return this.props.legend;
            } else {
                var _legend3 = this.props.data.map(function (e) {
                    return e.label;
                });
                return _legend3;
            }

            return legend;
        }
    }, {
        key: "getTitulo",
        value: function getTitulo() {
            var encabezado = {
                titulo: "",
                subtitulo: ""
            };
            if (this.props.hasOwnProperty("titulo") && this.props.titulo != "") {
                encabezado.titulo = this.props.titulo;
            }
            if (this.props.hasOwnProperty("subtitulo") && this.props.subtitulo != "") {
                encabezado.subtitulo = this.props.subtitulo;
            }

            return encabezado;
        }
    }, {
        key: "getSeries",
        value: function getSeries() {
            if (this.props.hasOwnProperty("series") && this.props.series.length > 0) {
                return this.props.series;
            } else {
                var series = this.props.data.map(function (e) {
                    return {
                        name: e.label,
                        value: new Number(e.value).toFixed(2),
                    };
                });

                return series;
            }
        }
    }, {
        key: "validParams",
        value: function validParams() {
            if (!this.props.hasOwnProperty("data")) throw 'Data no encontrada';
            if (this.props.data.length <= 0) throw 'Data no encontrada';
        }
    }, {
        key: "getOptions",
        value: function getOptions() {
            var titulo = this.getTitulo().titulo;
            var subtitulo = this.getTitulo().subtitulo;
            //var legend = this.getLegend();
            var series = this.getSeries();
            
            var option = {
                title : {
                    text: titulo,
                    subtext: subtitulo,
                },
                tooltip: {
                    trigger: 'item',
                    position: ['48.5%', '49.2%'],
                    backgroundColor: 'rgba(50,50,50,0)',
                    textStyle : {
                        color: 'blue',
                        fontWeight: 'bold'
                    },
                    formatter: "{d}%"
                },
                toolbox: {
                    show: true,
                    feature: {
                        saveAsImage: { show: true, name: this.props.nameseries }
                    }
                },
                series : [
                    {
                        name: 'Series',
                        type: 'pie',
                        startAngle : 90,
                        radius : ['5%', '70%'],
                        roseType: 'area',
                        color:['red'],
                        data: series,
                        labelLine: {
                            normal: {
                                show: true
                            }
                        },
                        label: {
                            normal: {
                                show: true,
                                color : 'red',
                                fontSize : 20
                            }
                        },
                        itemStyle: {
                            normal: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            },
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            }
                        }
                    },
                    {
                        name: '',
                        type: 'gauge',
                        min: 0,
                        max: 8,
                        startAngle: 90,
                        endAngle: 449.9,
                        radius: '100%',
                        splitNumber: 8,
                        clockwise: false,
                        animation: false,
                        detail: {
                            formatter: '{value}',
                            textStyle: {
                                color: 'red'
                            }
                        },
                        detail:{
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        axisLine: {
                            lineStyle: {
                                color: [
                                    [0.25, '#63869e'],
                                    [0.75, '#ffffff'],
                                    [1, '#63869e']
                                ],
                                width: '40%',
                                shadowColor: '#0d4b81', //默认透明
                                shadowBlur: 10,
                                opacity: 1
                            }
                        },
                        splitLine: {
                            length: 5,
                            lineStyle: {
                                color: '#ffffff',
                                width: 2
                            }
                        },
                        axisLabel: {
                            show : false,
                            formatter: function(v){
                                return v?v:'';
                            },
                            textStyle: {
                                color: "#f44242",
                                fontWeight: 700
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: 'green',
                                width: 2
                            }
                        }
                    },
                    {
                        name: '',
                        type: 'gauge',
                        min: 0,
                        max: 8,
                        startAngle: 90,
                        endAngle: 449.9,
                        radius: '72%',
                        splitNumber: 8,
                        clockwise: false,
                        animation: false,
                        detail: {
                            formatter: '{value}',
                            textStyle: {
                                color: 'red'
                            }
                        },
                        detail:{
                            show: false
                        },
                        axisTick: {
                            show: false
                        },
                        axisLine: {
                            lineStyle: {
                                color: [
                                    [1, '#E8E8E8']
                                ],
                                width: '10%',
                                opacity:0.8
                            }
                        },
                        splitLine: {
                            show:true,
                            length: '92%',
                            lineStyle: {
                                color: 'grey',
                                width: '1'
                            }
                        },
                        axisLabel: {
                            show:false,
                            formatter: function(v){
                                return v?v:'';
                            },
                            textStyle: {
                                color: "#fb5310",
                                fontWeight: 700
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: 'green',
                                width: 2,
                                borderWidth: 3,
                            }
                        }
                    }
                ]
            };

            return option;
        }
    }, {
        key: "render",
        value: function render() {
            this.validParams();
            var option = this.getOptions();
            return React.createElement(Graficas, { options: option, id: this.props.id, height: this.props.height });
        }
    }]);

    return Brujula;
}(React.Component);

var Medidor = function (_React$Component6) {
    _inherits(Medidor, _React$Component6);

    function Medidor(props) {
        _classCallCheck(this, Medidor);

        return _possibleConstructorReturn(this, (Medidor.__proto__ || Object.getPrototypeOf(Medidor)).call(this, props));
    }

    _createClass(Medidor, [{
        key: "getLegend",
        value: function getLegend() {
            var legend = [];
            if (this.props.hasOwnProperty("legend") && this.props.legend.length > 0) {
                return this.props.legend;
            } else {
                var _legend3 = this.props.data.map(function (e) {
                    return e.label;
                });
                return _legend3;
            }

            return legend;
        }
    }, {
        key: "getTitulo",
        value: function getTitulo() {
            var encabezado = {
                titulo: "",
                subtitulo: ""
            };
            if (this.props.hasOwnProperty("titulo") && this.props.titulo != "") {
                encabezado.titulo = this.props.titulo;
            }
            if (this.props.hasOwnProperty("subtitulo") && this.props.subtitulo != "") {
                encabezado.subtitulo = this.props.subtitulo;
            }

            return encabezado;
        }
    }, {
        key: "getSeries",
        value: function getSeries() {
            if (this.props.hasOwnProperty("series") && this.props.series.length > 0) {
                return this.props.series;
            } else {
                var series = this.props.data.map(function (e) {
                    return {
                        name: e.label,
                        value: new Number(e.value).toFixed(2),
                    };
                });

                return series;
            }
        }
    }, {
        key: "getUnidad",
        value: function getUnidad() {
            var unidades = ''
            if (this.props.hasOwnProperty("unidad") && this.props.unidad != "") {
                unidades = this.props.unidad;
            }

            return unidades;
        }
    }, {
        key: "getValor",
        value: function getValor() {
            var value = 0
            if (this.props.hasOwnProperty("value") && this.props.value != "") {
                value = this.props.value;
            }

            return value;
        }
    }, {
        key: "validParams",
        value: function validParams() {
            
        }
    }, {
        key: "getOptions",
        value: function getOptions() {
            let titulo = this.getTitulo().titulo,
                subtitulo = this.getTitulo().subtitulo,
                unidades = this.getUnidad(),
                valor = this.getValor()
            
            var option = {
                tooltip : {
                    formatter: `{c} ${unidades}`
                },
                toolbox: {
                    feature: {
                        restore: {},
                        saveAsImage: {}
                    }
                },
                series: [
                    {
                        type: 'gauge',
                        detail: {
                            formatter:`{value}\n${unidades}`
                        },
                        data: [{value: valor, name: ``}]
                    }
                ]
            };

            return option;
        }
    }, {
        key: "render",
        value: function render() {
            this.validParams();
            var option = this.getOptions();
            return React.createElement(Graficas, { options: option, id: this.props.id, height: this.props.height });
        }
    }]);

    return Medidor;
}(React.Component);

var Radar = function (_React$Component7) {
    _inherits(Radar, _React$Component7);

    function Radar(props) {
        _classCallCheck(this, Radar);

        return _possibleConstructorReturn(this, (Radar.__proto__ || Object.getPrototypeOf(Radar)).call(this, props));
    }

    _createClass(Radar, [{
        key: "getLegend",
        value: function getLegend() {
            var legend = [];
            if (this.props.hasOwnProperty("series") && this.props.series.length > 0) {
                var _legend3 = this.props.series.map(function (e) {
                    return e.name;
                });
                return _legend3;
            } else {
                var _legend3 = this.props.data.map(function (e) {
                    return e.label;
                });
                return _legend3;
            }

            return legend;
        }
    }, {
        key: "getTitulo",
        value: function getTitulo() {
            var encabezado = {
                titulo: "",
                subtitulo: ""
            };
            if (this.props.hasOwnProperty("titulo") && this.props.titulo != "") {
                encabezado.titulo = this.props.titulo;
            }
            if (this.props.hasOwnProperty("subtitulo") && this.props.subtitulo != "") {
                encabezado.subtitulo = this.props.subtitulo;
            }

            return encabezado;
        }
    }, {
        key: "getSeries",
        value: function getSeries() {
            if (this.props.hasOwnProperty("series") && this.props.series.length > 0) {
                return this.props.series;
            } else {
                var series = this.props.data.map(function (e) {
                    return {
                        name: e.label,
                        value: new Number(e.value).toFixed(2),
                        itemStyle: {
                            normal: {
                                color: '#F9713C'
                            }
                        },
                    };
                });

                return series;
            }
        }
    }, {
        key: "getUnidad",
        value: function getUnidad() {
            var unidades = ''
            if (this.props.hasOwnProperty("unidad") && this.props.unidad != "") {
                unidades = this.props.unidad;
            }

            return unidades;
        }
    }, {
        key: "validParams",
        value: function validParams() {
            if (!this.props.hasOwnProperty("series")) throw 'Data no encontrada';
            if (this.props.series.length <= 0) throw 'Data no encontrada';
        }
    }, {
        key: "getOptions",
        value: function getOptions() {
            var titulo = this.getTitulo().titulo;
            var subtitulo = this.getTitulo().subtitulo;
            var unidades = this.getUnidad();
            var series = this.getSeries();
            var legends = this.getLegend();
            
            var option = {
                title: {
                    text: titulo
                },
                tooltip: {},
                legend: {
                    data: legends
                },
                radar: {
                    // shape: 'circle',
                    name: {
                        textStyle: {
                            borderRadius: 3,
                            padding: [3, 5]
                       }
                    },
                    indicator: this.props.legend
                },
                series: [{
                    name: 'Radar',
                    type: 'radar',
                    data : series
                }]
            };

            return option;
        }
    }, {
        key: "render",
        value: function render() {
            this.validParams();
            var option = this.getOptions();
            return React.createElement(Graficas, { options: option, id: this.props.id, height: this.props.height });
        }
    }]);

    return Radar;
}(React.Component);

var BarrasStacked = function (_React$Component8) {
    _inherits(BarrasStacked, _React$Component8);

    function BarrasStacked(props) {
        _classCallCheck(this, BarrasStacked);

        var _this3 = _possibleConstructorReturn(this, (BarrasStacked.__proto__ || Object.getPrototypeOf(BarrasStacked)).call(this, props));

        _this3.state = {
            umbral: _this3.props.umbral
        };
        return _this3;
    }

    _createClass(BarrasStacked, [{
        key: "getLegend",
        value: function getLegend() {
            var legend = [];
            if (this.props.hasOwnProperty("legend") && this.props.legend.length > 0) {
                return this.props.legend;
            } else {
                var _legend2 = this.props.data.map(function (e) {
                    return e.label;
                });
                return _legend2;
            }

            return legend;
        }
    }, {
        key: "getTitulo",
        value: function getTitulo() {
            var encabezado = {
                titulo: "",
                subtitulo: ""
            };
            if (this.props.hasOwnProperty("titulo") && this.props.titulo != "") {
                encabezado.titulo = this.props.titulo;
            }
            if (this.props.hasOwnProperty("subtitulo") && this.props.subtitulo != "") {
                encabezado.subtitulo = this.props.subtitulo;
            }

            return encabezado;
        }
    }, {
        key: "getEjeX",
        value: function getEjeX() {
            var ejeX = [];
            if (this.props.hasOwnProperty("ejeX") && this.props.ejeX) {
                var _ejeX = this.props.data.map(function (e) {
                    return e.label;
                });
                return _ejeX;
            }
            return ejeX;
        }
    },{
        key: "getNames",
        value: function getNames() {
            if (this.props.hasOwnProperty("series") && this.props.series.length > 0) {
                return this.props.series.map((s) => s.name); 
            } else {
                let groups = []
                for(let i in this.props.data){
                    let s = this.props.data[i]
                    if(!groups.includes(s.group)){
                        groups.push(s.group)
                    }
                }

                return groups;
            }
        }
    }, {
        key: "getSeries",
        value: function getSeries() {
            if (this.props.hasOwnProperty("series") && this.props.series.length > 0) {
                return this.props.series;
            } else {
                var markLine = {
                    name: "Umbral",
                    value: parseFloat(this.state.umbral),
                    xAxis: -1,
                    yAxis: parseFloat(this.state.umbral)
                };
                var series = this.props.data.map(function (e) {
                    return {
                        name: e.label,
                        type: 'bar',
                        data: [new Number(e.value).toFixed(2)],
                        markLine: null,
                        stacked : 'stack'
                    };
                });

                if (this.state.umbral > 0 && series.length > 0) {
                    series[0].markLine = {
                        data: []
                    };
                    series[0].markLine.data.push(markLine);
                }

                return series;
            }
        }
    }, {
        key: "validParams",
        value: function validParams() {
            return true;
        }
    }, {
        key: "getOptions",
        value: function getOptions() {
            var titulo = this.getTitulo().titulo;
            var subtitulo = this.getTitulo().subtitulo;
            var legend = this.getLegend();
            var series = this.getSeries();
            var option = {
                title: {
                    text: titulo,
                    subtext: subtitulo
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    show : this.props.hasOwnProperty('showLegends') ? this.props.showLegends : true,
                    data: this.getNames(),
                    bottom: '1%'
                },
                toolbox: {
                    show: true,
                    feature: {
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    }
                },
                yAxis: {
                    type: 'value',
                    min : this.props.hasOwnProperty('min') ? this.props.min : null
                },
                xAxis: {
                    type : 'category',
                    data : legend
                },
                series: series
            };
            if(this.props.grid){
                option.grid = this.props.grid
            }
            if(this.props.orientation == 'horizontal'){
                option.xAxis = {
                    type: 'value',
                    min : this.props.hasOwnProperty('min') ? this.props.min : null
                }
                option.yAxis = {
                    type : 'category',
                    data : legend
                }
            }
            return option;
        }
    },{
        key : "componentDidMount",
        value: function componentDidMount(){
            var option = this.getOptions();
            let chart = echarts.init(document.getElementById(this.props.id+'-child'), 'default')
            chart.setOption(option)
        }
    }, {
        key: "render",
        value: function render() {
            this.validParams();
            return React.createElement("div", { id : this.props.id+'-child', className : 'charts-400' });
        }
    }]);

    return BarrasStacked;
}(React.Component);