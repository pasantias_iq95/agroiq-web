"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function formatMoney(num, c){
    var n = num, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

var Tags = function (_React$Component) {
    _inherits(Tags, _React$Component);

    function Tags(props) {
        _classCallCheck(this, Tags);

        var _this = _possibleConstructorReturn(this, (Tags.__proto__ || Object.getPrototypeOf(Tags)).call(this, props));

        _this.state = {
            valor: _this.props.valor,
            promedio: _this.props.promedio,
            longitud: _this.props.longitud,
            tittle: _this.props.tittle,
            subtittle: _this.props.subtittle,
            tipo: _this.props.tipo,
            cssClass: _this.props.cssClass,
            withTheresholds: _this.props.withTheresholds,
            umbrales: {},
            miles : _this.props.hasOwnProperty('miles') ? _this.props.miles : false
        };
        return _this;
    }

    _createClass(Tags, [{
        key: 'getClassName',
        value: function getClassName() {
            if (this.state.withTheresholds) {
                if (this.state.valor >= parseFloat(this.state.umbrales.green_umbral_1)) return 'font-green-jungle';else if (this.state.valor >= parseFloat(this.state.umbrales.yellow_umbral_1) && this.state.valor <= parseFloat(this.state.umbrales.yellow_umbral_2)) return 'font-yellow-lemon';else return 'font-red-thunderbird';
            } else {
                return 'font-' + this.state.cssClass;
            }
        }
    }, {
        key: 'getClassProgress',
        value: function getClassProgress() {
            if (this.state.withTheresholds) {
                if (this.state.valor >= parseFloat(this.state.umbrales.green_umbral_1)) return 'progress-bar progress-bar-success green-jungle';else if (this.state.valor >= parseFloat(this.state.umbrales.yellow_umbral_1) && this.state.valor <= parseFloat(this.state.umbrales.yellow_umbral_2)) return 'progress-bar progress-bar-success yellow-lemon';else return 'progress-bar progress-bar-success red-thunderbird';
            } else {
                return 'progress-bar progress-bar-success ' + this.state.cssClass;
            }
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(newProps) {
            newProps.valor = parseFloat(newProps.valor);
            this.state.valor = parseFloat(this.state.valor);
            if (newProps.valor != this.state.valor) {
                this.setState({
                    valor: newProps.valor
                });
            }
            if (newProps.promedio != this.state.promedio) {
                this.setState({
                    promedio: newProps.promedio
                });
            }
            if (newProps.withTheresholds != this.state.withTheresholds) {
                this.setState({
                    withTheresholds: newProps.withTheresholds
                });
            }
            if (newProps.tipo != this.state.tipo) {
                this.setState({
                    tipo: newProps.tipo
                });
            }
            if (newProps.cssClass != this.state.cssClass) {
                this.setState({
                    cssClass: newProps.cssClass
                });
            }
            if (newProps.tittle != this.state.tittle) {
                this.setState({
                    tittle: newProps.tittle
                });
            }
            if (newProps.subtittle != this.state.subtittle) {
                this.setState({
                    subtittle: newProps.subtittle
                });
            }
            this.setState({
                umbrales: this.props.umbrales
            });
        }
    }, {
        key: 'reload',
        value: function reload() {
            setTimeout(function () {
                $(".counter_tags").counterUp({
                    delay: 10,
                    time: 1000
                });
            }, 1000);
        }
    }, {
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.reload();
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate() {
            this.reload();
        }
    }, {
        key: 'isInt',
        value: function isInt(n) {
            return Number(n) === n && n % 1 === 0;
        }
    }, {
        key: 'getNumber',
        value: function getNumber(n, miles = false) {
            if (this.isInt(n) && !miles) 
                return parseInt(n);
            else 
                if(miles) 
                    return formatMoney(new Number(parseFloat(n)).toFixed(2), 2)
                else
                    return new Number(parseFloat(n)).toFixed(2);
        }
    }, {
        key: 'render',
        value: function render() {
            var classType = this.getClassName();
            var classProgressBar = this.getClassProgress();
            var valor = this.getNumber(this.state.valor, this.state.miles);
            var promedio = this.getNumber(this.state.promedio);
            var tittle = this.state.tittle;
            var tipo = this.state.tipo;
            var Subtittle = this.state.subtittle;
            var widthStyle = this.state.promedio + '%';
            return React.createElement(
                'div',
                { className: this.state.longitud },
                React.createElement(
                    'div',
                    { className: 'dashboard-stat2 ' },
                    React.createElement(
                        'div',
                        { className: 'display' },
                        React.createElement(
                            'div',
                            { className: 'number' },
                            React.createElement(
                                'h3',
                                { className: classType },
                                React.createElement('span', { ref: 'counter_tags', className: 'counter_tags', 'data-value': valor }),
                                React.createElement(
                                    'small',
                                    { className: classType },
                                    ' ',
                                    tipo,
                                    ' '
                                )
                            ),
                            React.createElement(
                                'small',
                                null,
                                tittle
                            )
                        ),
                        React.createElement(
                            'div',
                            { className: 'icon' },
                            React.createElement('i', { className: 'icon-settings' })
                        )
                    ),
                    React.createElement(
                        'div',
                        { className: 'progress-info' },
                        React.createElement(
                            'div',
                            { className: 'progress' },
                            React.createElement(
                                'span',
                                { style: { width: widthStyle }, className: classProgressBar },
                                React.createElement(
                                    'span',
                                    { className: 'sr-only' },
                                    promedio,
                                    '%'
                                )
                            )
                        ),
                        React.createElement(
                            'div',
                            { className: 'status' },
                            React.createElement(
                                'div',
                                { className: 'status-title' },
                                Subtittle
                            ),
                            React.createElement(
                                'div',
                                { className: 'status-number' },
                                promedio,
                                '%'
                            )
                        )
                    )
                )
            );
        }
    }]);

    return Tags;
}(React.Component);

var ListTags = function (_React$Component2) {
    _inherits(ListTags, _React$Component2);

    function ListTags(props) {
        _classCallCheck(this, ListTags);

        return _possibleConstructorReturn(this, (ListTags.__proto__ || Object.getPrototypeOf(ListTags)).call(this, props));
    }

    _createClass(ListTags, [{
        key: 'getTags',
        value: function getTags() {
            var colums = this.props.colums;
            var tags = this.props.tags;
            var counTags = tags.length;
            var umbrales = this.props.umbrales;
            var withTheresholds = this.props.withTheresholds;

            var cols = {
                1: 12,
                2: 6,
                3: 4,
                4: 3
            };

            var col = cols[colums];

            var className = 'col-lg-' + col + ' col-md-' + col + ' col-sm-6 col-xs-12';

            var list = tags.map(function (e) {
                return React.createElement(Tags, { longitud: className, cssClass: e.cssClass, withTheresholds: withTheresholds, promedio: e.promedio, valor: e.valor, tipo: '', tittle: e.tittle, subtittle: e.subtittle, umbrales: umbrales, miles : e.miles });
            });

            return list;
        }
    }, {
        key: 'render',
        value: function render() {
            var Tags = this.getTags();
            return React.createElement(
                'div',
                { className: 'row' },
                Tags
            );
        }
    }]);

    return ListTags;
}(React.Component);
