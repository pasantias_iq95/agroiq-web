import React from 'react'
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, ResponsiveContainer, Tooltip, ReferenceLine} from 'recharts';

class BarsChart extends React.Component {
	render () {
        const { data, umbral } = this.props
        return (
            <div style={{height: 400}}>
                <ResponsiveContainer>
                    <BarChart data={data} margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                        <XAxis dataKey="name"/>
                        <YAxis/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip />

                        { umbral &&
                            <ReferenceLine y={umbral} label="" stroke="red"/>
                        }
                        <Bar dataKey="value" name="KM" fill="#2ca02c" label={{ position: 'top' }}>
                            
                        </Bar>
                        <Bar dataKey="value2" name="Galones" fill="#d62728" label={{ position: 'top' }}>
                            
                        </Bar>
                    </BarChart>
                </ResponsiveContainer>
            </div>
        );
    }
}

export default BarsChart