import React from 'react';
import { Sector, Cell, PieChart, Pie, ResponsiveContainer } from 'recharts';

class GaugeChart extends React.Component {

    render(){
        const height = 200;
        const chartValue = this.props.value;
        const colorData = [{
                value: 40, // Meaning span is 0 to 40
                color: '#26C281'
            }, {
                value: 40, // span 140 to 190
                color: '#E87E04'
            }, {
                value: 40, // span > 190
                color: '#D91E18'
            }
        ];

        const activeSectorIndex = colorData.map((cur, index, arr) => {
            const curMax = [...arr]
                .splice(0, index + 1)
                .reduce((a, b) => ({ value: a.value + b.value }))
                .value;
            return (chartValue > (curMax - cur.value)) && (chartValue <= curMax);
        })
        .findIndex(cur => cur);

        const sumValues = colorData
            .map(cur => cur.value)
            .reduce((a, b) => a + b);

        const arrowData = [
            { value: chartValue },
            { value: 0 },
            { value: sumValues - chartValue }
        ];

        const pieProps = {
            startAngle: 180,
            endAngle: 0,
            cx: height,
            cy: 150
        };

        const pieRadius = {
            innerRadius: (height / 1) * 0.35,
            outerRadius: (height / 1) * 0.4
        };

        const Arrow = ({ cx, cy, midAngle, outerRadius, innerRadius, startAngle, endAngle, fill }) => { //eslint-disable-line react/no-multi-comp
            const RADIAN = Math.PI / 180;
            const sin = Math.sin(-RADIAN * midAngle);
            const cos = Math.cos(-RADIAN * midAngle);
            const mx = cx + (outerRadius + height * 0.3) * cos;
            const my = cy + (outerRadius + height * 0.3) * sin;
            const ex = mx + (cos >= 0 ? 1 : -1) * 22;
            const ey = my;
            const textAnchor = cos >= 0 ? 'start' : 'end';
            return (
                <g>
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={innerRadius}
                        outerRadius={outerRadius}
                        startAngle={startAngle}
                        endAngle={endAngle}
                        fill={fill}
                    />
                    <Sector
                        cx={cx}
                        cy={cy}
                        startAngle={startAngle}
                        endAngle={endAngle}
                        innerRadius={outerRadius + 6}
                        outerRadius={outerRadius + 10}
                        fill={fill}
                    />
                    <circle cx={cx} cy={cy} r={height * 0.05} fill="#666" stroke="none"/>
                    {/*<text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`${chartValue} Km/h`}</text>*/}
                    <path d={`M${cx},${cy}L${mx},${my}`} strokeWidth="6" stroke="#666" fill="none" strokeLinecap="round"/>
                </g>
            );
        };

        const ActiveSectorMark = ({ cx, cy, innerRadius, outerRadius, startAngle, endAngle, fill }) => { //eslint-disable-line react/no-multi-comp
            return (
                <g>
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={innerRadius}
                        outerRadius={outerRadius * 1.2}
                        startAngle={startAngle}
                        endAngle={endAngle}
                        fill={fill}
                    />
                </g>
            );
        };

        return (
            <div className="row" style={{height: 200}}>
                <ResponsiveContainer>
                    <PieChart>
                        <Pie
                            activeIndex={activeSectorIndex}
                            activeShape={ActiveSectorMark}
                            data={colorData}
                            fill="#8884d8"
                            { ...pieRadius }
                            { ...pieProps }
                        >
                            {
                                colorData.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={colorData[index].color} />
                                ))
                            }
                        </Pie>
                        <Pie
                            stroke="none"
                            activeIndex={1}
                            activeShape={ Arrow }
                            data={ arrowData }
                            outerRadius={ pieRadius.innerRadius }
                            fill="none"
                            { ...pieProps }
                        />
                    </PieChart>
                </ResponsiveContainer>
            </div>
        );
    }
};

export default GaugeChart;