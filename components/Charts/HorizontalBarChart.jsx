import React from 'react'
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, ResponsiveContainer, Cell, Tooltip, ReferenceLine} from 'recharts';
const COLORS = ["#2ca02c", "#d62728", "#bcbd22", "#1f77b4", "#ff7f0e", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#17becf"];

class BarsChart extends React.Component {
	render () {
        const { data, umbral } = this.props
        return (
            <div style={{height: 400}}>
                <ResponsiveContainer>
                    <BarChart layout="vertical" data={data} margin={{top: 20, right: 30, left: 20, bottom: 5}}>
                        <XAxis type="number"/>
                        <YAxis dataKey="name" type="category"/>
                        <CartesianGrid strokeDasharray="3 3"/>
                        <Tooltip />

                        { umbral &&
                            <ReferenceLine y={umbral} label="" stroke="red"/>
                        }
                        <Bar dataKey="value" name="Total" fill="#8884d8" label={{ position: 'top' }}>
                            {
                                data.map((entry, index) => (
                                    <Cell key={`cell-${index}`} fill={COLORS[index % 20]}/>
                                ))
                            }
                        </Bar>
                    </BarChart>
                </ResponsiveContainer>
            </div>
        );
    }
}

export default BarsChart