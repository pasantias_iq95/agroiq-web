import React from 'react'

import { PieChart, Pie, Legend, Cell, ResponsiveContainer, Sector } from 'recharts';
const COLORS = ["#2ca02c", "#d62728", "#bcbd22", "#1f77b4", "#ff7f0e", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#17becf"];


const renderActiveShape = (props) => {
    const RADIAN = Math.PI / 180;
    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';
  
    return (
      <g>
        <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius}
          outerRadius={outerRadius}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
        <Sector
          cx={cx}
          cy={cy}
          startAngle={startAngle}
          endAngle={endAngle}
          innerRadius={outerRadius + 6}
          outerRadius={outerRadius + 10}
          fill={fill}
        />
        <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none"/>
        <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none"/>
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`Total ${value}`}</text>
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
            {`(${(percent * 100).toFixed(2)}%)`}
        </text>
      </g>
    );
  };

class BarsChart extends React.Component {

    state = {activeIndex: 0}

    constructor(props){
        super(props)
        this.onPieEnter = this.onPieEnter.bind(this)
    }

    onPieEnter(data, index) {
        this.setState({
            activeIndex: index,
        });
    }

	render () {
        const { data } = this.props
        return (
            <div style={{height: 400}}>
                <ResponsiveContainer>
                    <PieChart>
                        <Pie
                            data={data}
                            fill="#8884d8"
                            dataKey="value"
                            animationEasing={'ease-in'}
                            animationDuration={1000}
                            innerRadius={60}
                            outerRadius={80} 
                            onMouseEnter={this.onPieEnter}
                            activeIndex={this.state.activeIndex}
                            activeShape={renderActiveShape} 
                        >
                            {
                                data.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]}/>)
                            }
                        </Pie>
                        <Legend align="center" layout="horizontal" height={125}/>
                    </PieChart>
                </ResponsiveContainer>
            </div>
        );
    }
}

export default BarsChart