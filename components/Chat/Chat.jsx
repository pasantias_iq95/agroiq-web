import React from 'react'
import App from './components/App'

const styles = {
    message : {

    },
    ownMessage : {
        display: 'inline-block',
        maxWidth: '100%',
        marginBottom: '0.1em',
        backgroundcolor: 'rgb(13, 68, 155)',
        color: 'rgb(255, 255, 255)',
        borderTopLeftRadius: '1.4em',
        borderTopRightRadius: '1.4em',
        borderBottomRightRadius: '0.3em',
        borderBottomLeftRadius: '1.4em',
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: 'rgba(0, 0, 0, 0.05)',
        borderImage: 'initial'
    }
}

class Chat extends React.Component {
    state = {
        users : {
            '1' : {
                avatarUrl : 'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-1/p160x160/25594157_1854751144566521_5833514752748371997_n.jpg?_nc_cat=0&oh=b65c5cf7dccd36e4c6b6f0e1c4da3ac4&oe=5C078EDD'
            }
        },
        events : [],
        currentAgent : {
            avatarUrl : 'https://scontent.fgye1-1.fna.fbcdn.net/v/t1.0-1/p160x160/25594157_1854751144566521_5833514752748371997_n.jpg?_nc_cat=0&oh=b65c5cf7dccd36e4c6b6f0e1c4da3ac4&oe=5C078EDD',
            name : 'Victor'
        }
    }

    constructor(props){
        super(props)
        this.onMessageSend = this.onMessageSend.bind(this)
    }

    componentDidMount(){
        let self = this
        if(self.props.socket){
            self.props.socket.on('new user', (data) => {
                self.setState({
                    users : self.state.users.push({
                        name : data.name,
                        id_socket : data.id_socket,
                        lastMessage : {
                            message : '',
                            hour : '',
                            watched : true
                        },
                        messages : []
                    })
                })
            })

            self.props.socket.on('change name user', (data) => {
                let users = self.state.users
                for(let i in users){
                    if(users[i].id_socket === data.id_socket){
                        users[i].name = data.name
                        break;
                    }
                }
                self.setState({
                    users
                })
            })

            self.props.socket.on('new message', (data) => {
                let users = self.state.users
                for(let i in users){
                    if(users[i].id_socket === data.id_socket){
                        users[i].lastMessage = {
                            message: data.message,
                            hour : data.hour,
                            watched : false
                        }
                        users[i].messages.push({
                            message: data.message,
                            hour : data.hour,
                            watched : false
                        })
                        break;
                    }
                }
                self.setState({
                    users
                })
            })
        }
    }

    onMessageSend(message){
        let events = this.state.events
        if(events.length === 0){
            events.push([
                {
                    authorName : 'Victor',
                    text : message,
                    own : true,
                    authorId : 1,
                    id : 1,
                    style : styles.ownMessage,
                    radiusType : 'first'
                }
            ])
        }else{
            events[0].push({
                authorName : 'Victor',
                text : message,
                own : true,
                authorId : 1,
                id : 2,
                style : styles.ownMessage,
            })
        }
        this.setState({
            events
        })
        if(this.props.socket.connected){
            this.props.socket.emit('send message', {
                message,
                to_id_socket : '' 
            })
        }
    }

    render(){
        const { users, events, currentAgent } = this.state
        return (
            <App 
                onMessageSend={this.onMessageSend} 
                sendMessage={this.onMessageSend}
                events={events}
                users={users}
                currentAgent={currentAgent}
            />
        )
    }
}

export default Chat