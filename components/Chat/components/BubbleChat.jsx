import React from 'react'
import store from './../reducers'
import { IconButton } from '@material-ui/core'

class BubbleChat extends React.Component {

    onClick(){
        if(this.props.onClick){
            this.props.onClick(this.props)
        }
        console.log("hola")
        store.dispatch({
            type : 'OPEN_CHAT',
            id_socket : this.props.id_socket,
            name : this.props.name
        })
    }

    render(){
        const { avatarUrl } = this.props
        const styles = {
            bubble: {
                backgroundImage: `url(${avatarUrl})`,
                backgroundRepeat: 'no-repeat',
                backgroundSize : 'cover',
                width: 60,
                height: 60
            }
        };

        return (
            <div
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: '60px',
                    height: '60px',
                    background: '#0093FF',
                    color: '#fff',
                    borderRadius: '50%',
                    cursor: 'pointer'
                }}>
                <IconButton style={styles.bubble} onClick={this.onClick.bind(this)}>
                </IconButton>
            </div>
        )
    }
}

export default BubbleChat