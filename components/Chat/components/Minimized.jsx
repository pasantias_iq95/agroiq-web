import React from 'react'
import store from './../reducers'
import { IconButton, ChatIcon } from '@livechat/ui-kit'

class Minimized extends React.Component {

	constructor(props){
		super(props)
		this.maximize = this.maximize.bind(this)
	}

	componentDidMount(){
		let self = this
		store.subscribe(() => {
			let state = store.getState()
			if(state.lastAction === 'OPEN_CHAT'){
				self.props.maximize()
			}
		})
	}

	maximize(){
		this.props.maximize()
	}

	render(){
		return (
			<div
				onClick={this.maximize}
				style={{
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center',
					width: '60px',
					height: '60px',
					background: '#0093FF',
					color: '#fff',
					borderRadius: '50%',
					cursor: 'pointer',
				}}
			>
				<IconButton color="#fff">
					<ChatIcon />
				</IconButton>
			</div>
		)
	}
}

export default Minimized
