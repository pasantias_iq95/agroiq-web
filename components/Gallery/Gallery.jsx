import React from 'react'
import ImageGallery from 'react-image-gallery'
import "react-image-gallery/styles/css/image-gallery.css";
import "./gallery.css"

class Gallery extends React.Component {
  render() {
        const { images } = this.props
        return (
            <ImageGallery 
                items={images}
                lazyLoad={true}
             />
        )
    }
}
export default Gallery