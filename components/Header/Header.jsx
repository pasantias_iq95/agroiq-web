import React from 'react'
import Logo from './../../logo.png'
import Auth from 'utils/store/session'
import { Link } from 'react-router-dom'
const user_data_demo = {
    label : 'IQ DEMO', 
    image : 'https://s3.amazonaws.com/json-publicos/iqicon.png'
}

class Header extends React.Component {

    constructor(props){
        super(props)
        this.logout = this.logout.bind(this)
    }

    logout(){
        Auth.dispatch({
            type : 'LOGOUT'
        })
    }

    render(){
        const user_data = localStorage.getItem('user_data') ? JSON.parse(localStorage.getItem('user_data')) : user_data_demo
        return ( 
            <div className="page-header navbar navbar-fixed-top">
                <div className="page-header-inner" style={{ width: '100%' }}>
                    <div className="page-logo" style={{display : 'flex', justifyContent : 'center'}}>
                        <img 
                            src={Logo} alt="logo" className="logo-default"
                            style={{
                                width: 132, 
                                margin : 'auto'
                            }} />
                    </div>
                    <a className="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                    <div className="page-top">
                        <div className="top-menu">
                            <ul className="nav navbar-nav pull-right">
                                <li className="dropdown dropdown-user dropdown-dark" style={{ minWidth : 185 }}>
                                    <a className="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <span className="username username-hide-on-mobile"> { user_data.label } </span>
                                        <img alt="" className="logo-default" src={user_data.image} style={{borderRadius : '50%'}} />
                                    </a>
                                    <ul className="dropdown-menu dropdown-menu-default">
                                        <li>
                                            <Link to={{
                                                pathname : '/perfil'
                                            }}>
                                                <i className="fas fa-user-circle"></i> Mi Perfil
                                            </Link>
                                        </li>
                                        <li>
                                            <a role="button" onClick={this.logout}>
                                                <i className="fas fa-sign-out-alt"></i> Log Out 
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Header.defaultProps = {
    user : '',
    LogoModule : '',
    Logo : ''
}

export default Header