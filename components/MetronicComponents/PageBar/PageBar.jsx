import React from 'react'

class PageBar extends React.Component {
    render(){
        const { path, toolbar, style } = this.props
        return (
            <div className="page-bar" style={style}>
                {path &&
                    <ul className="page-breadcrumb">
                        { path &&
                            <li>
                                <i className="icon-home"></i>
                                <a>{ path.home.name }</a>
                                <i className="fa fa-angle-right"></i>
                            </li>
                        }
                    </ul>
                }
                <div className="page-toolbar">
                    { toolbar }
                </div>
            </div>
        )
    }
}

PageBar.defaultProps = {
    path : null,
    toolbar : null,
    style : {}
}

export default PageBar