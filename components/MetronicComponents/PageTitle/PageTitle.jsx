import React from 'react'

class PageTitle extends React.Component {
    render(){
        const { title } = this.props
        return (
            <div>
                <h4 className="page-title">
                    { title }
                </h4>
            </div>
        )
    }
}

export default PageTitle