import React from 'react'

class GroupItemMenu extends React.Component {

    state = {
        expended : 'hide'
    }

    toggleShow(){
        this.setState({
            expended : this.state.expended === 'hide' ? 'show' : 'hide'
        })
    }

    render(){
        const { title, icon, iconClass } = this.props
        const { expended } = this.state
        return (
            <li className="nav-item">
                <a role="button" className="nav-link" onClick={this.toggleShow.bind(this)}>
                    <i className={iconClass} src={icon}></i>
                    <span className="title">{ title }</span>
                    <span className={`arrow ${expended === 'hide' ? '' : 'active open'}`}></span>
                </a>
                <ul className={`sub-menu ${expended}`}>
                    { this.props.children }
                </ul>
            </li>
        )
    }
}

export default GroupItemMenu