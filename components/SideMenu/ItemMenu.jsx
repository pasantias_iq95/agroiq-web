import React from 'react'
import { Link } from 'react-router-dom'

class ItemMenu extends React.Component {

    render(){
        const { name, href } = this.props
        return (
            <li className="nav-item">
                <Link to={{
                    pathname : href
                }}
                className="nav-link nav-toggle">
                    <span className="title">{ name }</span>
                    <span className="selected"></span>
                </Link>
            </li>
        )
    }
}

export default ItemMenu