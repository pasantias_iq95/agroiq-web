/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/arregui/mermadia.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/arregui/mermadia.js":
/*!****************************************!*\
  !*** ./js_modules/arregui/mermadia.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/*----------  UTILIDADES SOBRE ARRAYS  ----------*/\napp.filter('unique', function () {\n    return function (collection, keyname) {\n        var output = [],\n            keys = [];\n\n        angular.forEach(collection, function (item) {\n            var key = item[keyname];\n            if (keys.indexOf(key) === -1) {\n                keys.push(key);\n                output.push(item);\n            }\n        });\n\n        return output;\n    };\n});\n\nfunction loadScript(options) {}\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            // console.log(item)\n            if (item.hasOwnProperty(\"lote\")) {\n                if (!isNaN(parseFloat(item.lote))) item.lote = parseFloat(item.lote);\n            }\n            if (item.hasOwnProperty(\"total_peso_merma\")) {\n                item.total_peso_merma = parseFloat(item.total_peso_merma);\n            }\n            if (item.hasOwnProperty(\"total_defectos\")) {\n                if (!isNaN(parseFloat(item.total_defectos))) item.total_defectos = parseFloat(item.total_defectos);\n            }\n            if (item.hasOwnProperty(\"merma\")) {\n                if (!isNaN(parseFloat(item.merma))) item.merma = parseFloat(item.merma);\n            }\n            if (item.hasOwnProperty(\"danhos_peso\")) {\n                if (!isNaN(parseFloat(item.danhos_peso))) item.danhos_peso = parseFloat(item.danhos_peso);\n            }\n            if (item.hasOwnProperty(\"filter\")) {\n                if (!isNaN(parseFloat(item.filter))) item.filter = parseFloat(item.filter);\n            }\n            if (item.hasOwnProperty(\"cantidad\")) {\n                if (!isNaN(parseFloat(item.cantidad))) item.cantidad = parseFloat(item.cantidad);\n            }\n\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            //alert(a[field]);\n            return a[field] > b[field] ? 1 : -1;\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.controller('informe_merma_dia', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', function ($scope, $http, $interval, client, $controller, $timeout, $window) {\n\n    $scope.leyendaGeneralTitle = 'Merma';\n\n    $scope.id_company = 0;\n\n    $scope.labelLbKg = \"Kilos\";\n    $scope.statusLbKg = true;\n    $scope.labelPeso = \"\";\n\n    $scope.umbrales = {};\n\n    $scope.rangesDirectives = {\n        'Hoy': [moment(), moment()],\n        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],\n        'Hace 2 Días': [moment().subtract(2, 'days'), moment().subtract(2, 'days')]\n    };\n\n    $scope.limitDirectives = {\n        days: 0\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment(),\n        endDate: moment()\n    };\n\n    $scope.singleDatePickerDirective = true;\n\n    $scope.table = {\n        merma: [],\n        merma_travel: [],\n        merma_details: [],\n        merma_campos: [],\n        merma_widthCell: \"10%\",\n        defectos: [],\n        defectos_travel: [],\n        defectos_details: [],\n        details_campos: [],\n        peso: [],\n        peso_travel: [],\n        peso_details: [],\n        peso_campos: [],\n        peso_widthCell: \"10%\",\n        adicional: []\n    };\n\n    $scope.calidad = {\n        params: {\n            idFinca: \"\",\n            idFincaDia: \"\",\n            idMerma: \"MATERIA PRIMA\",\n            idLote: 0,\n            idLabor: 0,\n            fecha_inicial: moment().format('YYYY-MM-DD'),\n            fecha_final: moment().format('YYYY-MM-DD'),\n            cliente: \"\",\n            marca: \"\",\n            palanca: \"\",\n            statusLbKg: true,\n            categoria: \"COSECHA\"\n        },\n        step: 0,\n        path: ['phrapi/dia/index'],\n        templatePath: [],\n        nocache: function nocache() {\n            this.templatePath.push('/views/templetes/mermadia/step1.html?' + Math.random());\n        }\n\n    };\n\n    $scope.palancas = [];\n\n    $scope.mermas = {\n        NETA: \"Merma Neta\",\n        \"MATERIA PRIMA\": \"Merma Prima\"\n    };\n\n    var cambiosMerma = function cambiosMerma() {\n        $scope.calidad.params.palanca = \"\";\n        $scope.calidad.params.idFincaDia = \"\";\n        $scope.loadExternal();\n    };\n\n    $scope.loadExternal = function () {\n        //console.log($scope.calidad.path[$scope.calidad.step]);\n        if ($scope.calidad.path[$scope.calidad.step] != \"\") {\n            var data = $scope.calidad.params;\n            client.post($scope.calidad.path[$scope.calidad.step], $scope.startDetails, data);\n        }\n    };\n\n    cambiosCategoria = function cambiosCategoria() {\n        $scope.calidad.params.categoria = $('#categoria').val();\n        $scope.loadExternal();\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.calidad.params.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.wizardStep.params.fecha_inicial;\n            $scope.calidad.params.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.wizardStep.params.fecha_final;\n            $scope.loadExternal();\n        }\n    };\n\n    $scope.convertKg = function () {\n        $scope.statusLbKg = !$scope.statusLbKg;\n        if ($scope.statusLbKg === true) $scope.labelLbKg = \"Kilos\";else if ($scope.statusLbKg === false) $scope.labelLbKg = \"Libras\";\n\n        $scope.calidad.params.statusLbKg = $scope.statusLbKg;\n        $scope.loadExternal();\n    };\n\n    $scope.getKey = function (data, key) {\n        return Object.keys(data)[key];\n    };\n\n    $scope.totalDefectos = {};\n\n    $scope.fincas = [];\n\n    cambiosFincas = function cambiosFincas() {\n        $scope.calidad.params.palanca = \"\";\n        $scope.calidad.params.idFincaDia = \"\";\n        $scope.loadExternal();\n    };\n\n    $scope.startDetails = function (r, b) {\n        b();\n        if (r) {\n            var options = {};\n            // company \n            $scope.id_company = r.id_company;\n            // data grafica\n            $scope.palancas = r.palanca || [];\n            $scope.fincas = r.fincas || [];\n            // merma : [],\n            // defectos : [],\n            // peso : [],\n            $scope.table.merma = r.merma || [];\n            $scope.table.merma_travel = r.merma_travel || [];\n            $scope.table.merma_details = r.merma_details || [];\n            $scope.table.merma_campos = r.merma_campos || [];\n            $scope.table.defectos = r.defectos || [];\n            $scope.table.defectos_travel = r.defectos_travel || [];\n            $scope.table.defectos_details = r.defectos_details || [];\n            $scope.table.details_campos = r.details_campos || [];\n            $scope.table.peso = r.peso || [];\n            $scope.table.dedos_prom = r.dedos_prom || [];\n            $scope.table.dedos_details = r.dedos_details || [];\n            $scope.table.peso_travel = r.peso_travel || [];\n            $scope.table.peso_details = r.peso_details || [];\n            $scope.table.peso_campos = r.peso_campos || [];\n\n            console.log($scope.table.dedos_details);\n\n            if ($scope.id_company == 2) {}\n            if ($scope.id_company == 6) {\n                $scope.table.merma_widthCell = r.merma_widthCell + \"%\";\n                $scope.table.peso_widthCell = r.peso_widthCell + \"%\";\n            }\n            if ($scope.id_company == 7) {\n                $scope.table.adicional.maxTravel = r.adicional.maxTravel || [];\n                $scope.table.adicional.data = r.adicional.data || [];\n                $scope.table.adicional.details = r.adicional.details || [];\n                $scope.table.adicional.details_campos = r.adicional.details_campos || [];\n            }\n            if ($scope.id_company == 3) {\n                $scope.table.adicional.maxTravel = r.adicional.maxTravel || [];\n                $scope.table.adicional.data = r.adicional.data || [];\n                $scope.table.adicional.details = r.adicional.details || [];\n                $scope.table.adicional.details_campos = r.adicional.details_campos || [];\n            }\n        }\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/arregui/mermadia.js?");

/***/ })

/******/ });