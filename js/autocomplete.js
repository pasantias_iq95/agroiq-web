/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/autocomplete.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/autocomplete.js":
/*!************************************!*\
  !*** ./js_modules/autocomplete.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/**\n * Angucomplete\n * Autocomplete directive for AngularJS\n * By Daryl Rowland\n */\n\nangular.module('angucomplete', []).directive('angucomplete', function ($parse, $http) {\n    return {\n        restrict: 'EA',\n        scope: {\n            \"id\": \"@id\",\n            \"placeholder\": \"@placeholder\",\n            \"selectedObject\": \"=selectedobject\",\n            \"url\": \"@url\",\n            \"titleField\": \"@titlefield\",\n            \"descriptionField\": \"@descriptionfield\",\n            \"imageField\": \"@imagefield\",\n            \"inputClass\": \"@inputclass\",\n            \"userPause\": \"@pause\",\n            \"localData\": \"=localdata\",\n            \"searchFields\": \"@searchfields\",\n            \"minLengthUser\": \"@minlength\"\n        },\n        template: '<div class=\"angucomplete-holder\"><input id=\"{{id}}_value\" ng-model=\"searchStr\" type=\"text\" placeholder=\"{{placeholder}}\" class=\"{{inputClass}}\" ng-keyup=\"keyPressed($event)\"/><div id=\"{{id}}_dropdown\" class=\"angucomplete-dropdown\" ng-if=\"showDropdown\"><div class=\"angucomplete-searching\" ng-show=\"searching\">Searching...</div><div class=\"angucomplete-searching\" ng-show=\"!searching && (!results || results.length == 0)\">No results found</div><div class=\"angucomplete-row\" ng-repeat=\"result in results\" ng-click=\"selectResult(result)\" ng-mouseover=\"hoverRow()\" ng-class=\"{\\'angucomplete-selected-row\\': $index == currentIndex}\"><div ng-if=\"result.image && result.image != \\'\\'\" class=\"angucomplete-image-holder\"><img ng-src=\"{{result.image}}\" class=\"angucomplete-image\"/></div><div>{{result.title}}</div><div ng-if=\"result.description && result.description != \\'\\'\" class=\"angucomplete-description\">{{result.description}}</div></div></div></div>',\n        controller: function controller($scope) {\n            $scope.lastFoundWord = null;\n            $scope.currentIndex = null;\n            $scope.justChanged = false;\n            $scope.searchTimer = null;\n            $scope.searching = false;\n            $scope.pause = 500;\n            $scope.minLength = 3;\n\n            if ($scope.minLengthUser && $scope.minLengthUser != \"\") {\n                $scope.minLength = $scope.minLengthUser;\n            }\n\n            if ($scope.userPause) {\n                $scope.pause = $scope.userPause;\n            }\n\n            $scope.processResults = function (responseData) {\n                if (responseData && responseData.length > 0) {\n                    $scope.results = [];\n\n                    var titleFields = [];\n                    if ($scope.titleField && $scope.titleField != \"\") {\n                        titleFields = $scope.titleField.split(\",\");\n                    }\n\n                    for (var i = 0; i < responseData.length; i++) {\n                        // Get title variables\n                        var titleCode = \"\";\n\n                        for (var t = 0; t < titleFields.length; t++) {\n                            if (t > 0) {\n                                titleCode = titleCode + \" + ' ' + \";\n                            }\n                            titleCode = titleCode + \"responseData[i].\" + titleFields[t];\n                        }\n\n                        // Figure out description\n                        var description = \"\";\n\n                        if ($scope.descriptionField && $scope.descriptionField != \"\") {\n                            eval(\"description = responseData[i].\" + $scope.descriptionField);\n                        }\n\n                        // Figure out image\n                        var image = \"\";\n\n                        if ($scope.imageField && $scope.imageField != \"\") {\n                            eval(\"image = responseData[i].\" + $scope.imageField);\n                        }\n\n                        var resultRow = {\n                            title: eval(titleCode),\n                            description: description,\n                            image: image,\n                            originalObject: responseData[i]\n                        };\n\n                        $scope.results[$scope.results.length] = resultRow;\n                    }\n                } else {\n                    $scope.results = [];\n                }\n            };\n\n            $scope.searchTimerComplete = function (str) {\n                // Begin the search\n\n                if (str.length >= $scope.minLength) {\n                    if ($scope.localData) {\n                        var searchFields = $scope.searchFields.split(\",\");\n\n                        var matches = [];\n\n                        for (var i = 0; i < $scope.localData.length; i++) {\n                            var match = false;\n\n                            for (var s = 0; s < searchFields.length; s++) {\n                                var evalStr = 'match = match || ($scope.localData[i].' + searchFields[s] + '.toLowerCase().indexOf(\"' + str.toLowerCase() + '\") >= 0)';\n                                eval(evalStr);\n                            }\n\n                            if (match) {\n                                matches[matches.length] = $scope.localData[i];\n                            }\n                        }\n\n                        $scope.searching = false;\n                        $scope.processResults(matches);\n                        $scope.$apply();\n                    } else {\n                        $http.get($scope.url + str, {}).success(function (responseData, status, headers, config) {\n                            $scope.searching = false;\n                            $scope.processResults(responseData);\n                        }).error(function (data, status, headers, config) {\n                            console.log(\"error\");\n                        });\n                    }\n                }\n            };\n\n            $scope.hoverRow = function (index) {\n                $scope.currentIndex = index;\n            };\n\n            $scope.keyPressed = function (event) {\n                if (!(event.which == 38 || event.which == 40 || event.which == 13)) {\n                    if (!$scope.searchStr || $scope.searchStr == \"\") {\n                        $scope.showDropdown = false;\n                    } else {\n\n                        if ($scope.searchStr.length >= $scope.minLength) {\n                            $scope.showDropdown = true;\n                            $scope.currentIndex = -1;\n                            $scope.results = [];\n\n                            if ($scope.searchTimer) {\n                                clearTimeout($scope.searchTimer);\n                            }\n\n                            $scope.searching = true;\n\n                            $scope.searchTimer = setTimeout(function () {\n                                $scope.searchTimerComplete($scope.searchStr);\n                            }, $scope.pause);\n                        }\n                    }\n                } else {\n                    event.preventDefault();\n                }\n            };\n\n            $scope.selectResult = function (result) {\n                $scope.searchStr = result.title;\n                $scope.selectedObject = result;\n                $scope.showDropdown = false;\n                $scope.results = [];\n                //$scope.$apply();\n            };\n        },\n\n        link: function link($scope, elem, attrs, ctrl) {\n\n            elem.bind(\"keyup\", function (event) {\n                if (event.which === 40) {\n                    if ($scope.currentIndex + 1 < $scope.results.length) {\n                        $scope.currentIndex++;\n                        $scope.$apply();\n                        event.preventDefault;\n                        event.stopPropagation();\n                    }\n\n                    $scope.$apply();\n                } else if (event.which == 38) {\n                    if ($scope.currentIndex >= 1) {\n                        $scope.currentIndex--;\n                        $scope.$apply();\n                        event.preventDefault;\n                        event.stopPropagation();\n                    }\n                } else if (event.which == 13) {\n                    if ($scope.currentIndex >= 0 && $scope.currentIndex < $scope.results.length) {\n                        $scope.selectResult($scope.results[$scope.currentIndex]);\n                        $scope.$apply();\n                        event.preventDefault;\n                        event.stopPropagation();\n                    } else {\n                        $scope.results = [];\n                        $scope.$apply();\n                        event.preventDefault;\n                        event.stopPropagation();\n                    }\n                } else if (event.which == 27) {\n                    $scope.results = [];\n                    $scope.showDropdown = false;\n                    $scope.$apply();\n                } else if (event.which == 8) {\n                    $scope.selectedObject = null;\n                    $scope.$apply();\n                }\n            });\n        }\n    };\n});\n\n//# sourceURL=webpack:///./js_modules/autocomplete.js?");

/***/ })

/******/ });