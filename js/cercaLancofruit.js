/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/cercaLancofruit.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/cercaLancofruit.js":
/*!***************************************!*\
  !*** ./js_modules/cercaLancofruit.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('server', ['$http', function ($http) {\n    this.index = function (params) {\n        var url = 'phrapi/lancofruit/polygon/index';\n        return $http.post(url, params || {});\n    };\n    this.save = function (params) {\n        var url = 'phrapi/lancofruit/polygon/save';\n        return $http.post(url, params || {});\n    };\n}]);\n\napp.controller('lancofruit', ['$scope', 'server', function ($scope, server) {\n\n    $scope.filters = {\n        search: \"\"\n    };\n    $scope.nombre = \"\";\n\n    var options = {\n        zoom: 15,\n        center: new google.maps.LatLng(-2.188861, -79.898896),\n        mapTypeId: google.maps.MapTypeId.ROADMAP\n    };\n\n    var map = new google.maps.Map(document.getElementById('map-polygon'), options);\n\n    var polygonArray = [];\n    var myPolygon = null;\n    var ArrayDebugMode = [];\n\n    $scope.polygonArray = [];\n\n    var clearBuffering = function clearBuffering() {\n        myPolygon.setMap(null);\n        myPolygon = null;\n        polygonArray = [];\n        $scope.polygonArray = polygonArray;\n    };\n\n    var draw = function draw() {\n        if (polygonArray.length > 0) {\n            $scope.polygonArray = angular.copy(polygonArray);\n            if (myPolygon == null) {\n                myPolygon = new google.maps.Polygon({\n                    paths: polygonArray,\n                    draggable: true, // turn off if it gets annoying\n                    editable: true,\n                    strokeColor: '#FF0000',\n                    strokeOpacity: 0.8,\n                    strokeWeight: 2,\n                    fillColor: '#FF0000',\n                    fillOpacity: 0.35\n                });\n                myPolygon.setMap(map);\n            } else {\n                myPolygon.setPaths(polygonArray);\n            }\n        } else {\n            clearBuffering();\n        }\n    };\n\n    var getPoint = function getPoint(event) {\n        var latitude = event.latLng.lat();\n        var longitude = event.latLng.lng();\n        var point = event.latLng;\n        polygonArray.push(point);\n        draw();\n    };\n\n    var deletePoint = function deletePoint(event) {\n        event.preventDefault();\n        polygonArray.pop();\n        draw();\n    };\n\n    var newPolygon = function newPolygon() {\n        var newPoly = true;\n        if (myPolygon != null) {\n            var result = confirm(\"Desea crear un nuevo polygono? , se perdera el progreso actual\");\n            if (!result) {\n                newPoly = false;\n            } else {\n                clearBuffering();\n                $scope.nombre = \"\";\n                $scope.id = 0;\n            }\n        }\n\n        if (newPoly) {\n            myPolygon = null;\n            polygonArray = [];\n            map.addListener('click', getPoint);\n        }\n    };\n\n    var getPolygonCoords = function getPolygonCoords() {\n        var length = myPolygon.getPath().getLength();\n        var points = [];\n        var coords = '';\n        for (var i = 0; i < length; i++) {\n            coords = myPolygon.getPath().getAt(i).lat() + ',' + myPolygon.getPath().getAt(i).lng();\n            points.push(coords);\n        }\n\n        return points;\n    };\n\n    var savePolygon = function savePolygon() {\n        if (polygonArray.length > 2 && $scope.nombre != \"\") {\n            google.maps.event.clearListeners(map, 'click');\n            var points = getPolygonCoords();\n            var _options = {\n                id: $scope.id,\n                nombre: $scope.nombre,\n                paths: points\n            };\n            server.save(_options).then(function (r) {\n                var data = r.data;\n                if (data.code == 200) {\n                    clearBuffering();\n                    alert(\"Guardada con exito\", \"Cercas\", \"success\");\n                    $scope.init();\n                }\n            });\n            // showPolygon()\n        } else {\n            alert(\"Para generar una cerca debe contener al menos 3 puntos\", \"Cercas\");\n        }\n    };\n\n    var clearMap = function clearMap() {\n        clearBuffering();\n    };\n\n    var showPolygon = function showPolygon(paths) {\n        if (paths.length < 2) {\n            return false;\n        }\n        polygonArray = paths.map(function (e) {\n            return new google.maps.LatLng(e[0], e[1]);\n        });\n        draw();\n    };\n\n    $scope.polygons = [];\n    $scope.id = 0;\n    $scope.showPoly = function (polygon) {\n        if (myPolygon != null) {\n            myPolygon.setMap(null);\n            myPolygon = null;\n        }\n        polygonArray = [];\n        var coords = polygon.polygon.split(\"|\").map(function (e) {\n            return e.split(\",\");\n        });\n        $scope.id = polygon.id;\n        $scope.nombre = polygon.name;\n        showPolygon(coords);\n    };\n    $scope.init = function () {\n        server.index().then(function (r) {\n            var data = r.data;\n            if (data.code == 200) {\n                $scope.nombre = \"\";\n                $scope.id = 0;\n                $scope.polygons = data.response;\n            }\n        });\n    };\n\n    document.getElementById('last-point').addEventListener('click', deletePoint);\n    document.getElementById('new-draw').addEventListener('click', newPolygon);\n    document.getElementById('save-draw').addEventListener('click', savePolygon);\n}]);\n\n//# sourceURL=webpack:///./js_modules/cercaLancofruit.js?");

/***/ })

/******/ });