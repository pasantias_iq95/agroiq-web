/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/reiset/climaDiario.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/reiset/climaDiario.js":
/*!******************************************!*\
  !*** ./js_modules/reiset/climaDiario.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', function ($http) {\n    var service = {};\n\n    service.tags = function (callback, params) {\n        $http.post('phrapi/reiset/climaDiario/tags', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    service.datatable = function (callback, params) {\n        $http.post('phrapi/reiset/climaDiario/datatable', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    service.horasluz = function (callback, params) {\n        $http.post('phrapi/reiset/climaDiario/horasluz', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    service.graficasViento = function (callback, params) {\n        $http.post('phrapi/reiset/climaDiario/viento', params || {}).then(function (r) {\n            return callback(r.data);\n        });\n    };\n    service.last = function (callback, params) {\n        $http.post('phrapi/reiset/climaDiario/last', params || {}).then(function (r) {\n            return callback(r.data);\n        });\n    };\n\n    return service;\n});\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {\n        hora_inicio: moment().format('HH:mm'),\n        hora_fin: moment().format('HH:mm'),\n        luz: 400,\n        fecha_inicial: moment().format('YYYY-MM-DD'),\n        fecha_final: moment().format('YYYY-MM-DD')\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment(),\n        endDate: moment()\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.filters.fecha_final;\n\n            //$scope.difDias = moment($scope.filters.fecha_inicial).isBefore($scope.filters.fecha_final);\n        }\n        $scope.init();\n    };\n\n    $scope.loadTags = function () {\n        load.block('indicadores');\n        $request.tags(function (r) {\n            $scope.tags = r.tags;\n            load.unblock('indicadores');\n        }, $scope.filters);\n    };\n    $scope.loadDataTable = function () {\n        load.block('datatable');\n        $request.datatable(function (r) {\n            $scope.datatable = r.data;\n            load.unblock('datatable');\n        }, $scope.filters);\n    };\n    $scope.loadGraficasViento = function () {\n        $request.graficasViento(function (r) {\n            if (r) {\n                printGraficaDireccionViento(r.direccion_viento);\n                printGraficaVelocidadViento(r.velocidad_viento);\n            }\n        }, $scope.filters);\n    };\n    $scope.loadHorasLuz = function () {\n        load.block('grafica_horas_luz');\n        $request.horasluz(function (r) {\n            printGraficaHorasLuz(r.data);\n            load.unblock('grafica_horas_luz');\n        }, $scope.filters);\n    };\n\n    printGraficaHorasLuz = function printGraficaHorasLuz(data) {\n        if (data.data.length == 0) {\n            var props = {\n                series: [0],\n                legend: ['NO HAY DATOS'],\n                umbral: null,\n                id: \"grafica_horas_luz\",\n                type: 'line',\n                zoom: false,\n                legendBottom: true,\n                min: data.min\n            };\n            ReactDOM.render(React.createElement(Historica, props), document.getElementById('grafica_horas_luz'));\n        } else {\n            var props = {\n                series: data.data,\n                legend: data.legend,\n                umbral: null,\n                id: \"grafica_horas_luz\",\n                type: 'line',\n                zoom: false,\n                legendBottom: true,\n                min: data.min\n            };\n            ReactDOM.render(React.createElement(Historica, props), document.getElementById('grafica_horas_luz'));\n        }\n    };\n    printGraficaDireccionViento = function printGraficaDireccionViento(data) {\n        var props = {\n            //titulo : 'Viento',\n            //subtitulo : 'Dirección',\n            data: [{\n                label: 'N',\n                value: data['N'] || 0\n            }, {\n                label: 'NE',\n                value: data['NE'] || 0\n            }, {\n                label: 'E',\n                value: data['E'] || 0\n            }, {\n                label: 'SE',\n                value: data['SE'] || 0\n            }, {\n                label: 'S',\n                value: data['S'] || 0\n            }, {\n                label: 'SW',\n                value: data['SW'] || 0\n            }, {\n                label: 'W',\n                value: data['W'] || 0\n            }, {\n                label: 'NW',\n                value: data['NW'] || 0\n            }],\n            id: \"grafica-direccion-viento\"\n        };\n        ReactDOM.render(React.createElement(Brujula, props), document.getElementById('grafica-direccion-viento'));\n    };\n    printGraficaVelocidadViento = function printGraficaVelocidadViento(kmh) {\n        var props = {\n            titulo: 'Viento',\n            subtitulo: 'Velocidad',\n            unidad: 'km/h',\n            value: kmh,\n            id: \"grafica-velocidad-viento\"\n        };\n        ReactDOM.render(React.createElement(Medidor, props), document.getElementById('grafica-velocidad-viento'));\n    };\n\n    $scope.init = function () {\n        $scope.loadTags();\n        $scope.loadDataTable();\n        $scope.loadHorasLuz();\n        $scope.loadGraficasViento();\n    };\n    $scope.last = function () {\n        $request.last(function (r) {\n            $scope.filters.fecha_inicial = r.fecha;\n            $scope.filters.fecha_final = r.fecha;\n            $scope.filters.hora_inicio = r.hora;\n            $scope.filters.hora_fin = r.hora;\n            $(\"date-picker\").html(r.fecha + ' - ' + r.fecha);\n\n            initPickers();\n            $scope.init();\n        });\n    };\n    $scope.last();\n\n    initPickers = function initPickers() {\n        $('#timepicker').timepicker({\n            minuteStep: 1,\n            appendWidgetTo: 'body',\n            showMeridian: false,\n            defaultTime: false\n        });\n        $('#timepicker').timepicker('setTime', $scope.filters.hora_inicio);\n        $('#timepicker').timepicker().on('changeTime.timepicker', function (e) {\n            $scope.isChangeHour = true;\n\n            var inicio = e.time.value.split(':');\n            var fin = $scope.filters.hora_fin.split(':');\n\n            var inicio_time = moment().hour(inicio[0]).minute(inicio[1]);\n            var fin_time = moment().hour(fin[0]).minute(inicio[1]);\n            if (inicio_time.isSameOrBefore(fin_time)) {\n                $scope.filters.hora = e.time.value;\n            } else {\n                alert('El tiempo debe ser menor a ' + $scope.filters.hora_fin);\n            }\n        });\n\n        $('#timepicker2').timepicker({\n            minuteStep: 1,\n            appendWidgetTo: 'body',\n            showMeridian: false,\n            defaultTime: false\n        });\n        $('#timepicker2').timepicker('setTime', $scope.filters.hora_fin);\n        $('#timepicker2').timepicker().on('changeTime.timepicker', function (e) {\n            $scope.isChangeHour = true;\n\n            var inicio = $scope.filters.hora_inicio.split(':');\n            var fin = e.time.value.split(':');\n\n            var inicio_time = moment().hour(inicio[0]).minute(inicio[1]);\n            var fin_time = moment().hour(fin[0]).minute(inicio[1]);\n            if (inicio_time.isSameOrAfter(fin_time)) {\n                $scope.filters.hora = e.time.value;\n            } else {\n                alert('El tiempo debe ser mayor a ' + $scope.filters.hora_inicio);\n            }\n        });\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/reiset/climaDiario.js?");

/***/ })

/******/ });