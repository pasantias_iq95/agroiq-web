/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/configLancofruit.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/configLancofruit.js":
/*!****************************************!*\
  !*** ./js_modules/configLancofruit.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _window = window,\n    app = _window.app;\n\n\napp.service('request', ['$http', function ($http) {\n    this.list = function (callback, params) {\n        $http.post('phrapi/lancofruit/categoriasConfig/index', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.saveCategoria = function (callback, params) {\n        $http.post('phrapi/lancofruit/categoriasConfig/save', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.borrarCategoria = function (callback, params) {\n        $http.post('phrapi/lancofruit/categoriasConfig/borrar', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.toggleEstatusCategoria = function (callback, params) {\n        $http.post('phrapi/lancofruit/categoriasConfig/toggleStatus', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.saveSubcategoria = function (callback, params) {\n        $http.post('phrapi/lancofruit/subcategoriasConfig/save', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.borrarSubcategoria = function (callback, params) {\n        $http.post('phrapi/lancofruit/subcategoriasConfig/borrar', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.toggleEstatusSubcategoria = function (callback, params) {\n        $http.post('phrapi/lancofruit/subcategoriasConfig/toggleStatus', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.controller('lancofruit', ['$scope', 'request', function ($scope, $request) {\n\n    /* VARIABLES  */\n\n    var modalCategoria = $(\"#modal_categoria\"),\n        modalSubcategoria = $(\"#modal_subcategoria\");\n    $scope.filters = {};\n    $scope.colorStatus = {\n        'Activo': 'bg-green-haze bg-font-green-haze',\n        'Inactivo': 'bg-red bg-font-red'\n    };\n    $scope.categorias = [];\n    $scope.subcategorias = [];\n    $scope.subcategorias_disponibles = [];\n\n    $scope.newCategoria = {\n        id: null,\n        nombre: '',\n        subcategorias: [],\n        save: function save() {\n            return new Promise(function (resolve, reject) {\n                var subcategorias = $('#select_multiple').val() || [];\n\n                $request.saveCategoria(function (r) {\n                    resolve(r);\n                }, {\n                    id: $scope.newCategoria.id,\n                    nombre: $scope.newCategoria.nombre,\n                    subcategorias: subcategorias\n                });\n            });\n        }\n    };\n\n    $scope.newSubcategoria = {\n        id: null,\n        nombre: '',\n        id_categoria: [],\n        save: function save() {\n            return new Promise(function (resolve, reject) {\n                $request.saveSubcategoria(function (r) {\n                    resolve(r);\n                }, {\n                    id: $scope.newSubcategoria.id,\n                    nombre: $scope.newSubcategoria.nombre,\n                    id_categoria: $scope.newSubcategoria.id_categoria\n                });\n            });\n        }\n    };\n\n    $scope.init = function () {\n        initMultiSelectCategorias();\n\n        $request.list(function (r) {\n            $scope.categorias = r.categorias;\n            $scope.subcategorias = r.subcategorias;\n        }, $scope.filters);\n    };\n\n    /* CATEGORIAS */\n\n    $scope.nuevaCategoria = function () {\n        modalCategoria.modal('show');\n        $scope.newCategoria.id = null;\n        $scope.newCategoria.nombre = \"\";\n        $scope.newCategoria.subcategorias = [];\n        $scope.subcategorias_disponibles = $scope.subcategorias.filter(function (s) {\n            return s.id_categoria === null || s.id_categoria === 0;\n        });\n\n        setTimeout(function () {\n            return $('#select_multiple').multiSelect('refresh');\n        }, 250);\n    };\n\n    $scope.modificarCategoria = function (categoria) {\n        modalCategoria.modal('show');\n        $scope.newCategoria.id = angular.copy(categoria.id);\n        $scope.newCategoria.nombre = angular.copy(categoria.nombre);\n        $scope.newCategoria.subcategorias = $scope.subcategorias.filter(function (s) {\n            return s.id_categoria == categoria.id;\n        });\n        $scope.subcategorias_disponibles = $scope.subcategorias.filter(function (s) {\n            return s.id_categoria === null || s.id_categoria === 0;\n        });\n\n        setTimeout(function () {\n            return $('#select_multiple').multiSelect('refresh');\n        }, 250);\n    };\n\n    $scope.toggleEstatusCategoria = function (categoria) {\n        $request.toggleEstatusCategoria(function (r) {\n            if (r.status === 200) {\n                //alert(\"Se guardo correctamente\", \"Configuración\", \"success\")\n                categoria.status = categoria.status === 'Activo' ? 'Inactivo' : 'Activo';\n            } else {\n                alert(\"Ocurrio algo inesperado\");\n            }\n        }, {\n            id: categoria.id\n        });\n    };\n\n    $scope.borrarCategoria = function (categoria, index) {\n        $request.borrarCategoria(function (r) {\n            if (r.status === 200) {\n                alert(\"Se guardo correctamente\", \"Configuración\", \"success\");\n                $scope.categorias.splice(index, 1);\n            } else {\n                alert(\"Ocurrio algo inesperado\");\n            }\n        }, {\n            id: categoria.id\n        });\n    };\n\n    $scope.guardarCategoria = function () {\n        $scope.newCategoria.save().then(function (r) {\n            if (r.status === 200) {\n                if ($scope.newCategoria.id === null) {\n                    $scope.categorias.push({\n                        id: r.id,\n                        nombre: r.nombre,\n                        status: 'Activo'\n                    });\n                } else {\n                    modificarCategoria($scope.newCategoria.id, r.nombre, r.subcategorias);\n                }\n\n                $scope.$apply();\n                cerrarModalCategoria();\n                alert(\"Se guardo correctamente\", \"Configuración\", \"success\");\n            } else if (r.message) {\n                alert(r.message);\n            } else {\n                alert(\"Ocurrio algo inesperado\");\n            }\n        });\n    };\n\n    var modificarCategoria = function modificarCategoria(id, nombre, subcategorias) {\n        $scope.categorias.map(function (c) {\n            if (c.id === $scope.newCategoria.id) {\n                c.nombre = nombre;\n            }\n        });\n\n        $scope.subcategorias.map(function (s) {\n            if (subcategorias.indexOf(s.id) > -1) {\n                s.id_categoria = id;\n                s.categoria = nombre;\n            } else if (s.id_categoria === id) {\n                s.id_categoria = null;\n                s.categoria = '';\n            }\n        });\n    };\n\n    var cerrarModalCategoria = function cerrarModalCategoria() {\n        modalCategoria.modal('hide');\n        $scope.newCategoria.id = null;\n        $scope.newCategoria.nombre = \"\";\n        $scope.newCategoria.subcategorias = [];\n    };\n\n    var initMultiSelectCategorias = function initMultiSelectCategorias() {\n        $('#select_multiple').multiSelect({\n            selectableHeader: \"<div class='custom-header'> Subcategorías Disponibles </div>\",\n            selectionHeader: \"<div class='custom-header'> Subcategorías Asignadas </div>\"\n        });\n    };\n\n    /* SUBCATEGORIAS */\n\n    $scope.nuevaSubcategoria = function () {\n        modalSubcategoria.modal('show');\n        $scope.newSubcategoria.id = null;\n        $scope.newSubcategoria.nombre = \"\";\n        $scope.newSubcategoria.id_categoria = null;\n    };\n\n    $scope.modificarSubcategoria = function (categoria) {\n        modalSubcategoria.modal('show');\n        $scope.newSubcategoria.id = angular.copy(categoria.id);\n        $scope.newSubcategoria.nombre = angular.copy(categoria.nombre);\n        $scope.newSubcategoria.id_categoria = angular.copy(categoria.id_categoria);\n    };\n\n    $scope.guardarSubcategoria = function () {\n        $scope.newSubcategoria.save().then(function (r) {\n            if (r.status === 200) {\n                if ($scope.newSubcategoria.id === null) {\n                    $scope.subcategorias.push({\n                        id: r.id,\n                        nombre: r.nombre,\n                        id_categoria: r.id_categoria,\n                        categoria: r.categoria,\n                        status: 'Activo'\n                    });\n                } else {\n                    modificarSubcategoria($scope.newSubcategoria.id, r.nombre, r.id_categoria, r.categoria);\n                }\n\n                $scope.$apply();\n                cerrarModalSubcategoria();\n                alert(\"Se guardo correctamente\", \"Configuración\", \"success\");\n            } else if (r.message) {\n                alert(r.message);\n            } else {\n                alert(\"Ocurrio algo inesperado\");\n            }\n        });\n    };\n\n    $scope.toggleEstatusSubcategoria = function (categoria) {\n        $request.toggleEstatusSubcategoria(function (r) {\n            if (r.status === 200) {\n                //alert(\"Se guardo correctamente\", \"Configuración\", \"success\")\n                categoria.status = categoria.status === 'Activo' ? 'Inactivo' : 'Activo';\n            } else {\n                alert(\"Ocurrio algo inesperado\");\n            }\n        }, {\n            id: categoria.id\n        });\n    };\n\n    $scope.borrarSubcategoria = function (categoria, index) {\n        $request.borrarSubcategoria(function (r) {\n            if (r.status === 200) {\n                alert(\"Se guardo correctamente\", \"Configuración\", \"success\");\n                $scope.subcategorias.splice(index, 1);\n            } else {\n                alert(\"Ocurrio algo inesperado\");\n            }\n        }, {\n            id: categoria.id\n        });\n    };\n\n    var modificarSubcategoria = function modificarSubcategoria(id, nombre, id_categoria, categoria) {\n        $scope.subcategorias.map(function (s) {\n            if (s.id === id) {\n                s.nombre = nombre;\n                s.id_categoria = id_categoria;\n                s.categoria = categoria;\n            }\n        });\n    };\n\n    var cerrarModalSubcategoria = function cerrarModalSubcategoria() {\n        modalSubcategoria.modal('hide');\n        $scope.newSubcategoria.id = null;\n        $scope.newSubcategoria.nombre = \"\";\n        $scope.newSubcategoria.id_categoria = null;\n    };\n\n    /* INIT */\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/configLancofruit.js?");

/***/ })

/******/ });