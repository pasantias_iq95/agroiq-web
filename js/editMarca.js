/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/editMarca.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/editMarca.js":
/*!*********************************!*\
  !*** ./js_modules/editMarca.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.controller('control', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', function ($scope, $http, $interval, client, $controller, $timeout, $window) {\n\n\t$scope.original = {\n\t\tid: \"<?= $_GET['id'] ?>\",\n\t\tcliente: \"\",\n\t\texportador: \"\",\n\t\tmarca: \"\",\n\t\ttipo_caja: \"\"\n\t};\n\t$scope.data = {\n\t\tid: \"<?= $_GET['id'] ?>\",\n\t\tcliente: \"\",\n\t\texportador: \"\",\n\t\tmaraca: \"\",\n\t\ttipo_caja: \"\"\n\t};\n\t$scope.exportadores = [];\n\t$scope.clientes = [];\n\n\t$scope.init = function () {\n\t\tclient.post(\"phrapi/configuracion/get/marca\", function (r, b) {\n\t\t\tb();\n\t\t\tif (r) {\n\t\t\t\tconsole.log(r);\n\t\t\t\tif (r.hasOwnProperty(\"exportadores\")) {\n\t\t\t\t\t$scope.exportadores = r.exportadores;\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"clientes\")) {\n\t\t\t\t\t$scope.clientes = r.clientes;\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"marca\")) {\n\t\t\t\t\t$scope.original.exportador = r.marca.id_exportador;\n\t\t\t\t\t$scope.original.tipo_caja = r.marca.tipo_caja;\n\t\t\t\t\t$scope.original.marca = r.marca.nombre;\n\n\t\t\t\t\t$scope.data.exportador = r.marca.id_exportador;\n\t\t\t\t\t$scope.data.tipo_caja = r.marca.tipo_caja;\n\t\t\t\t\t$scope.data.marca = r.marca.nombre;\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"id_cliente\")) {\n\t\t\t\t\t$scope.data.cliente = r.id_cliente;\n\t\t\t\t}\n\t\t\t}\n\t\t}, { id: $scope.original.id });\n\n\t\t$(\"#changeCliente\").change(function () {\n\t\t\t$scope.changeCliente();\n\t\t});\n\t};\n\n\t$scope.changeCliente = function () {\n\t\tclient.post(\"phrapi/configuracion/get/marca\", function (r, b) {\n\t\t\tb();\n\t\t\tconsole.log(r);\n\t\t\tif (r) {\n\t\t\t\tif (r.hasOwnProperty(\"exportadores\")) {\n\t\t\t\t\t$scope.exportadores = r.exportadores;\n\t\t\t\t}\n\t\t\t\tif (r.hasOwnProperty(\"clientes\")) {\n\t\t\t\t\t$scope.clientes = r.clientes;\n\t\t\t\t}\n\t\t\t}\n\t\t}, $scope.data);\n\t};\n\n\t$scope.saveDatos = function () {\n\t\tif ($scope.data.cliente != \"\" && $scope.data.exportador != \"\" && $scope.data.marca != \"\" && $scope.data.tipo_caja != \"\") {\n\t\t\tclient.post(\"phrapi/configuracion/save/marca\", function (r, b) {\n\t\t\t\tb();\n\t\t\t\tif (r) {\n\t\t\t\t\talert(\"Formulario Guardado\", \"Marcas\", \"success\");\n\t\t\t\t\twindow.location = \"configMarcas\";\n\t\t\t\t}\n\t\t\t}, $scope.data);\n\t\t} else {\n\t\t\talert(\"Formulario incompleto\");\n\t\t}\n\t};\n\n\t$scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/editMarca.js?");

/***/ })

/******/ });