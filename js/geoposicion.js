/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/geoposicion.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/geoposicion.js":
/*!***********************************!*\
  !*** ./js_modules/geoposicion.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.controller('mapas', ['$scope', 'client', function ($scope, client) {\n\n    $scope.map = {};\n    $scope.type = \"AUDITORIA\";\n    $scope.id_company = 0;\n    $scope.idFinca = 1;\n    $scope.auditorias = [];\n    $scope.fincas = [];\n    $scope.bounds = new google.maps.LatLngBounds();\n\n    $scope.init = function () {\n        $scope.map = new GMaps({\n            div: '#mapa_agroaudit',\n            lat: -1.725187,\n            lng: -78.854211,\n            zoom: 7,\n            streetViewControl: false\n        });\n\n        var data = {\n            opt: \"INDEX\",\n            type: $scope.type\n        };\n\n        client.post(\"phrapi/mapas/index\", $scope.printSem, data);\n    };\n\n    var changeFincas = function changeFincas() {\n        var idFinca = angular.element(\"#fincas\").val();\n        var Periodo = parseInt(angular.element(\"#auditorias\").val());\n        $scope.idFinca = idFinca;\n        if (!isNaN(Periodo) && Periodo > 0) {\n            $scope.getMarker(Periodo);\n        }\n    };\n\n    $scope.changeType = function (type) {\n        $scope.type = type || \"Mes\";\n        var data = {\n            opt: \"INDEX\",\n            type: $scope.type\n        };\n        client.post(\"phrapi/mapas/index\", $scope.printSem, data);\n    };\n\n    $scope.printSem = function (r, b) {\n        b();\n        if (r) {\n            $scope.auditorias = r.auditorias;\n            $scope.id_company = r.id_company;\n            $scope.fincas = r.fincas;\n            $(\"#auditorias\").change();\n        }\n    };\n\n    $scope.addMarker = function (data, label) {\n        if (data) {\n            for (var i in data) {\n                $scope.map.addMarker({\n                    lat: data[i].mlat,\n                    lng: data[i].mlng,\n                    title: data[i].nombre_muestra,\n                    infoWindow: {\n                        content: '<span>' + data[i].nombre_muestra + '</span>'\n                    }\n                });\n\n                $scope.bounds.extend(new google.maps.LatLng(data[i].mlat, data[i].mlng));\n                $scope.map.fitBounds($scope.bounds);\n            }\n        }\n    };\n\n    $scope.printMarkers = function (r, b) {\n        if (r) {\n            b();\n            for (var i in r) {\n                $scope.addMarker(r[i], i);\n            }\n        }\n    };\n\n    $scope.getMarker = function (value) {\n        var periodo = parseFloat(value) || 0;\n        if (value > 0) {\n            var data = {\n                opt: \"MARKERS\",\n                auditorias: periodo,\n                type: $scope.type,\n                idFinca: $scope.idFinca\n            };\n            client.post(\"phrapi/mapas/markers\", $scope.printMarkers, data);\n        }\n    };\n\n    $(\"#auditorias\").on(\"change\", function () {\n        $scope.map.removeMarkers();\n        if (this.value > -1) {\n            $scope.getMarker(this.value);\n        }\n    });\n}]);\n\n//# sourceURL=webpack:///./js_modules/geoposicion.js?");

/***/ })

/******/ });