/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/lancofruit.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/lancofruit.js":
/*!**********************************!*\
  !*** ./js_modules/lancofruit.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.controller('lancofruit', ['$scope', '$http', 'client', function ($scope, $http, client) {\n    $scope.datos = {\n        id: \"<?=$_GET['id']?>\",\n        codigo: \"<?=$_GET['id']?>\",\n        referencia: \"\",\n        ruc: \"\",\n        nombre_propietario: \"\",\n        tipo_local: \"\",\n        nombre_local: \"\",\n        status: \"\",\n        telefono: \"\",\n        correo: \"\",\n        ruta: \"\",\n        sector: \"\",\n        direccion: \"\",\n        tendero: \"\"\n    };\n\n    /***\n     * Send a broadcast to the directive in order to change itself\n     * if an id parameter is given only this ancucomplete is changed\n     * @param id\n     */\n    $scope.changeInput = function (id, pos) {\n        if (id) {\n            $scope.$broadcast('angucomplete-alt:changeInput', id, $scope.tipos_locales[pos]);\n        }\n    };\n\n    $scope.changeRutaSelected = function () {\n        $(\"#rutas\").val($scope.datos.id_ruta);\n        var index = $(\"#rutas :selected\").attr(\"index\");\n        $scope.modal.selected_ruta = $scope.rutas[index];\n        setTimeout(function () {\n            $(\"#vendedores\").val($scope.datos.id_vendedor);\n        }, 250);\n    };\n\n    $scope.olddatos = angular.copy($scope.datos);\n    $scope.rutas = [];\n    $scope.tipos_locales = [{\n        local: \"Gimnasio\"\n    }, {\n        local: \"Minorista\"\n    }, {\n        local: \"Restaurante\"\n    }];\n\n    $scope.init = function () {\n        var data = {\n            id: $scope.datos.id\n        };\n        client.post(\"phrapi/lancofruit/edit\", $scope.print, data);\n        $scope.modal.init();\n    };\n\n    $scope.print = function (r, b) {\n        b();\n\n        if (r.hasOwnProperty(\"data\")) {\n            $scope.datos = r.data;\n\n            $(\"#rutas\").val(r.data.id_ruta);\n            if (r.id_vendedor > 0) {\n                $(\"#vendedores\").val(r.data.id_vendedor);\n            }\n\n            /*var pos = 0\n            if (r.tipo_local == \"Gimnasio\")\n                pos = 0\n            if (r.tipo_local == \"Minorista\")\n                pos = 1\n            if (r.tipo_local == \"Restaurante\")\n                pos = 2\n             $scope.changeInput(\"tipo_local\" , pos)*/\n        }\n    };\n\n    $scope.save = function () {\n        var data = $scope.datos;\n        client.post(\"phrapi/lancofruit/save\", $scope.success, data);\n    };\n\n    $scope.success = function (r, b) {\n        b();\n        if (r.hasOwnProperty(\"data\")) {\n            alert(\"Registro registrado/modificado con el ID \" + $scope.datos.id, \"Lancofruit\", \"success\", function () {\n                document.location.href = \"/revisionLancofruit\";\n            });\n        }\n    };\n\n    $scope.cancel = function () {\n        document.location.href = \"/revisionLancofruit\";\n    };\n\n    $scope.modal = {\n        vendedores: [],\n        init: function init() {\n            client.post(\"phrapi/lacofruit/filters\", $scope.modal.print, {});\n        },\n        print: function print(r, b) {\n            b();\n            if (r) {\n                $scope.categorias = r.categorias;\n                $scope.subcategorias = r.subcategorias;\n                $scope.modal.vendedor.vendedor = \"\";\n                if (r.hasOwnProperty(\"rutas\")) {\n                    $scope.rutas = r.rutas;\n\n                    setTimeout(function () {\n                        $scope.$apply(function () {\n                            $scope.changeRutaSelected();\n                        });\n                    }, 250);\n                }\n            }\n        },\n        success: function success(r, b) {\n            b();\n            if (r) {\n                if (r.status == 200) {\n                    $(\"#ruta-modal\").modal(\"hide\");\n                    $scope.init();\n                    alert(\"Se asigno correctamente\", \"Asignar Vendedor\", \"success\");\n                } else if (r.status == 400) {\n                    alert(r.message, \"Asignar Vendedor\");\n                }\n            }\n        },\n        vendedor: {\n            vendedor: \"\",\n            save: function save() {\n                if ($(\"#vendedores\").text().trim() != \"\") {\n                    var data = {\n                        id_ruta: $scope.datos.id_ruta,\n                        id_vendedor: $scope.datos.id_vendedor,\n                        id: \"<?= $_GET['id'] ?>\"\n                    };\n                    $('#vendedor-modal').modal('hide');\n                    client.post(\"phrapi/lancofruit/vendedor\", $scope.modal.success, data);\n                } else {\n                    alert(\"El vendedor no puede estar vacio\");\n                }\n            }\n        }\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/lancofruit.js?");

/***/ })

/******/ });