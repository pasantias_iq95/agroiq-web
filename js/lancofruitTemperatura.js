/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/lancofruitTemperatura.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/lancofruitTemperatura.js":
/*!*********************************************!*\
  !*** ./js_modules/lancofruitTemperatura.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _window = window,\n    app = _window.app;\n\n\napp.service('temperatura', ['client', function (client) {\n    this.tags = function (params, callback) {\n        var url = 'phrapi/temperatura/index';\n        var data = params || {};\n        client.post(url, callback, data);\n    };\n}]);\n\napp.controller('temperatura', ['$scope', '$http', '$location', 'temperatura', '$controller', '$window', function ($scope, $http, $location, temperatura, $controller, $window) {\n\n    $scope.filters = {\n        variable: 'max'\n    };\n    $scope.semanas = [];\n    $scope.data = {\n        max: [],\n        min: [],\n        prom: []\n    };\n    $scope.service = {\n        index: function index() {\n            var data = {};\n            temperatura.tags(data, $scope.print);\n        },\n        all: function all() {\n            this.index();\n        }\n    };\n\n    $scope.print = function (r, b) {\n        b();\n        $scope.semanas = r.temp_maxima.legends;\n        setData('max', r.temp_maxima.series, r.temp_maxima.legends);\n        setData('min', r.temp_minima.series, r.temp_minima.legends);\n        setData('prom', r.temp_promedio.series, r.temp_promedio.legends);\n\n        var optionsMax = {\n            series: r.temp_maxima.series,\n            legend: r.temp_maxima.legends,\n            id: \"temp_maxima\",\n            type: 'line',\n            min: 'dataMin'\n        };\n        reRenderChart('temp_maxima', optionsMax);\n\n        var optionsMin = {\n            series: r.temp_minima.series,\n            legend: r.temp_minima.legends,\n            id: \"temp_minima\",\n            type: 'line',\n            min: 'dataMin'\n        };\n        reRenderChart('temp_minima', optionsMin);\n\n        var optionsProm = {\n            series: r.temp_promedio.series,\n            legend: r.temp_promedio.legends,\n            id: \"temp_promedio\",\n            type: 'line',\n            min: 'dataMin'\n        };\n        reRenderChart('temp_promedio', optionsProm);\n\n        $scope.reRenderTable();\n    };\n\n    $scope.reRenderTable = function () {\n        $(\"#table-react\").html(\"\");\n        var props = {\n            header: [{\n                key: 'estacion',\n                name: 'ESTACION',\n                titleClass: 'text-center',\n                locked: true,\n                resizable: true,\n                width: 170\n            }, {\n                key: 'prom',\n                name: 'PROM',\n                titleClass: 'text-center',\n                locked: true,\n                resizable: true\n            }, {\n                key: 'max',\n                name: 'MAX',\n                titleClass: 'text-center',\n                locked: true,\n                resizable: true\n            }, {\n                key: 'min',\n                name: 'MIN',\n                titleClass: 'text-center',\n                locked: true,\n                resizable: true\n            }],\n            data: $scope.data[$scope.filters.variable],\n            height: 200,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    $scope.table1.exportToExcel();\n                },\n                className: ''\n            }]\n        };\n        for (var i in $scope.semanas) {\n            var sem = $scope.semanas[i];\n            props.header.push({\n                key: 'sem_' + sem,\n                name: sem,\n                titleClass: 'text-center',\n                locked: true,\n                resizable: true\n            });\n        }\n        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-react'));\n    };\n\n    var setData = function setData(key, series, legends) {\n        var rows = Object.keys(series).map(function (key) {\n            return { estacion: key };\n        });\n        for (var i in rows) {\n            var serie = rows[i].estacion,\n                data = [];\n\n            for (var j in legends) {\n                var sem = legends[j];\n                data[j] = rows[i]['sem_' + sem] = series[serie] && parseFloat(series[serie].data[j]) ? parseFloat(series[serie].data[j]) : '';\n            }\n\n            // MAX MIN PROM\n            var max = null,\n                min = null,\n                sum = 0,\n                count = 0,\n                prom = null;\n            for (var _i in data) {\n                var value = parseFloat(data[_i]);\n                if (value > 0) {\n                    if (max == null || max < value) max = value;\n                    if (min == null || min > value) min = value;\n                    count++;\n                    sum += value;\n                }\n            }\n            prom = Math.round(sum / count * 100) / 100;\n\n            rows[i].max = max;\n            rows[i].min = min;\n            rows[i].prom = prom;\n        }\n\n        $scope.data[key] = rows;\n    };\n\n    var reRenderChart = function reRenderChart(id, options) {\n        var parent = $('#' + id).parent();\n        $('#' + id).remove();\n        parent.append('<div id=\"' + id + '\" style=\"height:500px;\"></div>');\n        ReactDOM.render(React.createElement(Historica, options), document.getElementById(id));\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/lancofruitTemperatura.js?");

/***/ })

/******/ });