/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/lancofruitVentasDia.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/lancofruitVentasDia.js":
/*!*******************************************!*\
  !*** ./js_modules/lancofruitVentasDia.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _window = window,\n    app = _window.app,\n    load = _window.load;\n\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('sumOfValueDouble', function () {\n    return function (data, key, key2) {\n        if (angular.isUndefined(data) || angular.isUndefined(key) || angular.isUndefined(key2)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key][key2] != \"\" && value[key][key2] != undefined && parseFloat(value[key][key2])) {\n                sum = sum + parseFloat(value[key][key2], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('getNotRepeat', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return [];\n\n        var _arr = [];\n        data.forEach(function (value) {\n            if (value[key]) {\n                if (!_arr.includes(value[key])) _arr.push(value[key]);\n            }\n        });\n        return _arr;\n    };\n});\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && value[key] > 0) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.filter('keys', function () {\n    return function (data) {\n        return Object.keys(data).length;\n    };\n});\n\napp.service('request', ['$http', function ($http) {\n    this.ventaDia = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/ventaDia';\n        load.block('tabla_venta_dia');\n        $http.post(url, params || {}).then(function (r) {\n            load.unblock('tabla_venta_dia');\n            callback(r.data);\n        });\n    };\n    this.last = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/last';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.tags = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/indicadores';\n        load.block('indicadores');\n        $http.post(url, params || {}).then(function (r) {\n            load.unblock('indicadores');\n            callback(r.data);\n        });\n    };\n    this.eliminar = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/eliminar';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n    this.editar = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/editar';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\nvar datesEnabled = [];\napp.controller('dia', ['$scope', 'request', function ($scope, request) {\n    var datepickerHighlight = function datepickerHighlight() {\n        $('#datepicker').datepicker({\n            beforeShowDay: function beforeShowDay(date) {\n                var fecha = moment(date).format('YYYY-MM-DD');\n                var has = datesEnabled.indexOf(fecha) > -1;\n                return has ? { classes: 'highlight', tooltip: 'Ventas', enabled: true } : { tooltip: 'No hay ventas', enabled: false };\n            }\n        });\n        $('#datepicker').datepicker().on('changeDate', function (e) {\n            $scope.changeRangeDate({ first_date: moment(e.date).format('YYYY-MM-DD'), second_date: moment(e.date).format('YYYY-MM-DD') });\n        });\n\n        $(\"#datepicker\").datepicker('setDate', $scope.fecha);\n        $('#datepicker').datepicker('update');\n    };\n\n    $(\"#actualizar\").prop(\"disabled\", true);\n    toastr.options = {\n        \"positionClass\": \"toast-top-center\"\n    };\n\n    $scope.var = 'tabla';\n    $scope.filters = {\n        fecha: \"\",\n        id_ruta: 1,\n        type: \"VENTAS\"\n    };\n    $scope.mostrar = true;\n    $scope.filtros = {};\n\n    $scope.loadCalidadUmbral = function () {\n        var c = localStorage.getItem('banano_calidad_cluster_umbral');\n        if (c) {\n            $scope.umbral_cluster = JSON.parse(c);\n        } else {\n            $scope.umbral_cluster = {\n                max: 92,\n                min: 90\n            };\n        }\n    };\n    $scope.saveCalidadUmbral = function () {\n        toastr.success('Umbral guardado');\n        localStorage.setItem('banano_calidad_cluster_umbral', JSON.stringify($scope.umbral_cluster));\n        localStorage.setItem('banano_calidad_dedos_umbral', JSON.stringify($scope.umbral_dedos));\n        localStorage.setItem('banano_calidad_empaque_umbral', JSON.stringify($scope.umbral_empaque));\n        $scope.reRenderMarkers();\n        $scope.closeMenu();\n    };\n    $scope.openMenu = function () {\n        $(\".toggler-close, .theme-options\").show();\n    };\n    $scope.closeMenu = function () {\n        $(\".toggler-close, .theme-options\").hide();\n    };\n    $scope.init = function () {\n        request.last(function (r) {\n            $scope.clientes = r.clientes;\n            $scope.rutas = r.rutas;\n            $scope.filters.fecha = r.fecha;\n            $scope.fecha = r.fecha;\n            datesEnabled = r.fechas_exist;\n            datepickerHighlight();\n\n            setTimeout(function () {\n                $(\"#fecha\").html('' + r.fecha);\n                $scope.indicadores();\n                $scope.ventaDia();\n            }, 250);\n        }, $scope.filters);\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.anioCambio = moment(data.first_date).year() != moment($scope.filters.fecha).year();\n            $scope.filters.fecha = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha;\n        }\n        $scope.porFiltroFecha();\n    };\n\n    $scope.porFiltroFecha = function () {\n        request.last(function (r) {\n            $scope.clientes = r.clientes;\n            $scope.rutas = r.rutas;\n            setTimeout(function () {\n                $scope.indicadores();\n                $scope.ventaDia();\n            }, 250);\n        }, $scope.filters);\n    };\n\n    $scope.changeDate = function () {\n        if ($scope.filters.fecha == \"\") {\n            $scope.filters.fecha = $scope.fecha;\n        } else {\n            request.last(function (r) {\n                $scope.clientes = r.clientes;\n                setTimeout(function () {\n                    $scope.indicadores();\n                    $scope.ventaDia();\n                }, 250);\n            }, $scope.filters);\n        }\n    };\n    $scope.changeRuta = function () {\n        $scope.indicadores();\n        $scope.ventaDia();\n    };\n    $scope.editar = function () {\n        if (confirm(\"SE GUARDARAN TODOS LOS CAMBIOS, ¿ESTÁ SEGURO?\")) {\n            request.editar(function (r) {\n                if (r.status == 200) {\n                    alert(\"Total actualizado correctamente\", \"ALERTA\", \"success\");\n                    $scope.mostrar = true;\n                    $scope.changeFilter();\n                } else {\n                    alert(\"No se ha enviado ningun total a modificar\");\n                }\n            }, $scope.filters);\n        }\n    };\n    $scope.activar = function () {\n        $scope.mostrar = !$scope.mostrar;\n    };\n    $scope.eliminar = function (id) {\n        if (confirm(\"¿ESTÁ SEGURO QUE DESEA ELIMINAR ESTE REGISTRO?\")) {\n            $scope.filters.id_venta = id;\n            request.eliminar(function (r) {\n                if (r.status == 200) {\n                    alert(\"Registro eliminado correctamente\", \"ALERTA\", \"success\");\n                    $scope.request.all_pestana_uno();\n                } else {\n                    alert(\"No se ha podido completar la acción, intente de nuevo\");\n                }\n            }, $scope.filters);\n        }\n    };\n    $scope.indicadores = function () {\n        request.tags(function (r) {\n            if (r.hasOwnProperty(\"tags\")) {\n                var data = {\n                    colums: 4,\n                    tags: r.tags,\n                    withTheresholds: false\n                };\n                ReactDOM.render(React.createElement(ListTags, data), document.getElementById('indicadores'));\n            }\n        }, $scope.filters);\n    };\n    $scope.ventaDia = function () {\n        request.ventaDia(function (r) {\n            $scope.datesEnabled = r.days;\n            $scope.data_ventas_dia = r.data_venta_dia;\n            if (r.data_venta_dia.length > 0) {\n                $scope.data_bar = r.data;\n                $scope.data_pie = r.clientes.map(function (r) {\n                    return {\n                        label: r.cliente,\n                        value: r.total\n                    };\n                });\n                $scope.initPastel('pastel-ventadia', $scope.data_pie);\n                $scope.printGrafica('line-ventadia', $scope.data_bar);\n            } else {\n                $scope.initPastel('pastel-ventadia', [{ label: 'VENTAS POR DIA', value: 0 }]);\n                $scope.printGrafica('line-ventadia', $scope.data_bar);\n            }\n        }, $scope.filters);\n    };\n    $scope.renderPie = function () {\n        $scope.initPastel('pastel-ventadia', $scope.data_pie);\n    };\n    $scope.renderBar = function () {\n        $scope.printGrafica('line-ventadia', $scope.data_bar);\n    };\n    $scope.initPastel = function (id, series) {\n        var legends = [];\n        series.map(function (key) {\n            if (legends.indexOf(series[key]) != -1) legends.push(series[key]);\n        });\n        setTimeout(function () {\n            var data = {\n                data: series,\n                nameseries: \"Pastel\",\n                legend: legends,\n                titulo: \"\",\n                id: id\n            };\n            var parent = $(\"#\" + id).parent();\n            parent.empty();\n            parent.append('<div id=\"' + id + '\" class=\"chart\"></div>');\n            ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));\n        }, 250);\n    };\n    $scope.printGrafica = function (id, r) {\n        var type = '';\n        if (r.legend.length > 1) {\n            type = 'line';\n        } else {\n            type = 'bar';\n        }\n\n        setTimeout(function () {\n            var data = {\n                series: r.data,\n                legend: r.legend,\n                umbral: '',\n                id: id,\n                type: type,\n                min: 'dataMin'\n            };\n            var parent = $(\"#\" + id).parent();\n            parent.empty();\n            parent.append('<div id=\"' + id + '\" class=\"chart\"></div>');\n            ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));\n        }, 250);\n    };\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/lancofruitVentasDia.js?");

/***/ })

/******/ });