/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/lancofruitVentasTendencia.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/lancofruitVentasTendencia.js":
/*!*************************************************!*\
  !*** ./js_modules/lancofruitVentasTendencia.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _window = window,\n    app = _window.app,\n    load = _window.load;\n\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (field == 'hora' || field == 'fecha') {\n                item.date = moment(item.fecha + ' ' + item.hora);\n            }\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (field == 'hora' || field == 'fecha') {\n                return moment(a.date).isAfter(b.date) ? 1 : -1;\n            } else if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && value[key] > 0) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.service('request', ['$http', function ($http) {\n    this.ventaTendencia = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/ventaTendencia';\n        load.block('tabla_venta_dia');\n        $http.post(url, params || {}).then(function (r) {\n            load.unblock('tabla_venta_dia');\n            callback(r.data);\n        });\n    };\n    this.ventaTendenciaGrafica = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/tendenciaGrafica';\n        load.block('tabla_venta_dia');\n        $http.post(url, params || {}).then(function (r) {\n            load.unblock('tabla_venta_dia');\n            callback(r.data);\n        });\n    };\n    this.last = function (callback, params) {\n        var url = 'phrapi/lancofruit/reportes/last';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\nfunction getOptionsGraficaReact(id, options, umbral) {\n    var newOptions = {\n        series: options.series,\n        legend: options.legends,\n        umbral: umbral,\n        id: id,\n        min: 'dataMin',\n        actions: false\n    };\n    return newOptions;\n}\n\nfunction initGrafica(id, options, umbral) {\n    setTimeout(function () {\n        var data = getOptionsGraficaReact(id, options, umbral);\n        var parent = $(\"#\" + id).parent();\n        parent.empty();\n        parent.append('<div id=\"' + id + '\" class=\"chart\"></div>');\n        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));\n    }, 250);\n}\n\napp.controller('tendencia', ['$scope', 'request', '$filter', function ($scope, request, $filter) {\n    $scope.filters = {\n        type: 'VENTAS'\n    };\n\n    $scope.init = function () {\n        request.last(function (r) {\n            $scope.clientes = r.clientes;\n            setTimeout(function () {\n                $scope.general();\n            }, 250);\n        }, $scope.filters);\n    };\n\n    $scope.general = function () {\n        request.ventaTendencia(function (r) {\n            renderTablaGeneral(r.series, r.legends);\n        }, $scope.filters);\n        request.ventaTendenciaGrafica(function (r) {\n            renderGraficaGeneral(r.data, 0);\n        }, $scope.filters);\n    };\n\n    var renderGraficaGeneral = function renderGraficaGeneral(data, umbral) {\n        initGrafica('line-ventadia', data, umbral);\n    };\n\n    var renderTablaGeneral = function renderTablaGeneral(data, semanas) {\n        var umbral = null;\n        //if($scope.filters.var == 'calidad_cluster') umbral = getClusterUmbral\n\n        var props = {\n            header: [{\n                key: 'name',\n                name: 'RUTA',\n                titleClass: 'text-center',\n                locked: true,\n                expandable: true,\n                resizable: true,\n                width: 170\n            }, {\n                key: 'prom',\n                name: 'PROM',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                formatter: 'Number',\n                resizable: true,\n                customCell: function customCell(row) {\n                    return '\\n                            <div class=\"' + (umbral != null ? umbral(row['prom']) : '') + '\">\\n                                ' + row['prom'] + '\\n                            </div>\\n                        ';\n                }\n            }, {\n                key: 'max',\n                name: 'MAX',\n                locked: true,\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                customCell: function customCell(row) {\n                    return '\\n                            <div class=\"' + (umbral != null ? umbral(row['max']) : '') + '\">\\n                                ' + row['max'] + '\\n                            </div>\\n                        ';\n                }\n            }, {\n                key: 'minimo',\n                name: 'MIN',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'right',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                customCell: function customCell(row) {\n                    return '\\n                            <div class=\"' + (umbral != null ? umbral(row['minimo']) : '') + '\">\\n                                ' + row['minimo'] + '\\n                            </div>\\n                        ';\n                }\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    $scope.table1.exportToExcel();\n                },\n                className: ''\n            }],\n            height: $scope.filters.mode == 'zona' ? 250 : 500\n        };\n        semanas.map(function (sem) {\n            props.header.push({\n                key: '' + sem,\n                name: '' + sem,\n                sortable: true,\n                alignContent: 'right',\n                titleClass: 'text-center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                customCell: function customCell(row) {\n                    return '\\n                        <div class=\"' + (umbral != null ? umbral(row.data['' + sem]) : '') + '\">\\n                            ' + row.data['' + sem] + '\\n                        </div>\\n                    ';\n                }\n            });\n        });\n        document.getElementById('tabla-general').innerHTML = \"\";\n        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('tabla-general'));\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/lancofruitVentasTendencia.js?");

/***/ })

/******/ });