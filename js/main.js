/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/main.js":
/*!****************************!*\
  !*** ./js_modules/main.js ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _window = window,\n    ahttp = _window.ahttp;\n\nvar spy = __webpack_require__(/*! console-spy */ \"./node_modules/console-spy/index.js\")();\n\nsetInterval(function () {\n    ahttp.post('phrapi/access/ping');\n}, 60000);\n\nArray.prototype.countIf = function (valid) {\n    var count = 0;\n    this.forEach(function (obj) {\n        if (valid(obj)) {\n            count++;\n        }\n    });\n    return count;\n};\n\n$.fn.datepicker.dates['es'] = {\n    days: [\"Domingo\", \"Lunes\", \"Martes\", \"Miercoles\", \"Jueves\", \"Viernes\", \"Sabado\"],\n    daysShort: [\"Dom\", \"Lun\", \"Mar\", \"Mie\", \"Jue\", \"Vie\", \"Sab\"],\n    daysMin: [\"Dom\", \"Lun\", \"Mar\", \"Mie\", \"Jue\", \"Vie\", \"Sab\"],\n    months: [\"Enero\", \"Febrero\", \"Marzo\", \"Abril\", \"Mayo\", \"Junio\", \"Julio\", \"Agosto\", \"Septiembre\", \"Octubre\", \"Noviembre\", \"Diciembre\"],\n    monthsShort: [\"Ene\", \"Feb\", \"Mar\", \"Abr\", \"May\", \"Jun\", \"Jul\", \"Ago\", \"Sep\", \"Oct\", \"Nov\", \"Dic\"],\n    today: \"Hoy\",\n    clear: \"Limpiar\",\n    format: \"dd/mm/yyyy\"\n};\n\n$(window).on('load', function () {\n    // makes sure the whole site is loaded\n    $('[data-loader=\"circle-side\"]').fadeOut(); // will first fade out the loading animation\n    $('#preloader').delay(100).fadeOut('slow'); // will fade out the white DIV that covers the website.\n    $('body').delay(100).css({\n        'overflow': 'visible'\n    });\n    $(\"#notificaciones ul li\").click(function () {\n        var noti = $(this);\n        swal({\n            title: 'Borrar notificación',\n            text: '¿Estas seguro de borrar esta notificación?',\n            icon: 'warning',\n            buttons: ['Cancelar', { text: 'Borrar', className: 'bg-red' }]\n        }).then(function (r_confirm) {\n            if (r_confirm) {\n                ahttp.post('phrapi/notificaciones/delete', function (r) {\n                    if (r.status === 200) {\n                        toatsr.success('Borrada');\n                        noti.remove();\n                    }\n                }, { id: noti.data('id') });\n            }\n        });\n    });\n});\n\nvar excepciones_errores = ['Warning: Each child in an array or iterator should have a unique \"key\" prop. Check the top-level render call using <div>. See https://fb.me/react-warning-keys for more information.\\n    in button', 'Warning: Invalid argument supplid to oneOfType. Expected an array of check functions, but received undefined at index 0.', 'WARNING: Tried to load AngularJS more than once.', 'Warning: Each child in an array or iterator should have a unique \"key\" prop. Check the top-level render call using <DIV>. See https://fb.me/react-warning-keys for more information.\\n    in DIV', 'Warning: Failed prop type: The prop `value` is marked as required in `SimpleCellFormatter`, but its value is `null`.\\n    in SimpleCellFormatter', 'ECharts#setTheme() is DEPRECATED in ECharts 3.0', 'Warning: Each child in an array or iterator should have a unique \"key\" prop. Check the top-level render call using <DIV>. See https://fb.me/react-warning-keys for more information.\\n    in SPAN', 'Invariant Violation: Minified React error #37; visit http://facebook.github.io/react/docs/error-decoder.html?invariant=37 for the full message or use the non-minified dev environment for full errors and additional helpful warnings.'];\n\nspy.on('error', function (err) {\n    try {\n        var error = err;\n        if (excepciones_errores.indexOf(error) === -1) {\n            ahttp.post('phrapi/consolelog/add', function () {}, { error: error, url: window.location.href });\n        }\n    } catch (e) {\n        console.log(e);\n    }\n});\nspy.enable();\n\n//# sourceURL=webpack:///./js_modules/main.js?");

/***/ }),

/***/ "./node_modules/console-spy/index.js":
/*!*******************************************!*\
  !*** ./node_modules/console-spy/index.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var emitter = __webpack_require__(/*! mittens */ \"./node_modules/mittens/index.js\")\nvar methods = __webpack_require__(/*! ./lib/methods */ \"./node_modules/console-spy/lib/methods.js\")\nvar console = __webpack_require__(/*! ./lib/console */ \"./node_modules/console-spy/lib/console.browser.js\")\nvar slice = Array.prototype.slice\nvar spying = false\n\nfunction ConsoleSpy () {\n  if (!(this instanceof ConsoleSpy)) {\n    return new ConsoleSpy\n  }\n\n  this.console = {}\n\n  ;(function proxy (spy, remaining) {\n    var method = methods[--remaining]\n\n    spy.console[method] = function () {\n      if (console.obj && typeof console.obj[method] === 'function') {\n        console.obj[method].apply(console.obj, arguments)\n      }\n      spy.emit.apply(spy, [method].concat(slice.call(arguments)))\n    }\n\n    if (remaining) proxy(spy, remaining)\n  })(this, methods.length)\n\n  this.enable()\n}\n\nemitter.call(ConsoleSpy.prototype)\n\nConsoleSpy.prototype.enable = function () {\n  if (!spying) {\n    console.set(this.console)\n    spying = true\n  }\n}\n\nConsoleSpy.prototype.disable = function () {\n  if (spying) {\n    console.set(console.obj)\n    spying = false\n  }\n}\n\nmodule.exports = ConsoleSpy\n\n\n//# sourceURL=webpack:///./node_modules/console-spy/index.js?");

/***/ }),

/***/ "./node_modules/console-spy/lib/console.browser.js":
/*!*********************************************************!*\
  !*** ./node_modules/console-spy/lib/console.browser.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var global = __webpack_require__(/*! global/window */ \"./node_modules/global/window.js\")\n\nmodule.exports = {\n  obj: ('console' in global &&\n    typeof global.console === 'object' &&\n    typeof global.console.log === 'function' ?\n      global.console : null\n  ),\n  set: function (obj) {\n    global.console = obj\n  }\n}\n\n\n//# sourceURL=webpack:///./node_modules/console-spy/lib/console.browser.js?");

/***/ }),

/***/ "./node_modules/console-spy/lib/methods.js":
/*!*************************************************!*\
  !*** ./node_modules/console-spy/lib/methods.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = [\n  'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',\n  'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',\n  'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',\n  'timeStamp', 'trace', 'warn'\n]\n\n\n//# sourceURL=webpack:///./node_modules/console-spy/lib/methods.js?");

/***/ }),

/***/ "./node_modules/global/window.js":
/*!***************************************!*\
  !*** ./node_modules/global/window.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("/* WEBPACK VAR INJECTION */(function(global) {var win;\n\nif (typeof window !== \"undefined\") {\n    win = window;\n} else if (typeof global !== \"undefined\") {\n    win = global;\n} else if (typeof self !== \"undefined\"){\n    win = self;\n} else {\n    win = {};\n}\n\nmodule.exports = win;\n\n/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ \"./node_modules/webpack/buildin/global.js\")))\n\n//# sourceURL=webpack:///./node_modules/global/window.js?");

/***/ }),

/***/ "./node_modules/mittens/index.js":
/*!***************************************!*\
  !*** ./node_modules/mittens/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = mittens\n\nfunction mittens (o) {\n  o = o || this\n  o.emit = emit\n  o.off = off\n  o.on = on\n  return o\n}\n\nfunction emit () {\n  var o = this, a = arguments\n  o.µ = o.µ || {}\n  var fn, ls = o.µ[a[0]] || []\n  var d = 0, al = a.length\n  var i = 0, l = ls.length\n  for (; i < l; i++) {\n    (fn = ls[i]) ?\n      al == 2 ? fn.call(o, a[1]) :\n      fn.apply(o, slice(a, 1)) :\n    d++\n  }\n  if (d) clean(ls)\n}\n\nfunction off () {\n  var o = this, a = arguments\n  var i, name = a[0], fn = a[1]\n  o.µ = o.µ || {}\n  if (!name) return o.µ = {}\n  var ls = o.µ[name] || []\n  if (fn && ~(i = ndx(ls, fn))) ls[i] = null\n  if (a.length === 1) o.µ[name] = []\n}\n\nfunction on (name, fn) {\n  var o = this\n  o.µ = o.µ || {}\n  var ls = o.µ[name] = o.µ[name] || []\n  if (fn && !~ndx(ls, fn)) ls[ls.length] = fn\n}\n\nfunction clean (ls, i) {\n  while (~(i = ndx(ls, null)))\n    ls.splice(i, 1)\n}\n\nfunction slice (ai, o) {\n  o = o || 0\n  var a = [], i = 0, l = ai.length - o\n  for (; i < l; i++) a[i] = ai[i + o]\n  return a\n}\n\nfunction ndx (ai, x) {\n  var i = 0, l = ai.length\n  for (; i < l; i++) if (ai[i] === x) return i\n  return -1\n}\n\n\n//# sourceURL=webpack:///./node_modules/mittens/index.js?");

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var g;\n\n// This works in non-strict mode\ng = (function() {\n\treturn this;\n})();\n\ntry {\n\t// This works if eval is allowed (see CSP)\n\tg = g || Function(\"return this\")() || (1, eval)(\"this\");\n} catch (e) {\n\t// This works if the window reference is available\n\tif (typeof window === \"object\") g = window;\n}\n\n// g can still be undefined, but nothing to do about it...\n// We return undefined, instead of nothing here, so it's\n// easier to handle this case. if(!global) { ...}\n\nmodule.exports = g;\n\n\n//# sourceURL=webpack:///(webpack)/buildin/global.js?");

/***/ })

/******/ });