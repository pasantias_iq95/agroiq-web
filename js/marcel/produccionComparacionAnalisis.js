/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marcel/produccionComparacionAnalisis.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marcel/produccionComparacionAnalisis.js":
/*!************************************************************!*\
  !*** ./js_modules/marcel/produccionComparacionAnalisis.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n\n    var service = {};\n\n    service.porHectareas = function (callback, params) {\n        load.block('porHectareas');\n        $http.post('phrapi/comparacion/hectareas', params || {}).then(function (r) {\n            load.unblock('porHectareas');\n            callback(r.data);\n        });\n    };\n\n    service.semanal = function (callback, params) {\n        load.block('semanal');\n        $http.post('phrapi/comparacion/semanal', params || {}).then(function (r) {\n            load.unblock('semanal');\n            callback(r.data);\n        });\n    };\n\n    return service;\n}]);\n\nfunction getOptionsGraficaReact(id, options, umbral) {\n    var newOptions = {\n        series: options.series,\n        legend: options.legends,\n        umbral: umbral,\n        id: id,\n        type: 'bar',\n        actions: false,\n        zoom: false\n    };\n    return newOptions;\n}\n\nfunction initGrafica(id, options, umbral) {\n    setTimeout(function () {\n        var parent = $(\"#\" + id).parent();\n        $(\"#\" + id).remove();\n        parent.append('<div id=\"' + id + '\" style=\"height:400px\"></div>');\n        var data = getOptionsGraficaReact(id, options, umbral);\n        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));\n    }, 250);\n}\n\nvar invertidos = ['RACIMOS RECUSADOS', 'EDAD PROM', '% RECUSADOS', '% MERMA CORTADA', '% MERMA PROCESADA'];\n\napp.controller('produccion', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.getUmbral = function (umbral, value, name) {\n        var invertido = true;\n        if (invertidos.indexOf(name) > -1) invertido = false;\n        if (!invertido) {\n            if (value > 0 && umbral > 0) {\n                if (value <= umbral) return 'border-bottom-green';else return 'border-bottom-red';\n            }\n        } else {\n            if (value > 0 && umbral > 0) {\n                if (value >= umbral) return 'border-bottom-green';else return 'border-bottom-red';\n            }\n        }\n        return '';\n    };\n\n    $scope.getUmbralFont = function (umbral, value, name) {\n        var invertido = true;\n        if (invertidos.indexOf(name) > -1) invertido = false;\n        if (!invertido) {\n            if (value > 0 && umbral > 0) {\n                if (value <= umbral) return 'text-green';else return 'text-red';\n            }\n        } else {\n            if (value > 0 && umbral > 0) {\n                if (value >= umbral) return 'text-green';else return 'text-red';\n            }\n        }\n        return '';\n    };\n\n    $scope.filters = {\n        por_hectareas: 'HA/ANIO',\n        por_semana: '',\n        chart_por_hectareas: 'RACIMOS CORTADOS',\n        chart_semanal: 'PESO RACIMO PROM LB'\n    };\n    $scope.por_hectareas = [];\n    $scope.init = function () {\n        $scope.getPorHectareas();\n        $scope.getSemanal();\n    };\n\n    $scope.getSemanal = function () {\n        console.log($scope.filters);\n        $request.semanal(printSemanal, $scope.filters);\n    };\n    $scope.getPorHectareas = function () {\n        $request.porHectareas(printPorHectareas, $scope.filters);\n    };\n    $scope.chartPorHectareas = function () {\n        var data = {};\n        $scope.por_hectareas.map(function (s) {\n            if (s.name == $scope.filters.chart_por_hectareas) data = s;\n        });\n        var series = {\n            QUINTANA: {\n                name: 'QUINTANA',\n                data: [data.quintana],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            LANIADO: {\n                name: 'LANIADO',\n                data: [data.laniado],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            MARUN: {\n                name: 'MARUN',\n                data: [data.marun],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            REISET: {\n                name: 'REISET',\n                data: [data.reiset],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            }\n        },\n            legends = [''];\n        initGrafica('chart-porHeactareas', { series: series, legends: legends }, data.agroban);\n    };\n    $scope.chartSemanal = function () {\n        var data = {};\n        $scope.semanal.map(function (s) {\n            if (s.name == $scope.filters.chart_semanal) data = s;\n        });\n        var series = {\n            QUINTANA: {\n                name: 'QUINTANA',\n                data: [data.quintana],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            LANIADO: {\n                name: 'LANIADO',\n                data: [data.laniado],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            MARUN: {\n                name: 'MARUN',\n                data: [data.marun],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            },\n            REISET: {\n                name: 'REISET',\n                data: [data.reiset],\n                connectNulls: true,\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        barBorderRadius: 0,\n                        barBorderWidth: 6\n                    }\n                },\n                label: {\n                    normal: {\n                        show: true,\n                        position: \"inside\"\n                    }\n                }\n            }\n        },\n            legends = [''];\n        initGrafica('chart-semanal', { series: series, legends: legends }, data.agroban);\n    };\n\n    printPorHectareas = function printPorHectareas(r) {\n        $scope.por_hectareas = r.data;\n        $scope.chartPorHectareas();\n    };\n\n    printSemanal = function printSemanal(r) {\n        $scope.semanal = r.data;\n        $scope.semanas = r.semanas;\n        $scope.chartSemanal();\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/marcel/produccionComparacionAnalisis.js?");

/***/ })

/******/ });