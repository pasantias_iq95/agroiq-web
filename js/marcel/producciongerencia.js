/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marcel/producciongerencia.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marcel/producciongerencia.js":
/*!*************************************************!*\
  !*** ./js_modules/marcel/producciongerencia.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (parseFloat(value[key])) sum = sum + parseFloat(value[key], 10);\n        });\n        return sum;\n    };\n});\n\napp.service('cajas', ['client', function () {\n    this.index = function (callback, params) {\n        var url = 'phrapi/cajas/index';\n        var data = params || {};\n        client.post(url, callback, data, 'content');\n    };\n}]);\n\napp.controller('produccion', ['$scope', '$http', '$interval', '$controller', '$timeout', '$window', '$filter', function ($scope, $http, $interval, $controller, $timeout, $window, $filter) {\n\n    $scope.produccion_questions = [{\n        name: 'Racimos cosechados'\n    }, {\n        name: 'Racimos recusados'\n    }, {\n        name: 'Racimos procesados'\n    }, {\n        name: 'Cajas de primera'\n    }, {\n        name: 'Cajas de segunda'\n    }, {\n        name: 'Total cajas'\n    }, {\n        name: 'Conversión cajas'\n    }, {\n        name: 'Enfunde'\n    }];\n\n    $scope.promedios_questions = [{\n        name: '% Recusados'\n    }, {\n        name: 'Peso prom racimos'\n    }, {\n        name: 'Calibre promedio'\n    }, {\n        name: 'Manos promedio'\n    }, {\n        name: 'Largo dedos prom'\n    }, {\n        name: 'Ratio cortado'\n    }, {\n        name: 'Ratio procesado'\n    }, {\n        name: 'Merma cortada'\n    }, {\n        name: 'Merma procesada'\n    }, {\n        name: 'Merma neta'\n    }, {\n        name: '% Recobro'\n    }, {\n        name: 'Edad prom cosecha'\n    }];\n}]);\n\n//# sourceURL=webpack:///./js_modules/marcel/producciongerencia.js?");

/***/ })

/******/ });