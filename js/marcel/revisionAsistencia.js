/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marcel/revisionAsistencia.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marcel/revisionAsistencia.js":
/*!*************************************************!*\
  !*** ./js_modules/marcel/revisionAsistencia.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar EnableEdit = 0;\ngrid.id = \"#asistencia\";\ngrid.url = \"phrapi/revision/asistencia/index\";\ngrid.addButtons({ extend: 'excel', \"title\": \"Asistencia\", \"text\": \"Excel\", className: 'btn yellow btn-outline ' });\ngrid.init();\n\n/*var grid2 = grid;\ngrid2.id = \"#asistencia_2\";\ngrid2.url = \"phrapi/revision/asistencia/excel/index\";\ngrid2.init();*/\n\nvar className = \".horas-\";\nvar editEnable = function editEnable(actve) {\n\tEnableEdit = EnableEdit == 0 ? 1 : 0;\n\tvar label = EnableEdit == 0 ? 'Editar' : 'Guardar';\n\t$(\"#EnableEdit\").val(EnableEdit);\n\t$(\"#editData\").html(label);\n\tif (actve) {\n\t\tgrid.table.getDataTable().clearPipeline().draw();\n\t\t$(\"#cancelarData\").css('display', '');\n\t} else {\n\t\t$(\"#cancelarData\").css('display', 'none');\n\t}\n};\n\n$(\"table\").on('change', '.labor_change', function (e) {\n\te.preventDefault();\n\tvar id = this.id.split(\"-\");\n\tconsole.log(id);\n\tif (this.value != \"NUM\") {\n\t\t$(className + id[1]).attr('readonly', 'readonly');\n\t\t$(className + id[1]).val(this.value);\n\t} else {\n\t\t$(className + id[1]).removeAttr('readonly');\n\t\t$(className + id[1]).val(\"\");\n\t}\n});\n\nvar save = function save() {\n\tvar data = $(\".save\").serializeObject();\n\tif (data) {\n\t\tconsole.log(data);\n\t\tahttp.post(\"phrapi/cambiar/revision/asistencia\", function (r, b) {\n\t\t\tb();\n\t\t\tif (r && r.hasOwnProperty(\"success\") && r.success == 200) {\n\t\t\t\teditEnable(true);\n\t\t\t} else {\n\t\t\t\tconsole.log(\"Ocurrio un Error\");\n\t\t\t}\n\t\t}, data);\n\t}\n};\n\nfunction getRandomInt(min, max) {\n\treturn Math.floor(Math.random() * (max - min)) + min;\n}\n\nvar reload = function reload(r, b) {\n\tb();\n\tif (r) {\n\t\tconsole.log(r);\n\t}\n};\n\nvar deleteRow = function deleteRow(key) {\n\tvar res = confirm(\"Esta seguro de Eliminar el registro\");\n\tif (res) {\n\t\tif (key) {\n\n\t\t\tvar data = {\n\t\t\t\tid_row: key,\n\t\t\t\ttoken: getRandomInt(1, 99999)\n\t\t\t};\n\n\t\t\tahttp.post(\"phrapi/drop/asistencia\", reload, data);\n\t\t}\n\t}\n};\n\n$(\"#editData\").on('click', function (event) {\n\tevent.preventDefault();\n\t/* Act on the event */\n\tif ($(this).html() == \"Guardar\") {\n\t\tsave();\n\t} else {\n\t\teditEnable(true);\n\t}\n});\n\n$(\"#cancelarData\").on('click', function (event) {\n\tevent.preventDefault();\n\t/* Act on the event */\n\tif (EnableEdit > 0) {\n\t\teditEnable(false);\n\t}\n});\n\n$(\"table\").on('change', '.date-picker', function (event) {\n\tevent.preventDefault();\n\t/* Act on the event */\n\t$('.date-picker').datepicker({\n\t\trtl: App.isRTL(),\n\t\tautoclose: true\n\t});\n});\n\n$(\".filter-cancel\").on('click', function (event) {\n\tevent.preventDefault();\n\t/* Act on the event */\n\tif (EnableEdit > 0) {\n\t\teditEnable(false);\n\t}\n});\n$(document).ready(function () {\n\t//var table = $('#asistencia').dataTable();\n\t//new $.fn.dataTable.FixedHeader( table );\n});\n\n//# sourceURL=webpack:///./js_modules/marcel/revisionAsistencia.js?");

/***/ })

/******/ });