/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marun/produccionResumenCorte.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marun/produccionResumenCorte.js":
/*!****************************************************!*\
  !*** ./js_modules/marun/produccionResumenCorte.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['client', '$http', function (client, $http) {\n    this.lastDay = function (callback, params) {\n        var data = params || {};\n        var url = 'phrapi/marun/produccion2/last';\n        client.post(url, callback, data);\n    };\n\n    this.filters = function (callback, params) {\n        var data = params || {};\n        var url = 'phrapi/marun/resumencorte/filters';\n        client.post(url, callback, data);\n    };\n\n    this.racimosEdad = function (callback, params) {\n        var data = params || {};\n        var url = 'phrapi/marun/produccion2/edad';\n        client.post(url, callback, data, 'table_por_edad');\n    };\n\n    this.guiasRemicion = function (callback, params) {\n        var url = 'phrapi/marun/cajas/cuadrar';\n        var data = params || {};\n        client.post(url, callback, data, 'table_4');\n    };\n\n    this.resumenCajas = function (callback, params) {\n        var url = 'phrapi/marun/resumencorte/cajas';\n        var data = params || {};\n        client.post(url, callback, data, 'div_table_2');\n    };\n\n    this.resumenProduccion = function (callback, params) {\n        var url = 'phrapi/marun/resumencorte/resumenProduccion';\n        var data = params || {};\n        client.post(url, callback, data, 'resumen_produccion');\n    };\n\n    this.racimosLote = function (callback, params) {\n        var data = params || {};\n        var url = 'phrapi/marun/produccion2/lote';\n        client.post(url, callback, data);\n    };\n}]);\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n            }\n        });\n        return sum;\n    };\n});\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined && parseFloat(value[key])) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        return sum;\n    };\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (field == 'hora' || field == 'fecha') {\n                item.date = moment(item.fecha + ' ' + item.hora);\n            }\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (field == 'hora' || field == 'fecha') {\n                return moment(a.date).isAfter(b.date) ? 1 : -1;\n            } else if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\nvar datesEnabled = [];\napp.controller('produccion', ['$scope', 'request', function ($scope, $request) {\n\n    datepickerHighlight = function datepickerHighlight() {\n        $('#datepicker').datepicker({\n            beforeShowDay: function beforeShowDay(date) {\n                var fecha = moment(date).format('YYYY-MM-DD');\n                var has = datesEnabled.indexOf(fecha) > -1;\n                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled: true } : { tooltip: 'Sin proceso', enabled: false };\n            }\n        });\n        $('#datepicker').datepicker().on('changeDate', function (e) {\n            $scope.changeRangeDate({ first_date: moment(e.date).format('YYYY-MM-DD'), second_date: moment(e.date).format('YYYY-MM-DD') });\n        });\n        $(\"#datepicker\").datepicker('setDate', $scope.filters.fecha_inicial);\n        $('#datepicker').datepicker('update');\n    };\n\n    $scope.filters = {\n        id_finca: '',\n        finca: '',\n        idFinca: '',\n        fecha_inicial: moment().startOf('month').format('YYYY-MM-DD'),\n        fecha_final: moment().endOf('month').format('YYYY-MM-DD'),\n        unidad: 'kg'\n    };\n\n    $scope.tabla = {};\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.wizardStep.params.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.wizardStep.params.fecha_final;\n        }\n\n        $scope.init();\n    };\n\n    $scope.getLastDay = function () {\n        $request.lastDay(function (r, b) {\n            b();\n            if (r) {\n                datesEnabled = r.days;\n                $scope.fincas = r.fincas;\n\n                $scope.enabled = r.enabled;\n                $scope.fecha = r.last.fecha;\n                $scope.filters.fecha_inicial = r.last.fecha;\n                $scope.filters.fecha_final = r.last.fecha;\n                datepickerHighlight();\n                //$scope.init()\n            }\n        });\n    };\n\n    $scope.init = function () {\n        var data = angular.copy($scope.filters);\n\n        $request.filters(function (r, b) {\n            $scope.fincas = r.fincas;\n            var ids = r.fincas.map(function (f) {\n                return f.id;\n            });\n            if (ids.length > 0) {\n                if (ids.indexOf($scope.filters.id_finca) == -1) {\n                    $scope.filters.id_finca = ids[0];\n                    $scope.filters.finca = ids[0];\n                    $scope.filters.idFinca = ids[0];\n\n                    data = angular.copy($scope.filters);\n                }\n            }\n\n            setTimeout(function () {\n                $(\"#finca\").val($scope.filters.id_finca);\n            }, 250);\n\n            $request.racimosLote(function (r, b) {\n                b();\n                if (r) {\n                    $scope.totales = r.totales;\n                }\n            }, data);\n\n            $request.racimosEdad(function (r, b) {\n                b('table_por_edad');\n                if (r) {\n                    $scope.tabla.edades = r.data;\n                }\n            }, data);\n\n            $request.guiasRemicion(function (r, b) {\n                b('table_4');\n                if (r) {\n                    $scope.cuadrarCajas = r.data;\n                    $scope.guias = r.guias;\n                }\n            }, data);\n\n            $request.resumenCajas(function (r, b) {\n                b('div_table_2');\n                if (r) {\n                    $scope.resumen = r.data;\n                }\n            }, data);\n\n            $request.resumenProduccion(function (r, b) {\n                b('resumen_produccion');\n                if (r) {\n                    $scope.resumenProduccion = r.data;\n                }\n            }, data);\n        }, data);\n    };\n\n    $scope.waitInit = function () {\n        $scope.filters.finca = $scope.filters.id_finca;\n        $scope.filters.idFinca = $scope.filters.id_finca;\n\n        var promise = new Promise(function (resolve, reject) {\n            setTimeout(function () {\n                resolve();\n            }, 500);\n        }).then(function () {\n            $scope.init();\n        });\n\n        promise();\n    };\n\n    $scope.getLastDay();\n}]);\n\n//# sourceURL=webpack:///./js_modules/marun/produccionResumenCorte.js?");

/***/ })

/******/ });