/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/marun/merma_res.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/marun sync recursive":
/*!*******************************!*\
  !*** ./js_modules/marun sync ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function webpackEmptyContext(req) {\n\tvar e = new Error(\"Cannot find module '\" + req + \"'\");\n\te.code = 'MODULE_NOT_FOUND';\n\tthrow e;\n}\nwebpackEmptyContext.keys = function() { return []; };\nwebpackEmptyContext.resolve = webpackEmptyContext;\nmodule.exports = webpackEmptyContext;\nwebpackEmptyContext.id = \"./js_modules/marun sync recursive\";\n\n//# sourceURL=webpack:///./js_modules/marun_sync?");

/***/ }),

/***/ "./js_modules/marun/merma_res.js":
/*!***************************************!*\
  !*** ./js_modules/marun/merma_res.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _defineProperty2 = __webpack_require__(/*! babel-runtime/helpers/defineProperty */ \"./node_modules/babel-runtime/helpers/defineProperty.js\");\n\nvar _defineProperty3 = _interopRequireDefault(_defineProperty2);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/*----------  UTILIDADES SOBRE ARRAYS  ----------*/\n/*app.filter('orderObjectBy', function() {\n    return function(items, field, reverse) {\n        //alert(field);\n        var filtered = [];\n        angular.forEach(items, function(item) {\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if(parseFloat(a[field]) && parseFloat(b[field]))\n                return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);\n            else\n                return (a[field] > b[field] ? 1 : -1);\n        });\n        if(reverse) filtered.reverse();\n        return filtered;\n    };\n});*/\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            //// console.log(item)\n            if (item.hasOwnProperty(\"lote\")) {\n                if (isNaN(parseFloat(item.lote))) {\n                    item.lote = parseFloat(item.lote);\n                } else {\n                    item.lote = item.lote;\n                }\n            }\n            if (item.hasOwnProperty(\"total_peso_merma\")) {\n                item.total_peso_merma = parseFloat(item.total_peso_merma);\n            }\n            if (item.hasOwnProperty(\"total_defectos\")) {\n                if (!isNaN(parseFloat(item.total_defectos))) item.total_defectos = parseFloat(item.total_defectos);\n            }\n            if (item.hasOwnProperty(\"merma\")) {\n                if (!isNaN(parseFloat(item.merma))) item.merma = parseFloat(item.merma);\n            }\n            if (item.hasOwnProperty(\"danhos_peso\")) {\n                if (!isNaN(parseFloat(item.danhos_peso))) item.danhos_peso = parseFloat(item.danhos_peso);\n            }\n            if (item.hasOwnProperty(\"filter\")) {\n                if (!isNaN(parseFloat(item.filter))) item.filter = parseFloat(item.filter);\n            }\n            if (item.hasOwnProperty(\"cantidad\")) {\n                if (!isNaN(parseFloat(item.cantidad))) item.cantidad = parseFloat(item.cantidad);\n            }\n\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            //alert(a[field]);\n            return a[field] > b[field] ? 1 : -1;\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.filter('unique', function () {\n    return function (collection, keyname) {\n        var output = [],\n            keys = [];\n\n        angular.forEach(collection, function (item) {\n            var key = item[keyname];\n            if (keys.indexOf(key) === -1) {\n                keys.push(key);\n                output.push(item);\n            }\n        });\n\n        return output;\n    };\n});\n\n/*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/\nvar globalEcharts;\nvar appEcharts = {\n    require: __webpack_require__(\"./js_modules/marun sync recursive\"),\n    options_historico: [],\n    options_historico_legends: [],\n    options_tendencia: [],\n    options_tendencia_legends: [],\n    options_dia: [],\n    dia_title: \"\",\n    historico_avg: 0,\n    margen: {\n        min: 0,\n        min_tendencia: 0,\n        max: 5,\n        max_tendencia: 2,\n        umbral: 2,\n        umbral_tendencia: .5\n    },\n    options_danos: [],\n    options_danos_detalle: [],\n    type: \"ENFUNDE\",\n    init: function init(callback) {\n        callback = callback || this.loadModules;\n        this.require.config({\n            paths: {\n                echarts: '/assets/global/plugins/echarts/'\n            }\n        });\n\n        this.require(['echarts', 'echarts/chart/bar', 'echarts/chart/chord', 'echarts/chart/eventRiver', 'echarts/chart/force', 'echarts/chart/funnel', 'echarts/chart/gauge', 'echarts/chart/heatmap', 'echarts/chart/k', 'echarts/chart/line', 'echarts/chart/map', 'echarts/chart/pie', 'echarts/chart/radar', 'echarts/chart/scatter', 'echarts/chart/tree', 'echarts/chart/treemap', 'echarts/chart/venn', 'echarts/chart/wordCloud'], callback);\n    },\n    // Este metodo existe por que aun no completo la function de parseo para que funcione directo\n    danos: function danos() {\n        var series = this.options_danos;\n        var options = {\n            title: {\n                text: series.title.text,\n                subtext: series.title.subtext,\n                x: 'center'\n            },\n            tooltip: {\n                trigger: 'item',\n                formatter: \"{a} <br/>{b} : {c} ({d}%)\"\n            },\n            legend: {\n                show: true,\n                // orient : 'vertical',\n                x: 'center',\n                y: 'bottom',\n                data: series.legend.data\n            },\n            toolbox: {\n                show: true,\n                feature: {\n                    mark: { show: false },\n                    dataView: { show: false, readOnly: false },\n                    magicType: {\n                        show: true,\n                        type: ['pie', 'funnel'],\n                        option: {\n                            funnel: {\n                                x: '25%',\n                                width: '50%',\n                                funnelAlign: 'left',\n                                max: 1548\n                            }\n                        }\n                    },\n                    restore: { show: true },\n                    saveAsImage: { show: true, name: series.toolbox.feature.saveAsImage.name }\n                }\n            },\n            calculable: true,\n            series: [{\n                name: series.series.name,\n                type: 'pie',\n                radius: '40%',\n                center: ['50%', '40%'],\n                data: series.series.data\n            }]\n        };\n        return options;\n    },\n    // Este metodo existe por que aun no completo la function de parseo para que funcione directo\n    // Adicionalmente es aqui donde se cambia entre tipo de detalle por medio de la flag 'type'\n    danos_detalle: function danos_detalle() {\n        var options = {};\n        ////console.log(this.type);\n        if (this.options_danos_detalle.hasOwnProperty(this.type)) {\n            var series = this.options_danos_detalle[this.type];\n            options = {\n                title: {\n                    text: series.title.text,\n                    subtext: series.title.subtext,\n                    x: 'center'\n                },\n                tooltip: {\n                    trigger: 'item',\n                    formatter: \"{a} <br/>{b} : {c} ({d}%)\"\n                },\n                legend: {\n                    show: true,\n                    // orient : 'vertical',\n                    x: 'center',\n                    y: 'bottom',\n                    data: series.legend.data\n                },\n                toolbox: {\n                    show: true,\n                    feature: {\n                        mark: { show: false },\n                        dataView: { show: false, readOnly: false },\n                        magicType: {\n                            show: true,\n                            type: ['pie', 'funnel'],\n                            option: {\n                                funnel: {\n                                    x: '25%',\n                                    width: '50%',\n                                    funnelAlign: 'left',\n                                    max: 1548\n                                }\n                            }\n                        },\n                        restore: { show: true },\n                        saveAsImage: { show: true, name: series.toolbox.feature.saveAsImage.name }\n                    }\n                },\n                calculable: true,\n                series: [{\n                    name: series.series.name,\n                    type: 'pie',\n                    radius: '40%',\n                    center: ['50%', '40%'],\n                    data: series.series.data\n                }]\n            };\n        }\n        return options;\n    },\n    historico: function historico() {\n        //// console.log(this.options_historico);\n        var category = [];\n        var legend = [];\n        var series = [];\n        var legend = [];\n        var legend_data = [];\n        var series = [];\n        legend = Object.keys(this.options_historico_legends);\n\n        if (id_company == 100) {\n            $.each(this.options_historico, function (index, value) {\n                var newSeries = [];\n                $.each(legend, function (i, val) {\n                    if (value.data.hasOwnProperty(val) >= 0) {\n                        newSeries.push(value.data[val]);\n                    } else {\n                        newSeries.push(null);\n                    }\n                });\n                value.data = newSeries;\n                series.push(value);\n                legend_data.push(value.name);\n            });\n        } else {\n            for (var i in this.options_historico) {\n                series.push(this.options_historico[i]);\n                legend_data.push(this.options_historico[i].name);\n            }\n        }\n\n        var option = {\n            tooltip: {\n                trigger: 'axis'\n            },\n            legend: {\n                show: true,\n                // orient : 'vertical',\n                x: 'center',\n                y: 'bottom',\n                data: legend_data\n            },\n            toolbox: {\n                show: true,\n                feature: {\n                    // dataView : {show: true, readOnly: false},\n                    magicType: { show: true, type: ['line', 'bar'] },\n                    restore: { show: true },\n                    saveAsImage: { show: true }\n                }\n            },\n            calculable: true,\n            xAxis: [{\n                // xAxisName:'hola',\n                type: 'category',\n                boundaryGap: false,\n                data: legend\n            }],\n            yAxis: [{\n                type: 'value',\n                axisLabel: {\n                    formatter: '{value}'\n                },\n                min: this.margen.min,\n                max: this.margen.max\n            }],\n            series: series\n        };\n        return option;\n    },\n    dia: function dia() {\n        var legend_tmp = [];\n        var legend = [];\n        var series = [];\n        var options_dia = this.options_dia;\n\n        legend_tmp = Object.keys(options_dia.label);\n\n        legend_tmp.sort(function (a, b) {\n            return new Date(a) - new Date(b);\n        });\n\n        legend = legend_tmp.map(function (value, index, arr) {\n            var date = new Date(value);\n            var horas = date.getHours();\n            var minutos = date.getMinutes();\n            if (parseInt(minutos) == 0) {\n                minutos = \"00\";\n            }\n            if (parseInt(minutos) < 10) {\n                minutos = \"0\" + minutos;\n            }\n            series.push(options_dia.series[value]);\n            return horas + \":\" + minutos;\n        });\n        //// console.log(legend);\n        //// console.log(options_dia);\n        //// console.log(series);\n\n        // var avg = new Number(this.historico_avg).toFixed(2);\n\n        var option = {\n            title: {\n                text: this.dia_title,\n                x: 'center'\n            },\n            tooltip: {\n                show: true\n            },\n            xAxis: [{\n                type: 'category',\n                data: legend\n            }],\n            yAxis: [{\n                type: 'value',\n                min: this.margen.min,\n                max: this.margen.max\n            }],\n            series: [{\n                name: \"Merma\",\n                type: \"bar\",\n                itemStyle: {\n                    normal: {\n                        color: '#007537',\n                        barBorderColor: '#007537',\n                        barBorderWidth: 6,\n                        barBorderRadius: 0,\n                        label: {\n                            show: true, position: 'insideTop'\n                        }\n                    }\n                },\n                markLine: {\n                    data: [[{ name: 'Umbral', value: this.margen.umbral, xAxis: -1, yAxis: this.margen.umbral }, { xAxis: 52, yAxis: this.margen.umbral }]]\n                },\n                data: series\n            }]\n        };\n\n        //console.log(option)\n        return option;\n    },\n    tendencia: function tendencia() {\n        //// console.log(this.options_historico);\n        var category = [];\n        var legend = [];\n        var series = [];\n        var legend = [];\n        var legend_data = [];\n        var series = [];\n        legend = Object.keys(this.options_tendencia_legends);\n        //console.log(legend)\n        for (var i in this.options_tendencia) {\n            //console.log(this.options_tendencia[i]);\n            series.push(this.options_tendencia[i]);\n            legend_data.push(this.options_tendencia[i].name);\n        }\n\n        //console.log(series);\n\n        var option = {\n            tooltip: {\n                trigger: 'axis'\n            },\n            legend: {\n                show: true,\n                // orient : 'vertical',\n                x: 'center',\n                y: 'bottom',\n                data: legend_data\n            },\n            toolbox: {\n                show: true,\n                feature: {\n                    // dataView : {show: true, readOnly: false},\n                    magicType: { show: true, type: ['line'] },\n                    restore: { show: true },\n                    saveAsImage: { show: true }\n                }\n            },\n            calculable: true,\n            xAxis: [{\n                // xAxisName:'hola',\n                type: 'category',\n                boundaryGap: false,\n                data: legend\n            }],\n            yAxis: [{\n                type: 'value',\n                axisLabel: {\n                    formatter: '{value}'\n                },\n                min: this.margen.min_tendencia,\n                max: this.margen.max_tendencia\n            }],\n            series: series\n        };\n        return option;\n    },\n    loadModules: function loadModules(echarts) {\n        globalEcharts = echarts;\n        var historico = [],\n            options_historico = appEcharts.historico();\n        var tendencia = [],\n            options_tendencia = appEcharts.tendencia();\n        var dia = [],\n            options_dia = appEcharts.dia();\n        var danos = [],\n            options_danos = appEcharts.danos();\n        var danos_detalle = [],\n            options_danos_detalle = appEcharts.danos_detalle();\n\n        // Creamos la grafica\n        historico = echarts.init(document.getElementById('historico'), 'infographic');\n        // Asignamos las series , titulos , etc\n        historico.setOption(options_historico);\n\n        // Creamos la grafica\n        tendencia = echarts.init(document.getElementById('tendencia'), 'infographic');\n        // Asignamos las series , titulos , etc\n        tendencia.setOption(options_tendencia);\n\n        // Creamos la grafica\n        dia = echarts.init(document.getElementById('dia'), 'infographic');\n        // Asignamos las series , titulos , etc\n        dia.setOption(options_dia);\n\n        // Creamos la grafica\n        danos = echarts.init(document.getElementById('echarts_generales'), 'infographic');\n        // Asignamos las series , titulos , etc\n        danos.setOption(options_danos);\n\n        // Creamos la grafica\n        danos_detalle = echarts.init(document.getElementById('echarts_generales_detalles'), 'infographic');\n        // Validamos que exista data para el detalle\n        if (options_danos_detalle.hasOwnProperty(\"title\")) {\n            // Asignamos las series , titulos , etc\n            danos_detalle.setOption(options_danos_detalle);\n        }\n\n        // Asignamos el tema\n        danos.setTheme('infographic');\n        danos_detalle.setTheme('infographic');\n        // Asignamos el 'responsive'\n        window.onresize = function () {\n            historico.resize();\n            tendencia.resize();\n            dia.resize();\n            danos_detalle.resize();\n            danos.resize();\n        };\n    }\n    /*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/\n\n};function loadScript(options, margen) {\n    appEcharts.options_historico = options.historico;\n    appEcharts.options_historico_legends = options.historico_legends;\n    appEcharts.options_tendencia = options.tendencia;\n    appEcharts.options_tendencia_legends = options.tendencia_legends;\n    appEcharts.historico_avg = options.historico_avg;\n    appEcharts.options_dia = options.dia;\n    appEcharts.dia_title = options.dia_title;\n    appEcharts.options_danos = options.danos;\n    appEcharts.options_danos_detalle = options.danos_detalle;\n    appEcharts.margen = margen;\n    appEcharts.init();\n}\n\napp.controller('informe_calidad', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', function ($scope, $http, $interval, client, $controller, $timeout, $window) {\n    var _$scope$tags;\n\n    $scope.leyendaGeneralTitle = 'Merma';\n\n    $scope.labelLbKg = \"Kilos\";\n    $scope.statusLbKg = true;\n\n    $scope.labelPeso = \"\";\n\n    $scope.id_company = 0;\n    $scope.tags = (_$scope$tags = {\n        merma: { value: 0, label: \"\" },\n        merma_procesada: { value: 0, label: \"\" },\n        merma_cortada: { value: 0, label: \"\" },\n        tallo: { value: 0, label: \"\" }\n    }, (0, _defineProperty3.default)(_$scope$tags, \"merma_cortada\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"enfunde\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"empaque\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"deshoje\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"lotero\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"aereo\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"terrestre\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"administracion\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"adm\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"natural\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"proceso\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"campo\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"cosecha\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"animales\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"hongos\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"empacadora\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"fisiologicos\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"virus\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"animal\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"bacteria\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"insectos\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"viejos\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"cajas\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"usd\", { value: 0, label: \"\" }), (0, _defineProperty3.default)(_$scope$tags, \"apuntalador\", { value: 0, label: \"\" }), _$scope$tags);\n\n    $scope.factor = 1;\n\n    $scope.graficas = {\n        historico: {},\n        tendencia: {},\n        historico_avg: 0,\n        dia: {},\n        dia_title: \"\",\n        danos: {},\n        danos_detalle: {},\n        historico_legends: {},\n        tendencia_legends: {}\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment().startOf('month'),\n        endDate: moment().endOf('month')\n    };\n\n    $scope.umbrales = {};\n    $scope.calidad = {\n        params: {\n            idFinca: \"\",\n            idFincaDia: \"\",\n            idMerma: \"MATERIA PRIMA\",\n            idLote: 0,\n            idLabor: 0,\n            fecha_inicial: moment().startOf('month').format('YYYY-MM-DD'),\n            fecha_final: moment().endOf('month').format('YYYY-MM-DD'),\n            cliente: \"\",\n            marca: \"\",\n            palanca: \"\",\n            statusLbKg: true,\n            year: \"YEAR(CURRENT_DATE)\"\n        },\n        step: 0,\n        path: ['phrapi/marun/merma/index', 'phrapi/merma/labores', 'phrapi/merma/causas'],\n        templatePath: [],\n        nocache: function nocache() {\n            this.templatePath.push('/views/marun/templetes/merma/step1.html?' + Math.random());\n        }\n    };\n\n    $scope.historico_tabla = [];\n    $scope.semanas_historico = [];\n\n    $scope.openDetalleHistorico = function (data) {\n        data.expanded = !data.expanded;\n    };\n\n    $scope.bgTagsFlags = function (umbral, value) {\n        var className = \"bg-yellow-gold bg-font-yellow-gold\";\n        if (!isNaN(parseFloat(value))) {\n            value = parseFloat(value);\n            umbral = parseFloat(umbral || 0);\n\n            if (value == umbral) {\n                return className;\n            } else if (value < umbral) {\n                className = \"bg-green-haze bg-font-green-haze\";\n            } else if (value > umbral) {\n                className = \"bg-red-thunderbird bg-font-red-thunderbird\";\n            }\n        }\n        return className;\n    };\n\n    convertModeSelect = function convertModeSelect() {\n        var mode = $(\"#mode\").val();\n        $scope.historicoSemanal(mode);\n    };\n\n    // 24/06/2017 - TAG: HISTORICO SEMANAL\n\n    $scope.fincas = [];\n    $scope.mermas = {\n        NETA: \"Merma Neta\",\n        \"MATERIA PRIMA\": \"Merma Prima\"\n    };\n\n    $scope.palancas = [];\n\n    $scope.loadExternal = function () {\n        ////console.log($scope.calidad.path[$scope.calidad.step]);\n        if ($scope.calidad.path[$scope.calidad.step] != \"\") {\n            var data = $scope.calidad.params;\n            client.post($scope.calidad.path[$scope.calidad.step], $scope.startDetails, data);\n            //$scope.historicoSemanal(\"Total Peso\");\n        }\n    };\n\n    changeYears = function changeYears() {\n        $scope.calidad.params.year = $('#historico_years').val();\n        $scope.loadExternal();\n    };\n\n    cambiosMerma = function cambiosMerma() {\n        $scope.calidad.params.palanca = \"\";\n        $scope.calidad.params.idFincaDia = \"\";\n        $scope.loadExternal();\n    };\n\n    cambiosPalanca = function cambiosPalanca() {\n        $scope.loadExternal();\n    };\n\n    cambiosFincas = function cambiosFincas() {\n        $scope.calidad.params.palanca = \"\";\n        $scope.calidad.params.idFincaDia = \"\";\n        $scope.loadExternal();\n    };\n\n    cambiarDanosDetalle = function cambiarDanosDetalle() {\n        appEcharts.type = $('#detalles_danhos').val();\n        appEcharts.init();\n    };\n\n    fincasDia = function fincasDia() {\n        $scope.loadExternal();\n    };\n\n    changeYearTendencia = function changeYearTendencia() {\n        $scope.calidad.params.yearTendencia = $(\"#yearTendencia\").val();\n        $scope.loadExternal();\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.calidad.params.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.wizardStep.params.fecha_inicial;\n            $scope.calidad.params.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.wizardStep.params.fecha_final;\n            $scope.loadExternal();\n        }\n    };\n\n    $scope.tabla_danos_merma = [];\n    $scope.tabla_lote_merma = [];\n    $scope.tabla_danos_merma_danhos_merma = [];\n\n    $scope.convertKg = function () {\n        $scope.statusLbKg = !$scope.statusLbKg;\n        if ($scope.statusLbKg === true) {\n            $scope.labelLbKg = \"Kilos\";\n            // $scope.labelPeso = \"(KG)\";\n        } else if ($scope.statusLbKg === false) {\n            $scope.labelLbKg = \"Libras\";\n            // $scope.labelPeso = \"(Lb)\";\n        }\n\n        $scope.calidad.params.statusLbKg = $scope.statusLbKg;\n        $scope.loadExternal();\n    };\n\n    $scope.classColumns = \"col-md-6 col-sm-6\";\n\n    $scope.disableColumns = function (column, e) {\n        if (column) {\n            if ($(\".\" + column).hasClass('column_hide')) {\n                $(\".\" + column).removeClass(column + ' column_hide').addClass(column);\n                $(e.target).parent().removeClass('active');\n            } else {\n                $(\".\" + column).addClass(column + ' column_hide');\n                $(e.target).parent().addClass('active');\n            }\n        }\n    };\n\n    /*\n        MODIFICACION JAVI \n    */\n    var has = Object.prototype.hasOwnProperty;\n\n    var second_level = function second_level(params) {\n        load.block(\"lote_table\");\n        return $http.post('phrapi/marun/merma/second', params);\n    };\n    var third_level = function third_level(params) {\n        load.block(\"lote_table\");\n        return $http.post('phrapi/marun/merma/third', params);\n    };\n\n    $scope.openDetalle = function (data, position) {\n        if (has.call(data, \"detalle\")) {\n            var detalle = data.detalle;\n\n            if (detalle.length <= 0) {\n                data.expanded = !data.expanded;\n                var params = {\n                    fecha_inicial: $scope.calidad.params.fecha_inicial,\n                    fecha_final: $scope.calidad.params.fecha_final,\n                    bloque: data.lote,\n                    palanca: $scope.calidad.params.palanca,\n                    idFinca: $scope.calidad.params.idFinca\n                };\n\n                second_level(params).then(function (r) {\n                    data.detalle = r.data;\n                    setTimeout(function () {\n                        load.unblock(\"lote_table\");console.log(data);\n                    }, 1000);\n                });\n            } else {\n                data.expanded = !data.expanded;\n            }\n        }\n        if (has.call(data, \"details\")) {\n            var details = data.details;\n\n            if (details.length <= 0) {\n                data.expanded = !data.expanded;\n                var _params = {\n                    fecha_inicial: $scope.calidad.params.fecha_inicial,\n                    fecha_final: $scope.calidad.params.fecha_final,\n                    bloque: data.bloque,\n                    type: data.type,\n                    danhos_peso: data.danhos_peso,\n                    racimos_lote: data.racimos_lote,\n                    cantidad: data.cantidad,\n                    palanca: $scope.calidad.params.palanca,\n                    idFinca: $scope.calidad.params.idFinca\n                };\n\n                third_level(_params).then(function (r) {\n                    data.details = r.data;\n                    setTimeout(function () {\n                        load.unblock(\"lote_table\");console.log(data);\n                    }, 1000);\n                });\n            } else {\n                data.expanded = !data.expanded;\n            }\n        }\n\n        if (position == 1) {\n            var generateId = \".detalle_\" + data.bloque + \"_\" + data.type.replace(' ', '_');\n            console.log(generateId);\n            console.log($(generateId).length);\n            if ($(generateId).length > 0) {\n                if (data.expanded) {\n                    $(generateId).css('display', '');\n                } else {\n                    $(generateId).css('display', 'none');\n                }\n            } else {\n                var row = $scope.generateRow(data);\n                $(\"#2nivel_\" + data.bloque + \"_\" + data.type.replace(' ', '_')).after(row);\n            }\n        }\n    };\n    /*\n        MODIFICACION JAVI \n    */\n\n    $scope.generateRow = function (data) {\n        var row = \"\";\n        var generateId = \"detalle_\" + data.bloque + \"_\" + data.type.replace(' ', '_');\n        if (data.hasOwnProperty(\"details\")) {\n            var table = data.details;\n            for (var i in table) {\n                row += \"<tr class='\" + generateId + \"' ng-show='category.expanded' style='cursor: pointer; text-align: right;'>\";\n                row += \"<td style='text-align: left !important;'>\" + table[i].campo + \"</td>\";\n                row += \"<td>\" + parseFloat(table[i].cantidad).toFixed(2) + \"</td>\";\n                row += \"<td>\" + parseFloat(table[i].danhos_peso).toFixed(2) + \"</td>\";\n                row += \"<td></td>\";\n                row += \"</tr>\";\n            }\n        }\n\n        return row;\n    };\n\n    var $table_transform = $(\"#lote_table\");\n\n    $scope.startDetails = function (r, b) {\n        b();\n        if (r) {\n            $scope.factor = 1;\n            $scope.classColumns = \"col-md-6 col-sm-6\";\n            var options = {};\n            // company \n            id_company = r.id_company;\n            $scope.id_company = r.id_company;\n            // data grafica\n            $scope.palancas = r.palanca || [];\n            $scope.fincas = r.fincas || [];\n\n            var data_graficas = r.historico || [];\n            $.each(data_graficas, function (index, value) {\n                var newData = [];\n                Object.keys(value.data).map(function (val) {\n                    newData.push(value.data[val]);\n                });\n                data_graficas[index].data = [];\n                data_graficas[index].data = newData;\n            });\n            $scope.graficas.historico = angular.copy(data_graficas);\n\n            $scope.graficas.historico_avg = r.historico_avg || [];\n            $scope.graficas.historico_legends = r.historico_legends || [];\n            $scope.graficas.dia = r.dia || [];\n            $scope.graficas.dia_title = r.dia_title || \"\";\n            $scope.graficas.danos = r.danos || [];\n            // La propiedad danos_detalle es un objeto con todos los detalles de daños \n            $scope.graficas.danos_detalle = r.danos_detalle || [];\n            // Tags \n            $scope.umbrales = r.umbrals || {};\n            $scope.tags.merma.value = new Number(r.tags.merma).toFixed(2);\n            var margen = {\n                min: 0,\n                min_tendencia: 0,\n                max: 5,\n                max_tendencia: 100,\n                umbral: 2,\n                umbral_tendencia: 50\n                /*if($scope.id_company == 2 || $scope.id_company == 8){\n                    $scope.tags.enfunde.value = new Number(r.tags.enfunde).toFixed(2);\n                    $scope.tags.campo.value = new Number(r.tags.campo).toFixed(2);\n                    $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);\n                    $scope.tags.animales.value = new Number(r.tags.animales).toFixed(2);\n                    $scope.tags.hongos.value = new Number(r.tags.hongos).toFixed(2);\n                    $scope.tags.empacadora.value = new Number(r.tags.empacadora).toFixed(2);\n                    $scope.tags.fisiologicos.value = new Number(r.tags.fisiologicos).toFixed(2);\n                    $scope.tags.apuntalador.value = new Number(r.tags.apuntalador).toFixed(2);\n                    $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);\n                    $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);\n                    if($scope.statusLbKg === true){\n                        $scope.labelPeso = \"(KG)\";\n                    }\n                    else if($scope.statusLbKg === false){\n                        $scope.labelPeso = \"(Lb)\";\n                    }\n                     if($scope.id_company == 8){\n                        margen.max = 5;\n                        margen.umbral = 2;\n                    }\n                }\n                /*if($scope.id_company == 6){\n                    $scope.classColumns = \"col-md-12 col-sm-12\";\n                    $scope.tags.enfunde.value = new Number(r.tags.enfunde).toFixed(2);\n                    $scope.tags.adm.value = new Number(r.tags.adm).toFixed(2);\n                    $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);\n                    $scope.tags.natural.value = new Number(r.tags.natural).toFixed(2);\n                    $scope.tags.proceso.value = new Number(r.tags.proceso).toFixed(2);\n                    $scope.tags.merma_procesada.value = new Number(r.tags.merma_procesada).toFixed(2);\n                    $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);\n                    $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);\n                    margen.max = 25;\n                    margen.umbral = 10;\n                }*/\n            };if ($scope.id_company == 7) {\n                $scope.tags.merma_procesada.value = new Number(r.tags.merma_procesada).toFixed(2);\n                $scope.tags.merma_cortada.value = new Number(r.tags.merma_cortada).toFixed(2);\n                $scope.tags.tallo.value = new Number(r.tags.tallo).toFixed(2);\n                $scope.tags.empaque.value = new Number(r.tags.empaque).toFixed(2);\n                $scope.tags.administracion.value = new Number(r.tags.administracion).toFixed(2);\n                $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);\n                $scope.tags.lotero.value = new Number(r.tags.lotero).toFixed(2);\n                $scope.tags.deshoje.value = new Number(r.tags.deshoje).toFixed(2);\n                $scope.tags.fisiologicos.value = new Number(r.tags.fisiologicos).toFixed(2);\n                $scope.tags.apuntalador.value = new Number(r.tags.apuntalador).toFixed(2);\n                $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);\n                $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);\n                if (r.hasOwnProperty(\"factor\")) {\n                    $scope.factor = new Number(r.factor).toFixed(2);\n                }\n                $scope.labelPeso = \"(KG)\";\n                margen.max = 25;\n                margen.umbral = 10;\n            }\n\n            /*if($scope.id_company == 3){\n                $scope.tags.merma_procesada.value = new Number(r.tags.merma_procesada).toFixed(2);\n                $scope.tags.merma_cortada.value = new Number(r.tags.merma_cortada).toFixed(2);\n                $scope.tags.tallo.value = new Number(r.tags.tallo).toFixed(2);\n                $scope.tags.empaque.value = new Number(r.tags.empaque).toFixed(2);\n                $scope.tags.administracion.value = new Number(r.tags.administracion).toFixed(2);\n                $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);\n                $scope.tags.lotero.value = new Number(r.tags.lotero).toFixed(2);\n                $scope.tags.deshoje.value = new Number(r.tags.deshoje).toFixed(2);\n                $scope.tags.fisiologicos.value = new Number(r.tags.fisiologicos).toFixed(2);\n                $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);\n                $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);\n                if($scope.statusLbKg === true){\n                    $scope.labelPeso = \"(KG)\";\n                }\n                else if($scope.statusLbKg === false){\n                    $scope.labelPeso = \"(Lb)\";\n                }\n                margen.max = 25;\n                margen.umbral = 10;\n            }\n             if($scope.id_company == 4 || $scope.id_company == 9){\n                $scope.tags.merma_procesada.value = new Number(r.tags.merma_procesada).toFixed(2);\n                $scope.tags.merma_cortada.value = new Number(r.tags.merma_cortada).toFixed(2);\n                $scope.tags.tallo.value = new Number(r.tags.tallo).toFixed(2);\n                $scope.tags.empacadora.value = new Number(r.tags.empacadora).toFixed(2);\n                $scope.tags.animal.value = new Number(r.tags.animal).toFixed(2);\n                $scope.tags.cosecha.value = new Number(r.tags.cosecha).toFixed(2);\n                $scope.tags.terrestre.value = new Number(r.tags.terrestre).toFixed(2);\n                $scope.tags.insectos.value = new Number(r.tags.insectos).toFixed(2);\n                // $scope.tags.bacteria.value = new Number(r.tags.bacteria).toFixed(2);\n                $scope.tags.aereo.value = new Number(r.tags.aereo).toFixed(2);\n                $scope.tags.hongos.value = new Number(r.tags.hongos).toFixed(2);\n                $scope.tags.fisiologicos.value = new Number(r.tags.fisiologicos).toFixed(2);\n                $scope.tags.cajas.value = new Number(r.tags.cajas).toFixed(2);\n                $scope.tags.usd.value = new Number(r.tags.usd).toFixed(2);\n                $scope.labelPeso = \"(KG)\";\n                margen.max = 20;\n                margen.umbral = 5;\n            }*/\n\n            $scope.graficas.tendencia = r.tendencia || [];\n            $scope.graficas.tendencia_legends = r.tendencia_legends || [];\n\n            // tablas\n            // console.log(r.tabla_lote_merma);\n            $scope.tabla_lote_merma = r.tabla_lote_merma;\n            $scope.tabla_danos_merma = r.tabla_danos_merma;\n            $scope.tabla_danos_merma_danhos_merma = r.tabla_danos_merma_danhos_merma;\n            // header\n            if (r.data_header) {\n                $scope.param_peso = r.data_header.peso;\n                $scope.param_cluster = r.data_header.cluster;\n                $scope.param_logo = r.data_header.logo;\n            }\n\n            setTimeout(function () {\n                loadScript($scope.graficas, margen);\n                generateSelect($scope.graficas.danos_detalle);\n                $(\".counter_tags\").counterUp({\n                    delay: 10,\n                    time: 1000\n                });\n                $table_transform.bootstrapTable();\n            }, 1000);\n        }\n    };\n\n    generateSelect = function generateSelect(data) {\n        var html = [];\n        var keys = Object.keys(data);\n        console.log(data);\n        for (var i in keys) {\n            html.push(\"<option value='\" + keys[i] + \"'>\" + keys[i] + \"</option>\");\n        }\n\n        $('#detalles_danhos').empty();\n        $('#detalles_danhos').append(html.join(' '));\n\n        setTimeout(function () {\n            cambiarDanosDetalle();\n        }, 1000);\n    };\n\n    $scope.companiesDefect = [];\n    $scope.companiesDefectProm = [2, 7];\n\n    $scope.inArray = function (needed, arr) {\n        return $.inArray(needed, arr) > -1 ? true : false;\n    };\n\n    $scope.exportExcel = function (table_id, title) {\n        var tableToExcel = function () {\n            var uri = 'data:application/vnd.ms-excel;base64,',\n                template = '<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body><table>{table}</table></body></html>',\n                base64 = function base64(s) {\n                return window.btoa(unescape(encodeURIComponent(s)));\n            },\n                format = function format(s, c) {\n                return s.replace(/{(\\w+)}/g, function (m, p) {\n                    return c[p];\n                });\n            };\n            return function (table, name) {\n                var id_table = table;\n                if (!table.nodeType) table = document.getElementById(table);\n                // delete input\n                var contentTable = table.innerHTML;\n                /*if(id_table == 'table_bonificacion'){\n                    var cont = 0\n                    while(contentTable.search('<input') > 0){\n                        var input_start = contentTable.search('<input')\n                        var input_end = contentTable.search('end=\"\"></td>')\n                         var input_comp = contentTable.substring(input_start, input_end + 7)\n                        var value =  $(input_comp).val()\n                        contentTable = contentTable.replace(input_comp, value)\n                    }\n                }*/\n                var ctx = { worksheet: name || 'Worksheet', table: contentTable };\n                window.location.href = uri + base64(format(template, ctx));\n            };\n        }();\n        tableToExcel(table_id, title);\n    };\n\n    $scope.loadExternal();\n}]);\n\n//# sourceURL=webpack:///./js_modules/marun/merma_res.js?");

/***/ }),

/***/ "./node_modules/babel-runtime/core-js/object/define-property.js":
/*!**********************************************************************!*\
  !*** ./node_modules/babel-runtime/core-js/object/define-property.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = { \"default\": __webpack_require__(/*! core-js/library/fn/object/define-property */ \"./node_modules/core-js/library/fn/object/define-property.js\"), __esModule: true };\n\n//# sourceURL=webpack:///./node_modules/babel-runtime/core-js/object/define-property.js?");

/***/ }),

/***/ "./node_modules/babel-runtime/helpers/defineProperty.js":
/*!**************************************************************!*\
  !*** ./node_modules/babel-runtime/helpers/defineProperty.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nexports.__esModule = true;\n\nvar _defineProperty = __webpack_require__(/*! ../core-js/object/define-property */ \"./node_modules/babel-runtime/core-js/object/define-property.js\");\n\nvar _defineProperty2 = _interopRequireDefault(_defineProperty);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nexports.default = function (obj, key, value) {\n  if (key in obj) {\n    (0, _defineProperty2.default)(obj, key, {\n      value: value,\n      enumerable: true,\n      configurable: true,\n      writable: true\n    });\n  } else {\n    obj[key] = value;\n  }\n\n  return obj;\n};\n\n//# sourceURL=webpack:///./node_modules/babel-runtime/helpers/defineProperty.js?");

/***/ }),

/***/ "./node_modules/core-js/library/fn/object/define-property.js":
/*!*******************************************************************!*\
  !*** ./node_modules/core-js/library/fn/object/define-property.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("__webpack_require__(/*! ../../modules/es6.object.define-property */ \"./node_modules/core-js/library/modules/es6.object.define-property.js\");\nvar $Object = __webpack_require__(/*! ../../modules/_core */ \"./node_modules/core-js/library/modules/_core.js\").Object;\nmodule.exports = function defineProperty(it, key, desc) {\n  return $Object.defineProperty(it, key, desc);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/fn/object/define-property.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_a-function.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_a-function.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function (it) {\n  if (typeof it != 'function') throw TypeError(it + ' is not a function!');\n  return it;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_a-function.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_an-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_an-object.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var isObject = __webpack_require__(/*! ./_is-object */ \"./node_modules/core-js/library/modules/_is-object.js\");\nmodule.exports = function (it) {\n  if (!isObject(it)) throw TypeError(it + ' is not an object!');\n  return it;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_an-object.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_core.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_core.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var core = module.exports = { version: '2.5.7' };\nif (typeof __e == 'number') __e = core; // eslint-disable-line no-undef\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_core.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_ctx.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ctx.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// optional / simple context binding\nvar aFunction = __webpack_require__(/*! ./_a-function */ \"./node_modules/core-js/library/modules/_a-function.js\");\nmodule.exports = function (fn, that, length) {\n  aFunction(fn);\n  if (that === undefined) return fn;\n  switch (length) {\n    case 1: return function (a) {\n      return fn.call(that, a);\n    };\n    case 2: return function (a, b) {\n      return fn.call(that, a, b);\n    };\n    case 3: return function (a, b, c) {\n      return fn.call(that, a, b, c);\n    };\n  }\n  return function (/* ...args */) {\n    return fn.apply(that, arguments);\n  };\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_ctx.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_descriptors.js":
/*!**************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_descriptors.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// Thank's IE8 for his funny defineProperty\nmodule.exports = !__webpack_require__(/*! ./_fails */ \"./node_modules/core-js/library/modules/_fails.js\")(function () {\n  return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;\n});\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_descriptors.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_dom-create.js":
/*!*************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_dom-create.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var isObject = __webpack_require__(/*! ./_is-object */ \"./node_modules/core-js/library/modules/_is-object.js\");\nvar document = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\").document;\n// typeof document.createElement is 'object' in old IE\nvar is = isObject(document) && isObject(document.createElement);\nmodule.exports = function (it) {\n  return is ? document.createElement(it) : {};\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_dom-create.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_export.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_export.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var global = __webpack_require__(/*! ./_global */ \"./node_modules/core-js/library/modules/_global.js\");\nvar core = __webpack_require__(/*! ./_core */ \"./node_modules/core-js/library/modules/_core.js\");\nvar ctx = __webpack_require__(/*! ./_ctx */ \"./node_modules/core-js/library/modules/_ctx.js\");\nvar hide = __webpack_require__(/*! ./_hide */ \"./node_modules/core-js/library/modules/_hide.js\");\nvar has = __webpack_require__(/*! ./_has */ \"./node_modules/core-js/library/modules/_has.js\");\nvar PROTOTYPE = 'prototype';\n\nvar $export = function (type, name, source) {\n  var IS_FORCED = type & $export.F;\n  var IS_GLOBAL = type & $export.G;\n  var IS_STATIC = type & $export.S;\n  var IS_PROTO = type & $export.P;\n  var IS_BIND = type & $export.B;\n  var IS_WRAP = type & $export.W;\n  var exports = IS_GLOBAL ? core : core[name] || (core[name] = {});\n  var expProto = exports[PROTOTYPE];\n  var target = IS_GLOBAL ? global : IS_STATIC ? global[name] : (global[name] || {})[PROTOTYPE];\n  var key, own, out;\n  if (IS_GLOBAL) source = name;\n  for (key in source) {\n    // contains in native\n    own = !IS_FORCED && target && target[key] !== undefined;\n    if (own && has(exports, key)) continue;\n    // export native or passed\n    out = own ? target[key] : source[key];\n    // prevent global pollution for namespaces\n    exports[key] = IS_GLOBAL && typeof target[key] != 'function' ? source[key]\n    // bind timers to global for call from export context\n    : IS_BIND && own ? ctx(out, global)\n    // wrap global constructors for prevent change them in library\n    : IS_WRAP && target[key] == out ? (function (C) {\n      var F = function (a, b, c) {\n        if (this instanceof C) {\n          switch (arguments.length) {\n            case 0: return new C();\n            case 1: return new C(a);\n            case 2: return new C(a, b);\n          } return new C(a, b, c);\n        } return C.apply(this, arguments);\n      };\n      F[PROTOTYPE] = C[PROTOTYPE];\n      return F;\n    // make static versions for prototype methods\n    })(out) : IS_PROTO && typeof out == 'function' ? ctx(Function.call, out) : out;\n    // export proto methods to core.%CONSTRUCTOR%.methods.%NAME%\n    if (IS_PROTO) {\n      (exports.virtual || (exports.virtual = {}))[key] = out;\n      // export proto methods to core.%CONSTRUCTOR%.prototype.%NAME%\n      if (type & $export.R && expProto && !expProto[key]) hide(expProto, key, out);\n    }\n  }\n};\n// type bitmap\n$export.F = 1;   // forced\n$export.G = 2;   // global\n$export.S = 4;   // static\n$export.P = 8;   // proto\n$export.B = 16;  // bind\n$export.W = 32;  // wrap\n$export.U = 64;  // safe\n$export.R = 128; // real proto method for `library`\nmodule.exports = $export;\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_export.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_fails.js":
/*!********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_fails.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function (exec) {\n  try {\n    return !!exec();\n  } catch (e) {\n    return true;\n  }\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_fails.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_global.js":
/*!*********************************************************!*\
  !*** ./node_modules/core-js/library/modules/_global.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028\nvar global = module.exports = typeof window != 'undefined' && window.Math == Math\n  ? window : typeof self != 'undefined' && self.Math == Math ? self\n  // eslint-disable-next-line no-new-func\n  : Function('return this')();\nif (typeof __g == 'number') __g = global; // eslint-disable-line no-undef\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_global.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_has.js":
/*!******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_has.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var hasOwnProperty = {}.hasOwnProperty;\nmodule.exports = function (it, key) {\n  return hasOwnProperty.call(it, key);\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_has.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_hide.js":
/*!*******************************************************!*\
  !*** ./node_modules/core-js/library/modules/_hide.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var dP = __webpack_require__(/*! ./_object-dp */ \"./node_modules/core-js/library/modules/_object-dp.js\");\nvar createDesc = __webpack_require__(/*! ./_property-desc */ \"./node_modules/core-js/library/modules/_property-desc.js\");\nmodule.exports = __webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\") ? function (object, key, value) {\n  return dP.f(object, key, createDesc(1, value));\n} : function (object, key, value) {\n  object[key] = value;\n  return object;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_hide.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_ie8-dom-define.js":
/*!*****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_ie8-dom-define.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = !__webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\") && !__webpack_require__(/*! ./_fails */ \"./node_modules/core-js/library/modules/_fails.js\")(function () {\n  return Object.defineProperty(__webpack_require__(/*! ./_dom-create */ \"./node_modules/core-js/library/modules/_dom-create.js\")('div'), 'a', { get: function () { return 7; } }).a != 7;\n});\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_ie8-dom-define.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_is-object.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_is-object.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function (it) {\n  return typeof it === 'object' ? it !== null : typeof it === 'function';\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_is-object.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_object-dp.js":
/*!************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_object-dp.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var anObject = __webpack_require__(/*! ./_an-object */ \"./node_modules/core-js/library/modules/_an-object.js\");\nvar IE8_DOM_DEFINE = __webpack_require__(/*! ./_ie8-dom-define */ \"./node_modules/core-js/library/modules/_ie8-dom-define.js\");\nvar toPrimitive = __webpack_require__(/*! ./_to-primitive */ \"./node_modules/core-js/library/modules/_to-primitive.js\");\nvar dP = Object.defineProperty;\n\nexports.f = __webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\") ? Object.defineProperty : function defineProperty(O, P, Attributes) {\n  anObject(O);\n  P = toPrimitive(P, true);\n  anObject(Attributes);\n  if (IE8_DOM_DEFINE) try {\n    return dP(O, P, Attributes);\n  } catch (e) { /* empty */ }\n  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');\n  if ('value' in Attributes) O[P] = Attributes.value;\n  return O;\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_object-dp.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_property-desc.js":
/*!****************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_property-desc.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("module.exports = function (bitmap, value) {\n  return {\n    enumerable: !(bitmap & 1),\n    configurable: !(bitmap & 2),\n    writable: !(bitmap & 4),\n    value: value\n  };\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_property-desc.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/_to-primitive.js":
/*!***************************************************************!*\
  !*** ./node_modules/core-js/library/modules/_to-primitive.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// 7.1.1 ToPrimitive(input [, PreferredType])\nvar isObject = __webpack_require__(/*! ./_is-object */ \"./node_modules/core-js/library/modules/_is-object.js\");\n// instead of the ES6 spec version, we didn't implement @@toPrimitive case\n// and the second argument - flag - preferred type is a string\nmodule.exports = function (it, S) {\n  if (!isObject(it)) return it;\n  var fn, val;\n  if (S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;\n  if (typeof (fn = it.valueOf) == 'function' && !isObject(val = fn.call(it))) return val;\n  if (!S && typeof (fn = it.toString) == 'function' && !isObject(val = fn.call(it))) return val;\n  throw TypeError(\"Can't convert object to primitive value\");\n};\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/_to-primitive.js?");

/***/ }),

/***/ "./node_modules/core-js/library/modules/es6.object.define-property.js":
/*!****************************************************************************!*\
  !*** ./node_modules/core-js/library/modules/es6.object.define-property.js ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("var $export = __webpack_require__(/*! ./_export */ \"./node_modules/core-js/library/modules/_export.js\");\n// 19.1.2.4 / 15.2.3.6 Object.defineProperty(O, P, Attributes)\n$export($export.S + $export.F * !__webpack_require__(/*! ./_descriptors */ \"./node_modules/core-js/library/modules/_descriptors.js\"), 'Object', { defineProperty: __webpack_require__(/*! ./_object-dp */ \"./node_modules/core-js/library/modules/_object-dp.js\").f });\n\n\n//# sourceURL=webpack:///./node_modules/core-js/library/modules/es6.object.define-property.js?");

/***/ })

/******/ });