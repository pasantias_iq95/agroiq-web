/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/mermahistorico.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/mermahistorico.js":
/*!**************************************!*\
  !*** ./js_modules/mermahistorico.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            // console.log(item)\n            if (item.hasOwnProperty(\"lote\")) {\n                if (!isNaN(parseFloat(item.lote))) item.lote = parseFloat(item.lote);\n            }\n            if (item.hasOwnProperty(\"total\")) {\n                item.total = parseFloat(item.total);\n            }\n            if (item.hasOwnProperty(\"exedente\")) {\n                if (!isNaN(parseFloat(item.exedente))) item.exedente = parseFloat(item.exedente);\n            }\n            if (item.hasOwnProperty(\"usd\")) {\n                if (!isNaN(parseFloat(item.usd))) item.usd = parseFloat(item.usd);\n            }\n\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            //alert(a[field]);\n            return a[field] > b[field] ? 1 : -1;\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.controller('historico', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', function ($scope, $http, $interval, client, $controller, $timeout, $window) {\n    $scope.id_company = 0;\n    $scope.reverseSort = false;\n    $scope.orderByField = \"lote\";\n    $scope.historico = {\n        params: {\n            idFinca: 1,\n            idLote: 0,\n            idLabor: 0,\n            semana: \"\",\n            type: \"ENFUNDE\"\n        },\n        step: 0,\n        path: ['phrapi/historico/index'],\n        templatePath: [],\n        nocache: function nocache() {\n            $scope.historica.init();\n            $scope.init();\n        }\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment().startOf('month'),\n        endDate: moment().endOf('month')\n    };\n\n    changeweek = function changeweek() {\n        $scope.init();\n    };\n\n    $scope.title = {\n        column_one: \"Lote\",\n        column_two: \"Enfundador\"\n    };\n\n    changeHistorica = function changeHistorica() {\n        $scope.historica.type = $(\"#type\").val();\n        $scope.historico.params.type = $(\"#type\").val();\n        if ($(\"#type\").val() == \"ENFUNDE\") {\n            $scope.title.column_one = \"Lote\";\n            $scope.title.column_two = \"Enfundador\";\n        } else if ($(\"#type\").val() == \"COSECHA\") {\n            $scope.title.column_one = \"Código\";\n            $scope.title.column_two = \"Palanca\";\n        }\n        $scope.historica.init();\n        $scope.init();\n    };\n\n    $scope.tabla = {\n        historico: []\n    };\n\n    $scope.openDetalle = function (data) {\n        data.expanded = !data.expanded;\n    };\n\n    $scope.data = [];\n    $scope.totales = [];\n    $scope.semanas = [];\n    $scope.historico = new echartsPlv();\n    $scope.historica = {\n        type: \"ENFUNDE\",\n        init: function init() {\n            var data = {\n                type: this.type\n            };\n            client.post('phrapi/historico/historico', $scope.printDetailsHistorica, data);\n        }\n    };\n    $scope.init = function () {\n        var data = $scope.historico.params;\n        client.post($scope.historico.path, $scope.printDetails, data);\n    };\n\n    $scope.start = true;\n    $scope.umbral = 10;\n    $scope.tagsFlags = function (value) {\n        var className = \"fa fa-arrow-up font-red-thunderbird\";\n        if (!isNaN(parseFloat(value))) {\n            if (value <= $scope.umbral) {\n                className = \"fa fa-arrow-down font-green-jungle\";\n            }\n        } else {\n            className = \"\";\n        }\n\n        return className;\n    };\n\n    $scope.printDetailsHistorica = function (r, b) {\n        b();\n        if (r) {\n            if (r.hasOwnProperty(\"historico\")) {\n                $scope.historico.init(\"historico\", r.historico);\n            }\n        }\n    };\n\n    $scope.printDetails = function (r, b) {\n        b();\n        if (r) {\n            if (r.hasOwnProperty(\"totales\")) {\n                $scope.totales = r.totales;\n            }\n            if (r.hasOwnProperty(\"data\")) {\n                $scope.data = r.data;\n            }\n            if ($scope.start) {\n                $scope.start = false;\n                setInterval(function () {\n                    $scope.init();\n                }, 60000 * 5);\n            }\n        }\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/mermahistorico.js?");

/***/ })

/******/ });