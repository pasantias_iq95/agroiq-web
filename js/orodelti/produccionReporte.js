/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/orodelti/produccionReporte.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/orodelti/produccionReporte.js":
/*!**************************************************!*\
  !*** ./js_modules/orodelti/produccionReporte.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n\n    this.data = function (callback, params) {\n        var url = 'phrapi/orodelti/produccionreporte/data';\n        load.block();\n        $http.post(url, params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    this.getWeeks = function (callback, params) {\n        var url = 'phrapi/orodelti/produccionreporte/weeks';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.last = function (callback, params) {\n        var url = 'phrapi/orodelti/produccionreporte/last';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {};\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.year = '';\n            $scope.semana = '';\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.filters.fecha_final;\n            getData();\n        }\n    };\n\n    $scope.changeYear = function () {\n        if ($scope.filters.year != '') {\n            $request.getWeeks(function (r) {\n                $scope.semanas = r.semanas;\n                if (r.semanas.indexOf($scope.filters.semana) == -1) {\n                    $scope.filters.semana = '';\n                }\n            }, {\n                year: $scope.filters.year\n            });\n        }\n    };\n    $scope.changeWeek = function () {\n        if ($scope.filters.semana != '' && $scope.filters.year != '') {\n            $scope.filters.fecha_inicial = '';\n            $scope.filters.fecha_final = '';\n            getData();\n        }\n    };\n\n    getData = function getData() {\n        $request.data(function (r) {\n            $scope.cintaSC = r.cintaSC || false;\n            $scope.data = r.data;\n            $scope.totales = r.totales;\n            $scope.edades = r.edades;\n            $scope.cintas = r.cintas;\n        }, $scope.filters);\n    };\n\n    $request.last(function (r) {\n        $scope.filters = {\n            fecha_inicial: r.fecha_inicial,\n            fecha_final: r.fecha_final,\n            year: '',\n            semana: '',\n            sector: ''\n        };\n        $scope.years = r.years;\n        $scope.semanas = r.weeks;\n        $scope.filters.year = r.last_year;\n        $scope.filters.semana = r.last_week;\n        getData();\n    });\n}]);\n\n//# sourceURL=webpack:///./js_modules/orodelti/produccionReporte.js?");

/***/ })

/******/ });