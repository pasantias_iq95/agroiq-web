/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/palmar/mermadia.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/palmar/mermadia.js":
/*!***************************************!*\
  !*** ./js_modules/palmar/mermadia.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\n/*----------  UTILIDADES SOBRE ARRAYS  ----------*/\napp.filter('unique', function () {\n    return function (collection, keyname) {\n        var output = [],\n            keys = [];\n\n        angular.forEach(collection, function (item) {\n            var key = item[keyname];\n            if (keys.indexOf(key) === -1) {\n                keys.push(key);\n                output.push(item);\n            }\n        });\n\n        return output;\n    };\n});\n\nfunction loadScript(options) {}\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            // console.log(item)\n            if (item.hasOwnProperty(\"lote\")) {\n                if (!isNaN(parseFloat(item.lote))) item.lote = parseFloat(item.lote);\n            }\n            if (item.hasOwnProperty(\"total_peso_merma\")) {\n                item.total_peso_merma = parseFloat(item.total_peso_merma);\n            }\n            if (item.hasOwnProperty(\"total_defectos\")) {\n                if (!isNaN(parseFloat(item.total_defectos))) item.total_defectos = parseFloat(item.total_defectos);\n            }\n            if (item.hasOwnProperty(\"merma\")) {\n                if (!isNaN(parseFloat(item.merma))) item.merma = parseFloat(item.merma);\n            }\n            if (item.hasOwnProperty(\"danhos_peso\")) {\n                if (!isNaN(parseFloat(item.danhos_peso))) item.danhos_peso = parseFloat(item.danhos_peso);\n            }\n            if (item.hasOwnProperty(\"filter\")) {\n                if (!isNaN(parseFloat(item.filter))) item.filter = parseFloat(item.filter);\n            }\n            if (item.hasOwnProperty(\"cantidad\")) {\n                if (!isNaN(parseFloat(item.cantidad))) item.cantidad = parseFloat(item.cantidad);\n            }\n\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            //alert(a[field]);\n            return a[field] > b[field] ? 1 : -1;\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.controller('informe_merma_dia', ['$scope', '$http', '$interval', 'client', '$controller', '$timeout', '$window', function ($scope, $http, $interval, client, $controller, $timeout, $window) {\n\n    $scope.leyendaGeneralTitle = 'Merma';\n\n    $scope.id_company = 0;\n\n    $scope.labelLbKg = \"Libras\";\n    $scope.statusLbKg = false;\n    $scope.labelPeso = \"\";\n    $scope.umbral_merma = Number(localStorage.getItem('umbral_merma') || 0);\n    $scope.umbral_lb = Number(localStorage.getItem('umbral_lb') || 0);\n    $scope.umbrales = {};\n\n    $scope.rangesDirectives = {\n        'Hoy': [moment(), moment()],\n        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],\n        'Hace 2 Días': [moment().subtract(2, 'days'), moment().subtract(2, 'days')]\n    };\n\n    $scope.limitDirectives = {\n        days: 0\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment(),\n        endDate: moment()\n    };\n    $scope.datesEnabled = [];\n    $scope.singleDatePickerDirective = true;\n\n    $scope.table = {\n        merma_widthCell: \"10%\",\n        peso_widthCell: \"10%\"\n    };\n\n    $scope.calidad = {\n        params: {\n            idFinca: \"\",\n            idFincaDia: \"\",\n            idMerma: \"MATERIA PRIMA\",\n            idLote: 0,\n            idLabor: 0,\n            fecha_inicial: moment().format('YYYY-MM-DD'),\n            fecha_final: moment().format('YYYY-MM-DD'),\n            cliente: \"\",\n            marca: \"\",\n            palanca: \"\",\n            statusLbKg: true,\n            categoria: \"COSECHA\"\n        },\n        step: 0,\n        path: ['phrapi/palmar/diamermaaaaa/index'],\n        templatePath: ['/views/palmar/templetes/mermadia/step1.html?' + Math.random()]\n    };\n\n    $scope.palancas = [];\n\n    $scope.mermas = {\n        NETA: \"Merma Neta\",\n        \"MATERIA PRIMA\": \"Merma Prima\"\n    };\n\n    $scope.cambiosMerma = function () {\n        $scope.calidad.params.palanca = \"\";\n        $scope.calidad.params.idFincaDia = \"\";\n        $scope.loadExternal();\n    };\n\n    $scope.loadExternal = function () {\n        if ($scope.calidad.path[$scope.calidad.step] != \"\") {\n            var data = $scope.calidad.params;\n            client.post($scope.calidad.path[$scope.calidad.step], $scope.startDetails, data);\n        }\n    };\n\n    $scope.last = function () {\n        client.post('phrapi/palmar/merma/last', function (r, b) {\n            b();\n            $scope.datesEnabled = r.days;\n            if (r.fecha) {\n                $scope.calidad.params.fecha_inicial = r.fecha;\n                $scope.calidad.params.fecha_final = r.fecha;\n                $(\"#date-picker\").html('' + r.fecha);\n            }\n            $scope.loadExternal();\n        });\n    };\n\n    $scope.cambiosCategoria = function () {\n        $scope.loadExternal();\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.calidad.params.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.wizardStep.params.fecha_inicial;\n            $scope.calidad.params.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.wizardStep.params.fecha_final;\n            $scope.loadExternal();\n        }\n    };\n\n    $scope.convertKg = function () {\n        $scope.statusLbKg = !$scope.statusLbKg;\n        if ($scope.statusLbKg === true) $scope.labelLbKg = \"Kilos\";else if ($scope.statusLbKg === false) $scope.labelLbKg = \"Libras\";\n\n        $scope.calidad.params.statusLbKg = $scope.statusLbKg;\n        $scope.loadExternal();\n    };\n\n    $scope.getKey = function (data, key) {\n        return Object.keys(data)[key];\n    };\n\n    $scope.saveUmbral = function (key, value) {\n        localStorage.setItem(key, value);\n    };\n\n    $scope.getUmbralMerma = function (value) {\n        if (value > 0) {\n            if (value > $scope.umbral_merma) return 'fa fa-arrow-up font-red-thunderbird';else value < $scope.umbral_merma;\n            return 'fa fa-arrow-down font-green-haze';\n        }\n        return 'fa fa-arrows-h font-yellow-gold';\n    };\n\n    $scope.getUmbralPeso = function (value) {\n        if (value > 0) {\n            if (value > $scope.umbral_lb) return 'fa fa-arrow-up font-red-thunderbird';else value < $scope.umbral_lb;\n            return 'fa fa-arrow-down font-green-haze';\n        }\n        return 'fa fa-arrows-h font-yellow-gold';\n    };\n\n    $scope.totalDefectos = {};\n\n    $scope.fincas = [];\n\n    $scope.cambiosFincas = function () {\n        $scope.calidad.params.palanca = \"\";\n        $scope.calidad.params.idFincaDia = \"\";\n        $scope.loadExternal();\n    };\n\n    $scope.startDetails = function (r, b) {\n        b();\n        if (r) {\n            $scope.numViajes = r.viajes;\n            $scope.merma = r.merma;\n            $scope.defectos = r.defectos;\n            $scope.pesos = r.pesos;\n        }\n    };\n}]);\n\n//# sourceURL=webpack:///./js_modules/palmar/mermadia.js?");

/***/ })

/******/ });