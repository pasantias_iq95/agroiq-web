/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/palmar/nuevoEnfunde.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/palmar/nuevoEnfunde.js":
/*!*******************************************!*\
  !*** ./js_modules/palmar/nuevoEnfunde.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (parseFloat(value[key])) sum = sum + parseFloat(value[key], 10);\n        });\n        return sum;\n    };\n});\n\napp.service('request', ['$http', function ($http) {\n    this.index = function (callback, params) {\n        var url = 'phrapi/sumifru/enfunde/edit/index';\n        var data = params || {};\n        $http.post(url, data).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.guardar = function (callback, params) {\n        var url = 'phrapi/sumifru/enfunde/edit/save';\n        var data = params || {};\n        $http.post(url, data).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.saldo = function (callback, params) {\n        var url = 'phrapi/sumifru/enfunde/edit/saldo';\n        var data = params || {};\n        $http.post(url, data).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.getColorCinta = function (callback, params) {\n        var url = 'phrapi/sumifru/enfunde/edit/cinta';\n        var data = params || {};\n        $http.post(url, data).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.controller('produccion', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.data = {\n        fecha_res: moment().format('YYYY-MM-DD'),\n        fecha: moment().format('YYYY-MM-DD'),\n        semana: moment().week(),\n        saldo_inicial: 0\n    };\n\n    printInit = function printInit(r) {\n        $scope.cinta = r.cinta;\n        $scope.enfundadores = r.enfundadores;\n        $scope.lotes = r.lotes;\n        $scope.data.saldo_inicial = parseInt(r.saldo_inicial);\n    };\n    printChangeFecha = function printChangeFecha(r) {\n        $scope.enfundadores = r.enfundadores;\n        $scope.lotes = r.lotes;\n        $scope.data.saldo_inicial = parseInt(r.saldo_inicial);\n    };\n    $scope.init = function () {\n        $request.index(printInit, $scope.data);\n    };\n\n    $scope.changeSaldo = function () {\n        $request.saldo(function (r) {\n            $scope.data.saldo_inicial = parseInt(r.saldo_inicial);\n        }, $scope.data);\n    };\n\n    $scope.changeFecha = function () {\n        if ($scope.data.fecha == \"\") {\n            $scope.data.fecha = angular.copy($scope.data.fecha_res);\n        } else {\n            $scope.data.fecha_res = angular.copy($scope.data.fecha);\n            $scope.data.semana = moment($scope.data.fecha_res).week();\n            $request.getColorCinta(function (r) {\n                $scope.cinta = r.cinta;\n            }, { year: moment($scope.data.fecha_res).year(), semana: $scope.data.semana });\n            $request.index(printChangeFecha, $scope.data);\n        }\n    };\n\n    $scope.guardar = function () {\n        var cancel = false;\n        if (!$scope.data.enfundador > 0) {\n            cancel = true;\n            alert(\"Colocar un enfundador\");\n        }\n        if (!$scope.data.lote > 0) {\n            cancel = true;\n            alert(\"Colocar el lote\");\n        }\n        if (!$scope.data.usadas > 0) {\n            cancel = true;\n            alert(\"Colocar las fundas usadas\");\n        }\n        if (!cancel) {\n            var data = angular.copy($scope.data);\n            $request.guardar(function (r) {\n                if (r.status == 200) {\n                    alert(\"Se guardo con éxito\", \"\", \"success\", function () {\n                        window.location.href = \"produccionenfunde\";\n                    });\n                } else {\n                    alert(\"Ocurrio algun problema\");\n                }\n            }, data);\n        }\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/palmar/nuevoEnfunde.js?");

/***/ })

/******/ });