/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/perchas.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/perchas.js":
/*!*******************************!*\
  !*** ./js_modules/perchas.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n\n    this.data = function (params, callback) {\n        var url = 'phrapi/marcel/perchas/data';\n        var data = params || {};\n        $http.post(url, data).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.last = function (callback, params) {\n        var url = 'phrapi/marcel/perchas/last';\n        var data = params || {};\n        $http.post(url, data).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.filter('inArray', function () {\n    return function (data, key) {\n        var arr = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : [];\n\n        if (data && key) {\n            if (arr) return data.filter(function (row) {\n                return arr.includes(row[key]);\n            });else return data;\n        } else return [];\n    };\n});\n\napp.controller('perchas', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.logged = \"<?= Session::getInstance()->logged ?>\";\n\n    $scope.filters = {\n        fecha_inicial: moment().startOf('month').format('YYYY-MM-DD'),\n        fecha_final: moment().endOf('month').format('YYYY-MM-DD')\n    };\n\n    $scope.changeRangeDate = function (data) {\n        if (data) {\n            $scope.filters.fecha_inicial = data.hasOwnProperty(\"first_date\") ? data.first_date : $scope.filters.fecha_inicial;\n            $scope.filters.fecha_final = data.hasOwnProperty(\"second_date\") ? data.second_date : $scope.filters.fecha_final;\n\n            $scope.getDataTable();\n        }\n    };\n\n    $scope.init = function () {\n        $request.last(function (r) {\n            $scope.filters.fecha_inicial = r.fecha;\n            $scope.filters.fecha_final = r.fecha;\n\n            setTimeout(function () {\n                $(\"#date-picker\").html(r.fecha + ' - ' + r.fecha);\n                $scope.getDataTable();\n            }, 500);\n        });\n    };\n\n    $scope.getDataTable = function () {\n        var response = function response(r) {\n            $scope.data = r.data;\n            $scope.locales = r.locales;\n            $scope.clientes = r.clientes;\n\n            printTags(r.tags);\n            printGraficaPastelKilos(r.data.kilos_totales, r.data.kilos_destruidos);\n            printGraficaPastelFruta(r.data.porc_fruta_lancofruit, r.data.porc_fruta_terceros, r.data.porc_percha_vacia);\n            printGraficaPastelPerchas(r.data.total_perchas_frutas_verduras, r.data.total_perchas_solo_banano);\n            printGraficaBarraGrados(r.data.grado_1, r.data.grado_2, r.data.grado_3, r.data.grado_4, r.data.grado_5, r.data.grado_6);\n            printGraficaPastelCategorias(r.categorias.main);\n            printGraficaPastelDefectos(r.categorias.defectos, r.categorias.categorias);\n        };\n        $request.data($scope.filters, response);\n    };\n\n    $scope.filterLocales = function () {\n        var response = function response(r) {\n            $scope.data = r.data;\n\n            printTags(r.tags);\n            printGraficaPastelKilos(r.data.kilos_totales, r.data.kilos_destruidos);\n            printGraficaPastelFruta(r.data.porc_fruta_lancofruit, r.data.porc_fruta_terceros, r.data.porc_percha_vacia);\n            printGraficaPastelPerchas(r.data.total_perchas_frutas_verduras, r.data.total_perchas_solo_banano);\n            printGraficaBarraGrados(r.data.grado_1, r.data.grado_2, r.data.grado_3, r.data.grado_4, r.data.grado_5, r.data.grado_6);\n            printGraficaPastelCategorias(r.categorias.main);\n            printGraficaPastelDefectos(r.categorias.defectos, r.categorias.categorias);\n        };\n        $request.data($scope.filters, response);\n    };\n\n    printGraficaPastelCategorias = function printGraficaPastelCategorias(data) {\n        var data = {\n            data: data || [],\n            id: \"pastel-categorias\",\n            height: \"charts-400\",\n            nameseries: \"categorias\"\n        };\n\n        var parent = $(\"#pastel-categorias\").parent();\n        $(\"#pastel-categorias\").remove();\n        parent.append('<div id=\"pastel-categorias\" class=\"charts\"></div>');\n        ReactDOM.render(React.createElement(Pastel, data), document.getElementById('pastel-categorias'));\n    };\n\n    printGraficaPastelDefectos = function printGraficaPastelDefectos(data, categorias) {\n        $scope.categorias = categorias;\n        // la categoria seleccionada existe\n        if (categorias.indexOf($scope.filters.categoria) == -1) {\n            $scope.filters.categoria = categorias[0];\n        }\n        $scope.data_defectos = data;\n\n        $scope.reloadGraficaDefectos();\n    };\n\n    $scope.reloadGraficaDefectos = function () {\n        var d = $scope.data_defectos[$scope.filters.categoria];\n        var options = {\n            data: d,\n            id: \"pastel-defectos\",\n            height: \"charts-400\",\n            nameseries: \"defectos\"\n        };\n\n        var parent = $(\"#pastel-defectos\").parent();\n        $(\"#pastel-defectos\").remove();\n        parent.append('<div id=\"pastel-defectos\" class=\"charts\"></div>');\n        ReactDOM.render(React.createElement(Pastel, options), document.getElementById('pastel-defectos'));\n    };\n\n    printTags = function printTags(tags) {\n        var options = {\n            colums: 3,\n            tags: tags,\n            withTheresholds: false\n        };\n        ReactDOM.render(React.createElement(ListTags, options), document.getElementById('indicadores'));\n    };\n\n    printGraficaPastelKilos = function printGraficaPastelKilos(kilosTotales, kilosDestruidos) {\n        var options = {\n            data: [{\n                label: 'KILOS TOTALES',\n                value: kilosTotales || 0\n            }, {\n                label: 'KILOS DESTRUIDOS',\n                value: kilosDestruidos || 0\n            }],\n            id: \"pastel-kilos\",\n            height: \"charts-400\",\n            nameseries: \"kilos\"\n        };\n        var parent = $(\"#pastel-kilos\").parent();\n        $(\"#pastel-kilos\").remove();\n        parent.append('<div id=\"pastel-kilos\" class=\"charts\"></div>');\n        ReactDOM.render(React.createElement(Pastel, options), document.getElementById('pastel-kilos'));\n    };\n\n    printGraficaPastelPerchas = function printGraficaPastelPerchas(frutasVerduras, banano) {\n        var options = {\n            data: [{\n                label: 'PERCHAS FRUTAS Y VERDURAS',\n                value: frutasVerduras || 0\n            }, {\n                label: 'PERCHAS SOLO BANANO',\n                value: banano || 0\n            }],\n            id: \"pastel-perchas\",\n            height: \"charts-400\",\n            nameseries: \"perchas\"\n        };\n        var parent = $(\"#pastel-perchas\").parent();\n        $(\"#pastel-perchas\").remove();\n        parent.append('<div id=\"pastel-perchas\" class=\"charts\"></div>');\n        ReactDOM.render(React.createElement(Pastel, options), document.getElementById('pastel-perchas'));\n    };\n\n    printGraficaPastelFruta = function printGraficaPastelFruta(lancofruit, terceros, vacia) {\n        var options = {\n            data: [{\n                label: '% Fruta Lancofruit',\n                value: lancofruit || 0\n            }, {\n                label: '% Fruta Terceros',\n                value: terceros || 0\n            }, {\n                label: '% Percha vacía',\n                value: vacia || 0\n            }],\n            id: \"pastel-fruta\",\n            height: \"charts-400\",\n            nameseries: \"fruta\"\n        };\n        var parent = $(\"#pastel-fruta\").parent();\n        $(\"#pastel-fruta\").remove();\n        parent.append('<div id=\"pastel-fruta\" class=\"charts\"></div>');\n        ReactDOM.render(React.createElement(Pastel, options), document.getElementById('pastel-fruta'));\n    };\n\n    printGraficaBarraGrados = function printGraficaBarraGrados(grado1, grado2, grado3, grado4, grado5, grado6) {\n        var options = {\n            data: [{ label: 'GRADO 1', value: grado1 }, { label: 'GRADO 2', value: grado2 }, { label: 'GRADO 3', value: grado3 }, { label: 'GRADO 4', value: grado4 }, { label: 'GRADO 5', value: grado5 }, { label: 'GRADO 6', value: grado6 }],\n            umbral: 0,\n            height: \"charts-400\",\n            id: \"barras-grados\"\n        };\n        var parent = $(\"#barras-grados\").parent();\n        $(\"#barras-grados\").remove();\n        parent.append('<div id=\"barras-grados\" class=\"charts\"></div>');\n        ReactDOM.render(React.createElement(Barras, options), document.getElementById('barras-grados'));\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/perchas.js?");

/***/ })

/******/ });