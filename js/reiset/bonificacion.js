/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/reiset/bonificacion.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/reiset/bonificacion.js":
/*!*******************************************!*\
  !*** ./js_modules/reiset/bonificacion.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.filter('order', function () {\n    return function (items) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (parseFloat(a) && parseFloat(b)) {\n                return parseFloat(a) > parseFloat(b) ? 1 : -1;\n            }\n            return a > b ? 1 : -1;\n        });\n        return filtered;\n    };\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            }\n            return a[field] > b[field] ? 1 : -1;\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.filter('avgOfArray', function () {\n    return function (data) {\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value != \"\" && value != undefined && parseFloat(value)) {\n                sum = sum + parseFloat(value, 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        return sum;\n    };\n});\n\napp.filter('avgOfObject', function () {\n    return function (data, field) {\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[field] != \"\" && value[field] != undefined && parseFloat(value[field])) {\n                sum = sum + parseFloat(value[field], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        return sum;\n    };\n});\n\napp.filter('avgOfArray', function () {\n    return function (data) {\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value != \"\" && value != undefined && parseFloat(value)) {\n                sum = sum + parseFloat(value, 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        return sum;\n    };\n});\n\napp.controller('bonificacion', ['$scope', '$http', function ($scope, $http) {\n\n    $scope.bonificacion = {\n        params: {\n            variable: 'Daños',\n            labor: 'ENFUNDE',\n            semana: moment().week(),\n            idFinca: 'GUARUMAL A',\n            year: '' + moment().year()\n        },\n        historica: {\n            orderByField: 'semana',\n            reverseSort: false\n        }\n    };\n\n    $scope.checkUmbralHistorica = function (value) {\n        if (parseFloat(value) >= 0) {\n            var val = parseFloat(value);\n            if (val == $scope.umbral) {\n                return 'bg-yellow-gold bg-font-yellow-gold';\n            } else if (val < $scope.umbral) {\n                return 'bg-green-haze bg-font-green-haze';\n            } else {\n                return 'bg-red-thunderbird bg-font-red-thunderbird';\n            }\n        }\n        return '';\n    };\n\n    $scope.tagsFlags = function (umbral, value) {\n        var className = \"fa fa-arrow-up font-red-thunderbird\";\n        if (!isNaN(parseFloat(value))) {\n            if (value <= (umbral || 0)) {\n                className = \"fa fa-arrow-down font-green-jungle\";\n            }\n        } else {\n            className = \"\";\n        }\n        return className;\n    };\n\n    $scope.getFilters = function () {\n        $http.post('phrapi/reiset/bonif/filters', $scope.bonificacion.params).then(function (r) {\n            $scope.fincas = r.data.fincas;\n            $scope.semanas_historico = r.data.semanas;\n            $scope.umbral = parseFloat(r.data.umbrals) || 10;\n        });\n    };\n\n    $scope.getTablaDiarioLotes = function () {\n        $http.post('phrapi/reiset/bonif/diario', $scope.bonificacion.params).then(function (r) {\n            $scope.lotes = r.data.data;\n            $scope.dias = r.data.dias;\n            $scope.totales = r.data.totales;\n        });\n    };\n\n    $scope.getTablaHistoricaSemanal = function (orderBy) {\n        load.block('table_historico');\n        load.block('historico');\n\n        var data = angular.copy($scope.bonificacion.params);\n        data.order = orderBy;\n        $http.post('phrapi/reiset/bonif/semanal', data).then(function (r) {\n            load.unblock('table_historico');\n            load.unblock('historico');\n\n            if (r.data) {\n                $scope.historico_tabla_semana = r.data.data_semana;\n                $scope.historico_tabla = r.data.data;\n                Object.keys(r.data.totales).map(function (value, index) {\n                    r.data.totales[value].avg = r.data.totales[value].sum / r.data.totales[value].avg;\n                });\n                $scope.totales_historico = r.data.totales;\n                //$scope.lotes = r.data.lotes\n                $scope.grafica_historica = new echartsPlv();\n                $scope.grafica_historica.init('historico', r.data.chart);\n            }\n        });\n    };\n\n    $scope.openDetalle = function (semana) {\n        if (!semana.defectos_loaded && !semana.loading) {\n            semana.loading = true;\n            semana.defectos = [{ defecto: 'CARGANDO...' }];\n            load.block('sub_' + semana.semana);\n\n            var data = {\n                idFinca: $scope.bonificacion.params.idFinca,\n                labor: $scope.bonificacion.params.labor,\n                semana: semana.semana,\n                variable: $scope.bonificacion.params.variable,\n                year: $scope.bonificacion.params.year\n            };\n            $http.post('phrapi/reiset/bonif/detalle', data).then(function (r) {\n                load.unblock('sub_' + semana.semana);\n\n                semana.defectos = r.data.data;\n                semana.defectos_loaded = true;\n                semana.loading = false;\n            });\n        } else if (!semana.defectos_loaded) {\n            toastr.warning('Por favor esperar ya casi terminamos');\n            return;\n        }\n        semana.expanded = !semana.expanded;\n    };\n\n    $scope.saveUmbral = function () {\n        var data = {\n            labor: $scope.bonificacion.params.labor,\n            variable: $scope.bonificacion.params.variable,\n            umbral: $scope.umbral\n        };\n        $http.post('phrapi/reiset/bonif/saveUmbral', data).then(function (r) {\n            if (r.data.status == 200) {\n                alert(\"Se modifico el umbral correctamente\", \"\", \"success\");\n            } else {\n                alert(\"Hubo un problema, intene mas tarde\");\n                $Scope.umbral = r.data.umbral;\n            }\n        });\n    };\n\n    $scope.reloadTable = function () {\n        $scope.getFilters();\n        $scope.getTablaDiarioLotes();\n    };\n\n    $scope.init = function () {\n        $scope.reloadTable();\n        $scope.getTablaHistoricaSemanal('semana');\n    };\n\n    $scope.exportExcel = function (id_table, title) {\n        var tableToExcel = function () {\n            var uri = 'data:application/vnd.ms-excel;base64,',\n                template = '<html xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv=\"content-type\" content=\"text/plain; charset=UTF-8\"/></head><body><table>{table}</table></body></html>',\n                base64 = function base64(s) {\n                return window.btoa(unescape(encodeURIComponent(s)));\n            },\n                format = function format(s, c) {\n                return s.replace(/{(\\w+)}/g, function (m, p) {\n                    return c[p];\n                });\n            };\n            return function (table, name) {\n                if (!table.nodeType) table = document.getElementById(table);\n                var contentTable = table.innerHTML;\n                var ctx = { worksheet: name || 'Worksheet', table: contentTable };\n                window.location.href = uri + base64(format(template, ctx));\n            };\n        }();\n        tableToExcel(id_table, title);\n    };\n\n    $scope.fnExcelReport = function (id_table, title) {\n        var data = new Blob([document.getElementById(id_table).outerHTML], {\n            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'\n        });\n        saveAs(data, 'Sample.xls');\n    };\n\n    $scope.exportPrint = function (id_table) {\n        var image = '<div style=\"display:block; height:55;\"><img style=\"float: right;\" width=\"100\" height=\"50\" src=\"./../logos/Logo.png\" /></div><br>';\n        var table = document.getElementById(id_table);\n        var contentTable = table.outerHTML;\n        // remove filters\n        var cut = contentTable.search('<tr role=\"row\" class=\"filter\">');\n        var cut2 = contentTable.search('</thead>');\n        var part1 = contentTable.substring(0, cut);\n        var part2 = contentTable.substring(cut2, contentTable.length);\n        // add image\n        contentTable = image + part1 + part2;\n\n        newWin = window.open(\"\");\n        newWin.document.write(contentTable);\n        newWin.print();\n        newWin.close();\n    };\n\n    $scope.init();\n}]);\n\n//# sourceURL=webpack:///./js_modules/reiset/bonificacion.js?");

/***/ })

/******/ });