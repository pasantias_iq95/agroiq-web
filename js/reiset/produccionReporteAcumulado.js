/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/reiset/produccionReporteAcumulado.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/reiset/produccionReporteAcumulado.js":
/*!*********************************************************!*\
  !*** ./js_modules/reiset/produccionReporteAcumulado.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.service('request', ['$http', function ($http) {\n\n    this.data2 = function (callback, params) {\n        var url = 'phrapi/reiset/produccionreporteacumulado/data2';\n        load.block();\n        $http.post(url, params || {}).then(function (r) {\n            load.unblock();\n            callback(r.data);\n        });\n    };\n\n    this.getWeeks = function (callback, params) {\n        var url = 'phrapi/reiset/produccionreporteacumulado/weeks';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.last = function (callback, params) {\n        var url = 'phrapi/reiset/produccionreporteacumulado/last';\n        $http.post(url, params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.filter('avgOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        var count = 0;\n        angular.forEach(data, function (value) {\n            if (value[key] != \"\" && value[key] != undefined) {\n                sum = sum + parseFloat(value[key], 10);\n                count++;\n            }\n        });\n        sum = sum / count;\n        if (isNaN(sum)) return 0;\n        return sum;\n    };\n});\n\napp.controller('controller', ['$scope', 'request', function ($scope, $request) {\n\n    $scope.filters = {\n        desde: { semana: 0, anio: 0 },\n        hasta: { semana: 0, anio: 0 }\n    };\n    $scope.semanas = [];\n\n    $scope.changeSector = function () {\n        getData();\n    };\n\n    $scope.changeRangeDate = function () {\n        setTimeout(function () {\n            var desde = $scope.filters.desde_concated.split('-');\n            var hasta = $scope.filters.hasta_concated.split('-');\n            var filters = {\n                desde_concated: $scope.filters.desde_concated,\n                hasta_concated: $scope.filters.hasta_concated,\n                desde: {\n                    anio: desde[0],\n                    semana: desde[1]\n                },\n                hasta: {\n                    anio: hasta[0],\n                    semana: hasta[1]\n                },\n                sector: $scope.filters.sector\n            };\n            $scope.filters = filters;\n            console.log($scope.filters);\n            getData(filters);\n        }, 200);\n    };\n\n    $scope.StartEndDateDirectives = {\n        startDate: moment().startOf('week'),\n        endDate: moment().endOf('week')\n    };\n\n    var renderReporte2 = function renderReporte2(data) {\n        var props = {\n            header: [{\n                key: 'lote',\n                name: 'LOTE',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                resizable: true\n            }, {\n                key: 'hectareas',\n                name: 'HA',\n                titleClass: 'text-center',\n                sortable: true,\n                alignContent: 'center',\n                resizable: true /*,{\n                                  key : 'enfunde',\n                                  name : 'TOT. ENF.',\n                                  sortable : true,\n                                  titleClass : 'text-center',\n                                  alignContent : 'center',\n                                  filterable : true,\n                                  filterRenderer: 'NumericFilter',\n                                  resizable : true,\n                                },{\n                                   key : 'enfunde_ha',\n                                   name : 'TOT. ENFU. / HA',\n                                   sortable : true,\n                                   titleClass : 'text-center',\n                                   alignContent : 'center',\n                                   filterable : true,\n                                   filterRenderer: 'NumericFilter',\n                                   resizable : true,\n                                   width : 150\n                                }*/ }, {\n                key: 'peso_prom_racimo',\n                name: 'PESO PROM.',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'manos',\n                name: 'NO. MANOS',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'calibre',\n                name: 'CALIB.',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }, /*{\n                key : 'dedos',\n                name : 'L. DEDOS',\n                sortable : true,\n                titleClass : 'text-center',\n                alignContent : 'center',\n                filterable : true,\n                filterRenderer: 'NumericFilter',\n                resizable : true,\n               },*/{\n                key: 'merma_total',\n                name: 'MERMA TOT.',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'racimo_ha',\n                name: 'RAC. /HA',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }, {\n                key: 'porc_racimos_recu',\n                name: '% RAC. RECH.',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'racimos_recu_ha',\n                name: 'RAC. RECH. /HA',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 120\n            }, {\n                key: 'racimos_proc_ha',\n                name: 'RAC. PROC. /HA',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 120\n            }, {\n                key: 'cajas_ha',\n                name: 'CAJAS /HA',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true\n            }, {\n                key: 'ratio_cortado',\n                name: 'RATIO CORTADO',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }, {\n                key: 'cajas_ha_proyeccion',\n                name: 'CAJAS /HA PROYEC',\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                resizable: true,\n                width: 100\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    $scope.table1.exportToExcel();\n                },\n                className: ''\n            }]\n        };\n        $(\"#table-reporte\").html(\"\");\n        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-reporte'));\n    };\n\n    var getData = function getData() {\n        var filters = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : $scope.filters;\n\n        $request.data2(function (r) {\n            renderReporte2(r.data);\n        }, filters);\n    };\n\n    $request.last(function (r) {\n        $scope.semanas = r.weeks;\n        $scope.sectores = r.sectores;\n        $scope.filters.desde_concated = r.weeks[0].concated;\n        $scope.filters.hasta_concated = r.weeks[0].concated;\n        $scope.filters.desde = r.weeks[0];\n        $scope.filters.hasta = r.weeks[0];\n        setTimeout(function () {\n            $(\"#date-picker\").html(r.last_date + ' - ' + r.last_date);\n        }, 200);\n        getData();\n    });\n}]);\n\n//# sourceURL=webpack:///./js_modules/reiset/produccionReporteAcumulado.js?");

/***/ })

/******/ });