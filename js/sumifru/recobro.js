/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js_modules/sumifru/recobro.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js_modules/sumifru/recobro.js":
/*!***************************************!*\
  !*** ./js_modules/sumifru/recobro.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\napp.filter('sumOfValue', function () {\n    return function (data, key) {\n        if (angular.isUndefined(data) || angular.isUndefined(key)) return 0;\n        var sum = 0;\n        angular.forEach(data, function (value) {\n            if (parseFloat(value[key])) sum = sum + parseFloat(value[key], 10);\n        });\n        return sum;\n    };\n});\n\napp.filter('orderObjectBy', function () {\n    return function (items, field, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (field == 'hora' || field == 'fecha') {\n                return moment(a.date).isAfter(b.date) ? 1 : -1;\n            } else if (parseFloat(a[field]) && parseFloat(b[field])) {\n                return parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1;\n            } else {\n                return a[field] > b[field] ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.filter('orderBy', function () {\n    return function (items, reverse) {\n        var filtered = [];\n        angular.forEach(items, function (item) {\n            if (!isNaN(parseInt(item))) {\n                item = parseInt(item);\n            }\n            filtered.push(item);\n        });\n        filtered.sort(function (a, b) {\n            if (parseFloat(a) && parseFloat(b)) {\n                return parseFloat(a) > parseFloat(b) ? 1 : -1;\n            } else {\n                return a > b ? 1 : -1;\n            }\n        });\n        if (reverse) filtered.reverse();\n        return filtered;\n    };\n});\n\napp.service('request', ['$http', function ($http) {\n    this.getData = function (callback, params) {\n        var url = 'phrapi/sumifru/recobro/data';\n        var data = params || {};\n\n        load.block('data-table');\n        $http.post(url, data).then(function (r) {\n            callback(r.data);\n            load.unblock('data-table');\n        }).catch(function () {\n            load.unblock('data-table');\n        });\n    };\n\n    this.index = function (callback, params) {\n        $http.post('phrapi/sumifru/resumenenfunde/index', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n\n    this.last = function (callback, params) {\n        $http.post('phrapi/sumifru/resumenenfunde/last', params || {}).then(function (r) {\n            callback(r.data);\n        });\n    };\n}]);\n\napp.controller('produccion', ['$scope', 'request', '$filter', function ($scope, $request, $filter) {\n\n    $scope.totales = {};\n    $scope.filters = {\n        year: '',\n        week: '',\n        id_finca: ''\n    };\n\n    $scope.init = function () {\n        load.block('saldo_fundas');\n        $request.index(function (r) {\n            load.unblock('saldo_fundas');\n            $scope.colorClass = r.color.class;\n            $scope.edades = r.edades;\n            $scope.data = r.data;\n            $scope.reloadTotales();\n        }, $scope.filters);\n    };\n\n    var getUmbral = function getUmbral(value) {\n        if (value) {\n            if (value < 98) return 'bg-red-thunderbird bg-font-red-thunderbird';else if (value == 98) return 'bg-yellow-gold bg-font-yellow-gold';else return 'bg-green-haze bg-font-green-haze';\n        }\n    };\n    $scope.getUmbral = getUmbral;\n\n    $scope.getDataTable = function () {\n        $request.getData(function (r) {\n            $scope.lotes = r.lotes;\n\n            r.data.map(function (row) {\n                Object.keys(row).map(function (col) {\n                    if (col != 'sem_enf') if (col.includes('sem_')) {\n                        if (row[col] == 0) row[col] = '';\n                    }\n                });\n            });\n\n            initTable(r.data, r.semanas_edad);\n        }, $scope.filters);\n    };\n\n    var initTable = function initTable(data, semanas) {\n        var props = {\n            header: [{\n                key: 'sem_enf',\n                name: 'SEM/ENF',\n                titleClass: 'text-center',\n                alignContent: 'center',\n                locked: true,\n                expandable: true,\n                resizable: true,\n                width: 100,\n                customCell: function customCell(rowData) {\n                    var valueCell = rowData['sem_enf'];\n                    return '\\n                            <div class=\"text-center ' + rowData['class'] + '\" style=\"height: 100%; padding: 5px;\">\\n                                ' + valueCell + '\\n                            </div>\\n                        ';\n                }\n            }, {\n                key: 'enfunde',\n                name: 'RAC ENF',\n                locked: true,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                filterable: true,\n                filterRenderer: 'NumericFilter',\n                formatter: 'Number',\n                resizable: true\n            }],\n            data: data,\n            buttons: [{\n                title: 'Excel',\n                action: function action() {\n                    $scope.table1.exportToExcel();\n                },\n                className: ''\n            }]\n        };\n        semanas.map(function (sem) {\n            props.header.push({\n                key: 'sem_' + sem,\n                name: '' + sem,\n                sortable: true,\n                titleClass: 'text-center',\n                alignContent: 'center',\n                //filterable : true,\n                filterRenderer: 'NumericFilter',\n                formatter: 'Number',\n                resizable: true,\n                width: 50\n                /*customCell : function(rowData){\n                    let valueCell = parseInt(rowData[`sem_${sem}`])\n                    let textCell = (valueCell > 0) ? valueCell : ''\n                    return `\n                        <div class=\"text-center\" style=\"height: 100%;\">\n                            ${textCell}\n                        </div>\n                    `;\n                }*/\n            });\n        });\n        props.header.push({\n            key: 'total',\n            name: 'TOTAL COSE',\n            sortable: true,\n            titleClass: 'text-center',\n            alignContent: 'center',\n            filterable: true,\n            filterRenderer: 'NumericFilter',\n            formatter: 'Number',\n            resizable: true\n        });\n        props.header.push({\n            key: 'saldo',\n            name: 'SALDO',\n            sortable: true,\n            titleClass: 'text-center',\n            alignContent: 'center',\n            filterable: true,\n            filterRenderer: 'NumericFilter',\n            formatter: 'Number',\n            resizable: true\n        });\n        props.header.push({\n            key: 'rec',\n            name: '% RECOBRO',\n            sortable: true,\n            titleClass: 'text-center',\n            alignContent: 'center',\n            filterable: true,\n            filterRenderer: 'NumericFilter',\n            formatter: 'Number',\n            resizable: true,\n            customCell: function customCell(rowData) {\n                var valueCell = parseFloat(rowData['rec']);\n                var textCell = valueCell > 0 ? valueCell : '';\n                var umbral = getUmbral(valueCell);\n                return '\\n                    <div class=\"text-center ' + umbral + '\" style=\"height: 100%; padding: 5px;\">\\n                        ' + textCell + '\\n                    </div>\\n                ';\n            }\n        });\n        props.header.push({\n            key: 'cintas_caidas',\n            name: 'CINTAS CAIDAS',\n            sortable: true,\n            titleClass: 'text-center',\n            alignContent: 'center',\n            filterable: true,\n            filterRenderer: 'NumericFilter',\n            formatter: 'Number',\n            resizable: true\n        });\n        props.header.push({\n            key: 'porc_cintas_caidas',\n            name: '% CAIDAS',\n            sortable: true,\n            titleClass: 'text-center',\n            alignContent: 'center',\n            filterable: true,\n            filterRenderer: 'NumericFilter',\n            formatter: 'Number',\n            resizable: true\n        });\n        props.header.push({\n            key: 'porc_total',\n            name: '% TOTAL',\n            sortable: true,\n            titleClass: 'text-center',\n            alignContent: 'center',\n            filterable: true,\n            filterRenderer: 'NumericFilter',\n            formatter: 'Number',\n            resizable: true\n        });\n        props.header.push({\n            key: 'no_recuperable',\n            name: 'NO RECUP',\n            sortable: true,\n            titleClass: 'text-center',\n            alignContent: 'center',\n            filterable: true,\n            filterRenderer: 'NumericFilter',\n            formatter: 'Number',\n            resizable: true\n        });\n        $(\"#data-table\").html(\"\");\n        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('data-table'));\n    };\n\n    $scope.reloadTotales = function () {\n        setTimeout(function () {\n            $scope.$apply(function () {\n                $scope.totales.racimos_enfunde = $filter('sumOfValue')($scope.data, 'racimos_enfunde');\n                $scope.totales.recobro = $scope.totales.cosechados > 0 && $scope.totales.racimos_enfunde > 0 ? $scope.totales.cosechados / $scope.totales.racimos_enfunde * 100 : 0;\n                $scope.totales.cintas_caidas = $filter('sumOfValue')($scope.data, 'cintas_caidas') || 0;\n                $scope.totales.p_cintas_caidas = $scope.totales.cintas_caidas > 0 && $scope.totales.racimos_enfunde > 0 ? $scope.totales.cintas_caidas / $scope.totales.racimos_enfunde * 100 : 0;\n            });\n        }, 100);\n    };\n\n    $scope.last = function () {\n        $request.last(function (r) {\n            $scope.fincas = r.fincas;\n            $scope.years = r.years;\n            $scope.semanas = r.weeks;\n            if (r.last_year && r.last_week) {\n                $scope.filters.year = r.last_year;\n                $scope.filters.week = r.last_week;\n            }\n            $scope.init();\n        }, $scope.filters);\n    };\n\n    $scope.last();\n\n    $scope.getDataTable();\n}]);\n\n//# sourceURL=webpack:///./js_modules/sumifru/recobro.js?");

/***/ })

/******/ });