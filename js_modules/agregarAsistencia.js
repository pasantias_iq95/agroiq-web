app.controller('agregarAsistencia', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){

	$scope.data = {
		id : "<?= $_GET['id'] ?>",
		fecha : "",
		hora : "",
		responsable : "",
		trabajador : "",
		hacienda : "",
		lote : "",
		cedula : "",
		almuerzo : 0,
		observaciones : "",
		nombre_trabajador_nuevo : "",
		labores : [],
		cables : []
	};
	
	$scope.init = function(){
		const data = {
			id : $scope.data.id
		};
		client.post("phrapi/asistencia/index", function(r, b){
			b()
			if(r){
				if(r.hasOwnProperty("labores")){
					$scope.labores = r.labores;
				}
				if(r.hasOwnProperty("user_data")){
					$scope.data = r.user_data
				}
			}
		}, data)
	}

	$scope.saveDatos = function(){
		if($scope.data.fecha != "" && $scope.data.hora != "" && $scope.data.hacienda != "" && $scope.data.trabajador != "" && $scope.data.nombre_trabajador_nuevo != "" && $scope.data.lote != ""){
			$scope.data.labores = []
			$.each($(".labor"), function(key, value){
				if($(value).is(":checked")){
					$scope.data.labores.push($(value).attr("id").split("_")[1])
				}
			})
			client.post("phrapi/asistencia/save", function(r, b){
				b()
				if(r){
					window.location = "revisionAsistencia";
				}
			}, $scope.data)
		}else{
			alert("Formulario incompleto")
		}
	}

	$scope.init();
}]);