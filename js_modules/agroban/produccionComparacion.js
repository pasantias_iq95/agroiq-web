app.service('request', [ '$http', ($http) => {

    var service = {}

    service.graficaVariables = (callback, params) => {
        load.block('graficas')
        $http.post('phrapi/comparacion/graficaVariables', params || {}).then(r => {
            load.unblock('graficas')
            callback(r.data)
        })
    }

    service.laniado = (callback, params) => {
        load.block('laniado')
        $http.post('phrapi/marcel/produccionsemana/graficaVariables', params || {}).then(r => {
            load.unblock('laniado')
            callback(r.data)
        })
    }

    service.marun = (callback, params) => {
        load.block('marun')
        $http.post('phrapi/marun/produccionsemana/graficaVariables', params || {}).then(r => {
            load.unblock('marun')
            callback(r.data)
        })
    }

    service.reiset = (callback, params) => {
        load.block('reiset')
        $http.post('phrapi/reiset/produccionsemana/graficaVariables', params || {}).then(r => {
            load.unblock('reiset')
            callback(r.data)
        })
    }

    service.quintana = (callback, params) => {
        load.block('quintana')
        $http.post('phrapi/quintana/produccionsemana/graficaVariables', params || {}).then(r => {
            load.unblock('quintana')
            callback(r.data)
        })
    }

    return service
}])

let charts = {}

app.controller('produccion', ['$scope', 'request', function($scope, $request){
    $scope.variables = {
        cajas : [
            "CAJAS/HA"
        ],
        racimos : [
            "Calibre",
            "Edad",
            "Peso (lb)",
            "RAC PROC/HA",
            "RAC CORT/HA",
            "RATIO CORTADO",
            "RATIO PROCESADO",
            "MERMA CORTADA",
            "MERMA PROCESADA"
        ],
    };

    $scope.variables_correlacion = {
        cajas : [
            "Cajas"
        ],
        clima : [
            "Horas Luz (400)",
            "Humedad",
            "Lluvia",
            "Rad. Solar",
            "Temp Max.",
            "Temp Min.",
        ],
        merma : [
            "Merma Neta",
            "Tallo",
        ],
        racimos : [
            "Calibre",
            "Edad",
            "Manos",
            "Peso",
            "Racimos Procesados",
            "Racimos Cosechados",
            "Ratio Cortado",
            "Ratio Procesado"
        ],
        sigat : [
            "HT (0S)",
            "HT (11S)"
        ]
    };

    $scope.charts = {
        variables : new echartsPlv()
    }

    $scope.filters = {
        var1 : 'Peso (lb)',
        type1 : 'line'
    }

    $scope.filtersCorrelacion = {
        year : moment().year(),
        var1 : "Peso",
        var2 : "Calibre",
        type1 : "line",
        type2 : "line"
    }

    $scope.init = function(){
        $scope.initGraficaVariables()
        $scope.initCorrelacion()
    }

    $scope.initCorrelacion = () => {
        $scope.initLanido()
        $scope.initMarun()
        $scope.initReiset()
        $scope.initQuintana()
    }
 
    var data_variables = {}
    var printGraficaVariables = (data, id) => {
        let height = id != 'variables' ? 400 : 500
        let parent = $("#"+id).parent()
        $("#"+id).remove()
        parent.append(`<div id="${id}" style="height:${height}px;"></div>`)
        charts[id] = new echartsPlv().init(id, data)
    }

    $scope.initGraficaVariables = () => {
        $request.graficaVariables((r) => {
            data_variables.correlacion = r.data
            printGraficaVariables(r.data, 'variables')
        }, $scope.filters)
    }

    $scope.initLanido = () => {
        $request.laniado((r) => {
            data_variables.laniado = r.data
            printGraficaVariables(r.data, 'laniado')
        }, $scope.filtersCorrelacion)
    }
    $scope.initMarun = () => {
        $request.marun((r) => {
            data_variables.marun = r.data
            printGraficaVariables(r.data, 'marun')
        }, Object.assign({ idFinca : 1, finca : 1 }, $scope.filtersCorrelacion))
    }
    $scope.initReiset = () => {
        $request.reiset((r) => {
            data_variables.reiset = r.data
            printGraficaVariables(r.data, 'reiset')
        }, $scope.filtersCorrelacion)
    }
    $scope.initQuintana = () => {
        $request.quintana((r) => {
            data_variables.quintana = r.data
            printGraficaVariables(r.data, 'quintana')
        }, Object.assign({ idFinca : 2, finca : 2 }, $scope.filtersCorrelacion))
    }

    $scope.selected = (val) => {
        return [$scope.filters.var1, $scope.filters.var2].indexOf(val) > -1;
    }

    $scope.toggleLineBar = (num, type) => {
        if(num == 1){
            data_variables.laniado.series = $scope.toggle(data_variables.laniado, $scope.filtersCorrelacion.var1, type)
            data_variables.marun.series = $scope.toggle(data_variables.marun, $scope.filtersCorrelacion.var1, type)
            data_variables.reiset.series = $scope.toggle(data_variables.reiset, $scope.filtersCorrelacion.var1, type)
            data_variables.quintana.series = $scope.toggle(data_variables.quintana, $scope.filtersCorrelacion.var1, type)
        }

        if(num == 2){
            data_variables.laniado.series = $scope.toggle(data_variables.laniado, $scope.filtersCorrelacion.var2, type)
            data_variables.marun.series = $scope.toggle(data_variables.marun, $scope.filtersCorrelacion.var2, type)
            data_variables.reiset.series = $scope.toggle(data_variables.reiset, $scope.filtersCorrelacion.var2, type)
            data_variables.quintana.series = $scope.toggle(data_variables.quintana, $scope.filtersCorrelacion.var2, type)
        }

        printGraficaVariables(data_variables.laniado, 'laniado')
        printGraficaVariables(data_variables.marun, 'marun')
        printGraficaVariables(data_variables.reiset, 'reiset')
        printGraficaVariables(data_variables.quintana, 'quintana')
    }

    $scope.toggle = (data, variable, type) => {
        let series = angular.copy(data.series)
        series.map((serie, index) => {
            if(serie.name.toUpperCase().includes(variable.toUpperCase())){
                series[index].type = type
                $scope.filters.type1 = type
            }
        })

        return series
    }

    $scope.resizeChart = () => {
        console.log("hola")
    }
}]);

function resizeChart(id){
    console.log("hola")
    charts[id].resize()
}
