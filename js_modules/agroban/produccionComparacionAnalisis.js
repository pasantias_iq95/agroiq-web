const responseHectareas = {
    "data": [
        {
            "name": "RACIMOS CORTADOS",
            "laniado": "2052.96",
            "marun": "1849.12",
            "reiset": "2322.32",
            "quintana": "2412.80",
            "agroban": 2159.3
        },
        {
            "name": "RACIMOS PROCESADOS",
            "laniado": "2046.20",
            "marun": "1824.68",
            "reiset": "2273.96",
            "quintana": "2390.96",
            "agroban": 2133.95
        },
        {
            "name": "RACIMOS RECUSADOS",
            "laniado": "6.76",
            "marun": "25.48",
            "reiset": "48.36",
            "quintana": "43.68",
            "agroban": 31.07
        },
        {
            "name": "CAJAS CONV",
            "laniado": "3623.88",
            "marun": "3254.68",
            "reiset": "3756.48",
            "quintana": "4456.92",
            "agroban": 3772.99
        },
        {
            "name": "ENFUNDE",
            "laniado": "2297.36",
            "marun": null,
            "reiset": "2551.64",
            "quintana": null,
            "agroban": 2424.5
        }
    ]
}

const responseSemanal = {
    "semanas": [
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28
    ],
    "data": [
        {
            "name": "PESO RACIMO PROM LB",
            "laniado": "83.04",
            "marun": "83.20",
            "reiset": "79.69",
            "quintana": "91.43",
            "agroban": 84.34
        },
        {
            "name": "EDAD PROM",
            "laniado": "12.99",
            "marun": "11.07",
            "reiset": "11.80",
            "quintana": "12.48",
            "agroban": 12.09
        },
        {
            "name": "% TALLO",
            "laniado": "9.86",
            "marun": "7.64",
            "reiset": "9.37",
            "quintana": "9.67",
            "agroban": 9.14
        },
        {
            "name": "% RECUSADOS",
            "laniado": "0.31",
            "marun": "1.21",
            "reiset": "2.07",
            "quintana": "0.92",
            "agroban": 1.13
        },
        {
            "name": "CALIBRE PROM",
            "laniado": "44.88",
            "marun": "44.93",
            "reiset": "44.54",
            "quintana": "44.79",
            "agroban": 44.79
        },
        {
            "name": "MANOS PROM",
            "laniado": "8.37",
            "marun": "5.62",
            "reiset": "9.53",
            "quintana": "9.62",
            "agroban": 8.29
        },
        {
            "name": "% MUESTREO",
            "laniado": "9.90",
            "marun": "32.50",
            "reiset": "38.00",
            "quintana": "100.00",
            "agroban": 45.1
        },
        {
            "name": "RATIO CORTADO",
            "laniado": "1.76",
            "marun": "1.94",
            "reiset": "1.56",
            "quintana": "1.84",
            "agroban": 1.78
        },
        {
            "name": "RATIO PROCESADO",
            "laniado": "1.77",
            "marun": "1.96",
            "reiset": "1.59",
            "quintana": "1.86",
            "agroban": 1.8
        },
        {
            "name": "% MERMA CORTADA",
            "laniado": "15.62",
            "marun": "19.46",
            "reiset": "17.03",
            "quintana": "19.32",
            "agroban": 17.86
        },
        {
            "name": "% MERMA PROCESADA",
            "laniado": 5.76,
            "marun": 11.82,
            "reiset": 7.66,
            "quintana": 9.65,
            "agroban": 8.72
        },
        {
            "name": "% MERMA NETA",
            "laniado": "1.16",
            "marun": "3.31",
            "reiset": "2.76",
            "quintana": "2.40",
            "agroban": 2.41
        },
        {
            "name": "RECOBRO",
            "laniado": "97.01",
            "marun": "",
            "reiset": "95.35",
            "quintana": "",
            "agroban": 96.18
        }
    ]
}

const responseHectareasSem = {
    "data": [
        {
            "name": "RACIMOS CORTADOS",
            "laniado": "39.48",
            "marun": "35.56",
            "reiset": "44.66",
            "quintana": "46.40",
            "agroban": 41.53
        },
        {
            "name": "RACIMOS PROCESADOS",
            "laniado": "39.35",
            "marun": "35.09",
            "reiset": "43.73",
            "quintana": "45.98",
            "agroban": 41.04
        },
        {
            "name": "RACIMOS RECUSADOS",
            "laniado": "0.13",
            "marun": "0.49",
            "reiset": "0.93",
            "quintana": "0.84",
            "agroban": 0.6
        },
        {
            "name": "CAJAS CONV",
            "laniado": "69.69",
            "marun": "62.59",
            "reiset": "72.24",
            "quintana": "85.71",
            "agroban": 72.56
        },
        {
            "name": "ENFUNDE",
            "laniado": "44.18",
            "marun": null,
            "reiset": "49.07",
            "quintana": null,
            "agroban": 46.63
        }
    ]
}

app.service('request', [ '$http', ($http) => {

    var service = {}

    service.porHectareas = (callback, params) => {
        if(params.por_hectareas == 'HA/ANIO') callback(responseHectareas)
        else if (params.por_hectareas == 'HA/SEM') callback(responseHectareasSem)
        else {
            load.block('porHectareas')
            $http.post('phrapi/comparacion/hectareas', params || {}).then(r => {
                load.unblock('porHectareas')
                callback(r.data)
            })
        }
    }

    service.semanal = (callback, params) => {
        if(params.por_semana == '') callback(responseSemanal)
        else {
            load.block('semanal')
            $http.post('phrapi/comparacion/semanal', params || {}).then(r => {
                load.unblock('semanal')
                callback(r.data)
            })
        }
    }

    return service
}])

function getOptionsGraficaReact(id, options, umbral){
    var newOptions = {
        series: options.series,
        legend: options.legends,
        umbral: umbral,
        id: id,
        type : 'bar',
        actions : false,
        zoom : false
    }
    return newOptions
}

function initGrafica(id, options, umbral){
    setTimeout(() => {
        let parent = $("#"+id).parent()
        $("#"+id).remove()
        parent.append(`<div id="${id}" style="height:400px"></div>`)
        var data = getOptionsGraficaReact(id, options, umbral)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
    }, 250)
}


const invertidos = ['RACIMOS RECUSADOS', 'EDAD PROM', '% RECUSADOS', '% MERMA CORTADA', '% MERMA PROCESADA']

app.controller('produccion', ['$scope', 'request', function($scope, $request){

    $scope.getUmbral = (umbral, value, name) => {
        let invertido = true
        if(invertidos.indexOf(name) > -1) invertido = false
        if(!invertido){
            if(value > 0 && umbral > 0){
                if(value <= umbral) return 'border-bottom-green'
                else return 'border-bottom-red'
            }
        }else{
            if(value > 0 && umbral > 0){
                if(value >= umbral) return 'border-bottom-green'
                else return 'border-bottom-red'
            }
        }
        return ''
    }

    $scope.getUmbralFont = (umbral, value, name) => {
        let invertido = true
        if(invertidos.indexOf(name) > -1) invertido = false
        if(!invertido){
            if(value > 0 && umbral > 0){
                if(value <= umbral) return 'text-green'
                else return 'text-red'
            }
        }else{
            if(value > 0 && umbral > 0){
                if(value >= umbral) return 'text-green'
                else return 'text-red'
            }
        }
        return ''
    }

    $scope.filters = {
        por_hectareas : 'HA/ANIO',
        por_semana : '',
        chart_por_hectareas : 'RACIMOS CORTADOS',
        chart_semanal : 'PESO RACIMO PROM LB'
    }
    $scope.por_hectareas= []
    $scope.init = () => {
        $scope.getPorHectareas()
        $scope.getSemanal()
    }

    $scope.getSemanal = () => {
        $request.semanal(printSemanal, $scope.filters)
    }
    $scope.getPorHectareas = () => {
        $request.porHectareas(printPorHectareas, $scope.filters)
    }
    $scope.chartPorHectareas = () => {
        let data = {}
        $scope.por_hectareas.map((s) => {
            if(s.name == $scope.filters.chart_por_hectareas) data = s
        })
        let series = {
                QUINTANA : {
                    name : 'LOS RIOS NORTE',
                    data : [data.quintana],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                LANIADO : {
                    name : 'EL ORO',
                    data : [data.laniado],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                MARUN : {
                    name : 'GUAYAS',
                    data : [data.marun],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                REISET : {
                    name : 'LOS RIOS SUR',
                    data : [data.reiset],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                }
            },
            legends = ['']
        initGrafica('chart-porHeactareas', { series, legends }, data.agroban)
    }
    $scope.chartSemanal = () => {
        let data = {}
        $scope.semanal.map((s) => {
            if(s.name == $scope.filters.chart_semanal) data = s
        })
        let series = {
                QUINTANA : {
                    name : 'LOS RIOS NORTE',
                    data : [data.quintana],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                LANIADO : {
                    name : 'EL ORO',
                    data : [data.laniado],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                MARUN : {
                    name : 'GUAYAS',
                    data : [data.marun],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                },
                REISET : {
                    name : 'LOS RIOS SUR',
                    data : [data.reiset],
                    connectNulls : true,
                    type : "bar",
                    itemStyle : {
                        normal : {
                            barBorderRadius : 0,
                            barBorderWidth : 6
                        }
                    },
                    label : {
                        normal : {
                            show : true,
                            position : "inside"
                        }
                    }
                }
            },
            legends = ['']
        initGrafica('chart-semanal', { series, legends }, data.agroban)
    }

    printPorHectareas = (r) => {
        $scope.por_hectareas = r.data
        $scope.chartPorHectareas()
    } 

    printSemanal = (r) => {
        $scope.semanal = r.data
        $scope.semanas = r.semanas
        $scope.chartSemanal()
    }

    $scope.init()
    
}]);
