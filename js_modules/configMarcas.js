var EnableEdit = 0;
grid.id = "#datatable_ajax";
grid.url = "phrapi/configuracion/marcas/list";
grid.addButtons({ extend: 'excel', "title" : "Marcas", "text" : "Excel" ,className: 'btn yellow btn-outline '});
grid.init();

var className = ".horas-";
var editEnable = function(actve){
	EnableEdit = (EnableEdit == 0) ? 1 : 0;
	var label = (EnableEdit == 0) ? 'Editar' : 'Guardar';
	$("#EnableEdit").val(EnableEdit);
	$("#editData").html(label);
	if(actve){
		grid.table.getDataTable().clearPipeline().draw();
		$("#cancelarData").css('display', '');
	}else{
		$("#cancelarData").css('display', 'none');
	}
}

$("table").on('change' , '.labor_change' , function(e){
		e.preventDefault();
		var id = this.id.split("-");
		console.log(id)
		if(this.value != "NUM"){
				$(className + id[1]).attr('readonly', 'readonly');
				$(className + id[1]).val(this.value);
		}else{
				$(className + id[1]).removeAttr('readonly');
				$(className + id[1]).val("");
		}
});

var save = function(){
	var data = $(".save").serializeObject();
	if(data){
		console.log(data)
		ahttp.post("phrapi/cambiar/revision/asistencia" , function(r,b){
			b()
			if(r && r.hasOwnProperty("success") && r.success == 200){
				editEnable(true);
			}else{
				console.log("Ocurrio un Error");
			}
		} , data);
	}
}


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}


var reload = function(r , b){
	b();
	if(r){
		console.log(r);
	}
}

var deleteRow = function(key){
	var res = confirm("Esta seguro de Eliminar el registro");
	if(res){
		if(key){

			var data = {
				id_row : key ,
				token : getRandomInt(1 , 99999)
			}

			ahttp.post("phrapi/drop/asistencia" , reload , data);
		}
	}
}

$("#editData").on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
	if($(this).html() == "Guardar"){
		save();
	}else{
		editEnable(true);
	}
});

$("#cancelarData").on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
	if(EnableEdit > 0){
			editEnable(false);
	}
});

$("table").on('change', '.date-picker', function(event) {
	event.preventDefault();
	/* Act on the event */
	$('.date-picker').datepicker({
		rtl: App.isRTL(),
		autoclose: true
	});
});


$(".filter-cancel").on('click', function(event) {
	event.preventDefault();
	/* Act on the event */
	if(EnableEdit > 0){
			editEnable(false);
	}
});

$(document).ready( function () {
    //var table = $('#asistencia').dataTable();
    //new $.fn.dataTable.FixedHeader( table );
} );