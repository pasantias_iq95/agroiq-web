app.controller('control', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){

	$scope.original = {
		id : "<?= $_GET['id'] ?>",
		defecto : "",
	}
	$scope.data = {
		id : "<?= $_GET['id'] ?>",
	}

	$scope.categorias = [];


	$scope.init = function(){
		client.post("phrapi/configuracion/get/defecto", function(r, b){
			b()
			console.log(r)
			if(r){
				if (r.hasOwnProperty("defecto")){
					$scope.original.defecto = r.defecto.nombre
					$scope.data = r.defecto;
					$scope.categorias = r.categorias;
				}
			}
		}, { id : $scope.original.id })
	}

	$scope.saveDatos = function(){
		if($scope.data.destino != ""){
			client.post("phrapi/configuracion/save/defecto", function(r, b){
				b()
				if(r){
					alert("Formulario Guardado", "Defectos", "success")
					window.location = "configDefectos";
				}
			}, $scope.data)
		}else{
			alert("Formulario incompleto")
		}
	}

	$scope.init();
}]);