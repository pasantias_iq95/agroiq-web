grid.id = "#fincas";
grid.url = "phrapi/Fincas/index";
grid.addButtons({ extend: 'print', className: 'btn dark btn-outline', "title" : "Listado de Fincas", "text" : "Imprimir",
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10pt' )
                            .css( 'float', 'rigth' )
                            .prepend(
                                '<img src="http://orodelti.procesos-iq.com/Logo.png" />'
                            );
     
                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' );
                    }
                });
grid.addButtons({ extend: 'print', className: 'btn dark btn-outline', "title" : "Listado de Fincas", "text" : "Imprimir <i class='fa fa-check' aria-hidden='true'></i>",
                    customize: function ( win ) {
                        $(win.document.body)
                            .css( 'font-size', '10pt' )
                            .css( 'float', 'rigth' )
                            .prepend(
                                '<img src="http://orodelti.procesos-iq.com/Logo.png" />'
                            );
     
                        $(win.document.body).find( 'table' )
                            .addClass( 'compact' )
                            .css( 'font-size', 'inherit' );
                    },
                    exportOptions: {
                        modifier: {
                            selected: true
                        }
                    }
                });
grid.addButtons({ extend: 'pdfHtml5',"text" : "PDF", "title" : "Listado de Fincas" ,className: 'btn green btn-outline',
                    customize: function ( doc ) {
                        var cols = [];
                           cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                           cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
                           var objFooter = {};
                           objFooter['columns'] = cols;
                           doc['footer']=objFooter;
                           doc.content.unshift(
                            {
                                margin: [0, 0, 0, 12],
                                alignment: 'left',
                                image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAoAIoDASIAAhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAAAAcFBgEECAID/8QAORAAAAUDAgMGBAQEBwAAAAAAAQIDBAUABhESIQcTMRQiIzJBUUJhcYEVUrHwCBaR0WJyc4KhsuH/xAAZAQACAwEAAAAAAAAAAAAAAAAAAwECBAX/xAAtEQABBAECAwYGAwAAAAAAAAABAAIDERIEIRMxUSIyM0FxoQUjNFKBwWFikf/aAAwDAQACEQMRAD8A6BvK6W1sxwLLEOs4ObSmiX/sYfhIH5vt1pLznG5RBUQapC/VDrhTlIF+mMib99aiOO8msM2JuYfnOtSRcD5W5TYAv+4dx+9aNpwMdAx0W/lmSD+ZlCdpaNXAakWzfoCxyfGJ/QvQA361WWThW0f7+lgkle5+LNlINeP88kqGuMiDp/lLzSj/AF1j+lNTh7xdhbtdEYrpHjZQ/kRVNqKr/lNtkf8ACIAPtmtyGixlYbD5Ji5QAu6CzRLl/wBNIVXILhNaZ55WaMCxWzc+r8OMfwkVQ31a+ok8ogHpvnIbBlinLzzTGsmaQbsJz0UrD8WklzLOIS2p2Vh0DiRWQbo+Ht5jEAfMAfapiV4kRMcjCmKxlnR5dIyzZFu0EyukMZyURAc71ryCfxW9Ve6KpKvEOLawKkrJM5WNTBfsyaDtmZNZc4hkCpp7ibP96925f0dNy4RZ2UrFSRkxVSbSTUUTKkDqYm4gbFGQU5turVzoqixvEiIfXia2TNZFnJhr0g7RKmBsb7d4R3DcNtwq9UA2pDg7ks0ViipVlmiisUIWaKKKEIrGKzRQhJPifakc7kWbiRZgsPIMkUwmOXoIj8Ih71S+KiJ457bs61TH8JPHJR+oO8CCqWvwx/LkB298GroW44dKajjtz9xTzJqflN/7+lLJcj6C7QxkWaSzNx3VUHKetFcv6fsK4mqe/Tzue+yx3smGBk0eLaDlV4XiByGGgFQxp96u1pIrTNnTPa3AMxnSnbMhVHTq7hg1B9RH7gGQ9KriMdarRYHDW02JFw7wc1dVVLV/pmHFWSHbzFwPuecR7KbSU5lC+FoD4QL8vl065AaTFqY2vAitxKqzSSAXKaUVaXEBOz7YZ25PW9Mozcel2YrZu0MoVyIdDJmDYc+/vnGaj+JTZ5IXTaM85YXazSVbK9pRjtSizI2nu6dGQIJhHf3AAyACAhTlJCkKAAV7IFKHQO0GqDvCRj7ThFJSWfSvJIYqZSJKajKHHoUOn/OPWu0BI6m47rO6MBtOdsFSXh3Llra09FRVxSKNuuVUnDWTRN21cihA8YgD5zFz9+lfKPkbil71LJRbi6kbfQOd29SfsypE0gXIIIk06ziI/v32ojiRGu5KOaSLO5Y5OR09lcuDeGrq2KbJfQcl7wZDcM7b1u3ZfkVCTZoZkE/LyxPOiyPq07Z0j88ewDj1xTzp57xwSs4e9nsl2vbV6vWLm9CxCBJM0j+KJkUFUHxSJiJSoglo8mPhzkQx9K6Mh3gSUW1eclZDnplV5axBKdPIeUQHoIVQrUvOFuOFk3zVzLoqRyQqOWqygc0oAUR23wOdI+v1xUPBcVYKclGMfHpXF2p2pygIqYhdIAXOrumH99cUM084J7HLmpY+GPfPmnLRStgeIcHMW7NTBDTTdtFkKZci5iaz5zjTpMIZEQx1DetJxxWhGtvM5hZpcfZna6iKRB5Ws3LxqN58YyOOvXNX4ExNYJvHiq8k36KWN2cSIa2TMm6gSjySdJkUKzQEDKJlN01b4yP5QyP23rYsbiLDXSu7bJi9YPGxRUUQeCUvcDzGAc+nrnAhRwZQ3PHZAnjLsct0xqKTzrjRElVXNHxU2+jmxvHfIphyyh6G3HoPz05qyyfEeAY2c2uQFl12TkwJoppk8Uym+U8CIABgwbOR9PXbNjBKKtvNQNREbp3JXyildA8WWEjNMY59EykWd9oM2VcJhoUA3l9ehvQdw+lND7VSSN0ZpwV2SNkFtWa+ayRFkxIoQpyj1AwZCiiluFjdXWiSGjCH1Ej2hT+4Il/tUgAAAYAKKKhrGs7opSbXqlxxpW5Vq6F7cVnWKinjAisKZ24h5VA0lMP39PXYRoorRB4jfVI1HhlKPh+9lG18xbOx30y8gznS7Sg8J4aBBN4hR+HYPiAC5HYPnpyjJW1+I8yrcjiejW7lVYyL2KPyzKFMfWXvjsYMdS+ggGaKK7Z+oMXkR+Vxq+Tn/KnYOMRb2TeE9GsbiIo7ZmaAeSEhhc8w4ZMAEIAm6+bcNx+eLn/D7bbZraJZJ3Hpkkll1BKssl4hSB3AxqDIfF9cjRRWHUvIjeP7LXp2AyN9P2lA/gpz+aZi1Y1usRs/kylE/KNo0lOfQYRx0ADavsFW7jNBCnK2dbMU1XOzaoFSASpmMHiHAmoRLtkdGR+uaKK1OmcZGel/mlnawcN3rXui7ir2Zxs/mSYj3biJObWkqkTUGnk8sMemoo+g423qch3T+/mV1LsLUYxqbhmsk2fily13Jz7FKJsBnIdTbgA4oopT6OmbNXaFD3TGCpzF5FLCBO0jIh9Ezil3tpBRUxRjWAlTSXKIAHeIYB32N6DkMYpgTcUhb/Cpk0XtWYkmDxc7lQjhyVNywPpACGHQQcZD3DbODZziiin6jvsH3EEpUA7Dz0URwwfSyPEBixtB9MP7b1BzyvUjFTSSx3shuBRD0MGnI7bhmumcUUVg+Iipq6LfoN4r6r//2Q=='
                            }
                           );
                    }
                });
grid.addButtons({ extend: 'pdfHtml5',"text" : "PDF <i class='fa fa-check' aria-hidden='true'></i>", "title" : "Listado de Fincas" ,className: 'btn green btn-outline',
                    customize: function ( doc ) {
                        // Splice the image in after the header, but before the table
                        // doc.content.splice( 1, 0, {
                        //     margin: [ 0, 0, 0, 12 ],
                        //     alignment: 'center',
                            
                        // } );
                        // Data URL generated by http://dataurl.net/#dataurlmaker
                          var cols = [];
                           cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                           cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
                           var objFooter = {};
                           objFooter['columns'] = cols;
                           doc['footer']=objFooter;
                           doc.content.unshift(
                            {
                                margin: [0, 0, 0, 12],
                                alignment: 'left',
                                image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAoAIoDASIAAhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAAAAcFBgEECAID/8QAORAAAAUDAgMGBAQEBwAAAAAAAQIDBAUABhESIQcTMRQiIzJBUUJhcYEVUrHwCBaR0WJyc4KhsuH/xAAZAQACAwEAAAAAAAAAAAAAAAAAAwECBAX/xAAtEQABBAECAwYGAwAAAAAAAAABAAIDERIEIRMxUSIyM0FxoQUjNFKBwWFikf/aAAwDAQACEQMRAD8A6BvK6W1sxwLLEOs4ObSmiX/sYfhIH5vt1pLznG5RBUQapC/VDrhTlIF+mMib99aiOO8msM2JuYfnOtSRcD5W5TYAv+4dx+9aNpwMdAx0W/lmSD+ZlCdpaNXAakWzfoCxyfGJ/QvQA361WWThW0f7+lgkle5+LNlINeP88kqGuMiDp/lLzSj/AF1j+lNTh7xdhbtdEYrpHjZQ/kRVNqKr/lNtkf8ACIAPtmtyGixlYbD5Ji5QAu6CzRLl/wBNIVXILhNaZ55WaMCxWzc+r8OMfwkVQ31a+ok8ogHpvnIbBlinLzzTGsmaQbsJz0UrD8WklzLOIS2p2Vh0DiRWQbo+Ht5jEAfMAfapiV4kRMcjCmKxlnR5dIyzZFu0EyukMZyURAc71ryCfxW9Ve6KpKvEOLawKkrJM5WNTBfsyaDtmZNZc4hkCpp7ibP96925f0dNy4RZ2UrFSRkxVSbSTUUTKkDqYm4gbFGQU5turVzoqixvEiIfXia2TNZFnJhr0g7RKmBsb7d4R3DcNtwq9UA2pDg7ks0ViipVlmiisUIWaKKKEIrGKzRQhJPifakc7kWbiRZgsPIMkUwmOXoIj8Ih71S+KiJ457bs61TH8JPHJR+oO8CCqWvwx/LkB298GroW44dKajjtz9xTzJqflN/7+lLJcj6C7QxkWaSzNx3VUHKetFcv6fsK4mqe/Tzue+yx3smGBk0eLaDlV4XiByGGgFQxp96u1pIrTNnTPa3AMxnSnbMhVHTq7hg1B9RH7gGQ9KriMdarRYHDW02JFw7wc1dVVLV/pmHFWSHbzFwPuecR7KbSU5lC+FoD4QL8vl065AaTFqY2vAitxKqzSSAXKaUVaXEBOz7YZ25PW9Mozcel2YrZu0MoVyIdDJmDYc+/vnGaj+JTZ5IXTaM85YXazSVbK9pRjtSizI2nu6dGQIJhHf3AAyACAhTlJCkKAAV7IFKHQO0GqDvCRj7ThFJSWfSvJIYqZSJKajKHHoUOn/OPWu0BI6m47rO6MBtOdsFSXh3Llra09FRVxSKNuuVUnDWTRN21cihA8YgD5zFz9+lfKPkbil71LJRbi6kbfQOd29SfsypE0gXIIIk06ziI/v32ojiRGu5KOaSLO5Y5OR09lcuDeGrq2KbJfQcl7wZDcM7b1u3ZfkVCTZoZkE/LyxPOiyPq07Z0j88ewDj1xTzp57xwSs4e9nsl2vbV6vWLm9CxCBJM0j+KJkUFUHxSJiJSoglo8mPhzkQx9K6Mh3gSUW1eclZDnplV5axBKdPIeUQHoIVQrUvOFuOFk3zVzLoqRyQqOWqygc0oAUR23wOdI+v1xUPBcVYKclGMfHpXF2p2pygIqYhdIAXOrumH99cUM084J7HLmpY+GPfPmnLRStgeIcHMW7NTBDTTdtFkKZci5iaz5zjTpMIZEQx1DetJxxWhGtvM5hZpcfZna6iKRB5Ws3LxqN58YyOOvXNX4ExNYJvHiq8k36KWN2cSIa2TMm6gSjySdJkUKzQEDKJlN01b4yP5QyP23rYsbiLDXSu7bJi9YPGxRUUQeCUvcDzGAc+nrnAhRwZQ3PHZAnjLsct0xqKTzrjRElVXNHxU2+jmxvHfIphyyh6G3HoPz05qyyfEeAY2c2uQFl12TkwJoppk8Uym+U8CIABgwbOR9PXbNjBKKtvNQNREbp3JXyildA8WWEjNMY59EykWd9oM2VcJhoUA3l9ehvQdw+lND7VSSN0ZpwV2SNkFtWa+ayRFkxIoQpyj1AwZCiiluFjdXWiSGjCH1Ej2hT+4Il/tUgAAAYAKKKhrGs7opSbXqlxxpW5Vq6F7cVnWKinjAisKZ24h5VA0lMP39PXYRoorRB4jfVI1HhlKPh+9lG18xbOx30y8gznS7Sg8J4aBBN4hR+HYPiAC5HYPnpyjJW1+I8yrcjiejW7lVYyL2KPyzKFMfWXvjsYMdS+ggGaKK7Z+oMXkR+Vxq+Tn/KnYOMRb2TeE9GsbiIo7ZmaAeSEhhc8w4ZMAEIAm6+bcNx+eLn/D7bbZraJZJ3Hpkkll1BKssl4hSB3AxqDIfF9cjRRWHUvIjeP7LXp2AyN9P2lA/gpz+aZi1Y1usRs/kylE/KNo0lOfQYRx0ADavsFW7jNBCnK2dbMU1XOzaoFSASpmMHiHAmoRLtkdGR+uaKK1OmcZGel/mlnawcN3rXui7ir2Zxs/mSYj3biJObWkqkTUGnk8sMemoo+g423qch3T+/mV1LsLUYxqbhmsk2fily13Jz7FKJsBnIdTbgA4oopT6OmbNXaFD3TGCpzF5FLCBO0jIh9Ezil3tpBRUxRjWAlTSXKIAHeIYB32N6DkMYpgTcUhb/Cpk0XtWYkmDxc7lQjhyVNywPpACGHQQcZD3DbODZziiin6jvsH3EEpUA7Dz0URwwfSyPEBixtB9MP7b1BzyvUjFTSSx3shuBRD0MGnI7bhmumcUUVg+Iipq6LfoN4r6r//2Q=='
                            }
                           );
                           // console.log(doc.content);
                    },
                    exportOptions: {
                        modifier: {
                            selected: true
                        }
                    }
                });
grid.addButtons({ extend: 'excel', "title" : "Listado de Fincas", "text" : "Excel" ,className: 'btn yellow btn-outline ' });
grid.addButtons({ extend: 'csv', "title" : "Listado de Fincas" ,"text" : "CSV", className: 'btn purple btn-outline ' });
grid.addButtons({
                    text: 'Nueva Finca',
                    className: 'btn blue btn-outline',
                    action: function ( e, dt, node, config ) {
                        //dt.ajax.reload();
                        document.location.href = 'finca';
                    }
                });

grid.init();