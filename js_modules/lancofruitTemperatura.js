const { app } = window

app.service('temperatura',[ 'client' , function(client){
    this.tags = function(params , callback){
        var url = 'phrapi/temperatura/index'
        var data = params || {}
        client.post(url , callback , data );
    }
}])

app.controller('temperatura', ['$scope', '$http', '$location', 'temperatura', '$controller', '$window', function ($scope, $http, $location, temperatura, $controller, $window) {
    
    $scope.filters = {
        variable : 'max'
    }
    $scope.semanas = []
    $scope.data = {
        max : [],
        min : [],
        prom : []
    }
    $scope.service = {
        index : function(){
            var data = {} 
            temperatura.tags(data , $scope.print)
        },
        all : function(){
            this.index()
        }
    } 

    $scope.print = function(r , b){
        b()
        $scope.semanas = r.temp_maxima.legends
        setData('max', r.temp_maxima.series, r.temp_maxima.legends)
        setData('min', r.temp_minima.series, r.temp_minima.legends)
        setData('prom', r.temp_promedio.series, r.temp_promedio.legends)
        
        let optionsMax = {
            series: r.temp_maxima.series,
            legend: r.temp_maxima.legends,
            id: "temp_maxima",
            type : 'line',
            min : 'dataMin'
        }
        reRenderChart('temp_maxima', optionsMax)

        let optionsMin = {
            series: r.temp_minima.series,
            legend: r.temp_minima.legends,
            id: "temp_minima",
            type : 'line',
            min : 'dataMin'
        }
        reRenderChart('temp_minima', optionsMin)

        let optionsProm = {
            series: r.temp_promedio.series,
            legend: r.temp_promedio.legends,
            id: "temp_promedio",
            type : 'line',
            min : 'dataMin'
        }
        reRenderChart('temp_promedio', optionsProm)

        $scope.reRenderTable()
    }

    $scope.reRenderTable = () => {
        $("#table-react").html("")
        let props = {
            header : [
                {
                    key : 'estacion',
                    name : 'ESTACION',
                    titleClass : 'text-center',
                    locked : true,
                    resizable : true,
                    width: 170
                },
                {
                    key : 'prom',
                    name : 'PROM',
                    titleClass : 'text-center',
                    locked : true,
                    resizable : true
                },
                {
                    key : 'max',
                    name : 'MAX',
                    titleClass : 'text-center',
                    locked : true,
                    resizable : true
                },
                {
                    key : 'min',
                    name : 'MIN',
                    titleClass : 'text-center',
                    locked : true,
                    resizable : true
                }
            ],
            data : $scope.data[$scope.filters.variable],
            height : 200,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        for(let i in $scope.semanas){
            let sem = $scope.semanas[i]
            props.header.push({
                key : `sem_${sem}`,
                name : sem,
                titleClass : 'text-center',
                locked : true,
                resizable : true
            })
        }
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-react'))
    }

    const setData = (key, series, legends) => {
        let rows = Object.keys(series).map((key) => { return { estacion : key }})
        for(let i in rows){
            let serie = rows[i].estacion,
                data = []

            for(let j in legends){
                let sem = legends[j]
                data[j] = rows[i][`sem_${sem}`] = series[serie] && parseFloat(series[serie].data[j]) ? parseFloat(series[serie].data[j]) : ''
            }

            // MAX MIN PROM
            let max = null, min = null, sum = 0, count = 0, prom = null
            for(let i in data){
                let value = parseFloat(data[i])
                if(value > 0){
                    if(max == null || max < value) max = value
                    if(min == null || min > value) min = value
                    count++
                    sum += value
                }
            }
            prom = Math.round(sum/count*100)/100

            rows[i].max = max
            rows[i].min = min
            rows[i].prom = prom
        }

        $scope.data[key] = rows
    }

    const reRenderChart = (id, options) => {
        let parent = $(`#${id}`).parent()
        $(`#${id}`).remove()
        parent.append(`<div id="${id}" style="height:500px;"></div>`)
        ReactDOM.render(React.createElement(Historica, options), document.getElementById(id));
    }
}]);