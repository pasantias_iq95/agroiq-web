const { app, load } = window

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('sumOfValueDouble', function () {
    return function (data, key, key2) {        
        if (angular.isUndefined(data) || angular.isUndefined(key) || angular.isUndefined(key2))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key][key2] != "" && value[key][key2] != undefined && parseFloat(value[key][key2])){
                sum = sum + parseFloat(value[key][key2], 10);
            }
        });
        return sum;
    }
})

app.filter('getNotRepeat', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return [];
        
        let _arr = []
        data.forEach((value) => {
            if(value[key]){
                if(!_arr.includes(value[key])) _arr.push(value[key])
            }
        })
        return _arr;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && value[key] > 0){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('keys', function () {
    return function (data) {        
        return Object.keys(data).length
    }
})

app.service('request',['$http', function($http){
    this.ventaComparativo = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/ventaComparativo'
        load.block('tabla_venta_dia')
        $http.post(url, params || {}).then(r => {
            load.unblock('tabla_venta_dia')
            callback(r.data)
        })
    }
    this.last = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/last'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
}])

function initPastel(id, series){
    var legends = []
    series.map(key => {
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })
    setTimeout(() => {
        let data = {
            data : series,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }
        let parent = $("#"+id).parent()
        $("#"+id).remove()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
    }, 250)
}

function printGrafica (id, r){
    let type = ''
    if(r.legend.length > 1){
        type = 'line'
    } else {
        type = 'bar'
    }

    setTimeout(() => {
        var data = {
            series: r.data,
            legend: r.legend,
            umbral: '',
            id: id,
            type : type,
            min : 'dataMin'
        }
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
    }, 250)
}

app.controller('comparativo', ['$scope','request', function($scope, request){
    
    $scope.var = 'tabla'
    $scope.datesEnabled = []
    $scope.filters = {
        fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
        fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
        id_ruta : 1,
        type : "VENTAS"
    }
    $scope.mostrar = true
    $scope.filtros = {}

    $scope.rangesDirectives = {
        'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
        'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
        'Últimos 90 Días': [moment().subtract(89, 'days'), moment()],
        'Último Año' : [moment().subtract(1, 'year').startOf('year') , moment().subtract(1, 'year').endOf('year')],
        'Este Año' : [moment().startOf('year') , moment().endOf("year")],
        'Este mes': [moment().startOf('month'), moment().endOf('month')],
        'Último mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate : moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
		if(data){
			$scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
			$scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
            $scope.changeCalendar()
        }
    }
    $scope.changeCalendar = function(){
        request.last((r) => {
            $scope.clientes = r.clientes
            $scope.comparativo()
        }, $scope.filters)
    }
    $scope.init = function(){
        request.last((r) => {
            $scope.clientes = r.clientes
            $scope.rutas = r.rutas
            $scope.filters.fecha_inicial = r.fecha
            $scope.filters.fecha_final = r.fecha
            setTimeout(() => {
                $("#date-picker").html(`${r.fecha} - ${r.fecha}`)
                $scope.changeRangeDate()
                $scope.comparativo()
            }, 250)
        }, $scope.filters)
    }
    $scope.comparativo = function(){
        request.ventaComparativo(r => {
            $scope.datesEnabled = r.days
            $scope.data = r.data
            if(r.data.length > 0){
                $scope.data_bar = r.data_bar
                $scope.data_pie = r.data_pie.map((r) => {
                    return  {
                        label : r.ruta,
                        value : r.ventas
                    }
                })

                initPastel('pastel-ventadia', $scope.data_pie)
                printGrafica('line-ventadia', $scope.data_bar)
            } else {
                initPastel('pastel-ventadia', [ { label : 'VENTAS POR DIA', value : 0 }])
                printGrafica('line-ventadia', $scope.data_bar)
            }
        }, $scope.filters)
    }

    $scope.renderPie = function(){  
        initPastel('pastel-ventadia', $scope.data_pie)
    }

    $scope.renderBar = () => {
        printGrafica('line-ventadia', $scope.data_bar)
    }

    $scope.init();
}]);