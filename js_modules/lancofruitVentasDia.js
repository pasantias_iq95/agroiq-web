const { app, load } = window

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('sumOfValueDouble', function () {
    return function (data, key, key2) {        
        if (angular.isUndefined(data) || angular.isUndefined(key) || angular.isUndefined(key2))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key][key2] != "" && value[key][key2] != undefined && parseFloat(value[key][key2])){
                sum = sum + parseFloat(value[key][key2], 10);
            }
        });
        return sum;
    }
})

app.filter('getNotRepeat', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return [];
        
        let _arr = []
        data.forEach((value) => {
            if(value[key]){
                if(!_arr.includes(value[key])) _arr.push(value[key])
            }
        })
        return _arr;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && value[key] > 0){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('keys', function () {
    return function (data) {        
        return Object.keys(data).length
    }
})

app.service('request',['$http', function($http){
    this.ventaDia = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/ventaDia'
        load.block('tabla_venta_dia')
        $http.post(url, params || {}).then(r => {
            load.unblock('tabla_venta_dia')
            callback(r.data)
        })
    }
    this.last = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/last'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.tags = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/indicadores'
        load.block('indicadores')
        $http.post(url, params || {}).then(r => {
            load.unblock('indicadores')
            callback(r.data)
        })
    }
    this.eliminar = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/eliminar'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.editar = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/editar'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
}])

let datesEnabled = []
app.controller('dia', ['$scope','request', function($scope, request){
    let datepickerHighlight = () => {
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Ventas', enabled : true } : { tooltip : 'No hay ventas', enabled : false }
            }
        });
        $('#datepicker').datepicker()
        .on('changeDate', function(e) {
            $scope.changeRangeDate({ first_date: moment(e.date).format('YYYY-MM-DD'), second_date : moment(e.date).format('YYYY-MM-DD') })
        });
        
        $("#datepicker").datepicker('setDate', $scope.fecha)
        $('#datepicker').datepicker('update')
    }

    $("#actualizar").prop("disabled", true)
    toastr.options = {
        "positionClass": "toast-top-center",
    }

    $scope.var = 'tabla'
    $scope.filters = {
        fecha : "",
        id_ruta : 1,
        type : "VENTAS"
    }
    $scope.mostrar = true
    $scope.filtros = {}
    
    $scope.loadCalidadUmbral = () => {
        let c = localStorage.getItem('banano_calidad_cluster_umbral')
        if(c){
            $scope.umbral_cluster = JSON.parse(c)
        } else {
            $scope.umbral_cluster = {
                max : 92,
                min : 90
            }
        }
    }
    $scope.saveCalidadUmbral = () => {
        toastr.success('Umbral guardado')
        localStorage.setItem('banano_calidad_cluster_umbral', JSON.stringify($scope.umbral_cluster))
        localStorage.setItem('banano_calidad_dedos_umbral', JSON.stringify($scope.umbral_dedos))
        localStorage.setItem('banano_calidad_empaque_umbral', JSON.stringify($scope.umbral_empaque))
        $scope.reRenderMarkers()
        $scope.closeMenu()
    }
    $scope.openMenu = () => {
        $(".toggler-close, .theme-options").show()
    }
    $scope.closeMenu = () => {
        $(".toggler-close, .theme-options").hide()
    }
    $scope.init = function(){
        request.last((r) => {
            $scope.clientes = r.clientes
            $scope.rutas = r.rutas
            $scope.filters.fecha = r.fecha
            $scope.fecha = r.fecha
            datesEnabled = r.fechas_exist
            datepickerHighlight()
            
            setTimeout(() => {
                $("#fecha").html(`${r.fecha}`)
                $scope.indicadores()
                $scope.ventaDia()
            }, 250) 
        }, $scope.filters)
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.anioCambio = moment(data.first_date).year() != moment($scope.filters.fecha).year()
            $scope.filters.fecha = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha;
        } 
        $scope.porFiltroFecha()
    }

    $scope.porFiltroFecha = function(){
        request.last((r) => {
            $scope.clientes = r.clientes
            $scope.rutas = r.rutas
            setTimeout(() => {
                $scope.indicadores()
                $scope.ventaDia()
            }, 250) 
        }, $scope.filters)
    }

    $scope.changeDate = function(){
		if($scope.filters.fecha == ""){
            $scope.filters.fecha = $scope.fecha
        } else {
            request.last((r) => {
                $scope.clientes = r.clientes
                setTimeout(() => {
                    $scope.indicadores()
                    $scope.ventaDia()
                }, 250) 
            }, $scope.filters)
        }
    }
    $scope.changeRuta = function(){
        $scope.indicadores()
        $scope.ventaDia()
    }
    $scope.editar = function(){
        if(confirm("SE GUARDARAN TODOS LOS CAMBIOS, ¿ESTÁ SEGURO?")){
            request.editar(r => {
                if(r.status == 200){
                    alert("Total actualizado correctamente","ALERTA","success")
                    $scope.mostrar = true
                    $scope.changeFilter()
                } else {
                    alert("No se ha enviado ningun total a modificar")
                }
            }, $scope.filters)
        }
    }
    $scope.activar = function(){
        $scope.mostrar = !$scope.mostrar
    }
    $scope.eliminar = function(id){
        if(confirm("¿ESTÁ SEGURO QUE DESEA ELIMINAR ESTE REGISTRO?")){
            $scope.filters.id_venta = id
            request.eliminar(r => {
                if(r.status == 200){
                    alert("Registro eliminado correctamente","ALERTA","success")
                    $scope.request.all_pestana_uno()
                } else {
                    alert("No se ha podido completar la acción, intente de nuevo")
                }
            }, $scope.filters)
        }
    }
    $scope.indicadores = function(){
        request.tags((r) =>{
            if(r.hasOwnProperty("tags")){
                var data = {
                    colums : 4,
                    tags: r.tags,
                    withTheresholds: false
                }
                ReactDOM.render(React.createElement(ListTags, data), document.getElementById('indicadores'));
            }
        }, $scope.filters)
    }
    $scope.ventaDia = function(){
        request.ventaDia(r => {
            $scope.datesEnabled = r.days
            $scope.data_ventas_dia = r.data_venta_dia
            if(r.data_venta_dia.length > 0){
                $scope.data_bar = r.data
                $scope.data_pie = r.clientes.map((r) => {
                    return  {
                        label : r.cliente,
                        value : r.total
                    }
                })
                $scope.initPastel('pastel-ventadia', $scope.data_pie)
                $scope.printGrafica('line-ventadia', $scope.data_bar)
            } else {
                $scope.initPastel('pastel-ventadia', [{ label : 'VENTAS POR DIA', value : 0 }])
                $scope.printGrafica('line-ventadia', $scope.data_bar)
            }
        }, $scope.filters)
    }
    $scope.renderPie = function(){  
        $scope.initPastel('pastel-ventadia', $scope.data_pie)
    }
    $scope.renderBar = () => {
        $scope.printGrafica('line-ventadia', $scope.data_bar)
    }
    $scope.initPastel = function(id, series){
        var legends = []
        series.map(key => {
            if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
        })
        setTimeout(() => {
            let data = {
                data : series,
                nameseries : "Pastel",
                legend : legends,
                titulo : "",
                id : id
            }
            let parent = $("#"+id).parent()
            parent.empty()
            parent.append(`<div id="${id}" class="chart"></div>`)
            ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
        }, 250)
    }
    $scope.printGrafica = function(id, r){
        let type = ''
        if(r.legend.length > 1){
            type = 'line'
        } else {
            type = 'bar'
        }
    
        setTimeout(() => {
            let data = {
                series: r.data,
                legend: r.legend,
                umbral: '',
                id: id,
                type : type,
                min : 'dataMin'
            }
            let parent = $("#"+id).parent()
            parent.empty()
            parent.append(`<div id="${id}" class="chart"></div>`)
            ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
        }, 250)
    }    
    $scope.init();
}]);