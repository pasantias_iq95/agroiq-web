const { app, load } = window

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && value[key] > 0){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.service('request',['$http', function($http){
    this.ventaTendencia = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/ventaTendencia'
        load.block('tabla_venta_dia')
        $http.post(url, params || {}).then(r => {
            load.unblock('tabla_venta_dia')
            callback(r.data)
        })
    }
    this.ventaTendenciaGrafica = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/tendenciaGrafica'
        load.block('tabla_venta_dia')
        $http.post(url, params || {}).then(r => {
            load.unblock('tabla_venta_dia')
            callback(r.data)
        })
    }
    this.last = function(callback, params){
        let url = 'phrapi/lancofruit/reportes/last'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
}])

function getOptionsGraficaReact(id, options, umbral){
    let newOptions = {
        series: options.series,
        legend: options.legends,
        umbral : umbral,
        id: id,
        min : 'dataMin',
        actions : false
    }
    return newOptions
}

function initGrafica(id, options, umbral){
    setTimeout(() => {
        let data = getOptionsGraficaReact(id, options, umbral)
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
    }, 250)
}

app.controller('tendencia', ['$scope','request', '$filter', function($scope, request, $filter){
    $scope.filters = {
        type : 'VENTAS'
    }

    $scope.init = function(){
        request.last((r) => {
            $scope.clientes = r.clientes
            setTimeout(() => {
                $scope.general()
            }, 250)
        }, $scope.filters)
    }

    $scope.general = function(){
        request.ventaTendencia((r) => {
            renderTablaGeneral(r.series, r.legends)
        }, $scope.filters)
        request.ventaTendenciaGrafica((r) => {
            renderGraficaGeneral(r.data, 0)
        }, $scope.filters)
    }

    const renderGraficaGeneral = (data, umbral) => {
        initGrafica('line-ventadia', data, umbral)
    }

    const renderTablaGeneral = (data, semanas) => {
        let umbral = null
        //if($scope.filters.var == 'calidad_cluster') umbral = getClusterUmbral

        let props = {
            header : [{
                   key : 'name',
                   name : 'RUTA',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                   key : 'prom',
                   name : 'PROM',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : (row) => {
                        return `
                            <div class="${umbral != null ? umbral(row[`prom`]) : ''}">
                                ${row[`prom`]}
                            </div>
                        `
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : (row) => {
                        return `
                            <div class="${umbral != null ? umbral(row[`max`]) : ''}">
                                ${row[`max`]}
                            </div>
                        `
                    }
                },{
                   key : 'minimo',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : (row) => {
                        return `
                            <div class="${umbral != null ? umbral(row[`minimo`]) : ''}">
                                ${row[`minimo`]}
                            </div>
                        `
                    }
                }
            ],
            data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ],
            height : $scope.filters.mode == 'zona' ? 250 : 500
        }
        semanas.map((sem) => {
            props.header.push({
                key : `${sem}`,
                name : `${sem}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : (row) => {
                    return `
                        <div class="${umbral != null ? umbral(row.data[`${sem}`]) : ''}">
                            ${row.data[`${sem}`]}
                        </div>
                    `
                }
            })
        })
        document.getElementById('tabla-general').innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('tabla-general'))
    }

    $scope.init();
}]);