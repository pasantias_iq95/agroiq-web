const { ahttp } = window
const spy = require('console-spy')()

setInterval(() => {
    ahttp.post('phrapi/access/ping')
}, 60000)

Array.prototype.countIf = function(valid) {
    let count = 0
    this.forEach((obj) => {
        if(valid(obj)){
            count++
        }
    });
	return count;
};

$.fn.datepicker.dates['es'] = {
    days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
    daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    daysMin: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
    months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
    today: "Hoy",
    clear: "Limpiar",
    format: "dd/mm/yyyy"
};

$(window).on('load', function () { // makes sure the whole site is loaded
    $('[data-loader="circle-side"]').fadeOut(); // will first fade out the loading animation
    $('#preloader').delay(100).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $('body').delay(100).css({
        'overflow': 'visible'
    });
    $("#notificaciones ul li").click(function(){
        let noti = $(this)
        swal({
            title : 'Borrar notificación',
            text : '¿Estas seguro de borrar esta notificación?',
            icon : 'warning',
            buttons : ['Cancelar', { text : 'Borrar', className : 'bg-red' }]
        })
        .then(function(r_confirm){
            if(r_confirm){
                ahttp.post('phrapi/notificaciones/delete', function(r){
                    if(r.status === 200){
                        toatsr.success('Borrada')
                        noti.remove()
                    }
                }, { id : noti.data('id') })
            }
        })
    })
})

const excepciones_errores = [
    `Warning: Each child in an array or iterator should have a unique "key" prop. Check the top-level render call using <div>. See https://fb.me/react-warning-keys for more information.
    in button`,
    `Warning: Invalid argument supplid to oneOfType. Expected an array of check functions, but received undefined at index 0.`,
    `WARNING: Tried to load AngularJS more than once.`,
    `Warning: Each child in an array or iterator should have a unique "key" prop. Check the top-level render call using <DIV>. See https://fb.me/react-warning-keys for more information.
    in DIV`,
    `Warning: Failed prop type: The prop \`value\` is marked as required in \`SimpleCellFormatter\`, but its value is \`null\`.
    in SimpleCellFormatter`,
    `ECharts#setTheme() is DEPRECATED in ECharts 3.0`,
    `Warning: Each child in an array or iterator should have a unique "key" prop. Check the top-level render call using <DIV>. See https://fb.me/react-warning-keys for more information.
    in SPAN`,
    `Invariant Violation: Minified React error #37; visit http://facebook.github.io/react/docs/error-decoder.html?invariant=37 for the full message or use the non-minified dev environment for full errors and additional helpful warnings.`,
]

spy.on('error', function (err) {
    try {
        const error = err
        if(excepciones_errores.indexOf(error) === -1){
            ahttp.post('phrapi/consolelog/add', () => {
                
            }, { error, url : window.location.href  })
        }
    }catch(e){
        console.log(e)
    }
})
spy.enable()