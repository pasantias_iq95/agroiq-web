// /*==============================================
// 	=            Modificacion del Alert            =
// 	==============================================*/
	window.old_alert = window.alert; 
	window.delay = 1000;
	window.alert = function(msj , categoria , typeMsj , callback){
		var titulo = typeof categoria == "undefined" ? "Notificacion" : categoria;
		var method = typeof typeMsj == "undefined" ? "error" : typeMsj;
		// var position = method == "error" ? "toast-top-full-width" : "toast-top-right";
		var functionCallback = typeof callback == "undefined" ? null : callback;
		var delay = window.delay;
		toastr.options = {
		  "closeButton": true,
		  "positionClass": "toast-top-full-width",
		  "onclick": functionCallback,
		  "showDuration": "1000",
		  "hideDuration": delay,
		  "timeOut": "5000",
		  "extendedTimeOut": "1000",
		  "showEasing": "swing",
		  "hideEasing": "linear",
		  "showMethod": "fadeIn",
		  "hideMethod": "fadeOut"
		}

		toastr[method](msj, titulo);

		if(callback){
			setTimeout(callback, delay);
		}
	}
			
	
// 	/*=====  End of Modificacion del Alert  ======*/

	var app = angular.module('app', []);

	// app.controller('menu', ['$scope', function ($scope) {
		
	// 	$scope.showNav = function(){
	// 		var content = $("#menuSidebar > li#catalogos");
	// 		var subconetent = $("#menuSidebar > li#catalogos > ul.sub-menu");
	// 		if(!content.hasClass('open')){
	// 			content.addClass('open');
	// 			subconetent.css('display', 'block');
	// 		}else{
	// 			content.removeClass('open');
	// 			subconetent.css('display', 'none');
	// 		}
	// 	}
	// }]);

	var load = {
		block : function(tableList){
		var config = (tableList && tableList != "") ? { target: '#' + tableList, animate: true } : { animate: true };
		App.blockUI(config);
		},
		unblock: function(tableList) { 
			var config = (tableList && tableList != "") ? '#' + tableList : '';
			App.unblockUI(config) 
		}
	};

	/////////// PLUGINS JQUERY
	$.fn.serializeObject = function() {
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};


	function ahttp (){
        var _this = this;
        _this = {
        url : "",
        method : "",
        header : {},
        data : {},
        callback : "" ,
        required : function(){
            if(!this.url) throw Error("url es Requida");
            if(!this.method) throw Error("Metodo es Requida");
            if(!this.dataType) this.dataType = 'json';
        },
        block : function(){
            load.block(ahttp.target);
        },
        unblock: function() { load.unblock(ahttp.target);  }
        }

        function call(url , method , callback,  data, header , dataType){
            _this.url = url;
            _this.method = method;
            _this.header = header;
            _this.data = data;
            _this.callback = callback;
            _this.required();
            _this.block();
            var request = $.ajax({
                url: _this.url,
                method: _this.method ,
                data: _this.data,
                dataType: _this.dataType
            });

            request.success(function( r ) {
                // console.log(r);
                callback(r , _this.unblock);
            });

            request.fail(function( jqXHR, textStatus ) {
            	load.unblock(ahttp.target);
                // console.log(jqXHR);
            });
        }

        ahttp.target  = "";

        ahttp.prototype.get = function(url , callback, data , header , dataType){
            return call(url , "get" , callback, data , header , dataType)
        }
        ahttp.prototype.post = function(url , callback, data , header , dataType){
            return call(url , "post" , callback, data , header , dataType)
        }
        ahttp.prototype.put = function(url , callback, data , header , dataType){
            return call(url , "put" , callback, data , header , dataType)
        }
        ahttp.prototype.delete = function(url , callback, data , header ,dataType){
            return call(url , "delete" , callback, data , header , dataType)
        }
    };


	////////// FACTORY PARA LAS PETICIONES HTTP
	app.service('client', function ($http) {
		var _this = this;
		_this.debug = false;
		_this.params = {
			url : "",
			method : "",
			header : {},
			data : {},
			callback : "" ,
			required : function(){
				if(!this.url) throw Error("url is Required");
				if(!this.method) throw Error("Metodo is Required");
				if(!typeof this.callback == 'function') throw Error("Callback is Required");
			},
	    block : function(){
	      load.block(_this.target);
	    },
	    unblock: function() { load.unblock(_this.target); }
		}

		function call(url , method , callback,  data, header){
			_this.params.url = url;
			_this.params.method = method;
			_this.params.header = header;
			_this.params.data = data;
			_this.params.callback = callback;
			_this.params.required();
			_this.params.block();

			var dataConfig = {};
			dataConfig.url = _this.params.url;
			dataConfig.method = _this.params.method;
			dataConfig.header = _this.params.header;
			if(dataConfig.method == "get" ){
				dataConfig.params = _this.params.data;
			}else{
				dataConfig.data = _this.params.data;
			}
			// console.log(dataConfig);
			$http({
	                url : dataConfig.url  , 
	                method : dataConfig.method ,
	                headers : dataConfig.header,
	                data : dataConfig.data,
	                params : dataConfig.params
	            }).then(function(r){
	            	// console.log(r);
	            	// console.log(r.data);
				    callback(r.data , _this.params.unblock);
				}, function(result){
					CallbackError( _this.params.unblock , result);
				})
		};

		function CallbackError(block , error){
			block();
			if(_this.debug){
				if(error.status == 500 ){
					throw new Error(error.statusText);
				}
			}
		}

		_this.target  = "";

		_this.get = function(url , callback, data , header){
			if(angular.isObject(data)){

			}
			return call(url , "get" , callback, data , header)
		}

		_this.post = function(url , callback, data , header){
			return call(url , "post" , callback, data , header)
		}

		_this.put = function(url , callback, data , header){
			return call(url , "put" , callback, data , header)
		}

		_this.delete = function(url , callback, data , header){
			return call(url , "delete" , callback, data , header)
		}

		return _this;

	})
	////////// FACTORY PARA LAS PETICIONES HTTP

	/*==================================
    =            TABLA GRID            =
    ==================================*/
    /**
    
    	TODO:
    	- EJECUTAR CODIGO [ grid.init(); ]  #recibe como parametro el id de la tabla si no lo recibe por default busca datatable_ajax
    	- RECARGAR LA TABLA [ grid.reload() ] #recarga la informacion de la tabla
    	- ACCEDER A LOS METODOS DEL PLUGIN [ grid.tableEdit.{metodo del plugin} ] #accede a los metodos propios del plugin
    
     */
        var grid = {
            table : {},
            tableEdit : {},
            targetButtons : {
			    "targets": -1,
			    "data": null,
			    "defaultContent": '<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
			} ,
            target : -3,
            url : "../controllers/index.php?accion=Productores",
            init : function(id , url , target , targetButtons){
                this.table = new Datatable();
                var id = id || "#datatable_ajax";
                this.url = url || this.url;
                this.target = target || this.target;
                this.targetButtons = targetButtons || this.targetButtons;
                this.table.init({
                    src: $(id),
                    onSuccess: function (grid, response) {
                        // grid:        grid object
                        // response:    json object of server side ajax response
                        // execute some code after table records loaded
                    },
                    onError: function (grid) {
                        // execute some code on network or other general error  
                    },
                    onDataLoad: function(grid) {
                        // execute some code on ajax data load
                    },
                    loadingMessage: 'Loading...',
                    dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                        // So when dropdowns used the scrollable div should be removed. 
                        //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                        
                        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                        "lengthMenu": [
                            [10, 20, 50, 100, 150, -1],
                            [10, 20, 50, 100, 150, "Todos"] // change per page values here
                        ],
                        "pageLength": 10, // default record count per page
                        "ajax": {
                            "url": this.url, // ajax source
                        },
                        "order": [
                            [1, "asc"]
                        ]// set first column as a default sort by asc,
                        ,"columnDefs": [ this.targetButtons,
                        {
                            "targets": this.target,
                            "data": function ( row, type, val, meta ) {
                            	if(type == 'display'){
                            		return (row[meta.col] == 0) ? '<span id="activo" class="btn btn-sm red-thunderbird">INACTIVO</span>' : '<span class="btn btn-sm green-jungle" id="inactivo">ACTIVO</span>';
                            	}
								// ($fila->status==0)?'<span id="activo" class="btn btn-sm red-thunderbird">INACTIVO</span>' : '<span class="btn btn-sm green-jungle" id="inactivo">ACTIVO</span>',
							} ,
                            "defaultContent": '<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Activar / Desactivar</button>'
                        } ]
                    }
                });

                this.table.getDataTable().ajax.reload();
                this.table.clearAjaxParams();
                this.tableEdit = this.table.getDataTable()
            },
            reload : function(){
                this.table.getDataTable().ajax.reload();
                this.table.clearAjaxParams();
            }
        }
    /*=====  End of TABLA GRID  ======*/

var ahttp = new ahttp();