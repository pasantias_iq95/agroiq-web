app.controller('agregarAsistencia', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){

	$scope.data = {
		id : "<?= $_GET['id'] ?>",
		id_personal : 1,
		fecha : "",
		hora : "",
		responsable : "",
		trabajador : "",
		hacienda : "NUEVA PUBENZA",
		lote : "",
		cedula : "",
		observaciones : "",
		lotes : []
	};
	
	$scope.init = function(){
		$(".js-example-basic-multiple").select2();
		$("span.select2-container").css("border", "1px solid gray");
		const data = {
			id : $scope.data.id
		};
		client.post("phrapi/marcel/asistencia/index", function(r, b){
			b()
			if(r){
				$scope.trabajadores = r.trabajadores

				if(r.hasOwnProperty("data")){
					$scope.data = r.data

					// set lotes
					var ll = []
					$.each(r.data.lotes, function(index, value){
						ll.push(value)
					});
					$(".js-example-basic-multiple").select2('val', ll);
				}
			}
		}, data)
	}

	$scope.changeTrabajador = function(){
		let index = $("#trabajador :selected").attr("data-index")
		let trabajador = $scope.trabajadores[index]
		$scope.data.perfil = trabajador.perfil
		$scope.data.cedula = trabajador.cedula
	}

	$scope.saveDatos = function(){
		if($scope.data.fecha != "" && $scope.data.hora != "" && $scope.data.finca != "" && $scope.data.trabajador != "" && $scope.data.lote != ""){
			/*$scope.data.labores = []
			$.each($(".labor"), function(key, value){
				if($(value).is(":checked")){
					$scope.data.labores.push($(value).attr("id").split("_")[1])
				}
			})*/
			$scope.data.lotes = $("#lotes").val().join(",")
			client.post("phrapi/marcel/asistencia/save", function(r, b){
				b()
				if(r){
					window.location = "revisionAsistencia";
				}
			}, $scope.data)
		}else{
			alert("Formulario incompleto")
		}
	}

	$scope.init();
}]);