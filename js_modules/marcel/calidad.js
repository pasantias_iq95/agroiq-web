const { app, load } = window

app.filter('num', function() {
    return function(input) {
        return parseInt(input, 10);
    };
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});


app.filter('unique', function() {
    return function(collection, keyname) {
        var output = [], 
            keys = [];
        angular.forEach(collection, function(item) {
            var key = item[keyname];
            if(keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });
        return output;
    };
});

function getQueryParams() {
    var params = {};

    if (location.search) {
        var parts = location.search.substring(1).split('&');

        for (var i = 0; i < parts.length; i++) {
            var nv = parts[i].split('=');
            if (!nv[0]) continue;
            params[nv[0]] = nv[1] || true;
        }
    }

    // Now you can get the parameters you want like so:
    //////console.log(params);
    return params;
}

function getMaxSeries(series){
    var max = null;
    Object.keys(series).map(key => {
        var values = series[key].data
        values.map(value => {
            if(value > 0 && value != null)
            if(max == null || value > max) max = value;
        })
    })
    return max;
}

function getMinSeries(series){
    var min = null;
    Object.keys(series).map(key => {
        var values = series[key].data
        values.map(value => {
            if(value > 0 && value != null)
            if(min == null || value < min) min = value;
        })
    })
    return min;
}

function getOptionsGraficaReact(id, options, title, min, type){
    var newOptions = {
        series: options.series,
        legend: options.legends,
        umbral: null,
        id: id,
        type : type || 'line',
        min : min || 'dataMin', //getMinSeries(options.series),
        max : null, //getMaxSeries(options.series)
        titulo : title || null,
        showLegends : true,
        actions : false
    }
    return newOptions
}

function initGrafica(id, options, title, type){
    let parent = $(`#${id}`).parent()
    parent.empty()
    parent.append(`<div id="${id}" style="height:500px;"></div>`)

    setTimeout(() => {
        let props = getOptionsGraficaReact(id, options, title, 0, type)
        ReactDOM.render(React.createElement(Historica, props), document.getElementById(id));
    }, 250)
}

function initPastel(id, series){
    let legends = []
    let newSeries = []
    Object.keys(series).map(key => {
        let label = series[key].label
        newSeries.push({
            label : series[key].label,
            value : parseFloat(series[key].value),
            color : (label.includes('>')) ? '#E43A45' : (label.includes('<')) ? '#C49F47' : '#26C281'
        })
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })

    let parent = $(`#${id}`).parent()
    parent.empty()
    parent.append(`<div id="${id}" style="height:300px;"></div>`)
    setTimeout(() => {
        let data = {
            data : newSeries,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
    }, 250)
}

app.factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
});

app.service('request', ['$http', function($http){
    
    this.getFotos = function(callback, params){
        load.block('fotos')

        let data = params || {}
        let url = 'phrapi/marcel/calidad/fotos'
        $http.post(url, data).then((r) => {
            load.unblock('fotos')
            callback(r.data)
        }).catch(() => {
            load.unblock('fotos')
        })
    }

    this.getLast = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marcel/calidad/last'
        $http.post(url, data).then((r) => {
            callback(r.data)
        })
    }
    
}])

app.controller('informe_calidad', ['$scope','$http','client', 'request', '$filter', function($scope,$http,client,$request,$filter){
    

    var cliente_marca = getQueryParams();
    var param_cliente = "";
    var param_marca   = "";

    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }

    if(cliente_marca.c && cliente_marca.m){
        param_cliente = decodeURI(cliente_marca.c);
        param_marca   = decodeURI(cliente_marca.m);
    }

    $scope.por_cluster = function(){
        var response = false;
        if($scope.checkParams()){
            if(param_marca == 'PINALINDA (1Kg)' || param_marca == 'PINALINDA 3 DEDOS'){
                response = true;
            }
        }
        return response;
    }

    $scope.leyendaGeneralTitle = 'Calidad';
    $scope.data_grafica_calidad = [];
    $scope.data_grafica_de      = [];
    $scope.data_grafica_peso    = [];
    $scope.data_grafica_cluster = [];
    $scope.porCluster = [];
    $scope.porClusterText = "Desviación Estándar";
    $scope.porClusterFlag = false;
    $scope.table_porCluster = {
        label : [],
        data : []
    };

    $scope.data_grafica_calidad_historico_marcas  = [];

    $scope.data_grafica_danos_total = [];
    $scope.data_grafica_danos_seleccion = [];
    $scope.data_grafica_danos_empaque = [];
    $scope.data_grafica_danos_otros = [];

    $scope.param_cliente = param_cliente;
    $scope.param_marca   = param_marca;
    $scope.param_peso    = "";
    $scope.param_cluster = "";
    $scope.param_logo = "";

    $scope.checkParams = function(){
        if(param_cliente && param_marca){
            //////console.log('>' + param_marca + '<')
            if(param_cliente == "" && param_marca == "")
                return false;
            else
                return true;
        }
        return false;
    }

    $scope.id_company = 0;
    $scope.tags = {
        calidad : {
            value : 0,
            label : ""
        },
        calidad_maxima_dedos : {
            value : 0,
            label : ""
        },
        calidad_maxima_cluster : {
            value : 0,
            label : ""
        },
        calidad_minima_dedos : {
            value : 0,
            label : ""
        },
        calidad_minima_cluster : {
            value : 0,
            label : ""
        },
        cluster : {
            value : 0,
            label : ""
        },
        desviacion_estandar : {
            value : 0,
            label : ""
        },
        peso : {
            value : 0,
            label : ""
        },
        calidad_dedos : {
            value : 0,
            label : ""
        },
        calidad_cluster : {
            value : 0,
            label : ""
        },
        dedos_promedio : {
            value : 0,
            label : ""
        }
    };

    $scope.initGrafica = function(id, options){
        //var props = getOptionsGraficaReact(id, options)
        //ReactDOM.render(React.createElement(Historica, props), document.getElementById(id));
        initGrafica(id, options)
    }

    $scope.initPastel = function(id, series){
        var legends = []
        var newSeries = []
        if(series && series.length > 0){
            Object.keys(series).map(key => {
                newSeries.push({
                    label : series[key].name,
                    value : parseFloat(series[key].value)
                })
                if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
            })
        }else{
            legends.push('NO HAY DATOS')
            newSeries.push({
                label : 'NO HAY DATOS',
                value : 0
            })
        }

        let parent = $(`#${id}`).parent()
        parent.empty()
        parent.append(`<div id="${id}" style="height:300px;"></div>`)
        setTimeout(() => {
            let data = {
                data : newSeries,
                nameseries : "Pastel",
                legend : legends,
                titulo : "",
                id : id
            }
            ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
        }, 250)
    }

    $scope.umbrales = {};
    $scope.calidad = {
        params : {
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            contenedor : "",
            fecha_inicial : moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD'),
            fecha_final :  moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD'),
            cliente: param_cliente,
            marca: param_marca,
            proceso : 'Proceso en Verde'
        },
        step : 0,
        path : ['phrapi/marcel/calidad/inicio' , 'phrapi/calidad/labores' , 'phrapi/calidad/causas'],
        templatePath : [],
        nocache : function(){
            this.templatePath.push('/views/marcel/templetes/calidad/step1.html?' +Math.random());
        }
    }

    /*----------  BARCO  ----------*/
    $scope.barco = {
        nombre : ""
    }

    $scope.saveBarco = function(){
        if($scope.barco.nombre != ""){
            var data = {
                fecha_inicio : $scope.calidad.params.fecha_inicial,
                fecha_final : $scope.calidad.params.fecha_final,
                barco : $scope.barco.nombre
            }
            client.post('phrapi/save/barco/index' , function(r , b){
                b();
                if(r){
                    console.log(r);
                }
            }, data);
        }
    }
    /*----------  BARCO  ----------*/
    

    $scope.tab = function(tab){
        appEcharts.type = tab;
        appEcharts.init();
    }

    $scope.changeContenedor = function(){
        $scope.loadExternal();
    }

	$scope.changeRangeDate = function(data){
		if(data){
			$scope.calidad.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
			$scope.calidad.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
            $scope.loadExternal();
            $scope.getFotos();
		}
    }
    
    $scope.loadExternal = function(){
        if($scope.calidad.path[$scope.calidad.step] != ""){
            var data = $scope.calidad.params;
            client.post($scope.calidad.path[$scope.calidad.step] , $scope.startDetails , data);

            $scope.initProduccion()
        }
    }

    $scope.por_clusterGrap = function(){
        appEcharts.por_cluster = $scope.porCluster;
        $scope.porClusterFlag = !$scope.porClusterFlag;
        var mode = 'general';
        if($scope.porClusterFlag) {
            mode = 'desviacion';  
            $scope.porClusterText = "General";
        }else{
            mode = 'general';
            $scope.porClusterText = "Desviación Estándar";
        }
        var grafica_por_cluster = appEcharts.getOptionsPorCluster(mode);
        
        appEcharts.init(function(echarts){
            var general_echarts_porcluster = echarts.init(document.getElementById('peso_cluster') , 'infographic');
            general_echarts_porcluster.setOption(grafica_por_cluster);
            window.onresize = function(){
                general_echarts_porcluster.resize();
            }
        })
    }

    $scope.startDetails = function(r , b){
        b();
        if(r){
            if(!$scope.contenedores)
                $scope.contenedores = r.contenedores

            // React Graficas (CALIDAD HISTÓRICO)
            $scope.data_grafica_calidad = r.grafica_principal_calidad;
            $scope.initGrafica('highstock_calidad_historica', $scope.data_grafica_calidad);
            $scope.data_grafica_de = r.grafica_principal_de;
            $scope.data_grafica_peso = r.grafica_principal_peso;
            $scope.data_grafica_peso_cluster = r.grafica_principal_peso_cluster;
            $scope.data_grafica_cluster = r.grafica_principal_cluster;

            // React Grafica (DAÑOS HISTÓRICO)
            $scope.data_grafica_categorias_todos = r.grafica_danos_total;
            $scope.initGrafica('highstock_danos_total', $scope.data_grafica_categorias_todos)
            $scope.data_grafica_categorias_seleccion = r.grafica_danos_seleccion
            $scope.data_grafica_categorias_empaque = r.grafica_danos_empaque
            $scope.data_grafica_categorias_otros = r.grafica_danos_otros

            // Mostrar/Ocultar botones (DAÑOS HISTÓRICO)
            $scope.seleccion_historico = r.grafica_danos_total.series.hasOwnProperty('SELECCION') ? true : false
            $scope.empaque_historico = r.grafica_danos_total.series.hasOwnProperty('EMPAQUE')
            $scope.otros_historico = r.grafica_danos_total.series.hasOwnProperty('OTROS')

            // React Graficas (DAÑOS GENERALES)
            $scope.initPastel('echarts_generales', r.danhos.series)
            $scope.data_grafica_pastel_seleccion = r.danhos.seleccion
            $scope.initPastel('echarts_generales_detalles', $scope.data_grafica_pastel_seleccion)
            $scope.data_grafica_pastel_empaque = r.danhos.empaque
            $scope.data_grafica_pastel_otros = r.danhos.otros
            
            // Mostrar/Ocultar botones (DAÑOS GENERALES)
            $scope.seleccion = r.danhos.seleccion.length > 0;
            $scope.empaque = r.danhos.empaque.length > 0;
            $scope.otros = r.danhos.otros.length > 0;

            $scope.porCluster = r.por_cluster || []

            $scope.table_porCluster = [];
            $scope.table_porCluster.data = (r.hasOwnProperty("table_por_cluster") && r.table_por_cluster.hasOwnProperty("table")) ? r.table_por_cluster.table : [];
            $scope.table_porCluster.label = (r.hasOwnProperty("table_por_cluster") && r.table_por_cluster.hasOwnProperty("labels")) ? r.table_por_cluster.labels : [];
            $scope.table_porCluster.totals = (r.hasOwnProperty("table_por_cluster") && r.table_por_cluster.hasOwnProperty("totales")) ? r.table_por_cluster.totales : [];
			$scope.tabla_principal = [];
			$scope.total_principal = [];
			$scope.total_principal.peso = 0;
			$scope.total_principal.num_cluster = 0;
			$scope.total_principal.num_dedos = 0;
			$scope.total_principal.danhos_por_dedo = 0;
			$scope.total_principal.danhos_por_cluster = 0;
			$scope.total_principal.calidad_dedos = 0;
			$scope.total_principal.calidad_cluster = 0;
			$scope.total_principal.seleccion = [];
			$scope.total_principal.seleccion.SR = 0;
			$scope.total_principal.seleccion.BR = 0;
			$scope.total_principal.seleccion.NI = 0;
			$scope.total_principal.seleccion.CT = 0;
			$scope.total_principal.seleccion.LP = 0;
			$scope.total_principal.seleccion.BM = 0;
			$scope.total_principal.seleccion.LS = 0;
			$scope.total_principal.seleccion.PS = 0;
			$scope.total_principal.empaque = [];
			$scope.total_principal.empaque.SR = 0;
			$scope.total_principal.empaque.BR = 0;
			$scope.total_principal.empaque.NI = 0;
			$scope.total_principal.empaque.CT = 0;
			$scope.total_principal.empaque.FL = 0;
			$scope.total_principal.empaque.TC = 0;
			$scope.total_principal.empaque.MF = 0;
			$scope.total_principal.empaque.UG = 0;
			$scope.total_principal.empaque.OQ = 0;
			$scope.total_principal.otros = [];
			$scope.total_principal.otros.WI = 0;
			$scope.total_principal.otros.SK = 0;
			$scope.total_principal.otros.BB = 0;
            $scope.total_principal.otros.TS = 0;
			if($scope.checkParams){

                let x = 0
				for(x=0;x<r.tabla_principal.length; x++){
					var todo = [];
					var seleccion = [];
					var empaque = [];
					var otros = [];
					todo['peso']=r.tabla_principal[x].peso;
					$scope.total_principal.peso = parseFloat($scope.total_principal.peso) + parseFloat(todo['peso']) ;
					todo['num_cluster']=r.tabla_principal[x].cantidad_gajos;
					$scope.total_principal.num_cluster = parseFloat($scope.total_principal.num_cluster) + parseFloat(todo['num_cluster']) ;
					todo['num_dedos']=r.tabla_principal[x].cantidad_dedos;
					$scope.total_principal.num_dedos = parseFloat($scope.total_principal.num_dedos) + parseFloat(todo['num_dedos']) ;
					todo['danhos_por_dedo']=r.tabla_principal[x].danhos_por_dedo;
					$scope.total_principal.danhos_por_dedo= parseFloat($scope.total_principal.danhos_por_dedo) + parseFloat(todo['danhos_por_dedo']) ;
					todo['danhos_por_cluster']=r.tabla_principal[x].danhos_por_cluster;
					$scope.total_principal.danhos_por_cluster = parseFloat($scope.total_principal.danhos_por_cluster) + parseFloat(todo['danhos_por_cluster']) ;
					todo['calidad_dedos']=r.tabla_principal[x].calidad_dedos;
					$scope.total_principal.calidad_dedos = parseFloat($scope.total_principal.calidad_dedos) + parseFloat(todo['calidad_dedos']) ;
					todo['calidad_cluster']=r.tabla_principal[x].calidad_cluster;
					$scope.total_principal.calidad_cluster = parseFloat($scope.total_principal.calidad_cluster) + parseFloat(todo['calidad_cluster']) ;
					
					for(let i=0;i<r.tabla_principal[x].seleccion.length;i++){
							var campo = r.tabla_principal[x].seleccion[i].campo.split(" ");
							seleccion[campo[0]] = r.tabla_principal[x].seleccion[i].cantidad;
						}
					for(let i=0;i<r.tabla_principal[x].empaque.length;i++){
							var campo = r.tabla_principal[x].empaque[i].campo.split(" ");
							empaque[campo[0]] = r.tabla_principal[x].empaque[i].cantidad;
						}
					for(let i=0;i<r.tabla_principal[x].otros.length;i++){
							var campo = r.tabla_principal[x].otros[i].campo.split(" ");
							otros[campo[0]] = r.tabla_principal[x].otros[i].cantidad;
						}
						
					if(parseFloat(seleccion.SR)>0)
					$scope.total_principal.seleccion.SR = parseFloat($scope.total_principal.seleccion.SR) + parseFloat(seleccion["SR"]);
					if(parseFloat(seleccion.BR)>0)
					$scope.total_principal.seleccion.BR = parseFloat($scope.total_principal.seleccion.BR) + parseFloat(seleccion['BR']);
					if(parseFloat(seleccion.NI)>0)
						$scope.total_principal.seleccion.NI = parseFloat($scope.total_principal.seleccion.NI) + parseFloat(seleccion['NI']);
					if(parseFloat(seleccion.CT)>0)
						$scope.total_principal.seleccion.CT = parseFloat($scope.total_principal.seleccion.CT) + parseFloat(seleccion['CT']);
					if(parseFloat(seleccion.LP)>0)
						$scope.total_principal.seleccion.LP = parseFloat($scope.total_principal.seleccion.LP) + parseFloat(seleccion['LP']);
					if(parseFloat(seleccion.BM)>0)
						$scope.total_principal.seleccion.BM = parseFloat($scope.total_principal.seleccion.BM) + parseFloat(seleccion['BM']);
					if(parseFloat(seleccion.LS)>0)
						$scope.total_principal.seleccion.LS = parseFloat($scope.total_principal.seleccion.LS) + parseFloat(seleccion['LS']);
					if(parseFloat(seleccion.PS)>0)
						$scope.total_principal.seleccion.PS = parseFloat($scope.total_principal.seleccion.PS) + parseFloat(seleccion['PS']);
					
					if(parseFloat(empaque.SR)>0)
					$scope.total_principal.empaque.SR = parseFloat($scope.total_principal.empaque.SR) + parseFloat(empaque["SR"]);
					if(parseFloat(empaque.BR)>0)
					$scope.total_principal.empaque.BR = parseFloat($scope.total_principal.empaque.BR) + parseFloat(empaque['BR']);
					if(parseFloat(empaque.NI)>0)
						$scope.total_principal.empaque.NI = parseFloat($scope.total_principal.empaque.NI) + parseFloat(empaque['NI']);
					if(parseFloat(empaque.CT)>0)
						$scope.total_principal.empaque.CT = parseFloat($scope.total_principal.empaque.CT) + parseFloat(empaque['CT']);
					if(parseFloat(empaque.FL)>0)
						$scope.total_principal.empaque.FL = parseFloat($scope.total_principal.empaque.FL) + parseFloat(empaque['FL']);
					if(parseFloat(empaque.TC)>0)
						$scope.total_principal.empaque.TC = parseFloat($scope.total_principal.empaque.TC) + parseFloat(empaque['TC']);
					if(parseFloat(empaque.MF)>0)
						$scope.total_principal.empaque.MF = parseFloat($scope.total_principal.empaque.MF) + parseFloat(empaque['MF']);
					if(parseFloat(empaque.UG)>0)
						$scope.total_principal.empaque.UG = parseFloat($scope.total_principal.empaque.UG) + parseFloat(empaque['UG']);
					if(parseFloat(empaque.OQ)>0)
						$scope.total_principal.empaque.OQ = parseFloat($scope.total_principal.empaque.OQ) + parseFloat(empaque['OQ']);
					
					if(parseFloat(otros.WI)>0)
						$scope.total_principal.otros.WI = parseFloat($scope.total_principal.otros.WI) + parseFloat(otros['WI']);
					if(parseFloat(otros.SK)>0)
						$scope.total_principal.otros.SK = parseFloat($scope.total_principal.otros.SK) + parseFloat(otros['SK']);
					if(parseFloat(otros.BB)>0)
						$scope.total_principal.otros.BB = parseFloat($scope.total_principal.otros.BB) + parseFloat(otros['BB']);
					if(parseFloat(otros.TS)>0)
						$scope.total_principal.otros.TS = parseFloat($scope.total_principal.otros.TS) + parseFloat(otros['TS']);
					
						//console.log(r.tabla_principal[x].calidad_detalle);
					
					todo['seleccion']=seleccion;
					todo['empaque']=empaque;
					todo['otros']=otros;
					$scope.tabla_principal.push(todo);
				}
				//console.log($scope.tabla_principal);
				//$scope.tabla_principal = r.tabla_principal;

                $scope.total_principal.peso = (($scope.total_principal.peso / x));
                $scope.total_principal.num_cluster = Math.round(($scope.total_principal.num_cluster / x));
                $scope.total_principal.num_dedos = Math.round(($scope.total_principal.num_dedos / x));
                // $scope.total_principal.danhos_por_cluster = ($scope.total_principal.danhos_por_cluster));
                $scope.total_principal.calidad_dedos = Math.round(($scope.total_principal.calidad_dedos / x));
                $scope.total_principal.calidad_cluster = Math.round(($scope.total_principal.calidad_cluster / x));
			}
			else{
				$scope.tabla_principal = [];
			}
			$scope.total_principal.peso = $scope.total_principal.peso.toFixed(2);

            // tags
            $.each(r.tags, function(index, value){
                r.tags[index] = parseFloat(value) || 0
            })

            $scope.umbrales = r.umbrals || {};
            $scope.tags.calidad.value = parseFloat(r.tags.calidad).toFixed(2);
            $scope.tags.calidad_maxima_dedos.value = parseFloat(r.tags.calidad_maxima_dedos).toFixed(2);
            $scope.tags.calidad_minima_dedos.value = parseFloat(r.tags.calidad_minima_dedos).toFixed(2);
            $scope.tags.calidad_maxima_cluster.value = parseFloat(r.tags.calidad_maxima_cluster).toFixed(2);
            $scope.tags.calidad_minima_cluster.value = parseFloat(r.tags.calidad_minima_cluster).toFixed(2);

            $scope.tags.cluster.value = parseFloat(r.tags.cluster).toFixed(2);
            $scope.tags.desviacion_estandar.value = parseFloat(r.tags.desviacion_estandar.toFixed(2));
            $scope.tags.peso.value = parseFloat(r.tags.peso).toFixed(2);
            $scope.tags.calidad_dedos.value = parseFloat(r.tags.calidad_dedos).toFixed(2);
            $scope.tags.calidad_cluster.value = parseFloat(r.tags.calidad_cluster).toFixed(2);
            $scope.tags.dedos_promedio.value = parseFloat(r.tags.dedos_promedio).toFixed(2);
            // tablas
            $scope.tabla_cliente_marca_calidad = r.tabla_principal_calidad;
            $scope.tabla_cliente_marca_danos = r.tabla_principal_danos;

            // header
            if(r.data_header){
                $scope.param_peso    = r.data_header.peso;
                $scope.param_cluster = r.data_header.cluster;
                $scope.param_logo = r.data_header.logo;
				$scope.param_marcas = r.data_header.marcas;
            }

            /*----------  BARCO  ----------*/
            if(r.hasOwnProperty("barco")){
                $scope.barco.nombre = r.barco;
            }
        }
    }

    $scope.initProduccion = () => {
        let { idFinca, fecha_inicial, marca } = $scope.calidad.params

        $http.post('phrapi/marun/cajas/registros', { finca : idFinca, fecha_inicial, marca })
        .then((r) => {
            $scope.registros = r.data.data
            
            $http.post('phrapi/marun/cajas/graficasBarras', { finca : idFinca, fecha_inicial, marca })
            .then((r) => {
                setTimeout(() => {
                    printGraficasBarras(r.data)
                }, 200)
            })
            setTimeout(() => {
                $scope.searchTable.changePagination()
            }, 200)
        })
    }

    $scope.next = function(dataSource){
        if($scope.searchTable.actual_page < $scope.searchTable.numPages) {
            $scope.searchTable.actual_page++;
            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
        }
    }

    $scope.prev = function(dataSource){
        if($scope.searchTable.actual_page > 1){
            $scope.searchTable.actual_page--;
            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
        }
    }

    $scope.setOrderTable = function(field){
        var reverse = false
        if(field == $scope.searchTable.orderBy){
            reverse = !$scope.searchTable.reverse
        }else{
            $scope.searchTable.orderBy = field
        }
        $scope.searchTable.reverse = reverse
    }

    $scope.table = {
        unidad : 'lb'
    }

    $scope.searchTable = {
        orderBy : "hora",
        reverse : true,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        numPages : 0,
        calculatePages : function(){
            $scope.searchTable.numPages = 
                parseInt($filter('filter')($scope.registros, $scope.table.search).length / $scope.searchTable.limit)
                    + ($filter('filter')($scope.registros, $scope.table.search).length % $scope.searchTable.limit == 0 ? 0 : 1)
        },
        changePagination : function(){
            if($scope.searchTable.limit == 0)
                $scope.searchTable.limit = $scope.registros.length

            setTimeout(function(){
                $scope.$apply(function(){
                    $scope.searchTable.numPages = parseInt($filter('filter')($scope.registros, $scope.table.search).length / $scope.searchTable.limit)  + ($filter('filter')($scope.registros, $scope.table.search).length % $scope.searchTable.limit == 0 ? 0 : 1)
                    $scope.searchTable.actual_page = 1
                    $scope.searchTable.startFrom = 0
                })
            }, 250)
        }
    }

    var printGraficasBarras = function(r){
        if(r){
            $scope.marcasBarras = r.marcas
            $scope.graficasBarrar = r.graficas
            $scope.rangos = r.rangos
            Object.keys(r.graficas).map((key, index) => {
                let min = null, max = null
                if(r.graficas[key].series.Cantidad.umbral){
                    min = parseFloat(r.graficas[key].series.Cantidad.umbral.min)
                    max = parseFloat(r.graficas[key].series.Cantidad.umbral.max)
                }
                
                r.graficas[key].series.Cantidad.itemStyle.normal.color = function(e){
                    // range umbral 
                    if(min && max){
                        var min_peso = 0, max_peso = 0
                        if(parseFloat(e.name)){
                            min_peso = max_peso = parseFloat(e.name)
                        }else{
                            var min_max = e.name.split(',')
                            min_peso = parseFloat(min_max[0].replace('[', '').trim())
                            max_peso = parseFloat(min_max[1].replace(']', '').trim())
                        }

                        if(min_peso >= min && max_peso <= max){
                            return '#26C281'
                        }else if(max_peso <= min){
                            return '#C49F47'
                        }else{
                            return '#E43A45'
                        }
                    }

                    return '#E43A45'
                }
                initGrafica(`grafica-excedente-barras`, r.graficas[key], key)
                initPastel(`grafica-excedente-pie`, r.pasteles[key])
            })
        }
    }

    $scope.revision = function(porcentaje){
        if(porcentaje >=  parseFloat($scope.umbrales.green_umbral_1))
            return 'green-jungle';
        else if (porcentaje >= parseFloat($scope.umbrales.yellow_umbral_1) && porcentaje <= parseFloat($scope.umbrales.yellow_umbral_2))
            return 'yellow-lemon';
        else 
            return 'red-thunderbird'; 
    }
	
    $scope.viewGraphDanos = function(i){
        if(1 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_total);
            $scope.leyendaGeneralTitle = 'Total';
        }
        if(2 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_seleccion);
            $scope.leyendaGeneralTitle = 'Selección';
        }
        if(3 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_empaque);
            $scope.leyendaGeneralTitle = 'Empaque';
        }
        if(4 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_otros);
            $scope.leyendaGeneralTitle = 'Otros';
        }
    }

    $scope.filters = {
        desde : "",
        hasta : "",
        marca : "<?= $_GET['m'] ?>",
        cliente : "<?= $_GET['c'] ?>"
    }

    var printFotos = (r) => {
        if(r.data){
            $scope.marcas = r.marcas
            $scope.fotos = r.data
        }
    }

    $scope.getFotos = () => {
        $request.getFotos(printFotos, $scope.calidad.params)
    }

    $scope.initFotos = () => {
        if($scope.checkParams()){
            $request.getLast((r) => {
                load.unblock('fotos')
                $scope.semanas = r.semanas_disponibles
                $scope.filters.desde = r.semana
                $scope.filters.hasta = r.semana
                $scope.getFotos()
            })
        }
    }

    $scope.datesEnabled = []
    $scope.fechas_verde = []
    $scope.fechas_maduracion = []
    $scope.last = () => {
        $http.post('phrapi/marcel/calidad/calidadLast', $scope.calidad.params).then(r => {
            $scope.datesEnabled = r.data.days

            $scope.fechas_maduracion = r.data.maduracion
            $scope.fechas_verde = r.data.verde

            $scope.calidad.params.fecha_inicial = r.data.fecha
            $scope.calidad.params.fecha_final = r.data.fecha
            $("#date-picker").html(`${r.data.fecha} -  ${r.data.fecha}`)
            
            $scope.loadExternal()
            $scope.initFotos()
        })
    }

    $scope.dateRangeDateCustomDate = (date) => {
        let fecha = date.format('YYYY-MM-DD')
        if($scope.fechas_verde || $scope.fechas_maduracion){
            if($scope.fechas_maduracion.includes(fecha) && $scope.fechas_verde.includes(fecha))
                return 'maduracion-verde'

            if($scope.fechas_verde.includes(fecha))
                return 'verde bg-green-jungle bg-font-green-jungle'

            if($scope.fechas_maduracion.includes(fecha))
                return 'maduracion bg-yellow-lemon bg-font-yellow-lemon'
        }
        return ''
    }

}]);