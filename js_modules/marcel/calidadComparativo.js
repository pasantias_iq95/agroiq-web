var colors = ['#5793f3', '#49d165', '#d14a61', '#d14a61'];
var option = {
    color: colors,

    tooltip: {
        trigger: 'none',
        axisPointer: {
            type: 'cross'
        }
    },
    legend: {
        data:['ECUADOR', 'HAMBURGO'],
        x: 'left'
    },
    grid: {
        top: 70,
        bottom: 80
    },
    toolbox: {
        feature: {
            dataZoom: {
                yAxisIndex: 'none'
            },
            magicType: {
                type: ['line', 'bar']
            },
            restore: {},
            saveAsImage: {}
        }
    },
    xAxis: [
        {
            type: 'category',
            axisTick: {
                alignWithLabel: true
            },
            boundaryGap: true,
            axisLine: {
                onZero: true,
                lineStyle: {
                    color: colors[1]
                }
            },
            axisPointer: {
                label: {
                    formatter: function (params) {
                        return (params.seriesData.length ? '：' + params.seriesData[0].data : '');
                    }
                }
            },
            data: ["2016-1", "2016-2", "2016-3", "2016-4", "2016-5", "2016-6", "2016-7", "2016-8", "2016-9", "2016-10", "2016-11", "2016-12"]
        },
        {
            type: 'category',
            axisTick: {
                alignWithLabel: true
            },
            boundaryGap: true,
            axisLine: {
                onZero: true,
                lineStyle: {
                    color: colors[0]
                }
            },
            axisPointer: {
                label: {
                    formatter: function (params) {
                        return (params.seriesData.length ? '：' + params.seriesData[0].data : '');
                    }
                }
            },
            data: ["2015-1", "2015-2", "2015-3", "2015-4", "2015-5", "2015-6", "2015-7", "2015-8", "2015-9", "2015-10", "2015-11", "2015-12"]
        }
    ],
    yAxis: [
        {
            type: 'value',
            min:'dataMax'
        }
    ],
    series: [
        {
            name:'2015 降水量',
            type:'line',
            xAxisIndex: 1,
            smooth: true,
            data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
        },
        {
            name:'2016 降水量',
            type:'line',
            smooth: true,
            data: [3.9, 5.9, 11.1, 18.7, 48.3, 69.2, 231.6, 46.6, 55.4, 18.4, 10.3, 0.7]
        }
    ],
    plotOptions: {
        series: {
            connectNulls: true
        }
    },
};
var option_p = option
var option_c = option
var option_defectos = option

app.service('request', ['$http', function($http){

    this.index = function(callback, params){
        let url = 'phrapi/marcel/calidad/comparativo'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        })
    }
}])

app.directive('file', function () {
    return {
        scope: {
            file: '='
        },
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var file = event.target.files[0];
                scope.file = file ? file : undefined;
                scope.$apply();
            });
        }
    };
});

function sortNumber(a,b) {
    return a - b;
}

app.controller('produccion', ['$scope', 'request' , '$http', function($scope, $request, $http){

    var table1, table2, table3, table4
    $scope.filters = {
        defecto : "BR"
    }

    const initTableCalidad = (r) => {
        var props = {
            header : [{
                   key : 'lugar',
                   name : '',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 140
                },{
                   key : 'avg',
                   name : '',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                },{
                   key : 'max',
                   name : '',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                   key : 'min',
                   name : '',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                }
            ],
            data : r.data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ],
            minHeight : 200
        }
        r.semanas.map((value, index) => {
            props.header.push({
                key : `SEM ${value}`,
                name : ``,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
            })
        })
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-comparativo'))        
    }

    const initTablePeso = (r) => {
        var props = {
            header : [{
                   key : 'lugar',
                   name : '',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                   key : 'avg',
                   name : '',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                },{
                   key : 'max',
                   name : '',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                   key : 'min',
                   name : '',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                }
            ],
            data : r.data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table2.exportToExcel()
                    },
                    className : ''
                }
            ],
            minHeight : 200
        }
        r.semanas.map((value, index) => {
            props.header.push({
                key : `SEM ${value}`,
                name : ``,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
            })
        })
        $scope.table2 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-peso'))        
    }

    const initTableCluster = (r) => {
        var props = {
            header : [{
                   key : 'lugar',
                   name : '',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                   key : 'avg',
                   name : '',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                },{
                   key : 'max',
                   name : '',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                   key : 'min',
                   name : '',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                }
            ],
            data : r.data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table3.exportToExcel()
                    },
                    className : ''
                }
            ],
            minHeight : 200
        }
        r.semanas.map((value, index) => {
            props.header.push({
                key : `SEM ${value}`,
                name : ``,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
            })
        })
        $scope.table3 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-cluster'))        
    }

    const initTableDaños = (r) => {
        let props = {
            header : [{
                   key : 'lugar',
                   name : '',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                   key : 'avg',
                   name : '',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                },{
                   key : 'max',
                   name : '',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                   key : 'min',
                   name : '',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                }
            ],
            data : r.data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table4.exportToExcel()
                    },
                    className : ''
                }
            ],
            minHeight : 200
        }
        r.semanas.map((value, index) => {
            props.header.push({
                key : `SEM ${value}`,
                name : ``,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
            })
        })
        
        if($scope.table4){
            $("#table-daños").html("")
        }
        $scope.table4 = React.createElement(ReactDataGrid, props)
        ReactDOM.render($scope.table4, document.getElementById('table-daños'))
    }
    
    $scope.charts = {
        comparativo : new echartsPlv(),
        peso : new echartsPlv(),
        cluster : new echartsPlv(),
        defectos : new echartsPlv(),

        options_calidad : [],
        options_cluster : [],
        options_peso : [],
        options_defectos : []
    }

    $scope.init = (refresh) => {
        $request.index((r) => {
            option.series = r.series
            option.xAxis[0].data = r.legends[0]
            option.xAxis[1].data = r.legends[1]
            $scope.charts.options_calidad = angular.copy(option)

            option_c.series = r.grafica_cluster.series
            option_c.xAxis[0].data = r.legends[0]
            option_c.xAxis[1].data = r.legends[1]
            $scope.charts.options_cluster = angular.copy(option_c)

            option_p.series = r.grafica_peso.series
            option_p.xAxis[0].data = r.legends[0]
            option_p.xAxis[1].data = r.legends[1]
            $scope.charts.options_peso = angular.copy(option_p)

            option_defectos.series = r.grafica_defectos.series
            option_defectos.xAxis[0].data = r.legends[0]
            option_defectos.xAxis[1].data = r.legends[1]
            $scope.charts.options_defectos = angular.copy(option_defectos)

            $scope.defectos = r.defectos

            $scope.initGraficaCalidad()
            $scope.initGraficaPeso()
            $scope.initGraficaCluster()
            $scope.initGraficaDefectos()
        }, $scope.filters)
    }

    $scope.initGraficaCalidad = () => {
        var data = {
            semanas : [],
            data : [
                {
                    lugar : 'Lugar',
                    avg : 'AVG',
                    min : 'MIN',
                    max : 'MAX',
                    class : 'text-primary'
                },
                {
                    lugar : 'Ecuador',
                    class : 'text-primary'
                },
                {
                    lugar : 'Hamburgo',
                    class : 'text-success'
                },
                {
                    lugar : '',
                    class : 'text-success'
                }
            ]
        }
        var numSem = [];
        $scope.charts.options_calidad.xAxis[1].data.map((value, index) => {
            data.semanas.push(index)
            data.data[0]['SEM ' + index] = value
            data.data[1]['SEM ' + index] = $scope.charts.options_calidad.series[0].data[index]
        })

        $scope.charts.options_calidad.xAxis[0].data.map((value, index) => {
            data.data[2]['SEM ' + index] = $scope.charts.options_calidad.series[1].data[index]
            data.data[3]['SEM ' + index] = value
        })
        initTableCalidad(data)
        setTimeout(() => {
            $scope.charts.comparativo.init('comparativo', $scope.charts.options_calidad)
        }, 250)
    }

    $scope.initGraficaPeso = () => {
        var data = {
            semanas : [],
            data : [
                {
                    lugar : 'Lugar',
                    avg : 'AVG',
                    min : 'MIN',
                    max : 'MAX',
                    class : 'text-primary'
                },
                {
                    lugar : 'Ecuador',
                    class : 'text-primary'
                },
                {
                    lugar : 'Hamburgo',
                    class : 'text-success'
                },
                {
                    lugar : '',
                    class : 'text-success'
                }
            ]
        }
        var numSem = [];
        $scope.charts.options_peso.xAxis[1].data.map((value, index) => {
            data.semanas.push(index)
            data.data[0]['SEM ' + index] = value
            data.data[1]['SEM ' + index] = $scope.charts.options_peso.series[0].data[index]
        })

        $scope.charts.options_peso.xAxis[0].data.map((value, index) => {
            data.data[2]['SEM ' + index] = $scope.charts.options_peso.series[1].data[index]
            data.data[3]['SEM ' + index] = value
        })
        initTablePeso(data)
        setTimeout(() => {
            $scope.charts.peso.init('peso', $scope.charts.options_peso)
        }, 250)
    }

    $scope.initGraficaCluster = () => {
        var data = {
            semanas : [],
            data : [
                {
                    lugar : 'Lugar',
                    avg : 'AVG',
                    min : 'MIN',
                    max : 'MAX',
                    class : 'text-primary'
                },
                {
                    lugar : 'Ecuador',
                    class : 'text-primary'
                },
                {
                    lugar : 'Hamburgo',
                    class : 'text-success'
                },
                {
                    lugar : '',
                    class : 'text-success'
                }
            ]
        }
        var numSem = [];
        $scope.charts.options_cluster.xAxis[1].data.map((value, index) => {
            data.semanas.push(index)
            data.data[0]['SEM ' + index] = value
            data.data[1]['SEM ' + index] = $scope.charts.options_cluster.series[0].data[index]
        })

        $scope.charts.options_cluster.xAxis[0].data.map((value, index) => {
            data.data[2]['SEM ' + index] = $scope.charts.options_cluster.series[1].data[index]
            data.data[3]['SEM ' + index] = value
        })
        initTableCluster(data)
        setTimeout(() => {
            $scope.charts.cluster.init('cluster', $scope.charts.options_cluster)
        }, 250)
    }

    $scope.initGraficaDefectos = () => {
        var data = {
            semanas : [],
            data : [
                {
                    lugar : 'Lugar',
                    avg : 'AVG',
                    min : 'MIN',
                    max : 'MAX',
                    class : 'text-primary'
                },
                {
                    lugar : 'Ecuador',
                    class : 'text-primary'
                },
                {
                    lugar : 'Hamburgo',
                    class : 'text-success'
                },
                {
                    lugar : '',
                    class : 'text-success'
                }
            ]
        }
        var numSem = [];
        $scope.charts.options_defectos.xAxis[1].data.map((value, index) => {
            data.semanas.push(index)
            data.data[0]['SEM ' + index] = value
            data.data[1]['SEM ' + index] = $scope.charts.options_defectos.series[0].data[index]
        })

        $scope.charts.options_defectos.xAxis[0].data.map((value, index) => {
            data.data[2]['SEM ' + index] = $scope.charts.options_defectos.series[1].data[index]
            data.data[3]['SEM ' + index] = value
        })
        initTableDaños(data)

        var tab = $("#tab_daños")
        if(tab.hasClass("collapsed")){
            setTimeout(() => {
                $scope.charts.defectos.init('daños', $scope.charts.options_defectos)
            }, 250)
        }
    }

    $scope.init()

    $scope.subir = () => {
        if(!$scope.isSet){
            alert("Necesita seleccionar un archivo .xlsx primero")
        }else{
            $http({
                method: 'POST',
                url: '/phrapi/calidad/importar',
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                data: {
                    upload: $scope.file
                },
                transformRequest: function (data, headersGetter) {
                    var formData = new FormData();
                    angular.forEach(data, function (value, key) {
                        formData.append(key, value);
                    });
    
                    var headers = headersGetter();
                    delete headers['Content-Type'];
    
                    return formData;
                }
            })
            .success(function (data) {
                alert("Se subio correctamente", "Importar", "success", () => {
                    location.reload();
                })
            })
            .error(function (data, status) {
                alert("Ocurrio un error, intente nuevamente")
            });
        }
    }

    $('#input-file').bind('change', function() {
        $scope.isSet = true
        //alert('This file size is: ' + this.files[0].size/1024/1024 + "MB");
    });

}]);