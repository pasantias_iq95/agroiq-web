app.service('request', ['client', '$http', function(client, $http){
    this.lastDay = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/racimos/last'
        client.post(url, callback, data)
    }

    this.resumen = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/racimos/resumen'
        client.post(url, callback, data, 'promedios_lotes')
    }

    this.racimosEdad = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/produccion2/edad'
        client.post(url, callback, data, 'racimos_edad')
    }

    this.registros = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/racimos/historico'
        client.post(url, callback, data, 'registros')
    }

    this.eliminar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/racimos/eliminar'
        client.post(url, callback, data)
    }

    this.editar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/racimos/editar'
        client.post(url, callback, data, 'edit_registros')
    }

    this.analisisRecusados = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/racimos/recusados'
        client.post(url, callback, data)
    }

    this.tags = function(callback, params){
        let data = params || {}
        let url = 'phrapi/marun/racimos/tags'
        client.post(url, callback, data)
    }

    this.recusados = function(callback, params){
        load.block('racimos_recusados')
        $http.post('phrapi/marun/racimos/diarecusados', params || {})
        .then((r) => {
            load.unblock('racimos_recusados')
            callback(r.data)
        })
    }

    this.filters = function(callback, params){
        $http.post('phrapi/marun/produccion2/filters', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.muestreo = function(callback, params){
        $http.post('phrapi/marun/racimos/muestreo', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.pesoCalibre = function(callback, params){
        $http.post('phrapi/marun/racimos/pesoCalibre', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.pesoMano = function(callback, params){
        $http.post('phrapi/marun/racimos/pesoMano', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.pesoRacimo = function(callback, params){
        $http.post('phrapi/marun/racimos/pesoRacimo', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.cantidadRacimoCalibreManos = function(callback, params){
        $http.post('phrapi/marun/racimos/cantidadRacimoCalibreManos', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.viajes = function(callback, params){
        load.block('viajes')
        $http.post('phrapi/marun/racimos/viajes', params || {})
        .then((r) => {
            load.unblock('viajes')
            callback(r.data)
        })
    }

    this.borrarViaje = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/borrarViaje', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.saveViajes = function(params) {
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/saveViajes', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.moverViajes = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/moverViajes', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.moverViajesOtraFinca = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/moverViajesOtraFinca', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.moverCrearViajes = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/moverCrearViajes', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.borrarRacimosViajes = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/borrarRacimosViajes', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.cambiarPalanca = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/cambiarPalanca', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.cambiarLote = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/cambiarLote', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.saveEditRacimo = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/saveEditRacimo', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.getViajesFinca = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/getViajesFinca', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }

    this.desbloquearViajes = function(params){
        return new Promise((resolve) => {
            $http.post('phrapi/marun/racimos/desbloquear', params || {})
            .then((r) => {
                resolve(r.data)
            })  
        })
    }
}])

app.filter('num', function() {
    return function(input) {
        return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

app.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('countOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != undefined && value[key] != null && value[key] > 0){
                count++
            }
        });
        return count;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	let filtered = [];
    	angular.forEach(items, function(item){
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.directive('enter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.enter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('escape', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 27) {
                scope.$apply(function (){
                    scope.$eval(attrs.escape);
                });

                event.preventDefault();
            }
        });
    };
});

function number_format(amount, decimals) {
    
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

function checkUmbral (value, umbral) {
    if(value && umbral){
        if(value > umbral) return 'bg-red-thunderbird bg-font-red-thunderbird'
        else return 'bg-green-haze bg-font-green-haze'
    }
    return ''
}

function renderPie(id, _data){
    let options = {
        data : _data,
        nameseries : "Pastel",
        titulo : "",
        id : id
    }
    ReactDOM.render(React.createElement(Pastel, options), document.getElementById(id));
}

let datesEnabled = []
app.controller('produccion', ['$scope', 'request', function($scope, $request){

    datepickerHighlight = () => {
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $('#datepicker').datepicker()
        .on('changeDate', function(e) {
            $scope.changeRangeDate({ first_date: moment(e.date).format('YYYY-MM-DD'), second_date : moment(e.date).format('YYYY-MM-DD') })
        });
        $("#datepicker").datepicker('setDate', $scope.produccion.params.fecha_inicial)
        $('#datepicker').datepicker('update')
    }

    $scope.anioCambio = false
    $scope.id_company = 0;
    $scope.subTittle = ""
    $scope.registros = [];
    $scope.count = 0;
    $scope.grupo_mano = 1
    $scope.grupo_racimo = 3
    $scope.grupo_calibre = 0.2
    $scope.totales = {}
    $scope.tabla = {}

    $scope.produccion = {
        params : {
            finca : 1,
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
            fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            initial : 0,
        },
        nocache : function(){
            $request.filters((r) => {
                $scope.fincas = r.fincas
                if(Object.keys(r.fincas).length > 0){
                    let keys = Object.keys(r.fincas)
                    if(keys.indexOf($scope.produccion.params.finca) == -1 && $scope.produccion.params.finca != ''){
                        $scope.produccion.params.finca = keys[0]
                    }
                }

                $scope.getRegistros()
                if($scope.anioCambio){
                    $scope.getAnalisisRecusados()
                }
            }, $scope.produccion.params)
        }
    }

    $scope.startDate = ''
    $scope.getLastDay = function(){
        $request.lastDay(function(r, b){
            b()
            if(r){
                $scope.causas = r.causas
                $scope.fincas = r.fincas
                $scope.fincasAvailable = r.fincasAvailable

                $scope.produccion.params.fecha_inicial = r.last.fecha
                $scope.produccion.params.fecha_final = r.last.fecha
                $scope.produccion.params.finca = Object.keys(r.fincas)[0]
                $scope.table = {
                    fecha_inicial : r.last.fecha
                }
                $scope.startDate = r.last.fecha
                datesEnabled = r.days
                datepickerHighlight()

                $scope.getAnalisisRecusados()
                //$scope.produccion.nocache()
            }
        })
    }

    $scope.lastDay = {
        startDate : moment().startOf('month'),
        endDate : moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.anioCambio = moment(data.first_date).year() != moment($scope.produccion.params.fecha_inicial).year()

            $scope.produccion.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.produccion.params.fecha_inicial;
            $scope.produccion.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.produccion.params.fecha_final;
            $scope.subTittle = data.date_select || "Ultimo Registro"
        }
        $scope.produccion.nocache()
    }

    $scope.tabla = {
        produccion : []
    }

    $scope.enableEdit = (row, campo) => {
        if(!row.grupo_racimo > 0 || !row.form){
            if(campo == 'lote'){
                row.editingLote = true
            }
            else if(campo == 'cuadrilla'){
                row.editingCuadrilla = true
            }
        }
    }

    $scope.enter = (row, campo) => {
        if(campo == 'lote'){
            if(row.newLote == ''){
                alert("El lote no puede ser vacio")
            }else{
                $request.cambiarLote((r, b) => {
                    b()
                    if(r.status == 200){
                        row.lote = row.newLote
                        $scope.check = {}
                        $scope.escape(row, campo)
                    }else{
                        alert('Hubo un problema favor intentelo mas tarde')
                    }
                }, { grupo_racimo : row.grupo_racimo, 
                    lote : row.newLote, 
                    id_finca : $scope.produccion.params.finca,
                    id_formulario : row.id_formulario
                })
            }
        }else if(campo == 'cuadrilla'){
            if(row.newCuadrilla == ''){
                alert("La cuadrilla no puede estar vacia")
            }else{
                $request.cambiarCuadrilla((r, b) => {
                    b()
                    if(r.status == 200){
                        row.cuadrilla = row.newCuadrilla
                        $scope.check = {}
                        $scope.escape(row, campo)
                    }else{
                        alert('Hubo un problema favor intentelo mas tarde')
                    }
                }, { grupo_racimo : row.grupo_racimo, 
                    cuadrilla : row.newCuadrilla, 
                    id_finca : $scope.produccion.params.finca,
                    id_formulario : row.id_formulario 
                })
            }
        }
    }

    $scope.escape = (row, campo) => {
        if(campo == 'lote'){
            row.editingLote = false
        }
        else if(campo == 'cuadrilla'){
            row.editingCuadrilla = false
        }
    }

    $scope.eliminar = () => {
        var elements = $(".delete")
        if(elements.length > 0){
            if(confirm(`Seguro deseas eliminar ${elements.length} elementos?`)){
                var ids = []
                var index = []
                for(var x = 0; x < elements.length; x++){
                    ids.push({
                        id : $(elements[x]).attr('data-id')
                    })
                }
                $request.eliminar((r, b) => { 
                    b()
                    if(r){
                        $scope.getRegistros()
                        alert(`Se borraron ${elements.length} registros con éxito`, "", "success")
                    }
                }, { ids : ids })
            }
        }else{
            alert("No has seleccionado ninguna fila")
        }
    }

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];
    $scope.graficas_add = {
        manos : [],
        calibre_segunda : [],
        calibre_ultima : [],
        var : 'manos'
    }

    $scope.getRegistros = function(){
        $scope.cargandoHistorico = true
        let data = $scope.produccion.params;

        $request.racimosEdad(function(r, b){
            b('racimos_edad')
            if(r){
                $scope.tabla.edades = r.data;
            }
        }, data)

        $request.registros(function(r, b){
            b('registros')
            $scope.cargandoHistorico = false

            if(r){
                $scope.registros = r.data
                $scope.viajes = r.viajes
                $scope.searchTable.changePagination()
            }
        }, data)

        $scope.getCuadreViajes()

        $request.resumen(function(r, b){
            b('promedios_lotes')
            if(r){
                $scope.resumen = r.data
                $scope.resumen_totales = r.totales
                $scope.edades = r.edades
            }
        }, data)

        $request.tags(function(r, b){
            b()
            if(r){
                $scope.tags = r.tags
            }
        }, data)

        $request.recusados((r) => {
            if(r.data.length > 0){
                $scope.recusados = r.data

                var data_chart = r.data.map((r) => {
                    return  {
                        label : r.causa,
                        value : r.cantidad
                    }
                })
                renderPie('grafica-defectos', data_chart)
            }else{
                $scope.recusados = []
                renderPie('grafica-defectos', [ { label : 'SIN CAUSAS', value : 0 }])
            }
        }, data)

        if($scope.produccion.params.initial == 0){
            $request.muestreo((r) => {
                $scope.muestreo = r.data
                $scope.openMuestreo()
            }, $scope.produccion.params)

            $scope.openGraficasAdicionales()
        }
    }

    $scope.openMuestreo = () => {
        setTimeout(() => {
            if($("[href=#collapse_3_6]").attr('aria-expanded') == 'true'){
                new echartsPlv().init('grafica-muestreo', $scope.muestreo)
            }
        }, 250)
    }

    $scope.openGraficasAdicionales = () => {
        setTimeout(() => {
            if($("[href=#collapse_3_10]").attr('aria-expanded') == 'true'){
                $request.pesoMano((r) => {
                    $scope.pesoManoTendencia = r.data
                    $scope.pesoMano = r.dia
                    
                    new echartsPlv().init('grafica-peso-mano-tendencia', $scope.pesoManoTendencia)
                    new echartsPlv().init('grafica-peso-mano', $scope.pesoMano)
                }, Object.assign(
                    { grupo : $scope.grupo_mano },
                    $scope.produccion.params
                ))
    
                $request.pesoCalibre((r) => {
                    $scope.pesoCalibreTendencia = r.data
                    $scope.pesoCalibre = r.dia

                    new echartsPlv().init('grafica-peso-calibre-tendencia', $scope.pesoCalibreTendencia)
                    new echartsPlv().init('grafica-peso-calibre', $scope.pesoCalibre)
                }, Object.assign(
                    { grupo : $scope.grupo_calibre },
                    $scope.produccion.params
                ))

                $request.pesoRacimo((r) => {
                    $scope.pesoRacimo = r.dia
                    $scope.pesoManosPeso = r.vs_manos

                    new echartsPlv().init('grafica-peso-racimo', $scope.pesoRacimo)
                    new echartsPlv().init('grafica-manos-peso', $scope.pesoManosPeso)
                }, Object.assign(
                    { grupo : $scope.grupo_racimo },
                    $scope.produccion.params
                ))

                $request.cantidadRacimoCalibreManos((r) => {
                    $scope.graficas_add.manos = r.manos
                    $scope.graficas_add.calibre_segunda = r.calibre_segunda
                    $scope.graficas_add.calibre_ultima = r.calibre_ultima
                    $scope.renderCantidad()
                }, Object.assign(
                    $scope.produccion.params
                ))
            }
        }, 250)
    }

    $scope.renderCantidad = () => {
        new echartsPlv().init('grafica-cantidad-manos-calibre', $scope.graficas_add[$scope.graficas_add.var])
    }

    $scope.edit = function(row){
        if(!$scope.editing){
            $scope.editing = true;
            $scope.editRow = row;
            $scope.editRow.editing = true;
        }
    }

    $scope.guardar = function(){
        var valid = true
        if(!$scope.editing) valid = false;
        //if($scope.editRow.peso <= 0) valid = false;
        if($scope.editRow.cuadrilla == "") valid = false;
        if($scope.editRow.lote == "") valid = false;
        if($scope.editRow.edad == "") valid = false;
        
        if(valid){
            $scope.editRow.editing = false;
            $scope.editing = false;
            $request.editar((r, b) => {
                b('edit_registros')
                if(r){
                    $scope.editRow.class = r.class
                }else{
                    alert("Ocurrio un error al guardar")
                }
            }, $scope.editRow)
        }else{
            alert("Favor de completar los campos")
        }
    }

    $scope.next = function(dataSource){
        if($scope.searchTable.actual_page < parseInt(dataSource.length / parseInt($scope.searchTable.limit)) + (dataSource.length % parseInt($scope.searchTable.limit) == 0 ? 0 : 1)) 
            $scope.searchTable.actual_page++;
        
        $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
    }

    $scope.prev = function(dataSource){
        if($scope.searchTable.actual_page > 1)
            $scope.searchTable.actual_page--;
        
        $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
    }

    $scope.setOrderTable = function(field){
        var reverse = false
        if(field == $scope.searchTable.orderBy){
            reverse = !$scope.searchTable.reverse
        }else{
            $scope.searchTable.orderBy = field
        }
        $scope.searchTable.reverse = reverse
    }

    $scope.searchTable = {
        orderBy : "hora",
        reverse : false,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        changePagination : function(){
            if($scope.searchTable.limit == 0)
                $scope.searchTable.limit = $scope.registros.length

            $scope.searchTable.numPages = parseInt($scope.registros.length / $scope.searchTable.limit)  + ($scope.registros.length % $scope.searchTable.limit == 0 ? 0 : 1)
            $scope.searchTable.actual_page = 1
            $scope.searchTable.startFrom = 0
        }
    }

    $scope.exportExcel = function(id_table, title){

        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, 'Base de datos racimos.xls')
    }

    $scope.exportPrint = function(id_table){
        $("#"+id_table).css("display", "")
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
        $("#"+id_table).css("display", "none")
    }

    $scope.exportarFormatoEspecial = () => {
        dayOfWeek = (day) => { return ["DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO"][day]  };
        embedInRow = (cols) => { return `<tr>${cols}</tr>` };
        generateRow = () => { var cols = []; for(var i = 0; i < 10; i ++) { cols.push("<td></td>") } return cols; };
        getCintaValida = (cinta) => { return { VERDE : 'VERDE', ROJA : 'ROJA', AMARILLO : 'AMARI', CAFE  : 'CAFE', AZUL : 'AZUL', BLANCO : 'BLANC', NEGRO : 'NEGRA', LILA : 'LILA' }[cinta]; }
        var table = []
        
        row = generateRow();
        row[0] = `<td>Archivo no.: ${moment($scope.produccion.params.fecha_inicial).format('DDD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Nombre: SEMANA ${moment($scope.produccion.params.fecha_inicial).format('WW')} ${dayOfWeek(moment($scope.produccion.params.fecha_inicial).format('d'))} ${moment($scope.produccion.params.fecha_inicial).format('DD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Fecha: ${moment($scope.produccion.params.fecha_inicial).format('DD-MM-YYYY')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        table.push([
            "<tr>",
            "<td>F01IDV(4)isID</td>",
            "<td>DW2Peso()</td>",
            "<td>C03LOTE()</td>",
            "<td>C13CINTA()</td>",
            "<td>F12MANOS(##)notID</td>",
            "<td>F22CALIBRE(##.##)notID</td>",
            "<td>F32L DEDOS(##.##)notID</td>",
            "<td>F43TIPO()notID</td>",
            "<td>F53NIVEL()notID</td>",
            "<td>F63CAUSA()notID</td>",
            "</tr>"
        ].join(""))

        table.push([
            "<tr>",
            "<td>IDV</td>",
            "<td>Peso</td>",
            "<td>LOTE</td>",
            "<td>CINTA</td>",
            "<td>MANOS</td>",
            "<td>CALIBRE</td>",
            "<td>L DEDOS</td>",
            "<td>TIPO</td>",
            "<td>NIVEL</td>",
            "<td>CAUSA</td>",
            "</tr>"
        ].join(""))

        $scope.registros.map((value, index) => {
            if(parseInt(value.id) > 0){
                table.push([
                    "<tr>",
                    `<td>${value.id}</td>`,
                    `<td>${value.peso}</td>`,
                    `<td>${value.lote}</td>`,
                    `<td>${getCintaValida(value.cinta)}</td>`,
                    `<td>${(value.manos > 0) ? value.manos : ''}</td>`,
                    `<td>${(value.calibre > 0) ? value.calibre : ''}</td>`,
                    `<td>${(value.dedos > 0) ? value.dedos : ''}</td>`,
                    `<td></td>`,
                    `<td>${(value.tipo == 'RECU') ? 'RECHA' : ''}</td>`,
                    `<td>${(value.tipo == 'RECU') ? value.causa : ''}</td>`,
                    "</tr>"
                ].join(""))
            }
        })

        // Añadie cuadre
        if($scope.dia_cuadrado){
            $scope.cuadre_proc.map((value, index) => {
                var diferencia = value.form - value.blz 
                for(var i = 0; i < diferencia; i++){
                    table.push([
                        "<tr>",
                        `<td></td>`,
                        `<td>${value.peso_prom}</td>`,
                        `<td>${value.lote}</td>`,
                        `<td>${getCintaValida(value.cinta)}</td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        "</tr>"
                    ].join(""))
                }
            })

            $scope.faltante_recu.map((value, index) => {
                var diferencia = value.cantidad - value.cantidad_blz 
                for(var i = 0; i < diferencia; i++){
                    table.push([
                        "<tr>",
                        `<td></td>`,
                        `<td>${value.peso_prom}</td>`,
                        `<td>${value.lote}</td>`,
                        `<td>${getCintaValida(value.color_cinta)}</td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td>RECHA</td>`,
                        `<td>${value.causa}</td>`,
                        "</tr>"
                    ].join(""))
                }
            })
        }
        
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function() {
                var contentTable = table.join("")
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel()
    }

    $scope.getAnalisisRecusados = () => {
        $request.analisisRecusados((r, b) => {
            b()
            r.data.map((value) => {
                if(value.dano == 'TOTAL') $scope.umbralRecusados = value.prom
            })
            printTableRecusados(r.data, r.semanas)
        }, $scope.produccion.params)
    }

    var printTableRecusados = (data, semanas) => {
        let id = 'table-defectos'
        var props = {
            header : [{
                   key : 'dano',
                   name : 'DEFECTO',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                    key : 'sum',
                    name : 'SUM',
                    locked : true,
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    formatter : 'Number',
                    resizable : true,
                    customCell : function(rowData, isChildren){
                        var valNumber = parseFloat(rowData['sum'])
                        if($scope.produccion.params.var_recusado == 'porc') valNumber = 0
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                             <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                 ${valueCell}
                             </div>
                        `;
                     }
                 },{
                   key : 'prom',
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['prom'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((value) => {
            props.header.push({
                key : `sem_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+value])
                    let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                    return `
                        <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }

    /** BEGIN CUADRE DE RACIMOS */
    $scope.dia_finalizado = false
    $scope.getCuadreViajes = () => {
        $request.viajes(function(r){
            $scope.dia_finalizado = r.dia_finalizado == 1
            $scope.racimosViajes = r.data
            $scope.lotes = r.lotes
        }, $scope.produccion.params)
    }

    $scope.config = {
        mode_process : 2,
        valid_lote : false,
        valid_cinta : false,
    }

    $scope.coloresClass = {
        'VERDE' : 'bg-green-haze bg-font-green-haze',
        'AZUL' : 'bg-blue bg-font-blue',
        'BLANCO' : 'bg-white bg-font-white',
        'LILA' : 'bg-purple-studio bg-font-purple-studio',
        'ROJA' : 'bg-red-thunderbird bg-font-red-thunderbird',
        'CAFE' : 'bg-brown bg-font-brown',
        'AMARILLO' : 'bg-yellow-lemon bg-font-yellow-lemon',
        'NEGRO' : 'bg-dark bg-font-dark',
        'CELESTE' : 'bg-blue-sharp bg-font-blue-sharp',
        '' : 'bg-white bg-font-white',
        'S/C' : 'bg-white bg-font-white'
    }
    $scope.numbers = []
    $scope.generateNumbers = (num = 20) => {
        let arr = []
        for(let i = 1; i <= num; i++){
            arr.push(i)
        }
        setTimeout(() => {
            $scope.$apply()
        }, 100)
        return arr
    }
    $scope.numbers = $scope.generateNumbers()
    $scope.check = {}
    $scope.racimosSelected = []

    $scope.selectCheck = (index, row) => {
        if($scope.check.hasOwnProperty(`${index}`)){
            if(!$scope.check[index]){
                delete $scope.check[index]
                let type = row.blz_viaje > 0 ? 'BLZ' : 'FORM'
                $scope.select[type] = true
                row.selected = false
            }
            else {
                let type = row.form_viaje > 0 ? 'BLZ' : 'FORM'
                $scope.select[type] = false
                row.selected = true
            }
        }
        
        var keys = Object.keys($scope.check)
        if(keys.length == 2){
            if(confirm('¿Seguro que quieres unir estos dos viajes?')){
                var balanza, formulario
                if($scope.racimosViajes[keys[0]].blz > 0){
                    balanza = angular.copy($scope.racimosViajes[keys[0]])
                    formulario  = angular.copy($scope.racimosViajes[keys[1]])
                }else{
                    formulario = angular.copy($scope.racimosViajes[keys[0]])
                    balanza  = angular.copy($scope.racimosViajes[keys[1]])
                }

                if(balanza.lote == formulario.lote && balanza.cuadrilla == formulario.cuadrilla){
                    $request.unirBalanzaFormulario((r, b) => {
                        b()
                        if(r.status == 200){
                            alert("Se unieron correctamente", "", "success")
                            $scope.select = { BLZ : true, FORM : true }
                            $scope.check = {}
                            $scope.getViajes()
                        }else{
                            alert("Ocurrio algun error, favor de intentar mas tarde")
                        }
                    }, 
                    { 
                        grupo_racimo : balanza.grupo_racimo, 
                        id_formulario : formulario.id_formulario, 
                        id_finca : $scope.produccion.params.finca ,
                        lote : balanza.lote
                    })
                }else{
                    alert("Para poder unir los viajes deben ser del mismo lote y cuadrilla")
                }
            }
        }else if(keys.length > 2){
            alert("Solo debes elegir 2 viajes", "", "warning")
        }
    }

    $scope.editarViaje = (viaje, index) => {
        $scope.selected = { blz : 0, form : 0}
        $scope.racimosSelected = []

        $("#viajes-modal").modal("show")

        if(viaje.form_racimos > 20) 
            $scope.numbers = $scope.generateNumbers(viaje.form_racimos)
        else if(viaje.blz_racimos > 20)
            $scope.numbers = $scope.generateNumbers(viaje.blz_racimos)
        else
            $scope.numbers = $scope.generateNumbers(20)

        $scope.viajeSelected = angular.copy(viaje)
    }

    $scope.borrarViaje = (viaje, index) => {
        swal({
            text: '¿Seguro de borrar el viaje?',
            buttons : ['No', 'Si']
        })
        .then((conf) => {
            if(conf){
                $request.borrarViaje({
                    id_viaje : viaje.id
                })
                .then((r) => {
                    if(r.status == 200){
                        swal("Guardado", "Se ha guardado con éxito", "success");
                        $scope.getCuadreViajes()
                    }else{
                        swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                    }
                })
            }
        })
    }

    $scope.guardarRacimosViajes = () => {
        $("#viajes-modal").modal("hide")
        swal({
            text: '¿Seguro de hacer guardar?',
            buttons : ['No', 'Si']
        })
        .then(r => {
            if (!r) throw null;
            return $request.saveViajes()
        })
        .then(r => {
            if(r.status == 200){
                swal("Guardado", "Se ha guardado con éxito", "success");
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.encontrarDiferenciasTodos = (viaje) => {
        return false
        let rac = []
        if(viaje.form_racimos > 20) 
            rac = $scope.generateNumbers(viaje.form_racimos)
        else if(viaje.blz_racimos > 20)
            rac = $scope.generateNumbers(viaje.blz_racimos)
        else
            rac = $scope.generateNumbers(20)

        for(let i in rac){
            if($scope.encontrarDiferencias(viaje.balanza[parseInt(i)+1], viaje.formulario[parseInt(i)+1])) return true
        }
        return false
    }

    $scope.encontrarDiferencias = (blz, form) => {
        if(blz && form){
            if($scope.config.valid_cinta) if(blz.tipo != form.tipo) return true
            if($scope.config.valid_cinta) if(blz.cinta != form.cinta) return true
        }
        return false
    }

    $scope.selected = { blz : 0, form : 0}
    $scope.changeChecked = (tipo, val, id) => {
        if(val){
            $scope.selected[tipo]++
            $scope.racimosSelected.push(id)
        }else{
            $scope.selected[tipo]--
            $scope.racimosSelected.splice($scope.racimosSelected.indexOf(id), 1)
        }
        setTimeout(() => {
            $scope.$apply()
        }, 10)
    }
    
    $scope.backRacimo = (num_racimo, _racimo) => {
        for(let i in $scope.racimosViajes){
            let _v = $scope.racimosViajes[i]
            if(_v.id == $scope.viajeSelected.id){
                $scope.viajeSelected.formulario[num_racimo] = angular.copy(_v.formulario[num_racimo])
                setTimeout(() => {
                    $scope.$apply()
                }, 10)
                break;
            }
        }
    }

    $scope.saveEditRacimo = (num_racimo, racimo) => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((r) => {
            if(r){
                return $request.saveEditRacimo({
                    racimo,
                    num_racimo,
                    viaje : $scope.viajeSelected
                })
            }
        })
        .then((r) => {
            if(r.status == 200){
                swal("Guardado", "Se ha guardado con éxito", "success");
                racimo.editing = false
                $scope.getCuadreViajes()
            }else if(r.message){
                swal("Error", r.message, "error");
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.selectAll = (tipo) => {
        $scope.selected[tipo == 'balanza' ? 'blz' : 'form'] = Object.keys($scope.viajeSelected[tipo]).length
        $scope.racimosSelected = []
        for(let i in $scope.viajeSelected[tipo]){
            $scope.racimosSelected.push($scope.viajeSelected[tipo][i].id_racimo)
            $scope.viajeSelected[tipo][i].selected = true
        }
    }

    $scope.diselectAll = (tipo) => {
        $scope.selected[tipo == 'balanza' ? 'blz' : 'form'] = 0
        for(let i in $scope.viajeSelected[tipo]){
            $scope.viajeSelected[tipo][i].selected = false
        }
    }

    $scope.crearRacimosViajes = () => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((r) => {
            if(r){
                $("#viajes-modal").modal('hide')
                $("#crear-viaje-racimos-modal").modal('show')
            }
        })
    }

    $scope.borrarRacimosViajes = () => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((r) => {
            if(r){
                return $request.borrarRacimosViajes({ tipo : $scope.selected.blz ? 'BLZ' : 'FORM', racimos : $scope.racimosSelected, id_viaje : $scope.viajeSelected.id })
            }
        })
        .then((r) => {
            if(r.status == 200){
                swal("Guardado", "Se ha guardado con éxito", "success");
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
            $("#viajes-modal").modal('hide')
            $scope.getCuadreViajes()
        })
    }

    $scope.moverRacimosViajes = () => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((r) => {
            if(r){
                $("#viajes-modal").modal('hide')
                $("#mover-racimos-modal").modal('show')
            }
        })
    }

    $scope.moverRacimosFinca = () => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((r) => {
            if(r){
                $("#viajes-modal").modal('hide')
                $("#mover-finca-racimos-modal").modal('show')
            }
        })
    }

    $scope.getViajesFinca = () => {
        $request.getViajesFinca({ fecha : $scope.produccion.params.fecha_inicial, id_finca : $scope.to_id_finca })
        .then((r) => {
            $scope.racimosViajesFinca = r.data
            $scope.$apply()
        })
    }

    $scope.guardarMoverRacimosViajes = () => {
        load.block('mover-racimos-modal')
        $request.moverViajes({ tipo : $scope.selected.blz ? 'BLZ' : 'FORM', racimos : $scope.racimosSelected, to_id_viaje : $scope.to_id_viaje })
        .then((r) => {
            $("#mover-racimos-modal").modal('hide')
            load.unblock('mover-racimos-modal')
            if(r.status == 200){
                swal("Guardado", "Se ha guardado con éxito", "success");
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
            $scope.getCuadreViajes()
        })
    }

    $scope.guardarMoverRacimosViajesOtraFinca = () => {
        load.block('mover-finca-racimos-modal')
        $request.moverViajesOtraFinca({ to_id_finca : $scope.to_id_finca, tipo : $scope.selected.blz ? 'BLZ' : 'FORM', racimos : $scope.racimosSelected, to_id_viaje : $scope.to_id_viaje_otra_finca })
        .then((r) => {
            $("#mover-finca-racimos-modal").modal('hide')
            load.unblock('mover-finca-racimos-modal')
            if(r.status == 200){
                swal("Guardado", "Se ha guardado con éxito", "success");
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
            $scope.getCuadreViajes()
        })
    }

    $scope.guardarCrearViaje = () => {
        let num = $("#numero-crear-viaje").val()
        if(num){
            $request.moverCrearViajes({ 
                fecha : $scope.produccion.params.fecha_inicial,
                tipo : $scope.selected.blz ? 'BLZ' : 'FORM', 
                racimos : $scope.racimosSelected, 
                numero : num,
                id_finca : $scope.produccion.params.finca
            })
            .then((r) => {
                if(r.status == 200){
                    swal("Guardado", "Se ha guardado con éxito", "success");
                }else{
                    swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                }
                $("#crear-viaje-racimos-modal").modal('hide')
                $scope.getCuadreViajes()
            })
        }else{
            toastr.error('Coloca el número de viaje')
        }
    }

    $scope.cambiarPalanca = (viaje) => {
        $("#cambiar-palanca-modal").modal('show')
        $scope.viajeSelected = angular.copy(viaje)
    }

    $scope.guardarCambiarPalanca = () => {
        let palanca = $("#numero-palanca-viaje").val()
        if(palanca){
            load.block('cambiar-palanca-modal')
            $request.cambiarPalanca({ 
                palanca,
                id_viaje : $scope.viajeSelected.id
            })
            .then((r) => {
                load.unblock('cambiar-palanca-modal')
                if(r.status == 200){
                    swal("Guardado", "Se ha guardado con éxito", "success");
                }else{
                    swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                }
                $("#cambiar-palanca-modal").modal('hide')
                $scope.getCuadreViajes()
            })
        }else{
            toastr.error('Coloca el número de palanca')
        }
    }

    $scope.cambiarLote = (viaje) => {
        $("#cambiar-lote-modal").modal('show')
        $scope.viajeSelected = angular.copy(viaje)
    }

    $scope.guardarCambiarLote = () => {
        let lote = $("#numero-lote-viaje").val()
        if(lote){
            load.block('cambiar-lote-modal')
            $request.cambiarLote({ 
                lote,
                id_viaje : $scope.viajeSelected.id
            })
            .then((r) => {
                load.unblock('cambiar-lote-modal')
                if(r.status == 200){
                    swal("Guardado", "Se ha guardado con éxito", "success");
                }else{
                    swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                }
                $("#cambiar-lote-modal").modal('hide')
                $scope.getCuadreViajes()
            })
        }else{
            toastr.error('Coloca el número de lote')
        }
    }

    $scope.fincaHasLote = (finca) => {
        let tipo = $scope.selected.blz ? 'BLZ' : 'FORM'
        let lote_selected = tipo == 'BLZ' ? $scope.viajeSelected.blz_lote : $scope.viajeSelected.form_lote

        for(let i in finca.lotes){
            let lote = finca.lotes[i]
            if(lote_selected == lote.nombre){
                return 1
            }
        }
        return 0
    }

    $scope.desbloquearViajes = () => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((r) => {
            if(r){
                $request.desbloquearViajes({
                    fecha : $scope.produccion.params.fecha_inicial
                })
                .then((r) => {
                    if(r.status === 200){
                        $scope.getCuadreViajes()
                    }
                })
            }
        })
    }

}]);

