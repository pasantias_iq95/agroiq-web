app.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            if(parseFloat(value[key]))
                sum = sum + parseFloat(value[key], 10);
        });        
        return sum;
    }
})

app.service('cajas', ['client', function(){
    this.index = function(callback, params){
        let url = 'phrapi/cajas/index'
        let data = params || {}
        client.post(url, callback, data, 'content')
    }
}])

app.controller('produccion', ['$scope','$http','$interval','$controller','$timeout' ,'$window','$filter', function($scope,$http,$interval,$controller , $timeout,$window,$filter){
    
    $scope.produccion_questions = [
        {
            name : 'Racimos cosechados'
        },
        {
            name : 'Racimos recusados'
        },
        {
            name : 'Racimos procesados'
        },
        {
            name : 'Cajas de primera'
        },
        {
            name : 'Cajas de segunda'
        },
        {
            name : 'Total cajas'
        },
        {
            name : 'Conversión cajas'
        },
        {
            name : 'Enfunde'
        }
    ]

    $scope.promedios_questions = [
        {
            name : '% Recusados'
        },
        {
            name : 'Peso prom racimos'
        },
        {
            name : 'Calibre promedio'
        },
        {
            name : 'Manos promedio'
        },
        {
            name : 'Largo dedos prom'
        },
        {
            name : 'Ratio cortado'
        },
        {
            name : 'Ratio procesado'
        },
        {
            name : 'Merma cortada'
        },
        {
            name : 'Merma procesada'
        },
        {
            name : 'Merma neta'
        },
        {
            name : '% Recobro'
        },
        {
            name : 'Edad prom cosecha'
        }
    ]

}]);

