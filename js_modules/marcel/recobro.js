app.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            if(parseFloat(value[key]))
                sum = sum + parseFloat(value[key], 10);
        });        
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('orderBy', function() {
	return function(items, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a) && parseFloat(b)){
				return (parseFloat(a) > parseFloat(b) ? 1 : -1);
			}else{
				return (a > b ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.service('request', ['$http', function($http){
    this.getData = function(callback, params){
        let url = 'phrapi/marcel/recobro/data'
        let data = params || {}

        load.block('data-table')
        $http.post(url, data).then((r) => {
            callback(r.data)
            load.unblock('data-table')
        })
        .catch(() => {
            load.unblock('data-table')
        });
    }

    this.index = (callback, params) => {
        $http.post('phrapi/marcel/resumenenfunde/index', params || {}).then((r) => {
            callback(r.data)
        })
    }

    this.last = (callback, params) => {
        $http.post('phrapi/marcel/resumenenfunde/last', params ||  {}).then((r) => {
            callback(r.data)
        })
    }

    this.saveCaidos = (callback, params) => {
        $http.post('phrapi/marcel/recobro/saveCaidos', params ||  {}).then((r) => {
            callback(r.data)
        })
    }

    this.papiro = (callback, params) => {
        load.block('papiro')
        $http.post('phrapi/marcel/recobro/papiro', params ||  {}).then((r) => {
            callback(r.data)
            load.unblock('papiro')
        })
    }
}])

app.controller('produccion', ['$scope', 'request', '$filter', function($scope, $request, $filter){

    $scope.tabActive = 'recobro'
    $scope.totales = {}
    $scope.filters = {
        year : moment().year(),
        week : moment().week(),
        id_finca : 1
    }
    $scope.lotesload = true

    $scope.newCaidos = {
        lote : '',
        cinta: '',
        enfunde : ''
    }

    $scope.coloresClass = {
        'VERDE' : 'bg-green-haze bg-font-green-haze',
        'AZUL' : 'bg-blue bg-font-blue',
        'BLANCO' : 'bg-white bg-font-white',
        'LILA' : 'bg-purple-studio bg-font-purple-studio',
        'ROJA' : 'bg-red-thunderbird bg-font-red-thunderbird',
        'CAFE' : 'bg-brown bg-font-brown',
        'AMARILLO' : 'bg-yellow-lemon bg-font-yellow-lemon',
        'NEGRO' : 'bg-dark bg-font-dark'
    }

    $scope.init = () => {
        $scope.lotesload = true
        load.block('saldo_fundas')

        $request.index((r) => {
            load.unblock('saldo_fundas')
            $scope.lotesload = false
            
            $scope.colorClass = r.color.class
            $scope.edades = r.edades
            $scope.data = r.data
            $scope.fincas = r.fincas
            $scope.lotes_caidos = r.lotes
            $scope.reloadTotales()

        }, $scope.filters)
    }

    const getUmbral = (value) => {
        if(value){
            if(value < 98)
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            else if (value == 98)
                return 'bg-yellow-gold bg-font-yellow-gold'
            else
                return 'bg-green-haze bg-font-green-haze'
        }
    }
    $scope.getUmbral = getUmbral

    $scope.getDataTable = ( ) => {
        $request.getData((r) => {
            $scope.lotes = r.lotes

            r.data.map((row) => {
                Object.keys(row).map((col) => {
                    if(col != 'sem_enf')
                    if(col.includes('sem_')){
                        if(row[col] == 0) row[col] = ''
                    }
                })
            })

            initTable(r.data, r.semanas_edad)
            initPapiro()
        }, $scope.filters)
    }

    const initTable = (data, semanas) => {
        var props = {
            header : [{
                   key : 'sem_enf',
                   name : 'SEM/ENF',
                   titleClass : 'text-center',
                   alignContent : 'center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 100,
                   customCell : (rowData) => {
                        let valueCell = rowData[`sem_enf`]
                        return `
                            <div class="text-center ${rowData['class']}" style="height: 100%; padding: 5px;">
                                ${valueCell}
                            </div>
                        `
                   }
                },{
                   key : 'enfunde',
                   name : 'RAC ENF',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'center',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((sem) => {
            props.header.push({
                key : `sem_${sem}`,
                name : `${sem}`,
                sortable : true,
                titleClass : 'text-center',
                alignContent : 'center',
                filterRenderer: 'NumericFilter',
                formatter : 'Number',
                resizable : true,
                width: 50
             })
        })
        props.header.push({
            key : `total`,
            name : `TOTAL COSE`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `saldo`,
            name : `SALDO`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `rec`,
            name : `%`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `racimos_caidos`,
            name : `CAIDOS`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `porc_caidos`,
            name : `% CAIDOS`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        props.header.push({
            key : `porc_total`,
            name : `% RECOBRO`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
            customCell : function(rowData){
                let valueCell = parseFloat(rowData[`porc_total`])
                let textCell = (valueCell > 0) ? valueCell : ''
                let umbral = getUmbral(valueCell)
                return `
                    <div class="text-center ${umbral}" style="height: 100%; padding: 5px;">
                        ${textCell}
                    </div>
                `;
            }
        })
        props.header.push({
            key : `no_recuperable`,
            name : `NO RECUP`,
            sortable : true,
            titleClass : 'text-center',
            alignContent : 'center',
            filterable : true,
            filterRenderer: 'NumericFilter',
            formatter : 'Number',
            resizable : true,
        })
        $("#data-table").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('data-table'))
    }

    const initPapiro = () => {
        $request.papiro((r) => {
            renderPapiro(r.semanas_enfunde, r.data)
        }, $scope.filters)
    }

    const renderPapiro = (semanas_enfunde, data) => {
        let columns = [
            {
                key: 'descripcion',
                name: 'DESCRIP',
                filterable: true,
                //filterRenderer: AutoCompleteFilter,
                sortable : true,
                resizable : true,
                locked : true,
                width : 140
            },
        ]
        semanas_enfunde.map((sem, i) => {
            columns.push({
                key : `enfunde_${sem.year}_${sem.week}`,
                name : `${sem.year} ${sem.week}`,
                sortable : true,
                resizable : true,
                //formatter: RecobroFormatter,
                width : 150,
                customCell : (rowData) => {
                    const { edad, cantidad, porcentaje, class_color, cinta, tipo } = rowData[`enfunde_${sem.year}_${sem.week}`]
                    let classes = `${class_color && cinta ? "text-center" : "text-right"} ${tipo === 'Recobro' ? getUmbral(porcentaje) : ''}`
                    return `
                        <div class="${classes}">
                            ${cantidad > 0 || porcentaje > 0
                                ?   `<div>
                                        ${edad ? `<span>(${edad})</span>` : ''} ${cantidad ? `<span>${cantidad}</span>` : ''} ${porcentaje ? `<b>${ porcentaje }%</b>` : ''}
                                    </div>`
                                : `${class_color && cinta
                                    ? `<div class="${class_color}">${cinta}</div>`
                                    : ''
                                }`
                            }
                        </div>
                    `
                }
            })
        })
        columns.push({
            key: 'total',
            name: 'TOTAL',
            filterable: true,
            //filterRenderer: NumericFilter,
            sortable : true,
            resizable : true
        })

        $("#papiro").html("")
        $scope.table2 = ReactDOM.render(React.createElement(ReactDataGrid, { header : columns, data : data, buttons : [
            {
                title : 'Excel',
                action : () => {
                    $scope.table2.exportToExcel()
                },
                className : ''
            }
        ] }), document.getElementById('papiro'))
    }

    $scope.reloadTotales = () => {
        setTimeout(() => {
            $scope.$apply(() => {
                $scope.totales.racimos_enfunde = $filter('sumOfValue')($scope.data, 'racimos_enfunde')
                $scope.totales.recobro = $scope.totales.cosechados > 0 && $scope.totales.racimos_enfunde > 0 ? ($scope.totales.cosechados / $scope.totales.racimos_enfunde * 100) : 0
                $scope.totales.caidos = $filter('sumOfValue')($scope.data, 'caidos') || 0
                $scope.totales.porc_caidos = $scope.totales.caidos > 0 && $scope.totales.racimos_enfunde > 0 ? ($scope.totales.caidos / $scope.totales.racimos_enfunde * 100) : 0
            })
        }, 100)
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.years = r.years
            $scope.semanas = r.weeks
            $scope.filters.week = r.last_week
            $scope.fincas = r.fincas
            $scope.init()
        }, $scope.filters)
    }

    $scope.saveCaidos = () => {
        if($scope.newCaidos.lote != '' && $scope.newCaidos.cinta != '' && $scope.newCaidos.enfunde != '' && $scope.newCaidos.cantidad > 0){
            let [year, week] = $scope.newCaidos.enfunde.split('-')
            $request.saveCaidos((r) => {
                if(r.status === 200){
                    $("#caidos-modal").modal('hide')
                    alert(`Guardado con éxito`, ``, `success`)
                    $scope.getDataTable()
                    $scope.init()
                }else{
                    alert(`Paso algo inesperado, contacte a soporte`)
                }
            }, 
            Object.assign($scope.newCaidos, { year, week }))
        }else{
            alert(`Favor de llenar los campos requeridos`)
        }
    }

    $scope.last()
    $scope.getDataTable()
}]);

