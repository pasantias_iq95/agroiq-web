app.controller('personal_tthh', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){
	
	$scope.idPersonal = "<?=$_GET['idPersonal']?>";

	$scope.labores = [];
	$scope.fincas = [];
	$scope.showFinca = 0;
	$scope.expediente = [];

	$scope.Personal = {
		idPersonal : $scope.idPersonal,
		idFinca : 1,
		ingreso : "",
		contrato : "",
		id_contrato : 0,
		salida : "",
		nombre : "",
		codigo : "",
		cedula : "",
		sexo : "Masculino",
		cargo : "OPERADOR AGRICOLA",
		labor : "",
		discapacitado : "No",
		afiliado : "Si",
		civil : "Soltero",
		familiares : "",
		beneficios : "No",
		horas : "8",
		sueldos : [],
	}

	$scope.BkPersonal = {};

	$scope.nocache = function(){
		if($('.date-picker').length > 0){
            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                autoclose: true
            });
        }

        $scope.getLabores();

        $scope.BkPersonal = angular.copy($scope.Personal);

	};

	$scope.saveDatos = function(){
		if($scope.Personal.nombre != ""){
			var data = $scope.Personal;
			client.post("phrapi/marcel/savePersonal" , $scope.printPersonalSave , data);
		}
	}

	$scope.download = function(id_persona, id_doc, ext){
		let url = "http://app.procesos-iq.com/expediente_documentos/marcel/expediente_d="+id_doc+"_p="+id_persona+"."+ext;
		document.getElementById('iframe').src = url;
	}

	$scope.saveSal = function(){
		var data = {
			idPersonal : parseInt($scope.idPersonal),
			sueldo : $("#sueldo").val(),
			fecha : $("#fecha_sueldo").val(),
		}
		if(data.sueldo > 0 && data.fecha != "" && data.fecha != '0000-00-00'){
			client.post("phrapi/guardar/sueldo" , function(r , b){
				b();
				if(r){
					$scope.Personal.sueldos = r;
					$("#sueldo").val('');
					$("#fecha_sueldo").val('');
				}
			} , data);
		}
	}

	$scope.getLabores = function(){
		client.post("phrapi/cargo/tipo" , function(r,b){
			b();
			if(r){
				$scope.labores = r || [];
				$scope.init();
			}
		} , {});
	}

	$scope.init = function(){
		var data = {
			idPersonal : parseInt($scope.idPersonal)
		}
		client.post("phrapi/marcel/personal" , $scope.printPersonal , data);
	}

	$scope.printPersonalSave = function(r , b){
		b();
		alert("Registro Actualiazdo / Registrado" , "Personal" , "success" , function(){
			if(r){
				if(r.hasOwnProperty("data") && r.data.hasOwnProperty("nombre")){
					$scope.Personal = r.data;
					$scope.Personal.sueldos = r.sueldos;
				}
			}
		});
	}

	$scope.printPersonal = function(r , b){
		b();
		if(r){
			$scope.clase = r.clase ||  [];
			$scope.perfiles = r.perfiles ||  [];
			$scope.cargos = r.cargos || [];

			if(r.hasOwnProperty("data") && r.data.hasOwnProperty("nombre")){
				$scope.Personal = r.data;
				$scope.Personal.sueldos = r.sueldos;
				$scope.fincas = r.fincas;
				$scope.showFinca = r.showFinca;
				$scope.expediente = r.expediente;
			}
		}
	}

}]);