app.directive('fileInput', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            element.bind('change', function () {
                $parse(attributes.fileInput)
                .assign(scope,element[0].files)
                scope.$apply()
            });
        }
    };
}]);

app.controller('personal_tthh', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope, $http, $interval, client, $controller , $timeout,$window){

	$scope.data = {
		id_personal : "<?=$_GET['idPersonal'] ?>",
		id_doc : "<?= $_GET['doc'] ?>",
		fecha : "<?= date('Y-m-d') ?>"
	}

	$scope.saveDatos = function(){
		$('form').submit()
		/*client.post('phrapi/expediente/save', function(r, b){ 
			b() 
			window.location = "tthhFPersonal?idPersonal=<?= $_GET['idPersonal'] ?>"
		}, $scope.data)*/
	}

	$scope.init = function(){
		client.post('phrapi/expediente/show', $scope.showExp, $scope.data)
	}

	$scope.eliminarArchivo = function(){
		client.post('phrapi/expediente/delete', $scope.deleteExp, $scope.data)
	}

	$scope.showExp = function(r, b){
		b()
		if(r){
			$scope.data.personal = r.personal || ""
			$scope.data.status = r.status || "NO"
			$scope.data.descripcion = r.descripcion || ""
			$scope.data.observaciones = r.observaciones || ""
			$scope.data.ext = r.ext || ""
		}
	}

	$scope.deleteExp = function(r, b){
		b()
		if(r){
			$scope.data.ext = ""
		}
	}

	$scope.init()

}]);