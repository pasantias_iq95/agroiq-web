app.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            if(parseFloat(value[key]))
                sum = sum + parseFloat(value[key], 10);
        });        
        return sum;
    }
})

app.service('request', ['$http', function($http){
    this.index = function(callback, params){
        let url = 'phrapi/marcel/tthhenfunde/edit/index'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.guardar = function(callback, params){
        let url = 'phrapi/marcel/tthhenfunde/edit/save'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.saldo = function(callback, params){
        let url = 'phrapi/marcel/tthhenfunde/edit/saldo'
        let data = params ||  {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.getColorCinta = function(callback, params){
        let url = 'phrapi/marcel/tthhenfunde/edit/cinta'
        let data = params ||  {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }
}])

app.controller('produccion', ['$scope', 'request', function($scope, $request){
    
    $scope.data = {
        fecha_res : moment().format('YYYY-MM-DD'),
        fecha : moment().format('YYYY-MM-DD'),
        semana : moment().week(),
        saldo_inicial : 0
    }

    const printInit = (r) => {
        $scope.cinta = r.cinta
        $scope.enfundadores = r.enfundadores
        $scope.lotes = r.lotes
        $scope.data.saldo_inicial = parseInt(r.saldo_inicial)
    }
    const printChangeFecha = (r) => {
        $scope.enfundadores = r.enfundadores
        $scope.lotes = r.lotes
        $scope.data.saldo_inicial = parseInt(r.saldo_inicial)
    }
    $scope.init = () => {
        $request.index(printInit, $scope.data)
    }

    $scope.changeSaldo = () => {
        $request.saldo((r) => {
            $scope.data.saldo_inicial = parseInt(r.saldo_inicial)
        }, $scope.data)
    }

    $scope.changeFecha = () => {
        if($scope.data.fecha == ""){
            $scope.data.fecha = angular.copy($scope.data.fecha_res)
        }else{
            $scope.data.fecha_res = angular.copy($scope.data.fecha)
            $scope.data.semana = moment($scope.data.fecha_res).week()
            $request.getColorCinta((r) => {
                $scope.cinta = r.cinta
            }, { year : moment($scope.data.fecha_res).year(), semana : $scope.data.semana })
            $request.index(printChangeFecha, $scope.data)
        }
    }

    $scope.guardar = () => {
        var cancel = false
        if(!$scope.data.enfundador > 0){
            cancel = true
            alert("Colocar un enfundador")
        }
        if(!$scope.data.lote > 0){
            cancel = true
            alert("Colocar el lote")
        }
        if(($scope.data.entregadas == '' || $scope.data.entregadas == 0) && !$scope.data.usadas > 0){
            cancel = true
            alert("Colocar las fundas usadas")
        }
        if(!cancel){
            let data = angular.copy($scope.data)
            $request.guardar((r) => {
                if(r.status == 200){
                    alert("Se guardo con éxito", "", "success", () => {
                        window.location.href = "tthhEnfunde"
                    })
                }else{
                    alert("Ocurrio algun problema")
                }
            }, data)
        }
    }

    $scope.init()

}]);

