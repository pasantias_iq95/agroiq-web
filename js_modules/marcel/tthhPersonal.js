var TableDatatablesEditable = function () {

    var cargos = "";
    function getCargos() {
        ahttp.post("phrapi/cargo/tipo" , function(r , b){
            b()
            if(r){
                cargos = r || [];
            }
        })
    }

    getCargos();

    var handleTable = function () {


        function generateSelect(value){
            var html = [];
            html.push('<select name="labor" id="labor">');
            var selected = "";
            for (var i in cargos) {
                selected = "";
                if(value == cargos[i]){
                    selected = "selected";
                }
                html.push('<option value="'+i+'" '+selected+'>'+cargos[i]+'</option>');
            }
            html.push('</select>');

            return html.join("");
        }

        function restoreRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);

            for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                oTable.fnUpdate(aData[i], nRow, i, false);
            }

            oTable.fnDraw();
        }

        function reloadDate(){
            // console.log("Entro")
            if($('.date-picker').length > 0){
                $('.date-picker').datepicker({
                    rtl: App.isRTL(),
                    autoclose: true
                });
            }
        }

        function editRow(oTable, nRow) {
            var aData = oTable.fnGetData(nRow);
            var jqTds = $('>td', nRow);
            jqTds[0].innerHTML =  aData[0];
            jqTds[1].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[1] + '">';
            jqTds[2].innerHTML = '<input type="text" class="form-control input-small" value="' + aData[2] + '">';
            jqTds[3].innerHTML =  generateSelect(aData[3]);
            jqTds[4].innerHTML = '<input type="text" class="form-control input-small date-picker" data-date-format="yyyy-mm-dd" readonly value="' + aData[4] + '">';
            jqTds[5].innerHTML = '<input type="text" class="form-control input-small date-picker" data-date-format="yyyy-mm-dd" readonly value="' + aData[5] + '">';
            jqTds[6].innerHTML = aData[6];
            jqTds[7].innerHTML = '<a class="edit" href="">Guardar</a>';
            // jqTds[8].innerHTML = '<a class="cancel" href="">Cancelar</a>';
            // console.log("Entro")
            reloadDate();
        }

        function saveRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            var jqSelect = $('select', nRow);
            console.log(jqInputs)
            console.log(jqSelect)
            // oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[0].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 2, false);
            oTable.fnUpdate(jqSelect[0].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 5, false);
            // oTable.fnUpdate(jqInputs[6].value, nRow, 6, false);
            oTable.fnUpdate('<a class="edit" href="">Editar</a>', nRow, 7, false);
            // oTable.fnUpdate('<a class="delete" href="">Borrar</a>', nRow, 8, false);
            oTable.fnDraw();
        }

        function changeStatusRow(oTable, nRow ,status) {
            var html = [];
            var className = (status == 1) ? 'bg-green-jungle bg-font-green-jungle' : 'red';
            var label = (status == 1) ? 'Activo' : 'Inactivo';
            html.push('<button class="btn btn-sm '+className+' status" id="status">');
            html.push(label);
            html.push('</button>');
            oTable.fnUpdate(html.join(""), nRow, 6, false);
            oTable.fnDraw();
        }

        function cancelEditRow(oTable, nRow) {
            var jqInputs = $('input', nRow);
            oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
            oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
            oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
            oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
            oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
            oTable.fnUpdate(jqInputs[5].value, nRow, 5, false);
            oTable.fnUpdate(jqInputs[6].value, nRow, 6, false);
            oTable.fnUpdate('<a class="edit" href="">Editar</a>', nRow, 7, false);
            oTable.fnDraw();
        }

        var table = $('#sample_editable_1');

        var oTable = table.dataTable({

            // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
            // So when dropdowns used the scrollable div should be removed. 
            //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

            "lengthMenu": [
                [5, 15, 20, -1],
                [5, 15, 20, "All"] // change per page values here
            ],

            // Or you can use remote translation file
            //"language": {
            //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
            //},

            // set the initial value
            "pageLength": 5,

            "language": {
                "lengthMenu": " _MENU_ records"
            },
            "columnDefs": [{ // set default column settings
                'orderable': true,
                'targets': [0]
            }, {
                "searchable": true,
                "targets": [0]
            },
            {
                "visible" : false,
                "targets" : [-1]
            }
            ],
            "order": [
                [0, "asc"]
            ] // set first column as a default sort by asc
        });

        console.log(oTable);
        var tableWrapper = $("#sample_editable_1_wrapper");

        var nEditing = null;
        var nNew = false;

        $('#sample_editable_1_new').click(function (e) {
            e.preventDefault();
            document.location.href = "/tthhFPersonal";
            // if (nNew && nEditing) {
            //     alert("Ya existe un formulario de registro");
            //     // if (confirm("Ya existe un formulario de registro , desea perderlo?")) {
            //     // //     saveRow(oTable, nEditing); // save
            //     // //     $(nEditing).find("td:first").html("Untitled");
            //     // //     nEditing = null;
            //     // //     nNew = false;

            //     // // } else {
            //     //     oTable.fnDeleteRow(nEditing); // cancel
            //     //     nEditing = null;
            //     //     nNew = false;
                    
            //     //     return;
            //     // }
            // }else{
            //     var aiNew = oTable.fnAddData(['', '', '', '', '', '' ,'' ,'' ,'']);
            //     var nRow = oTable.fnGetNodes(aiNew[0]);
            //     editRow(oTable, nRow);
            //     nEditing = nRow;
            //     nNew = true;
            // }

        });


        table.on('click', '.status', function (e) {
            e.preventDefault();
            if(confirm("¿Esta seguro de cambiar el registro?")){
                var nRow = $(this).parents('tr')[0];
                var aData = oTable.fnGetData(nRow);
                // saveRow(oTable, nRow);
                if(aData.length > 0){
                    var data = {
                        id_personal : aData[0],
                        status : aData[8]
                    }
                    ahttp.post("phrapi/change/status" , function(r , b){
                        b();
                        if(r){
                            changeStatusRow(oTable, nRow , r.success);
                        }
                    } , data);
                }
            }
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            if (confirm("Esta seguro de Eliminar el registro?") == false) {
                return;
            }

            var nRow = $(this).parents('tr')[0];
            var aData = oTable.fnGetData(nRow);
            oTable.fnDeleteRow(nRow);
        });

        table.on('click', '.cancel', function (e) {
            e.preventDefault();
            if (nNew) {
                oTable.fnDeleteRow(nEditing);
                nEditing = null;
                nNew = false;
            } else {
                restoreRow(oTable, nEditing);
                nEditing = null;
            }
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();
            // nNew = false;
            
            // /* Get the row as a parent of the link that was clicked on */
            // var nRow = $(this).parents('tr')[0];
            // // console.log(nRow);
            // if (nEditing !== null && nEditing != nRow) {
            //     /* Currently editing - but not this row - restore the old before continuing to edit mode */
            //     restoreRow(oTable, nEditing);
            //     editRow(oTable, nRow);
            //     nEditing = nRow;
            // } else if (nEditing == nRow && this.innerHTML == "Guardar") {
            //     /* Editing this row and want to save it */
            //     var aData = oTable.fnGetData(nRow);
            //     var jqInputs = $('input', nRow);
            //     var jqSelect = $('select', nRow);
            //     // console.log(aData)
            //     var data = {
            //         id_personal : aData[0],
            //         nombre : jqInputs[0].value,
            //         cedula : jqInputs[1].value,
            //         cargo : jqSelect[0].value,
            //         fecha_inicio : jqInputs[2].value,
            //         fecha_fina : jqInputs[3].value,
            //     }
            //     // console.log(data)
            //     if(data.nombre != "" && data.fecha_inicio != ""){
            //         saveRow(oTable, nEditing);
            //         nEditing = null;
            //         ahttp.post("phrapi/save/personal" , function(){} , data);
            //     }else{
            //         alert("El campo nombre y el campo fecha contrato son requeridos");
            //     }
            // } else {
            //     /* No edit in progress - let's start one */
            //     editRow(oTable, nRow);
            //     nEditing = nRow;
            //     reloadDate();
            // }

        });
    }

    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        }

    };

}();

app.controller('informe_tthh', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){
    
    $scope.leyendaGeneralTitle = 'Merma';

    $scope.id_company = 0;
    $scope.tags = {
        merma : {
            value : 0,
            label : ""
        },
        enfunde : {
            value : 0,
            label : ""
        },
        campo : {
            value : 0,
            label : ""
        },
        cosecha : {
            value : 0,
            label : ""
        },
        animales : {
            value : 0,
            label : ""
        },
        hongos : {
            value : 0,
            label : ""
        },
        empacadora : {
            value : 0,
            label : ""
        },
        fisiologicos : {
            value : 0,
            label : ""
        }
    };

    $scope.graficas = {
        historico : {},
        historico_avg : 0,
        dia : {},
        dia_title : "",
        danos : {},
        danos_detalle : {},
    }

    $scope.umbrales = {};
    $scope.calidad = {
        params : {
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            fecha_inicial : '2016-08-01',
            fecha_final : '2016-09-30',
            cliente: "",
            marca: "",
            palanca : ""
        },
        step : 0,
        path : ['phrapi/personal/index'],
        templatePath : [],
        nocache : function(){
            this.templatePath.push('/views/marcel/templetes/tthh/step1.html?' +Math.random());
            
            $scope.tags.merma.value = new Number(98).toFixed(2);
            $scope.tags.enfunde.value = new Number(90).toFixed(2);
            $scope.tags.campo.value = new Number(99).toFixed(2);
            $scope.tags.cosecha.value = new Number(97).toFixed(2);

            setTimeout(function(){
                // console.log($scope.tags);
                $(".counter_tags").counterUp({
                    delay: 10,
                    time: 1000
                });
            } , 1000);
        }

    }

    $scope.tabla = {
        detalle : []
    }

    var detalle = {
                nombre : "FRNA",
                cedula : "23423423",
                cargo : "ADMON",
                fecha_ingreso : "2016-09-29",
                fecha_salida : "2017-09-29",
                status : "ACTIVO",
                ficha : "1",
            }

    // $scope.tabla.detalle.push(detalle)

    $scope.palancas = []

    $scope.loadExternal = function(){
        //console.log($scope.calidad.path[$scope.calidad.step]);
        if($scope.calidad.path[$scope.calidad.step] != ""){
            var data = $scope.calidad.params;
            client.post($scope.calidad.path[$scope.calidad.step] , $scope.startDetails , data);
        }
    }

    $scope.changeStatus = function(){
        alert("Hola")
    }

    $scope.startDetails = function(r , b){
        b();
        if(r){
			var options = {};
            $scope.tags.merma.value = new Number(98).toFixed(2);
            $scope.tags.enfunde.value = new Number(90).toFixed(2);
            $scope.tags.campo.value = new Number(99).toFixed(2);
            $scope.tags.cosecha.value = new Number(97).toFixed(2);
            $scope.tabla.detalle = r.data || [];
            setTimeout(function(){
                // console.log($scope.tags);
                $(".counter_tags").counterUp({
                    delay: 10,
                    time: 1000
                });

                TableDatatablesEditable.init();

            } , 1000);
        }
    }
}]);