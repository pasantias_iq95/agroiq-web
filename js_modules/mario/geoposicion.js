app.controller('mapas', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){

    $scope.map = {};
    $scope.type = "PERIODO";
    $scope.id_company = 0;
    $scope.idFinca = 1;
    $scope.auditorias = [];
    $scope.fincas = [];
    $scope.bounds = new google.maps.LatLngBounds();

    $scope.init = function(){
        $scope.map = new GMaps({
            div: '#mapa_agroaudit',
            lat: -1.725187,
            lng: -78.854211,
            zoom: 7
        });

        data = {
            opt : "INDEX",
            type : $scope.type
        }
        
        client.post("phrapi/mario/mapas/index" ,$scope.printSem , data);
    }


    changeFincas = function(){
        var idFinca = angular.element("#fincas").val();
        var Periodo = parseInt(angular.element("#auditorias").val());
        $scope.idFinca = idFinca
        if(!isNaN(Periodo) && Periodo > 0){
            $scope.getMarker(Periodo);
        }
    }

    $scope.changeType = function(type){
        $scope.type = type || "Mes";
        data = {
            opt : "INDEX",
            type : $scope.type
        }
        client.post("phrapi/mario/mapas/index" ,$scope.printSem , data);
    }

    $scope.printSem = function(r , b){
        b();
        if(r){
            $scope.auditorias = r.auditorias;
            $scope.id_company = r.id_company;
            $scope.fincas = r.fincas;
            $("#auditorias").change();
        }
    }

    $scope.addMarker = function(data , label){
        if(data){
            for(var i in data){

                $scope.map.addMarker({
                    lat: data[i].mlat,
                    lng: data[i].mlng,
                    title: data[i].nombre_muestra,
                    infoWindow: {
                        content: '<span>'+data[i].nombre_muestra+'</span>'
                    }
                });

                $scope.bounds.extend(new google.maps.LatLng(data[i].mlat, data[i].mlng));
                $scope.map.fitBounds($scope.bounds);
            }

            // angular.forEach(data ,function(value , key){
            // });
        }
    }

    $scope.printMarkers = function(r , b){
        if(r){
            b();
            for(var i in r){
                $scope.addMarker(r[i] , i);
            }

            // $scope.map.setZoom(7);
        }
    }

    $scope.getMarker = function(value){
        var periodo = parseFloat(value) || 0;
        if(value > 0){
             data = {
                opt : "MARKERS",
                auditorias : periodo,
                type : $scope.type,
                idFinca : $scope.idFinca,
            }
            client.post("phrapi/mario/mapas/markers" ,$scope.printMarkers , data);
        }
    }


    $("#auditorias").on("change" , function(){
        $scope.map.removeMarkers();
        if(this.value > -1){
           $scope.getMarker(this.value);
        }
    })


}]);
