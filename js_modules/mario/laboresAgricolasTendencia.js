app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && value[key] > 0){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.service('request', ['$http', ($http) => {
    let service = {}

    service.last = (callback, params) => {
        load.block()
        $http.post('phrapi/sumifru/calidadTendencia/last', params)
        .then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.index = (callback, params) => {
        load.block('principal')
        $http.post('phrapi/sumifru/calidadTendencia/index', params)
        .then((r) => {
            load.unblock('principal')
            callback(r.data)
        })
    }

    return service
}])

function getOptionsGraficaReact(id, options, umbral){
    let newOptions = {
        series: options.series,
        legend: options.legends,
        umbral : umbral,
        id: id,
        min : 'dataMin',
        actions : false
    }
    return newOptions
}

function initGrafica(id, options, umbral){
    setTimeout(() => {
        let data = getOptionsGraficaReact(id, options, umbral)
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
    }, 250)
}

app.controller('controller', ['$scope','request', '$filter', function($scope, $request, $filter){

    // UMBRAL

        $scope.loadCalidadUmbral = () => {
            let c = localStorage.getItem('banano_calidad_cluster_umbral')
            if(c){
                $scope.umbral_cluster = JSON.parse(c)
            }else{
                $scope.umbral_cluster = {
                    max : 92,
                    min : 90
                }
            }

            let d = localStorage.getItem('banano_calidad_dedos_umbral')
            if(d){
                $scope.umbral_dedos = JSON.parse(d)
            }else{
                $scope.umbral_dedos = {
                    max : 92,
                    min : 90
                }
            }

            let e = localStorage.getItem('banano_calidad_empaque_umbral')
            if(e){
                $scope.umbral_empaque = JSON.parse(e)
            }else{
                $scope.umbral_empaque = {
                    max : 92,
                    min : 90
                }
            }
        }

        $scope.umbral_cluster = {}
        const getUmbralClusterMin = () => {
            return $scope.umbral_cluster.min
        }

        const getUmbralClusterHigh = () => {
            return $scope.umbral_cluster.max
        }
        const getClusterUmbral = (value) => {
            if(value <= getUmbralClusterMin()){
                return 'font-red-thunderbird'
            }
            else if(value >= getUmbralClusterHigh()){
                return 'font-green-haze'
            }else {
                return 'font-yellow-gold'
            }
        }

        $scope.umbral_dedos = {}
        const getUmbralDedosMin = () => {
            return $scope.umbral_dedos.min
        }

        const getUmbralDedosHigh = () => {
            return $scope.umbral_dedos.max
        }
        const getDedosUmbral = (value) => {
            if(value <= getUmbralDedosMin()){
                return 'font-red-thunderbird'
            }
            else if(value >= getUmbralDedosHigh()){
                return 'font-green-haze'
            }else {
                return 'font-yellow-gold'
            }
        }

        $scope.umbral_empaque = {}
        const getUmbralEmpaqueMin = () => {
            return $scope.umbral_empaque.min
        }

        const getUmbralEmpaqueHigh = () => {
            return $scope.umbral_empaque.max
        }
        const getEmpaqueUmbral = (value) => {
            if(value <= getUmbralEmpaqueMin()){
                return 'font-red-thunderbird'
            }
            else if(value >= getUmbralEmpaqueHigh()){
                return 'font-green-haze'
            }else {
                return 'font-yellow-gold'
            }
        }

    //

    // BEGIN CONFIG
        $scope.config = {
            calidad_empaque : true,
            peso_prom_cluster : true
        }
        $scope.nombre_cliente = 'Sumifru'
        $scope.tag_md = $scope.config.calidad_empaque ? 4 : 3
    // END CONFIG

    $scope.filters = {
        anio : moment().year(),
        mode : 'zona',
        period : 'semana',
        var : 'calidad_cluster'
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.filters.anio = r.anio
            $scope.index()
        })
    }

    $scope.index = () => {
        $request.index((r) => {
            $scope.marcas = r.marcas

            let _data_chart = r.grafica
            let serie_total = {
                connectNulls : true,
                data : [],
                name : $scope.nombre_cliente,
                type : 'line'
            }
            let _data = r.tabla.data
            let umbral = Math.round($filter('avgOfValue')(_data, 'prom')*100)/100
            let total = {
                detalle : $scope.nombre_cliente,
                prom : umbral,
                max : Math.round($filter('avgOfValue')(_data, 'max')*100)/100,
                min : Math.round($filter('avgOfValue')(_data, 'min')*100)/100,
            }
            for(let i in r.tabla.semanas){
                let sem = r.tabla.semanas[i]
                total[`sem_${sem}`] = Math.round($filter('avgOfValue')(_data, `sem_${sem}`)*100)/100
                serie_total.data.push(total[`sem_${sem}`])
            }
            _data.push(total)
            _data_chart.series.push(serie_total)
            
            renderGraficaGeneral(_data_chart, umbral)
            renderTablaGeneral(_data, r.tabla.semanas)
        }, $scope.filters)
    }

    const renderGraficaGeneral = (data, umbral) => {
        initGrafica('chart', data, umbral)
    }

    const renderTablaGeneral = (data, semanas) => {
        let umbral = null
        if($scope.filters.var == 'calidad_cluster') umbral = getClusterUmbral
        if($scope.filters.var == 'calidad_dedos') umbral = getDedosUmbral
        if($scope.filters.var == 'calidad_empaque') umbral = getEmpaqueUmbral

        let props = {
            header : [{
                   key : 'detalle',
                   name : 'DETALLE',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                   key : 'prom',
                   name : 'PROM',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : (row) => {
                        return `
                            <div class="${umbral != null ? umbral(row[`prom`]) : ''}">
                                ${row[`prom`]}
                            </div>
                        `
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : (row) => {
                        return `
                            <div class="${umbral != null ? umbral(row[`max`]) : ''}">
                                ${row[`max`]}
                            </div>
                        `
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : (row) => {
                        return `
                            <div class="${umbral != null ? umbral(row[`min`]) : ''}">
                                ${row[`min`]}
                            </div>
                        `
                    }
                }
            ],
            data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ],
            height : $scope.filters.mode == 'zona' ? 250 : 500
        }
        semanas.map((sem) => {
            props.header.push({
                key : `sem_${sem}`,
                name : `${sem}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : (row) => {
                    return `
                        <div class="${umbral != null ? umbral(row[`sem_${sem}`]) : ''}">
                            ${row[`sem_${sem}`]}
                        </div>
                    `
                }
            })
        })
        document.getElementById('tabla-general').innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('tabla-general'))
    }

    $scope.last()
    $scope.loadCalidadUmbral()

}]);