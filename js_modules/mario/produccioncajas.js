app.service('cajas', ['client', '$http', function(client, $http){
    this.registros = function(callback, params){
        let url = 'phrapi/sumifru/cajas/registros'
        let data = params || {}
        client.post(url, callback, data, 'tabla_base')
    }
    
    this.last = function(callback, params){
        let url = 'phrapi/sumifru/cajas/last'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.resumenCajas = function(callback, params){
        let url = 'phrapi/sumifru/cajas/resumen'
        let data = params || {}
        client.post(url, callback, data, 'div_table_2')
    }

    this.filters = function(callback, params){
        let url = 'phrapi/sumifru/cajas/filters'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.cajasSemanal = function(callback, params){
        let url = 'phrapi/sumifru/cajas/cajasSemanal'
        let data = params || {}
        client.post(url, callback, data, 'cajas_semanal')
    }

    this.getMarcas = function(callback, params){
        let url = 'phrapi/sumifru/cajas/marcas'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.cuadrar = function(callback, params){
        let url = 'phrapi/sumifru/cajas/cuadrar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.guardarCuadrar = function(callback, params){
        let url = 'phrapi/sumifru/cajas/guardarCuadrar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.getGuiasDia = function(callback, params){
        let url = 'phrapi/sumifru/cajas/guias'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.procesar = function(callback, params){
        let url = 'phrapi/sumifru/cajas/procesar'
        let data = params || {}
        client.post(url, callback, data)
    }
    
    this.eliminar = function(callback, params){
        let url = 'phrapi/sumifru/cajas/eliminar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.graficasBarras = function(callback, params){
        let url = 'phrapi/sumifru/cajas/graficasBarras'
        let data = params || {}
        client.post(url, callback, data, 'barras')
    }

    this.tablaDiferencias = function(callback, params){
        let url = 'phrapi/sumifru/cajas/diferencias'
        let data = params || {}
        client.post(url, callback, data, 'tablas')
    }

    this.historicoExcedente = function(callback, params){
        let url = 'phrapi/sumifru/cajas/excedente'
        let data = params || {}
        client.post(url, callback, data, 'collapse_3_3')
    }

    this.hasData = function(callback,params){
        let url = 'phrapi/sumifru/cajas/hasData'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        })
    }
}])

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

function getOptionsGraficaReact(id, options, title){
    var newOptions = {
        series: options.series,
        legend: options.legends,
        umbral: null,
        id: id,
        titulo : ''
    }
    return newOptions
}

function initGrafica(id, options, title){
    setTimeout(() => {
        var data = getOptionsGraficaReact(id, options, title)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
    }, 250)
}

function initPastel(id, series){
    var legends = []
    var newSeries = []
    Object.keys(series).map(key => {
        let label = series[key].label
        newSeries.push({
            label : series[key].label,
            value : parseFloat(series[key].value),
            color : (label.includes('>')) ? '#E43A45' : (label.includes('<')) ? '#C49F47' : '#26C281'
        })
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })
    setTimeout(() => {
        data = {
            data : newSeries,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
    }, 250)
}

function number_format(amount, decimals) {
    
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

let datesDisabled = [], datesEnabled = []

app.controller('produccion', ['$scope','cajas', function($scope, cajas){

    $scope.marcasSeleccionadas = [
        '',
        '',
        '',
        '',
        '',
        ''
    ];

    datepickerHighlight = () => {
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $("#datepicker").datepicker('setDate', $scope.table.fecha_inicial)
        $("#datepicker").on('changeDate', function(e){
            $scope.table.fecha_inicial = moment(e.date).format('YYYY-MM-DD')
            $scope.getRegistros()
        })
        $('#datepicker').datepicker('update')
    }

    checkUmbral = (value) => {
        if($scope.umbralExcedente)
        if(parseFloat(value)){
            let umbral = parseFloat($scope.umbralExcedente[$scope.filters.var])
            if(value > umbral) return 'bg-red-thunderbird bg-font-red-thunderbird'
            else return 'bg-green-jungle bg-font-green-jungle'
        }else{
            console.log(value)
        }
        return ''
    }

    initTableExcedente = (id, r) => {
        let val = $scope.filters.var
        var props = {
            header : [{
                   key : 'marca',
                   name : 'MARCA',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                    key : 'sum_'+val,
                    name : 'SUM',
                    locked : true,
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    formatter : 'Number',
                    resizable : true,
                    customCell : function(rowData, isChildren){
                         let valNumber = parseFloat(rowData['sum_'+val])
                         let valueCell = valNumber > 0 ? number_format(valNumber, 2) : ''
                         return `
                             <div class="text-center" style="height: 100%">
                                 ${valueCell}
                             </div>
                         `;
                     }
                 },{
                   key : 'avg_'+val,
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['avg_'+val])
                        let valueCell = valNumber > 0 ? number_format(valNumber, 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max_'+val,
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max_'+val])
                        let valueCell = valNumber > 0 ? number_format(valNumber, 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min_'+val,
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min_'+val])
                        let valueCell = valNumber > 0 ? number_format(valNumber, 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : r.data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        Object.keys(r.semanas).map((key) => {
            let value = r.semanas[key]
            props.header.push({
                key : `sem_${val}_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+val+'_'+value])
                    let valueCell = valNumber > 0 ? number_format(valNumber, 2) : ''
                    return `
                        <div class="text-center ${checkUmbral(valNumber)}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }
    
    $scope.fechaPasada = moment().format('YYYY-MM-DD')
    $scope.anioCambio = true

    $scope.filters = {
        var : 'exce'
    }
    $scope.registros = []
    $scope.table = {
        pagination : 10,
        startFrom : 0,
        actualPage : 1,
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
        search : {},
        year : moment().year(),
        semana : moment().week(),
        unidad : 'lb',
        requerimiento : 41.5,
        var : 'CONV'
    }
    $scope.charts = {
        cajasSemanal : new echartsPlv()
    }

    $scope.saveUmbral = () => {
        var umbral = angular.copy($scope.table.requerimiento)
        if($scope.table.unidad == 'lb'){
            umbral = Math.round($scope.table.requerimiento * 0.4536 * 100) / 100
        }
        cajas.guardarUmbral((r) => {
            
        }, {
            umbral : umbral
        })

    }

    $scope.convertKg = () => {
        if($scope.table.unidad == 'lb'){
            $scope.table.unidad = 'kg'
            $scope.table.requerimiento = Math.round($scope.table.requerimiento * 0.4536 * 100) / 100
        }else{
            $scope.table.unidad = 'lb'
            $scope.table.requerimiento = Math.round($scope.table.requerimiento / 0.4536 * 100) / 100
        }
        
        setTimeout(( ) => $scope.$apply() , 100)
        
        $scope.getRegistrosBase()
        $scope.getResumenCajas()
        $scope.getTablaDiferencias()
        $scope.getHistoricoExcedente()
        cajas.graficasBarras(printGraficasBarras, $scope.table)
    }

    $scope.guardarRequerimiento = () => {
        var data = {
            year : $scope.table.year,
            requerimiento : $scope.table.requerimiento,
            fecha_inicial : $scope.table.fecha_inicial,
            unidad : $scope.table.unidad,
            finca : $scope.table.finca,
            requerimiento : $scope.requerimiento
        };

        $scope.editRequerimiento = false;
        $scope.saveUmbral()

        cajas.historicoExcedente(function(r, b){
            b('collapse_3_3')
            $scope.dataExcedente = r
            $scope.umbralExcedente = r.umbrales
            initTableExcedente('table-historico-excedente', r)
        }, data)

        cajas.tablaDiferencias(function(r, b){
            b('tablas')
            $scope.tablasDiferencias = r.tablas
        }, data)
    }
    
    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }

    $scope.changeRangeDate = function () {
    
        if(!$("#expand_cuadrar").hasClass('collapsed')){
            $("#expand_cuadrar").addClass('collapsed')
            $("#expand_cuadrar").attr('aria-expanded', 'false')

            $("#collapse_3_2").attr('aria-expanded', 'false')
            $("#collapse_3_2").removeClass('in')
            $("#collapse_3_2").css('height', '0px')
        }

        $scope.anioCambio = false
        if(moment($scope.fechaPasada).year() != moment($scope.table.fecha_inicial).year()) $scope.anioCambio = true;
        $scope.fechaPasada = $scope.table.fecha_inicial

        $scope.table.fecha_final = $scope.table.fecha_inicial;
        $scope.getRegistros();   
    }

    $scope.next = function(dataSource){
        if($scope.searchTable.actual_page < $scope.searchTable.numPages) {
            $scope.searchTable.actual_page++;
            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
        }
    }

    $scope.prev = function(dataSource){
        if($scope.searchTable.actual_page > 1){
            $scope.searchTable.actual_page--;
            $scope.searchTable.startFrom = ($scope.searchTable.actual_page - 1) * (parseInt($scope.searchTable.limit))
        }
    }

    $scope.setOrderTable = function(field){
        var reverse = false
        if(field == $scope.searchTable.orderBy){
            reverse = !$scope.searchTable.reverse
        }else{
            $scope.searchTable.orderBy = field
        }
        $scope.searchTable.reverse = reverse
    }

    $scope.getFilters = function(){
        return new Promise((resolve, reject) => {
            let data = $scope.table
            cajas.filters(function(r, b){
                b()
                if(r){
                    $scope.fincas = r.fincas
                    if(Object.keys(r.fincas).indexOf($scope.table.finca) == -1){
                        $scope.table.finca = Object.keys(r.fincas)[0]
                    }
                    $scope.anios = r.years
                    $scope.semanas = r.semanas
                    resolve()
                }
            }, data)
        })
    }

    $scope.changeExcedente = () => {
        initTableExcedente('table-historico-excedente', $scope.dataExcedente)
    }

    $scope.getRegistrosBase = function(){
        let data = $scope.table
        cajas.registros(function(r, b){
            b('tabla_base')
            if(r){
                $scope.marcas = r.marcas
                $scope.registros = r.data
                setTimeout(function(){
                    $scope.searchTable.changePagination()
                }, 500)
            }
        }, data)
    }

    $scope.eliminar = () => {
        var elements = $(".delete")
        if(elements.length > 0){
            if(confirm(`Seguro deseas eliminar ${elements.length} elementos?`)){
                var ids = []
                var index = []
                for(var x = 0; x < elements.length; x++){
                    ids.push({
                        id : $(elements[x]).attr('data-id'),
                        marca : $(elements[x]).attr('data-marca')
                    })
                }
                cajas.eliminar((r, b) => {
                    b()
                    if(r){
                        $scope.getRegistros()
                        alert(`Se borraron ${elements.length} registros con éxito`, "", "success")
                    }
                }, { ids : ids })
            }
        }else{
            alert("No has seleccionado ninguna fila")
        }
    }

    $scope.editar = (row, index) => {
        if(!$scope.editando){
            $scope.editando = true
            row.edit = true
            row.index = index
            $scope.originalFilaEditando = angular.copy(row)
            $scope.filaEditando = row
        }
    }

    $scope.editarGuia = (guia) => {
        setTimeout(() => {
            $scope.modalCuadrar()
            $scope.guia = {
                guia : guia.guia,
                fecha : guia.fecha,
                sello_seguridad : guia.sello_seguridad,
                magap : guia.codigo_magap,
                productor : guia.codigo_productor,
                marca : {}
            }
            $scope.marcasSeleccionadas = ['', '', '', '', '', '']
            guia.detalle.map((value, index) => {
                $scope.marcasSeleccionadas[index] = value.marca
                $scope.guia.marca[value.marca] = Math.round(value.valor)
            })
            $scope.$apply()
        }, 100)
    }

    $scope.cancelar = (row, index) => {
        if($scope.editando){
            setTimeout(() => {
                row.peso = $scope.originalFilaEditando.peso
                row.marca = $scope.originalFilaEditando.marca
                row.edit = false
                $scope.editando = false
                $scope.$apply()
            }, 100)
        }
    }

    $scope.guardar = (row, index) => {
        if($scope.editando){
            row.unidad = $scope.table.unidad
            cajas.guardarCaja((r, b) => {
                b()
                if(r.status == 200){
                    if(r.registro){
                        row.id = r.registro.id
                        row.tipo = r.registro.tipo
                    }
                    row.edit = false
                    $scope.editando = false
                    $scope.$apply()
                    alert("Se modifico correctamente", "Modificación caja #"+row.id, "success")
                }else{
                    alert(r.message)
                }
            }, row)
        }
    }

    $scope.modalCuadrar = function(){
        $scope.guia = {
            marca : {}
        }
        $("#cuadrar").modal('toggle')
    }

    $scope.guardarCuadrar = function(){
        var data = angular.copy($scope.guia)

        cajas.guardarCuadrar(function(r, b){
            b()
            let data = $scope.table
            cajas.cuadrar(function(r, b){
                b()
                if(r){
                    $scope.guia.procesable = true
                    $scope.guia = {}

                    setTimeout(() => {
                        alert("Guardado con éxito", "", "success")
                        $scope.$apply(() => {
                            console.log("apply")
                            $scope.cuadrarCajas = r.data
                            $scope.guias = r.guias
                        })
                    }, 500)
                }
            }, data)
        }, data)

    }

    $scope.giasDia = (fecha, guia) => {
        $scope.guia.procesable = false
        if(fecha && guia){
            cajas.getGuiasDia((r, b) => {
                b()
                if(r){
                    $scope.guia.procesable = true
                    $scope.guia.marca = {}
                    r.marcas.forEach(value => {
                        $scope.guia.marca[value.marca] = parseInt(value.valor)
                    })
                }
            }, { fecha: fecha, guia: guia })
        }
    }

    $scope.getCuadrar = function(){
        let showing = $("#collapse_3_2").attr('aria-expanded') == 'false'
        if(showing){
            let data = $scope.table
            cajas.cuadrar(function(r, b){
                b()
                if(r){
                    $scope.cuadrarCajas = r.data
                    $scope.guias = r.guias
                }
            }, data)
        }
    }

    $scope.getResumenCajas = () => {
        let data = $scope.table
        cajas.resumenCajas(function(r, b){
            b('div_table_2')
            if(r){
                $scope.resumen = r.data 
                $scope.tags = r.tags
            }
        }, data)
    }

    $scope.getTablaDiferencias = () => {
        let data = $scope.table
        cajas.tablaDiferencias(function(r, b){
            b('tablas')
            $scope.tablasDiferencias = r.tablas
        }, data)
    }

    $scope.getHistoricoExcedente  = () => {
        let data = $scope.table
        cajas.historicoExcedente(function(r, b){
            b('collapse_3_3')
            $scope.dataExcedente = r
            $scope.umbralExcedente = r.umbrales
            initTableExcedente('table-historico-excedente', r)
        }, data)
    }

    var printGraficasBarras = (r, b) => {
        b('barras')
        if(r){
            $scope.marcasBarras = r.marcas
            $scope.graficasBarrar = r.graficas
            Object.keys(r.graficas).map((key) => {
                let min = r.graficas[key].series.Cantidad.umbral.min, 
                    max = r.graficas[key].series.Cantidad.umbral.max
                
                r.graficas[key].series.Cantidad.itemStyle.normal.color = function(e){
                    // range umbral 
                    if(min && max){
                        var min_peso = 0, max_peso = 0
                        if(parseFloat(e.name)){
                            min_peso = max_peso = parseFloat(e.name)
                        }else{
                            var min_max = e.name.split('-')
                            min_peso = parseFloat(min_max[0])
                            max_peso = parseFloat(max_max[0])
                        }

                        if(min_peso >= min && max_peso <= max){
                            return '#26C281'
                        }else if(max_peso < min){
                            return '#C49F47'
                        }else{
                            return '#E43A45'
                        }
                    }

                    return '#E43A45'
                }

                initGrafica(`barras_${key}`, r.graficas[key], key)
                initPastel(`pastel_${key}`, r.pasteles[key])
            })
        }
    }

    $scope.getRegistros = function(){
        $scope.getFilters().then(() => {
            $scope.getRegistrosBase()
            let data = $scope.table
            $scope.getResumenCajas()

            if($scope.anioCambio){
                $scope.loadSemanal()
                $scope.getHistoricoExcedente()
            }

            $scope.getTablaDiferencias()
            cajas.graficasBarras(printGraficasBarras, data)
        })
    }

    $scope.loadSemanal = () => {
        cajas.cajasSemanal(function(r, b){
            b('cajas_semanal')
            if(r){
                $scope.charts.cajasSemanal.init('cajas_semanal', r.chart)
            }
        }, $scope.table)
    }

    $scope.init = function(){
        cajas.last(function(r, b){
            b()
            if(r.last.fecha){
                $scope.table.year = moment(r.last.fecha).year()
                $scope.table.fecha_inicial = r.last.fecha
                $scope.table.fecha_final = r.last.fecha
                datesEnabled = r.days
                datepickerHighlight()
                $scope.getRegistros()
            }
        })
    }

    $scope.searchTable = {
        orderBy : "hora",
        reverse : true,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        numPages : 0,
        changePagination : function(){
            if($scope.searchTable.limit == 0)
                $scope.searchTable.limit = $scope.registros.length

            setTimeout(function(){
                $scope.$apply(function(){
                    $scope.searchTable.numPages = parseInt($scope.registros.length / $scope.searchTable.limit)  + ($scope.registros.length % $scope.searchTable.limit == 0 ? 0 : 1)
                    $scope.searchTable.actual_page = 1
                    $scope.searchTable.startFrom = 0
                })
            }, 250)
        }
    }

    /* CONFIGURACION RANGOS */
    $scope.showRango = function(){
        $scope.config.rangos.getMarcas(function(r, b){
            b()
            if(r){
                $scope.config.rangos.marcas = r.data
            }
            $("#rangos").modal('toggle')
        })
    }

    $scope.config = {
        rangos : {
            marcas : [],
            getMarcas : function(callback){
                cajas.getMarcas(callback,  $scope.params)
            },
            newConfig : {
                marca : 0,
                min : 0,
                max : 0
            }
        }
    }

    $scope.procesarDia = () => {
        cajas.procesar((r, b) => {
            b()
            alert("Procesado Completo", "", "success")
            $scope.getRegistros()
        }, { fecha : $scope.table.fecha_inicial } )
    }

    $scope.guia = {}
    $scope.isValidGuia = () => {
        if($scope.table.fecha_inicial)
            return true;
        return false;
    }

    $scope.exportExcel = function(id_table){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML

                if(id_table == 'table_1'){
                    var cut = contentTable.search('<tr role="row" class="filter">')
                    var cut2 = contentTable.search('</thead>')
                    var part1 = contentTable.substring(0, cut)
                    var part2 = contentTable.substring(cut2, contentTable.length)
                    contentTable = part1 + part2
                }
                
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, "Información de Transtito")
    }

    $scope.exportPrint = function(id_table){
        
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;

        if(id_table == 'table_1'){
            var cut = contentTable.search('<tr role="row" class="filter">')
            var cut2 = contentTable.search('</thead>')
            var part1 = contentTable.substring(0, cut)
            var part2 = contentTable.substring(cut2, contentTable.length)
            contentTable = part1 +part2
        }

        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
        
    }

    $scope.hideActions = () => {
        $("#actions-listado").addClass("hide")
    }
    $scope.showActions = () => {
        $("#actions-listado").removeClass("hide")
    }

    $scope.init()

}]);