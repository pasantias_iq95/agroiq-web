app.filter('order', function () {
    return function (items) {
        let filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item)
        })
        filtered.sort(function (a, b) {
            if (parseFloat(a) && parseFloat(b)) {
                return (parseFloat(a) > parseFloat(b) ? 1 : -1);
            }
            return (a > b ? 1 : -1);
        });
        return filtered;
    }
})

app.filter('orderObjectBy', function () {
    return function (items, field, reverse) {
        let filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if (parseFloat(a[field]) && parseFloat(b[field])) {
                return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
            }
            return (a[field] > b[field] ? 1 : -1);
        });
        if (reverse) filtered.reverse();
        return filtered;
    };
});

app.filter('avgOfArray', function () {
    return function (data) {        
        let sum = 0;
        let count = 0;
        angular.forEach(data, function(value){
            if(value != "" && value != undefined && parseFloat(value)){
                sum = sum + parseFloat(value, 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('avgOfObject', function () {
    return function (data, field) {        
        let sum = 0;
        let count = 0;
        angular.forEach(data, function(value){
            if(value[field] != "" && value[field] != undefined && parseFloat(value[field])){
                sum = sum + parseFloat(value[field], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('avgOfArray', function () {
    return function (data) {        
        let sum = 0;
        let count = 0;
        angular.forEach(data, function(value){
            if(value != "" && value != undefined && parseFloat(value)){
                sum = sum + parseFloat(value, 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.controller('bonificacion', ['$scope', '$http', function ($scope, $http) {

    $scope.bonificacion = {
        params: {
            variable: 'Daños',
            labor : 'LOTERO AEREO',
            semana : `${moment().week()}`,
            idFinca : 'San José 1'
        },
        historica : {
            orderByField : 'semana',
            reverseSort : false
        }
    }
    $scope.umbrals = {
        max : 0,
        min : 0,
        umbral : 0
    }
    $scope.bonos = []

    $scope.checkUmbralHistorica = (value) => {
        if(parseFloat(value) >= 0){
            let val = parseFloat(value)
            if(val == $scope.umbral){
                return 'bg-yellow-gold bg-font-yellow-gold'
            }else if(val < $scope.umbral){
                return 'bg-green-haze bg-font-green-haze'
            }else {
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            }
        }
        return ''
    }

    $scope.tagsFlags = function(umbral, value){        
        let className = "fa fa-arrow-up font-red-thunderbird";
        if(!isNaN(parseFloat(value))){
            if(value <= (umbral || 0)){
                className = "fa fa-arrow-down font-green-jungle"
            }
        }else{
            className = "";
        }
        return className;
    }

    $scope.tagsFlagsSem = function(value){
        let _value = parseFloat(value)

        if(!_value || (!$scope.rangos.max && !$scope.rangos.min)) return ''
        else if(_value > $scope.rangos.max) return 'bg-red-thunderbird bg-font-red-thunderbird'
        else if(_value > $scope.rangos.min) return 'bg-yellow-gold bg-font-yellow-gold'
        else return 'bg-green-jungle bg-font-green-jungle'
    }

    $scope.getUsd = function(value){
        let usd = 0
        let _value = parseFloat(value) || 0
        for(let key in $scope.bonos){
            let bono = $scope.bonos[key]
            if(bono.max >= _value && _value >= bono.min){
                usd = bono.bono
                break
            }
        }
        return usd
    }

    $scope.getFilters = () => {
        $http.post('phrapi/marun/bonif/filters', $scope.bonificacion.params).then(r => {
            $scope.fincas = r.data.fincas;
            $scope.semanas_historico = r.data.semanas

            r.data.umbrals.max = parseFloat(r.data.umbrals.max) || 0
            r.data.umbrals.min = parseFloat(r.data.umbrals.min) || 0

            r.data.bonos.max = parseFloat(r.data.bonos.max) || 0
            r.data.bonos.min = parseFloat(r.data.bonos.min) || 0
            r.data.bonos.bono = parseFloat(r.data.bonos.bono) || 0

            $scope.rangos = r.data.umbrals
            $scope.bonos = r.data.bonos
            $scope.umbral = parseFloat(r.data.umbrals.umbral) || 10
        })
    }

    $scope.getTablaDiarioLotes = () => {
        load.block('table_bonificacion')
        $http.post('phrapi/marun/bonif/diario', $scope.bonificacion.params).then(r => {
            setTimeout(() => {
                load.unblock('table_bonificacion')
                $scope.lotes = r.data.data
                $scope.dias = r.data.dias
                $scope.totales = r.data.totales
                $scope.$apply();
            }, 250)
        })
    }

    $scope.getTablaHistoricaSemanal = (orderBy) => {
        load.block('table_historico')
        load.block('historico')

        let data = angular.copy($scope.bonificacion.params)
        data.order = orderBy
        $http.post('phrapi/marun/bonif/semanal', data).then(r => {
            load.unblock('table_historico')
            load.unblock('historico')

            if (r.data) {
                $scope.historico_tabla_semana = r.data.data_semana
                $scope.historico_tabla = r.data.data
                Object.keys(r.data.totales).map((value, index) => {
                    r.data.totales[value].avg = r.data.totales[value].sum / r.data.totales[value].avg
                })
                $scope.totales_historico = r.data.totales
                //$scope.lotes = r.data.lotes
                $scope.grafica_historica = new echartsPlv()
                $scope.grafica_historica.init('historico', r.data.chart)
            }
        })
    }

    $scope.openDetalle = semana => {
        if(!semana.defectos_loaded && !semana.loading){
            semana.loading = true
            semana.defectos = [{defecto : 'CARGANDO...'}]
            load.block('sub_'+semana.semana)

            var data = {
                idFinca : $scope.bonificacion.params.idFinca,
                labor : $scope.bonificacion.params.labor,
                semana : `${semana.semana}`,
                variable : $scope.bonificacion.params.variable
            }
            $http.post('phrapi/marun/bonif/detalle', data).then( r => {
                load.unblock('sub_'+semana.semana)

                semana.defectos = r.data.data
                semana.defectos_loaded = true
                semana.loading = false
            })
        }else if(!semana.defectos_loaded){
            toastr.warning('Por favor esperar ya casi terminamos')
            return;
        }
        semana.expanded = !semana.expanded
    }

    $scope.reloadTable = () => {
        $scope.getFilters()
        $scope.getTablaDiarioLotes()
    }

    $scope.init = () => {
        $scope.reloadTable()
        $scope.getTablaHistoricaSemanal('semana')
    }

    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title)
    }

    $scope.fnExcelReport = function(id_table, title){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, 'Sample.xls')
    }

    $scope.exportPrint = function(id_table){
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        // remove filters
        var cut = contentTable.search('<tr role="row" class="filter">')
        var cut2 = contentTable.search('</thead>')
        var part1 = contentTable.substring(0, cut)
        var part2 = contentTable.substring(cut2, contentTable.length)
        // add image
        contentTable = image + part1 + part2

        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
    }

    $scope.init()
}]);