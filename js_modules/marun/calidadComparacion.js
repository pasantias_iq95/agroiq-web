app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('sumOfValueDouble', function () {
    return function (data, key, key2) {        
        if (angular.isUndefined(data) || angular.isUndefined(key) || angular.isUndefined(key2))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key][key2] != "" && value[key][key2] != undefined && parseFloat(value[key][key2])){
                sum = sum + parseFloat(value[key][key2], 10);
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && value[key] > 0){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.service('request', ['$http', ($http) => {
    let service = {}

    service.index = (callback, params) => {
        load.block('principal')
        $http.post('phrapi/sumifru/calidadComparacion/principal', params)
        .then((r) => {
            load.unblock('principal')
            callback(r.data)
        })
    }

    service.variables = (callback, params) => {
        load.block()
        $http.post('phrapi/sumifru/calidadComparacion/variables', params)
        .then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.last = (callback, params) => {
        load.block()
        $http.post('phrapi/sumifru/calidadComparacion/last', params)
        .then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.graficaZona = (callback, params) => {
        load.block('graficas-zona')
        $http.post('phrapi/sumifru/calidadComparacion/graficaZona', params)
        .then((r) => {
            load.unblock('graficas-zona')
            callback(r.data)
        })
    }

    return service
}])

app.controller('controller', ['$scope','request', function($scope, $request){

    // UMBRAl

        $scope.loadCalidadUmbral = () => {
            let c = localStorage.getItem('banano_calidad_cluster_umbral')
            if(c){
                $scope.umbral_cluster = JSON.parse(c)
            }else{
                $scope.umbral_cluster = {
                    max : 92,
                    min : 90
                }
            }

            let d = localStorage.getItem('banano_calidad_dedos_umbral')
            if(d){
                $scope.umbral_dedos = JSON.parse(d)
            }else{
                $scope.umbral_dedos = {
                    max : 92,
                    min : 90
                }
            }

            let e = localStorage.getItem('banano_calidad_empaque_umbral')
            if(e){
                $scope.umbral_empaque = JSON.parse(e)
            }else{
                $scope.umbral_empaque = {
                    max : 92,
                    min : 90
                }
            }
        }

        $scope.umbral_cluster = {}
        const getUmbralClusterMin = () => {
            return $scope.umbral_cluster.min
        }

        const getUmbralClusterHigh = () => {
            return $scope.umbral_cluster.max
        }
        $scope.getClusterUmbral = (value) => {
            if(value <= getUmbralClusterMin()){
                return 'font-red-thunderbird'
            }
            else if(value >= getUmbralClusterHigh()){
                return 'font-green-haze'
            }else {
                return 'font-yellow-gold'
            }
        }

        $scope.umbral_dedos = {}
        const getUmbralDedosMin = () => {
            return $scope.umbral_dedos.min
        }

        const getUmbralDedosHigh = () => {
            return $scope.umbral_dedos.max
        }
        $scope.getDedosUmbral = (value) => {
            if(value <= getUmbralDedosMin()){
                return 'font-red-thunderbird'
            }
            else if(value >= getUmbralDedosHigh()){
                return 'font-green-haze'
            }else {
                return 'font-yellow-gold'
            }
        }

        $scope.umbral_empaque = {}
        const getUmbralEmpaqueMin = () => {
            return $scope.umbral_empaque.min
        }

        const getUmbralEmpaqueHigh = () => {
            return $scope.umbral_empaque.max
        }
        $scope.getEmpaqueUmbral = (value) => {
            if(parseFloat(value) <= getUmbralEmpaqueMin()){
                return 'font-red-thunderbird'
            }
            else if(parseFloat(value) >= getUmbralEmpaqueHigh()){
                return 'font-green-haze'
            }else {
                return 'font-yellow-gold'
            }
        }

    //

    // BEGIN CONFIG
        $scope.config = {
            calidad_empaque : false,
            peso_prom_cluster : false
        }

        $scope.tag_md = $scope.config.calidad_empaque ? 4 : 3
    // END CONFIG

    $scope.orderBy = {
        general : { key : 'zona', direction : false },
        defectos : { key : 'zona', direction : false },
        empaque : { key : 'zona', direction : false },
        cluster : { key : 'zona', direction : false }
    }
    $scope.toggleOrder = (table, column) => {
        if($scope.orderBy[table].key == column){
            $scope.orderBy[table].direction = !$scope.orderBy[table].direction
        }else{
           $scope.orderBy[table] = {
               key : column,
               direcion : false
           } 
        }
    }

    $scope.datesEnabled = []
    $scope.totales_defectos = {}
    $scope.totales_empaque = {}
    $scope.totales_cluster = {}
    $scope.mode = 'general'
    $scope.mode_graficas = 'defectos'
    $scope.moda_graficas_desc = '(# Daños)'
    $scope.modeCategoriaGraficaZona = ''
    $scope.filters = {
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
        id_finca : 0,
        marca : ''
    }

    $scope.changeRangeDate = function(data){
		if(data){
			$scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
			$scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
            $scope.variables()
		}
    }

    $scope.init = () => {
        $request.index((r) => {
            $scope.data = r
        }, $scope.filters)
        $request.graficaZona((r) => {
            $scope.dataGraficaZona = { cluster : r.cluster, defectos : r.defectos, empaque : r.empaque }
            $scope.dataGraficaFinca = { cluster : r.cluster_finca, defectos : r.defectos_finca, empaque : r.empaque_finca }
            $scope.renderGraficaZona($scope.modeCategoriaGraficaZona)

            $scope.modeZonaGraficaFinca = $scope.dataGraficaZona.defectos.data[$scope.modeCategoriaGraficaZona].legends[0]
            $scope.renderGraficaFinca($scope.modeCategoriaGraficaZona)
        }, $scope.filters)
    }

    // GRAFICAS
    $scope.renderGraficaZona = (categoria) => {
        let props = {}
        if($scope.mode_graficas == 'defectos'){
            if(!$scope.dataGraficaZona[$scope.mode_graficas].data[categoria]){
                categoria = Object.keys($scope.dataGraficaZona[$scope.mode_graficas].data)[0]
            }
            
            $scope.modeCategoriaGraficaZona = categoria
            props = {
                id : 'grafica-zona-chart',
                series : $scope.dataGraficaZona[$scope.mode_graficas].data[categoria].series,
                legend : $scope.dataGraficaZona[$scope.mode_graficas].data[categoria].legends
            }
        }else{
            props = {
                id : 'grafica-zona-chart',
                series : $scope.dataGraficaZona[$scope.mode_graficas].data.series,
                legend : $scope.dataGraficaZona[$scope.mode_graficas].data.legends
            }
        }
        
        $("#grafica-zona-chart").empty()
        ReactDOM.render(
            React.createElement(BarrasStacked, props),
            document.getElementById('grafica-zona-chart')
        )
    }

    $scope.renderGraficaFinca = (categoria) => {
        setTimeout(() => {
            let props = {}
            const zona = $scope.modeZonaGraficaFinca
            if($scope.mode_graficas == 'defectos'){
                if(!$scope.dataGraficaFinca[$scope.mode_graficas].data[zona][categoria]){
                    categoria = Object.keys($scope.dataGraficaFinca[$scope.mode_graficas].data[zona])[0]
                }
                
                //$scope.modeCategoriaGraficaZona = categoria
                props = {
                    id : 'grafica-finca-chart',
                    orientation : 'horizontal',
                    series : $scope.dataGraficaFinca[$scope.mode_graficas].data[zona][categoria].series,
                    legend : $scope.dataGraficaFinca[$scope.mode_graficas].data[zona][categoria].legends
                }
            }else{
                props = {
                    id : 'grafica-finca-chart',
                    orientation : 'horizontal',
                    series : $scope.dataGraficaFinca[$scope.mode_graficas].data[zona].series,
                    legend : $scope.dataGraficaFinca[$scope.mode_graficas].data[zona].legends
                }
            }
            
            $("#grafica-finca-chart").empty()
            ReactDOM.render(
                React.createElement(BarrasStacked, props),
                document.getElementById('grafica-finca-chart')
            )
        }, 100)
    }
    // END GRAFICAS
    
    $scope.variables = () => {
        $request.variables((r) => {
            $scope.marcas = r.marcas
            // seleccionar marca
            if(r.marcas){
                if($scope.filters.marca){
                    if(!r.marcas.includes($scope.filters.marca)){
                        $scope.filters.marca = r.marcas[0]
                    }
                }
            }else{
                // todas
                $scope.filters.marca = ''
            }
            $scope.init()
        }, $scope.filters)
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.datesEnabled = r.days
            if(r.fecha){
                $scope.filters.fecha_inicial = r.fecha
                $scope.filters.fecha_final = r.fecha
                setTimeout(() => {
                    $("#date-picker").html(`${r.fecha} - ${r.fecha}`)
                }, 100)
            }
            $scope.filters.id_finca = r.id_finca
            $scope.variables()
        })
    }

    $scope.getUmbralDefectos = (val) => {
        if(parseFloat(val)){
            return parseFloat(val) > 10 ? 'font-red-thunderbird' : ''
        }
        return ''
    }

    $scope.getUmbralEmpaque = (val) => {
        if(parseFloat(val)){
            return parseFloat(val) > 10 ? 'font-red-thunderbird' : ''
        }
        return ''
    }

    $scope.last()
    $scope.loadCalidadUmbral()
	
}]);