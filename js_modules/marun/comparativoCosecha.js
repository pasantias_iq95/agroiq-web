app.service('request', ['$http', function($http){
    this.loadData = (callback, params) => {        
        $http.post('phrapi/marun/comparativoCosecha/data', params || {}).then(r => {
            if(callback) callback(r.data)
        })
    }
}])

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.controller('controller', ['$scope', 'request', function($scope, $request){

    $scope.filters = {
        fecha : moment().format('YYYY-MM-DD')
    }
    $scope.data = { formularios : [], balanza : [] }

    $scope.load = () => {
        $request.loadData((r) => {
            $scope.data = r
        }, $scope.filters)
    }

    $scope.load()
    
}]);

