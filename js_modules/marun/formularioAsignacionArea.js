app.service('request', ['$http', function($http){
    this.loadData = (callback, params) => {
        load.block()
        $http.post('phrapi/marun/formularioCosecha/data', params || {}).then(r => {
            load.unblock()
            if(callback) callback(r.data)
        })
    }

    this.save = (callback, params) => {
        $http.post('phrapi/marun/formularioCosecha/save', params || {}).then(r => {
            if(callback) callback(r.data)
        })
    }
}])

app.controller('controller', ['$scope', 'request', function($scope, $request){

    const save = (id, campo, valor) => {
        if(id > 0){
            $request.save((r) => {
                if(r.status == 200){
                    alert("Se guardo correctamente", "", "success")
                    $scope.init()
                }else{
                    alert("Hubo un error favor de intentar mas tarde")
                }
            }, {
                id : id, campo : campo, valor : valor
            })
        }
    }

    const renderTableData = (data) => {
        var props = {
            header : [{
                   key : 'id',
                   name : 'ID',
                   titleClass : 'text-center',                   
                   resizable : true,
                   filterable : true,
                   sortable : true,
                   width: 50
                },{
                    key : 'fecha',
                    name : 'FECHA',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    editable : true,
                    width : 100,
                    events: {
                        onKeyDown: function(ev, column) {
                            if (ev.key === 'Enter') {
                                let index = parseInt(column.rowIdx)
                                let key = column.column.key
                                let row = $scope.table1.rowGetter(index)
                                save(row.id, key, row[key])
                            }
                        },
                    }
                },{
                   key : 'hora',
                   name : 'HORA',
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'center',
                   filterable : true,
                   resizable : true,
                   editable : true,
                   events: {
                        onKeyDown: function(ev, column) {
                            if (ev.key === 'Enter') {
                                let index = parseInt(column.rowIdx)
                                let key = column.column.key
                                let row = $scope.table1.rowGetter(index)
                                save(row.id, key, row[key])
                            }
                        },
                  }
                },{
                    key : 'responsable',
                    name : 'RESPONSABLE',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    editable : true,
                    width : 150,
                    events: {
                        onKeyDown: function(ev, column) {
                            if (ev.key === 'Enter') {
                                let index = parseInt(column.rowIdx)
                                let key = column.column.key
                                let row = $scope.table1.rowGetter(index)
                                save(row.id, key, row[key].toUpperCase())
                            }
                        },
                    }
                },{
                    key : 'sector',
                    name : 'SECTOR',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    editable : true,
                    events: {
                        onKeyDown: function(ev, column) {
                            if (ev.key === 'Enter') {
                                let index = parseInt(column.rowIdx)
                                let key = column.column.key
                                let row = $scope.table1.rowGetter(index)
                                save(row.id, key, row[key].toString().toUpperCase())
                            }
                        },
                    }
                },{
                    key : 'lote',
                    name : 'LOTE',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    editable : true,
                    events: {
                        onKeyDown: function(ev, column) {
                            if (ev.key === 'Enter') {
                                let index = parseInt(column.rowIdx)
                                let key = column.column.key
                                let row = $scope.table1.rowGetter(index)
                                save(row.id, key, row[key].toString().toUpperCase())
                            }
                        },
                    }
                },{
                    key : 'codigo',
                    name : 'CODIGO',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    filterable : true,
                    resizable : true,
                    editable : true,
                    events: {
                        onKeyDown: function(ev, column) {
                            if (ev.key === 'Enter') {
                                let index = parseInt(column.rowIdx)
                                let key = column.column.key
                                let row = $scope.table1.rowGetter(index)
                                save(row.id, key, row[key].toString().toUpperCase())
                            }
                        },
                    }
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        $("#table-data").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-data'))
    }

    $scope.init = () => {
        $request.loadData((r) => {
            renderTableData(r.data)
        })
    }
    $scope.init()
}]);

