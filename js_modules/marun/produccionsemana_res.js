function dlCanvas(nombre , mode ) {
    var self = this;
    // $("#inicio")[0]
    html2canvas($("#contenedor"), {
      onrendered: function(canvas) {
        var dt = canvas.toDataURL('image/png');
        /* Change MIME type to trick the browser to downlaod the file instead of displaying it */
        // dt = dt.replace(/^data:image\/[^;]*/, 'data:application/octet-stream');

        /* In addition to <a>'s "download" attribute, you can define HTTP-style headers */
        // dt = dt.replace(/^data:application\/octet-stream/, 'data:application/octet-stream;headers=Content-Disposition%3A%20attachment%3B%20filename=Canvas.png');
        if(mode == "pdf"){
            var doc = new jsPDF('p', 'mm');
            doc.addImage(dt, 'PNG',5, 5, 195, 290);
            doc.save(nombre+'.pdf');
        }else{
            var a  = document.createElement('a');
            a.href = dt;
            a.download = nombre + '.png';

            a.click()
        }
      }
    });
};

app.filter('orderObjectBy', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field]))
                return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
            else
                return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };
});

app.filter('orderByCajas', function() {
    return function(items, field) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if(a[field] == 'MI COMISARIATO') return 1;
            if(b[field] == 'MI COMISARIATO') return -1;
            return (a[field] > b[field] ? 1 : -1);
        });
        
        return filtered;
    };
});

app.service('request', [ '$http', ($http) => {

    var service = {}

    service.tags = (callback, params) => {
        $http.post('phrapi/marun/produccionsemana/tags', params || {}).then(r => {
            callback(r.data)
        })
    }

    service.graficaPromEdad = (callback, params) => {
        $http.post('phrapi/marun/produccionsemana/graficaEdadPromedio', params || {}).then(r => {
            callback(r.data)
        })
    }

    service.resumenAcumulado = (callback, params) => {
        $http.post('phrapi/marun/produccionsemana/resumenAcumulados', params || {}).then(r => {
            callback(r.data)
        })
    }

    service.reporteProduccion = (callback, params) => {
        load.block('reporte_produccion')
        $http.post('phrapi/marun/produccionsemana/reporteProduccion', params || {}).then(r => {
            load.unblock('reporte_produccion')
            callback(r.data)
        })
    }

    return service
}])

app.controller('produccion', ['$scope','client', 'request', function($scope, client, $request){
    cambiosLotes = function(){
        $scope.init();
    }
    $scope.type = "cosechados";
    $scope.interval = {};
    cambiarCosecha = function(){
        clearInterval($scope.interval);
        $scope.init();
    }

    $scope.printPdf = function(){
        dlCanvas("Reporte Racimos" , "pdf");
    }

    $scope.printNormal = function(){
        dlCanvas("Reporte Racimos" , "print");
    }

    $scope.id_company = 0;
    $scope.filters = {
        idFinca : 1,
        idLote : 0,
        idLabor : 0,
        fecha_inicial : '2017-09-10',//moment().startOf('month').format('YYYY-MM-DD'),
        fecha_final : '2017-09-16',//moment().endOf('month').format('YYYY-MM-DD'),
        cliente: "",
        marca: "",
        palanca : "",
        finca : "",
        type : "cosechados",
        year : moment().format('YYYY')
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate :moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
        }
        $scope.init()
    }

    $scope.tabla = {
        produccion : [],
        resumen : []
    }

    $scope.openDetalle = function(data){
        if(data.campo != "MAX" && data.campo != "MIN" && data.campo != "AVG")
            data.expanded = !data.expanded;
    }

    $scope.openDetalleLote = function(data){
        data.expanded = !data.expanded;
    }

    $scope.sorterFunc = function(semana){
        return parseInt(semana.campo);
    };

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];
    $scope.lotes = [];
    $scope.visibleColumns = 7;
    $scope.withTable = (100 / $scope.visibleColumns) + "%";

    $scope.tags = {
        racimo : {
            value : 0
        },
        ratio_cortado : {
            value : 0
        },
        ratio_procesado : {
            value : 0
        },
        ratooning : {
            value : 0
        },
        merma_cortada : {
            value : 0
        },
        merma_procesada : {
            value : 0
        },
        edad : {
            value : 0
        },
        recusados : {
            value : 0
        },
        cajas : {
            value :0
        },
        otras_cajas : {
            value : 0
        },
        calibracion : {
            value : 0
        },
        merma_cosechada : {
            value : 0
        },
        enfunde : {
            value : 0
        },
        recobro : {
            value : 0
        },
        merma_neta : {
            value : 0
        },
        merma_cajas : {
            value : 0
        },
        merma_dolares : {
            value : 0
        },
        merma_kg : {
            value : 0
        }
    }

    $scope.charts = {
        racimos : new echartsPlv(),
        peso : new echartsPlv(),
        manos : new echartsPlv(),
        calibracion : new echartsPlv(),
        dedo : new echartsPlv(),
        edad_promedio : new echartsPlv(),
    }    

    var printTags = (r) => {
        $scope.tags.cajas.value = r.data.peso_prom_cajas.value;
        $scope.tags.calibracion.value = r.data.calibracion_prom.value;
        $scope.tags.racimo.value = r.data.peso_prom_racimos.value;
        $scope.tags.edad.value = r.data.edad_prom_racimos.value;
        $scope.tags.ratio_cortado.value = r.data.ratio_cortado.value;
        $scope.tags.ratio_procesado.value = r.data.ratio_procesado.value;
        $scope.tags.merma_cosechada.value = r.data.porc_merma_cosechada.value;
        $scope.tags.merma_procesada.value = r.data.porc_merma_procesado.value;
        $scope.tags.enfunde.value = r.data.enfunde_ha.value;
        $scope.tags.merma_neta.value = r.data.porc_merma_neta.value;
        $scope.tags.merma_kg.value = r.data.merma_kg.value;
        $scope.tags.merma_cajas.value = r.data.merma_cajas.value;
        $scope.tags.merma_dolares.value = r.data.merma_dolares.value;

        setTimeout(function(){
            $(".counter_tags").counterUp({
                delay: 10,
                time: 1000
            });

            $scope.num_semanas = ($scope.filters.year == moment().format('YYYY')) ? moment().week() : 52;
        }, 250)
    }

    var printAcumulado = (r) => {
        $scope.resumen = r.resumen
    }

    $scope.init = function(){
        var data = $scope.filters;
        $request.tags(printTags, data)
        $request.resumenAcumulado(printAcumulado, data)
        //client.post('phrapi/marcel/produccionsemana/index', $scope.printDetails, data)
        $scope.initTableReporteProduccion()
    }

    $scope.columns = {
        col_racimos_cortados : true,
        col_racimos_procesados : true,
        peso_prom_cajas : false,
        col_peso_prom_otras_cajas : false,
        col_calibracion : false,
        col_ratooning : false,
        col_peso_prom_racimo : true,
        col_prom_edad_cosecha : false,
        col_ratio_cortado : true,
        col_ratio_procesado : true,
        col_merma_cosechada : false,
        col_merma_procesada : false,
        col_enfunde_ha : false,
        col_recobro : false,
        col_merma_neta : true,
        col_merma_kg : false,
        col_merma_cajas : false,
        col_merma_dolares : false,
        col_manos : false,
        col_largo_dedos : false
    }

    $scope.reviewColumns = function(column){
        
        if($("#"+column).hasClass("active")){
            $("#"+column).removeClass("active")
            $("."+column).addClass("hide")

            $scope.columns[column] = false
        }else{
            $("#"+column).addClass("active")
            $("."+column).removeClass("hide")

            $scope.columns[column] = true
        }

        $scope.visibleColumns = $(".dropdown-menu .active").length;
        $scope.withTable = (100 / $scope.visibleColumns) + "%";
    }

    $scope.start = true;
    $scope.cajas = 0;
    $scope.printDetails = function(r,b){
        b();
        $scope.lotes = [];        
        if(r){
            $scope.tabla.produccion = r.data;
            if(r.hasOwnProperty("resumen")){
                $scope.tabla.resumen = r.resumen;
            }
            /*
            *   tags
            */ 
            /*if(r.hasOwnProperty("tags")){
                if(r.tags.hasOwnProperty("peso_prom_cajas")){
                    $scope.tags.cajas.value = r.tags.peso_prom_cajas;
                }
                if(r.tags.hasOwnProperty("otras_cajas")){
                    $scope.tags.otras_cajas.value = r.tags.otras_cajas;
                }
                if(r.tags.hasOwnProperty("calibracion")){
                    $scope.tags.calibracion.value = r.tags.calibracion;
                }
                if(r.tags.hasOwnProperty("racimo")){
                    $scope.tags.racimo.value = r.tags.racimo;
                }
                if(r.tags.hasOwnProperty("ratio_cortado")){
                    $scope.tags.ratio_cortado.value = r.tags.ratio_cortado;
                }
                if(r.tags.hasOwnProperty("ratio_cortado_dos")){
                    $scope.tags.ratio_cortado_dos = r.tags.ratio_cortado_dos;
                }
                if(r.tags.hasOwnProperty("ratio_procesado")){
                    $scope.tags.ratio_procesado.value = r.tags.ratio_procesado;
                }
                if(r.tags.hasOwnProperty("ratio_procesado_dos")){
                    $scope.tags.ratio_procesado_dos = r.tags.ratio_procesado_dos;
                }
                if(r.tags.hasOwnProperty("merma_cosechada")){
                    $scope.tags.merma_cosechada.value = r.tags.merma_cosechada;
                    $scope.tags.merma_cosechada_dos = r.tags.merma_cosechada_dos;
                }
                if(r.tags.hasOwnProperty("merma_procesada")){
                    $scope.tags.merma_procesada.value = r.tags.merma_procesada;
                    $scope.tags.merma_procesada_dos = r.tags.merma_procesada_dos;
                    if($scope.tags.merma_procesada.value){
                        $scope.tags.merma_neta.value = r.tags.merma_procesada-10;
                        $scope.tags.merma_neta_dos = r.tags.merma_procesada_dos-10;
                    }
                }
                if(r.tags.hasOwnProperty("merma_kg")){
                    if(r.tags.merma_kg > 0){
                        $scope.tags.merma_kg.value = numberWithCommas(r.tags.merma_kg)

                        $scope.tags.merma_cajas.value = (r.tags.merma_kg / r.tags.peso_prom_cajas).toFixed(2);
                        $scope.tags.merma_dolares.value = numberWithCommas(($scope.tags.merma_cajas.value * 6.26).toFixed(2));
                    }
                }
            }*/
            if(r.hasOwnProperty("data") && r.data.hasOwnProperty("avg")){
                //$scope.tags.racimo.value = new Number(r.data.avg.peso).toFixed(2);
                //$scope.tags.recusados.value = new Number((r.data.totales.total_recusados / r.data.totales.total_cosechados)*100).toFixed(2);
                //$scope.tags.ratio_procesado.value = new Number(r.data.totales.ratio_procesado).toFixed(2);
                //$scope.tags.ratio_cortado.value = new Number(r.data.totales.ratio_cortado).toFixed(2);
            }
            if(r.hasOwnProperty("calibracion") && r.calibracion.hasOwnProperty("historico") && r.calibracion.historico.hasOwnProperty("series")){
                $scope.charts.calibracion.init("calibracion" , r.calibracion.historico);
            }
            // if(r.hasOwnProperty("calibre") && r.calibre.hasOwnProperty("series")){
            //     $scope.charts.calibre.init("calibre" , r.calibre);
            // }
            if(r.hasOwnProperty("peso") && r.peso.hasOwnProperty("historico") && r.peso.historico.hasOwnProperty("series")){
                $scope.charts.peso.init("peso" , r.peso.historico);
            }
            if(r.hasOwnProperty("dedo") && r.dedo.hasOwnProperty("series")){
                $scope.charts.dedo.init("dedo" , r.dedo);
            }
            // if(r.hasOwnProperty("manos") && r.manos.hasOwnProperty("series")){
            //     $scope.charts.manos.init("manos" , r.manos);
            // }
            if(r.hasOwnProperty("manos") && r.manos.hasOwnProperty("historico") && r.manos.historico.hasOwnProperty("series")){
                $scope.charts.manos.init("manos" , r.manos.historico);
            }
            if(r.hasOwnProperty("racimos") && r.racimos.hasOwnProperty("historico") && r.racimos.historico.hasOwnProperty("series")){
                $scope.charts.racimos.init("racimos" , r.racimos.historico);
            }
            if(r.hasOwnProperty("lotes")){
                $scope.lotes = r.lotes;
            }

            if(r.hasOwnProperty("edades") && r.edades.hasOwnProperty("edad")){
                //$scope.tags.edad.value = new Number(r.edades.edad).toFixed(2);
                //$scope.charts.edad_promedio.init("edad_promedio" , r.edades.edad_promedio);
            }

            /* BEGIN CAMBIO MARCEL */
            if(r.hasOwnProperty("anios")){
                $scope.anios = r.anios
            }
            /* END CAMBIO MARCEL */

            if($scope.start){
                $scope.start = false;
                $scope.interval = setInterval(function(){
                    $scope.init();
                } ,  (60000 * 5));
            }
        }
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    var printGraficaEdad = (r) => {
        
        var data = {
            series: r.chart.data,
            legend: r.chart.legend,
            umbral: r.chart.umbral,
            id: "edad_promedio",
            legendBottom : true,
            zoom : false,
            type : 'line',
            min : 'dataMin'
        }
        ReactDOM.render(React.createElement(Historica, data), document.getElementById('edad_promedio'));
    }
    var printTableReporteProduccion = (r) => {
        $scope.reporteProduccion = r.data
    }

    $scope.initGraficaEdadProm = () => {
        $request.graficaPromEdad(printGraficaEdad, $scope.filters)
    }
    $scope.initTableReporteProduccion = () => {
        $request.reporteProduccion(printTableReporteProduccion, $scope.filters)
    }

    $scope.initGraficaEdadProm()

    $scope.table = {
        orderBy : 'semana',
        reverse : false
    }
    $scope.setOrder = (column) => {
        if($scope.table.orderBy != column){
            $scope.table.orderBy = column
            $scope.table.reverse = false
        } else {
            $scope.table.reverse = !$scope.table.reverse
        }
    }

}]);

