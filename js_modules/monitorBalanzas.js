app.service('request', ['$http', ($http) => {
	var service = {}

	service.index = (params) => {
		return new Promise((resolve) => {
			$http.post('phrapi/monitor/balanzas/index', params || {}).then((r) => {
				resolve(r.data)
			})	
		})
	}

	service.polling = (params) => {
		return new Promise((resolve) => {
			$http.get('http://monitor.procesosiq.com:8010/monitor/api/v1/stages?client_id=11', params || {}).then((r) => {
				resolve(r.data)
			})
		})
	}

	service.logs = (params) => {
		return new Promise((resolve) => {
			$http.get('http://monitor.procesosiq.com:8010/monitor/api/v1/events?client_id=11', params || {}).then((r) => {
				resolve(r.data)
			})
		})
	}

	service.saveComentario = (params) => {
		return new Promise((resolve) => {
			$http.post('phrapi/monitor/balanzas/comentario', params || {}).then((r) => {
				resolve(r.data)
			})
		})
	}

	service.getComentario = (params) => {
		return new Promise((resolve) => {
			$http.post('phrapi/monitor/balanzas/getcomentario', params || {}).then((r) => {
				resolve(r.data)
			})
		})
	}

	return service
}])

app.directive('enter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.enter);
                });
                event.preventDefault();
            }
        });
    };
});

app.filter('dateFormat', function () {
    return function (input) {
		let _p = input.split('T')
        return moment(`${_p[0]} ${_p[1]}`).format('DD/MM/YYYY HH:mm:ss')
    };
});

app.filter('getNoRepeat', function () {
    return function (input, key) {
		if(input && key){
			if(Array.isArray(input) && input.length > 0){
				let r = []
				for(let i in input){
					let val = input[i]
					if(val[key])
					if(r.indexOf(val[key]) == -1) 
						r.push(val[key])
				}
				return r
			}
		}
		return []
    };
});

app.controller('controller', ['$scope', 'request', '$http', ($scope, $request, $http) => {

	$(".tooltips").tooltip()

	$scope.editing = false
	$scope.hora = moment().format('h:mm:ss a')
	$scope.data = []
	$scope.available_fincas = []
	$scope.data_unfilted = []
	$scope.filters_logs = {}

	$scope.filtroProceso = () => {
		let modal = $("#modalPoll")
		modal.modal('show')
	}

	$scope.index = async () => {
		let r = await $request.index()
		$scope.available_fincas = r.data.map((id) => parseInt(id))
		$scope.polling()
	}

	$scope.polling = async () => {
		let r = await $request.polling()

		if($scope.available_fincas.length > 0)
			$scope.data = r.filter((f) => $scope.available_fincas.includes(parseInt(f.id_finca)))
		else
			$scope.data = r

		$scope.data.map(async (row, i) => {
			let comentario = await $request.getComentario(row)
			$scope.data[i].comentarios = comentario.data
		})

		setTimeout(() => {
			$scope.$apply()
		}, 100)
	}

	$scope.pulsate = (status, id) => {
		setTimeout(() => {
			if(!status){
				$("#"+id).pulsate({
					color : '#D91E18',
					repeat: true
				})
			}
		}, 500)
	}

	$scope.saveComentario = (row) => {
		$request.saveComentario(row)
	}

	$scope.logs_data = []
	$scope.logs = () => {
		$request.logs()
		.then((r) => {
			$scope.logs_data = r
		})
	}

	$scope.filterUUID = (val, uuid) => {
		if(val != null){
			$scope.filters_logs = {
				uuid
			}

			$http.get(`http://monitor.procesosiq.com:8010/monitor/api/v1/stages/${uuid}/events`).then((r) => {
				$scope.logs_data = r.data
			})
		}
	}

	$scope.delete = (val, key) => {
		delete val[key]
	}

	$scope.edit = () => {
		$scope.editing = true
	}

	$scope.noedit = () => {
		$scope.editing = false
	}

	$("#save_fincas").click((e) => {
		e.preventDefault()
		$scope.index()
	})

	$scope.index()
	$scope.logs()
	setInterval(() => {
		if(!$scope.editing)
			$scope.polling()

	}, (30 * 1000))
	setInterval(() => {
		$scope.hora = moment().format('h:mm:ss a')
		$scope.$apply()
	}, 1000)

}])