app.service('request', ['$http', function($http){

    this.last = function(){
        return new Promise((resolve) => {
            let url = 'phrapi/enfunde/semanal/last'
            let data = {}
            $http.post(url, data).then((r) => {
                resolve(r.data)
            });
        })
    }

    this.index = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/enfunde/semanal/index'
            let data = params || {}
            $http.post(url, data).then((r) => {
                resolve(r.data)
            });
        })
    }

    this.guardar = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/enfunde/semanal/guardar'
            let data = params || {}
            $http.post(url, data).then((r) => {
                resolve(r.data)
            });
        })
    }

}])

app.controller('produccion', ['$scope', 'request', function($scope, $request){

    $scope.semanas = []
    
    save = (id_finca, lote, valor) => {
        if(id_finca > 0 && lote != ''){
            $request.guardar({
                id_finca,
                lote,
                usadas : valor,
                anio : $scope.filters.anio,
                semana : $scope.filters.semana
            }).then((r) => {
                if(r.status == 200){
					alert("Se guardo correctamente", "", "success")
					//$scope.init()
                }else{
                    alert("Hubo un error favor de intentar mas tarde")
                }
            })
        }
    }

	$scope.filters = {
        anio : moment().year(),
        semana : moment().week(),
		id_finca : 0
	}

	$scope.last = () => {
		
	}

	$scope.init = () => {
		load.block()
		$request.index($scope.filters).then((r) => {
            load.unblock()
            $scope.semanas = r.semanas
            $scope.color = r.color
            renderTable(r.data, r.lotes)
            
            $scope.$apply()
		})
	}

	renderTable = (data, lotes) => {
		let props = {
            header : [
				{
                    key : 'finca',
                    name : 'FINCA',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
					resizable : true,
					width : 150
				},
            ],
            height : 500,
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
		}
		
		for(let i in lotes){
            let lote = lotes[i]
            if(typeof lote != 'function'){
                let id_fincas = lote.ids.split(',')
                props.header.push({
                    key : `lote_${lote.nombre}`,
                    name : lote.nombre != 'TOTAL' ? `Lote ${lote.nombre}` : 'TOTAL',
                    titleClass : 'text-center',
                    sortable : true,
                    alignContent : 'center',
                    resizable : true,
                    editable : true,
                    events: {
                        onKeyDown: function(ev, column) {
                            let index = parseInt(column.rowIdx)
                            let key = column.column.key
                            let row = $scope.table1.rowGetter(index)
                            let id_finca = row.id_finca

                            if (ev.key === 'Enter') {
                                if(!id_fincas.includes(id_finca)){
                                    toastr.error('No puedes modificar ahi')
                                    return;
                                }else{
                                    save(row.id_finca, key.split('_')[1], row[key].toString().toUpperCase())
                                }
                            }
                        },
                    },
                    customCell : (data) => {
                        console.log(data);
                        return `
                            <div style={{height: '100%;', 'background-color: #cecdcd6b;' }}>
                                ${data['lote_${lote.nombre}']}
                            </div>
                        `
                    }
                })
            }
		}
        $("#table-react").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-react'))
    }

    $scope.last = () => {
        $request.last()
        .then((r) => {
            $scope.filters.anio = r.anio
            $scope.filters.semana = r.semana

            $scope.init()
        })
    }

    $scope.last()

}]);

