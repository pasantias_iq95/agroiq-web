app.filter('orderObjectBy', function() {
    return function(items, field, reverse) {
        var filtered = [];
        angular.forEach(items, function(item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field]))
                return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
            else
                return (a[field] > b[field] ? 1 : -1);
        });
        if(reverse) filtered.reverse();
        return filtered;
    };
});
function checkUmbralMerma(value, umbral){
    if(value > 0 && umbral){
        if(value < umbral) return 'bg-green-haze bg-font-green-haze'
        else return 'bg-red-thunderbird bg-font-red-thunderbird'
    }
    return ''
}

var invertidos = ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'PESO', 'CAJAS', 'CAJAS 41.5', 'RATIO CORTADO', 'RATIO PROCESADO']
function checkUmbral(value, umbral, invertido = false){
    if(!invertido){
        if(value > 0 && umbral > 0){
            if(value <= umbral) return 'bg-green-haze bg-font-green-haze'
            else return 'bg-red-thunderbird bg-font-red-thunderbird'
        }
    }else{
        if(value > 0 && umbral > 0){
            if(value >= umbral) return 'bg-green-haze bg-font-green-haze'
            else return 'bg-red-thunderbird bg-font-red-thunderbird'
        }
    }
    return ''
}

function number_format(amount, decimals) {
    
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

app.service('request', [ '$http', ($http) => {

    var service = {}

    service.last = (callback, params) => {
        $http.post('phrapi/palmar/produccionsemana/last', params || {}).then(r => {
            callback(r.data)
        })
    }

    service.tags = (callback, params) => {
        $http.post('phrapi/palmar/produccionsemana/tags', params || {}).then(r => {
            callback(r.data)
        })
    }

    service.graficaPromEdad = (callback, params) => {
        load.block('edad_promedio')
        $http.post('phrapi/palmar/produccionsemana/graficaEdadPromedio', params || {}).then(r => {
            load.unblock('edad_promedio')
            callback(r.data)
        })
    }

    service.reporteProduccion = (callback, params) => {
        load.block('reporte-produccion')
        $http.post('phrapi/palmar/produccionsemana/reporteProduccion', params || {}).then(r => {
            load.unblock('reporte-produccion')
            callback(r.data)
        })
    }

    service.graficaVariables = (callback, params) => {
        load.block('graficas')
        $http.post('phrapi/palmar/produccionsemana/graficaVariables', params || {}).then(r => {
            load.unblock('graficas')
            callback(r.data)
        })
    }

    service.last = (callback, params) => {
        $http.post('phrapi/palmar/produccionsemana/last', params || {}).then(r => {
            callback(r.data)
        })
    }

    return service
}])

app.controller('produccion', ['$scope','client', 'request', function($scope, client, $request){

    $scope.anios = [
        2019,
        2018
    ]

    $scope.pdf = () => {
        console.log("pdf")
        html2canvas($("#report"), {
            onrendered : function(canvas){
                let data = canvas.toDataURL()
                var newWindow = window.open('','','width=200,height=100');
                newWindow.document.write(`
                    <img src="${data}" />
                `)
                newWindow.focus()
                newWindow.print()
            }
        })
    }

    $scope.variables = {
        cajas : [
            "Cajas"
        ],
        merma : [
            "Merma Neta",
            "Tallo",
        ],
        racimos : [
            "Calibre",
            "Edad",
            "Manos",
            "Peso",
            "Racimos Procesados",
            "Racimos Cosechados",
            "Ratio Cortado",
            "Ratio Procesado"
        ],
    };

    $scope.id_company = 0;
    $scope.filters = {
        idFinca : 1,
        fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
        fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
        year : `${moment().year()}`,
        var1 : "Peso",
        var2 : "Calibre",
        type1 : "line",
        type2 : "line"
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate :moment().endOf('month'),
    }

    $scope.getLastWeek = (year, callback) => {
        $request.getLastWeekOfYear(function(data){
            callback(data)
        }, { year: $scope.filters.year, finca : $scope.filters.finca })
    }

    $scope.changeYear = () => {
        let data = $scope.getLastWeek($scope.filters.year, function(data) {
            //$scope.filters.fecha_inicial = data.firts_date
            //$scope.filters.fecha_final = data.second_date
            $("#date-picker").html(`${data.firts_date} - ${data.second_date}`)

            $scope.changeRangeDate({
                first_date : data.firts_date,
                second_date : data.second_date
            })
        })
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.anioCambio = moment(data.first_date).year() != moment($scope.filters.fecha_inicial).year()

            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
        }

        if($scope.anioCambio){
            $scope.filters.year = moment($scope.filters.fecha_inicial).year()
        }
        $scope.init()
    }

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];
    $scope.lotes = [];
    $scope.visibleColumns = 7;
    $scope.withTable = (100 / $scope.visibleColumns) + "%";

    $scope.tags = {
        racimo : {
            value : 0
        },
        ratio_cortado : {
            value : 0
        },
        ratio_procesado : {
            value : 0
        },
        ratooning : {
            value : 0
        },
        merma_cortada : {
            value : 0
        },
        merma_procesada : {
            value : 0
        },
        edad : {
            value : 0
        },
        recusados : {
            value : 0
        },
        cajas : {
            value :0
        },
        otras_cajas : {
            value : 0
        },
        calibracion : {
            value : 0
        },
        merma_cosechada : {
            value : 0
        },
        enfunde : {
            value : 0
        },
        recobro : {
            value : 0
        },
        merma_neta : {
            value : 0
        },
        merma_cajas : {
            value : 0
        },
        merma_dolares : {
            value : 0
        },
        merma_kg : {
            value : 0
        },
        rac_cos_ha_sem : { value : 0 },
        rac_cos_ha_anio : { value : 0 },
        cajas_ha_sem : { value : 0 },
        cajas_ha_anio : { value : 0 },
    }

    $scope.charts = {
        variables : new echartsPlv(),
    }    

    var printTags = (r) => {
        $scope.tags.cajas.value = r.data.peso_prom_cajas;
        $scope.tags.calibracion.value = r.data.calibracion_prom;
        $scope.tags.racimo.value = r.data.peso_prom_racimos;
        $scope.tags.edad.value = r.data.edad_prom_racimos;
        $scope.tags.ratio_cortado.value = r.data.ratio_cortado;
        $scope.tags.ratio_procesado.value = r.data.ratio_procesado;
        $scope.tags.merma_cosechada.value = r.data.porc_merma_cosechada;
        $scope.tags.merma_procesada.value = r.data.porc_merma_procesado;
        $scope.tags.enfunde.value = r.data.enfunde_ha;
        $scope.tags.merma_neta.value = r.data.porc_merma_neta;
        $scope.tags.merma_kg.value = r.data.merma_kg;
        $scope.tags.merma_cajas.value = r.data.merma_cajas;
        $scope.tags.merma_dolares.value = r.data.merma_dolares;
        $scope.tags.rac_cos_ha_sem.value = r.data.rac_cos_ha_sem;
        $scope.tags.rac_cos_ha_anio.value = r.data.rac_cos_ha_anio;
        $scope.tags.cajas_ha_sem.value = r.data.cajas_conv_ha_sem;
        $scope.tags.cajas_ha_anio.value = r.data.cajas_conv_ha_anio;
        $scope.tags.recobro.value = r.data.recobro

        setTimeout(function(){
            $(".counter_tags").counterUp({
                delay: 10,
                time: 1000
            });

            $scope.num_semanas = ($scope.filters.year == moment().format('YYYY')) ? moment().week() : 52;
        }, 250)
    }

    $scope.init = function(){
        var data = $scope.filters;
        $request.tags(printTags, data)
        $scope.initGraficaEdadProm()
        $scope.initGraficaVariables()
        $scope.initTableReporteProduccion()
    }

    $scope.changeFinca = () => {
        $scope.init()
    }

    $scope.start = true;
    $scope.cajas = 0;

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    var printGraficaEdad = (r) => {
        
        var data = {
            series: r.chart.data,
            legend: r.chart.legend,
            umbral: r.chart.umbral,
            id: "edad_promedio",
            legendBottom : true,
            zoom : false,
            type : 'line',
            min : 'dataMin'
        }
        ReactDOM.render(React.createElement(Historica, data), document.getElementById('edad_promedio'));
    }

    $scope.initGraficaEdadProm = () => {
        $request.graficaPromEdad(printGraficaEdad, $scope.filters)
    }

    $scope.initTableReporteProduccion = () => {
        $request.reporteProduccion(printTablaProduccion, $scope.filters)
    }

    var data_variables = {}
    var printGraficaVariables = (r) => {
        data_variables = r.data
        $scope.charts.variables.init('variables', r.data)
    }

    $scope.initGraficaVariables = () => {
        $request.graficaVariables(printGraficaVariables, $scope.filters)
    }

    $scope.selected = (val) => {
        return [$scope.filters.var1, $scope.filters.var2].indexOf(val) > -1;
    }

    $scope.toggleLineBar = (num, type) => {
        if(num == 1)
        data_variables.series.map((data, index) => {
            if(data.name.toUpperCase().includes($scope.filters.var1.toUpperCase())){
                data_variables.series[index].type = type
                $scope.filters.type1 = type
            }
        })

        if(num == 2)
        data_variables.series.map((data, index) => {
            if(data.name.toUpperCase().includes($scope.filters.var2.toUpperCase())){
                data_variables.series[index].type = type
                $scope.filters.type2 = type
            }
        })
        printGraficaVariables({data : data_variables})
    }

    $scope.last = () => {
        $request.last((r) => {
            if(Object.keys(r.fincas).length > 0)
            if(Object.keys(r.fincas).indexOf($scope.filters.finca) == -1){
                $scope.filters.finca = Object.keys(r.fincas)[0]
            }
            $scope.filters.fecha_inicial = r.fecha
            $scope.filters.fecha_final = r.fecha
            $("#date-picker").html(`${r.fecha} -  ${r.fecha}`)
            $scope.init()
        })
    }

    var printTablaProduccion = (r) => {
        let id = 'reporte-produccion'
        var props = {
            header : [{
                   key : 'campo',
                   name : 'VARIABLE',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                   key : 'avg',
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['avg'])
                        let valueCell = valNumber > 0 ? valNumber : ''
                        var umbralClass = ''
                        if(valNumber > 0 && ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'RACIMOS RECUSADOS', 'CAJAS', 'CAJAS 41.5'].indexOf(rowData['campo']) >= 0) valueCell = number_format(valNumber, 0);
                        if(['MERMA CORTADA', 'MERMA PROCESADA', 'MERMA NETA'].indexOf(rowData['campo']) >= 0) umbralClass = checkUmbralMerma(valNumber, 2)
                        else umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)
                        return `
                            <div class="text-center ${umbralClass}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max'])
                        let valueCell = valNumber > 0 ? valNumber : ''
                        var umbralClass = ''
                        if(valNumber > 0 && ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'RACIMOS RECUSADOS', 'CAJAS', 'CAJAS 41.5'].indexOf(rowData['campo']) >= 0) valueCell = number_format(valNumber, 0);
                        if(['MERMA CORTADA', 'MERMA PROCESADA', 'MERMA NETA'].indexOf(rowData['campo']) >= 0) umbralClass = checkUmbralMerma(valNumber, 2)
                        else umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)
                        return `
                            <div class="text-center ${umbralClass}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min'])
                        var valueCell = valNumber > 0 ? valNumber : ''
                        var umbralClass = ''
                        if(valNumber > 0 && ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'RACIMOS RECUSADOS', 'CAJAS', 'CAJAS 41.5'].indexOf(rowData['campo']) >= 0) valueCell = number_format(valNumber, 0);
                        if(['MERMA CORTADA', 'MERMA PROCESADA', 'MERMA NETA'].indexOf(rowData['campo']) >= 0) umbralClass = checkUmbralMerma(valNumber, 2)
                        else umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)
                        return `
                            <div class="text-center ${umbralClass}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : r.data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ],
            height : r.data.length*35+80
        }
        Object.keys(r.semanas).map((key) => {
            let value = r.semanas[key]
            props.header.push({
                key : `sem_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+value])
                    let valueCell = valNumber > 0 ? valNumber : ''
                    var umbralClass = ''
                    if(valNumber > 0 && ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'RACIMOS RECUSADOS', 'CAJAS', 'CAJAS 41.5'].indexOf(rowData['campo']) >= 0) valueCell = number_format(valNumber, 0);
                    if(['MERMA CORTADA', 'MERMA PROCESADA', 'MERMA NETA'].indexOf(rowData['campo']) >= 0) umbralClass = checkUmbralMerma(valNumber, 2)
                    else umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)
                    return `
                        <div class="text-center ${umbralClass}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }

    $scope.lastFinca = () => {
        $request.last((r) => {
            if(r.finca){
                $scope.filters.idFinca = r.finca.id
            }
            $scope.fincas = r.fincas
            $scope.init()
        }, $scope.filters)
    }
    $scope.lastFinca()

}]);

