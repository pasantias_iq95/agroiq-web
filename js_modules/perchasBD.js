app.service('request',['$http', function($http){
    this.list = function(callback, params){
        let url = 'phrapi/bdPerchas/list'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
}])

app.controller('bdperchas', ['$scope', 'request' , function($scope,request){
    
    $scope.data = {}
    $scope.search = {}

    $scope.init = function(){
        request.list((r) => {
            $scope.listado = r.listado
        }, $scope.data)
    }

    $scope.init();
}])