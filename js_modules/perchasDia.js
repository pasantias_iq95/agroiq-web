const { app, load } = window

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('sumOfValueDouble', function () {
    return function (data, key, key2) {        
        if (angular.isUndefined(data) || angular.isUndefined(key) || angular.isUndefined(key2))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key][key2] != "" && value[key][key2] != undefined && parseFloat(value[key][key2])){
                sum = sum + parseFloat(value[key][key2], 10);
            }
        });
        return sum;
    }
})

app.filter('getNotRepeat', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return [];
        
        let _arr = []
        data.forEach((value) => {
            if(value[key]){
                if(!_arr.includes(value[key])) _arr.push(value[key])
            }
        })
        return _arr;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && value[key] > 0){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('keys', function () {
    return function (data) {        
        return Object.keys(data).length
    }
})

app.service('request', ['$http', ($http) => {
    let service = {}

    service.index = (callback, params) => {
        load.block('defectos')
        load.block('clusterGrado')
        load.block('porCategoria')
        load.block('fotos')
        $http.post('phrapi/perchasDia/index', params)
        .then((r) => {
            load.unblock('defectos')
            load.unblock('clusterGrado')
            load.unblock('porCategoria')
            load.unblock('fotos')
            callback(r.data)
        })
    }

    service.variables = (callback, params) => {
        load.block()
        $http.post('phrapi/perchasDia/variables', params)
        .then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.last = (callback, params) => {
        load.block()
        $http.post('phrapi/perchasDia/last', params)
        .then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.list = function(callback, params){
        let url = 'phrapi/bdPerchas/list'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }

    return service
}])

function initPastel(id, series){
    var legends = []
    series.map(key => {
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })
    setTimeout(() => {
        let data = {
            data : series,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
    }, 250)
}

app.controller('controller', ['$scope','request', function($scope, $request){

    // UMBRAL

    $scope.loadCalidadUmbral = () => {
        let c = localStorage.getItem('banano_calidad_cluster_umbral')
        if(c){
            $scope.umbral_cluster = JSON.parse(c)
        }else{
            $scope.umbral_cluster = {
                max : 92,
                min : 90
            }
        }

        let d = localStorage.getItem('banano_calidad_dedos_umbral')
        if(d){
            $scope.umbral_dedos = JSON.parse(d)
        }else{
            $scope.umbral_dedos = {
                max : 92,
                min : 90
            }
        }

        let e = localStorage.getItem('banano_calidad_empaque_umbral')
        if(e){
            $scope.umbral_empaque = JSON.parse(e)
        }else{
            $scope.umbral_empaque = {
                max : 92,
                min : 90
            }
        }
    }
    $scope.saveCalidadUmbral = () => {
        toastr.success('Umbral guardado')
        localStorage.setItem('banano_calidad_cluster_umbral', JSON.stringify($scope.umbral_cluster))
        localStorage.setItem('banano_calidad_dedos_umbral', JSON.stringify($scope.umbral_dedos))
        localStorage.setItem('banano_calidad_empaque_umbral', JSON.stringify($scope.umbral_empaque))
        $scope.reRenderMarkers()
        $scope.closeMenu()
    }

    $scope.openMenu = () => {
        $(".toggler-close, .theme-options").show()
    }
    $scope.closeMenu = () => {
        $(".toggler-close, .theme-options").hide()
    }

    $scope.umbral_cluster = {}
    const getUmbralClusterMin = () => {
        return $scope.umbral_cluster.min
    }

    const getUmbralClusterHigh = () => {
        return $scope.umbral_cluster.max
    }
    $scope.getClusterUmbral = (value) => {
        if(value <= getUmbralClusterMin()){
            return 'font-red-thunderbird'
        }
        else if(value >= getUmbralClusterHigh()){
            return 'font-green-haze'
        }else {
            return 'font-yellow-gold'
        }
    }

    $scope.umbral_dedos = {}
    const getUmbralDedosMin = () => {
        return $scope.umbral_dedos.min
    }

    const getUmbralDedosHigh = () => {
        return $scope.umbral_dedos.max
    }
    $scope.getDedosUmbral = (value) => {
        if(value <= getUmbralDedosMin()){
            return 'font-red-thunderbird'
        }
        else if(value >= getUmbralDedosHigh()){
            return 'font-green-haze'
        }else {
            return 'font-yellow-gold'
        }
    }

    $scope.umbral_empaque = {}
    const getUmbralEmpaqueMin = () => {
        return $scope.umbral_empaque.min
    }

    const getUmbralEmpaqueHigh = () => {
        return $scope.umbral_empaque.max
    }
    $scope.getEmpaqueUmbral = (value) => {
        if(value <= getUmbralEmpaqueMin()){
            return 'font-red-thunderbird'
        }
        else if(value >= getUmbralEmpaqueHigh()){
            return 'font-green-haze'
        }else {
            return 'font-yellow-gold'
        }
    }

    // 

    // BEGIN CONFIG
    $scope.config = {
        calidad_empaque : true,
        peso_prom_cluster : true
    }

    $scope.tag_md = $scope.config.calidad_empaque ? 4 : 3
    // END CONFIG

    $scope.datesEnabled = []
    $scope.total_empaque = { cantidad : 0}
    $scope.total_defectos = { cantidad_cluster_caja : 0 }
    $scope.clientes = []
    $scope.tags = {}
    $scope.filters = {
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
        sucursal : '',
        cliente : '',
        fotos : {},
        porCategoria : ''
    }

    $scope.changeRangeDate = function(data){
		if(data){
			$scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
			$scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
            $scope.variables()
		}
    }

    $scope.rangesDirectives = {
        'Últimos 7 Días': [moment().subtract(6, 'days'), moment()],
        'Últimos 30 Días': [moment().subtract(29, 'days'), moment()],
        'Últimos 90 Días': [moment().subtract(89, 'days'), moment()],
        'Último Año' : [moment().subtract(1, 'year').startOf('year') , moment().subtract(1, 'year').endOf('year')],
        'Este Año' : [moment().startOf('year') , moment().endOf("year")],
        'Este mes': [moment().startOf('month'), moment().endOf('month')],
        'Último mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }

    $scope.init = () => {
        $request.index((r) => {
            $scope.tags = r.tags
            $scope.defectos = r.defectos
            $scope.clusterGrado = r.clusterGrado
            $scope.porCategoria = r.porCategoria
            $scope.participacionMercado = r.participacionMercado
            $scope.fotos = r.fotos

            $scope.reRenderClusterGrado()
            $scope.reRenderPorCategoria()
            $scope.reRenderParticipacionMercado()
            $scope.baseDatos()
            
        }, $scope.filters)
    }

    $scope.baseDatos = function(){
        $request.list((r) => {
            $scope.listado = r.listado
        }, $scope.filters)
    }

    $scope.reRenderClusterGrado = () => {
        setTimeout(() => {
            initPastel('clusterGrado-pie', $scope.clusterGrado.data)
        }, 100)
    }

    $scope.reRenderParticipacionMercado = () => {
        setTimeout(() => {
            initPastel('participacion-pie', $scope.participacionMercado.data.map((c) => { return { label : `${c.participante}`, value : c.porcentaje } }))
        }, 100)
    }

    $scope.reRenderPorCategoria = () => {
        setTimeout(() => {
            if(!$scope.porCategoria.data[$scope.filters.porCategoria]){
                $scope.filters.porCategoria = Object.keys($scope.porCategoria.data)[0]
            }
            
            if($scope.porCategoria.data[$scope.filters.porCategoria]){
                initPastel('porCategoria-pie', $scope.porCategoria.data[$scope.filters.porCategoria].map((c) => { return { label : `${c.defecto}`, value : c.cantidad } }))
            }else{
                console.error('No hay info de daños por categoria')
                initPastel('porCategoria-pie', [])
            }
        }, 100)
    }

    $scope.variables = () => {
        $request.variables((r) => {
            $scope.sucursales = r.sucursales
            // seleccionar marca
            if(r.sucursales){
                if($scope.filters.sucursal){
                    if(!r.sucursales.includes($scope.filters.sucursal)){
                        $scope.filters.sucursal = r.sucursales[0]
                    }
                }
            }else{
                // todas
                $scope.filters.sucursal = ''
            }

            $scope.clientes = r.clientes
            // seleccionar una finca default
            if(r.clientes){
                let cliente = r.clientes.filter((c) => c === $scope.filters.cliente)
                if(!cliente.length > 0 && r.clientes.length > 0){
                    $scope.filters.cliente = r.clientes[0]
                    $scope.variables()
                    return ;
                }
            }

            $scope.init()
        }, $scope.filters)
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.datesEnabled = r.days
            if(r.fecha){
                $scope.filters.fecha_inicial = r.fecha
                $scope.filters.fecha_final = r.fecha
                setTimeout(() => {
                    $("#date-picker").html(`${r.fecha} - ${r.fecha}`)
                }, 100)
            }
            $scope.filters.id_finca = r.id_finca
            $scope.variables()
        })
    }

    $scope.deleteFilter = (campo) => {
        delete $scope.filters.fotos[campo]
    }

    $scope.last()
	$scope.loadCalidadUmbral()
}]);