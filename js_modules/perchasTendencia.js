const { app, load } = window

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && value[key] > 0){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.service('request', ['$http', ($http) => {
    let service = {}

    service.last = (callback, params) => {
        load.block()
        $http.post('phrapi/perchasTendencia/last', params)
        .then((r) => {
            load.unblock()
            callback(r.data)
        })
    }

    service.index = (callback, params) => {
        load.block('principal')
        $http.post('phrapi/perchasTendencia/index', params)
        .then((r) => {
            load.unblock('principal')
            callback(r.data)
        })
    }

    return service
}])

function getOptionsGraficaReact(id, options, umbral){
    let newOptions = {
        series: options.series,
        legend: options.legends,
        umbral : umbral,
        id: id,
        min : 'dataMin',
        actions : false
    }
    return newOptions
}

function initGrafica(id, options, umbral){
    setTimeout(() => {
        let data = getOptionsGraficaReact(id, options, umbral)
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById(id));
    }, 250)
}

app.controller('controller', ['$scope','request', '$filter', function($scope, $request, $filter){

    $scope.sucursales_available = []
    $scope.filters = {
        anio : moment().year(),
        mode : 'general',
        period : 'semana',
        var : 'kilos_totales'
    }

    $scope.variables_categorias = {
        general : [
            'gavetas',
            'kilos_totales',
            'kilos_destruidos',
            'porc_fruta_lancofruit',
            'porc_fruta_terceros',
            'porc_percha_vacia'
        ],
        temperatura : [
            'temp_bodega',
            'temp_percha_1',
            'temp_percha_2'
        ],
        cluster : [
            'grado_2_cantidad_cluster',
            'grado_3_cantidad_cluster',
            'grado_4_cantidad_cluster',
            'grado_5_cantidad_cluster',
            'grado_6_cantidad_cluster'
        ],
        defectos : [
            'total_defectos'
        ]
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.filters.anio = r.anio
            $scope.clientes = r.clientes
            $scope.sucursales_clientes = r.sucursales
            //$scope.index()
            $("#sucursales").chosen()
        })
    }

    $scope.changeCliente = () => {
        $scope.sucursales = $scope.sucursales_clientes[$scope.filters.cliente]
        chosenUpdate()
        $scope.index()
    }

    // CATEGORIAS
    $scope.$watch('filters.mode', () => {
        // cambiar primera pestaña
        setTimeout(() => {
            $scope.filters.var = $scope.variables_categorias[$scope.filters.mode][0]
            $scope.index()
        }, 100)
    })

    $scope.$watch('filters.cliente', (newvalue, oldvalue) => {
        if(!oldvalue && newvalue){
            $("#contenedor").removeClass('hide')
        }else if(oldvalue && !newvalue){
            $("#contenedor").addClass('hide')
        }
    })

    $scope.changeSucursales = () => {
        $scope.filters.sucursales = $("#sucursales").val()
        $scope.index()
    }

    /** BEGIN GENERAL */
    $scope.index = () => {
        $request.index((r) => {
            $scope.sucursales_available = r.sucursales_available || []
            $scope.defectos = r.defectos
            deseleccionarNoHabilidas()
            chosenUpdate()

            let _data_chart = r.grafica
            let serie_total = {
                connectNulls : true,
                data : [],
                name : $scope.nombre_cliente,
                type : 'line'
            }
            let _data = r.tabla ? r.tabla.data : []
            let semanas = r.tabla ? r.tabla.semanas : []
            let umbral = Math.round($filter('avgOfValue')(_data, 'prom')*100)/100
            let total = {
                detalle : $scope.nombre_cliente,
                prom : umbral,
                max : Math.round($filter('avgOfValue')(_data, 'max')*100)/100,
                min : Math.round($filter('avgOfValue')(_data, 'min')*100)/100,
            }
            for(let i in semanas){
                let sem = semanas
                total[`sem_${sem}`] = Math.round($filter('avgOfValue')(_data, `sem_${sem}`)*100)/100
                serie_total.data.push(total[`sem_${sem}`])
            }
            //_data.push(total)
            //_data_chart.series.push(serie_total)
            
            renderGraficaGeneral(_data_chart, umbral)
            renderTablaGeneral(_data, semanas)
        }, $scope.filters)
    }

    const deseleccionarNoHabilidas = () => {
        let selected = $("#sucursales").val() || []
        let newselected = []
        for(let i in selected){
            let suc = selected[i]
            if($scope.sucursales_available.indexOf(suc) !== -1){
                newselected.push(suc)
            }
        }
        $("#sucursales").val(newselected)

        if(newselected.length != selected.length){
            $scope.changeSucursales()
        }
    }

    const chosenUpdate = () => {
        setTimeout(() => {
            $scope.$apply()
            $("#sucursales").trigger("chosen:updated");
        }, 250)
    }

    const renderGraficaGeneral = (data, umbral) => {
        initGrafica('chart', data, umbral)
    }

    const renderTablaGeneral = (data, semanas) => {
        let umbral = null
        if($scope.filters.var == 'calidad_cluster') umbral = getClusterUmbral
        if($scope.filters.var == 'calidad_dedos') umbral = getDedosUmbral
        if($scope.filters.var == 'calidad_empaque') umbral = getEmpaqueUmbral

        let props = {
            header : [{
                   key : 'detalle',
                   name : 'DETALLE',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                   key : 'prom',
                   name : 'PROM',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : (row) => {
                        return `
                            <div class="${umbral != null ? umbral(row[`prom`]) : ''}">
                                ${row[`prom`]}
                            </div>
                        `
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : (row) => {
                        return `
                            <div class="${umbral != null ? umbral(row[`max`]) : ''}">
                                ${row[`max`]}
                            </div>
                        `
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : (row) => {
                        return `
                            <div class="${umbral != null ? umbral(row[`min`]) : ''}">
                                ${row[`min`]}
                            </div>
                        `
                    }
                }
            ],
            data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ],
            height : data.length*35+80
        }
        semanas.map((sem) => {
            props.header.push({
                key : `sem_${sem}`,
                name : `${sem}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : (row) => {
                    return `
                        <div class="${umbral != null ? umbral(row[`sem_${sem}`]) : ''}">
                            ${row[`sem_${sem}`]}
                        </div>
                    `
                }
            })
        })
        document.getElementById('tabla-general').innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('tabla-general'))
    }
    /** END GENERAL */

    $scope.last()

}]);