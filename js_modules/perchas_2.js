
app.service('perchas',[ 'client' , function(client){
    this.tags = function(params , callback){
        var url = 'phrapi/perchas/tags'
        var data = params || {}
        client.post(url , callback , data );
    }
}])

app.controller('perchas', ['$scope', '$http', '$location', 'perchas', '$controller', '$window', function ($scope, $http, $location, perchas, $controller, $window) {
    
    $scope.service = {
        index : function(){
            var data = {} 
            perchas.tags(data , $scope.print)
        },
        all : function(){
            this.index()
        }
    } 

    $scope.table = [];
    $scope.tableHistorica = [];
    $scope.tableComparation = [];

    $scope.print = function(r , b){
        b()
        if(r.hasOwnProperty("tags")){
            var data = {
                colums : 3,
                tags: r.tags,
                withTheresholds: false
            }
            ReactDOM.render(React.createElement(ListTags, data), document.getElementById('indicadores'));
        }
        if(r.hasOwnProperty("table")){
            $scope.table = r.table;
        }

        if(r.hasOwnProperty("table_history")){
            $scope.tableHistorica = r.table_history;
        }

        if(r.hasOwnProperty("table_comparation")){
            $scope.tableComparation = r.table_comparation;
        }

        if(r.hasOwnProperty("grafica_comparation")){
            var data = {
                data: r.grafica_comparation,
                umbral: 0,
                height: "charts-300",
                id: "comparation_charts"
            }
            ReactDOM.render(React.createElement(Barras, data), document.getElementById('comparation_chart'));
        }

        if(r.hasOwnProperty("grafica")){
            var data = {
                data: r.grafica,
                id: "main_charts",
                height: "charts-300",
                nameseries : "Causas"
            }
            ReactDOM.render(React.createElement(Pastel, data), document.getElementById('main_chart'));
        }

        if(r.hasOwnProperty("history")){
             var data = {
                series: r.history,
                legend: r.legend_history,
                umbral: 0,
                id: "historica_charts"
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('historico_chart'));
        }
    }
}]);