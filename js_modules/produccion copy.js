//
// Pipelining function for DataTables. To be used to the `ajax` option of DataTables
//
$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    console.log(opts);
    var conf = $.extend( {
        pages: 5,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
                      // matching how `ajax.data` works in DataTables
        method: 'POST' // Ajax HTTP method
    }, opts );
 
    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;
 
    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;
         
        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
                  JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
                  JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }
         
        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );
 
        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));
 
                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }
             
            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);
 
            request.start = requestStart;
            request.length = requestLength*conf.pages;
 
            // Provide the same `data` options as DataTables.
            console.log(conf);
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }

            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);
 
                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    json.data.splice( requestLength, json.data.length );
                    $('#datatable_ajax tbody').off( 'click', 'button');
                    $('#datatable_ajax').off('click', '.filter-submit');
                    $('#datatable_ajax').off('click', '.filter-cancel');
                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            
            json.draw = request.draw; // Update the echo for each response
            /*json.data.splice( 0, requestStart-cacheLower );*/
            json.data.splice( requestLength, json.data.length );
            $('#datatable_ajax tbody').off( 'click', 'button');
            $('#datatable_ajax').off('click', '.filter-submit');
            $('#datatable_ajax').off('click', '.filter-cancel');
            drawCallback(json);
        }
    }
};
 



// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );
 

var TableDatatablesAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    }

    var handleRecords = function () {

        var grid = new Datatable();
        grid.params = {};
        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error  
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { 

                "lengthMenu": [
                            [10, 20, 50, 100, 150, -1],
                            [10, 20, 50, 100, 150, "Todos"] // change per page values here
                ],
                "language": {
                    "lengthMenu": "Vista de _MENU_ registros por página",
                    "zeroRecords": "No se encontro ningun registro",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "aria": {
                        "sortAscending": ": orden acendente",
                        "sortDescending": ": orden decendente"
                    },
                    "emptyTable": "No hay registros en la tabla",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior",
                        "page": "Página",
                        "pageOf": "de"
                    },
                    "select": {
                        "rows": {
                            _: " %d filas selecionadas",
                            0: "",
                            1: "1 fila seleccionada"
                        }
                    }
                },

                "pageLength": 10, // default record count per page
                "ajax":  $.fn.dataTable.pipeline( {
                            url: "phrapi/produccion/index", // ajax source
                            pages: 10, // number of pages to cache,
                            data : function(){
                                var table = $('#datatable_ajax');
                                grid.params = {};
                                $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });

                                // get all checkboxes
                                $('input.form-filter[type="checkbox"]:checked', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });

                                // get all radio buttons
                                $('input.form-filter[type="radio"]:checked', table).each(function() {
                                    var _self = $(this);
                                    var name = _self.attr("name");
                                    var data = [];
                                    if (!data[name]) {
                                        data[name] = _self.val();
                                    }
                                    $.extend( grid.params, data);
                                });
                                // console.log(grid.params);
                                var data = {
                                    data : grid.params
                                }

                                return grid.params;
                            }
                } ),
                "columnDefs": [

                        ],
                "order": [
                    [0, "asc"]
                ],// set first column as a default sort by asc
                "buttons": [
                            { extend: 'print', className: 'btn dark btn-outline', "title" : "Produccion", "text" : "Imprimir",
                                customize: function ( win ) {
                                    $(win.document.body)
                                        .css( 'font-size', '10pt' )
                                        .css( 'float', 'rigth' )
                                        .prepend(
                                            '<img src="http://app.procesos-iq.com/assets/logo.png" />'
                                        );
                 
                                    $(win.document.body).find( 'table' )
                                        .addClass( 'compact' )
                                        .css( 'font-size', 'inherit' );
                                },
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            { extend: 'print', className: 'btn dark btn-outline', "title" : "Producción", "text" : "Imprimir <i class='fa fa-check' aria-hidden='true'></i>",
                                customize: function ( win ) {
                                    $(win.document.body)
                                        .css( 'font-size', '10pt' )
                                        .css( 'float', 'rigth' )
                                        .prepend(
                                            '<img src="http://app.procesos-iq.com/assets/logo.png" />'
                                        );
                 
                                    $(win.document.body).find( 'table' )
                                        .addClass( 'compact' )
                                        .css( 'font-size', 'inherit' );
                                },
                                exportOptions: {
                                    modifier: {
                                        selected: true
                                    },
                                    columns: ':visible'
                                }
                            },
                             { extend: 'pdfHtml5',"text" : "PDF", "title" : "Produccion" ,className: 'btn green btn-outline',
                                customize: function ( doc ) {
                                    var cols = [];
                                       cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                                       cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
                                       var objFooter = {};
                                       objFooter['columns'] = cols;
                                       doc['footer']=objFooter;
                                       doc.content.unshift(
                                        {
                                            margin: [0, 0, 0, 12],
                                            alignment: 'left',
                                            image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAoAIoDASIAAhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAAAAcFBgEECAID/8QAORAAAAUDAgMGBAQEBwAAAAAAAQIDBAUABhESIQcTMRQiIzJBUUJhcYEVUrHwCBaR0WJyc4KhsuH/xAAZAQACAwEAAAAAAAAAAAAAAAAAAwECBAX/xAAtEQABBAECAwYGAwAAAAAAAAABAAIDERIEIRMxUSIyM0FxoQUjNFKBwWFikf/aAAwDAQACEQMRAD8A6BvK6W1sxwLLEOs4ObSmiX/sYfhIH5vt1pLznG5RBUQapC/VDrhTlIF+mMib99aiOO8msM2JuYfnOtSRcD5W5TYAv+4dx+9aNpwMdAx0W/lmSD+ZlCdpaNXAakWzfoCxyfGJ/QvQA361WWThW0f7+lgkle5+LNlINeP88kqGuMiDp/lLzSj/AF1j+lNTh7xdhbtdEYrpHjZQ/kRVNqKr/lNtkf8ACIAPtmtyGixlYbD5Ji5QAu6CzRLl/wBNIVXILhNaZ55WaMCxWzc+r8OMfwkVQ31a+ok8ogHpvnIbBlinLzzTGsmaQbsJz0UrD8WklzLOIS2p2Vh0DiRWQbo+Ht5jEAfMAfapiV4kRMcjCmKxlnR5dIyzZFu0EyukMZyURAc71ryCfxW9Ve6KpKvEOLawKkrJM5WNTBfsyaDtmZNZc4hkCpp7ibP96925f0dNy4RZ2UrFSRkxVSbSTUUTKkDqYm4gbFGQU5turVzoqixvEiIfXia2TNZFnJhr0g7RKmBsb7d4R3DcNtwq9UA2pDg7ks0ViipVlmiisUIWaKKKEIrGKzRQhJPifakc7kWbiRZgsPIMkUwmOXoIj8Ih71S+KiJ457bs61TH8JPHJR+oO8CCqWvwx/LkB298GroW44dKajjtz9xTzJqflN/7+lLJcj6C7QxkWaSzNx3VUHKetFcv6fsK4mqe/Tzue+yx3smGBk0eLaDlV4XiByGGgFQxp96u1pIrTNnTPa3AMxnSnbMhVHTq7hg1B9RH7gGQ9KriMdarRYHDW02JFw7wc1dVVLV/pmHFWSHbzFwPuecR7KbSU5lC+FoD4QL8vl065AaTFqY2vAitxKqzSSAXKaUVaXEBOz7YZ25PW9Mozcel2YrZu0MoVyIdDJmDYc+/vnGaj+JTZ5IXTaM85YXazSVbK9pRjtSizI2nu6dGQIJhHf3AAyACAhTlJCkKAAV7IFKHQO0GqDvCRj7ThFJSWfSvJIYqZSJKajKHHoUOn/OPWu0BI6m47rO6MBtOdsFSXh3Llra09FRVxSKNuuVUnDWTRN21cihA8YgD5zFz9+lfKPkbil71LJRbi6kbfQOd29SfsypE0gXIIIk06ziI/v32ojiRGu5KOaSLO5Y5OR09lcuDeGrq2KbJfQcl7wZDcM7b1u3ZfkVCTZoZkE/LyxPOiyPq07Z0j88ewDj1xTzp57xwSs4e9nsl2vbV6vWLm9CxCBJM0j+KJkUFUHxSJiJSoglo8mPhzkQx9K6Mh3gSUW1eclZDnplV5axBKdPIeUQHoIVQrUvOFuOFk3zVzLoqRyQqOWqygc0oAUR23wOdI+v1xUPBcVYKclGMfHpXF2p2pygIqYhdIAXOrumH99cUM084J7HLmpY+GPfPmnLRStgeIcHMW7NTBDTTdtFkKZci5iaz5zjTpMIZEQx1DetJxxWhGtvM5hZpcfZna6iKRB5Ws3LxqN58YyOOvXNX4ExNYJvHiq8k36KWN2cSIa2TMm6gSjySdJkUKzQEDKJlN01b4yP5QyP23rYsbiLDXSu7bJi9YPGxRUUQeCUvcDzGAc+nrnAhRwZQ3PHZAnjLsct0xqKTzrjRElVXNHxU2+jmxvHfIphyyh6G3HoPz05qyyfEeAY2c2uQFl12TkwJoppk8Uym+U8CIABgwbOR9PXbNjBKKtvNQNREbp3JXyildA8WWEjNMY59EykWd9oM2VcJhoUA3l9ehvQdw+lND7VSSN0ZpwV2SNkFtWa+ayRFkxIoQpyj1AwZCiiluFjdXWiSGjCH1Ej2hT+4Il/tUgAAAYAKKKhrGs7opSbXqlxxpW5Vq6F7cVnWKinjAisKZ24h5VA0lMP39PXYRoorRB4jfVI1HhlKPh+9lG18xbOx30y8gznS7Sg8J4aBBN4hR+HYPiAC5HYPnpyjJW1+I8yrcjiejW7lVYyL2KPyzKFMfWXvjsYMdS+ggGaKK7Z+oMXkR+Vxq+Tn/KnYOMRb2TeE9GsbiIo7ZmaAeSEhhc8w4ZMAEIAm6+bcNx+eLn/D7bbZraJZJ3Hpkkll1BKssl4hSB3AxqDIfF9cjRRWHUvIjeP7LXp2AyN9P2lA/gpz+aZi1Y1usRs/kylE/KNo0lOfQYRx0ADavsFW7jNBCnK2dbMU1XOzaoFSASpmMHiHAmoRLtkdGR+uaKK1OmcZGel/mlnawcN3rXui7ir2Zxs/mSYj3biJObWkqkTUGnk8sMemoo+g423qch3T+/mV1LsLUYxqbhmsk2fily13Jz7FKJsBnIdTbgA4oopT6OmbNXaFD3TGCpzF5FLCBO0jIh9Ezil3tpBRUxRjWAlTSXKIAHeIYB32N6DkMYpgTcUhb/Cpk0XtWYkmDxc7lQjhyVNywPpACGHQQcZD3DbODZziiin6jvsH3EEpUA7Dz0URwwfSyPEBixtB9MP7b1BzyvUjFTSSx3shuBRD0MGnI7bhmumcUUVg+Iipq6LfoN4r6r//2Q=='
                                        }
                                       );
                                },
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            { extend: 'pdfHtml5',"text" : "PDF <i class='fa fa-check' aria-hidden='true'></i>", "title" : "Producción" ,className: 'btn green btn-outline',
                                customize: function ( doc ) {
                                   
                                    // Data URL generated by http://dataurl.net/#dataurlmaker
                                      var cols = [];
                                       cols[0] = {text: 'Left part', alignment: 'left', margin:[20] };
                                       cols[1] = {text: 'Right part', alignment: 'right', margin:[0,0,20] };
                                       var objFooter = {};
                                       objFooter['columns'] = cols;
                                       doc['footer']=objFooter;
                                       doc.content.unshift(
                                        {
                                            margin: [0, 0, 0, 12],
                                            alignment: 'left',
                                            image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAoAIoDASIAAhEBAxEB/8QAHAAAAgIDAQEAAAAAAAAAAAAAAAcFBgEECAID/8QAORAAAAUDAgMGBAQEBwAAAAAAAQIDBAUABhESIQcTMRQiIzJBUUJhcYEVUrHwCBaR0WJyc4KhsuH/xAAZAQACAwEAAAAAAAAAAAAAAAAAAwECBAX/xAAtEQABBAECAwYGAwAAAAAAAAABAAIDERIEIRMxUSIyM0FxoQUjNFKBwWFikf/aAAwDAQACEQMRAD8A6BvK6W1sxwLLEOs4ObSmiX/sYfhIH5vt1pLznG5RBUQapC/VDrhTlIF+mMib99aiOO8msM2JuYfnOtSRcD5W5TYAv+4dx+9aNpwMdAx0W/lmSD+ZlCdpaNXAakWzfoCxyfGJ/QvQA361WWThW0f7+lgkle5+LNlINeP88kqGuMiDp/lLzSj/AF1j+lNTh7xdhbtdEYrpHjZQ/kRVNqKr/lNtkf8ACIAPtmtyGixlYbD5Ji5QAu6CzRLl/wBNIVXILhNaZ55WaMCxWzc+r8OMfwkVQ31a+ok8ogHpvnIbBlinLzzTGsmaQbsJz0UrD8WklzLOIS2p2Vh0DiRWQbo+Ht5jEAfMAfapiV4kRMcjCmKxlnR5dIyzZFu0EyukMZyURAc71ryCfxW9Ve6KpKvEOLawKkrJM5WNTBfsyaDtmZNZc4hkCpp7ibP96925f0dNy4RZ2UrFSRkxVSbSTUUTKkDqYm4gbFGQU5turVzoqixvEiIfXia2TNZFnJhr0g7RKmBsb7d4R3DcNtwq9UA2pDg7ks0ViipVlmiisUIWaKKKEIrGKzRQhJPifakc7kWbiRZgsPIMkUwmOXoIj8Ih71S+KiJ457bs61TH8JPHJR+oO8CCqWvwx/LkB298GroW44dKajjtz9xTzJqflN/7+lLJcj6C7QxkWaSzNx3VUHKetFcv6fsK4mqe/Tzue+yx3smGBk0eLaDlV4XiByGGgFQxp96u1pIrTNnTPa3AMxnSnbMhVHTq7hg1B9RH7gGQ9KriMdarRYHDW02JFw7wc1dVVLV/pmHFWSHbzFwPuecR7KbSU5lC+FoD4QL8vl065AaTFqY2vAitxKqzSSAXKaUVaXEBOz7YZ25PW9Mozcel2YrZu0MoVyIdDJmDYc+/vnGaj+JTZ5IXTaM85YXazSVbK9pRjtSizI2nu6dGQIJhHf3AAyACAhTlJCkKAAV7IFKHQO0GqDvCRj7ThFJSWfSvJIYqZSJKajKHHoUOn/OPWu0BI6m47rO6MBtOdsFSXh3Llra09FRVxSKNuuVUnDWTRN21cihA8YgD5zFz9+lfKPkbil71LJRbi6kbfQOd29SfsypE0gXIIIk06ziI/v32ojiRGu5KOaSLO5Y5OR09lcuDeGrq2KbJfQcl7wZDcM7b1u3ZfkVCTZoZkE/LyxPOiyPq07Z0j88ewDj1xTzp57xwSs4e9nsl2vbV6vWLm9CxCBJM0j+KJkUFUHxSJiJSoglo8mPhzkQx9K6Mh3gSUW1eclZDnplV5axBKdPIeUQHoIVQrUvOFuOFk3zVzLoqRyQqOWqygc0oAUR23wOdI+v1xUPBcVYKclGMfHpXF2p2pygIqYhdIAXOrumH99cUM084J7HLmpY+GPfPmnLRStgeIcHMW7NTBDTTdtFkKZci5iaz5zjTpMIZEQx1DetJxxWhGtvM5hZpcfZna6iKRB5Ws3LxqN58YyOOvXNX4ExNYJvHiq8k36KWN2cSIa2TMm6gSjySdJkUKzQEDKJlN01b4yP5QyP23rYsbiLDXSu7bJi9YPGxRUUQeCUvcDzGAc+nrnAhRwZQ3PHZAnjLsct0xqKTzrjRElVXNHxU2+jmxvHfIphyyh6G3HoPz05qyyfEeAY2c2uQFl12TkwJoppk8Uym+U8CIABgwbOR9PXbNjBKKtvNQNREbp3JXyildA8WWEjNMY59EykWd9oM2VcJhoUA3l9ehvQdw+lND7VSSN0ZpwV2SNkFtWa+ayRFkxIoQpyj1AwZCiiluFjdXWiSGjCH1Ej2hT+4Il/tUgAAAYAKKKhrGs7opSbXqlxxpW5Vq6F7cVnWKinjAisKZ24h5VA0lMP39PXYRoorRB4jfVI1HhlKPh+9lG18xbOx30y8gznS7Sg8J4aBBN4hR+HYPiAC5HYPnpyjJW1+I8yrcjiejW7lVYyL2KPyzKFMfWXvjsYMdS+ggGaKK7Z+oMXkR+Vxq+Tn/KnYOMRb2TeE9GsbiIo7ZmaAeSEhhc8w4ZMAEIAm6+bcNx+eLn/D7bbZraJZJ3Hpkkll1BKssl4hSB3AxqDIfF9cjRRWHUvIjeP7LXp2AyN9P2lA/gpz+aZi1Y1usRs/kylE/KNo0lOfQYRx0ADavsFW7jNBCnK2dbMU1XOzaoFSASpmMHiHAmoRLtkdGR+uaKK1OmcZGel/mlnawcN3rXui7ir2Zxs/mSYj3biJObWkqkTUGnk8sMemoo+g423qch3T+/mV1LsLUYxqbhmsk2fily13Jz7FKJsBnIdTbgA4oopT6OmbNXaFD3TGCpzF5FLCBO0jIh9Ezil3tpBRUxRjWAlTSXKIAHeIYB32N6DkMYpgTcUhb/Cpk0XtWYkmDxc7lQjhyVNywPpACGHQQcZD3DbODZziiin6jvsH3EEpUA7Dz0URwwfSyPEBixtB9MP7b1BzyvUjFTSSx3shuBRD0MGnI7bhmumcUUVg+Iipq6LfoN4r6r//2Q=='
                                        }
                                       );
                                       // console.log(doc.content);
                                },
                                exportOptions: {
                                    modifier: {
                                        selected: true
                                    },
                                    columns: ':visible'
                                }
                            },
                            { extend: 'excel', "text" :"Excel","title" : "Producción" ,className: 'btn yellow btn-outline ',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            { extend: 'csv',"text" :"CSV", "title" : "Producción" , className: 'btn purple btn-outline ',
                                exportOptions: {
                                    columns: ':visible'
                                }
                            },
                            
                        ],
                select: true,
                "bSort": true,
                "processing": true,
                "serverSide": true,
                "dom": "<'row' <'col-md-12'B>><'row'><'row'<'col-md-6 col-sm-12'l>r><'table-scrollable't><'row'<'col-md-6 col-sm-12'i><'col-md-6 col-sm-12'p>>", // horizobtal scrollable datatable
                "fnDrawCallback": function( oSettings ) {
                        // alert("Cargo los datos");
                        var count = 0;
                        function details (event) {
                            event.preventDefault();
                            event.stopPropagation();
                            // console.log(grid.table.getSelectedRows());
                            // console.log($(this).parents('tr'));
                            // console.log(grid.table.getDataTable());
                            var btnid = $(this).prop("id");            
                            var data = grid.getDataTable().row( $(this).parents('tr') ).data();

                            /* if(data.length > 0){
                                if(btnid == "edit"){
                                    document.location.href = "/clientes?id=" + data[0];
                                }
                                else if(btnid == "sucu"){
                                    document.location.href = "/sucursalesList?idc=" + data[0];
                                }
                                else if(btnid == "equi"){
                                    document.location.href = "/equiposList?idc=" + data[0];
                                }
                                else if(btnid == "status"){
                                    if(confirm("¿Esta seguro de cambiar el registro?")){
                                         $.ajax({
                                            type: "POST",
                                            url: "controllers/index.php",
                                            data: "accion=clientes.ChangeStatus&idcli="+data[0],
                                            success: function(msg){
                                                alert("Registro cambiado" , "CLIENTES" , 'success' , function(){
                                                    $('#datatable_ajax tbody').off( 'click', 'button',  details);
                                                    $('#datatable_ajax').off('click', '.filter-submit');
                                                    $('#datatable_ajax').off('click', '.filter-cancel');
                                                    grid.getDataTable().clearPipeline().draw();
                                                    // console.log(grid.getDataTable().clearPipeline().draw());
                                                    // grid.getDataTable().fnDraw();
                                                });
                                            }
                                        });
                                    }
                                }
                             }*/
                        }
                        $('#datatable_ajax tbody > button').unbind('click', details)
                        $('#datatable_ajax_tools > li > a.tool-action').on('click', function(event) {
                            event.preventDefault();
                            var action = $(this).attr('data-action');
                            grid.getDataTable().button(action).trigger();
                        });

                        $('#datatable_ajax tbody').on( 'click', 'button',  details);
                        
                        $('#datatable_ajax').on('click', '.filter-submit', function(e) {
                            grid.getDataTable().clearPipeline().draw();
                        });

                        $('#datatable_ajax').on('click', '.filter-cancel', function(e) {
                             $('textarea.form-filter, select.form-filter, input.form-filter', $('#datatable_ajax')).each(function() {
                                $(this).val("");
                            });
                            $('input.form-filter[type="checkbox"]', $('#datatable_ajax')).each(function() {
                                $(this).attr("checked", false);
                            });
                            grid.getDataTable().clearPipeline().draw();
                        });
                 }  
                // "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l>><'table-scrollable't><'row'<'col-md-5 col-sm-12'i>>"
            }
        });

        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });

        grid.setAjaxParam("customActionType", "group_action");
        grid.getDataTable().ajax.reload();
        grid.clearAjaxParams();

        $('#datatable_ajax_tools > li > a.tool-action').on('click', function() {
            var action = $(this).attr('data-action');
            grid.getDataTable().button(action).trigger();
        });

       
    }

    return {

        //main function to initiate the module
        init: function () {

            initPickers();
            handleRecords();
        }

    };

}();

jQuery(document).ready(function() {
    TableDatatablesAjax.init();
});

app.controller('produccion', ['$scope','$http','$interval','client','$controller','Excel','$timeout' ,'$window', '$routeParams', function($scope,$http,$interval,client, $controller,Excel , $timeout,$window, $routeParams){


}]);

