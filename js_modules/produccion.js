


app.controller('produccion', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){
    $scope.id_company = 0;
    $scope.produccion = {
        params : {
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
            fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            finca : ""
        },
        step : 0,
        path : ['phrapi/produccion/index'],
        templatePath : [],
        nocache : function(){
            $scope.init();
        }
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().startOf('month'),
        endDate :moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            // TableDatatablesEditable.restart("sample_editable_1");
            $scope.produccion.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
            $scope.produccion.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
        }
        $scope.init()
    }

    $scope.tabla = {
        produccion : []
    }

    $scope.openDetallePalanca = function(data){
        data.expanded = !data.expanded;
    }

    $scope.openDetalleLote = function(data){
        data.expanded = !data.expanded;
    }

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];

    $scope.init = function(){
        var data = $scope.produccion.params;
        client.post($scope.produccion.path , $scope.printDetails ,data)
    }

    $scope.start = true;

    $scope.printDetails = function(r,b){
        b();
        if(r){
            $scope.tabla.produccion = r.data;
            $scope.id_company = r.id_company;
            $scope.colores = r.colores;
            $scope.colores = r.colores;
            $scope.palancas = r.palancas;
            $scope.totales = r.totales;
            if($scope.start){
                $scope.start = false;
                setInterval(function(){
                    $scope.init();
                } , (60000 * 5));
            }
        }
    }

}]);

