app.service('request', ['$http' , ($http) => {
    let service = {}

    service.grafica = (params) => {
        return $http.post('phrapi/comparacionAnual/grafica', params)
    }

    service.variables = (params) => {
        return $http.post('phrapi/comparacionAnual/variables', params)
    }

    return service
}])

app.controller('control', ['$scope', 'request', function($scope, $request){

    $scope.filters = {
        variable : 'RACIMOS CORTADOS'
    }
    
    const printGraficaEdad = (r) => {    
        let data = {
            series: r.chart.data,
            legend: r.chart.legend,
            umbral: r.chart.umbral,
            id: "grafica",
            zoom : true,
            type : 'line',
            min : 'dataMin',
            legendBottom : false,
            showLegends : true
        }
        let parent = $("#grafica").parent()
        parent.empty()
        parent.append(`<div id="grafica" class="chart"></div>`)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica'));
    }

    const printVariables = (r) => {
        $scope.variables = r.variables
    }

    $scope.init = () => {
        $request.variables($scope.filters)
            .then((r) => printVariables(r.data))

        $scope.changeGrafica()
    }

    $scope.changeGrafica = ( ) => {
        load.block('grafica')
        $request.grafica($scope.filters)
            .then((r) => {
                load.unblock('grafica')
                printGraficaEdad(r.data)
            })
    }

    $scope.init()

}]);

