app.service('request', ['client', '$http', function(client, $http){
    this.registros = function(callback, params){
        let url = 'phrapi/marun/cajas/regMarcas'
        let data = params || {}
        client.post(url, callback, data, 'tabla_base')
    }
    
    this.last = function(callback, params){
        let url = 'phrapi/marun/cajas/last'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.resumenCajas = function(callback, params){
        let url = 'phrapi/marun/cajas/resumen'
        let data = params || {}
        client.post(url, callback, data, 'div_table_2')
    }

    this.filters = function(callback, params){
        let url = 'phrapi/marun/cajas/filters'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.cajasSemanal = function(callback, params){
        let url = 'phrapi/marun/cajas/cajasSemanal'
        let data = params || {}
        client.post(url, callback, data, 'cajas_semanal')
    }

    this.eficienciaBalanza = function(callback, params){
        let url = 'phrapi/marun/cajas/eficienciaBalanza'
        let data = params || {}
        client.post(url, callback, data, 'eficiencia')
    }

    this.getMarcas = function(callback, params){
        let url = 'phrapi/marun/cajas/marcas'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.cuadrar = function(callback, params){
        let url = 'phrapi/marun/cajas/cuadrar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.guardarCuadrar = function(callback, params){
        let url = 'phrapi/marun/cajas/guardarCuadrar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.getGuiasDia = function(callback, params){
        let url = 'phrapi/marun/cajas/guias'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.procesar = function(callback, params){
        let url = 'phrapi/marun/cajas/procesar'
        let data = params || {}
        client.post(url, callback, data)
    }
    
    this.eliminar = function(callback, params){
        let url = 'phrapi/marun/cajas/eliminar'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.graficasBarras = function(callback, params){
        let url = 'phrapi/marun/cajas/graficasBarras'
        let data = params || {}
        client.post(url, callback, data, 'barras')
    }

    this.tablaDiferencias = function(callback, params){
        let url = 'phrapi/marun/cajas/diferencias'
        let data = params || {}
        client.post(url, callback, data, 'tablas')
    }

    this.historicoExcedente = function(callback, params){
        let url = 'phrapi/marun/cajas/excedente'
        let data = params || {}
        client.post(url, callback, data, 'collapse_3_3')
    }

    this.guardarCaja = function(callback, params){
        let url = 'phrapi/marun/cajas/guardarCaja'
        let data = params || {}
        client.post(url, callback, data)
    }

    this.editGuia = (callback, params) => {
        let url = 'phrapi/marun/cajas/editGuia'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        })
    }

    this.asignarGuia = (callback, params) => {
        let url = 'phrapi/marun/cajas/asignarGuia'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        })
    }

    this.borrarGuiaMarca = function(callback, params){
        let url = 'phrapi/marun/cajas/borrarGuiaMarca'
        let data = params || {}
        client.post(url, callback, data)
    }

    // MARCAS
    this.deleteMarca = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/deleteMarca'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.saveMarca = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/saveMarca'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.addAlias = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/addAlias'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.deleteAlias = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/deleteAlias'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.saveAlias = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/saveAlias'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.addRango = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/addRango'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.deleteRango = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/deleteRango'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    this.saveRango = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marcas/saveRango'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }
    // END MARCAS

    this.procesarMarca = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marun/cajas/marcaProcesarPeso'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }

    this.changeUnidad = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marun/cajas/changeUnidad'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }

    this.crearFincaProceso = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marun/cajas/crearFincaProceso'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }

    this.crearCajas = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marun/cajas/crearCajas'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }

    this.movimientos = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marun/cajas/movimientos'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }

    this.comentarios = function(params){
        return new Promise((resolve) => {
            let url = 'phrapi/marun/cajas/comentarios'
            let data = params || {}
            $http.post(url, data)
            .then((r) => {
                resolve(r.data)
            })
        })
    }

}])

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        if(isNaN(sum))
            return 0;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('notInArray', function(){
    return function(items, data){
        if(!data) return items;

        let keys2 = Object.keys(data)
        let filtered = {}

        angular.forEach(items, (row, i) => {
            if(!keys2.includes(i)){
                filtered[i] = row
            }
        })
        return filtered
    }
})

app.filter('notInArrayObject', function(){
    return function(items, key, data, except = null){
        if(!data) return items;
        let keys2 = Object.keys(data).map((key) => {
            if(except == null) return data[key]
            return key != except ? data[key] : null
        })
        let filtered = []
        
        angular.forEach(items, (row) => {
            if(!keys2.includes(row[key])){
                filtered.push(row)
            }
        })
        return filtered
    }
})

app.directive('escape', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 27) {
                scope.$apply(function (){
                    scope.$eval(attrs.escape);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('enter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.enter);
                });

                event.preventDefault();
            }
        });
    };
});

function getOptionsGraficaReact(id, options, title){
    var newOptions = {
        series: options.series,
        legend: options.legends,
        umbral: null,
        id: id,
        titulo : title || null,
        actions : false,
        showLegends : false,
        legendOrient : 'vertical'
    }
    return newOptions
}

function initGrafica(id, options, title){
    setTimeout(() => {
        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)

        let data = getOptionsGraficaReact(id, options, title)
        let chart = React.createElement(Historica, data)
        ReactDOM.render(chart, document.getElementById(id));
    }, 250)
}

function initPastel(id, series){
    let legends = []
    let newSeries = []
    Object.keys(series).map(key => {
        let label = series[key].label
        newSeries.push({
            label : series[key].label,
            value : parseFloat(series[key].value),
            color : (label.includes('>')) ? '#E43A45' : (label.includes('<')) ? '#C49F47' : '#26C281'
        })
        if(legends.indexOf(series[key]) != -1) legends.push(series[key]);
    })
    setTimeout(() => {
        let data = {
            data : newSeries,
            nameseries : "Pastel",
            legend : legends,
            titulo : "",
            id : id
        }

        let parent = $("#"+id).parent()
        parent.empty()
        parent.append(`<div id="${id}" class="chart"></div>`)
        ReactDOM.render(React.createElement(Pastel, data), document.getElementById(id));
        
    }, 250)
}

function in_array(value, array){
    if(!Array.isArray(array)) return false
    return array.indexOf(value) >= 0
}

function getParam(name){
    let url_string = window.location.href
    let url = new URL(url_string);
    return url.searchParams.get(name);
}

let datesEnabled = []

app.controller('produccion', ['$scope','request', '$filter', function($scope, $request, $filter){

    let datepickerHighlight = () => {
        let datepicker = $('#datepicker').datepicker({
            language : 'es',
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $('#datepicker').datepicker()
        .on('changeDate', function(e) {
            let anioCambio = false
            let olddate = moment($scope.table.fecha_inicial),
                newdate = moment(e.date)
            if(olddate.year() != newdate.year()){
                anioCambio = true
            }

            $scope.rangos = {}
            $scope.table.fecha_inicial = moment(e.date).format('YYYY-MM-DD')
            $scope.table.fecha_final = moment(e.date).format('YYYY-MM-DD')
            $scope.table.year = moment(e.date).year()
            $scope.temp = {
                fecha_inicial : $scope.table.fecha_inicial,
                fecha_final : $scope.table.fecha_inicial
            }

            $scope.reloadDashboard()
            if(anioCambio){
                $scope.reloadHistorico()
            }
        });
        $("#datepicker").datepicker('setDate', $scope.table.fecha_inicial)
        $('#datepicker').datepicker('update')
    }

    $scope.changeUnidad = () => {
        $request.changeUnidad().then((r) => {
            window.location.reload()
        })
    }

    $scope.marcasSeleccionadas = ['','','','','',''];
    $scope.crearFincaProcesoModal = {
        id_finca : '',
        marcasSeleccionadas : {},
        cantidadMarcasSeleccionadas : {}
    }
    $scope.crearCajasModal = {
        marcasSeleccionadas : {},
        cantidadMarcasSeleccionadas : {}
    }

    $scope.marcas = []
    $scope.fechaPasada = moment().format('YYYY-MM-DD')
    $scope.anioCambio = true
    $scope.filters = {
        var: 'exce'
    }
    $scope.registros = []
    $scope.table = {
        pagination : 10,
        startFrom : 0,
        actualPage : 1,
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD'),
        search : {},
        year : moment().year(),
        semana : moment().week(),
        unidad : 'lb',
        finca : 0,
        var : 'CONV'
    }

    $scope.charts = {
        cajasSemanal : new echartsPlv(),
        eficienciaBalanza : new echartsPlv()
    }

    $scope.changeFinca = async () => {
        await $scope.reloadDia()
    }

    $scope.reloadDia = async () => {
        await $scope.getResumen()
        await $scope.getFilters()
        await $scope.getBaseMarcas()
        await $scope.loadPasteles()
        await $scope.getTablaExcedente()
        await $scope.getCuadrar()
    }

    $scope.reloadHistorico = async () => {
        await $scope.loadSemanal()
        await $scope.loadExcedenteHistorico()
    }

    $scope.loadExcedenteHistorico = () => {
        $scope.umbralesBD = {}
        $request.historicoExcedente((r, b) => {
            b('collapse_3_3')
            r.data.map((marca) => {
                $scope.umbralesBD[marca.marca] = parseFloat(marca.requerimiento).toFixed(2)
            })
            $scope.dataExcedente = r
            $scope.umbralExcedente = r.umbrales
            initTableExcedente('table-historico-excedente', r)
        }, $scope.table)
    }

    const initTableExcedente = (id, r) => {
        const _data = angular.copy(r.data)
        let val = $scope.filters.var
        var props = {
            header : [{
                   key : 'marca',
                   name : 'MARCA',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                    key : 'sum_'+val,
                    name : 'SUM',
                    locked : true,
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    formatter : 'Number',
                    resizable : true,
                    customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['sum_'+val])
                        let umbral = parseFloat(rowData['avg_'+val])
                        let valueCell = valNumber > 0 ? $filter('number')(valNumber, 2) : ''
                        return `
                            <div class="text-center" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                     }
                 },{
                   key : 'avg_'+val,
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['avg_'+val])
                        let umbral = parseFloat(rowData['avg_'+val])
                        let valueCell = valNumber > 0 ? $filter('number')(valNumber, 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, umbral)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max_'+val,
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max_'+val])
                        let umbral = parseFloat(rowData['avg_'+val])
                        let valueCell = valNumber > 0 ? $filter('number')(valNumber, 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, umbral)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min_'+val,
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min_'+val])
                        let umbral = parseFloat(rowData['avg_'+val])
                        let valueCell = valNumber > 0 ? $filter('number')(valNumber, 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, umbral)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : _data.map((row) => {
                for(let i in r.semanas){
                    let sem = r.semanas[i]
                    if(!row[`sem_${val}_${sem}`]){
                        row[`sem_${val}_${sem}`] = ''
                    }
                }
                return row
            }),
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        Object.keys(r.semanas).map((key) => {
            let value = r.semanas[key]
            props.header.push({
                key : `sem_${val}_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+val+'_'+value])
                    let umbral = parseFloat(rowData['avg_'+val])
                    let valueCell = valNumber > 0 ? $filter('number')(valNumber, 2) : ''
                    if(rowData['marca'] != 'TOTAL'){
                        return `
                            <div class="text-center ${checkUmbral(valNumber, umbral)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }else{
                        return `
                            <div class="text-center" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }

    const checkUmbral = (value, umbral_tmp, umbral2_tmp) => {
        if(parseFloat(value)){
            if(!umbral2_tmp){
                let umbral = umbral_tmp || parseFloat($scope.umbralExcedente[$scope.filters.var])
                if(value > umbral) return 'bg-red-thunderbird bg-font-red-thunderbird'
                else return 'bg-green-jungle bg-font-green-jungle'
            }else{
                if(value >= umbral_tmp && value <= umbral2_tmp) return 'bg-green-jungle bg-font-green-jungle'
                else if (value > umbral_tmp) return 'bg-red-thunderbird bg-font-red-thunderbird'
                else return 'bg-yellow-gold bg-font-yellow-gold'
            }
        }
        return ''
    }

    $scope.reloadDashboard = function () {
    
        if(!$("#expand_cuadrar").hasClass('collapsed')){
            $("#expand_cuadrar").addClass('collapsed')
            $("#expand_cuadrar").attr('aria-expanded', 'false')

            $("#collapse_3_2").attr('aria-expanded', 'false')
            $("#collapse_3_2").removeClass('in')
            $("#collapse_3_2").css('height', '0px')
        }
        $scope.anioCambio = false
        if(moment($scope.fechaPasada).year() != moment($scope.table.fecha_inicial).year()) $scope.anioCambio = true;
        $scope.fechaPasada = $scope.table.fecha_inicial

        $scope.table.fecha_final = $scope.table.fecha_inicial;
        $scope.reloadDia()
        initSelectCrearFincaProceso()
    }

    $scope.StartEndDateDirectives = {
        startDate : moment(),
        endDate :moment(),
    }

    $scope.changeRangeDate = function(data = {}){
        if(!data.first_date || !data.second_date){
            data = {
                first_date : $scope.temp.fecha_inicial,
                second_date : $scope.temp.fecha_final
            }
        }else{
            $scope.temp = {
                fecha_inicial : data.first_date,
                fecha_final : data.second_date
            }
        }

        $request.resumenCajas(function(r, b){
            b('div_table_2')
            if(r){
                $scope.resumen = r.data
            }
        }, { 
            fecha_inicial : data.first_date, 
            fecha_final : data.second_date, 
            finca : $scope.table.finca
        })
    }

    $scope.getFilters = function(){
        return new Promise((resolve) => {
            let data = $scope.table
            $request.filters(function(r, b){
                b()
                if(r){
                    $scope.anios = r.years
                    $scope.semanas = r.semanas
                }
                resolve()
            }, data)
        })
    }

    let reRenderSelect = (id, options, value, setValue) => {
        let props = {
            value,
            placeholder : "Seleccione...",
            onChange : (e) => {
                setValue(value = (e != null ? e.value : ''))
                reRenderSelect(id, options, value, setValue)
            },
            options : options.map((c) => { return { label:c.nombre, value:c.id } })
        }
        window[`SelectMarca_${id}`] = React.createElement(Select, props)
        window[`SelectMarcaRendered_${id}`] = ReactDOM.render(window[`SelectMarca_${id}`], document.getElementById(id))
    }

    let initSelectCrearFincaProceso = () => {
        reRenderSelect(
            'crear-finca-proceso-marca_1', //id
            $scope.marcas, //opciones
            $scope.crearFincaProcesoModal.marcasSeleccionadas[0], // valor por default
            (value) => $scope.crearFincaProcesoModal.marcasSeleccionadas[0] = value // evento
        )
        reRenderSelect('crear-finca-proceso-marca_2', $scope.marcas, $scope.crearFincaProcesoModal.marcasSeleccionadas[1], (value) => $scope.crearFincaProcesoModal.marcasSeleccionadas[1] = value)
        reRenderSelect('crear-finca-proceso-marca_3', $scope.marcas, $scope.crearFincaProcesoModal.marcasSeleccionadas[2], (value) => $scope.crearFincaProcesoModal.marcasSeleccionadas[2] = value)
        reRenderSelect('crear-finca-proceso-marca_4', $scope.marcas, $scope.crearFincaProcesoModal.marcasSeleccionadas[3], (value) => $scope.crearFincaProcesoModal.marcasSeleccionadas[3] = value)
        reRenderSelect('crear-finca-proceso-marca_5', $scope.marcas, $scope.crearFincaProcesoModal.marcasSeleccionadas[4], (value) => $scope.crearFincaProcesoModal.marcasSeleccionadas[4] = value)
        reRenderSelect('crear-finca-proceso-marca_6', $scope.marcas, $scope.crearFincaProcesoModal.marcasSeleccionadas[5], (value) => $scope.crearFincaProcesoModal.marcasSeleccionadas[5] = value)
    }

    /*$scope.getRegistrosBase = function(){
        return new Promise((resolve) => {
            let data = $scope.table
            load.block('marcas')
            $request.registros(async function(r, b){
                b('tabla_base')
                load.unblock('marcas')
                if(r){
                    $scope.marcas = r.marcas
                    $scope.peso_prom_cajas = r.pesos_prom_cajas
                    initSelectCrearFincaProceso()
                }

                await $scope.loadPasteles()
                resolve()
            }, data)

            $request.tablaDiferencias(function(r, b){
                b('tablas')
                $scope.tablasDiferencias = r.tablas
            }, data)
        })
    }*/

    $scope.getBaseMarcas = () => {
        return new Promise((resolve) => {
            let data = $scope.table
            load.block('marcas')
            $request.registros(async function(r, b){
                b('tabla_base')
                load.unblock('marcas')
                if(r){
                    $scope.marcas = r.marcas
                    $scope.peso_prom_cajas = r.pesos_prom_cajas
                    initSelectCrearFincaProceso()
                }
                resolve()
            }, data)
        })
    }

    $scope.loadPasteles = () => {
        return new Promise((resolve) => {
            let data = $scope.table
            $request.graficasBarras(function(r, b){
                b('barras')
                if(r){
                    $scope.marcasBarras = r.marcas
                    $scope.graficasBarrar = r.graficas
                    $scope.rangos = r.rangos
                    Object.keys(r.graficas).map((key, index) => {
                        let min = null, max = null
                        if(r.graficas[key].series.Cantidad.umbral){
                            min = parseFloat(r.graficas[key].series.Cantidad.umbral.min)
                            max = parseFloat(r.graficas[key].series.Cantidad.umbral.max)
                        }
                        
                        r.graficas[key].series.Cantidad.itemStyle.normal.color = function(e){
                            // range umbral 
                            if(min && max){
                                var min_peso = 0, max_peso = 0
                                if(parseFloat(e.name)){
                                    min_peso = max_peso = parseFloat(e.name)
                                }else{
                                    var min_max = e.name.split(',')
                                    min_peso = parseFloat(min_max[0].replace('[', '').trim())
                                    max_peso = parseFloat(min_max[1].replace(']', '').trim())
                                }

                                if(min_peso >= min && max_peso <= max){
                                    return '#26C281'
                                }else if(max_peso <= min){
                                    return '#C49F47'
                                }else{
                                    return '#E43A45'
                                }
                            }

                            return '#E43A45'
                        }
                        initGrafica(`barras_${index}`, r.graficas[key], key)
                        initPastel(`pastel_${index}`, r.pasteles[key])
                    })
                }
                resolve()
            }, Object.assign({ grupos : $scope.rangos }, data))
        })
    }

    $scope.changeExcedente = () => {
        $scope.loadExcedenteHistorico()
    }

    $scope.getTablaExcedente = () => {
        return new Promise((resolve) => {
            let data = $scope.table
            $request.tablaDiferencias(function(r, b){
                b('tablas')
                $scope.tablasDiferencias = r.tablas
                resolve()
            }, data)
        })
    }

    $scope.crearFincaProcesoGuardar = () => {
        let data = angular.copy($scope.crearFincaProcesoModal)
        let marcas = {}

        if(!data.id_finca){
            alert('Favor de seleccionar finca')
            return;
        }
        for(let i in $scope.crearFincaProcesoModal.marcasSeleccionadas){
            let id_marca = $scope.crearFincaProcesoModal.marcasSeleccionadas[i]
            let cantidad = $scope.crearFincaProcesoModal.cantidadMarcasSeleccionadas[i]

            if(id_marca > 0 && !cantidad > 0){
                alert('Favor de corregir errores')
                return;
            }
            if(id_marca > 0 && cantidad > 0){
                marcas[id_marca] = cantidad
            }
        }

        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((res) => {
            $.blockUI({ message: `
                <div data-loader="circle-side" style="display: block; position: relative; top: 25px;">
                    <img src="iqicon.png" width="50" height="50" alt="IQ"/>
                </div>
                <h1>Estamos creando la información, Por favor espere...</h1>` 
            });
            if(res){
                $request.crearFincaProceso({
                    fecha : $scope.table.fecha_inicial,
                    id_finca : $scope.crearFincaProcesoModal.id_finca,
                    marcas
                })
                .then((r) => {
                    $.unblockUI();
                    if(r.status === 200){
                        $scope.crearFincaProcesoModal = {
                            id_finca : '',
                            cantidadMarcasSeleccionadas : {},
                            marcasSeleccionadas : {}
                        }
                        swal("Éxito!", "La información fue creada", "success");
                        $scope.reloadDashboard()
                    }else{
                        swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                    }
                })
            }
        })
    }

    $scope.crearCajasGuardar = () => {
        let data = angular.copy($scope.crearCajasModal)
        let marcas = {}

        for(let i in $scope.crearCajasModal.marcasSeleccionadas){
            let id_marca = $scope.crearCajasModal.marcasSeleccionadas[i]
            let cantidad = $scope.crearCajasModal.cantidadMarcasSeleccionadas[i]

            if(id_marca > 0 && !cantidad > 0){
                alert('Favor de corregir errores')
                return;
            }
            if(id_marca > 0 && cantidad > 0){
                marcas[id_marca] = cantidad
            }
        }

        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((res) => {
            if(res){
                $.blockUI({ message: `
                    <div data-loader="circle-side" style="display: block; position: relative; top: 25px;">
                        <img src="iqicon.png" width="50" height="50" alt="IQ"/>
                    </div>
                    <h1>Estamos creando la información, Por favor espere...</h1>` 
                });
                $request.crearCajas({
                    fecha : $scope.table.fecha_inicial,
                    id_finca : $scope.table.finca,
                    marcas
                })
                .then((r) => {
                    $.unblockUI();
                    if(r.status === 200){
                        $scope.crearCajasModal = {
                            cantidadMarcasSeleccionadas : {},
                            marcasSeleccionadas : {}
                        }
                        if(r.message){  
                            swal("Éxito!", r.message, "success");
                        }else{
                            swal("Éxito!", "La información fue creada", "success");
                        }
                        $scope.reloadDashboard()
                    }else{
                        swal("Contacte a soporte", r.message, "error");
                    }
                })
            }
        })
    }

    $scope.eliminar = () => {
        var elements = $(".delete")
        if(elements.length > 0){
            if(confirm(`Seguro deseas eliminar ${elements.length} elementos?`)){
                var ids = []
                var index = []
                for(var x = 0; x < elements.length; x++){
                    ids.push({
                        id : $(elements[x]).attr('data-id'),
                        marca : $(elements[x]).attr('data-marca')
                    })
                }
                $request.eliminar((r, b) => {
                    b()
                    if(r){
                        $scope.reloadDia()
                        alert(`Se borraron ${elements.length} registros con éxito`, "", "success")
                    }
                }, { ids : ids })
            }
        }else{
            alert("No has seleccionado ninguna fila")
        }
    }

    $scope.editarGuia = (guia) => {
        setTimeout(() => {
            $scope.modalCuadrar()
            $scope.guia = {
                id : guia.id,
                guia : guia.guia,
                fecha : guia.fecha,
                sello_seguridad : guia.sello_seguridad,
                magap : guia.codigo_magap,
                productor : guia.codigo_productor,
                numero_contenedor : guia.numero_contenedor,
                finca : guia.id_finca,
                marca : {}
            }
            $scope.marcasSeleccionadas = ['', '', '', '', '', '']
            guia.detalle.map((value, index) => {
                $scope.marcasSeleccionadas[index] = value.id_marca
                $scope.guia.marca[value.id_marca] = Math.round(value.valor)
            })
            $scope.$apply()
        }, 100)
    }

    $scope.modalCuadrar = function(){
        $scope.guia = {
            marca : {}
        }
        $("#cuadrar").modal('toggle')
    }

    $scope.guardarCuadrar = function(){
        var data = angular.copy($scope.guia)
        Object.keys(data.marca).map((marca) => {
            if(!in_array(marca, $scope.marcasSeleccionadas)) delete data.marca[marca]
        })

        if(!data.fecha){
            alert('Favor de colocar la fecha')
            return;
        }
        if(!data.guia){
            alert('Favor de colocar el numero de remision')
            return;
        }
        if(!data.finca){
            alert('Favor de colocar la finca')
            return;
        }

        $request.guardarCuadrar(function(r, b){
            b()
            if(r.status == 200){
                if(r.message){
                    alert(r.message, "", "success")
                }else{
                    alert("Guardado con éxito", "", "success")
                }

                let data = $scope.table
                $request.cuadrar(function(r, b){
                    b()
                    if(r){
                        $scope.guia.procesable = true
                        $scope.guia = {}

                        setTimeout(() => {
                            $scope.$apply(() => {
                                console.log("apply")
                                $scope.cuadrarCajas = r.data
                                $scope.guias = r.guias
                                $scope.saldos = r.saldos
                            })
                        }, 500)
                    }
                }, data)
            }else if(r.message){
                alert(r.message)
            }else{
                alert('Ha pasado algo inesperado, contacte a soporte')
            }
        }, data)
    }

    $scope.giasDia = (fecha, id_guia) => {
        $scope.guia.procesable = false
        if(fecha && id_guia){
            $request.getGuiasDia((r, b) => {
                b()
                if(r){
                    $scope.guia.procesable = true
                    $scope.guia.marca = {}
                    r.marcas.forEach(value => {
                        $scope.guia.marca[value.marca] = parseInt(value.valor)
                    })
                }
            }, { fecha: fecha, id: id_guia })
        }
    }

    $scope.getCuadrar = function(){
        return new Promise((resolve) => {
            setTimeout(async () => {
                let showing = $("#collapse_3_2").attr('aria-expanded') == 'true'
                if(showing){
                    let data = $scope.table
                    $request.cuadrar(function(r, b){
                        b()
                        if(r){
                            $scope.cuadrarCajas = r.data
                            $scope.guias = r.guias
                            $scope.saldos = r.saldos
                        }
                        resolve()
                    }, data)

                    let res_movimientos = await $request.movimientos(data)
                    $scope.saldos_movientos = res_movimientos.data
                    $scope.comentarios = res_movimientos.comentarios
                }else{
                    resolve()
                }
            }, 150)
        })
    }

    $scope.guardarComentarios = async () => {
        let data = angular.copy($scope.table)
        data.comentarios = angular.copy($scope.comentarios)
        let r = $request.comentarios(data)
    }

    $scope.getResumen = ( ) => {
        return new Promise((resolve) => {
            let data = $scope.table
            $request.resumenCajas(function(r, b){
                b('div_table_2')
                if(r){
                    $scope.resumen = r.data 
                    $scope.convertidas_default = r.convertidas
                    $scope.tags = r.tags
                    $scope.fincas = r.fincas

                    if(!r.fincas.hasOwnProperty($scope.table.finca)){
                        $scope.table.finca = Object.keys(r.fincas)[0]
                    }
                }

                resolve()
            }, data)
        })
    }

    $scope.loadSemanal = () => {
        return new Promise((resolve) => {
            let data = $scope.table
            $request.cajasSemanal(function(r, b){
                b('cajas_semanal')
                if(r){
                    let parent = $("#cajas_semanal").parent()
                    parent.empty()
                    parent.append(`<div id="cajas_semanal" style="height: 400px;"></div>`)

                    $scope.charts.cajasSemanal.init('cajas_semanal', r.chart)
                }
                resolve()
            }, data)
        })
    }

    $scope.loadEficienciaBalanza = () => {
        return new Promise((resolve) => {
            setTimeout(() => {
                let panel = $("#eficiencia").attr('aria-expanded')
                if(panel == 'true'){
                    let data = $scope.table
                    $request.eficienciaBalanza(function(r, b){
                        b('eficiencia')
                        if(r){
                            $scope.charts.eficienciaBalanza.init('grafica-eficiencia', r.chart)
                        }
                        resolve()
                    }, data)
                }else{
                    resolve()
                }
            }, 200)
        })
    }

    $scope.edit = (row, field) => {
        $scope.originalGuia = angular.copy(row)
        row.editing = field
    }

    $scope.cancel = (row) => {
        row.editing = null
        Object.assign(row, $scope.originalGuia)
    }

    $scope.save = (row) => {
        $request.editGuia((r) => {
            if(r.status === 200){
                row.editing = false
                $scope.getCuadrar()
            }else{
                alert(`Ocurrio algo inesperado`)
            }
        }, {
            fecha: $scope.table.fecha_inicial,
            marca : row.marca,
            pendiente : row.pendiente,
            asignada : row.asignada,
            id_finca : $scope.table.finca
        })
    }

    $scope.init = function(newDate, fecha){
        return new Promise((resolve) => {
            $request.last(function(r, b){
                b()
                if(r){
                    $scope.available_fincas = r.available_fincas
                    $scope.produccion_fincas = r.produccion_fincas
                    $scope.enabled = r.enabled

                    if(Object.keys(r.fincas).length > 0)
                    if(Object.keys(r.fincas).indexOf($scope.table.finca) == -1){
                        $scope.table.finca = Object.keys(r.fincas)[0]
                        $scope.anioCambio = true
                    }

                    if(r.days){
                        datesEnabled = r.days
                    }
    
                    if(newDate){
                        $scope.table.fecha_inicial = fecha || r.last.fecha
                        $scope.table.fecha_final = fecha || r.last.fecha
                        $scope.table.year = moment(r.last.fecha).year()
                        $("#date-picker").html($scope.table.fecha_inicial + ' - '+ $scope.table.fecha_final)
                        $scope.anioCambio = true
                    }

                    $scope.table.unidad = r.unidad
                }
                resolve()
            }, { newDate : newDate, filters: $scope.table })
        })
    }

    /* CONFIGURACION RANGOS */
    $scope.showRango = function(){
        $scope.config.rangos.getMarcas(function(r, b){
            b()
            if(r){
                $scope.config.rangos.marcas = r.data
            }
            $("#rangos").modal('toggle')
        })
    }

    $scope.config = {
        rangos : {
            marcas : [],
            getMarcas : function(callback){
                $request.getMarcas(callback,  $scope.params)
            },
            newConfig : {
                marca : 0,
                min : 0,
                max : 0
            }
        }
    }

    $scope.procesarDia = () => {
        $request.procesar((r, b) => {
            b()
            alert("Procesado Completo", "", "success")
            $scope.reloadDia()
        }, { fecha : $scope.table.fecha_inicial, id_finca : $scope.table.finca } )
    }

    $scope.guia = {}
    $scope.isValidGuia = () => {
        if($scope.table.fecha_inicial)
            return true;
        return false;
    }

    $scope.getClassUmbral = (marca) => {
        if(marca.class != ''){
            return marca.class
        } else {
            let peso_prom = parseFloat($scope.peso_prom_cajas[marca.marca])
            var classBg = ''
            if(marca.peso < peso_prom){
                classBg = 'bg-yellow-gold bg-font-yellow-gold'
            }else if(marca.peso > peso_prom){
                classBg = 'bg-red-thunderbird bg-font-red-thunderbird'
            }else{
                classBg = 'bg-green-haze bg-font-green-haze'
            }
            marca.class = classBg
            return classBg
        } 
    }

    $scope.exportExcel = function(id_table){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, 'Cajas.xls')
    }

    $scope.exportPrint = function(id_table){
        
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;

        if(id_table == 'table_1'){
            var cut = contentTable.search('<tr role="row" class="filter">')
            var cut2 = contentTable.search('</thead>')
            var part1 = contentTable.substring(0, cut)
            var part2 = contentTable.substring(cut2, contentTable.length)
            contentTable = part1 +part2
        }

        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
    }

    $scope.not_selected_marca = (index) => {
        var array = []
        $scope.marcasSeleccionadas.map((value, i) => {
            if(i != index) array.push(value)
        })
        return in_array($scope.marcasSeleccionadas[index], array)
    }

    $scope.modalAsignar = (row) => {
        $("#asignar").modal('show')
        $scope.rowAsignar = angular.copy(row)
    }

    $scope.asignarGuia = () => {
        $request.asignarGuia((r) => {
            if(r.status === 200){
                alert('Asignadas','','success')
                $scope.getCuadrar()
                $("#asignar").modal('hide')
            }else{
                alert(r.message)
            }
        }, Object.assign({ id_finca_origen : $scope.rowAsignar.id_finca, fecha : $scope.table.fecha_inicial }, $scope.asignar_form))
    }

    $scope.borrarGuiaMarca = (guia, marca) => {
        if(confirm('¿Estas seguro de eliminar esta marca?')){
            let data = {
                id : guia.id,
                id_marca : marca.id_marca,
            }
            $request.borrarGuiaMarca((r,b) => {
                b()
                $scope.getCuadrar()
            }, data)
        }
    }

    /* MARCAS */
    $scope.saveMarca = () => {
        $request.saveMarca($scope.marcaSelected)
        .then((r) => {
            if(r.status === 200){
                toastr.success('Guardado')
                $scope.reloadDia()
                $("#marca-modal").modal('hide')
            }else if(r.message){
                swal("Error", r.message, "error");
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.editarMarca = (marca) => {
        $("#marca-modal").modal('show')
        $scope.marcaSelected = angular.copy(marca)
    }

    $scope.asignarAliasF = async (marca) => {
        if(marca.nombre == 'N/A'){
            let res = await swal({
                text : '¿Ir a base de datos? Para editar nombre',
                buttons : ['No', 'Si']
            })
            if(res){
                window.location.href = 'produccioncajasbase?fecha=' + $scope.table.fecha_inicial
            }
        }else{
            $("#asignar-alias-modal").modal('show')
            $scope.asignarAlias = angular.copy(marca)
            let _marcas = angular.copy($scope.marcas)
            let _keys = $scope.asignarAlias.nombre.split(' ')
            _keys = _keys.filter(r => r !== '').map(r => r.toUpperCase())
            
            $scope.sugerenciasAlias = _marcas.filter(r => {
                let _k = r.nombre.split(' ')
                for(let i in _k){
                    if(typeof _k[i] !== 'function'){
                        let key = (_k[i] || "").toUpperCase()
                        if(_keys.indexOf(key) !== -1)
                            return true
                    }
                }
                return false
            })
        }
    }

    $scope.agregarAlias = async (marca) => {
        let res = await swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        if(res){
            $request.addAlias({
                id : marca.id,
                alias : $scope.asignarAlias.nombre
            })
            .then((r) => {
                if(r.status === 200){
                    $("#asignar-alias-modal").modal('hide')
                    toastr.success('Guardado')
                    $scope.reloadDia()
                    $scope.$apply()
                }else{
                    swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                }
            })
        }
    }

    $scope.deleteMarca = (marca, $index) => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((res) => {
            if(res){
                $request.deleteMarca(marca)
                .then((r) => {
                    if(r.status === 200){
                        toastr.success('Borrado')
                        $scope.marcas.splice([$index], 1)
                        $scope.reloadDia()
                        $scope.$apply()
                    }else{
                        swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                    }
                })
            }
        })
    }

    $scope.addAlias = () => {
        $request.addAlias($scope.marcaSelected)
        .then((r) => {
            if(r.status === 200){
                $scope.marcaSelected.alias.push({
                    id : r.id
                })
                $scope.$apply()
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }
    
    $scope.addRango = () => {
        $request.addRango($scope.marcaSelected)
        .then((r) => {
            if(r.status === 200){
                $scope.marcaSelected.rangos.push({
                    id : r.id
                })
                $scope.$apply()
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.saveRango = (rango) => {
        $request.saveRango(rango)
        .then((r) => {
            if(r.status === 200){
                toastr.success('Guardado')
                $scope.reloadDia()
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.deleteRango = (rango, $index) => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((res) => {
            if(res){
                $request.deleteRango(rango)
                .then((r) => {
                    if(r.status === 200){
                        toastr.success('Borrado')
                        $scope.marcaSelected.rangos.splice([$index], 1)
                        $scope.reloadDia()
                        $scope.$apply()
                    }else{
                        swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                    }
                })
            }
        })
    }

    $scope.saveAlias = (alias) => {
        $request.saveAlias(alias)
        .then((r) => {
            if(r.status === 200){
                toastr.success('Guardado')
            }else{
                swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
            }
        })
    }

    $scope.deleteAlias = (alias, $index) => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((res) => {
            if(res){
                $request.deleteAlias(alias)
                .then((r) => {
                    if(r.status === 200){
                        toastr.success('Borrado')
                        $scope.marcaSelected.alias.splice([$index], 1)
                        $scope.$apply()
                    }else{
                        swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                    }
                })
            }
        })
    }

    $scope.deleteProp = (data, prop) => {
        if(!data[prop]){
            delete data[prop]
        }
    }

    /* END MARCAS */

    $scope.sum = (...numbers) => {
        let sum = 0
        numbers.map((n) => {
            if(parseFloat(n)){
                sum += parseFloat(n)
            }
        })
        return sum
    }

    $scope.procesarMarca = (id_marca) => {
        swal({
            text : '¿Estas seguro de hacer esta acción?',
            buttons : ['No', 'Si']
        })
        .then((res) => {
            if(res){
                $request.procesarMarca({
                    fecha : $scope.table.fecha_inicial,
                    id_marca
                })
                .then((r) => {
                    if(r.status === 200){
                        toastr.success('Procesado')
                        $scope.getResumen()
                    }else{
                        swal("Contacte a soporte", "Ha ocurrido algo inesperado", "error");
                    }
                })
            }
        })
    }

    /* BEGIN OCULTAR FILAS RESUMEN POR MARCA */
    $scope.hiddenRows = false
    $scope.getHiddenRows = () => {
        if($scope.resumen){
            $scope.hiddenRows = $scope.resumen.countIf(obj => obj.hidden) > 0
        }else{
            $scope.hiddenRows = false
        }
    }
    $scope.$watch('resumen', $scope.getHiddenRows)
    $.contextMenu({
        selector: '.context-menu-one', 
        items: {
            "hide": {
                name: "Ocultar", 
                icon: "fa-eye",
                callback: function(itemKey, opt, rootMenu, originalEvent) {
                    $(opt.$trigger).removeClass('selected')

                    let index = $(opt.$trigger).attr('key')
                    setTimeout(() => {
                        $scope.$apply(() => {
                            $scope.resumen[index].hidden = true
                            $scope.getHiddenRows()
                        })
                    }, 100)
                }
            }
        }
    });

    $('.context-menu-one').on('click', function(e){
        $(this).addClass('selected')
    }) 
    /* END OCULTAR FILAS RESUMEN POR MARCA */

    /* BEGIN CONTEXT MENU CALENDARIO */
    $.contextMenu({
        selector: 'td.day.disabled', 
        items: {
            "new_day": {
                name: "Dar de Alta Día", 
                icon: "fa-unlock-alt",
                callback: function(itemKey, opt, rootMenu, originalEvent) {
                    let menu = $(opt.$trigger[0])
                    let date = menu.data('date')
                    $("#datepicker").datepicker('setDate', date)
                }
            }
        }
    });
    /* END CONTEXT MENU CALENDARIO */

    /* BEGIN CREAR FINCA PROCESO */
    $scope.clearCrearFinca = () => {
        $scope.crearFincaProcesoModal = {
            id_finca : '',
            marcasSeleccionadas : {},
            cantidadMarcasSeleccionadas : {}
        }
    }
    $scope.crearFincaGuardar = () => {
        console.log($scope.crearFincaProcesoModal)
    }
    /* END CREAR FINCA PROCESO */

    let fecha = getParam('fecha')
    $scope.init(true, fecha)
    .then(() => {
        datepickerHighlight()
        $scope.reloadHistorico()
    })

    // target element
    let getDateCalendar = (target) => {
        if (target.hasClass('day') && !target.hasClass('disabled')){
            day = parseInt(target.text(), 10)||1;
            year = this.viewDate.getUTCFullYear();
            month = this.viewDate.getUTCMonth();
            if (target.hasClass('old')){
                if (month === 0){
                    month = 11;
                    year -= 1;
                }
                else {
                    month -= 1;
                }
            }
            else if (target.hasClass('new')){
                if (month === 11){
                    month = 0;
                    year += 1;
                }
                else {
                    month += 1;
                }
            }
            console.log(year, month, day)
            //return UTCDate(year, month, day);
        }
    }

}]);