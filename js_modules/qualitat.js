/*----------  UTILIDADES SOBRE ARRAYS  ----------*/
Array.prototype.amax = function() {
    return {
        label : this.indexOf(Math.max.apply(null, this)),
        value : Math.round(Math.max.apply(null, this))
    }
};

Array.prototype.amin = function() {
    return {
        label : this.indexOf(Math.min.apply(null, this)),
        value : Math.round(Math.min.apply(null, this))
    }
};

Array.prototype.sum = function (prop) {
    var property = prop || -1;
    var total = 0
    if(property == prop){
        for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total += this[i][prop]
        }
    }else{
        for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total += this[i]
        }
    }
    return total
}

Array.prototype.avg = function (prop) {
    var property = prop || -1;
    var total = 0 , avg = 0;
    if(property == prop){
        for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total += this[i][prop]
        }
    }else{
        for ( var i = 0, _len = this.length; i < _len; i++ ) {
            total += this[i]
        }
    }

    avg = (total / this.length);

    return Math.round(avg)
}

Array.prototype.getUnique = function(){
   var u = {}, a = [];
   for(var i = 0, l = this.length; i < l; ++i){
      if(u.hasOwnProperty(this[i])) {
         continue;
      }
      a.push(this[i]);
      u[this[i]] = 1;
   }
   return a;
}
/*----------  UTILIDADES SOBRE ARRAYS  ----------*/
app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
      //alert(field);
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});


/*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/
var globalEcharts;
var appEcharts = {
    require : require,
    options : [],
    options_principal : [],
    grafica_principal_calidad: [],
    grafica_principal_de: [],
    grafica_principal_peso: [],
    grafica_principal_cluster: [],
    grafica_principal_peso_cluster: [],
    grafica_calidad_historico_marcas: [],
    grafica_danos_total: [],
    grafica_danos_seleccion: [],
    grafica_danos_seleccion_campos: [],
    grafica_danos_empaque: [],
    grafica_danos_empaque_campos: [],
    grafica_danos_otros: [],
    grafica_danos_otros_campos: [],
    por_cluster: [],
    type : "seleccion",
    init : function(callback){
        callback = callback || this.loadModules;
        this.require.config({
            paths: {
                echarts: '/assets/global/plugins/echarts/'
            }
        });

        this.require(
            [
                'echarts',
                'echarts/chart/bar',
                'echarts/chart/chord',
                'echarts/chart/eventRiver',
                'echarts/chart/force',
                'echarts/chart/funnel',
                'echarts/chart/gauge',
                'echarts/chart/heatmap',
                'echarts/chart/k',
                'echarts/chart/line',
                'echarts/chart/map',
                'echarts/chart/pie',
                'echarts/chart/radar',
                'echarts/chart/scatter',
                'echarts/chart/tree',
                'echarts/chart/treemap',
                'echarts/chart/venn',
                'echarts/chart/wordCloud'
            ],
            callback
        );
    },
    getOptionsPorCluster : function(mode){
        var category = [];
        var series = [];
        var serie_cluster = [];
        var legend = [];
        var SeriesPrincipal = this.por_cluster;
        category = this.por_cluster.categories;
        var Type = [];
        if(mode == "general"){
            Type.push("Promedio")
            Type.push("Max")
            Type.push("Min")
        }else{
            Type.push("Desviacion Estandar");
        }
        for(var n in Type){
            if(SeriesPrincipal.hasOwnProperty(Type[n])){
                series.push({ name: Type[n], type:'line', data: [] });
                legend.push(Type[n]);
                for(var x in SeriesPrincipal[Type[n]]){
                    series[n].data.push(SeriesPrincipal[Type[n]][x]);
                }
            }
        }

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: false},
                    dataZoom : {show: false},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },
    getOptionsPrincipalCalidad : function(){
        ////////console.log(this.grafica_principal_calidad);
        var category = [];
        var series = [];
        var serie_calidad = [];
        var serie_calidad_maxima = [];
        var serie_calidad_minima = [];
        var legend = [];

        for(var n in this.grafica_principal_calidad){
            category.push(n);
            serie_calidad.push(this.grafica_principal_calidad[n].calidad);
            serie_calidad_maxima.push(this.grafica_principal_calidad[n].calidad_maxima);
            serie_calidad_minima.push(this.grafica_principal_calidad[n].calidad_minima);
        }

        series.push({ name:'Quality', type:'line', data:serie_calidad });
        series.push({ name:'Quality Max', type:'line', data:serie_calidad_maxima });
        series.push({ name:'Quality Min', type:'line', data:serie_calidad_minima });

        legend.push('Quality');
        legend.push('Quality Max');
        legend.push('Quality Min');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value',
                    min : 50,
                    max : 95
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsPrincipalDE : function(){
        ////////console.log(this.grafica_principal_de);
        var category = [];
        var series = [];
        var serie_de = [];
        var legend = [];

        for(var n in this.grafica_principal_de){
            category.push(n);
            serie_de.push(this.grafica_principal_de[n].desviacion_estandar);
        }

        series.push({ name:'Desviación Estándar', type:'line', data:serie_de });

        legend.push('Desviación Estándar');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsPrincipalPeso : function(){
        ////////console.log(this.grafica_principal_peso);
        var category = [];
        var series = [];
        var serie_peso = [];
        var legend = [];

        for(var n in this.grafica_principal_peso){
            category.push(n);
            serie_peso.push(this.grafica_principal_peso[n].peso);
        }

        series.push({ name:'Weigth', type:'line', data:serie_peso });

        legend.push('Weigth');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsPrincipalCluster : function(){
        //////console.log(this.grafica_principal_cluster);
        var category = [];
        var series = [];
        var serie_cluster = [];
        var legend = [];

        for(var n in this.grafica_principal_cluster){
            category.push(n);
            serie_cluster.push(this.grafica_principal_cluster[n].cluster);
        }

        series.push({ name:'Clúster', type:'line', data:serie_cluster });

        legend.push('Clúster');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value',
                    min: 50,
                    max: 95
                }
            ],
            series : series
        };

        return option;
    },
	getOptionsPrincipalPesoCluster : function(){
        //console.log(this.grafica_principal_peso_cluster);
        var category = [];
        var series = [];
        var serie_cluster = [];
        var legend = [];

        for(var n in this.grafica_principal_peso_cluster){
            category.push(n);
            serie_cluster.push(this.grafica_principal_peso_cluster[n].peso_cluster);
        }

        series.push({ name:'Peso clúster', type:'line', data:serie_cluster });

        legend.push('Peso Clúster');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value',
                    min: 50,
                    max: 95
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsCalidadMarcas : function(){
        var category = [];
        var series = [];
        var serie_rewe = [];
        var serie_pinalinda = [];
        var serie_palmar_aldi = [];
        var legend = [];

        for(var n in this.grafica_calidad_historico_marcas){
            category.push(n);
            serie_rewe.push(this.grafica_calidad_historico_marcas[n].rewe);
            serie_pinalinda.push(this.grafica_calidad_historico_marcas[n].pinalinda);
            serie_palmar_aldi.push(this.grafica_calidad_historico_marcas[n].palmar_aldi);
        }

        series.push({ name:'REWE', type:'line', data:serie_rewe });
        series.push({ name:'PINALINDA', type:'line', data:serie_pinalinda });
        series.push({ name:'PALMAR/ALDI', type:'line', data:serie_palmar_aldi });

        legend.push('REWE');
        legend.push('PINALINDA');
        legend.push('PALMAR/ALDI');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsDanosTotal : function(){
        ////////console.log(this.grafica_danos_total);
        var category = [];
        var series = [];
        var serie_seleccion = [];
        var serie_empaque = [];
        var serie_otros = [];
        var legend = [];

        for(var n in this.grafica_danos_total){
            category.push(n);
            serie_seleccion.push(this.grafica_danos_total[n].seleccion);
            serie_empaque.push(this.grafica_danos_total[n].empaque);
            serie_otros.push(this.grafica_danos_total[n].otros);
        }

        series.push({ name:'Selección', type:'line', data:serie_seleccion });
        series.push({ name:'Empaque', type:'line', data:serie_empaque });
        series.push({ name:'Otros', type:'line', data:serie_otros });

        legend.push('Selección');
        legend.push('Empaque');
        legend.push('Otros');

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsDanosSeleccion : function(){
        ////////console.log(this.grafi);
        var category = [];
        var series = [];
        var legend = [];

        // indicadores
		
        for(var n in this.grafica_danos_seleccion_campos){
            if(!isNaN(n)){
                legend.push(this.grafica_danos_seleccion_campos[n]);
                series.push({ name: this.grafica_danos_seleccion_campos[n], type:'line', data:[] });
            }
        }
		////console.log(this.grafica_danos_seleccion);
        for(var n in this.grafica_danos_seleccion){
            category.push(n); // barra de abajo

            for(var j in series){ // ciclo a todos los campos
                if(!isNaN(j)){
                    var ele = series[j];
                    var existe = false;

                    for(var m in this.grafica_danos_seleccion[n]){
                        if(m == ele.name){ // si el campo coincide se agrega el valor
                            ele.data.push( this.grafica_danos_seleccion[n][m] );
							//////console.log(this.grafica_danos_seleccion[n][m]);
                            existe = true;
                            break;
                        }
                    }

                    if(existe == false) // si no se encontró el campo se agrega un cero
                        ele.data.push( 0 );
                }
            }
        }

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };
		////console.log(option);
        return option;
    },

    getOptionsDanosEmpaque : function(){
        ////////console.log(this.grafi);
        var category = [];
        var series = [];
        var legend = [];

        // indicadores
        for(var n in this.grafica_danos_empaque_campos){
            if(!isNaN(n)){
                legend.push(this.grafica_danos_empaque_campos[n]);
                series.push({ name: this.grafica_danos_empaque_campos[n], type:'line', data:[] });
            }
        }

        for(var n in this.grafica_danos_empaque){
            category.push(n); // barra de abajo

            for(var j in series){ // ciclo a todos los campos
                if(!isNaN(j)){
                    var ele = series[j];
                    var existe = false;

                    for(var m in this.grafica_danos_empaque[n]){
                        if(m == ele.name){ // si el campo coincide se agrega el valor
                            ele.data.push( this.grafica_danos_empaque[n][m] );
							
                            existe = true;
                            break;
                        }
                    }

                    if(existe == false) // si no se encontró el campo se agrega un cero
                        ele.data.push( 0 );
                }
            }
        }

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsDanosOtros : function(){
        ////////console.log(this.grafi);
        var category = [];
        var series = [];
        var legend = [];

        // indicadores
        for(var n in this.grafica_danos_otros_campos){
            if(!isNaN(n)){
                legend.push(this.grafica_danos_otros_campos[n]);
                series.push({ name: this.grafica_danos_otros_campos[n], type:'line', data:[] });
            }
        }

        for(var n in this.grafica_danos_otros){
            category.push(n); // barra de abajo

            for(var j in series){ // ciclo a todos los campos
                if(!isNaN(j)){
                    var ele = series[j];
                    var existe = false;

                    for(var m in this.grafica_danos_otros[n]){
                        if(m == ele.name){ // si el campo coincide se agrega el valor
                            ele.data.push( this.grafica_danos_otros[n][m] );
                            existe = true;
                            break;
                        }
                    }

                    if(existe == false) // si no se encontró el campo se agrega un cero
                        ele.data.push( 0 );
                }
            }
        }

        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : series
        };

        return option;
    },

    getOptionsPrincipal : function(){
        var category = [] , serie = [];
        ////////console.log(this.options_principal);
        this.options_principal.amap(function(item){
            category.push(item.fecha)
            serie.push(parseFloat(item.calidad))
            return item.name;
        });
        ////////console.log(category)
        ////////console.log(serie)
        var option = {
            tooltip : {
                trigger: 'axis'
            },
            legend: {
                data:['REWE']
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataZoom : {show: true},
                    dataView : {show: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            dataZoom : {
                show : true,
                realtime : true,
                start : 20,
                end : 80
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data : category
                }
            ],
            yAxis : [
                {
                    type : 'value',
                    min: 50,
                    max: 95
                }
            ],
            series : [
                {
                    name:'REWE',
                    type:'line',
                    data:serie
                }
            ]
        };

        return option;
    },
    getOptionType : function(){
        ////////console.log('SUPERMAN');
        console.log(this.type);
        var legend = [];
        this.options[this.type].amap(function(item){
            legend.push(item.name)
            return item.name;
        });
		//console.log(options[this.type]);
        ////////console.log(legend)
        var options = {
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient : 'vertical',
                x : 'left',
                data:legend
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataView : {show: false, readOnly: false},
                    magicType : {
                        show: true, 
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                x: '25%',
                                width: '50%',
                                funnelAlign: 'center',
                                max: 1548
                            }
                        }
                    },
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            series : [
                {
                    name:'PROMEDIO GENERAL',
                    type:'pie',
                    radius : ['0%'],
                    itemStyle : {
                        normal : {
                            label : {
                                show : false
                            },
                            labelLine : {
                                show : false
                            }
                        },
                        emphasis : {
                            label : {
                                show : true,
                                position : 'outer',
                                textStyle : {
                                    fontSize : '12',
                                    fontWeight : 'bold'
                                }
                            }
                        }
                    },
                    data:this.options[this.type]
                }
            ]
        };
		//console.log(options);
        return options;
    },
    generateOptions : function(){
        var serie = this.options.series;
        var options = {
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient : 'vertical',
                x : 'left',
                data:['SELECCION' , 'EMPAQUE' , 'OTROS']
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    dataView : {show: false, readOnly: false},
                    magicType : {
                        show: true, 
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                x: '25%',
                                width: '50%',
                                funnelAlign: 'center',
                                max: 1548
                            }
                        }
                    },
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            series : [
                {
                    name:'PROMEDIO GENERAL',
                    type:'pie',
                    radius : ['0%'],
                    itemStyle : {
                        normal : {
                            label : {
                                show : false
                            },
                            labelLine : {
                                show : false
                            }
                        },
                        emphasis : {
                            label : {
                                show : true,
                                position : 'outer',
                                textStyle : {
                                    fontSize : '12',
                                    fontWeight : 'bold'
                                }
                            }
                        }
                    },
                    data:serie
                }
            ]
        };
        return options;
    },
    loadModules : function(echarts){
        globalEcharts = echarts;
        var general = [];
        var general_echarts = [];
        var option = appEcharts.generateOptions();
        var option2 = appEcharts.getOptionType()
        var option_principal = appEcharts.getOptionsPrincipal();
        var grafica_principal_calidad = appEcharts.getOptionsPrincipalCalidad();
        var grafica_principal_de = appEcharts.getOptionsPrincipalDE();
        var grafica_principal_peso = appEcharts.getOptionsPrincipalPeso();
        var grafica_principal_cluster = appEcharts.getOptionsPrincipalCluster();
        var grafica_principal_peso_cluster = appEcharts.getOptionsPrincipalPesoCluster();

        var grafica_calidad_historico_marcas = appEcharts.getOptionsCalidadMarcas();

        var grafica_danos_total = appEcharts.getOptionsDanosTotal();
        var grafica_danos_seleccion = appEcharts.getOptionsDanosSeleccion();
        var grafica_danos_empaque = appEcharts.getOptionsDanosEmpaque();
        var grafica_danos_otros = appEcharts.getOptionsDanosOtros();
        var grafica_por_cluster = appEcharts.getOptionsPorCluster("general");
        ////console.log(grafica_danos_seleccion);
        // general_echarts.push(option);
        // general_echarts.push(option);
        // general_echarts.push(option);
        
        // daños
        general_echarts = echarts.init(document.getElementById('highstock_1') , 'infographic');
        general_echarts.setOption(option_principal);

        // calidad
        general_echarts = echarts.init(document.getElementById('highstock_calidad') , 'infographic');
        general_echarts.setOption(grafica_principal_calidad);

        // calidad historico
        general_echarts = echarts.init(document.getElementById('highstock_calidad_historico') , 'infographic');
        general_echarts.setOption(grafica_calidad_historico_marcas);

        // daños total
        general_echarts = echarts.init(document.getElementById('highstock_danos_total') , 'infographic');
        general_echarts.setOption(grafica_danos_total);

        // general_echarts = echarts.init(document.getElementById('highstock_danos_seleccion') , 'infographic');
        // general_echarts.setOption(grafica_danos_seleccion);
        // general_echarts = echarts.init(document.getElementById('highstock_danos_empaque') , 'infographic');
        // general_echarts.setOption(grafica_danos_empaque);
        // general_echarts = echarts.init(document.getElementById('highstock_danos_otros') , 'infographic');
        // general_echarts.setOption(grafica_danos_otros);

        if(document.getElementById('peso_cluster')){
            var general_echarts_porcluster = echarts.init(document.getElementById('peso_cluster') , 'infographic');
            general_echarts_porcluster.setOption(grafica_por_cluster);
        }

        
        console.log(option)
  //       general.push(echarts.init(document.getElementById('echarts_generales') , 'infographic'));
  //       general[0].setOption(option);


  //       general.push(echarts.init(document.getElementById('echarts_generales_detalles') , 'infographic'));
		// //console.log(option2);
  //       general[1].setOption(option2);


  //       general[0].setTheme('infographic');
  //       general[1].setTheme('infographic');
        general_echarts.setTheme('infographic');
        window.onresize = function(){
            general[0].resize();
            general[1].resize();
            general_echarts.resize();
            general_echarts_porcluster.resize();
        }
    }
}
/*----------  OBJETO PARA GRAFICAS ECHARTS  ----------*/

function getQueryParams() {
    var params = {};

    if (location.search) {
        var parts = location.search.substring(1).split('&');

        for (var i = 0; i < parts.length; i++) {
            var nv = parts[i].split('=');
            if (!nv[0]) continue;
            params[nv[0]] = nv[1] || true;
        }
    }

    // Now you can get the parameters you want like so:
    //////console.log(params);
    return params;
}

function loadScript(options){
  // HIGHSTOCK DEMOS
  // COMPARE MULTIPLE SERIES
    // var seriesOptions = [],
   //      seriesCounter = 0,
   //      names = ['REWE'],
   //      // create the chart when all data is loaded
   //      createChart = function () {
   //       //////console.log(seriesOptions);
   //          $('#highstock_1').highcharts('StockChart', {
   //              chart : {
   //                  style: {
   //                      fontFamily: 'Open Sans'
   //                  }
   //              },

   //              rangeSelector: {
   //                  selected: 1
   //              },
   //              xAxis:{
   //               // type: 'datetime',
   //               ordinal: false
   //              },
   //              // yAxis: {
   //              //     labels: {
   //              //         formatter: function () {
   //              //             return (this.value > 0 ? ' + ' : '') + this.value + '%';
   //              //         }
   //              //     },
   //              //     plotLines: [{
   //              //         value: 0,
   //              //         width: 2,
   //              //         color: 'silver'
   //              //     }]
   //              // },

   //              // plotOptions: {
   //              //     series: {
   //              //         compare: 'percent'
   //              //     }
   //              // },

   //              tooltip: {
   //                  pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>'
   //              },

   //              series: seriesOptions
   //          });
   //      };
    
   //  $.each(names, function (i, name) {

   //      $.getJSON('phrapi/calidad/index',    function (data) {

   //       var xdata = [];

   //       for(var x in data.data){
   //           xdata.push([
   //               new Date(data.data[x].fecha).getTime(),
   //               parseFloat(data.data[x].calidad)
   //           ])
   //       }

   //          seriesOptions[i] = {
   //              name: name,
   //              data: xdata,
            //  tooltip: {
   //                  valueDecimals: 2
   //              },
   //              connectNulls: true
   //          };

   //          // As we're loading the data asynchronously, we don't know what order it will arrive. So
   //          // we keep a counter and create the chart when all the data is loaded.
   //          seriesCounter += 1;

   //          if (seriesCounter === names.length) {
   //              createChart();
   //          }
   //      });
   //  });
    // $.each(names, function (i, name) {

    //     $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=' + name.toLowerCase() + '-c.json&callback=?',    function (data) {

    //         seriesOptions[i] = {
    //             name: name,
    //             data: data
    //         };

    //         // As we're loading the data asynchronously, we don't know what order it will arrive. So
    //         // we keep a counter and create the chart when all the data is loaded.
    //         seriesCounter += 1;

    //         if (seriesCounter === names.length) {
    //             createChart();
    //         }
    //     });
    // });


    appEcharts.options = options.danhos;
    ////////console.log(options.main)
    appEcharts.options_principal = options.main;
    ////////console.log(appEcharts.options_principal)

    appEcharts.grafica_principal_calidad = options.grafica_principal_calidad;
    appEcharts.grafica_principal_de = options.grafica_principal_de;
    appEcharts.grafica_principal_peso = options.grafica_principal_peso;
    appEcharts.grafica_principal_cluster = options.grafica_principal_cluster;
    appEcharts.grafica_principal_peso_cluster = options.grafica_principal_peso_cluster;

    appEcharts.grafica_calidad_historico_marcas = options.grafica_calidad_historico_marcas;

    appEcharts.grafica_danos_total = options.grafica_danos_total;
    appEcharts.grafica_danos_seleccion = options.grafica_danos_seleccion;
	////console.log(appEcharts.grafica_danos_seleccion);
    appEcharts.grafica_danos_seleccion_campos = options.grafica_danos_seleccion_campos;
    appEcharts.grafica_danos_empaque = options.grafica_danos_empaque;
    appEcharts.grafica_danos_empaque_campos = options.grafica_danos_empaque_campos;
    appEcharts.grafica_danos_otros = options.grafica_danos_otros;
    appEcharts.grafica_danos_otros_campos = options.grafica_danos_otros_campos;
    appEcharts.por_cluster = options.por_cluster;

    appEcharts.init();
}

function viewGraphPrincipal(id, a){
    var id_div;
    $("#highstock_calidad").addClass("hide");
    $("#highstock_de").addClass("hide");
    $("#highstock_peso").addClass("hide");
    $("#highstock_peso_cluster").addClass("hide");
    $("#highstock_cluster").addClass("hide");
    $("#highstock_marcas").addClass("hide");

    if(1 == id){
        id_div = 'highstock_calidad';
        appEcharts.grafica_principal_calidad = a;
        var options = appEcharts.getOptionsPrincipalCalidad();
    }
    if(2 == id){
        id_div = 'highstock_de';
        appEcharts.grafica_principal_de = a;
        var options = appEcharts.getOptionsPrincipalDE();
    }
    if(3 == id){
        id_div = 'highstock_peso';
        appEcharts.grafica_principal_peso = a;
        var options = appEcharts.getOptionsPrincipalPeso();
    }
    if(4 == id){
        id_div = 'highstock_cluster';
        appEcharts.grafica_principal_cluster = a;
        var options = appEcharts.getOptionsPrincipalCluster();
    }
	if(5 == id){
        id_div = 'highstock_peso_cluster';
        appEcharts.grafica_principal_peso_cluster = a;
        var options = appEcharts.getOptionsPrincipalPesoCluster();
    }


    $("#" + id_div).removeClass("hide");
    general_echarts = globalEcharts.init(document.getElementById(id_div) , 'infographic');
    general_echarts.setOption(options);
}

function viewGraphDanos(id, a){
    var id_div;
    $("#highstock_danos_total").addClass("hide");
    $("#highstock_danos_seleccion").addClass("hide");
    $("#highstock_danos_empaque").addClass("hide");
    $("#highstock_danos_otros").addClass("hide");

    if(1 == id){
        id_div = 'highstock_danos_total';
        appEcharts.grafica_danos_total = a;
        var options = appEcharts.getOptionsDanosTotal();
    }
    if(2 == id){
        id_div = 'highstock_danos_seleccion';
        appEcharts.grafica_danos_seleccion = a;
        var options = appEcharts.getOptionsDanosSeleccion();
    }
    if(3 == id){
        id_div = 'highstock_danos_empaque';
        appEcharts.grafica_danos_empaque = a;
        var options = appEcharts.getOptionsDanosEmpaque();
    }
    if(4 == id){
        id_div = 'highstock_danos_otros';
        appEcharts.grafica_danos_otros = a;
        var options = appEcharts.getOptionsDanosOtros();
    }


    $("#" + id_div).removeClass("hide");
    general_echarts = globalEcharts.init(document.getElementById(id_div) , 'infographic');
    general_echarts.setOption(options);
}

function viewGraphCalidadHistorico(id, a){
    var id_div;
    $("#highstock_calidad_historico").addClass("hide");

    if(1 == id){
        id_div = 'highstock_calidad_historico';
        appEcharts.grafica_danos_total = a;
        var options = appEcharts.getOptionsDanosTotal();
    }

    $("#" + id_div).removeClass("hide");
    general_echarts = globalEcharts.init(document.getElementById(id_div) , 'infographic');
    general_echarts.setOption(options);
}

app.factory('Excel',function($window){
    var uri='data:application/vnd.ms-excel;base64,',
        template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
        format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
    return {
        tableToExcel:function(tableId,worksheetName){
            var table=$(tableId),
                ctx={worksheet:worksheetName,table:table.html()},
                href=uri+base64(format(template,ctx));
            return href;
        }
    };
});

app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
        //alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.controller('informe_calidad', ['$scope','$http','$interval','client','$controller','Excel','$timeout' ,'$window', function($scope,$http,$interval,client, $controller,Excel , $timeout,$window){
    

    var cliente_marca = getQueryParams();
    var param_cliente = "";
    var param_marca   = "";


    if(cliente_marca.c && cliente_marca.m){
        param_cliente = decodeURI(cliente_marca.c);
        param_marca   = decodeURI(cliente_marca.m);
    }

    $scope.por_cluster = function(){
        var response = false;
        if($scope.checkParams()){
            if(param_marca == 'PINALINDA (1Kg)' || param_marca == 'PINALINDA 3 DEDOS'){
                response = true;
            }
        }
        return response;
    }
    $scope.vassel = "";
    $scope.week = "";
    $scope.leyendaGeneralTitle = 'Quality';
    $scope.data_grafica_calidad = [];
    $scope.data_grafica_de      = [];
    $scope.data_grafica_peso    = [];
    $scope.data_grafica_cluster = [];
    $scope.porCluster = [];
    $scope.porClusterText = "Standard deviation";
    $scope.porClusterFlag = false;
    $scope.table_porCluster = {
        label : [],
        data : []
    };

    $scope.data_grafica_calidad_historico_marcas  = [];

    $scope.data_grafica_danos_total = [];
    $scope.data_grafica_danos_seleccion = [];
    $scope.data_grafica_danos_empaque = [];
    $scope.data_grafica_danos_otros = [];

    $scope.param_cliente = param_cliente;
    $scope.param_marca   = param_marca;
    $scope.param_peso    = "";
    $scope.param_cluster = "";
    $scope.param_logo = "";

    $scope.checkParams = function(){
        if(param_cliente && param_marca){
            //////console.log('>' + param_marca + '<')
            if(param_cliente == "" && param_marca == "")
                return false;
            else
                return true;
        }
        return false;
    }

    $scope.id_company = 0;
    $scope.tags = {
        calidad : {
            value : 0,
            label : ""
        },
        calidad_maxima : {
            value : 0,
            label : ""
        },
        calidad_minima : {
            value : 0,
            label : ""
        },
        cluster : {
            value : 0,
            label : ""
        },
        desviacion_estandar : {
            value : 0,
            label : ""
        },
        peso : {
            value : 0,
            label : ""
        },
        calidad_dedos : {
            value : 0,
            label : ""
        },
        calidad_cluster : {
            value : 0,
            label : ""
        },
        dedos_promedio : {
            value : 0,
            label : ""
        }
    };

    $scope.umbrales = {};
    $scope.calidad = {
        params : {
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            // fecha_inicial : moment().startOf('month'),
            fecha_inicial : moment().startOf('year').format('YYYY-MM-DD'),
            // fecha_inicial : moment().subtract(6, 'days').format('YYYY-MM-DD'),
            fecha_final :  moment().endOf("year").format('YYYY-MM-DD'),
            // fecha_final :  moment().format('YYYY-MM-DD'),
            // fecha_final :  moment().endOf('month'),
            cliente: param_cliente,
            marca: param_marca
        },
        step : 0,
        path : ['phrapi/qualitat/index' , 'phrapi/calidad/labores' , 'phrapi/calidad/causas'],
        templatePath : [],
        nocache : function(){
            this.templatePath.push('/views/templetes/calidad/schritt1.html?' +Math.random());
            ////////console.log(this.templatePath);
            // this.templatePath.push('/views/step2.html?' +Math.random());
            // this.templatePath.push('/views/step1.html?' +Math.random());
        }

    }

    $scope.tab = function(tab){
        appEcharts.type = tab;
        appEcharts.init();
    }

    $scope.StartEndDateDirectives = {
        startDate :moment().subtract(5, 'month'),
        endDate :moment().subtract(4, 'month'),
    }

	$scope.changeRangeDate = function(data){
		if(data){
			$scope.calidad.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
			$scope.calidad.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
			$scope.loadExternal();
		}
	}
    $scope.loadExternal = function(){
        ////////console.log($scope.calidad.path[$scope.calidad.step]);
        if($scope.calidad.path[$scope.calidad.step] != ""){
            var data = $scope.calidad.params;
            client.post($scope.calidad.path[$scope.calidad.step] , $scope.startDetails , data);
        }
    }

    $scope.por_clusterGrap = function(){
        appEcharts.por_cluster = $scope.porCluster;
        $scope.porClusterFlag = !$scope.porClusterFlag;
        var mode = 'general';
        if($scope.porClusterFlag) {
            mode = 'desviacion';  
            $scope.porClusterText = "General";
        }else{
            mode = 'general';
            $scope.porClusterText = "Standard deviation";
        }
        var grafica_por_cluster = appEcharts.getOptionsPorCluster(mode);
        
        appEcharts.init(function(echarts){
            var general_echarts_porcluster = echarts.init(document.getElementById('peso_cluster') , 'infographic');
            general_echarts_porcluster.setOption(grafica_por_cluster);
            window.onresize = function(){
                general_echarts_porcluster.resize();
            }
        })
    }

    $scope.startDetails = function(r , b){
        b();
        if(r){
            var options = {
                main : r.data || [],
                danhos : r.danhos || [],
                grafica_principal_calidad: r.grafica_principal_calidad || [],
                grafica_principal_de: r.grafica_principal_de || [],
                grafica_principal_peso: r.grafica_principal_peso || [],
                grafica_principal_cluster: r.grafica_principal_cluster || [],
                grafica_principal_peso_cluster: r.grafica_principal_peso_cluster || [],

                grafica_calidad_historico_marcas: r.grafica_calidad_historico_marcas || [],
                
                grafica_danos_total: r.grafica_danos_total || [],
                grafica_danos_seleccion: r.grafica_danos_seleccion || [],
                grafica_danos_seleccion_campos: r.grafica_danos_seleccion_campos || [],
                grafica_danos_empaque: r.grafica_danos_empaque || [],
                grafica_danos_empaque_campos: r.grafica_danos_empaque_campos || [],
                grafica_danos_otros: r.grafica_danos_otros || [],
                grafica_danos_otros_campos: r.grafica_danos_otros_campos || [],
                por_cluster: r.por_cluster || [],
            };
            $scope.porCluster = r.por_cluster || []

            $scope.table_porCluster = [];
            $scope.table_porCluster.data = (r.hasOwnProperty("table_por_cluster") && r.table_por_cluster.hasOwnProperty("table")) ? r.table_por_cluster.table : [];
            $scope.table_porCluster.label = (r.hasOwnProperty("table_por_cluster") && r.table_por_cluster.hasOwnProperty("labels")) ? r.table_por_cluster.labels : [];
            $scope.table_porCluster.totals = (r.hasOwnProperty("table_por_cluster") && r.table_por_cluster.hasOwnProperty("totales")) ? r.table_por_cluster.totales : [];
			$scope.tabla_principal = [];
			$scope.total_principal = [];
			$scope.total_principal.peso = 0;
			$scope.total_principal.num_cluster = 0;
			$scope.total_principal.num_dedos = 0;
			$scope.total_principal.danhos_por_dedo = 0;
			$scope.total_principal.danhos_por_cluster = 0;
			$scope.total_principal.calidad_dedos = 0;
			$scope.total_principal.calidad_cluster = 0;

			$scope.total_principal.ROTS = [];
			$scope.total_principal.SCARS = [];
            $scope.total_principal.LATEX = [];
            $scope.total_principal.LENGTH = [];
            $scope.total_principal.MAJOR = [];
            $scope.total_principal.PACKING = [];
			$scope.total_principal.OTHER = [];

            $scope.total_principal.ROTS.cm = 0;
            $scope.total_principal.ROTS.cr = 0;
            $scope.total_principal.ROTS.ni = 0;
            $scope.total_principal.ROTS.nr = 0;
            $scope.total_principal.ROTS.pr = 0;
            // <!-- ROTS/MOLDS -->
            // <!-- SCARS/BRUISES -->
            $scope.total_principal.SCARS.sr = 0;
            $scope.total_principal.SCARS.lsr = 0;
            $scope.total_principal.SCARS.psr = 0;
            $scope.total_principal.SCARS.csr = 0;
            $scope.total_principal.SCARS.br = 0;
            // <!-- SCARS/BRUISES -->
            // <!-- LATEX -->
            $scope.total_principal.LATEX.ls = 0;
            $scope.total_principal.LATEX.lg = 0;
            // <!-- LATEX -->
            // <!-- LENGTH/GRAD -->
            $scope.total_principal.LENGTH.ts = 0;
            $scope.total_principal.LENGTH.tl = 0;
            $scope.total_principal.LENGTH.ug = 0;
            $scope.total_principal.LENGTH.og = 0;
            // <!-- LENGTH/GRAD -->
            // <!-- OTHER MAJOR DEFECTS -->
            $scope.total_principal.MAJOR.rt = 0;
            $scope.total_principal.MAJOR.ns = 0;
            $scope.total_principal.MAJOR.bf = 0;
            $scope.total_principal.MAJOR.is = 0;
            $scope.total_principal.MAJOR.id = 0;
            $scope.total_principal.MAJOR.idt = 0;
            $scope.total_principal.MAJOR.ct = 0;
            $scope.total_principal.MAJOR.dc = 0;
            $scope.total_principal.MAJOR.fr = 0;
            $scope.total_principal.MAJOR.rsk = 0;
            // <!-- OTHER MAJOR DEFECTS -->
            // <!-- PACKING DEFECTS -->
            $scope.total_principal.PACKING.nl = 0;
            $scope.total_principal.PACKING.hp = 0;
            $scope.total_principal.PACKING.jp = 0;
            // <!-- PACKING DEFECTS -->
            // <!-- OTHER OBSERVATIONS -->
            $scope.total_principal.OTHER.mc = 0;
            $scope.total_principal.OTHER.pcc = 0;
            $scope.total_principal.OTHER.pt = 0;
            $scope.total_principal.OTHER.upd = 0;
            $scope.total_principal.OTHER.re = 0;
            $scope.total_principal.OTHER.sm = 0;
            $scope.total_principal.OTHER.sk = 0;
            $scope.total_principal.OTHER.asc = 0;
			if($scope.checkParams){
				//console.log(r.tabla_principal.length);
				for(x=0;x<r.tabla_principal.length; x++){
					//console.log(r.tabla_principal[x].peso);
					var todo = [];
                    var ROTS = [];
                    var SCARS = [];
                    var LATEX = [];
                    var LENGTH = [];
                    var MAJOR = [];
                    var PACKING = [];
                    var OTHER = [];

					todo['peso']=r.tabla_principal[x].peso;
					$scope.total_principal.peso = parseFloat($scope.total_principal.peso) + parseFloat(todo['peso']) ;
					todo['num_cluster']=r.tabla_principal[x].cluster;
					$scope.total_principal.num_cluster = parseFloat($scope.total_principal.num_cluster) + parseFloat(todo['num_cluster']) ;
					todo['num_dedos']=r.tabla_principal[x].dedos;
					$scope.total_principal.num_dedos = parseFloat($scope.total_principal.num_dedos) + parseFloat(todo['num_dedos']) ;
					todo['danhos_por_dedo']=r.tabla_principal[x].danhos_por_dedo;
					$scope.total_principal.danhos_por_dedo= parseFloat($scope.total_principal.danhos_por_dedo) + parseFloat(todo['danhos_por_dedo']) ;
					todo['danhos_por_cluster']=r.tabla_principal[x].danhos_por_cluster;
					$scope.total_principal.danhos_por_cluster = parseFloat($scope.total_principal.danhos_por_cluster) + parseFloat(todo['danhos_por_cluster']) ;
					todo['calidad_dedos']=r.tabla_principal[x].calidad;
					$scope.total_principal.calidad_dedos = parseFloat($scope.total_principal.calidad_dedos) + parseFloat(todo['calidad_dedos']) ;
					todo['calidad_cluster']=r.tabla_principal[x].calidad_cluster;
					$scope.total_principal.calidad_cluster = parseFloat($scope.total_principal.calidad_cluster) + parseFloat(todo['calidad_cluster']) ;
					
					for(i=0;i<r.tabla_principal[x].ROTS.length;i++){
                    	var campo = r.tabla_principal[x].ROTS[i].campo.split(" ");
                    	ROTS[campo[0]] = r.tabla_principal[x].ROTS[i].cantidad;
                    }
                    for(i=0;i<r.tabla_principal[x].SCARS.length;i++){
                    	var campo = r.tabla_principal[x].SCARS[i].campo.split(" ");
                    	SCARS[campo[0]] = r.tabla_principal[x].SCARS[i].cantidad;
                    }
                    for(i=0;i<r.tabla_principal[x].LATEX.length;i++){
                    	var campo = r.tabla_principal[x].LATEX[i].campo.split(" ");
                    	LATEX[campo[0]] = r.tabla_principal[x].LATEX[i].cantidad;
                    }
                    for(i=0;i<r.tabla_principal[x].LENGTH.length;i++){
                        var campo = r.tabla_principal[x].LENGTH[i].campo.split(" ");
                        LENGTH[campo[0]] = r.tabla_principal[x].LENGTH[i].cantidad;
                    }
                    for(i=0;i<r.tabla_principal[x].MAJOR.length;i++){
                        var campo = r.tabla_principal[x].MAJOR[i].campo.split(" ");
                        MAJOR[campo[0]] = r.tabla_principal[x].MAJOR[i].cantidad;
                    }
                    for(i=0;i<r.tabla_principal[x].PACKING.length;i++){
                        var campo = r.tabla_principal[x].PACKING[i].campo.split(" ");
                        PACKING[campo[0]] = r.tabla_principal[x].PACKING[i].cantidad;
                    }
                    for(i=0;i<r.tabla_principal[x].OTHER.length;i++){
                        var campo = r.tabla_principal[x].OTHER[i].campo.split(" ");
                        OTHER[campo[0]] = r.tabla_principal[x].OTHER[i].cantidad;
                    }

                    console.log(ROTS)
                    console.log(SCARS)
                    console.log(LATEX)
                    console.log(LENGTH)
                    console.log(MAJOR)
                    console.log(PACKING)
                    console.log(OTHER)
						
                    for(var i in ROTS){
    					if(ROTS.hasOwnProperty(i) && parseFloat(ROTS[i])>0)
    					   $scope.total_principal.ROTS[i] = parseFloat($scope.total_principal.ROTS[i]) + parseFloat(ROTS[i]);
                    }
                	for(var i in SCARS){
                        if(SCARS.hasOwnProperty(i) && parseFloat(SCARS[i])>0)
                           $scope.total_principal.SCARS[i] = parseFloat($scope.total_principal.SCARS[i]) + parseFloat(SCARS[i]);
                    }
                    for(var i in LATEX){
                        if(LATEX.hasOwnProperty(i) && parseFloat(LATEX[i])>0)
                           $scope.total_principal.LATEX[i] = parseFloat($scope.total_principal.LATEX[i]) + parseFloat(LATEX[i]);
                    }
                    for(var i in LENGTH){
                        if(LENGTH.hasOwnProperty(i) && parseFloat(LENGTH[i])>0)
                           $scope.total_principal.LENGTH[i] = parseFloat($scope.total_principal.LENGTH[i]) + parseFloat(LENGTH[i]);
                    }
                    for(var i in MAJOR){
                        if(MAJOR.hasOwnProperty(i) && parseFloat(MAJOR[i])>0)
                           $scope.total_principal.MAJOR[i] = parseFloat($scope.total_principal.MAJOR[i]) + parseFloat(MAJOR[i]);
                    }
                    for(var i in PACKING){
                        if(PACKING.hasOwnProperty(i) && parseFloat(PACKING[i])>0)
                           $scope.total_principal.PACKING[i] = parseFloat($scope.total_principal.PACKING[i]) + parseFloat(PACKING[i]);
                    }
                    for(var i in OTHER){
                        if(OTHER.hasOwnProperty(i) && parseFloat(OTHER[i])>0)
                           $scope.total_principal.OTHER[i] = parseFloat($scope.total_principal.OTHER[i]) + parseFloat(OTHER[i]);
                    }

                    todo['ROTS'] = ROTS;
                    todo['SCARS'] = SCARS;
                    todo['LATEX'] = LATEX;
                    todo['LENGTH'] = LENGTH;
                    todo['MAJOR'] = MAJOR;
                    todo['PACKING'] = PACKING;
					todo['OTHER'] = OTHER;

					$scope.tabla_principal.push(todo);
				}
				//console.log($scope.tabla_principal);
				//$scope.tabla_principal = r.tabla_principal;

                $scope.total_principal.peso = (($scope.total_principal.peso / x));
                $scope.total_principal.num_cluster = Math.round(($scope.total_principal.num_cluster / x));
                $scope.total_principal.num_dedos = Math.round(($scope.total_principal.num_dedos / x));
                $scope.total_principal.danhos_por_cluster = Math.round(($scope.total_principal.danhos_por_cluster / x));
                $scope.total_principal.calidad_dedos = Math.round(($scope.total_principal.calidad_dedos / x));
                $scope.total_principal.calidad_cluster = Math.round(($scope.total_principal.calidad_cluster / x));
			}
			else{
				$scope.tabla_principal = [];
			}
			$scope.total_principal.peso = $scope.total_principal.peso.toFixed(2);
            // data grafica
            $scope.data_grafica_calidad = r.grafica_principal_calidad   || [];
            $scope.data_grafica_de      = r.grafica_principal_de        || [];
            $scope.data_grafica_peso    = r.grafica_principal_peso      || [];
            $scope.data_grafica_cluster = r.grafica_principal_cluster   || [];
            $scope.grafica_principal_peso_cluster = r.grafica_principal_peso_cluster   || [];

            // data calidad historico
            $scope.data_grafica_calidad_historico_marcas  = r.grafica_calidad_historico_marcas      || [];

            // data daños
            $scope.data_grafica_danos_total = r.grafica_danos_total     || [];
            $scope.data_grafica_danos_seleccion = r.grafica_danos_seleccion     || [];
            $scope.data_grafica_danos_empaque = r.grafica_danos_empaque     || [];
            $scope.data_grafica_danos_otros = r.grafica_danos_otros     || [];

            // tags
            $scope.umbrales = r.umbrals || {};
            $scope.tags.calidad.value = Math.round(r.tags.calidad);
            $scope.tags.calidad_maxima.value = Math.round(r.tags.calidad_maxima);
            $scope.tags.calidad_minima.value = Math.round(r.tags.calidad_minima);
            $scope.tags.cluster.value = Math.round(r.tags.cluster);
            $scope.tags.desviacion_estandar.value = parseFloat(r.tags.desviacion_estandar).toFixed(2);
            $scope.tags.peso.value = parseFloat(r.tags.peso).toFixed(2);
			$scope.tags.calidad_dedos.value = parseFloat(r.tags.calidad_dedos).toFixed(2);
			$scope.tags.calidad_cluster.value = parseFloat(r.tags.calidad_cluster).toFixed(2);
			$scope.tags.dedos_promedio.value = Math.round(r.tags.dedos_promedio);

            // tablas
            $scope.tabla_cliente_marca_calidad = r.tabla_principal_calidad;
            $scope.tabla_cliente_marca_danos = r.tabla_principal_danos;


            /*----------  BARCO  ----------*/
            if(r.hasOwnProperty("vessel")){
                $scope.vassel = r.vessel;
            }
            if(r.hasOwnProperty("week")){
                $scope.week = r.week;
            }
            /*----------  BARCO  ----------*/
            
            // header
            if(r.data_header){
                $scope.param_peso    = r.data_header.peso;
                $scope.param_cluster = r.data_header.cluster;
                $scope.param_logo = r.data_header.logo;
				$scope.param_marcas = r.data_header.marcas;
            }
			//console.log("lala");
			//console.log(r.tabla_principal);
            setTimeout(function(){
                loadScript(options)
                // //////console.log($scope.tags);
                $(".counter_tags").counterUp({
                    delay: 10,
                    time: 1000
                });
            } , 1000);
			setTimeout(function(){
                ////////console.log(appEcharts.getOptionsDanosSeleccion());
            } , 5000);
			
        }
    }

    $scope.revision = function(porcentaje){
        if(porcentaje >=  parseFloat($scope.umbrales.green_umbral_1))
            return 'green-jungle';
        else if (porcentaje >= parseFloat($scope.umbrales.yellow_umbral_1) && porcentaje <= parseFloat($scope.umbrales.yellow_umbral_2))
            return 'yellow-lemon';
        else 
            return 'red-thunderbird'; 
    }
	
	

    $scope.viewGraph = function(i){
        if(1 == i){
            viewGraphPrincipal(i, $scope.data_grafica_calidad);
            $scope.leyendaGeneralTitle = 'Quality';
        }
        if(2 == i){
            viewGraphPrincipal(i, $scope.data_grafica_de);
            $scope.leyendaGeneralTitle = 'Standard deviation';
        }
        if(3 == i){
            viewGraphPrincipal(i, $scope.data_grafica_peso);
            $scope.leyendaGeneralTitle = 'Weight case';
        }
        if(4 == i){
            viewGraphPrincipal(i, $scope.data_grafica_cluster);
            $scope.leyendaGeneralTitle = 'Clúster';
        }
		if(5 == i){
            viewGraphPrincipal(i, $scope.grafica_principal_peso_cluster);
            $scope.leyendaGeneralTitle = 'Weight clúster';
        }
    }

    $scope.viewGraphCalidadHistorico = function(i){
        if(1 == i){
            viewGraphPrincipal(i, $scope.data_grafica_calidad_historico_marcas);
            $scope.leyendaGeneralTitle = 'Brands';
        }
    }

    $scope.viewGraphDanos = function(i){
        if(1 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_total);
            $scope.leyendaGeneralTitle = 'Total';
        }
        if(2 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_seleccion);
            $scope.leyendaGeneralTitle = 'Selection';
        }
        if(3 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_empaque);
            $scope.leyendaGeneralTitle = 'Packing';
        }
        if(4 == i){
            viewGraphDanos(i, $scope.data_grafica_danos_otros);
            $scope.leyendaGeneralTitle = 'Others';
        }
    }

}]);




