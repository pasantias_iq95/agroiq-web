var TableDatatablesEditable = function () {
    var oTable = {};
    var table = {};
    var labores = "";

     function destroy(){
        if(!isEmpty(table)){
            table.unbind("click");
            oTable.fnClearTable();
            oTable.fnDestroy();
        }
        return false;
    }

    var handleTable = function (restart) {
        var destroy_table = restart || 0;

        if(destroy_table > 0){
            destroy();
        }else{
            table = $('#sample_editable_1');
            table.unbind("click");
            oTable = table.dataTable({
                 "fixedHeader": true,
                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js). 
                // So when dropdowns used the scrollable div should be removed. 
                //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

                "lengthMenu": [
                    [5, 15, 20, -1],
                    [5, 15, 20, "All"] // change per page values here
                ],

                // Or you can use remote translation file
                //"language": {
                //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
                //},

                // set the initial value
                "pageLength": 5,
                "destroy": true,
                "language": {
                        "lengthMenu": "Vista de _MENU_ registros por página",
                        "zeroRecords": "No se encontro ningun registro",
                        "info": "Página _PAGE_ de _PAGES_",
                        "infoEmpty": "No hay registros disponibles",
                        "infoFiltered": "(filtrado de un total de _MAX_ registros)",
                        "aria": {
                            "sortAscending": ": orden acendente",
                            "sortDescending": ": orden decendente"
                        },
                        "emptyTable": "No hay registros en la tabla",
                        "search" : "Buscar :",
                        "paginate": {
                            "first":      "Primero",
                            "last":       "Ultimo",
                            "next":       "Siguiente",
                            "previous":   "Anterior",
                            "page": "Página",
                            "pageOf": "de"
                        },
                        "select": {
                            "rows": {
                                _: " %d filas selecionadas",
                                0: "",
                                1: "1 fila seleccionada"
                            }
                        }
                },
                "columnDefs": [{ // set default column settings
                    'orderable': true,
                    'targets': [0]
                }, {
                    "searchable": true,
                    "targets": [0]
                }],
                "buttons": [
                    { extend: 'excel', "title" : "Asistencias", "text" : "Excel" ,className: 'btn yellow btn-outline '}
                ],
                "order": [
                    [0, "asc"]
                ], // set first column as a default sort by asc
                "dom": "<'row' <'col-md-12'B>><'row'><'row'<'col-md-6 col-sm-12'lf>r><'table-scrollable't><'row'<'col-md-6 col-sm-12'i><'col-md-6 col-sm-12'p>>", // horizobtal scrollable datatable
                // "dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
            });
        }

        var tableWrapper = $("#sample_editable_1_wrapper");

    }
    return {

        //main function to initiate the module
        init: function () {
            handleTable();
        },
        restart : function(){
            handleTable(1);
        },
        destroy : function(){
            destroy();
        }

    };

}();

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

$(document).ready(function () {
         $('.calendar.left').hide();
         $('.calendar.right').hide();
         $('.ranges li:last-child' ).hide();
         $('.range_inputs' ).hide();
});

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        if(input === undefined)
            return;
        return input.slice(start);
    }
});

app.controller('informe_tthh', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){
    
    $scope.leyendaGeneralTitle = 'Merma';
    /*----------  CALENDARIO  ----------*/
    // $scope.getMonths = function(){
    //     var m = moment();
    //     var data = {};
    //     var months = moment.months();
    //     var month = "";
    //     var quincena1 = "";
    //     var quincena2 = "";
    //     // for(var i in months) {
    //         i = 9;
    //         month = "Quincena "+i+" 1";
    //         data[month] = []

    //         quincena1 = m.months(i).startOf("month").format('YYYY-MM-DD');
    //         // console.log(quincena1)
    //         data[month].push(quincena1);
    //         // console.log(data[month]);

    //         quincena2 = m.months(i).startOf("month").add(14,'days').format('YYYY-MM-DD');
    //         data[month].push(quincena2);

    //         month = "Quincena "+i+" 2";
    //         data[month] = [m.months(i).startOf("month").add(15,'days').format('YYYY-MM-DD') ,m.months(i).endOf("month").format('YYYY-MM-DD')];
    //     // }
    //     console.log(data);
    //     return data;
    // }

    $scope.rangesDirectives = {
        // "OCT.QUINCENA.1":[moment().months(9).startOf("month") , moment().months(9).startOf("month").add(14,'days')],
        // "OCT.QUINCENA.2":[moment().months(9).startOf("month").add(15,'days') , moment().months(9).endOf("month")],
        // "NOV.QUINCENA.1":[moment().months(10).startOf("month") , moment().months(10).startOf("month").add(14,'days')],
        // "NOV.QUINCENA.2":[moment().months(10).startOf("month").add(15,'days') , moment().months(10).endOf("month")],
        // "DIC.QUINCENA.1":[moment().months(11).startOf("month") , moment().months(11).startOf("month").add(14,'days')],
        // "DIC.QUINCENA.2":[moment().months(11).startOf("month").add(15,'days') , moment().months(11).endOf("month")],
        "ENE.QUINCENA.1":[moment().months(0).startOf("month") , moment().months(0).startOf("month").add(14,'days')],
        "ENE.QUINCENA.2":[moment().months(0).startOf("month").add(15,'days') , moment().months(0).endOf("month")],
        "FEB.QUINCENA.1":[moment().months(1).startOf("month") , moment().months(1).startOf("month").add(14,'days')],
        "FEB.QUINCENA.2":[moment().months(1).startOf("month").add(15,'days') , moment().months(1).endOf("month")],
        "MAR.QUINCENA.1":[moment().months(2).startOf("month") , moment().months(2).startOf("month").add(14,'days')],
        "MAR.QUINCENA.2":[moment().months(2).startOf("month").add(15,'days') , moment().months(2).endOf("month")],
        "ABR.QUINCENA.1":[moment().months(3).startOf("month") , moment().months(3).startOf("month").add(14,'days')],
        "ABR.QUINCENA.2":[moment().months(3).startOf("month").add(15,'days') , moment().months(3).endOf("month")],
        "MAY.QUINCENA.1":[moment().months(4).startOf("month") , moment().months(4).startOf("month").add(14,'days')],
        "MAY.QUINCENA.2":[moment().months(4).startOf("month").add(15,'days') , moment().months(4).endOf("month")],
        "JUN.QUINCENA.1":[moment().months(5).startOf("month") , moment().months(5).startOf("month").add(14,'days')],
        "JUN.QUINCENA.2":[moment().months(5).startOf("month").add(15,'days') , moment().months(5).endOf("month")],
        "JUL.QUINCENA.1":[moment().months(6).startOf("month") , moment().months(6).startOf("month").add(14,'days')],
        "JUL.QUINCENA.2":[moment().months(6).startOf("month").add(15,'days') , moment().months(6).endOf("month")],
        "AGO.QUINCENA.1":[moment().months(7).startOf("month") , moment().months(7).startOf("month").add(14,'days')],
        "AGO.QUINCENA.2":[moment().months(7).startOf("month").add(15,'days') , moment().months(7).endOf("month")],
        "SEP.QUINCENA.1":[moment().months(8).startOf("month") , moment().months(8).startOf("month").add(14,'days')],
        "SEP.QUINCENA.2":[moment().months(8).startOf("month").add(15,'days') , moment().months(8).endOf("month")],
        "OCT.QUINCENA.1":[moment().months(9).startOf("month") , moment().months(9).startOf("month").add(14,'days')],
        "OCT.QUINCENA.2":[moment().months(9).startOf("month").add(15,'days') , moment().months(9).endOf("month")],
    };

    $scope.limitDirectives = {
        days : 0
    }

    $scope.getDateRange = function(){
        var date = "";
        var ActualMonth = moment().month();
        var ActualDay = moment().date();

        var date = {
            inicial : "",
            fin : "",
        }
        if(ActualDay >= 15){
            date.inicial = moment().months((ActualMonth)).startOf("month").add(15,'days');
            date.fin = moment().months((ActualMonth)).endOf("month");
        }else{
            date.inicial = moment().months(ActualMonth).startOf("month")
            date.fin = moment().months(ActualMonth).startOf("month").add(14,'days')
        }

        return date;
    }

    $scope.StartEndDateDirectives = {
        startDate : $scope.getDateRange().inicial,
        endDate : $scope.getDateRange().fin,
    }

    // $scope.save = function(data){
    //     if(data.hasOwnProperty("V2")){
    //         if($scope.calidad.params.finca != ""){
    //             data.fecha_inicial = $scope.calidad.params.fecha_inicial;
    //             data.fecha_final = $scope.calidad.params.fecha_final;
    //             data.finca = $scope.calidad.params.finca;
    //             client.post("phrapi/guardar/asistencia" , function(r,b){
    //                 b();
    //             },data);
    //         }
    //     }
    // }

    /*----------  CALENDARIO  ----------*/
    
    // $scope.id_company = 0;
    // $scope.tags = {
    //     merma : {
    //         value : 0,
    //         label : ""
    //     },
    //     enfunde : {
    //         value : 0,
    //         label : ""
    //     },
    //     campo : {
    //         value : 0,
    //         label : ""
    //     },
    //     cosecha : {
    //         value : 0,
    //         label : ""
    //     },
    //     animales : {
    //         value : 0,
    //         label : ""
    //     },
    //     hongos : {
    //         value : 0,
    //         label : ""
    //     },
    //     empacadora : {
    //         value : 0,
    //         label : ""
    //     },
    //     fisiologicos : {
    //         value : 0,
    //         label : ""
    //     }
    // };

    // $scope.graficas = {
    //     historico : {},
    //     historico_avg : 0,
    //     dia : {},
    //     dia_title : "",
    //     danos : {},
    //     danos_detalle : {},
    // }

    // $scope.dia_semana = {
    //     L1 : "",
    //     M1 : "",
    //     MI1 : "",
    //     J1 : "",
    //     V1 : "",
    //     L2 : "",
    //     M2 : "",
    //     MI2 : "",
    //     J2 : "",
    //     V2 : "",
    // }
    // $scope.umbrales = {};

    $scope.getDateRange = function(){
        var date = "";
        var ActualMonth = moment().month();
        var ActualDay = moment().date();

        console.log(ActualMonth);
        console.log(ActualDay);

        var date = {
            inicial : "",
            fin : "",
        }
        if(ActualDay > 15 ){
            date.inicial = moment().months((ActualMonth)).startOf("month").add(15,'days');
            date.fin = moment().months((ActualMonth)).endOf("month");
        }else{
            date.inicial = moment().months(ActualMonth).startOf("month")
            date.fin = moment().months(ActualMonth).startOf("month").add(14,'days')
        }

        return date;
    }


    $scope.calidad = {
        params : {
            idFinca : 1,
            finca : "",
            fecha_inicial : $scope.getDateRange().inicial.format('YYYY-MM-DD'),
            fecha_final :  $scope.getDateRange().fin.format('YYYY-MM-DD'),
        },
        step : 0,
        path : ['phrapi/quintana/asistencia/index' , 'phrapi/merma/labores' , 'phrapi/merma/causas'],
        templatePath : [],
        nocache : function(){
            this.templatePath.push('/views/quintana/templetes/tthh/asistencia.html?' +Math.random());
            // $scope.tags.merma.value = new Number(0).toFixed(2);
            // $scope.tags.enfunde.value = new Number(0).toFixed(2);
            // $scope.tags.campo.value = new Number(0).toFixed(2);
            // $scope.tags.cosecha.value = new Number(0).toFixed(2);
            $scope.getDateRange();
            // $scope.getMonths();
            setTimeout(function(){
                // console.log($scope.tags);
                $(".counter_tags").counterUp({
                    delay: 10,
                    time: 1000
                });
            } , 1000);
        }

    }

    changeFinca = function(){
        // TableDatatablesEditable.restart();
        $scope.calidad.params.finca = $("#finca").val();
        $scope.loadExternal();
    }

    $scope.changeSort = function(column){
        if($scope.search.orderBy != column){
            var previous = $("th.selected")[0]            
            $(previous).removeClass("selected")
            $(previous).removeClass("sorting_asc")
            $(previous).removeClass("sorting_desc")
        }
        $scope.search.reverse = ($scope.search.orderBy != column) ? false : !$scope.search.reverse; 
        $scope.search.orderBy = column;
        var actual_select = $("#"+column+"_column")
        if(!actual_select.hasClass("selected")){
            actual_select.addClass("selected")
        }
        if($scope.search.reverse){
            actual_select.addClass("sorting_desc");
            actual_select.removeClass("sorting_asc");
        }else{
            actual_select.addClass("sorting_asc");
            actual_select.removeClass("sorting_desc");
        }
    }

    $scope.tabla = {
        detalle : []
    }

    $scope.fincas = [];

    // var detalle = {
    //             nombre : "FRNA",
    //             L1 : "1",
    //             M1 : "1",
    //             M1 : "1",
    //             J1 : "1",
    //             V1 : "1",
    //             L2 : "1",
    //             M2 : "1",
    //             M2 : "1",
    //             J2 : "1",
    //             V2 : "1",
    //             total : 10,
    //         }

    // $scope.tabla.detalle.push(detalle)

    $scope.changeRangeDate = function(data){

        if(data){
            // TableDatatablesEditable.restart();
            $scope.calidad.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
            $scope.calidad.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
        }
        $scope.loadExternal()
    }

    $scope.palancas = [];

    $scope.search = {
        nombre : "",
        orderBy : "nombre",
        reverse : false,
        actual_page : 1
    }
    $scope.tittles = [];
    $scope.personal = [];

    $scope.loadExternal = function(){
        //console.log($scope.calidad.path[$scope.calidad.step]);
        if($scope.calidad.path[$scope.calidad.step] != ""){
            var data = $scope.calidad.params;
            client.post($scope.calidad.path[$scope.calidad.step] , $scope.startDetails , data);
        }
    }

    $scope.exportarExcel = function(){
        tableToExcel("sample_editable_1", "Asistencia")
    }

    $scope.factorizar = function(personal){
        var res = [];
        var response = {};
        angular.forEach(personal, function (data) {
            response = {};
            angular.forEach($scope.tittles, function (dias) {

                if(data.dias.hasOwnProperty(dias.dia)){
                    response["lotes"] = data.dias[dias.dia].lotes;
                    response["labor"] = data.dias[dias.dia].labor;
                    response["hectareas"] = data.dias[dias.dia].hectareas;
                }else{
                   response[dias.dia] = "" ;
                }
            });
            response["id"] = data.id;
            response["nombre"] = data.nombre;
            response["total"] = data.total;
            res.push(response);
        });

        return res;
    }

    var tableToExcel = (function() {
      var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
      return function(table, name) {
        if (!table.nodeType) table = document.getElementById(table)
        var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
        window.location.href = uri + base64(format(template, ctx))
      }
    })()

    $scope.startDetails = function(r , b){
        b();
        if(r){
            // TableDatatablesEditable.destroy();

			var options = {};
            $scope.fincas = r.fincas || [];
            $scope.tittles = r.tittles || [];
            // $scope.personal = r.personal || [];

            // $scope.personal = $scope.factorizar(r.personal) || [];
            $scope.personal = r.personal || [];
            console.log($scope.personal);
            // setTimeout(function(){
            //     TableDatatablesEditable.init();
            // } , 1500);
        }
    }

    $scope.next = function(dataSource){
        if($scope.search.actual_page < parseInt(dataSource.length / parseInt($scope.search.limit)) + (dataSource.length % parseInt($scope.search.limit) == 0 ? 0 : 1)) 
            $scope.search.actual_page++;
    }

    $scope.prev = function(dataSource){
        if($scope.search.actual_page > 1)
            $scope.search.actual_page--;
    }
}]);