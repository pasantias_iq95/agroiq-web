app.service('request', function($http){
    var service = {};

    service.init = function (params) {
        return $http.post('phrapi/quintana/clima/index', params);
    };

    service.graficaHoras = function(params){
        return $http.post('phrapi/quintana/clima/horas', params);
    }

    return service;
});

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
        var filtered = [];
    	angular.forEach(items, function(item) {
      		filtered.push(item);
        });
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.filter('avg', function() {
	return function(items) {
        var sum = 0, count = 0;
    	angular.forEach(items, function(value) {
      		if(parseFloat(value)){
                  count++;
                  sum += parseFloat(value);
            }
        });

    	return (sum / count).toFixed(2);
  	};
});

app.filter('sum', function() {
	return function(items) {
        var sum = 0;
    	angular.forEach(items, function(value) {
      		if(parseFloat(value)){
                sum += parseFloat(value);
            }
        });

    	return (sum).toFixed(2);
  	};
});

app.filter('min', function() {
	return function(items) {
        var min = null;
    	angular.forEach(items, function(value) {
      		if((parseFloat(value) && min == null) || (parseFloat(value) && parseFloat(value) < min)){
                min = parseFloat(value);
            }
        });

    	return min;
  	};
});

app.filter('max', function() {
	return function(items) {
        var max = null;
    	angular.forEach(items, function(value) {
      		if((parseFloat(value) && max == null) || (parseFloat(value) && parseFloat(value) > max)){
                max = parseFloat(value);
            }
        });

    	return max;
  	};
});

app.controller('controller', ['$scope', 'request', function ($scope, $request) {
    $scope.filters = {
        year : moment().format("YYYY"),
        luz : 400,
        fecha_inicial : moment().subtract(7, 'day').format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD')
    }

    $scope.StartEndDateDirectives = {
        startDate : moment().subtract(7, 'day'),
        endDate :moment(),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.filters.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.filters.fecha_inicial;
            $scope.filters.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.filters.fecha_final;
        }
        $scope.chageGraficaHorasDias()
    }

    $scope.years = [];
    $scope.gerentes = [];
    var tables = []

    $scope.grafica_temp_min = []
    $scope.grafica_temp_max = []
    $scope.grafica_lluvia = []
    $scope.grafica_humedad = []
    $scope.grafica_rad_solar = []
    $scope.grafica_horas_luz = {}

    const initTable = (r, index) => {
        var props = {
            header : [{
                   key : 'detalle',
                   name : 'DETALLE',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                    key : 'total',
                    name : 'TOTAL',
                    locked : true,
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    formatter : 'Number',
                    resizable : true,
                },{
                   key : 'avg',
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                }
            ],
            data : r,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        tables[index].exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        $scope.semanas.map((sem) => {
            props.header.push({
                key : `sem_${sem}`,
                name : `SEM ${sem}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
            })
        })
        tables[index] = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById("table_"+index))        
    }

    const initTables = () => {
        Object.keys($scope.tablas).map((key, index) => {
            var data = [
                $scope.tablas[key].temp_min, 
                $scope.tablas[key].temp_max, 
                $scope.tablas[key].lluvia, 
                $scope.tablas[key].rad_solar, 
                $scope.tablas[key].horas_luz_100, 
                $scope.tablas[key].horas_luz_150, 
                $scope.tablas[key].horas_luz_200, 
                $scope.tablas[key].horas_luz_400, 
                $scope.tablas[key].humedad
            ]
            initTable(data, index)
        })
    }

    $scope.success = function(r){
        var r = r.data

        if(r.hasOwnProperty("semanas")){
            $scope.semanas = r.semanas;
        }
        if(r.hasOwnProperty("years")){
            $scope.years = r.years;
        }
        if(r.hasOwnProperty("gerentes")){
            $scope.gerentes = r.gerentes;
        }
        if(r.hasOwnProperty("tablas")){
            $scope.tablas = r.tablas;
        }
        if(r.hasOwnProperty("grafica_temp_min")){
            var data = {
                series: r.grafica_temp_min.data,
                legend: r.grafica_temp_min.legend,
                umbral: null,
                id: "grafica_temp_min",
                type : 'line',
                zoom : false,
                legendBottom : true,
                min : r.grafica_temp_min.min
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_temp_min'));
        }
        if(r.hasOwnProperty("grafica_temp_max")){
            var data = {
                series: r.grafica_temp_max.data,
                legend: r.grafica_temp_max.legend,
                umbral: null,
                id: "grafica_temp_max",
                type : 'line',
                zoom : false,
                legendBottom : true,
                min : r.grafica_temp_max.min
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_temp_max'));
        }
        if(r.hasOwnProperty("grafica_lluvia")){
            var data = {
                series: r.grafica_lluvia.data,
                legend: r.grafica_lluvia.legend,
                umbral: null,
                id: "grafica_lluvia",
                type : 'line',
                zoom : false,
                legendBottom : true,
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_lluvia'));
        }
        if(r.hasOwnProperty("grafica_humedad")){
            var data = {
                series: r.grafica_humedad.data,
                legend: r.grafica_humedad.legend,
                umbral: null,
                id: "grafica_humedad",
                type : 'line',
                zoom : false,
                legendBottom : true,
                min : r.grafica_humedad.min
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_humedad'));
        }
        if(r.hasOwnProperty("grafica_rad_solar")){
            var data = {
                series: r.grafica_rad_solar.data,
                legend: r.grafica_rad_solar.legend,
                umbral: null,
                id: "grafica_rad_solar",
                type : 'line',
                zoom : false,
                legendBottom : true,
                min : r.grafica_rad_solar.min
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_rad_solar'));
        }
        if(r.hasOwnProperty("grafica_horas_luz")){
            $scope.grafica_horas_luz = r.grafica_horas_luz
            var data = {
                series: r.grafica_horas_luz[$scope.filters.luz].data,
                legend: r.grafica_horas_luz[$scope.filters.luz].legend,
                umbral: null,
                id: "grafica_horas_luz",
                type : 'line',
                zoom : false,
                legendBottom : true,
                min: r.grafica_horas_luz[$scope.filters.luz].min
            }
            ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_horas_luz'));
        }
        

        setTimeout(() => {
            initTables()
        }, 200)
    }

    $scope.changeLuz = () => {
        var data = {
            series: $scope.grafica_horas_luz[$scope.filters.luz].data,
            legend: $scope.grafica_horas_luz[$scope.filters.luz].legend,
            umbral: null,
            id: "grafica_horas_luz",
            type : 'line',
            zoom : false,
            legendBottom : true,
            min : $scope.grafica_horas_luz[$scope.filters.luz].min
        }
        ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_horas_luz'));
    }

    $scope.chageGraficaHorasDias = () => {
        var data = $scope.filters
        load.block('grafica_horas_dia')
        $request.graficaHoras(data).then(r => {
            load.unblock('grafica_horas_dia')
            var r = r.data
            
            if(r.hasOwnProperty("grafica_horas_dia")){
                var data = {
                    series: r.grafica_horas_dia.data,
                    legend: r.grafica_horas_dia.legend,
                    umbral: null,
                    id: "grafica_horas_dia",
                    type : 'line',
                    zoom : false,
                    legendBottom : true,
                    min: r.grafica_horas_dia.min
                }
                let parent = $("#grafica_horas_dia").parent()
                parent.empty()
                parent.append('<div id="grafica_horas_dia" class="charts"></div>')

                ReactDOM.render(React.createElement(Historica, data), document.getElementById('grafica_horas_dia'));
            }
        }).catch($scope.error)
    }

    $scope.setFilterGerente = function(data){
        if(data){
            $scope.filters.gerente = data.id
            $scope.filters.gerenteName = data.label
            $scope.init($scope.filters);
        }
    }
    $scope.setFilterYears = function(year){
        if(year){
            $scope.filters.year = year
            $scope.init($scope.filters);
        }
    }
    
    $scope.error = function(err){
        console.error(err)
    }

    $scope.init = function(){
        var data = $scope.filters
        $request.init(data)
            .then($scope.success)
            .catch($scope.error)

        $scope.chageGraficaHorasDias()
    }
    
    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                // remove filters
                /*var cut = contentTable.search('<tr role="row" class="filter">')
                var cut2 = contentTable.search('</thead>')

                var part1 = contentTable.substring(0, cut)
                var part2 = contentTable.substring(cut2, contentTable.length)
                contentTable = part1 + part2*/
                
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, title || "")
    }

    $scope.exportPrint = function(id_table){
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        // remove filters
        /*var cut = contentTable.search('<tr role="row" class="filter">')
        var cut2 = contentTable.search('</thead>')
        var part1 = contentTable.substring(0, cut)
        var part2 = contentTable.substring(cut2, contentTable.length)
        // add image
        contentTable = image + part1 + part2*/

        var newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
    }

}])