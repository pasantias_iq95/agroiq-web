app.service('cajas', ['client', function(){
    this.index = function(callback, params){
        let url = 'phrapi/cajas/index'
        let data = params || {}
        client.post(url, callback, data, 'content')
    }
}])

app.controller('produccion', ['$scope','$http','$interval','$controller','$timeout' ,'$window','$filter', function($scope,$http,$interval,$controller , $timeout,$window,$filter){
    
    $scope.charts = {
        racimos : new echartsPlv(),
        peso : new echartsPlv(),
        manos : new echartsPlv(),
        calibracion : new echartsPlv(),
        dedo : new echartsPlv(),
        edad_promedio : new echartsPlv(),
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

}]);

