function initGrafica(id){
    var options = {
            tooltip : {
                trigger: 'axis',
                axisPointer : {
                    type : 'shadow'
                }
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            legend: {
                data:['TM', 'TE'],
                y : 'bottom'
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '10%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
                    data : ['RUTA 1', 'RUTA 2']
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'TM',
                    type:'bar',
                    data:[0, 0]
                },
                {
                    name:'TE',
                    type:'bar',
                    data:[0, 0]
                },
            ]
        };
    var myChart = new echartsPlv();
    myChart.init(id , options);
}

function initGrafica2(id){
    var options = {
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            legend: {
                show : true,
                orient : 'vertical',
                x : 'left'
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '10%',
                containLabel: true
            },
            series : {
                type : 'pie',
                data : [{
                        name:'TM',
                        type:'pie',
                        value : 0,
                        label : {
                            normal : {
                                show : true,
                                position : 'inside',
                                formatter : '{b} \n {c} ({d}%)'
                            }
                        }
                    },
                    {
                        name:'TE',
                        value : 0,
                        label : {
                            normal : {
                                show : true,
                                position : 'inside',
                                formatter : '{b} \n {c} ({d}%)'
                            }
                        }
                    }
                ]
            }
        };
    var myChart = new echartsPlv();
    myChart.init(id , options);
}

app.service('request',['$http', function($http){
    
    this.ventaDia = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/ventadia'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.ventaSemana = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/ventasemana'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.ventaMes = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/ventames'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.ventaAnio = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/ventaanio'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.dia = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/dia'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.semana = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/semana'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.mes = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/mes'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.anio = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/anio'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.last = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/last'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
    this.loadPoints = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/points'

        load.block('map_box')
        $http.post(url, params || {}).then(r => {
            load.unblock('map_box')
            callback(r.data)
        })
    }
    this.loadMorePoints = function(callback, params){
        let url = 'phrapi/lancofruit/rastreo/points'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }
}])

app.controller('rastreo', ['$scope', 'request' , function($scope,request){

    // 27/04/2017 - TAG: GOOGLE MAPS
    var lineSymbol = {
        path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
    };
    $scope.now = moment().format('YYYY-MM-DD')

    var cargo_pestana_uno = false
    var cargo_pestana_dos = false
    var cargo_pestana_tres = false
    var cargo_pestana_cuatro = false
    var routes = [], map = {}
    $scope.data_ventas_dia = []
    $scope.data_ventas_semana = []
    $scope.data_ventas_mes = []
    $scope.data_ventas_anio = []
    $scope.data_semanas = []
    $scope.table = []
    $scope.filters = {
        sector : "",
        ruta : "",
        search : "",
        tipo : "",
        status : "",
        year : moment().year(),
        semana : moment().week(),
        fecha : moment().format('YYYY-MM-DD')
    }

    setCameraPosition = (lat, lng) => {
        var fromProjection = new OpenLayers.Projection("EPSG:4326");   // Transform from WGS 1984
        var toProjection   = new OpenLayers.Projection("EPSG:900913"); // to Spherical Mercator Projection
        var position       = new OpenLayers.LonLat(lng, lat).transform( fromProjection, toProjection);
        var zoom = 16;
        map.setCenter (position, zoom);
    }

    $scope.createMap = () => {
        var options = {
            controls: [
                new OpenLayers.Control.Navigation(),
                new OpenLayers.Control.PanZoomBar(),
            ]
        };
        map = new OpenLayers.Map("gmap_routes", options);
        map.addLayer(new OpenLayers.Layer.OSM());
        setCameraPosition(-2.188861, -79.898896)
    }

    $scope.last = () => {
        request.last((r) => {
            $scope.anios = r.years
            $scope.filters.year = r.last_year
            $scope.fechas = r.fechas

            $scope.notToday = r.fechas.indexOf($scope.now) == -1
        })
    }

    $scope.route = []
    $scope.routeObj = []
    
    $scope.trazarRuta = () => {
        $scope.removeAllSteps()

        let waypoints = angular.copy($scope.route)
        $scope.startRouting(waypoints)
    }

    /*$scope.startRouting = (start, end, waypoints) => {
        console.log(`${start.lat},${start.lng} -> ${end.lat},${end.lng}`)

        map.travelRoute({
            origin: [start.lat, start.lng],
            destination: [end.lat, end.lng],
            travelMode: 'driving',
            step: function(e) {
                $('#gmap_routes_instructions').append('<li>'+e.instructions+'</li>');
                $('#gmap_routes_instructions li:eq(' + e.step_number + ')').delay(450 * e.step_number).fadeIn(200, function() {
                    routes.push(map.drawPolyline({
                        path: e.path,
                        strokeColor: '#131540',
                        strokeOpacity: 0.6,
                        strokeWeight: 6
                    }));
                });
            },
            end : function(){
                waypoints.splice(0, 1)
                if(waypoints.length > 1){
                    $scope.startRouting(waypoints[0], waypoints[1], waypoints)
                }
            }
        });
    }*/

    $scope.startRouting = (waypoints) => {
        var vectorLayer = new OpenLayers.Layer.Vector("Overlay", {
            renderers: ['SVGExtended', 'VMLExtended', 'CanvasExtended'],
            styleMap: new OpenLayers.StyleMap({
                'default': OpenLayers.Util.extend({
                    orientation: true
                }, OpenLayers.Feature.Vector.style['default']),
                'temporary': OpenLayers.Util.extend({
                    orientation: true
                }, OpenLayers.Feature.Vector.style['temporary'])
            }),
            style : {
                strokeColor : '#ff0000',
                orientation : 'any value'
            }
        });
        
        var feature = new OpenLayers.Feature.Vector(
            new OpenLayers.Geometry.LineString(waypoints)
        );
    
        vectorLayer.addFeatures(feature);
        map.addLayer(vectorLayer);
        routes.push(vectorLayer)
    }

    delay = () => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if($scope.filters.fecha == moment().format('YYYY-MM-DD'))
                    $scope.loadMorePoints(true)
            }, 5000)
        })
    }

    $scope.changeDate = () => {
        if($scope.filters.fecha == moment().format('YYYY-MM-DD'))
            $("#live").removeClass('hide')
        else
            $("#live").addClass('hide')

        $scope.removeAllSteps()
        $scope.route = []
        $scope.routeObj = []
        $scope.loadMorePoints(false)
    }

    responsePoints = (r) => {
        if(r.data.length > 0){
            setCameraPosition(r.data[r.data.length-1].lat, r.data[r.data.length-1].lng)

            var waypoints = []
            let epsg4326 =  new OpenLayers.Projection("EPSG:4326"); //WGS 1984 projection
            let projectTo = map.getProjectionObject(); //The map projection (Spherical Mercator)
            r.data.map((c) => {
                waypoints.push(new OpenLayers.Geometry.Point( c.lng, c.lat ).transform(epsg4326, projectTo))
            })
            $scope.startRouting(waypoints)
            $scope.route = $scope.route.concat(waypoints)
            $scope.routeObj = $scope.routeObj.concat(r.data)
        }
        delay()
    }

    $scope.loadMorePoints = (morePoints) => {
        if(morePoints){
            request.loadMorePoints(responsePoints, {
                hora : $scope.routeObj.length > 0 ? $scope.routeObj[$scope.routeObj.length-1].time : '',
                fecha : $scope.filters.fecha
            })
        }else{
            request.loadPoints(responsePoints, {
                hora : $scope.routeObj.length > 0 ? $scope.routeObj[$scope.routeObj.length-1].time : '',
                fecha : $scope.filters.fecha
            })
        }
    }

    $scope.removeAllSteps = () => {
        var ids = []
        routes.map((vectorLayer) => {
            vectorLayer.features.map((feature) => {
                ids.push(feature.id)
            })
            ids.map((id) => {
                vectorLayer.removeFeatures(vectorLayer.getFeatureById(id));
            })
        })
    }

    /*$scope.semaforo = value => {
        if(value > 0)
        if($scope.isNumeric(value)){
            return 'bg-green-jungle bg-font-green-jungle'
        }else if(value == 'F'){
            return 'bg-red-thunderbird bg-font-red-thunderbird'
        }
        return ''
    }*/

	$scope.request = {
		all_pestana_uno : function(){
            if(!cargo_pestana_uno){
                cargo_pestana_uno = true;
                this.ventaDia();
                setTimeout(function(){
                    initGrafica('chart_1');
                    initGrafica2('chart_2');
                },1000)
            }
        },
        all_pestana_dos : function(){
            if(!cargo_pestana_dos){
                cargo_pestana_dos = true
                this.semana();
                this.ventaSemana();
                setTimeout(function(){
                    initGrafica('chart_3');
                    initGrafica2('chart_4');
                },1000)
            }
        }
        ,
        all_pestana_tres : function(){
            if(!cargo_pestana_tres){
                cargo_pestana_tres = true
                this.ventaMes();
                setTimeout(function(){
                    initGrafica('chart_5');
                    initGrafica2('chart_6');
                },1000)
            }
        }
        ,
        all_pestana_cuatro : function(){
            if(!cargo_pestana_cuatro){
                cargo_pestana_cuatro = true
                this.ventaAnio();
                setTimeout(function(){
                    initGrafica('chart_7');
                    initGrafica2('chart_8');
                },1000)
            }
        },
        init : function(){
            $scope.createMap();
            $scope.last();
            $scope.loadMorePoints(false)
        },
        ventaDia : function(){
            request.ventaDia(r => {
                if(r.data_venta_dia){
                    $scope.data_ventas_dia = r.data_venta_dia;
                }
            }, $scope.filters)
        },
        ventaSemana : function(){
            request.ventaSemana(r => {
                if(r.data_venta_semana){
                    $scope.data_ventas_semana = r.data_venta_semana;
                }
            }, $scope.filters)
        },
        ventaMes : function(){
            request.ventaMes(r => {
                if(r.data_venta_mes){
                    $scope.data_ventas_mes = r.data_venta_mes;
                }
            }, $scope.filters)
        },
        ventaAnio : function(){
            request.ventaAnio(r => {
                if(r.data_venta_anio){
                    $scope.data_ventas_anio = r.data_venta_anio;
                }
            }, $scope.filters)
        },
        semana : function(){
            request.semana(r => {
                if(r.data_semana){
                    $scope.data_semanas = r.data_semana;
                }
            }, $scope.filters)
        }
    }

    $scope.filtraSemana = function(){
        $scope.filters.semana = ("#semana").val();
        $scope.request.all_pestana_dos();
    }

    $scope.request.init();
}])