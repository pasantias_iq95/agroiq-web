app.controller('controller', ['$scope', '$http', function($scope, $http){
    
    $scope.filters = {
        fecha_inicial : moment().format('YYYY-MM-DD'),
        fecha_final : moment().format('YYYY-MM-DD')
    }

    checkUmbral = (value) => {
        if(parseFloat(value)){
            var umbral = $scope.umbral

            if(value > umbral){
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            }else if(value == umbral){
                return 'bg-yellow-gold bg-font-yellow-gold'
            }else{
                return 'bg-green-jungle bg-font-green-jungle'
            }
        }
        return ''
    }

    initTableDedosProm = (r) => {
        var props = {
            header : [{
                   key : 'lote',
                   name : 'LOTE',
                   titleClass : 'text-center',
                   alignContent : 'center',
                   locked : true,
                   resizable : true,
                   sortable : true,
                   width: 170
                },{
                   key : 'avg',
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valueCell = rowData['avg']
                        return `
                            <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valueCell = rowData['max']
                        return `
                            <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valueCell = rowData['min']
                        return `
                            <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : r.data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        Object.keys(r.semanas).map((index) => {
            let value = r.semanas[index]
            props.header.push({
                key : `sem_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valueCell = rowData['sem_'+value]
                    return `
                        <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        $("#table-dedos-prom").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('table-dedos-prom'))
        $scope.table1.handleGridSort('lote', 'ASC')
    }

    $scope.loadTableDedosProm = () => {
        $http.post('phrapi/reiset/merma/tableDedosPromedio', $scope.filters).then((r) => {
            if(r){
                $scope.umbral = r.data.umbral
                initTableDedosProm(r.data)
            }
        })
    }

    $scope.init = () => {
        $scope.loadTableDedosProm()
    }

    $scope.init()

}]);