app.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            if(parseFloat(value[key]))
                sum = sum + parseFloat(value[key], 10);
        });        
        return sum;
    }
})

app.service('request', ['$http', function($http){
    this.index = function(callback, params){
        let url = 'phrapi/reiset/enfunde/edit/index'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.guardar = function(callback, params){
        let url = 'phrapi/reiset/enfunde/edit/save'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.saldo = function(callback, params){
        let url = 'phrapi/reiset/enfunde/edit/saldo'
        let data = params ||  {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.getDataCinta = function(callback, params){
        let url = 'phrapi/reiset/enfunde/edit/cinta'
        let data = params ||  {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }
}])

app.controller('produccion', ['$scope', 'request', function($scope, $request){

    $scope.coloresClass = {
        'VERDE' : 'bg-green-haze bg-font-green-haze',
        'AZUL' : 'bg-blue bg-font-blue',
        'BLANCO' : 'bg-white bg-font-white',
        'LILA' : 'bg-purple-studio bg-font-purple-studio',
        'ROJA' : 'bg-red-thunderbird bg-font-red-thunderbird',
        'CAFE' : 'bg-brown bg-font-brown',
        'AMARILLO' : 'bg-yellow-lemon bg-font-yellow-lemon',
        'NEGRO' : 'bg-dark bg-font-dark'
    }
    
    $scope.data = {
        fecha_res : moment().format('YYYY-MM-DD'),
        fecha : moment().format('YYYY-MM-DD'),
        semana : moment().week(),
        saldo_inicial : 0,
        cinta : ''
    }

    printInit = (r) => {
        $scope.cinta = r.cinta
        $scope.cintas_available = r.cintas_available
        $scope.enfundadores = r.enfundadores
        $scope.lotes = r.lotes
        $scope.data.saldo_inicial = parseInt(r.saldo_inicial)
    }
    /*printChangeFecha = (r) => {
        $scope.enfundadores = r.enfundadores
        $scope.lotes = r.lotes
        $scope.data.saldo_inicial = parseInt(r.saldo_inicial)
    }*/
    printDataCinta = (r) => {
        $scope.edad = r.edad
    }
    $scope.init = () => {
        $request.index(printInit, $scope.data)
    }

    $scope.changeCinta = () => {
        $request.getDataCinta(printDataCinta, $scope.data)
    }

    $scope.changeSaldo = () => {
        $request.saldo((r) => {
            $scope.data.saldo_inicial = parseInt(r.saldo_inicial)
        }, $scope.data)
    }

    $scope.changeFecha = () => {
        if($scope.data.fecha == ""){
            $scope.data.fecha = angular.copy($scope.data.fecha_res)
        }else{
            $scope.data.fecha_res = angular.copy($scope.data.fecha)
            $scope.data.semana = moment($scope.data.fecha_res).week()
            $request.index(printInit, $scope.data)
        }
    }

    $scope.guardar = () => {
        var cancel = false
        if(!$scope.data.enfundador > 0){
            cancel = true
            alert("Colocar un enfundador")
        }
        if(!$scope.data.lote > 0){
            cancel = true
            alert("Colocar el lote")
        }
        if(($scope.data.entregadas == '' || $scope.data.entregadas == 0) && !$scope.data.usadas > 0){
            cancel = true
            alert("Colocar las fundas usadas")
        }
        if(!cancel){
            let data = angular.copy($scope.data)
            $request.guardar((r) => {
                if(r.status == 200){
                    alert("Se guardo con éxito", "", "success", () => {
                        window.location.href = "produccionenfunde"
                    })
                }else{
                    alert("Ocurrio algun problema")
                }
            }, data)
        }
    }

    $scope.init()

}]);

