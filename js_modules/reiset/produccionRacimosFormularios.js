app.service('produccion', ['client', '$http', function(client, $http){

    this.lastDay = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/formulariosracimos/last'
        client.post(url, callback, data)
    }

    this.resumen = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/formulariosracimos/resumen'
        client.post(url, callback, data, 'promedios_lotes')
    }

    this.edades = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/formulariosracimos/edades'
        client.post(url, callback, data, 'edades')
    }

    this.registros = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/formulariosracimos/historico'
        client.post(url, callback, data, 'registros')
    }

    this.eliminar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/formulariosracimos/eliminar'
        client.post(url, callback, data)
    }

    this.editar = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/formulariosracimos/editar'
        client.post(url, callback, data, 'edit_registros')
    }

    this.analisisRecusados = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/formulariosracimos/recusados'
        client.post(url, callback, data, 'table-defectos')
    }

    this.tags = function(callback, params){
        let data = params || {}
        let url = 'phrapi/reiset/formulariosracimos/tags'
        client.post(url, callback, data, 'tags')
    }

    this.recusados = function(callback, params){
        load.block('recusados')
        $http.post('phrapi/reiset/formulariosracimos/diarecusados', params || {})
        .then((r) => {
            load.unblock('recusados')
            callback(r.data)
        })
    }

    this.muestreo = function(callback, params){
        $http.post('phrapi/reiset/formulariosracimos/muestreo', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.editarVariableViaje = function(callback, params){
        $http.post('phrapi/reiset/formulariosracimos/variableViaje', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.borrarViaje = function(callback, params){
        $http.post('phrapi/reiset/formulariosracimos/borrarViaje', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.borrarViajeBalanza = function(callback, params){
        $http.post('phrapi/reiset/racimos/borrarViaje', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.agregarRacimo = function(callback, params){
        $http.post('phrapi/reiset/formulariosracimos/agregarRacimo', params || {})
        .then((r) => {
            callback(r.data)
        })
    }

    this.procesarRacimosViajes = function(callback, params){
        $http.post('phrapi/reiset/formulariosracimos/procesar', params || {})
        .then((r) => {
            callback(r.data)
        })
    }
}])

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
            }
        });
        return sum;
    }
})

app.filter('countOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != undefined && value[key] != null && value[key] > 0){
                count++
            }
        });
        return count;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', ['$filter', function($filter) {
	return function(items, field, reverse) {
    	let filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
            }
            if(Object.keys(item)){
                Object.keys(item).map((key) => {
                    if(Number(item[key])){
                        item[key] = Number(item[key])
                    }
                })
            }
      		filtered.push(item);
        });
        
        if(Array.isArray(field)){
            filtered = $filter('orderBy')(filtered, field);
        }else{
            filtered.sort(function (a, b) {
                if(field == 'hora' || field == 'fecha'){
                    return moment(a.date).isAfter(b.date) ? 1 : -1;
                }else if(parseFloat(a[field]) && parseFloat(b[field])){
                    return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
                }else{
                    return (a[field] > b[field] ? 1 : -1);
                }
            });
            if(reverse) filtered.reverse();
        }

    	return filtered;
  	};
}]);

app.directive('enter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.enter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('escape', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 27) {
                scope.$apply(function (){
                    scope.$eval(attrs.escape);
                });

                event.preventDefault();
            }
        });
    };
});

function number_format(amount, decimals) {
    
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

function checkUmbral (value, umbral) {
    if(value && umbral){
        if(value > umbral) return 'bg-red-thunderbird bg-font-red-thunderbird'
        else return 'bg-green-haze bg-font-green-haze'
    }
    return ''
}

function renderPie(id, _data){
    let options = {
        data : _data,
        nameseries : "Pastel",
        titulo : "",
        id : id
    }
    ReactDOM.render(React.createElement(Pastel, options), document.getElementById(id));
}

let datesEnabled = []

app.controller('produccion', ['$scope', 'produccion', function($scope, produccion){

    datepickerHighlight = () => {
        $('#datepicker').datepicker({
            beforeShowDay: function (date) {
                let fecha = moment(date).format('YYYY-MM-DD')
                let has = datesEnabled.indexOf(fecha) > -1
                return has ? { classes: 'highlight', tooltip: 'Procesado', enabled : true } : { tooltip : 'Sin proceso', enabled : false }
            }
        });
        $('#datepicker').datepicker()
        .on('changeDate', function(e) {
            $scope.changeRangeDate({ first_date: moment(e.date).format('YYYY-MM-DD'), second_date : moment(e.date).format('YYYY-MM-DD') })
        });
        $("#datepicker").datepicker('setDate', $scope.produccion.params.fecha_inicial)
        $('#datepicker').datepicker('update')
    }

    $scope.anioCambio = false
    $scope.id_company = 0;
    $scope.subTittle = ""
    $scope.registros = [];
    $scope.count = 0;

    $scope.produccion = {
        params : {
            finca : '',
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
            fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
            cliente: "",
            marca: "",
            palanca : "",
            initial : 0,
        },
        nocache : function(){
            $scope.getRegistros()
            if($scope.anioCambio){
                $scope.getAnalisisRecusados()
            }
        }
    }
    $scope.coloresClass = {
        'VERDE' : 'bg-green-haze bg-font-green-haze',
        'AZUL' : 'bg-blue bg-font-blue',
        'BLANCO' : 'bg-white bg-font-white',
        'LILA' : 'bg-purple-studio bg-font-purple-studio',
        'ROJO' : 'bg-red-thunderbird bg-font-red-thunderbird',
        'CAFE' : 'bg-brown bg-font-brown',
        'AMARILLO' : 'bg-yellow-lemon bg-font-yellow-lemon',
        'NEGRO' : 'bg-dark bg-font-dark'
    }

    $scope.startDate = ''
    $scope.getLastDay = function(){
        produccion.lastDay(function(r, b){
            b()
            if(r){
                $scope.fincas = r.fincas
                $scope.lotes = r.lotes

                $scope.produccion.params.fecha_inicial = r.last.fecha
                $scope.produccion.params.fecha_final = r.last.fecha
                //$scope.produccion.params.finca = Object.keys(r.fincas)[0]
                $scope.produccion.params.finca = ''
                $scope.table = {
                    fecha_inicial : r.last.fecha
                }
                $scope.startDate = r.last.fecha
                datesEnabled = r.days
                datepickerHighlight()
                $scope.getAnalisisRecusados()
                $scope.produccion.nocache()
            }
        })
    }

    $scope.lastDay = {
        startDate : moment().startOf('month'),
        endDate : moment().endOf('month'),
    }

    $scope.changeRangeDate = function(data){
        if(data){
            $scope.anioCambio = moment(data.first_date).year() != moment($scope.produccion.params.fecha_inicial).year()

            $scope.produccion.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.produccion.params.fecha_inicial;
            $scope.produccion.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.produccion.params.fecha_final;
            $scope.subTittle = data.date_select || "Ultimo Registro"
        }
        $scope.produccion.nocache()
    }

    $scope.tabla = {
        produccion : []
    }

    $scope.enableEdit = (row, campo) => {
        if(!row.grupo_racimo > 0 || !row.form){
            if(campo == 'lote'){
                row.editingLote = true
            }
            else if(campo == 'cuadrilla'){
                row.editingCuadrilla = true
            }
        }
    }

    $scope.enter = (row, campo) => {
        if(campo == 'lote'){
            if(row.newLote == ''){
                alert("El lote no puede ser vacio")
            }else{
                produccion.cambiarLote((r, b) => {
                    b()
                    if(r.status == 200){
                        row.lote = row.newLote
                        $scope.check = {}
                        $scope.escape(row, campo)
                    }else{
                        alert('Hubo un problema favor intentelo mas tarde')
                    }
                }, { grupo_racimo : row.grupo_racimo, 
                    lote : row.newLote, 
                    id_finca : $scope.produccion.params.finca,
                    id_formulario : row.id_formulario
                })
            }
        }else if(campo == 'cuadrilla'){
            if(row.newCuadrilla == ''){
                alert("La cuadrilla no puede estar vacia")
            }else{
                produccion.cambiarCuadrilla((r, b) => {
                    b()
                    if(r.status == 200){
                        row.cuadrilla = row.newCuadrilla
                        $scope.check = {}
                        $scope.escape(row, campo)
                    }else{
                        alert('Hubo un problema favor intentelo mas tarde')
                    }
                }, { grupo_racimo : row.grupo_racimo, 
                    cuadrilla : row.newCuadrilla, 
                    id_finca : $scope.produccion.params.finca,
                    id_formulario : row.id_formulario 
                })
            }
        }
    }

    $scope.escape = (row, campo) => {
        if(campo == 'lote'){
            row.editingLote = false
        }
        else if(campo == 'cuadrilla'){
            row.editingCuadrilla = false
        }
    }

    $scope.eliminar = () => {
        var elements = $(".delete")
        if(elements.length > 0){
            if(confirm(`Seguro deseas eliminar ${elements.length} racimo(s)?`)){
                var ids = []
                var index = []
                for(var x = 0; x < elements.length; x++){
                    ids.push({
                        id : $(elements[x]).attr('data-id')
                    })
                }
                produccion.eliminar((r, b) => { 
                    b()
                    if(r){
                        $scope.getRegistros()
                        alert(`Se borraron ${elements.length} registros con éxito`, "", "success")
                    }
                }, { ids : ids })
            }
        }else{
            alert("No has seleccionado ninguna fila")
        }
    }

    $scope.colores = [];
    $scope.totales = [];
    $scope.palancas = [];

    $scope.getRegistros = function(){
        $scope.cargandoHistorico = true
        let data = $scope.produccion.params;
        
        produccion.registros(function(r, b){
            b('registros')
            $scope.cargandoHistorico = false

            if(r){
                $scope.registros = r.data
                $scope.registrosBalanza = r.data_balanza
                $scope.viajes = r.viajes
                $scope.viajesBalanza = r.viajes_balanza
                $scope.comparativo = r.comparativo

                $scope.searchTable.changePagination()
                $scope.searchTableBalanza.changePagination()
            }
        }, data)

        produccion.resumen(function(r, b){
            b('promedios_lotes')
            if(r){
                $scope.resumen = r.data
                $scope.edades = r.edades
            }
        }, data)

        produccion.edades(function(r, b){
            b('edades')
            if(r){
                $scope.resumen_edades = r.data
            }
        }, data)

        produccion.tags(function(r, b){
            b('tags')
            if(r){
                $scope.tags = r.tags
            }
        }, data)

        produccion.recusados((r) => {
            if(r){
                $scope.recusados = r.data

                var data_chart = r.data.map((r) => {
                    return  {
                        label : r.causa,
                        value : r.cantidad
                    }
                })
                renderPie('grafica-defectos', data_chart)
            }
        }, data)

        if($scope.produccion.params.initial == 0){
            produccion.muestreo((r) => {
                $scope.muestreo = r.data
                $scope.openMuestreo()
            }, $scope.produccion.params)
        }
    }

    $scope.openMuestreo = () => {
        setTimeout(() => {
            if($("[href=#collapse_3_6]").attr('aria-expanded') == 'true'){
                new echartsPlv().init('grafica-muestreo', $scope.muestreo)
            }
        }, 250)
    }

    $scope.edit = function(row){
        if(!$scope.editing){
            $scope.editing = true;
            $scope.editRow = row;
            $scope.editRow.editing = true;
        }
    }

    $scope.editViaje = function(row, key){
        if(row && key){
            if(!row[key]){
                row[key] = true
            }
        }
    }

    $scope.saveViaje = function(row, variable){
        produccion.editarVariableViaje((r) => {
            if(r){
                alert('Guardado con éxito', '', 'success')
                row[variable+'Editing'] = false
            }
        }, {
            viaje : row.viaje,
            variable : variable == 'lote' ? 'lote' : 'cuadrilla',
            valor : row[variable],
            fecha : $scope.produccion.params.fecha_inicial
        })
    }

    $scope.guardar = function(){
        var valid = true
        if(!$scope.editing) valid = false;
        //if($scope.editRow.peso <= 0) valid = false;
        if($scope.editRow.cuadrilla == "") valid = false;
        if($scope.editRow.lote == "") valid = false;
        if($scope.editRow.edad == "") valid = false;
        
        if(valid){
            $scope.editRow.editing = false;
            $scope.editing = false;
            produccion.editar((r, b) => {
                b('edit_registros')
                if(r){
                    $scope.editRow.class = r.class
                    $scope.getRegistros()
                }else{
                    alert("Ocurrio un error al guardar")
                }
            }, $scope.editRow)
        }else{
            alert("Favor de completar los campos")
        }
    }

    $scope.next = function(dataSource, config){
        if(config.actual_page < parseInt(dataSource.length / parseInt(config.limit)) + (dataSource.length % parseInt(config.limit) == 0 ? 0 : 1)) 
        config.actual_page++;
        
            config.startFrom = (config.actual_page - 1) * (parseInt(config.limit))
    }

    $scope.prev = function(dataSource, config){
        if(config.actual_page > 1)
        config.actual_page--;
        
        config.startFrom = (config.actual_page - 1) * (parseInt(config.limit))
    }

    $scope.setOrderTable = function(field){
        var reverse = false
        if(field == $scope.searchTable.orderBy){
            reverse = !$scope.searchTable.reverse
        }else{
            $scope.searchTable.orderBy = field
        }
        $scope.searchTable.reverse = reverse
    }

    $scope.filterViajes = {}
    $scope.searchTable = {
        orderBy : ['viaje', 'num_racimo'],
        reverse : false,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        changePagination : function(){
            if($scope.searchTable.limit == 0)
                $scope.searchTable.limit = $scope.registros.length

            $scope.searchTable.numPages = parseInt($scope.registros.length / $scope.searchTable.limit)  + ($scope.registros.length % $scope.searchTable.limit == 0 ? 0 : 1)
            $scope.searchTable.actual_page = 1
            $scope.searchTable.startFrom = 0
        }
    }

    $scope.searchFilterBalanza = {}
    $scope.searchTableBalanza = {
        orderBy : ['viaje', 'num_racimo'],
        reverse : false,
        limit : 10,
        actual_page : 1,
        startFrom : 0,
        optionsPagination : [
            10, 50, 100
        ],
        changePagination : function(){
            if($scope.searchTableBalanza.limit == 0)
                $scope.searchTableBalanza.limit = $scope.registrosBalanza.length

            $scope.searchTableBalanza.numPages = parseInt($scope.registrosBalanza.length / $scope.searchTableBalanza.limit)  + ($scope.registrosBalanza.length % $scope.searchTableBalanza.limit == 0 ? 0 : 1)
            $scope.searchTableBalanza.actual_page = 1
            $scope.searchTableBalanza.startFrom = 0
        }
    }

    $scope.exportExcel = function(id_table, title){
        var data = new Blob([document.getElementById(id_table).outerHTML], {
            type : 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf8'
        })
        saveAs(data, 'Base de datos racimos.xls')
    }

    $scope.exportPrint = function(id_table){
        $("#"+id_table).css("display", "")
        let image = '<div style="display:block; height:55;"><img style="float: right;" width="100" height="50" src="./../logos/Logo.png" /></div><br>';
        let table = document.getElementById(id_table);
        var contentTable = table.outerHTML;
        newWin = window.open("");
        newWin.document.write(contentTable);
        newWin.print();
        newWin.close();
        $("#"+id_table).css("display", "none")
    }

    $scope.exportarFormatoEspecial = () => {
        dayOfWeek = (day) => { return ["DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO"][day]  };
        embedInRow = (cols) => { return `<tr>${cols}</tr>` };
        generateRow = () => { var cols = []; for(var i = 0; i < 10; i ++) { cols.push("<td></td>") } return cols; };
        getCintaValida = (cinta) => { return { VERDE : 'VERDE', ROJA : 'ROJA', AMARILLO : 'AMARI', CAFE  : 'CAFE', AZUL : 'AZUL', BLANCO : 'BLANC', NEGRO : 'NEGRA', LILA : 'LILA' }[cinta]; }
        var table = []
        
        row = generateRow();
        row[0] = `<td>Archivo no.: ${moment($scope.produccion.params.fecha_inicial).format('DDD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Nombre: SEMANA ${moment($scope.produccion.params.fecha_inicial).format('WW')} ${dayOfWeek(moment($scope.produccion.params.fecha_inicial).format('d'))} ${moment($scope.produccion.params.fecha_inicial).format('DD')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        row = generateRow();
        row[0] = `<td>Fecha: ${moment($scope.produccion.params.fecha_inicial).format('DD-MM-YYYY')}</td>`;
        table.push("<tr>" + row.join("") + "</tr>")

        table.push([
            "<tr>",
            "<td>F01IDV(4)isID</td>",
            "<td>DW2Peso()</td>",
            "<td>C03LOTE()</td>",
            "<td>C13CINTA()</td>",
            "<td>F12MANOS(##)notID</td>",
            "<td>F22CALIBRE(##.##)notID</td>",
            "<td>F32L DEDOS(##.##)notID</td>",
            "<td>F43TIPO()notID</td>",
            "<td>F53NIVEL()notID</td>",
            "<td>F63CAUSA()notID</td>",
            "</tr>"
        ].join(""))

        table.push([
            "<tr>",
            "<td>IDV</td>",
            "<td>Peso</td>",
            "<td>LOTE</td>",
            "<td>CINTA</td>",
            "<td>MANOS</td>",
            "<td>CALIBRE</td>",
            "<td>L DEDOS</td>",
            "<td>TIPO</td>",
            "<td>NIVEL</td>",
            "<td>CAUSA</td>",
            "</tr>"
        ].join(""))

        $scope.registros.map((value, index) => {
            if(parseInt(value.id) > 0){
                table.push([
                    "<tr>",
                    `<td>${value.id}</td>`,
                    `<td>${value.peso}</td>`,
                    `<td>${value.lote}</td>`,
                    `<td>${getCintaValida(value.cinta)}</td>`,
                    `<td>${(value.manos > 0) ? value.manos : ''}</td>`,
                    `<td>${(value.calibre > 0) ? value.calibre : ''}</td>`,
                    `<td>${(value.dedos > 0) ? value.dedos : ''}</td>`,
                    `<td></td>`,
                    `<td>${(value.tipo == 'RECU') ? 'RECHA' : ''}</td>`,
                    `<td>${(value.tipo == 'RECU') ? value.causa : ''}</td>`,
                    "</tr>"
                ].join(""))
            }
        })

        // Añadie cuadre
        if($scope.dia_cuadrado){
            $scope.cuadre_proc.map((value, index) => {
                var diferencia = value.form - value.blz 
                for(var i = 0; i < diferencia; i++){
                    table.push([
                        "<tr>",
                        `<td></td>`,
                        `<td>${value.peso_prom}</td>`,
                        `<td>${value.lote}</td>`,
                        `<td>${getCintaValida(value.cinta)}</td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        "</tr>"
                    ].join(""))
                }
            })

            $scope.faltante_recu.map((value, index) => {
                var diferencia = value.cantidad - value.cantidad_blz 
                for(var i = 0; i < diferencia; i++){
                    table.push([
                        "<tr>",
                        `<td></td>`,
                        `<td>${value.peso_prom}</td>`,
                        `<td>${value.lote}</td>`,
                        `<td>${getCintaValida(value.color_cinta)}</td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td></td>`,
                        `<td>RECHA</td>`,
                        `<td>${value.causa}</td>`,
                        "</tr>"
                    ].join(""))
                }
            })
        }
        
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function() {
                var contentTable = table.join("")
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel()
    }

    $scope.getAnalisisRecusados = () => {
        produccion.analisisRecusados((r, b) => {
            b('table-defectos')
            r.data.map((value) => {
                if(value.dano == 'TOTAL') $scope.umbralRecusados = value.prom
            })
            printTableRecusados(r.data, r.semanas)
        }, $scope.produccion.params)
    }

    var printTableRecusados = (data, semanas) => {
        let id = 'table-defectos'
        var props = {
            header : [{
                   key : 'dano',
                   name : 'DEFECTO',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                    key : 'sum',
                    name : 'SUM',
                    locked : true,
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    formatter : 'Number',
                    resizable : true,
                    customCell : function(rowData, isChildren){
                        var valNumber = parseFloat(rowData['sum'])
                        if($scope.produccion.params.var_recusado == 'porc') valNumber = 0
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                             <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                 ${valueCell}
                             </div>
                        `;
                     }
                 },{
                   key : 'prom',
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['prom'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min'])
                        let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                        return `
                            <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        semanas.map((value) => {
            props.header.push({
                key : `sem_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+value])
                    let valueCell = valNumber > 0 ? number_format(valNumber, ($scope.var_recusado == 'cant') ? 0 : 2) : ''
                    return `
                        <div class="text-center ${checkUmbral(valNumber, $scope.umbralRecusados)}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }

    $scope.cleanFilter = (filters) => {
        if(Object.keys(filters)){
            Object.keys(filters).map((key) => {
                if(!filters[key]) delete filters[key]
            })
        }
    }

    $scope.borrarViaje = (row) => {
        if(row){
            if(confirm(`¿Estas seguro de borrar el viaje ${row.viaje}?`)){
                produccion.borrarViaje((r) => {
                    if(r){
                        $scope.getRegistros()
                    }else{
                        alert(`Ocurrio algo inesperado, Contacte a soporte`)
                    }
                }, Object.assign(row, { fecha : $scope.produccion.params.fecha_inicial }))
            }
        }
    }

    $scope.borrarViajeBalanza = (row) => {
        if(row){
            if(confirm(`¿Estas seguro de borrar el viaje ${row.viaje}?`)){
                produccion.borrarViajeBalanza((r) => {
                    if(r){
                        $scope.getRegistros()
                    }else{
                        alert(`Ocurrio algo inesperado, Contacte a soporte`)
                    }
                }, Object.assign(row, { fecha : $scope.produccion.params.fecha_inicial }))
            }
        }
    }

    // Agregar racimos
    $scope.newRacimo = {
        cinta : 'S/C',
        lote : '',
        viaje : undefined,
        cuadrilla : '',
        causa : ''
    }
    $scope.causas = [
        {
            "id": "1",
            "nombre": "ALTERADO"
        },
        {
            "id": "2",
            "nombre": "ANIMALES"
        },
        {
            "id": "3",
            "nombre": "BAJO GRADO"
        },
        {
            "id": "4",
            "nombre": "DA\u00d1O DE ZORRO"
        },
        {
            "id": "5",
            "nombre": "ESTROPEO"
        },
        {
            "id": "6",
            "nombre": "EXPLOSIVOS"
        },
        {
            "id": "16",
            "nombre": "FILIPINO"
        },
        {
            "id": "7",
            "nombre": "INSECTOS"
        },
        {
            "id": "8",
            "nombre": "MADURO"
        },
        {
            "id": "9",
            "nombre": "MUTANTE"
        },
        {
            "id": "10",
            "nombre": "POBRE"
        },
        {
            "id": "11",
            "nombre": "PUNTA DE CIGARRO"
        },
        {
            "id": "12",
            "nombre": "RACIMO CAIDO"
        },
        {
            "id": "13",
            "nombre": "RECHAZADO"
        },
        {
            "id": "14",
            "nombre": "SOBRE GRADO"
        },
        {
            "id": "15",
            "nombre": "VIROSIS"
        }
    ]
    $scope.abrirModalAgregarRacimos = () => {
        $scope.newRacimo = {
            cinta : 'S/C',
            lote : '',
            viaje : undefined,
            cuadrilla : '',
            causa : ''
        }
        $("#agregar-racimo").modal('show')
    }
    $scope.guardarAgregarRacimo = () => {
        if($scope.newRacimo.cinta != '' && $scope.newRacimo.cuadrilla != '' && $scope.newRacimo.lote != '' && $scope.newRacimo.viaje > 0){
            produccion.agregarRacimo((r) => {
                if(r){
                    alert('Agregado con éxito, Refresque', 'Racimos', 'success')
                }else{
                    alert(`Ocurrio algo inesperado, Contacte a soporte`)
                }
                $("#agregar-racimo").modal('hide')
            }, Object.assign($scope.newRacimo, { fecha : $scope.produccion.params.fecha_inicial }))
        }else{
            alert('Completar información')
        }
    }

    // Procesar racimos
    $scope.procesarRacimosViajes = () => {
        if(confirm(`¿Está seguro de hacer esto? (No hay retroseso)`)){
            /*produccion.procesarRacimosViajes((r) => {
                if(r.status === 200){
                    alert(`Procesado con éxito`, '', 'success', () => {
                        window.location.reload()
                    })
                }else{
                    alert(`Algo a salido mal, contacte a soporte`)
                }
            }, {
                fecha : $scope.produccion.fecha_inicial,
                
            })*/
            alert(`Esta funcionalidad esta desactivada`)
        }
    }

}]);

