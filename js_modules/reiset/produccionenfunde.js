app.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            if(parseFloat(value[key]))
                sum = sum + parseFloat(value[key], 10);
        });        
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
            if(field == 'hora' || field == 'fecha'){
                item.date = moment(item.fecha  + ' ' + item.hora)
            }
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(field == 'hora' || field == 'fecha'){
                return moment(a.date).isAfter(b.date) ? 1 : -1;
            }else if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.service('request', ['$http', function($http){
    this.last = function(callback, params){
        let url = 'phrapi/reiset/enfunde/last'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.index = function(callback, params){
        let url = 'phrapi/reiset/enfunde/index'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.tags = function(callback, params){
        let url = 'phrapi/reiset/enfunde/tags'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        })
    }

    this.grafica = function(callback, params){
        let url = 'phrapi/reiset/enfunde/grafica'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.graficaLote = function(callback, params){
        let url = 'phrapi/reiset/enfunde/loteGrafica'
        let data = params || {}
        $http.post(url, data).then((r) => {
            callback(r.data)
        });
    }

    this.lotesSemanal = function(callback, params){
        let url = 'phrapi/reiset/enfunde/lotesSemanal'
        let data = params || {}
        load.block('head_3_1')
        $http.post(url, data).then((r) => {
            load.unblock('head_3_1')
            callback(r.data)
        });
    }

    this.enfundadoresSemanal = function(callback, params){
        let url = 'phrapi/reiset/enfunde/enfundadoresSemanal'
        let data = params || {}
        load.block('head_3_2')
        $http.post(url, data).then((r) => {
            load.unblock('head_3_2')
            callback(r.data)
        });
    }

    this.enfundadoresLotes = function(callback, params){
        let url = 'phrapi/reiset/enfunde/enfundadoresLote'
        let data = params || {}
        load.block('table_enfundador_export')
        $http.post(url, data).then((r) => {
            load.unblock('table_enfundador_export')
            callback(r.data)
        }).catch((err) => {
            load.unblock('table_enfundador_export')
        });
    }
}])

app.controller('produccion', ['$scope', 'request', function($scope, $request){
    
    $scope.filters = {
        year : moment().year(),
        semana : moment().week(),
        variable : 'HA',
        sector : ''
    }
    $scope.data = {
        grafica : {},
        graficaLote : {},
        tableLote : {},
        tableEnfundadores : {},
        semanas : []
    }
    printInit = (r) => {
        $scope.anios = r.anios
        $scope.semanas = r.semanas
        $scope.tabla_enfundadores = r.enfundadores
        $scope.dias = r.dias
        setTimeout(() => {
            $scope.$apply(() => {
                $scope.enfundador_semana = r.tabla_enfundador_semanal
            })
        }, 250)
    }
    printGrafica = (r) => {
        let data = {
            series: r.data.data,
            legend: r.data.legend,
            umbral: parseFloat(r.umbral),
            id: "semanal",
            type : 'line',
            min : 'dataMin'
        }
        let parent = $("#semanal").parent()
        $("#semanal").remove()
        parent.append(`<div style="height: 400px;" id="semanal"></div>`)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById('semanal'));
    }
    printGraficaLote = (r) => {
        var data = {
            series: r.data.data,
            legend: r.data.legend,
            umbral: parseFloat(r.umbral),
            id: "lote_semanal",
            type : 'line',
            min : 'dataMin'
        }
        let parent = $("#lote_semanal").parent()
        $("#lote_semanal").remove()
        parent.append(`<div style="height: 400px;" id="lote_semanal"></div>`)
        ReactDOM.render(React.createElement(Historica, data), document.getElementById('lote_semanal'));
    }
    initTableLote = (r) => {
        if(!$scope.data.tableLote.data)
            $scope.data.tableLote = r
        var ha = $scope.filters.variable != 'SEM' ? '_ha' : '';
        var props = {
            header : [{
                   key : 'lote',
                   name : 'LOTE',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 170
                },{
                   key : 'avg'+ha,
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valueCell = rowData['avg'+ha]
                        if(!isChildren){
                            if(rowData.lote == 'TOTAL'){
                                return `
                                    <div class="text-center ${checkUmbralGlobal(valueCell)}" style="height: 100%">
                                        ${valueCell}
                                    </div>
                                `;
                            }else{
                                return `
                                    <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                        ${valueCell}
                                    </div>
                                `;
                            }
                        }else{
                            return `
                                <div class="text-center" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }
                    }
                },{
                   key : 'max'+ha,
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valueCell = rowData['max'+ha]
                        if(!isChildren){
                            if(rowData.lote == 'TOTAL'){
                                return `
                                    <div class="text-center ${checkUmbralGlobal(valueCell)}" style="height: 100%">
                                        ${valueCell}
                                    </div>
                                `;
                            }else{
                                return `
                                    <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                        ${valueCell}
                                    </div>
                                `;
                            }
                        }else{
                            return `
                                <div class="text-center" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }
                    }
                },{
                   key : 'min'+ha,
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valueCell = rowData['min'+ha]
                        if(!isChildren){
                            if(rowData.lote == 'TOTAL'){
                                return `
                                    <div class="text-center ${checkUmbralGlobal(valueCell)}" style="height: 100%">
                                        ${valueCell}
                                    </div>
                                `;
                            }else{
                                return `
                                    <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                        ${valueCell}
                                    </div>
                                `;
                            }
                        }else{
                            return `
                                <div class="text-center" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }
                    }
                }
            ],
            data : r.data,
            height : 450,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        Object.keys(r.semanas).map((index) => {
            let value = r.semanas[index]
            props.header.push({
                key : `sem${ha}_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valueCell = rowData['sem'+ha+'_'+value]
                    if(!isChildren){
                        if(rowData.lote == 'TOTAL'){
                            return `
                                <div class="text-center ${checkUmbralGlobal(valueCell)}" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }else{
                            return `
                                <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }
                    }else{
                        return `
                            <div class="text-center" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            })
        })
        $("#lotes_semanal").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('lotes_semanal'))
    }
    initTableEnfundadores = (r) => {
        if(!$scope.data.tableEnfundadores.data)
            $scope.data.tableEnfundadores = r

        var ha = $scope.filters.variable != 'SEM' ? '_ha' : '';
        var props = {
            header : [{
                   key : 'enfundador',
                   name : 'ENFUNDADOR',
                   locked : true,
                   width : 150,
                   resizable : true,
                   sortable : true,
                   titleClass : 'text-center',
                },{
                   key : 'avg'+ha,
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   titleClass : 'text-center',
                   customCell : function(rowData){
                        let valueCell = rowData['avg'+ha]
                        if(rowData.enfundador == 'TOTAL'){
                            return `
                                <div class="text-center ${checkUmbralGlobal(valueCell)}" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }else{
                            return `
                                <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }
                    }
                },{
                   key : 'max'+ha,
                   name : 'MAX',
                   locked : true,
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   titleClass : 'text-center',
                   customCell : function(rowData){
                        let valueCell = rowData['max'+ha]
                        if(rowData.enfundador == 'TOTAL'){
                            return `
                                <div class="text-center ${checkUmbralGlobal(valueCell)}" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }else{
                            return `
                                <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }
                    }
                },{
                   key : 'min'+ha,
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   titleClass : 'text-center',
                   customCell : function(rowData){
                        let valueCell = rowData['min'+ha]
                        if(rowData.enfundador == 'TOTAL'){
                            return `
                                <div class="text-center ${checkUmbralGlobal(valueCell)}" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }else{
                            return `
                                <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                    ${valueCell}
                                </div>
                            `;
                        }
                    }
                }
            ],
            data : r.data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table2.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        Object.keys(r.semanas).map((index) => {
            let value = r.semanas[index]
            props.header.push({
                key : `sem${ha}_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                titleClass : 'text-center',
                customCell : function(rowData){
                    let valueCell = rowData['sem'+ha+'_'+value]
                    if(rowData.enfundador == 'TOTAL'){
                        return `
                            <div class="text-center ${checkUmbralGlobal(valueCell)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }else{
                        return `
                            <div class="text-center ${checkUmbral(valueCell)}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }
            })
        })
        $("#enfundadores_semanal").html("")
        $scope.table2 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('enfundadores_semanal'))
    }
    printTableEnfundadores = (r) => {
        $scope.lotes = r.lotes
        $scope.enfundadores_lote = r.data
    }
    responseTags = (r) => {
        $scope.tags = r.tags
    }

    $scope.init = () => {
        $request.index(printInit, $scope.filters)
        $scope.loadEnfundadoresLote(false)
        $scope.loadTags()
    }

    $scope.changeYear = () => {
        $scope.init()
        $scope.loadLotesSemanal()
        $scope.loadEnfundadoresSemanal()
        $scope.loadTags()
    }

    $scope.loadGrafica = () => {
        $request.grafica((r) => {
            if(!$scope.data.grafica.data)
                $scope.data.grafica = r

            printGrafica({ data : $scope.filters.variable == 'HA' ? r.data_ha : r.data, umbral : r.umbral_ha })
        }, $scope.filters)
        $request.graficaLote((r) => {
            if(!$scope.data.graficaLote.data)
                $scope.data.graficaLote = r

            printGraficaLote({ data : $scope.filters.variable == 'HA' ? r.data_ha : r.data, umbral : r.umbral_ha })
        }, $scope.filters)
    }

    $scope.loadLotesSemanal = (click) => {
        if(click) if($("#collapse_3_1").attr('aria-expanded') == 'false'){
            var data = angular.copy($scope.filters)
            $request.lotesSemanal(initTableLote, data)
        }
    }

    var checkUmbral = (value) => {
        if(parseFloat(value)){
            var umbral = 0
            if($scope.filters.variable == 'SEM'){
                umbral = parseFloat(angular.copy($scope.data.graficaLote.umbral))
            }else{
                umbral = parseFloat(angular.copy($scope.data.graficaLote.umbral_ha))
            }

            if(value > umbral){
                return 'bg-green-jungle bg-font-green-jungle'
            }else if(value == umbral){
                return 'bg-yellow-gold bg-font-yellow-gold'
            }else{
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            }
        }
        return ''
    }
    var checkUmbralGlobal = (value) => {
        if(parseFloat(value)){
            var umbral = 0
            if($scope.filters.variable == 'SEM'){
                umbral = parseFloat(angular.copy($scope.data.grafica.umbral))
            }else{
                umbral = parseFloat(angular.copy($scope.data.grafica.umbral_ha))
            }

            if(value > umbral){
                return 'bg-green-jungle bg-font-green-jungle'
            }else if(value == umbral){
                return 'bg-yellow-gold bg-font-yellow-gold'
            }else{
                return 'bg-red-thunderbird bg-font-red-thunderbird'
            }
        }
        return ''
    }

    $scope.changeVariable = () => {
        if($scope.filters.variable == 'SEM'){
            printGrafica({ data : $scope.data.grafica.data, umbral : $scope.data.grafica.umbral })
            printGraficaLote({ data : $scope.data.graficaLote.data, umbral : $scope.data.graficaLote.umbral })
        }else{
            printGrafica({ data : $scope.data.grafica.data_ha, umbral : $scope.data.grafica.umbral_ha })
            printGraficaLote({ data : $scope.data.graficaLote.data_ha, umbral : $scope.data.graficaLote.umbral_ha })
        }

        if($scope.data.tableLote.data) initTableLote($scope.data.tableLote);
        if($scope.data.tableEnfundadores.data) initTableEnfundadores($scope.data.tableEnfundadores);
    }

    $scope.loadEnfundadoresLote = (click) => {
        if((click && $("#collapse_3_3").attr('aria-expanded') == 'false') || (!click && $("#collapse_3_3").attr('aria-expanded') == 'true')){
            var data = angular.copy($scope.filters)
            $request.enfundadoresLotes(printTableEnfundadores, data)
        }
    }

    $scope.loadEnfundadoresSemanal = (click) => {
        if(click && $("#collapse_3_2").attr('aria-expanded') == 'false'){
            var data = angular.copy($scope.filters)
            $request.enfundadoresSemanal(initTableEnfundadores, data)
        }
    }

    $scope.loadTags = () => {
        $request.tags(responseTags, angular.copy($scope.filters))
    }

    $scope.loadUltimaSemana = () => {
        $request.last((r) => {
            $scope.filters.semana = r.semana

            $scope.init()
            $scope.loadGrafica()
            $scope.loadTags()
        })
    }

    $scope.loadUltimaSemana()

    $scope.exportExcel = function(id_table, title){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var contentTable = table.innerHTML
                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel(id_table, "Información de Transtito")
    }
}]);

