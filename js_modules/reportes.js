import echarts from 'echarts'

/*----------  UTILIDADES SOBRE ARRAYS  ----------*/
Array.prototype.amax = function() {
	return {
		label : this.indexOf(Math.max.apply(null, this)),
		value : new Number(Math.max.apply(null, this)).toFixed(2)
	}
};

Array.prototype.amin = function() {
	return {
		label : this.indexOf(Math.min.apply(null, this)),
		value : new Number(Math.min.apply(null, this)).toFixed(2)
	}
};

Array.prototype.sum = function (prop) {
	var property = prop || -1;
    var total = 0
    if(property == prop){
	    for ( var i = 0, _len = this.length; i < _len; i++ ) {
	        total += this[i][prop]
	    }
    }else{
    	for ( var i = 0, _len = this.length; i < _len; i++ ) {
	        total += this[i]
	    }
    }
    return total
}

Array.prototype.avg = function (prop) {
	var property = prop || -1;
    var total = 0 , avg = 0;
    if(property == prop){
	    for ( var i = 0, _len = this.length; i < _len; i++ ) {
	        total += this[i][prop]
	    }
    }else{
    	for ( var i = 0, _len = this.length; i < _len; i++ ) {
	        total += this[i]
	    }
    }

    avg = (total / this.length);

    return new Number(avg).toFixed(2);
}

Array.prototype.getUnique = function(){
   var u = {}, a = [];
   for(var i = 0, l = this.length; i < l; ++i){
      if(u.hasOwnProperty(this[i])) {
         continue;
      }
      a.push(this[i]);
      u[this[i]] = 1;
   }
   return a;
}
/*----------  UTILIDADES SOBRE ARRAYS  ----------*/


var myChartPrincial = {};
var data_graficas = { 
	semana : {},
	mes : {},
	auditoria : {}
};
var actual_graficas = {};

function loadScript(step , options , reload , optionsPrincipal , umbral , umbral_range, modo){
	//--- BAR ---
	console.log("step = " + step)
	if(step == 0){
		if(options.main){
			var label_principal = {
				selected : {},
				data : []
			};
			var series_principal = [] , count = 0;
			series_principal.push({
				name : options.label_principal[0],
				type : "line",
				data : [],
				markLine : {
					data : [
						[
							{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
							{xAxis: 52, yAxis: umbral}
						],
					]
				}
			});

			// label_principal = options.label_principal.amap(function(item){return item});
			label_principal.selected[series_principal[0].name] = true;
			label_principal.data.push(series_principal[0].name)
			for(var x in options.series){
				series_principal.push({
					name : x,
					type : "line",
					data : []
				});
				label_principal.selected[x] = false;
				label_principal.data.push(x);
				for(var i in options.categories){
					if(!isNaN(parseFloat(options.main[options.categories[i]])) && count == 0){
						series_principal[0].data.push(new Number(options.main[options.categories[i]]).toFixed(2))
					}else{
						series_principal[0].data.push("-");
					}

					if(!isNaN(parseFloat(options.series[x][options.categories[i]]))){
						series_principal[(count + 1)].data.push(new Number(options.series[x][options.categories[i]]).toFixed(2))
					}else{
						series_principal[(count + 1)].data.push("-")
					}
				}
				count++;
			}
			myChartPrincial = echarts.init(document.getElementById('echarts_bar') , 'infographic');
			myChartPrincial.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: label_principal,
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: true
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: options.categories,
				}],
				yAxis: [{
					axisLine : {onZero: false},
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series_principal
			});
			myChartPrincial.setTheme('infographic');
		}

		var graphic = {
			labor : false,
			lote : false
		};

		if(options.data_labores){
			var label = [] , series = [] , labor = "" , count = 0, categories = [];
				// var back = ["#b5c334"];
			// label.push("UMBRAL");
			categories = options.data_labores.labels;
			for (var p in options.data_labores) {
				if(p != "labels"){
					if (options.data_labores.hasOwnProperty(p)) {
						// series[0].data.push({value : umbral});)
						if(labor != options.data_labores[p].labor){
							label.push(options.data_labores[p].labor);
							labor = options.data_labores[p].labor;
							series[count] = {
								name : options.data_labores[p].labor ,
								type : "bar",
								data : [],
							}
							if(count == 0){
								series[count].markLine = {
									data : [
										[
											{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
											{xAxis: 12, yAxis: umbral}
										],
									]
								}
							}
							count++;
						}
						series[(count-1)].data.push({value : new Number((options.data_labores[p].promedio)).toFixed(2)});
					}
				}
			}
			/*=========================================
			=            GRAFICA PRINCIPAL            =
			=========================================*/

			/*=====  End of GRAFICA PRINCIPAL  ======*/

			var myChartLabor = echarts.init(document.getElementById('echarts_bar_labores') , 'green');
			myChartLabor.setOption({
				title : {
					text: 'Labores',
					subtext: 'Promedios Labores',
					x:'center'
				},
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					data: label,
					y : 'bottom'
				},
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: false
						},
						magicType: {
							show: false,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [
					{
						type: 'category',
						data: [""],
						position: 'bottom',
					}
				],
				yAxis: [{
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series
			});

			myChartLabor.setTheme('infographic');
			graphic.labor = true;
		}

		if(options.zonas){
			var data = {
						data : [
							[
								{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
								{xAxis: 12, yAxis: umbral}
							],
						]
					}
			options.zonas.series[0].markLine = data;
			var myChartLotes = echarts.init(document.getElementById('echarts_bar_lotes') , 'green');
			myChartLotes.setOption({
					title : {
						text: 'Zonas',
						subtext: 'Promedios Zonas',
						x:'center'
					},
					tooltip: {
						trigger: 'axis'
					},
					legend: {
						data: options.zonas.label,
						y : 'bottom'
					},
					toolbox: {
						show: true,
						feature: {
							mark: {
								show: false
							},
							dataView: {
								show: false,
								readOnly: false
							},
							magicType: {
								show: false,
								type: ['line', 'bar']
							},
							restore: {
								show: true
							},
							saveAsImage: {
								show: true
							}
						}
					},
					calculable: true,
					xAxis: [{
						type: 'category',
						data: [""]
					}],
					yAxis: [{
						type: 'value',
						min : umbral_range.min,
						max : umbral_range.max,
						splitArea: {
							show: true
						}
					}],
					series: options.zonas.series
			});
			myChartLotes.setTheme('infographic');
			graphic.lote = true;
		}
		if(options.fincas){

			var label = [] , series = [] , labor = "" , count = 0 ,count_lote = 0, promedio_lote = [] , label_promedio = [] , tpromedio = 0 , promedio = [] ,sum_promedio = 0;
			for(var finca in options.fincas){
				if (options.fincas.hasOwnProperty(finca)) {
					label.push(finca);
					count_lote = 0;
					sum_promedio = 0;
					promedio = [];
					for (var key in options.fincas[finca].lote) {
						if (options.fincas[finca].lote.hasOwnProperty(key)) {
							promedio_lote[count_lote] = [];
							promedio.push(key);
							label_promedio = options.fincas[finca].lote[key].labores.map(function(num) { promedio_lote[count_lote].push(num.promedio); return num.promedio; });
							tpromedio = promedio_lote[count_lote].reduce(function(a, b) { return parseFloat(a) + parseFloat(b); });
							sum_promedio += new Number((tpromedio / promedio_lote[count_lote].length)).toFixed(2);
							count_lote++;
						}
					}
					series[count] = {
						name : finca,
						type : "bar",
						data :  [new Number((sum_promedio / promedio.length)).toFixed(2)]
					}
					if(count == 0){
						series[count].markLine = {
							data : [
								[
									{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
									{xAxis: 12, yAxis: umbral}
								],
							]
						}
					}
					count++;
				}
			}

			var myChartLotes = echarts.init(document.getElementById('echarts_bar_lotes') , 'green');
			myChartLotes.setOption({
					title : {
						text: 'Lotes',
						subtext: 'Promedios Lotes',
						x:'center'
					},
					tooltip: {
						trigger: 'axis'
					},
					legend: {
						data: label,
						y : 'bottom'
					},
					toolbox: {
						show: true,
						feature: {
							mark: {
								show: false
							},
							dataView: {
								show: false,
								readOnly: false
							},
							magicType: {
								show: false,
								type: ['line', 'bar']
							},
							restore: {
								show: true
							},
							saveAsImage: {
								show: true
							}
						}
					},
					calculable: true,
					xAxis: [{
						type: 'category',
						data: [""]
					}],
					yAxis: [{
						type: 'value',
						min : umbral_range.min,
						max : umbral_range.max,
						splitArea: {
							show: true
						}
					}],
					series: series
			});
			myChartLotes.setTheme('infographic');
			graphic.lote = true;
		}
		window.onresize = function(){
			myChartPrincial.resize();
			if(graphic.labor)
				myChartLabor.resize();
			if(graphic.lote)
				myChartLotes.resize();
		}
	}else if(step == 1){
		if(options.main){
			var label_principal = {
				selected : {},
				data : []
			};
			var series_principal = [] , count = 0;
			series_principal.push({
				name : options.label_principal[0],
				type : "line",
				data : [],
				markLine : {
					data : [
						[
							{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
							{xAxis: 52, yAxis: umbral}
						],
					]
				}
			});

			// label_principal = options.label_principal.amap(function(item){return item});
			label_principal.selected[series_principal[0].name] = true;
			label_principal.data.push(series_principal[0].name)
			for(var x in options.series){
				series_principal.push({
					name : x,
					type : "line",
					data : []
				});
				label_principal.selected[x] = false;
				label_principal.data.push(x);
				for(var i in options.categories){
					if(!isNaN(parseFloat(options.main[options.categories[i]])) && count == 0){
						series_principal[0].data.push(new Number(options.main[options.categories[i]]).toFixed(2))
					}else{
						series_principal[0].data.push('-')
					}

					if(!isNaN(parseFloat(options.series[x][options.categories[i]]))){
						series_principal[(count + 1)].data.push(new Number(options.series[x][options.categories[i]]).toFixed(2))
					}else{
						series_principal[(count + 1)].data.push('-')
					}
				}
				count++;
			}
			myChartPrincial = echarts.init(document.getElementById('echarts_bar') , 'infographic');
			myChartPrincial.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: label_principal,
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: true
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: options.categories,
				}],
				yAxis: [{
					axisLine : {onZero: false},
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					},
				}],
				series: series_principal
			});
			myChartPrincial.setTheme('infographic');
		}

		var graphic = {
			labor : false,
			lote : false
		};

		if(options.data_labores){
			var label = [] , series = [] , labor = "" , count = 0, categories = [];
				// var back = ["#b5c334"];
			// label.push("UMBRAL");
			categories = options.data_labores.labels;
			for (var p in options.data_labores) {
				if(p != "labels"){
					if (options.data_labores.hasOwnProperty(p)) {
						// series[0].data.push({value : umbral});)
						if(labor != options.data_labores[p].labor){
							label.push(options.data_labores[p].labor);
							labor = options.data_labores[p].labor;
							series[count] = {
								name : options.data_labores[p].labor ,
								type : "bar",
								data : [],
							}
							if(count == 0){
								series[count].markLine = {
									data : [
										[
											{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
											{xAxis: 12, yAxis: umbral}
										],
									]
								}
							}
							count++;
						}
						series[(count-1)].data.push({value : new Number(options.data_labores[p].promedio).toFixed(2)});
					}
				}
			}
			/*=========================================
			=            GRAFICA PRINCIPAL            =
			=========================================*/

			/*=====  End of GRAFICA PRINCIPAL  ======*/

			var myChartLabor = echarts.init(document.getElementById('echarts_bar_labores') , 'green');
			myChartLabor.setOption({
				title : {
					text: 'Labores',
					subtext: 'Promedios Labores',
					x:'center'
				},
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					data: label,
					y : 'bottom'
				},
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: false
						},
						magicType: {
							show: false,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [
					{
						type: 'category',
						data: [""],
						position: 'bottom',
					}
				],
				yAxis: [{
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series
			});

			myChartLabor.setTheme('infographic');
			graphic.labor = true;
		}
		if(options.fincas){
			var label = [] , series = [] , labor = "" , count = 0 , promedio = [] , label_promedio = [] , tpromedio = 0;
			var data = {
						data : [
							[
								{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
								{xAxis: 12, yAxis: umbral}
							],
						]
					}
			options.fincas.series[0].markLine = data;

			var myChartLotes = echarts.init(document.getElementById('echarts_bar_lotes') , 'green');
			myChartLotes.setOption({
					title : {
						text: 'Fincas',
						subtext: 'Promedios Fincas',
						x:'center'
					},
					tooltip: {
						trigger: 'axis'
					},
					legend: {
						data: options.fincas.label,
						y : 'bottom'
					},
					toolbox: {
						show: true,
						feature: {
							mark: {
								show: false
							},
							dataView: {
								show: false,
								readOnly: false
							},
							magicType: {
								show: false,
								type: ['line', 'bar']
							},
							restore: {
								show: true
							},
							saveAsImage: {
								show: true
							}
						}
					},
					calculable: true,
					xAxis: [{
						type: 'category',
						data: [""]
					}],
					yAxis: [{
						type: 'value',
						min : umbral_range.min,
						max : umbral_range.max,
						splitArea: {
							show: true
						}
					}],
					series: options.fincas.series
			});
			myChartLotes.setTheme('infographic');
			graphic.lote = true;
		}
		window.onresize = function(){
			myChartPrincial.resize();
			if(graphic.labor)
				myChartLabor.resize();
			if(graphic.lote)
				myChartLotes.resize();
		}
	}else if(step == 2){
		if(options.main){
			var label_principal = {
				selected : {},
				data : []
			};
			var series_principal = [] , count = 0;
			series_principal.push({
				name : options.label_principal[0],
				type : "line",
				data : [],
				markLine : {
					data : [
						[
							{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
							{xAxis: 52, yAxis: umbral}
						],
					]
				}
			});

			// label_principal = options.label_principal.amap(function(item){return item});
			label_principal.selected[series_principal[0].name] = true;
			label_principal.data.push(series_principal[0].name)
			for(var x in options.series){
				series_principal.push({
					name : x,
					type : "line",
					data : []
				});
				label_principal.selected[x] = false;
				label_principal.data.push(x);
				for(var i in options.categories){
					if(!isNaN(parseFloat(options.main[options.categories[i]])) && count == 0){
						series_principal[0].data.push(new Number(options.main[options.categories[i]]).toFixed(2))
					}else{
						series_principal[0].data.push("-")
					}


					if(!isNaN(parseFloat(options.series[x][options.categories[i]]))){
						series_principal[(count + 1)].data.push(new Number(options.series[x][options.categories[i]]).toFixed(2))
					}
					else{
						series_principal[(count + 1)].data.push('-')
					}
				}
				count++;
			}
			myChartPrincial = echarts.init(document.getElementById('echarts_bar') , 'infographic');
			myChartPrincial.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: label_principal,
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: true
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: options.categories,
				}],
				yAxis: [{
					axisLine : {onZero: false},
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series_principal
			});
			myChartPrincial.setTheme('infographic');
		}

		var graphic = {
			labor : false,
			lote : false,
			supervisor : false
		};

		if(options.data_labores){
			var label = [] , series = [] , labor = "" , count = 0, categories = [];
				// var back = ["#b5c334"];
			// label.push("UMBRAL");
			// categories = options.data_labores.labels;
			for (var p in options.data_labores) {
				if(p != "labels"){
					if (options.data_labores.hasOwnProperty(p)) {
						// series[0].data.push({value : umbral});)
						if(labor != options.data_labores[p].labor){
							label.push(options.data_labores[p].labor);
							labor = options.data_labores[p].labor;
							series[count] = {
								name : options.data_labores[p].labor ,
								type : "bar",
								data : [],
							}
							if(count == 0){
								series[count].markLine = {
									data : [
										[
											{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
											{xAxis: 12, yAxis: umbral}
										],
									]
								}
							}
							count++;
						}
						series[(count-1)].data.push({value : new Number((options.data_labores[p].promedio)).toFixed(2)});
					}
				}
			}
			/*=========================================
			=            GRAFICA PRINCIPAL            =
			=========================================*/

			/*=====  End of GRAFICA PRINCIPAL  ======*/

			var myChartLabor = echarts.init(document.getElementById('echarts_bar_labores') , 'green');
			myChartLabor.setOption({
				title : {
					text: 'Labores',
					subtext: 'Promedios Labores',
					x:'center'
				},
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					data: label,
					y : 'bottom'
				},
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: false
						},
						magicType: {
							show: false,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [
					{
						type: 'category',
						data: [""],
						position: 'bottom',
					}
				],
				yAxis: [{
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series
			});

			myChartLabor.setTheme('infographic');
			graphic.labor = true;
		}
		if(options.lotes){
			var label = [] , series = [] , labor = "" , count = 0 , promedio = [] , label_promedio = [] , tpromedio = 0;
			var data = {
						data : [
							[
								{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
								{xAxis: 12, yAxis: umbral}
							],
						]
					}
			options.lotes.series[0].markLine = data;

			var myChartLotes = echarts.init(document.getElementById('echarts_bar_lotes') , 'green');
			myChartLotes.setOption({
					title : {
						text: 'Lotes',
						subtext: 'Promedios Lotes',
						x:'center'
					},
					tooltip: {
						trigger: 'axis'
					},
					legend: {
						data: options.lotes.label,
						y : 'bottom'
					},
					toolbox: {
						show: true,
						feature: {
							mark: {
								show: false
							},
							dataView: {
								show: false,
								readOnly: false
							},
							magicType: {
								show: false,
								type: ['line', 'bar']
							},
							restore: {
								show: true
							},
							saveAsImage: {
								show: true
							}
						}
					},
					calculable: true,
					xAxis: [{
						type: 'category',
						data: [""]
					}],
					yAxis: [{
						type: 'value',
						min : umbral_range.min,
						max : umbral_range.max,
						splitArea: {
							show: true
						}
					}],
					series: options.lotes.series
			});
			myChartLotes.setTheme('infographic');
			graphic.lote = true;
		}
		if(options.fincas){

			var label = [] , series = [] , labor = "" , count = 0 ,count_lote = 0, promedio_lote = [] , label_promedio = [] , tpromedio = 0 , promedio = [] ,sum_promedio = 0;
			for(var finca in options.fincas){
				if (options.fincas.hasOwnProperty(finca)) {
					label.push(finca);
					count_lote = 0;
					sum_promedio = 0;
					promedio = [];
					for (var key in options.fincas[finca].lote) {
						if (options.fincas[finca].lote.hasOwnProperty(key)) {
							promedio_lote[count_lote] = [];
							promedio.push(key);
							label_promedio = options.fincas[finca].lote[key].labores.map(function(num) { promedio_lote[count_lote].push(num.promedio); return num.promedio; });
							tpromedio = promedio_lote[count_lote].reduce(function(a, b) { return parseFloat(a) + parseFloat(b); });
							sum_promedio += new Number((tpromedio / promedio_lote[count_lote].length)).toFixed(2);
							count_lote++;
						}
					}
					series[count] = {
						name : finca,
						type : "bar",
						data :  [new Number((sum_promedio / promedio.length)).toFixed(2)]
					}
					if(count == 0){
						series[count].markLine = {
							data : [
								[
									{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
									{xAxis: 12, yAxis: umbral}
								],
							]
						}
					}
					count++;
				}
			}

			var myChartLotes = echarts.init(document.getElementById('echarts_bar_lotes') , 'green');
			myChartLotes.setOption({
					title : {
						text: 'Lotes',
						subtext: 'Promedios Lotes',
						x:'center'
					},
					tooltip: {
						trigger: 'axis'
					},
					legend: {
						data: label,
						y : 'bottom'
					},
					toolbox: {
						show: true,
						feature: {
							mark: {
								show: false
							},
							dataView: {
								show: false,
								readOnly: false
							},
							magicType: {
								show: false,
								type: ['line', 'bar']
							},
							restore: {
								show: true
							},
							saveAsImage: {
								show: true
							}
						}
					},
					calculable: true,
					xAxis: [{
						type: 'category',
						data: [""]
					}],
					yAxis: [{
						type: 'value',
						min : umbral_range.min,
						max : umbral_range.max,
						splitArea: {
							show: true
						}
					}],
					series: series
			});
			myChartLotes.setTheme('infographic');
			graphic.lote = true;
		}
		if(options.hasOwnProperty("personal")){
			var label = [] , series = [] , labor = "" , count = 0 , promedio = [] , label_promedio = [] , tpromedio = 0;
			var data = {
						data : [
							[
								{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
								{xAxis: 12, yAxis: umbral}
							],
						]
					}
			options.personal.series[0].markLine = data;

			var myChartSupervisor = echarts.init(document.getElementById('echarts_bar_supervisor') , 'green');
			myChartSupervisor.setOption({
					title : {
						text: 'Personal',
						subtext: 'Promedios Personal',
						x:'center'
					},
					tooltip: {
						trigger: 'axis'
					},
					legend: {
						data: options.personal.label,
						y : 'bottom'
					},
					toolbox: {
						show: true,
						feature: {
							mark: {
								show: false
							},
							dataView: {
								show: false,
								readOnly: false
							},
							magicType: {
								show: false,
								type: ['line', 'bar']
							},
							restore: {
								show: true
							},
							saveAsImage: {
								show: true
							}
						}
					},
					calculable: true,
					xAxis: [{
						type: 'category',
						data: [""]
					}],
					yAxis: [{
						type: 'value',
						min : umbral_range.min,
						max : umbral_range.max,
						splitArea: {
							show: true
						}
					}],
					series: options.personal.series
			});
			myChartSupervisor.setTheme('infographic');
			graphic.supervisor = true;
		}
		window.onresize = function(){
			myChartPrincial.resize();
			if(graphic.labor)
				myChartLabor.resize();
			if(graphic.lote)
				myChartLotes.resize();
			if(graphic.supervisor)
				myChartSupervisor.resize();
		}
	}else if(step == 3){
		if(options.lote.labores){
			var info = options.lote.labores;
			var label = [] , series = [] , labor = "" , count = 0;
			for (var key in info) {
				if (info.hasOwnProperty(key)) {
					label.push(key);
					if(labor != key){
						labor = key;
						series[count] = {
							name : key,
							type : "bar",
							data : []
						};
						if(count == 0){
							series[count].markLine = {
								data : [
									[
										{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
										{xAxis: 12, yAxis: umbral}
									],
								]
							}
						}
						count++;
					}
					series[(count-1)].data.push({value : new Number(info[key].promedio).toFixed(2)});
				}
			}
			var myChartLaborLote = echarts.init(document.getElementById('echarts_bar_labores_lotes') , 'green');
			myChartLaborLote.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					data: label,
					y : 'bottom'
				},
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: false
						},
						magicType: {
							show: false,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: [""]
				}],
				yAxis: [{
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series
			});
		}
		if(options.lote.operador){
			var info = options.lote.operador;
			var label = [] , series = [] , labor = "" , count = 0;
			for (var key in info) {
				if (info.hasOwnProperty(key)) {
					label.push(key);
					if(labor != key){
						labor = key;
						series[count] = {
							name : key,
							type : "bar",
							data : []
						};
						if(count == 0){
							series[count].markLine = {
								data : [
									[
										{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
										{xAxis: 12, yAxis: umbral}
									],
								]
							}
						}
						count++;
					}
					series[(count-1)].data.push({value : new Number((info[key].promedio / info[key].avg)).toFixed(2)});
				}
			}

			var myChartLaborLoteOperador = echarts.init(document.getElementById('echarts_bar_operador') , 'green');
			myChartLaborLoteOperador.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					data: label,
					y : 'bottom'
				},
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: false
						},
						magicType: {
							show: false,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: [""]
				}],
				yAxis: [{
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series
			});

			myChartLaborLoteOperador.setTheme('infographic');
		}
		if(options.main){
			var label_principal = {
				selected : {},
				data : []
			};
			var series_principal = [] , count = 0;
			series_principal.push({
				name : options.label_principal[0],
				type : "line",
				data : [],
				markLine : {
					data : [
						[
							{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
							{xAxis: 52, yAxis: umbral}
						],
					]
				}
			});

			// label_principal = options.label_principal.amap(function(item){return item});
			label_principal.selected[series_principal[0].name] = true;
			label_principal.data.push(series_principal[0].name)
			for(var x in options.series){
				if(options.label_principal[x]){
					series_principal.push({
						name : x,
						type : "line",
						data : []
					});
					label_principal.selected[x] = false;
					label_principal.data.push(x);
					for(var i in options.categories){
						if(!isNaN(parseFloat(options.main[options.categories[i]])) && count == 0){
							series_principal[0].data.push(new Number(options.main[options.categories[i]]).toFixed(2))
						}else{
							series_principal[0].data.push('-')
						}

						if(!isNaN(parseFloat(options.series[x][options.categories[i]]))){
							series_principal[(count + 1)].data.push(new Number(options.series[x][options.categories[i]]).toFixed(2))
						}else{
							series_principal[(count + 1)].data.push('-')
						}
					}
					count++;
				}
			}
			myChartPrincial = echarts.init(document.getElementById('echarts_bar') , 'infographic');
			myChartPrincial.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: label_principal,
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: true
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: options.categories,
				}],
				yAxis: [{
					axisLine : {onZero: false},
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					},
				}],
				series: series_principal
			});
			myChartPrincial.setTheme('infographic');
		}

		// // myChartPrincial.setTheme('infographic');
		myChartLaborLote.setTheme('infographic');
		// myChartOperador.setTheme('infographic');
		window.onresize = function(){
			myChartPrincial.resize();
			myChartLaborLote.resize();
			myChartLaborLoteOperador.resize();
			// myChartOperador.resize();
		}
	}else if(step == 4){
		var data = [] , label = [] , data2 = [] , label2 = [] , flag = false;
		for (var i = 0; i < options.length; i++) {
			label.push(options[i].causa);
			data.push({value : new Number(options[i].porcentaje).toFixed(2) , name : options[i].causa});
			if(options[i].valor == 0){
				flag = true;
				label2.push(options[i].causa);
				data2.push({value : new Number(options[i].porcentaje).toFixed(2) , name : options[i].causa});
			}
		}
		var myChartLoteLabor = echarts.init(document.getElementById('echarts_bar_lotes_labor') , 'infographic');
		myChartLoteLabor.setOption({
			tooltip : {
				trigger: 'item',
				formatter: "{a} <br/>{b} : {c} ({d}%)"
			},
			legend: {
				orient : 'vertical',
				x : 'left',
				data:label
			},
			calculable : true,
			toolbox: {
				show : true,
				feature : {
					mark : {show: true},
					dataView : {show: false, readOnly: false},
					magicType : {
						show: true,
						type: ['pie', 'funnel'],
						option: {
							funnel: {
								x: '50%',
								width: '100%',
								funnelAlign: 'left',
								max: 1548
							}
						}
					},
					restore : {show: true},
					saveAsImage : {show: true}
				}
			},
			calculable : true,
			series : [
				{
					name:'Causas',
					type:'pie',
					radius : '55%',
					center: ['50%', '60%'],
					data:data
				}
			]
		});

		if(flag){
			$("#casusas_negative").css('display', '');
			var myChartLoteLabor2 = echarts.init(document.getElementById('echarts_bar_lotes_labor2') , 'infographic');
			myChartLoteLabor2.setOption({
				tooltip : {
					trigger: 'item',
					formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
					orient : 'vertical',
					x : 'left',
					data:label2
				},
				calculable : true,
				toolbox: {
					show : true,
					feature : {
						mark : {show: true},
						dataView : {show: false, readOnly: false},
						magicType : {
							show: true,
							type: ['pie', 'funnel'],
							option: {
								funnel: {
									x: '50%',
									width: '100%',
									funnelAlign: 'left',
									max: 1548
								}
							}
						},
						restore : {show: true},
						saveAsImage : {show: true}
					}
				},
				calculable : true,
				series : [
					{
						name:'Causas',
						type:'pie',
						radius : '55%',
						center: ['50%', '60%'],
						data:data2
					}
				]
			});
		}else{
			$("#casusas_negative").css('display', 'none');
		}

		if(options.main){
			var label_principal = {
				selected : {},
				data : []
			};
			var series_principal = [] , count = 0;
			series_principal.push({
				name : options.label_principal[0],
				type : "line",
				data : [],
				markLine : {
					data : [
						[
							{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
							{xAxis: 52, yAxis: umbral}
						],
					]
				}
			});

			// label_principal = options.label_principal.amap(function(item){return item});
			label_principal.selected[series_principal[0].name] = true;
			label_principal.data.push(series_principal[0].name)
			for(var x in options.series){
				if(options.label_principal[x]){
					series_principal.push({
						name : x,
						type : "line",
						data : []
					});
					label_principal.selected[x] = false;
					label_principal.data.push(x);
					for(var i in options.categories){
						if(!isNaN(parseFloat(options.main[options.categories[i]])) && count == 0){
							series_principal[0].data.push(new Number(options.main[options.categories[i]]).toFixed(2))
						}else{
							series_principal[0].data.push("-")
						}


						if(!isNaN(parseFloat(options.series[x][options.categories[i]]))){
							series_principal[(count + 1)].data.push(new Number(options.series[x][options.categories[i]]).toFixed(2))
						}else{
							series_principal[(count + 1)].data.push('-')
						}
					}
				}
				count++;
			}
			myChartPrincial = echarts.init(document.getElementById('echarts_bar_causa_principal') , 'infographic');
			myChartPrincial.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: label_principal,
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: true
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: options.categories,
				}],
				yAxis: [{
					axisLine : {onZero: false},
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series_principal
			});
			myChartPrincial.setTheme('infographic');
		}

		// // myChartPrincial.setTheme('infographic');
		myChartLoteLabor.setTheme('infographic');
		if(flag){
			myChartLoteLabor2.setTheme('infographic');
		}
		// myChartOperador.setTheme('infographic');
		// window.onresize = function(){
		// 	myChartPrincial.resize();
		// 	myChartLaborLote.resize();
		// 	myChartLaborLoteOperador.resize();
		// 	// myChartOperador.resize();
		// }
		// }

		myChartLoteLabor.setTheme('infographic');
			window.onresize = function(){
			myChartPrincial.resize();
			myChartLoteLabor.resize();
			myChartLoteLabor2.resize();
		}
	}else if(step == 5){
		if(options.hasOwnProperty("main")){
			var label_principal = {
				selected : {},
				data : []
			};
			var series_principal = [] , count = 0;
			series_principal.push({
				name : "BONITA",
				type : "line",
				data : [],
				markLine : {
					data : [
						[
							{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
							{xAxis: 52, yAxis: umbral}
						],
					]
				}
			});

			// label_principal = options.label_principal.amap(function(item){return item});
			label_principal.selected[series_principal[0].name] = true;
			label_principal.data.push(series_principal[0].name);

			for(var x in options.labor){
				series_principal.push({
					name : x,
					type : "line",
					data : []
				});
				label_principal.selected[x] = false;
				label_principal.data.push(x);
				for(var i in options.categories){
					if(!isNaN(parseFloat(options.labor[x][options.categories[i]]))){
						series_principal[(series_principal.length - 1)].data.push(new Number(options.labor[x][options.categories[i]]).toFixed(2))
					}else{
						series_principal[(series_principal.length - 1)].data.push("-")
					}
				}
			}

			for(var x in options.series){
				series_principal.push({
					name : x,
					type : "line",
					data : []
				});
				label_principal.selected[x] = false;
				label_principal.data.push(x);
				for(var i in options.categories){
					if(!isNaN(parseFloat(options.main[options.categories[i]])) && count == 0){
						series_principal[0].data.push(new Number(options.main[options.categories[i]]).toFixed(2))
					}else{
						series_principal[0].data.push("-");
					}

					// if(options.labor[options.labor_name].hasOwnProperty(options.categories[i]) && !isNaN(parseFloat(options.labor[options.labor_name][options.categories[i]]))){
					// 	series_principal[1].data.push(new Number(options.labor[options.labor_name][options.categories[i]]).toFixed(2))
					// }else{
					// 	series_principal[1].data.push("-");
					// }

					if(!isNaN(parseFloat(options.series[x][options.categories[i]]))){
						series_principal[(count + 1)].data.push(new Number(options.series[x][options.categories[i]]).toFixed(2))
					}else{
						series_principal[(count + 1)].data.push("-")
					}
				}
				count++;
			}
			myChartPrincial = echarts.init(document.getElementById('echarts_bar') , 'infographic');
			myChartPrincial.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: label_principal,
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: true
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: options.categories,
				}],
				yAxis: [{
					axisLine : {onZero: false},
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series_principal
			});
			myChartPrincial.setTheme('infographic');
		}
		if(options.hasOwnProperty("options_zonas")){
			var series = [] , label = [];
			for (var i = 0; i < options.options_zonas.length; i++) {
				label.push(options.options_zonas[i].nombre);
				series.push({
					name : options.options_zonas[i].nombre,
					type : "bar",
					data : [{value : options.options_zonas[i].promedio}]
				});
				// series.;x
			}
			var myChartZonaLabores = echarts.init(document.getElementById('echarts_zona_labores') , 'infographic');
			myChartZonaLabores.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					data: label,
					y : 'bottom'
				},
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: false
						},
						magicType: {
							show: false,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: [""]
				}],
				yAxis: [{
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series
			});
		}
		if(options.hasOwnProperty("options_porcentaje")){
			var data = [] , label = [];
			for (var i = 0; i < options.options_porcentaje.length; i++) {
				label.push(options.options_porcentaje[i].causa);
				data.push({value : new Number(options.options_porcentaje[i].porcentaje).toFixed(2) , name : options.options_porcentaje[i].causa});
			}
			var myChartCausasLabores = echarts.init(document.getElementById('echarts_causas_labores') , 'infographic');
			myChartCausasLabores.setOption({
				tooltip : {
					trigger: 'item',
					formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
					orient : 'vertical',
					x : 'left',
					data:label
				},
				calculable : true,
				toolbox: {
					show : true,
					feature : {
						mark : {show: true},
						dataView : {show: false, readOnly: false},
						magicType : {
							show: true,
							type: ['pie', 'funnel'],
							option: {
								funnel: {
									x: '50%',
									width: '100%',
									funnelAlign: 'left',
									max: 1548
								}
							}
						},
						restore : {show: true},
						saveAsImage : {show: true}
					}
				},
				calculable : true,
				series : [
					{
						name:'Causas',
						type:'pie',
						radius : '55%',
						center: ['50%', '60%'],
						data:data
					}
				]
			});
		}
		if(options.hasOwnProperty("options_zonas") && options.hasOwnProperty("options_porcentaje")){
			window.onresize = function(){
				myChartZonaLabores.resize();
				myChartCausasLabores.resize();
			}
		}
	}else if(step == 6){
		if(options.hasOwnProperty("main")){
			var label_principal = {
				selected : {},
				data : []
			};
			var series_principal = [] , count = 0;
			series_principal.push({
				name : "BONITA",
				type : "line",
				data : [],
				markLine : {
					data : [
						[
							{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
							{xAxis: 52, yAxis: umbral}
						],
					]
				}
			});

			// label_principal = options.label_principal.amap(function(item){return item});
			label_principal.selected[series_principal[0].name] = true;
			label_principal.data.push(series_principal[0].name);

			for(var x in options.labor){
				series_principal.push({
					name : x,
					type : "line",
					data : []
				});
				label_principal.selected[x] = false;
				label_principal.data.push(x);
				for(var i in options.categories){
					if(!isNaN(parseFloat(options.labor[x][options.categories[i]]))){
						series_principal[(series_principal.length - 1)].data.push(new Number(options.labor[x][options.categories[i]]).toFixed(2))
					}else{
						series_principal[(series_principal.length - 1)].data.push("-")
					}
				}
			}

			for(var x in options.series){
				series_principal.push({
					name : x,
					type : "line",
					data : []
				});
				label_principal.selected[x] = false;
				label_principal.data.push(x);
				for(var i in options.categories){
					if(!isNaN(parseFloat(options.main[options.categories[i]])) && count == 0){
						series_principal[0].data.push(new Number(options.main[options.categories[i]]).toFixed(2))
					}else{
						series_principal[0].data.push("-");
					}

					// if(options.labor[options.labor_name].hasOwnProperty(options.categories[i]) && !isNaN(parseFloat(options.labor[options.labor_name][options.categories[i]]))){
					// 	series_principal[1].data.push(Math.round(options.labor[options.labor_name][options.categories[i]]))
					// }else{
					// 	series_principal[1].data.push("-");
					// }

					if(!isNaN(parseFloat(options.series[x][options.categories[i]]))){
						series_principal[(count + 1)].data.push(new Number(options.series[x][options.categories[i]]).toFixed(2))
					}else{
						series_principal[(count + 1)].data.push("-")
					}
				}
				count++;
			}
			myChartPrincial = echarts.init(document.getElementById('echarts_bar') , 'infographic');
			myChartPrincial.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: label_principal,
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: true
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: options.categories,
				}],
				yAxis: [{
					axisLine : {onZero: false},
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series_principal
			});
			myChartPrincial.setTheme('infographic');
		}
		if(options.hasOwnProperty("options_fincas")){
			var series = [] , label = [];
			for (var i = 0; i < options.options_fincas.length; i++) {
				label.push(options.options_fincas[i].nombre);
				series.push({
					name : options.options_fincas[i].nombre,
					type : "bar",
					data : [{value : options.options_fincas[i].promedio}]
				});
				// series.;x
			}
			var myChartZonaLabores = echarts.init(document.getElementById('echarts_finca_labores') , 'infographic');
			myChartZonaLabores.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					data: label,
					y : 'bottom'
				},
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: false
						},
						magicType: {
							show: false,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: [""]
				}],
				yAxis: [{
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series
			});
		}
		if(options.hasOwnProperty("options_porcentaje")){
			var data = [] , label = [];
			for (var i = 0; i < options.options_porcentaje.length; i++) {
				label.push(options.options_porcentaje[i].causa);
				data.push({value : new Number(options.options_porcentaje[i].porcentaje).toFixed(2) , name : options.options_porcentaje[i].causa});
			}
			var myChartCausasLabores = echarts.init(document.getElementById('echarts_causas_labores') , 'infographic');
			myChartCausasLabores.setOption({
				tooltip : {
					trigger: 'item',
					formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
					orient : 'vertical',
					x : 'left',
					data:label
				},
				calculable : true,
				toolbox: {
					show : true,
					feature : {
						mark : {show: true},
						dataView : {show: false, readOnly: false},
						magicType : {
							show: true,
							type: ['pie', 'funnel'],
							option: {
								funnel: {
									x: '50%',
									width: '100%',
									funnelAlign: 'left',
									max: 1548
								}
							}
						},
						restore : {show: true},
						saveAsImage : {show: true}
					}
				},
				calculable : true,
				series : [
					{
						name:'Causas',
						type:'pie',
						radius : '55%',
						center: ['50%', '60%'],
						data:data
					}
				]
			});
		}
		if(options.hasOwnProperty("options_fincas") && options.hasOwnProperty("options_porcentaje")){
			window.onresize = function(){
				myChartZonaLabores.resize();
				myChartCausasLabores.resize();
			}
		}
	}else if(step == 7){
		if(options.hasOwnProperty("main")){
			var label_principal = {
				selected : {},
				data : []
			};
			var series_principal = [] , count = 0;
			series_principal.push({
				name : "BONITA",
				type : "line",
				data : [],
				markLine : {
					data : [
						[
							{name: 'Umbral', value: umbral, xAxis: -1, yAxis: umbral},
							{xAxis: 52, yAxis: umbral}
						],
					]
				}
			});

			// label_principal = options.label_principal.amap(function(item){return item});
			label_principal.selected[series_principal[0].name] = true;
			label_principal.data.push(series_principal[0].name);

			for(var x in options.labor){
				series_principal.push({
					name : x,
					type : "line",
					data : []
				});
				label_principal.selected[x] = false;
				label_principal.data.push(x);
				for(var i in options.categories){
					if(!isNaN(parseFloat(options.labor[x][options.categories[i]]))){
						series_principal[(series_principal.length - 1)].data.push(new Number(options.labor[x][options.categories[i]]).toFixed(2))
					}else{
						series_principal[(series_principal.length - 1)].data.push("-")
					}
				}
			}

			for(var x in options.series){
				for(var i in options.categories){
					if(!isNaN(parseFloat(options.main[options.categories[i]])) && count == 0){
						series_principal[0].data.push(new Number(options.main[options.categories[i]]).toFixed(2))
					}else{
						series_principal[0].data.push("-");
					}
				}
				count++;
			}
			myChartPrincial = echarts.init(document.getElementById('echarts_bar') , 'infographic');
			myChartPrincial.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: label_principal,
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: true
						},
						magicType: {
							show: true,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: options.categories,
				}],
				yAxis: [{
					axisLine : {onZero: false},
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series_principal
			});
			myChartPrincial.setTheme('infographic');
		}
		if(options.hasOwnProperty("options_lotes")){
			var series = [] , label = [];
			for (var i = 0; i < options.options_lotes.length; i++) {
				label.push(options.options_lotes[i].nombre);
				series.push({
					name : options.options_lotes[i].nombre,
					type : "bar",
					data : [{value : options.options_lotes[i].promedio}]
				});
				// series.;x
			}
			var myChartZonaLabores = echarts.init(document.getElementById('echarts_lote_labores') , 'infographic');
			myChartZonaLabores.setOption({
				tooltip: {
					trigger: 'axis'
				},
				legend: {
					data: label,
					y : 'bottom'
				},
				toolbox: {
					show: true,
					feature: {
						mark: {
							show: false
						},
						dataView: {
							show: false,
							readOnly: false
						},
						magicType: {
							show: false,
							type: ['line', 'bar']
						},
						restore: {
							show: true
						},
						saveAsImage: {
							show: true
						}
					}
				},
				calculable: true,
				xAxis: [{
					type: 'category',
					data: [""]
				}],
				yAxis: [{
					type: 'value',
					min : umbral_range.min,
					max : umbral_range.max,
					splitArea: {
						show: true
					}
				}],
				series: series
			});
		}
		if(options.hasOwnProperty("options_porcentaje")){
			var data = [] , label = [];
			for (var i = 0; i < options.options_porcentaje.length; i++) {
				label.push(options.options_porcentaje[i].causa);
				data.push({value : new Number(options.options_porcentaje[i].porcentaje).toFixed(2) , name : options.options_porcentaje[i].causa});
			}
			var myChartCausasLabores = echarts.init(document.getElementById('echarts_causas_labores') , 'infographic');
			myChartCausasLabores.setOption({
				tooltip : {
					trigger: 'item',
					formatter: "{a} <br/>{b} : {c} ({d}%)"
				},
				legend: {
					orient : 'vertical',
					x : 'left',
					data:label
				},
				calculable : true,
				toolbox: {
					show : true,
					feature : {
						mark : {show: true},
						dataView : {show: false, readOnly: false},
						magicType : {
							show: true,
							type: ['pie', 'funnel'],
							option: {
								funnel: {
									x: '50%',
									width: '100%',
									funnelAlign: 'left',
									max: 1548
								}
							}
						},
						restore : {show: true},
						saveAsImage : {show: true}
					}
				},
				calculable : true,
				series : [
					{
						name:'Causas',
						type:'pie',
						radius : '55%',
						center: ['50%', '60%'],
						data:data
					}
				]
			});
		}
		if(options.hasOwnProperty("options_lotes") && options.hasOwnProperty("options_porcentaje")){
			window.onresize = function(){
				myChartZonaLabores.resize();
				myChartCausasLabores.resize();
			}
		}
	}
}

app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
    	if(item.hasOwnProperty("promedio")){
    		item.promedio = parseFloat(item.promedio);
    	}
    	if(item.hasOwnProperty("porcentaje")){
    		if(!isNaN(parseFloat(item.porcentaje)))
    			item.porcentaje = parseFloat(item.porcentaje);
    	}
		if(item.hasOwnProperty("porcentaje2")){
    		if(!isNaN(parseFloat(item.porcentaje2)))
    			item.porcentaje2 = parseFloat(item.porcentaje2);
    	}

      filtered.push(item);
    });
    filtered.sort(function (a, b) {
		//alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.filter('orderObjectBy2', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
    	if(item.hasOwnProperty("promedio")){
    		item.promedio = parseFloat(item.promedio);
    	}
    	if(item.hasOwnProperty("porcentaje")){
    		if(!isNaN(parseFloat(item.porcentaje)))
    			item.porcentaje = parseFloat(item.porcentaje);
    	}
		if(item.hasOwnProperty("porcentaje2")){
    		if(!isNaN(parseFloat(item.porcentaje2)))
    			item.porcentaje2 = parseFloat(item.porcentaje2);
    	}

      filtered.push(item);
    });
    filtered.sort(function (a, b) {
		if(!reverse){
			if(a[field]==null){a[field]='900.00000'}
			if(b[field]==null){b[field]='900.00000'}
			if(a[field]=='0.00000'){a[field]='900.00000'}
			if(b[field]=='0.00000'){b[field]='900.00000'}
		}
		else{
			if(a[field]==null){a[field]='0.00000'}
			if(b[field]==null){b[field]='0.00000'}
			if(a[field]=='900.00000'){a[field]='0.00000'}
			if(b[field]=='900.00000'){b[field]='0.00000'}
		}
		// alert(b[field]);
      return (a[field] - b[field]);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [],
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});

app.filter('float', function() {
    return function(input) {
      return parseFloat(input);
    };
});

app.factory('Excel',function($window){
        var uri='data:application/vnd.ms-excel;base64,',
            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
        return {
            tableToExcel:function(tableId,worksheetName){
                var table=$(tableId),
                    ctx={worksheet:worksheetName,table:table.html()},
                    href=uri+base64(format(template,ctx));
                return href;
            }
        };
    })

app.controller('reportes', ['$scope','$http','$interval','client','$controller','Excel','$timeout' ,'$window' , '$filter', function($scope,$http,$interval,client, $controller,Excel , $timeout,$window , $filter){
	var isPushStep4 = false;
	$scope.id_company = 0;
	$scope.umbrales = {};
	$scope.cache = {};


	var umbral_range = {
		min : 0 ,
		max : 100		
	}

	$scope.wizardStep = {
		params : {
			idZona : 1,
			idFinca : 0,
			idLote : 0,
			idLabor : 0,
			idZonaLabor : 0,
			idFincaLabor : 0,
			idLoteLabor : 0,
			fecha_inicial :moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD'),
			fecha_final : moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD'),
		},
		inicialPath : 'phrapi/Reportes/index',
		step : 0,
		path : [],
		templatePath : [],
		nocache : function(data){
			var self = this;
			client.post(self.inicialPath , function(r,b){
				if(r){
					self.params.idZona = 1;
					self.params.idFinca = 1;
					self.step = 2;
					if(r.id_company == 5 || r.id_company == 8){
						self.params.idZona = 0;
						self.params.idFinca = 0;
						self.step = 0;
					}else if(r.id_company == 4 || r.id_company == 7 || r.id_company == 3){
						self.params.idZona = 1;
						self.params.idFinca = 0;
						self.step = 1;
					}

					self.path.push('phrapi/Reportes/zonas');
					self.path.push('phrapi/Reportes/fincas');
					self.path.push('phrapi/Reportes/lotes');
					self.path.push('phrapi/Reportes/labores');
					self.path.push('phrapi/Reportes/causas');
					/*----------  LABORES  ----------*/
					self.path.push('phrapi/labores/zonas');
					self.path.push('phrapi/labores/fincas');
					self.path.push('phrapi/labores/lotes');
					/*----------  LABORES  ----------*/
					self.templatePath.push('/views/step5.html?' +Math.random());
					self.templatePath.push('/views/step4.html?' +Math.random());
					self.templatePath.push('/views/step3.html?' +Math.random());
					self.templatePath.push('/views/step2.html?' +Math.random());
					self.templatePath.push('/views/step1.html?' +Math.random());
					/*----------  LABORES  ----------*/
					self.templatePath.push('/views/labor_zona.html?' +Math.random());
					self.templatePath.push('/views/labor_finca.html?' +Math.random());
					self.templatePath.push('/views/labor_lote.html?' +Math.random());
					/*----------  LABORES  ----------*/

					$scope.id_company = r.id_company;
					var data = self.params;
				}
			});
		}

	}

	$scope.tags = {
		lote : {
			valor : 0,
			label : ""
		},
		labor : {
			valor : 0,
			label : ""
		},
		operador : {
			valor : 0,
			label : ""
		},
		auditoria : {
			valor : 0
		}
	}

	$scope.interval = 140000;
	$scope.modo = "mes";

	$scope.wizardData = {
		data : [],
		data_labores : [],
		data_labores_lotes : [],
		labores : [],
		main : [],
		lote_name : [],
		labor_name : [],
	}

	$scope.status_tittle = "";

	$scope.tittles = {
		lote : "Todos",
		labor : "Todos",
		getTittle : function(){
			var response = "Auditoría de Labores Agrícolas";
			if(this.lote == "Todos" && this.labor == "Todos"){
				response = "Auditoría de Labores Agrícolas";
			}else{
				response = "Lote " + this.lote +" - Labor "+ this.labor;
			}

			return response;
		}
	}

	$scope.exportToExcel=function(tableId){ // ex: '#my-table'
		var exportHref=Excel.tableToExcel(tableId,'sheet name');
		$timeout(function(){location.href=exportHref;},100); // trigger download
	}

	$scope.createPDF = function (){
        html2canvas($("#reportes_all"), {
            onrendered: function(canvas) {;
              var img = canvas.toDataURL("image/png"),
                doc = new jsPDF("p", "mm", "a4");
                doc.addImage(img, 'PNG', 0, 0 , 210, 300);
                doc.save('ficha.pdf');
                // $(".page-bar").css('display', '');
            }
        });
    }

	$scope.demoFromHTML = function() {
        // $(".page-bar").css('display', 'none');
         $scope.createPDF();
    }

	$scope.exportToPdf=function(tableId){
		$(".actions").css('display', 'none');
        var pdf = new jsPDF("p", "mm", "a4");
        // source can be HTML-formatted string, or a reference
        // to an actual DOM element from which the text will be scraped.
        source = $(tableId)[0];

        // we support special element handlers. Register them with jQuery-style
        // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
        // There is no support for any other type of selectors
        // (class, of compound) at this time.
        specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#bypassme': function(element, renderer) {
                // true = "handled elsewhere, bypass text extraction"
                return true
            }
        };
        margins = {
            top: 20,
            bottom: 20,
            left: 5,
            width: 80
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
                source, // HTML string or DOM elem ref.
                margins.left, // x coord
                margins.top, {// y coord
                    'width': margins.width, // max width of content on PDF
                    'elementHandlers': specialElementHandlers
                },
        function(dispose) {
            // dispose: object with X, Y of the last line add to the PDF
            //          this allow the insertion of new lines after html
            $(".actions").css('display', '');
            pdf.save(tableId+'.pdf');
        }
        , margins);
	}

	$scope.init = function(){
		$interval($scope.loadExternal, $scope.interval);
	}
	$scope.$watch('search', function(){

	});
	$scope.changeRangeDate = function(data){
		if(data){
			$scope.wizardStep.params.fecha_inicial = data.hasOwnProperty("first_date") ? data.first_date : $scope.wizardStep.params.fecha_inicial;
			$scope.wizardStep.params.fecha_final = data.hasOwnProperty("second_date") ? data.second_date : $scope.wizardStep.params.fecha_final;
			$scope.loadExternal($scope.modo);
		}
	}

	$scope.changeStep = function(step , idZona ,idFinca ,idLote , idLabor , idZonaLabor , idFincaLabor , idLoteLabor){
		$scope.wizardStep.step = step;
		$scope.wizardStep.params.idZona = idZona || 0;
		$scope.wizardStep.params.idFinca = idFinca || 0;
		$scope.wizardStep.params.idLote = idLote || 0;
		$scope.wizardStep.params.idLabor = idLabor || 0;
		$scope.wizardStep.params.idZonaLabor = idZonaLabor || 0;
		$scope.wizardStep.params.idFincaLabor = idFincaLabor || 0;
		$scope.wizardStep.params.idLoteLabor = idLoteLabor || 0;
		// $scope.tittles.lote = lote || 'Todos';
		// $scope.tittles.labor = labor || 'Todos';
		// $scope.wizardStep.templatePath[step] += "?"+Math.random();
	}

	$scope.loadExternal = function(modo){
		// $scope.modo = modo || "mes";
		if($scope.wizardStep.step != 0) $scope.startGallery = true;
		if($scope.wizardStep.path[$scope.wizardStep.step] != ""){
			var data = $scope.wizardStep.params;

			client.post($scope.wizardStep.path[$scope.wizardStep.step] , $scope.getDetails , data );

			// client.post('http://procesos-iq.com:3000/zonas/', function(r,b){

			// } , data , {xpublic : '289150995ed28e8860f9161d3cc9f259'});
			// saber si es cliente 4
			// client.post($scope.wizardStep.path[$scope.wizardStep.step] , function(r , b){
			// 	if(r.id_company != 4 || r.id_company != 5)
			// 		$scope.wizardStep.params.idFinca = 1;
			// 	else
			// 		$scope.wizardStep.params.idFinca = 0;

			// 	// inicio
			// 	var data = $scope.wizardStep.params;
			// 	 // $scope.getDetails(r , )

			// }, data);
		}
	}

	$scope.clearCache = function(modo){
		$scope.modo = modo || "mes";
		$scope.getDetails($scope.cache , function(){});
	}

	$scope.getDetails = function(r , b){
		b();
		if(r){
			if($scope.wizardStep.step <= 4){
				var options = angular.copy(r.causas) || {};
				var OptionsPrincipal = r.principal || {};
				$scope.id_company = r.id_company;
				$scope.wizardData.data =  angular.copy(r.causas) || [];
				$scope.wizardData.main = r.main || [];
				$scope.wizardData.lote_name = r.lote_name || [];
				$scope.wizardData.data_labores = r.data_labores || [];
				$scope.wizardData.data_labores_lotes = r.data_labores_lotes || [];
				$scope.umbrales = r.umbrals || {};
				options.id_company = parseInt(r.id_company);

				if(options){
					options.data_labores = {};
					var promedio_tipo = Object.keys($scope.wizardData.data_labores);
					var data = $scope.wizardData.data_labores.map(function(data){
						angular.extend(options.data_labores , data);
					});
					options.main = $scope.wizardData.main;
					options.lote_name = $scope.wizardData.lote_name;
					options.labor_name = r.labor_name || "";
				}
				if(r.hasOwnProperty("causas") && r.causas.hasOwnProperty("lote") && r.causas.lote.hasOwnProperty("labores")){
					var loteLabores = {}
					var copyCausas = angular.copy($scope.wizardData.data.lote.labores);
					var data = copyCausas.map(function(data){
						angular.extend(loteLabores , data);
					});
					options.lote.labores = angular.copy(loteLabores);
					$scope.wizardData.data = r.causas || [];
				}
				if(r.labores){
					var labores = r.labores.filter(function(item, pos) {
					    return  r.labores.indexOf(item) == pos;
					});

					$scope.wizardData.labores = labores;
				}
				if(r.hasOwnProperty("causas") && r.causas.hasOwnProperty("zonas")){
					options.zonas = {
						series : [],
						label : []
					};
					var zonas = $scope.wizardData.data.zonas;
					for(var i in zonas){
						if(zonas[i].hasOwnProperty("nombre")){
							options.zonas.label.push(zonas[i].nombre)
							options.zonas.series.push({
								name : zonas[i].nombre,
								type : "bar",
								data : [$filter('number')(zonas[i].promedio, 2)]
							});
						}
					}
				}
				if(r.hasOwnProperty("causas") && r.causas.hasOwnProperty("fincas")){
					options.fincas = {
						series : [],
						label : []
					};
					var fincas = $scope.wizardData.data.fincas;
					for(var i in fincas){
						if(fincas[i].hasOwnProperty("nombre")){
							options.fincas.label.push(fincas[i].nombre)
							options.fincas.series.push({
								name : fincas[i].nombre,
								type : "bar",
								data : [$filter('number')(fincas[i].promedio, 2)]
							});
						}
					}
				}
				if(r.hasOwnProperty("causas") && r.causas.hasOwnProperty("lote")){
					options.lotes = {
						series : [],
						label : []
					};
					var lote = $scope.wizardData.data.lote;
					for(var i in lote){
						if(lote[i].hasOwnProperty("lote")){
							options.lotes.label.push(lote[i].lote)
							options.lotes.series.push({
								name : lote[i].lote,
								type : "bar",
								data : [$filter('number')(lote[i].promedio, 2)]
							});
						}
					}
				}
				if(r.hasOwnProperty("data_labores_zonas")){
					$scope.wizardData.data_labores_lotes = r.data_labores_zonas.zonas || [];
				}
				if(r.hasOwnProperty("data_labores_fincas")){
					$scope.wizardData.filterLabores = r.filterLabores || [];
					$scope.wizardData.data_labores_lotes = r.data_labores_fincas.fincas || [];
				}
				if(r.hasOwnProperty("data_labores_lotes")){
					$scope.wizardData.filterLabores = r.filterLabores || [];
					$scope.wizardData.data_labores_lotes = r.data_labores_lotes.lotes || [];
				}
				if(r.hasOwnProperty("filterLabores")){
					$scope.wizardData.filterLabores = r.filterLabores || [];
				}
				if(r.hasOwnProperty("categories")){
					options.categories = r.categories || [];
				}
				if(r.hasOwnProperty("zonas") && r.zonas.hasOwnProperty("zonas")){
					options.label_principal = r.zonas.zonas || [];
					if(r.hasOwnProperty("idStepName")){
						actual_graficas[r.idStepName] = r.zonas.zonas || [];
					}
				}
				if(r.hasOwnProperty("zonas") && r.zonas.hasOwnProperty("fincas")){
					if(r.hasOwnProperty("idStepName")){
						actual_graficas[r.idStepName] = r.zonas.fincas || [];
					}
					options.label_principal = r.zonas.fincas || [];					
				}
				if(r.hasOwnProperty("type_labor")){
					$scope.wizardData.type_labor = r.type_labor;
				}
				if(r.hasOwnProperty("personal_type_labor")){
					$scope.wizardData.personal_type_labor = r.personal_type_labor;
				}
				if(r.hasOwnProperty("personal")){
					options.personal = {
						series : [],
						label : []
					};
					var personal = r.personal;
					for(var i in personal){
						if(personal[i].hasOwnProperty("personal")){
							options.personal.label.push(personal[i].personal)
							options.personal.series.push({
								name : personal[i].personal,
								type : "bar",
								data : [$filter('number')(personal[i].promedio, 2)]
							});
						}
					}
				}
				if(r.photos){
					$scope.wizardData.photos = r.photos || [];
					if($scope.startGallery){
						setTimeout(function(){
							$scope.startGallery = false;
				            $('#js-grid-lightbox-gallery').cubeportfolio({
							        filters: '#js-filters-lightbox-gallery1, #js-filters-lightbox-gallery2',
							        loadMore: '#js-loadMore-lightbox-gallery',
							        loadMoreAction: 'click',
							        layoutMode: 'grid',
							        mediaQueries: [{
							            width: 1500,
							            cols: 5
							        }, {
							            width: 1100,
							            cols: 4
							        }, {
							            width: 800,
							            cols: 3
							        }, {
							            width: 480,
							            cols: 2
							        }, {
							            width: 320,
							            cols: 1
							        }],
							        defaultFilter: '*',
							        animationType: 'rotateSides',
							        gapHorizontal: 10,
							        gapVertical: 10,
							        gridAdjustment: 'responsive',
							        caption: 'zoom',
							        displayType: 'sequentially',
							        displayTypeSpeed: 100,

							        // lightbox
							        lightboxDelegate: '.cbp-lightbox',
							        lightboxGallery: true,
							        lightboxTitleSrc: 'data-title',
							        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

							        // singlePageInline
							        singlePageInlineDelegate: '.cbp-singlePageInline',
							        singlePageInlinePosition: 'below',
							        singlePageInlineInFocus: true,
							        singlePageInlineCallback: function(url, element) {
							            // to update singlePageInline content use the following method: this.updateSinglePageInline(yourContent)
							            var t = this;

							            $.ajax({
							                    url: url,
							                    type: 'GET',
							                    dataType: 'html',
							                    timeout: 10000
							                })
							                .done(function(result) {

							                    t.updateSinglePageInline(result);

							                })
							                .fail(function() {
							                    t.updateSinglePageInline('AJAX Error! Please refresh the page!');
							                });
							        },
							});
			            } , 900);
					}
				}
				if(!angular.equals({},options.data_labores)){
					/*----------  LABOR  ----------*/
					var labor = [] , label = [] , value = [] , lowFinca = [];
					labor = options.data_labores.map(function(item){
						label[new Number(item.promedio).toFixed(2)] = item.labor;
						lowFinca[new Number(item.promedio).toFixed(2)] = item.finca;
						value.push(item.promedio);
						return (item.promedio);
					});

					if($scope.tags.labor.label == "")
					if(r.hasOwnProperty("tags")){ //victor
						$scope.tags.labor.valor = r.tags.labor.VALUE;
						$scope.tags.labor.label = r.tags.labor.label;
					}else{ //javi
						$scope.tags.labor.valor = value.amin().value;
						$scope.tags.labor.label = lowFinca[value.amin().value] + "." +label[value.amin().value];
					}
					/*----------  LABOR  ----------*/

					/*----------  LOTE  ----------*/
					if(r.hasOwnProperty("low_lote")){
						var label = "";
						if($scope.id_company == 5 || $scope.id_company == 8){
							label = r.low_lote.zona + ". " + r.low_lote.finca + ". " + r.low_lote.lote;
						}else{
							label = r.low_lote.finca + ". " + r.low_lote.lote;
						}
						$scope.tags.lote.label = label;
						$scope.tags.lote.valor = new Number(r.low_lote.prom).toFixed(2);
					}else{
						if(!r.hasOwnProperty("zonas") && !r.zonas.hasOwnProperty("zonas")){
							var lote = [] , lote_label = [] , lote_value = [];
							lote = r.causas.lote.map(function(item){
								lote_value.push($scope.getPromedio(item, 1));
								return Object.keys(item);
							});
							lote_label = Object.keys(r.causas.lote);
							$scope.tags.lote.valor = lote_value.amin().value;
							$scope.tags.lote.label = lote_label[lote_value.amin().label];
						}else{

						}
					}
					/*----------  LOTE  ----------*/

					/*----------  PERSONAL  ----------*/
					if(r.hasOwnProperty("low_personal")){
						var label = r.low_personal.finca + ". " + r.low_personal.lote + ". " + r.low_personal.personal;
						$scope.tags.operador.label = label;
						$scope.tags.operador.valor = new Number(r.low_personal.promedio).toFixed(2);
					}

					/*----------  PROMEDIO GENERAL  ----------*/
					if(r.hasOwnProperty("prom_general")){
						/*----------  AUDITORIA  ----------*/
						$scope.tags.auditoria.valor = new Number(r.prom_general.promedio).toFixed(2);
					}
					

					if(r.hasOwnProperty("operador")){
						/*----------  OPERADOR AGRICOLA  ----------*/
						var labor_operador = [] , label_operador = [] , value_operador = [];
						labor_operador = r.operador.map(function(item){
							label_operador.push(item.ResponsableLabor);
							value_operador.push((item.promedio / item.avg));
							return (item.promedio / item.avg);
						});
						$scope.tags.operador.valor = value_operador.amin().value;
						$scope.tags.operador.label = label_operador[value_operador.amin().label];
						/*----------  OPERADOR AGRICOLA  ----------*/
					}

					if(!r.hasOwnProperty("zonas") && !r.zonas.hasOwnProperty("zonas")){
						/*----------  AUDITORIA  ----------*/
						$scope.tags.auditoria.valor = lote_value.avg();
					}


					setTimeout(function(){
						$(".counter_tags").counterUp({
							delay: 10,
							time: 1000
						});
					} , 1000);

					options.data_labores.labels = promedio_tipo;
				}
			}

			if($scope.wizardStep.step > 4){
				var options = {};
				if(r.hasOwnProperty("causas")){
					$scope.wizardData.labores.causas =  angular.copy(r.causas) || [];
					options.options_porcentaje =  angular.copy(r.causas) || [];	
				}
				if(r.hasOwnProperty("zonas")){
					$scope.wizardData.labores.zonas =  angular.copy(r.zonas) || [];	
					options.options_zonas =  angular.copy(r.zonas) || [];	
				}
				if(r.hasOwnProperty("fincas")){
					$scope.wizardData.labores.fincas =  angular.copy(r.fincas) || [];	
					options.options_fincas =  angular.copy(r.fincas) || [];	
				}
				if(r.hasOwnProperty("lotes")){
					$scope.wizardData.labores.lotes =  angular.copy(r.lotes) || [];	
					options.options_lotes =  angular.copy(r.lotes) || [];	
				}
				if(r.hasOwnProperty("labor_name")){
					options.labor_name =  angular.copy(r.labor_name) || "LABOR";	
				}
			}


			$scope.cache = angular.copy(r) || {};

			if(r.hasOwnProperty("idStepName")){
				//mes
				var tmp = {};
				if(r.hasOwnProperty("zonas_main") && r.zonas_main.hasOwnProperty("months")){
					tmp = r.zonas_main.months;
				}else{
					tmp = r.zonas.months;
				}
				data_graficas.mes[r.idStepName] = tmp;

				//auditoria
				tmp = {};
				if(r.hasOwnProperty("zonas_main") && r.zonas_main.hasOwnProperty("months")){
					tmp = r.zonas_main.months;
				}else{
					tmp = r.zonas.months;
				}
				data_graficas.auditoria[r.idStepName] = tmp;

				//semana
				tmp = {};
				if(r.hasOwnProperty("zonas_main") && r.zonas_main.hasOwnProperty("weeks")){
					tmp = options.series = r.zonas_main.weeks;
				}else{
					tmp = options.series = r.zonas.weeks;
				}
				data_graficas.semana[r.idStepName] = tmp;
			}
			console.log("data_graficas")
			console.log(data_graficas)	

			// loadScript($scope.wizardStep.step , options , true, OptionsPrincipal);
			if($scope.modo == "mes"){
				options.main = r.main; 
				options.categories = r.categories.months;	

				if(r.hasOwnProperty("zonas") && r.zonas.hasOwnProperty("labores")){
					options.series_labores = r.zonas.labores.months;
				}

				if(r.hasOwnProperty("idStepName")){
					if(r.idStepName == "lotes"){
						options.series = []
						options.series[r.nameFinca] = data_graficas.mes.fincas[r.nameFinca]
					}else if(r.idStepName == "labores"){
						options.series = []
						options.series[r.nameFinca] = data_graficas.mes.fincas[r.nameFinca]
						options.series[r.nameLote] = data_graficas.mes.lotes[r.nameLote]
					}
				}

				if(r.hasOwnProperty("zonas_main") && r.zonas_main.hasOwnProperty("months")){
					options.series = r.zonas_main.months;
				}else{
					if($scope.isArray(options.series)){
						$.each(r.zonas.months, function(key, value){
							options.series[key] = (value)
						})
					}else{
						options.series = r.zonas.months;
					}
				}

				if(r.hasOwnProperty("zonas_main") && r.zonas_main.hasOwnProperty("mes")){
					options.labor = r.zonas_main.mes;
				}
			} 
			if($scope.modo == "auditoria"){
				options.main = r.auditoria; 
				options.categories = r.categories.months;
				
				if(r.hasOwnProperty("zonas") && r.zonas.hasOwnProperty("labores")){
					options.series_labores = r.zonas.labores.months;
				}

				if(r.hasOwnProperty("idStepName")){
					if(r.idStepName == "lotes"){
						options.series = []
						options.series[r.nameFinca] = data_graficas.auditoria.fincas[r.nameFinca]
					}else if(r.idStepName == "labores"){
						options.series = []
						options.series[r.nameFinca] = data_graficas.auditoria.fincas[r.nameFinca]
						options.series[r.nameLote] = data_graficas.auditoria.lotes[r.nameLote]
					}
				}

				if(r.hasOwnProperty("zonas_main") && r.zonas_main.hasOwnProperty("months")){
					options.series = r.zonas_main.months;
				}else{
					if($scope.isArray(options.series)){
						$.each(r.zonas.months, function(key, value){
							options.series[key] = (value)
						})
					}else{
						options.series = r.zonas.months;
					}
				}

				if(r.hasOwnProperty("zonas_main") && r.zonas_main.hasOwnProperty("mes")){
					options.labor = r.zonas_main.mes;
				}
			} 
			if($scope.modo == "semana"){
				options.main = r.week; 
				options.categories = r.categories.weeks;
				
				if(r.hasOwnProperty("zonas") && r.zonas.hasOwnProperty("labores")){
					options.series_labores = r.zonas.labores.weeks;
				}

				if(r.hasOwnProperty("idStepName")){
					if(r.idStepName == "lotes"){
						options.series = []
						options.series[r.nameFinca] = data_graficas.semana.fincas[r.nameFinca]
					}else if(r.idStepName == "labores"){
						options.series = []
						options.series[r.nameFinca] = data_graficas.semana.fincas[r.nameFinca]
						options.series[r.nameLote] = data_graficas.semana.lotes[r.nameLote]
					}
				}

				if(r.hasOwnProperty("zonas_main") && r.zonas_main.hasOwnProperty("weeks")){
					options.series = r.zonas_main.weeks;
				}else{
					if($scope.isArray(options.series)){
						$.each(r.zonas.weeks, function(key, value){
							options.series[key] = (value)
						})
					}else{
						options.series = r.zonas.weeks;
					}
				}

				if(r.hasOwnProperty("zonas_main") && r.zonas_main.hasOwnProperty("semana")){
					options.labor = r.zonas_main.semana;
				}
			} 

			if(r.hasOwnProperty("tittle")){
				$scope.status_tittle = r.tittle;
			}

			if($scope.umbrales.hasOwnProperty("range_min") && $scope.umbrales.hasOwnProperty("range_max")){
				umbral_range.min = $scope.umbrales.range_min;
				umbral_range.max = $scope.umbrales.range_max;
			}

			loadScript($scope.wizardStep.step , options , true, OptionsPrincipal , $scope.umbrales.umbral_line , umbral_range, $scope.modo);
			$scope.scrollAnimateTop();
		}
	}

	$scope.scrollAnimateTop = function(){
		var body = angular.element("html, body");
		body.stop().animate({scrollTop:0}, '500', 'swing', function() {});
	}

	$scope.getTotalsLabor = function(type){
		type = type || 1;
		var data = $scope.wizardData.labores.causas;
		var total_1 = 0;
		var total_2 = 0;
		for (var i = 0; i < data.length; i++) {
			if(!isNaN(parseFloat(data[i].porcentaje)))
				total_1 +=  parseFloat(data[i].porcentaje);
			if(!isNaN(parseFloat(data[i].porcentaje2)))
				total_2 += parseFloat(data[i].porcentaje2);
		}
		return (type == 1) ? total_1 : total_2;
	}

	$scope.getAvgTotalsStep2 = function(type_labor){
		var count = 0;
		var promedio = 0;
		var total = 0;
		for (var key in type_labor) {
			count++;
			promedio += parseFloat(type_labor[key].promedio);
		}
		total = new Number(promedio / count).toFixed(2);
		return total;
	}

	$scope.getAvgTotals = function(type_labor){
		var count = 0;
		var promedio = 0;
		var total = 0;
		for (var key in type_labor) {
			count++;
			promedio += parseFloat((type_labor[key].promedio / type_labor[key].avg));
		}
		total = new Number(promedio / count).toFixed(2);
		return total;
	}

	$scope.getTotals = function(type){
		type = type || 1;
		var data = $scope.wizardData.data;
		var total_1 = 0;
		var total_2 = 0;
		for (var i = 0; i < data.length; i++) {
			if(!isNaN(parseFloat(data[i].porcentaje)))
				total_1 += parseFloat(data[i].porcentaje);
			if(!isNaN(parseFloat(data[i].porcentaje2)))
				total_2 += parseFloat(data[i].porcentaje2);
		}
		return (type == 1) ? total_1 : total_2;
	}

	$scope.checks = function(porcentaje){
		porcentaje= parseFloat(porcentaje);
		if(porcentaje >= parseFloat($scope.umbrales.green_umbral_1))
			return 'fa fa-check font-green-jungle';
		else if (porcentaje >= parseFloat($scope.umbrales.yellow_umbral_1) && porcentaje <= parseFloat($scope.umbrales.yellow_umbral_2))
			return 'fa fa-exclamation font-yellow-lemon';
		else
			return 'fa fa-close font-red-thunderbird';
	}

	$scope.checks2 = function(porcentaje){
		porcentaje= parseFloat(porcentaje);
		if(porcentaje >= parseFloat($scope.umbrales.green_umbral_1) && porcentaje <= parseFloat($scope.umbrales.green_umbral_2))
			return 'fa fa-check font-green-jungle';
		else if (porcentaje >= parseFloat($scope.umbrales.yellow_umbral_1) && porcentaje <= parseFloat($scope.umbrales.yellow_umbral_2))
			return 'fa fa-exclamation font-yellow-lemon';
		else if (porcentaje > parseFloat($scope.umbrales.red_umbral_1) && porcentaje < parseFloat($scope.umbrales.red_umbral_2))
			return 'fa fa-close font-red-thunderbird';
		else
			return '';
	}

	$scope.revision = function(porcentaje){
  		// alert(porcentaje);
	  if(porcentaje >=  parseFloat($scope.umbrales.green_umbral_1))
	   return 'green-jungle';
	  else if (porcentaje >= parseFloat($scope.umbrales.yellow_umbral_1) && porcentaje <= parseFloat($scope.umbrales.yellow_umbral_2))
	   return 'yellow-lemon';
	  else
	   return 'red-thunderbird';
	}

	$scope.arrayToString = function(string){
    	return string;
	};

	$scope.getPromedio = function(data, v){
		var count = 0;
		var promedio = 0;
		var total = 0;
		for (var key in data.labores) {
			count++;
			promedio += parseFloat(data.labores[key].promedio);
		}
		total = parseFloat(parseFloat(promedio) / parseFloat(count));
		return total;
	}

	$scope.getPromedioLote = function(data){
		var count = 0;
		var promedio = 0;
		var total = 0;
		for (var key in data) {
			count++;
			promedio += parseFloat($scope.getPromedio(data[key]));
		}
		total = parseFloat(parseFloat(promedio) / parseFloat(count));
		return total;
	}

	$scope.getPromedioLoteZona = function(data){
		var count = 0;
		var promedio = 0;
		var total = 0;
		for (var key in data) {
			count++;
			promedio += parseFloat($scope.getPromedioLote(data[key]));
		}
		total = parseFloat(parseFloat(promedio) / parseFloat(count));
		return total;
	}

	$scope.createProperty = function(data , key , value , index){
		data[key] = value;
		return value;
	}

	$scope.isArray = function(value) {
		if (value) {
			if (typeof value === 'object') {
				return (Object.prototype.toString.call(value) == '[object Array]')
			}
		}
		return false;
	}

}]);
