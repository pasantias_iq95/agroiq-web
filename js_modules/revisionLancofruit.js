const { app, echartsPlv } = window

function initGrafica(options, id){
    var option = {
            tooltip : {
                trigger: 'axis',
                axisPointer : {
                    type : 'shadow'
                }
            },
            toolbox: {
                show : true,
                feature : {
                    mark : {show: true},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            legend: {
                data:['Test 1','Test 2']					
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
                    data : ['1','2','3','4','5','6','7']
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'Test 1',
                    type:'bar',
                    data:[862, 1018, 964, 1026, 1679, 1600, 1570],
                    markLine : {
                        lineStyle: {
                            normal: {
                                type: 'dashed'
                            }
                        },
                        data : [
                            [{type : 'min'}, {type : 'max'}]
                        ]
                    }
                },
                {
                    name:'Test 2',
                    type:'bar',
                    stack: 'Test 1',
                    data:[620, 732, 701, 734, 1090, 1130, 1120]
                },
            ]
        };
    var myChart = new echartsPlv();
    myChart.init(id , options);
}

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.controller('lancofruit', ['$scope','$http','$interval','client', function($scope,$http,$interval,client){

    // 27/04/2017 - TAG: GOOGLE MAPS
    $scope.map = {}
    $scope.table = []
    $scope.sectores = []
    $scope.rutas = []
    $scope.filters = {
        sector : "",
        ruta : "",
        search : "",
        tipo : "",
        status : ""
    }


    $scope.createMap = function(){
        var options = {
            zoom: 12
            , center: new google.maps.LatLng(-2.188861, -79.898896)
            , mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        $scope.map = new google.maps.Map(document.getElementById('map'), options);
    }

    $scope.data = []
    $scope.markers = {
        marker : {},
        infowindow: new google.maps.InfoWindow(),
        restart : function(){
			for(var key in this.marker){
				this.marker[key].setMap(null)
			}
			this.marker = {};
		},
        crearInfo: function (data) {
            // var self = $scope.markers;
            var content = ""
            if (data.hasOwnProperty("geolocalizacion")) {
                var img = "";
                if(data.hasOwnProperty("foto_lugar") && data.foto_lugar != ""){
                    img = `<img src="${data.foto_lugar}" class="img-responsive" style="width: 250px;height: 150px;" alt="${data.cliente}" width="100" height="30">`
                }
                content = `
                    <h4>${data.propietario}</h4>
                    ID : ${data.id} <br>
                    Local : ${data.cliente} <br>
                    Domicilio : ${data.direccion} <br>
                    Tipo : ${data.status} <br>
                    Referencia : ${data.referencia} <br>
                    Telefono : ${data.telefono} <br>
                    Correo : ${data.correo} <br>
                    RUC : ${data.ruc} <br>
                    Sector : ${data.sector} <br>
                    <br>
                        ${img}
                    <br>
                    
                `
                // self.infowindow[data.id] = new google.maps.InfoWindow({
                //     content: content
                // });
            }

            return content
        },
        generate : function(data){
            var path = "http://maps.google.com/mapfiles/ms/micons/";
            var self = $scope.markers;
            if(data.hasOwnProperty("id")){
                data.id = parseInt(data.id);
                var pushpin = "yellow.png";
                if (data.status == "Cliente")
                    pushpin = "blue.png";
                if (data.status == "Prospecto")
                    pushpin = "red.png";
                
                var icon = path + pushpin;

                var LatLng = data.geolocalizacion.split(" ")

                var position = { lat: parseFloat(LatLng[0]), lng: parseFloat(LatLng[1]) };
            

                if (!self.marker.hasOwnProperty(data.id)){
                    self.marker[data.id] = new google.maps.Marker({
                        position: position,
                        map: $scope.map,
                        icon: icon,
                        title: data.cliente
                    });

                    var content = self.crearInfo(data)

                    google.maps.event.addListener(self.marker[data.id] , 'click', function () {
                    // google.maps.event.addListener(self.marker[data.id] , 'mouseover', function () {
                        self.infowindow.setContent(content)
                        self.infowindow.open($scope.map, this);
                    });
                }
            }
        },
        openInfoWindow : function(data){
            var self = $scope.markers;
            if(data.hasOwnProperty("id")){
                var content = self.crearInfo(data)
                var marker = self.marker[data.id]
                self.infowindow.setContent(content)
                self.infowindow.open($scope.map, marker);
            }
        },
        load : function(r , b ){
            b();
            if(r.hasOwnProperty("data")){
                $scope.data = angular.copy(r.data)
                var data = r.data.map($scope.markers.generate)
            }
        }
    }

    // 27/04/2017 - TAG: GOOGLE MAPS

	$scope.init = function(){
        $scope.createMap();
		$scope.request.all()
	}

	$scope.request = {
		table : function(){
            var filters = $scope.filters;
			client.post("phrapi/lacofruit/index", $scope.printTable , filters)
		},
        markers : function(){
            var filters = $scope.filters;
            $scope.markers.restart()
            client.post("phrapi/lacofruit/markers", $scope.markers.load , filters)
        },
        filters : function(){
            var filters = $scope.filters;
            client.post("phrapi/lacofruit/filters", $scope.printFilters , filters)
        },
        pasteles : function(){
            var filters = $scope.filters;
            client.post("phrapi/lancofruit/pasteles", $scope.printPasteles , filters)
        },
		all : function(){
            this.table()
            this.markers()
            this.filters()
            this.pasteles()
		}
    }
    
    $scope.changeCategoriaPastel = function(){
        if(!$scope.filters.id_categoria_pastel > 0){
            if($scope.categorias_pie.categorias.length > 0){
                $scope.filters.id_categoria_pastel = $scope.categorias_pie[0].id
            }
        }
        if($scope.filters.id_categoria_pastel > 0){
            initGrafica($scope.pie_tipo[$scope.filters.id_categoria_pastel], "echarts_tipo");
        }
    }

    $scope.printPasteles = function(r, b){
        b()
        if(r){
            if(r.hasOwnProperty("pie_status")){
                initGrafica(r.pie_status, "echarts_status");
            }
            /*if(r.hasOwnProperty("pie_tipo")){
                initGrafica(r.pie_tipo, "echarts_tipo");
            }*/
            $scope.categorias_pie = r.categorias
            $scope.pie_tipo = r.pie_tipo
            
            if(r.categorias.length > 0){
                let cat = r.categorias[0]
                $scope.filters.id_categoria_pastel = cat.id
                initGrafica(r.pie_tipo[cat.id], "echarts_tipo");
            }
        }
    }

    $scope.printFilters = function(r , b){
        b();
        if(r){
            if(r.hasOwnProperty("sectores")){
                $scope.sectores = r.sectores
            }
            if(r.hasOwnProperty("rutas")){
                $scope.rutas = r.rutas
            }
            if(r.hasOwnProperty("status")){
                $scope.status = r.status
            }
            if(r.hasOwnProperty("tipos")){
                $scope.tipos = r.tipos
            }
            if(r.hasOwnProperty("subtipos")){
                $scope.subtipos = r.subtipos
            }
        }
    }

	$scope.printTable = function(r, b){
		b()
		if(r){
			$scope.table = angular.copy(r.table)
		}
	}

    const changeSector = function(){
        $scope.request.all()
    }

    const changeRuta = function(){
        $scope.request.all()
    }

	$scope.init()

    $scope.config = {
        modeNew : false,
        index_selected_ruta : 0,
        selected_ruta : {},
        ruta : "",
        rutas : [],
        open : function(){
            $("#my_multi_select1").multiSelect({
                selectableHeader: "<div class='bg-blue bg-font-blue' style='padding: 7px;'> Por Asignar </div>",
                selectionHeader: "<div class='bg-blue bg-font-blue' style='padding: 7px;'> Asignados </div>",
                selectableOptgroup : false,
                afterSelect: function(values){

                }
            });
            $("#my_multi_select1").multiSelect('refresh');
            client.post('phrapi/lancofruit/config', $scope.config.printRutas)
        },
        refreshRuta : function(){
            $scope.config.selected_ruta = $scope.config.rutas[$scope.config.index_selected_ruta]
            setTimeout(function(){
                $("#my_multi_select1").multiSelect('refresh');
            }, 250)
        },
        cancel : function(){
            $scope.config.modeNew = false
            $scope.config.modeNewVendedor = false
            $scope.config.ruta = ""
            $scope.config.vendedor = ""
        },
        newRuta : function(){
            $scope.config.modeNew = true
        },
        newVendedor : function(){
            $scope.config.modeNewVendedor = true
        },
        saveVendedor : function(){
            client.post('phrapi/lancofruit/addVendedor', $scope.config.sucess, { name : $scope.config.vendedor })
        },
        save : function(){
            client.post('phrapi/lancofruit/addRuta', $scope.config.sucess, { name : $scope.config.ruta })
        },
        saveConfig : function(){
            client.post('phrapi/lancofruit/rutas/save', $scope.config.sucess, { vendedores : $("#my_multi_select1").val(), ruta : $scope.config.selected_ruta.id })
        },
        sucess : function(r, b){
            b()
            if(r.status == 200){
                alert('Guardado con éxito', '', 'success')
            }
            $scope.config.selected_ruta = {}
            $scope.config.printRutas(r, b)
        },
        printRutas : function(r, b){
            b()
            if(r){
                if(r.status == 200){
                    $scope.config.cancel()
                    $scope.config.no_asignados = angular.copy(r.no_asignados || [])
                    $scope.config.rutas = angular.copy(r.rutas || [])

                    if(!$scope.config.selected_ruta.hasOwnProperty('id')){
                        $scope.config.selected_ruta = $scope.config.rutas[0]
                    }

                    setTimeout(function(){
                        let modal = $("#nsecado").modal()
                        modal.trigger('toggle')
                        $scope.config.refreshRuta()
                    }, 250)
                }
                if(r.status == 400){
                    alert(r.message)
                }
            }
        }
    }
	
	$scope.search = {
        orderBy : "id",
        reverse : false,
        limit : 10,
        actual_page : 1
    }

	$scope.changeSort = function(column){
        if($scope.search.orderBy != column){
            var previous = $("th.selected")[0]            
            $(previous).removeClass("selected")
            $(previous).removeClass("sorting_asc")
            $(previous).removeClass("sorting_desc")
        }
        $scope.search.reverse = ($scope.search.orderBy != column) ? false : !$scope.search.reverse;
        $scope.search.orderBy = column;
        var actual_select = $("#"+column+"_column")
        if(!actual_select.hasClass("selected")){
            actual_select.addClass("selected")
        }
        if($scope.search.reverse){
            actual_select.addClass("sorting_desc");
            actual_select.removeClass("sorting_asc");
        }else{
            actual_select.addClass("sorting_asc");
            actual_select.removeClass("sorting_desc");
        }
    }

	$scope.next = function(dataSource){
        if($scope.search.actual_page < parseInt(dataSource.length / parseInt($scope.search.limit)) + (dataSource.length % parseInt($scope.search.limit) == 0 ? 0 : 1)) 
            $scope.search.actual_page++;
    }

    $scope.prev = function(dataSource){
        if($scope.search.actual_page > 1)
            $scope.search.actual_page--;
    }

    $scope.disableColumns = function(column, event){
        //var cells = $("#lancofruit th:nth-child("+column+"), td:nth-child("+column+")")
        var isActive = $(event.target).parent().hasClass("active")
        if(isActive){
            $(event.target).parent().removeClass("active")
            $("#"+column).parent().addClass("hide")
        }else{
            $(event.target).parent().addClass("active")
            $("#"+column).parent().removeClass("hide")
        }
        $scope.show[column] = !isActive;

        /*$.each(cells, function(index, value){
            if(!isActive)
                $(value).removeClass("hide");
            else
                $(value).addClass("hide");
        })*/
    }

    $scope.exportExcel = function(){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)

                // modify content
                var contentTable = table.innerHTML
                // remove filters
                /*var cut = contentTable.search('<tr role="row" class="filter">')
                var cut2 = contentTable.search('</thead>')

                var part1 = contentTable.substring(0, cut)
                var part2 = contentTable.substring(cut2, contentTable.length)
                contentTable = part1 + part2*/

                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel("lancofruit", "Información General")
    }

    $scope.exportPdf = function(){
        openWindowWithPostRequest();
    }

    function openWindowWithPostRequest() {
        var winName='PDF';
        var winURL='phrapi/export/pdf';
        var windowoption='resizable=yes,height=200,width=400,location=0,menubar=0,scrollbars=1';
        var params = { 'table' : $("#lancofruit").html() };
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", winURL);
        form.setAttribute("target",winName); 
        for (var i in params) {
            if (params.hasOwnProperty(i)) {
                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = i;
                input.value = params[i];
                form.appendChild(input);
            }
        }
        document.body.appendChild(form);
        window.open('', winName,windowoption);
        form.target = winName;
        form.submit();
        document.body.removeChild(form);
    }

}])