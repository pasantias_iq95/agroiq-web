
app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
        console.log(field)
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
                console.log("parseFloat")
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.controller('lancofruit', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window' , '$filter', function($scope,$http,$interval,client, $controller, $timeout,$window , $filter){

    // 27/04/2017 - TAG: GOOGLE MAPS
    $scope.map = {}
    $scope.table = []
    $scope.sectores = []
    $scope.rutas = []
    $scope.filters = {
        sector : "",
        ruta : "",
        search : "",
        tipo : "",
        status : ""
    }

    $scope.data = []
    // 27/04/2017 - TAG: GOOGLE MAPS

	$scope.init = function(){
		$scope.request.all()
	}

	$scope.request = {
		table : function(){
            var filters = $scope.filters;
			client.post("phrapi/lacofruit/index", $scope.printTable , filters)
		},
		all : function(){
            this.table()
		}
	}

	$scope.printTable = function(r, b){
		b()
		if(r){
			$scope.table = angular.copy(r.table)
		}
    }
    
	$scope.init()
	
	$scope.search = {
        orderBy : "id",
        reverse : false,
        limit : 10,
        actual_page : 1
    }

	$scope.changeSort = function(column){
        if($scope.search.orderBy != column){
            var previous = $("th.selected")[0]            
            $(previous).removeClass("selected")
            $(previous).removeClass("sorting_asc")
            $(previous).removeClass("sorting_desc")
        }
        $scope.search.reverse = ($scope.search.orderBy != column) ? false : !$scope.search.reverse;
        $scope.search.orderBy = column;
        var actual_select = $("#"+column+"_column")
        if(!actual_select.hasClass("selected")){
            actual_select.addClass("selected")
        }
        if($scope.search.reverse){
            actual_select.addClass("sorting_desc");
            actual_select.removeClass("sorting_asc");
        }else{
            actual_select.addClass("sorting_asc");
            actual_select.removeClass("sorting_desc");
        }
    }

	$scope.next = function(dataSource){
        if($scope.search.actual_page < parseInt(dataSource.length / parseInt($scope.search.limit)) + (dataSource.length % parseInt($scope.search.limit) == 0 ? 0 : 1)) 
            $scope.search.actual_page++;
    }

    $scope.prev = function(dataSource){
        if($scope.search.actual_page > 1)
            $scope.search.actual_page--;
    }

    $scope.disableColumns = function(column, event){
        //var cells = $("#lancofruit th:nth-child("+column+"), td:nth-child("+column+")")
        var isActive = $(event.target).parent().hasClass("active")
        if(isActive){
            $(event.target).parent().removeClass("active")
            $("#"+column).parent().addClass("hide")
        }else{
            $(event.target).parent().addClass("active")
            $("#"+column).parent().removeClass("hide")
        }
        $scope.show[column] = !isActive;

        /*$.each(cells, function(index, value){
            if(!isActive)
                $(value).removeClass("hide");
            else
                $(value).addClass("hide");
        })*/
    }

    $scope.exportExcel = function(){
        var tableToExcel = (function() {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
                , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
            return function(table, name) {
                if (!table.nodeType) table = document.getElementById(table)

                // modify content
                var contentTable = table.innerHTML
                // remove filters
                /*var cut = contentTable.search('<tr role="row" class="filter">')
                var cut2 = contentTable.search('</thead>')

                var part1 = contentTable.substring(0, cut)
                var part2 = contentTable.substring(cut2, contentTable.length)
                contentTable = part1 + part2*/

                var ctx = {worksheet: name || 'Worksheet', table: contentTable}
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
        tableToExcel("lancofruit", "Información General")
    }

    $scope.exportPdf = function(){
        openWindowWithPostRequest();
    }

    function openWindowWithPostRequest() {
        var winName='PDF';
        var winURL='phrapi/export/pdf';
        var windowoption='resizable=yes,height=200,width=400,location=0,menubar=0,scrollbars=1';
        var params = { 'table' : $("#lancofruit").html() };
        var form = document.createElement("form");
        form.setAttribute("method", "post");
        form.setAttribute("action", winURL);
        form.setAttribute("target",winName); 
        for (var i in params) {
            if (params.hasOwnProperty(i)) {
                var input = document.createElement('input');
                input.type = 'hidden';
                input.name = i;
                input.value = params[i];
                form.appendChild(input);
            }
        }
        document.body.appendChild(form);
        window.open('', winName,windowoption);
        form.target = winName;
        form.submit();
        document.body.removeChild(form);
    }

}])