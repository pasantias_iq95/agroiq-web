app.controller('rpersonal', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window', function($scope,$http,$interval,client, $controller , $timeout,$window){

    /*----------  FILTERS  ----------*/
    $scope.search = "";
    $scope.weeks = [];

    $scope.viewModel = function(){
        console.log($scope.search);
    }

    $scope.generateWeeks = function(is_filter){
        is_filter = is_filter || 1;
        var semanas = [];
        for (var i = 1; 53 > i; i++) {
            semanas.push(i);
        }
        if(is_filter == 1){
            semanas.push("TOTAL");
        }
        return semanas;
    }
    
    $scope.weeks = $scope.generateWeeks(-1);
    // $scope.generateYears = function(){
    //     var years = [];
    //     var years = ["ENE" , "FEB" , "MAR" , "ABR" , "MAY" , "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC" , "TOTAL"];
    //     return years;
    // }

    // $scope.generateDays = function(){
    //     var days = ["LUN" , "MAR" , "MIE" , "JUE" , "VIE" , "SAB" , "TOTAL"];
    //     return days;
    // }

    $scope.dataFilters = [];
    $scope.filters = {}

    $scope.withTable = "";
    $scope.colspan = "";

    const changeFilter = function(){
        var type = $("#filter").val();
        $scope.init();
    }

    const changeFilterWeek = function(){
        $scope.init();
        $scope.withTable = "12.5%";
    }

    $scope.getType = function(type){
        $scope.rpersonal.params.type = type;
        $scope.init();
    }
    /*----------  FILTERS  ----------*/
    $scope.rpersonal = {
        params : {
            idFinca : 1,
            idLote : 0,
            idLabor : 0,
            fecha_inicial : moment().startOf('month').format('YYYY-MM-DD'),
            fecha_final : moment().endOf('month').format('YYYY-MM-DD'),
            finca : "",
            filter : "",
            type : "T",
            week : ""
        },
        step : 0,
        path : ['phrapi/agricolas/index'],
        templatePath : [],
        nocache : function(){
            $scope.init();
        }
    }



    $scope.tabla = {
        rpersonal : []
    }

    $scope.openDetalle= function(data){
        data.expanded = !data.expanded;
    }

    $scope.totales = [];
    $scope.umbral = [];
    $scope.loteros_aereo = [];
    $scope.cosechadores = [];

    $scope.init = function(){
        var data = $scope.rpersonal.params;
        client.post($scope.rpersonal.path , $scope.printDetails ,data)
    }

    $scope.start = true;
    $scope.printDetails = function(r,b){
        b();
        if(r){
            $scope.loteros_terrestre = [];
            $scope.loteros_aereo = [];
            $scope.totales = [];
            $scope.umbral = [];
            $scope.dataFilters = [];
            $scope.filters = [];
            $scope.colspan = 4;
            if(r.hasOwnProperty("data")){
                if(r.hasOwnProperty("fincas")){
                    $scope.filters = r.fincas;
                }
                if(r.data.hasOwnProperty("semana")){
                    $scope.dataFilters = r.data.semana;
                    $scope.colspan = parseInt(r.data.colspan);
                    $scope.withTable = parseInt(r.data.withTable) + "%";
                }
                if(r.data.hasOwnProperty("terrestre")){
                    $scope.loteros_terrestre = r.data.terrestre;
                }
                if(r.data.hasOwnProperty("aereo")){
                    $scope.loteros_aereo = r.data.aereo;
                }
                if(r.data.hasOwnProperty("totales")){
                    $scope.totales = r.data.totales;
                    $scope.umbral = r.data.umbral.campo;
                    console.log($scope.umbral)
                }
                if($scope.start){
                    $scope.start = false;
                    setInterval(function(){
                        $scope.init();
                    } , 60000 * 5 );
                }
            }
        }
    }

}]);

