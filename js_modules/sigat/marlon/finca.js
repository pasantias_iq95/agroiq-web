$.fn.sortElements = (function(){
    
    var sort = [].sort;
    
    return function(comparator, getSortable) {
        
        getSortable = getSortable || function(){return this;};
        
        var placements = this.map(function(){
            
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
                
                // Since the element itself will change position, we have
                // to have some way of storing it's original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
            
            return function() {
                
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
                
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
                
            };
            
        });
       
        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
        
    };
    
})();

$(function(){

    var gerentes = JSON.parse($("#json_gerentes").val())

    if("<?= $_GET['id'] ?>" > 0){
        $("#txtnom").change(function(){
            $(".lbladd").html("Guardar");
        })
    }
        
    /*$("#gerentes").change(function() {
        reDrawGerentes()
    });*/

    $(".cancel").on("click" , function(){
        document.location.href = "fincas";
    });

    function addRow(){
        var data = {
            id_finca : "<? echo $_GET['id']?>",
            sector : $("#s_sector").val(),
            fumigacion : $("#fumigacion").val(),
            produccion : $("#produccion").val(),
            neta : $("#neta").val()
        }

        if(data.lote != ""){
            ahttp.post("./controllers/index.php?accion=Fincas.addSector", printData , data);
        }
    }

    $(".addRow").on("click" , function(){
        addRow()
    });

    $("#rowsFilas").on("click" ,".removeRow", function(){
        var id = this.id;
        removeRow(id)
    });

    function removeRow(id){
        console.log(id);
        if(id && id > 0){
            var data = {
                id_finca : "<? echo $_GET['id']?>",
                id_sector : id
            }
          ahttp.post("./controllers/index.php?accion=Fincas.removeSector",printData , data);  
        }
    }

    function calculateTotal(){
        var hec_fumigacion = 0,
            hec_produccion = 0,
            hec_neta = 0;
        $(".hec_fumigacion").each(function(index, el) {
            var valor = $(this).html();
            hec_fumigacion += parseFloat(valor)
        });
        $(".hec_produccion").each(function(index, el) {
            var valor = $(this).html();
            hec_produccion += parseFloat(valor)
        });
        $(".hec_neta").each(function(index, el) {
            var valor = $(this).html();
            hec_neta += parseFloat(valor)
        });

        $(".hec_fumigacion_total").html(hec_fumigacion)
        $(".hec_produccion_total").html(hec_produccion)
        $(".hec_neta_total").html(hec_neta)
    }

    function printData(r , b){
        b();
        if(r){
            var inHTML = [];

            $.each(r, function(index, value){
                inHTML.push("<tr>");
                    inHTML.push("<td>");
                    inHTML.push(value.sector);
                    inHTML.push("</td>");
                    inHTML.push("<td class='hec_fumigacion'>");
                    inHTML.push(value.hec_fumigacion);
                    inHTML.push("</td>");
                    inHTML.push("<td class='hec_produccion'>");
                    inHTML.push(value.hec_produccion);
                    inHTML.push("</td>");
                    inHTML.push("<td class='hec_neta'>");
                    inHTML.push(value.hec_neta);
                    inHTML.push("</td>");
                    inHTML.push("<td>");
                    inHTML.push('<button type="button" class="btn btn-sm red-thunderbird removeRow" id="'+value.id+'">Eliminar</button>');
                    inHTML.push("</td>");
                inHTML.push("</tr>"); 
            });

            $("#rowsFilas").html(inHTML.join("")); //add generated tr html to corresponding table
            $("#lote").val('');
            $("#area").val('');
            calculateTotal()
        }
    }

    var table = $('table > tbody');
    var ids = [];
    $('.alphanumeric')
        .wrapInner('<span title="columna ordenada"/>')
        .each(function(){
            
            var th = $(this),
                thIndex = th.index(),
                inverse = false;
            
            th.click(function(){
                ids = [];
                table.find('td').filter(function(){
                    
                    return $(this).index() === thIndex;
                    
                }).sortElements(function(a, b){
                    
                    return $.text([a]) > $.text([b]) ?
                        inverse ? -1 : 1
                        : inverse ? 1 : -1;
                    
                }, function(){
                    
                    // parentNode is the element we want to move
                    return this.parentNode; 
                    
                });
                
                inverse = !inverse;
                  
	             var button = table.find("td > button").map(function(){
	             	var id = $(this).attr("id");
	             	ids.push(id)
	             });

	             var data = {
	                id : "<? echo $_GET['id']?>",
	                id_lotes : ids
	              }

	             ahttp.post("./controllers/index.php?accion=Fincas.positionSector",printData , data);  
            });
             
        });

});

jQuery(document).ready(function() {  
    // $("#gerentes").multiSelect();

    var id_gerente_selected = $("#gerentes").val()
    // $("#container_gerentes").on("click", ".ms-selectable li", function(event){
    //     reDrawGerentes(true)
    //     id_gerente_selected = $("#gerentes").val()
    // })

    // $("#container_gerentes").on("click", ".ms-selection li", function(event){
    //     id_gerente_selected = 0
    // })

    // function reDrawGerentes(select = false){
    //     var selected = $("#gerentes :selected")
    //     var indexs = 0
    //     $.each(gerentes, function(index, value){
    //         if(selected == value.nombre){
    //             selected = value
    //             indexs = index
    //         }
    //     })
    //     //gerentes por seleccionar
    //     var list_select = $(".ms-selectable ul")
    //     list_select.children("li").each(function(index, item){
    //         if(index == selected[0].index){
    //             $(item).attr("style", "display: none")
    //         }else{
    //             $(item).attr("style", "")
    //         }
    //     })
    //     //gerentes seleccionados
    //     var list_selected = $(".ms-selection ul")
    //     list_selected.children("li").each(function(index, item){
    //         if(index != selected[0].index){
    //             $(item).attr("style", "display: none")
    //         }else{
    //             $(item).attr("style", "")
    //         }
    //     })
    // }

    $(".btnadd").click(function(){
        
        if($("#txtnom").val()==''){
            alert("Favor de ingresar un nombre de Hacienda");
            return false;
        }
        else{ 
            var data = {
                id : "<? echo $_GET['id']?>",
                url : "controllers/index.php",
                option : "accion=Fincas.create",
                params : "&txtnom="+$("#txtnom").val()+"&id_gerente_selected="+$("#gerentes").val()+"&id=<? echo $_GET['id']?>",
            }

            if(parseInt(data.id) > 0){
                data.option = "accion=Fincas.update";
            }

            $.ajax({
                type: "POST",
                url: "controllers/index.php",
                data: data.option + data.params,
                success: function(msg){
                    alert("Registro registrado/modificado con el ID "+ msg , "Fincas" , "success" , function(){
                        if(parseInt(msg) > 0){
                            $('input[type=text]').each(function() {
                                $(this).val('');
                            });
                            document.location.href = "fincas";
                        }
                    });
                }
            });
        }
        return false;
    });
})