app.service('request', [ '$http', ($http) => {
    let service = {}
    service.last = (callback, params) => {
        $http.post('phrapi/sumifru/comparacion/last', params || {}).then(r => {
            callback(r.data)
        })
    }
    service.historico = (callback, params) => {
        load.block('react-table')
        load.block('react-table-2')
        $http.post('phrapi/sumifru/comparacion/historico', params || {}).then(r => {
            load.unblock('react-table')
            load.unblock('react-table-2')
            callback(r.data)
        })
    }
    service.grafica = (callback, params) => {
        load.block('grafica')
        $http.post('phrapi/sumifru/comparacion/grafica', params || {}).then(r => {
            load.unblock('grafica')
            callback(r.data)
        })
    }
    return service
}])

var invertidos = ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'PESO', 'CAJAS', 'CAJAS 41.5', 'RATIO CORTADO', 'RATIO PROCESADO']
function checkUmbral(value, umbral, invertido = false){
    if(!invertido){
        if(value > 0 && umbral > 0){
            if(value <= umbral) return 'bg-green-haze bg-font-green-haze'
            else return 'bg-red-thunderbird bg-font-red-thunderbird'
        }
    }else{
        if(value > 0 && umbral > 0){
            if(value >= umbral) return 'bg-green-haze bg-font-green-haze'
            else return 'bg-red-thunderbird bg-font-red-thunderbird'
        }
    }
    return ''
}
function checkUmbralMerma(value, umbral){
    if(value > 0 && umbral){
        if(value < umbral) return 'bg-green-haze bg-font-green-haze'
        else return 'bg-red-thunderbird bg-font-red-thunderbird'
    }
    return ''
}
function number_format(amount, decimals) {
    
    amount += ''; // por si pasan un numero en vez de un string
    amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

    decimals = decimals || 0; // por si la variable no fue fue pasada

    // si no es un numero o es igual a cero retorno el mismo cero
    if (isNaN(amount) || amount === 0) 
        return parseFloat(0).toFixed(decimals);

    // si es mayor o menor que cero retorno el valor formateado como numero
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
}

app.controller('control', ['$scope', 'request', function($scope, $request){

    $scope.filters = {
        anio : '',
        semana : '',
        variable : 'RACIMOS CORTADOS'
    }
    $scope.anios = []
    $scope.semanas = []
    $scope.variables = [
        'RACIMOS CORTADOS',
        'RACIMOS PROCESADOS',
        'RACIMOS RECUSADOS',
        'EDAD PROM',
        'PESO PROM',
        'CALIBRE PROM',
        'MANOS PROM',
        'DEDOS PROM',
        'CAJAS CONV',
        'RATIO CORTADO',
        'RATIO PROCESADO',
        'MERMA CORTADA',
        'MERMA PROCESADA',
        'ENFUNDE',
        'RECOBRO',
        '% RECUSADOS',
        'MUESTREO',
    ]

    const initReactTable = (data) => {
        let props = {
            header : [
                {
                   key : 'area',
                   name : '',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width : 200
                },{
                    key : 'racimos_cosechados',
                    name : 'RAC COS',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    formatter : 'Number',
                    resizable : true
                 },{
                   key : 'racimos_procesados',
                   name : 'RAC PROC',
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                },{
                   key : 'racimos_recusados',
                   name : 'RAC RECU',
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                   key : 'porc_recusados',
                   name : '% RECU',
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                },{
                    key : 'edad_prom',
                    name : 'EDAD',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'racimos_peso_prom',
                    name : 'RAC PESO',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                },{
                    key : 'calibre_prom',
                    name : 'CALIB',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'manos_prom',
                    name : 'MANOS',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'dedos_prom',
                    name : 'DEDOS',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'cajas',
                    name : 'CAJAS CONV',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'ratio_cortado',
                    name : 'RATIO CORT',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'ratio_procesado',
                    name : 'RATIO PROC',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'merma_cortada',
                    name : 'MERMA CORT',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'merma_procesada',
                    name : 'MERMA PROC',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'enfunde',
                    name : 'ENFUNDE',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 },{
                    key : 'recobro',
                    name : 'RECOBRO',
                    sortable : true,
                    titleClass : 'text-center',
                    alignContent : 'right',
                    filterable : true,
                    filterRenderer: 'NumericFilter',
                    resizable : true,
                 }
            ],
            data : data,
            height : 500,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        $scope.table1.exportToExcel()
                    },
                    className : ''
                }
            ]
        }
        $("#react-table").html("")
        $scope.table1 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById('react-table'))
    }

    const initReactTable2 = (data, semanas, id = 'react-table-2') => {
        let props = {
            header : [
                {
                   key : 'variable',
                   name : 'VARIABLE',
                   titleClass : 'text-center',
                   locked : true,
                   expandable : true,
                   resizable : true,
                   width: 250
                },{
                   key : 'promedio',
                   name : 'AVG',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   formatter : 'Number',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['promedio'])
                        let valueCell = valNumber > 0 ? valNumber : ''
                        var umbralClass = ''
                        if(valNumber > 0 && ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'RACIMOS RECUSADOS', 'CAJAS', 'CAJAS 41.5'].indexOf(rowData['variable_name']) >= 0) valueCell = number_format(valNumber, 0);
                        if(['MERMA CORTADA', 'MERMA PROCESADA', 'MERMA NETA'].indexOf(rowData['variable_name']) >= 0) umbralClass = checkUmbralMerma(valNumber, 2)
                        else umbralClass = checkUmbral(valNumber, rowData['promedio'], invertidos.indexOf(rowData['variable_name']) > -1)
                        return `
                            <div class="text-center ${umbralClass}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }/*,{
                   key : 'max',
                   name : 'MAX',
                   locked : true,
                   titleClass : 'text-center',
                   sortable : true,
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['max'])
                        let valueCell = valNumber > 0 ? valNumber : ''
                        var umbralClass = ''
                        if(valNumber > 0 && ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'RACIMOS RECUSADOS', 'CAJAS', 'CAJAS 41.5'].indexOf(rowData['campo']) >= 0) valueCell = number_format(valNumber, 0);
                        if(['MERMA CORTADA', 'MERMA PROCESADA', 'MERMA NETA'].indexOf(rowData['campo']) >= 0) umbralClass = checkUmbralMerma(valNumber, 2)
                        else umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)
                        return `
                            <div class="text-center ${umbralClass}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                },{
                   key : 'min',
                   name : 'MIN',
                   locked : true,
                   sortable : true,
                   titleClass : 'text-center',
                   alignContent : 'right',
                   filterable : true,
                   filterRenderer: 'NumericFilter',
                   resizable : true,
                   customCell : function(rowData, isChildren){
                        let valNumber = parseFloat(rowData['min'])
                        var valueCell = valNumber > 0 ? valNumber : ''
                        var umbralClass = ''
                        if(valNumber > 0 && ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'RACIMOS RECUSADOS', 'CAJAS', 'CAJAS 41.5'].indexOf(rowData['campo']) >= 0) valueCell = number_format(valNumber, 0);
                        if(['MERMA CORTADA', 'MERMA PROCESADA', 'MERMA NETA'].indexOf(rowData['campo']) >= 0) umbralClass = checkUmbralMerma(valNumber, 2)
                        else umbralClass = checkUmbral(valNumber, rowData['avg'], invertidos.indexOf(rowData['campo']) > -1)
                        return `
                            <div class="text-center ${umbralClass}" style="height: 100%">
                                ${valueCell}
                            </div>
                        `;
                    }
                }*/
            ],
            data : data,
            buttons : [
                {
                    title : 'Excel',
                    action : () => {
                        table2.exportToExcel()
                    },
                    className : ''
                }
            ],
            height : 550
        }
        Object.keys(semanas).map((key) => {
            let value = semanas[key]
            props.header.push({
                key : `sem_${value}`,
                name : `${value}`,
                sortable : true,
                alignContent : 'right',
                titleClass : 'text-center',
                filterable : true,
                filterRenderer: 'NumericFilter',
                resizable : true,
                customCell : function(rowData, isChildren){
                    let valNumber = parseFloat(rowData['sem_'+value])
                    let valueCell = valNumber > 0 ? valNumber : ''
                    var umbralClass = ''
                    if(valNumber > 0 && ['RACIMOS CORTADOS', 'RACIMOS PROCESADOS', 'RACIMOS RECUSADOS', 'CAJAS', 'CAJAS 41.5'].indexOf(rowData['variable_name']) >= 0) valueCell = number_format(valNumber, 0);
                    if(['MERMA CORTADA', 'MERMA PROCESADA', 'MERMA NETA'].indexOf(rowData['variable_name']) >= 0) umbralClass = checkUmbralMerma(valNumber, 2)
                    else umbralClass = checkUmbral(valNumber, rowData['promedio'], invertidos.indexOf(rowData['variable_name']) > -1)
                    return `
                        <div class="text-center ${umbralClass}" style="height: 100%">
                            ${valueCell}
                        </div>
                    `;
                }
            })
        })
        document.getElementById(id).innerHTML = ""
        $scope.table2 = ReactDOM.render(React.createElement(ReactDataGrid, props), document.getElementById(id))
    }

    $scope.changeGrafica = () => {
        $request.grafica((r) => {
            initGrafica(r.data)
        }, $scope.filters)
    }

    const initGrafica = (options) => {
        new echartsPlv().init('grafica', options)
    }

    $scope.last = () => {
        $request.last((r) => {
            $scope.semanas = r.semanas
            $scope.anios = r.anios
            setTimeout(() => {
                $scope.filters.anio = r.last_year
                $scope.filters.semana = r.last_week
                $scope.init()
                $scope.changeGrafica()
            }, 200)
        })
    }

    $scope.init = () => {
        $request.historico((r) => {
            let data = r.data
            data.map((d) => {
                Object.keys(d).map((key) => {
                    if(d[key] == 0){
                        d[key] = ''
                    }
                })
                d.children.map((finca) => {
                    Object.keys(finca).map((f) => {
                        if(finca[f] == 0){
                            finca[f] = ''
                        }
                    })
                    if(finca.children){
                        finca.children.map((empacadora) => {
                            Object.keys(empacadora).map((e) => {
                                if(empacadora[e] == 0){
                                    empacadora[e] = ''
                                }
                            })
                        })
                    }
                })
            })
            initReactTable(data)

            initReactTable2(r.data2, r.semanas)
        }, $scope.filters)
    }

    $scope.last()
}]);

