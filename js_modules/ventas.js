app.service('ventas', ['$http', function($http){

    this.index = function(callback, params){
        let url = 'phrapi/lancofruit/ventas/index'
        $http.post(url, params || {}).then(r => {
            callback(r)
        })
    }

    this.dia = function(callback, params){
        let url = 'phrapi/lancofruit/ventas/dia'
        $http.post(url, params || {}).then(r => {
            callback(r)
        })
    }

    this.semana = function(callback, params){
        let url = 'phrapi/lancofruit/ventas/semana'
        $http.post(url, params || {}).then(r => {
            callback(r)
        })
    }

    this.mes = function(callback, params){
        let url = 'phrapi/lancofruit/ventas/mes'
        $http.post(url, params || {}).then(r => {
            callback(r)
        })
    }

    this.anio = function(callback, params){
        let url = 'phrapi/lancofruit/ventas/anio'
        $http.post(url, params || {}).then(r => {
            callback(r)
        })
    }

    this.last = function(callback, params){
        let url = 'phrapi/lancofruit/ventas/last'
        $http.post(url, params || {}).then(r => {
            callback(r.data)
        })
    }

}])

app.filter('num', function() {
    return function(input) {
    return parseInt(input, 10);
    };
});

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

app.filter('sumOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        if(["peso", "manos", "calibre", "dedos"].indexOf(key) > -1){
            sum = sum / count;
        }
        return sum;
    }
})

app.filter('sum', function () {
    return function (data, key) {
        var sum = 0
        angular.forEach(data,function(value){
            if(parseFloat(value))
                sum += parseFloat(value) 
        });
        return sum;
    }
})

app.filter('avgOfValue', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;
        var count = 0;
        angular.forEach(data,function(value){
            if(value[key] != "" && value[key] != undefined && parseFloat(value[key])){
                sum = sum + parseFloat(value[key], 10);
                count++;
            }
        });
        sum = sum / count;
        return sum;
    }
})

app.filter('orderObjectBy', function() {
	return function(items, field, reverse) {
    	var filtered = [];
    	angular.forEach(items, function(item) {
    		if(!isNaN(parseInt(item))){
    			item = parseInt(item);
    		}
      		filtered.push(item);
    	});
    	filtered.sort(function (a, b) {
            if(parseFloat(a[field]) && parseFloat(b[field])){
				return (parseFloat(a[field]) > parseFloat(b[field]) ? 1 : -1);
			}else{
				return (a[field] > b[field] ? 1 : -1);
			}
    	});
    	if(reverse) filtered.reverse();
    	return filtered;
  	};
});

app.controller('myController', ['$scope','$http','$interval','client','$controller','$timeout' ,'$window' , 'ventas', function($scope, $http, $interval, client, $controller, $timeout, $window, ventas){

    var graficas = {

    }
    $scope.data = {
        dia : [],
        mes : [],
        semana : [],
        anio : []
    }
    $scope.filters = {
        semana : "",
        year : moment().year()
    }
    //moment().week()

    $scope.changeYear = () => {
        let dia_open = $("#collapse_3_1").hasClass('in')
        let semana_open = $("#collapse_3_2").hasClass('in')
        let mes_open = $("#collapse_3_3").hasClass('in')
        let anio_open = $("#collapse_3_4").hasClass('in')

        if(dia_open){
            $scope.getDataDia()
        }else if(semana_open){
            $scope.getDataSemana()
        }else if(mes_open){
            $scope.getDataMes()
        }else if(anio_open){
            $scope.getDataAnio()
        }
    }

    $scope.getDataAnio = () => {
        setTimeout(() => {
            let anio_open = $("#collapse_3_4").hasClass('in')
            if(anio_open){
                ventas.anio(r => {
                    if(r.data){
                        $scope.data.anio = angular.copy(r.data.data)
                        $scope.rutas_anio = angular.copy(r.data.rutas)
                        $scope.totales_anio = angular.copy(r.data.totales)
        
                        graficas.line_anio = new echartsPlv()
                        graficas.line_anio.init("lineal_anio", r.data.line_chart)
        
                        graficas.pie_anio = new echartsPlv()
                        graficas.pie_anio.init("pastel_anio", r.data.pie_chart)
                        $scope.$apply()
                    }
                }, $scope.filters)
            }
        }, 1000)
    }
    
    $scope.getDataMes = () => {
        setTimeout(() => {
            let mes_open = $("#collapse_3_3").hasClass('in')
            if(mes_open){
                ventas.mes(r => {
                    if(r.data){
                        $scope.data.mes = angular.copy(r.data.data)
                        $scope.rutas_mes = angular.copy(r.data.rutas)
                        $scope.totales_mes = angular.copy(r.data.totales)
        
                        graficas.line_mes = new echartsPlv()
                        graficas.line_mes.init("lineal_mes", r.data.line_chart)
        
                        graficas.pie_mes = new echartsPlv()
                        graficas.pie_mes.init("pastel_mes", r.data.pie_chart)
                        $scope.$apply()
                    }
                }, $scope.filters)
            }
        }, 1000)
    }

    $scope.getDataSemana = () => {
        setTimeout(() => {
            let semana_open = $("#collapse_3_2").hasClass('in')
            if(semana_open){
                ventas.semana(r => {
                    if(r.data){
                        $scope.data.semana = angular.copy(r.data.data)
                        $scope.rutas_semana = angular.copy(r.data.rutas)
                        $scope.totales_semana = angular.copy(r.data.totales)
        
                        graficas.line_semana = new echartsPlv()
                        graficas.line_semana.init("lineal_semana", r.data.line_chart)
        
                        graficas.pie_semana = new echartsPlv()
                        graficas.pie_semana.init("pastel_semana", r.data.pie_chart)
                    }
                }, $scope.filters)
            }
        }, 1000)
    }

    $scope.getDataDia = () => {
        setTimeout(() => {
            let dia_open = $("#collapse_3_1").hasClass('in')
            if(dia_open){
                ventas.dia(r => {
                    if(r.data){
                        $scope.data.dia = angular.copy(r.data.data)
                        $scope.rutas_dia = angular.copy(r.data.rutas)
                        $scope.totales_dia = angular.copy(r.data.totales)
                        
                        graficas.line_dia = new echartsPlv()
                        graficas.line_dia.init("lineal_dia", r.data.line_chart)

                        graficas.pie_dia = new echartsPlv()
                        graficas.pie_dia.init("pastel_dia", r.data.pie_chart)
                        $scope.$apply()
                    }
                }, $scope.filters)
            }
        }, 1000)
    }

    $scope.init = () => {
        ventas.last((r) => {
            $scope.anios = r.years
            $scope.filters.year = r.last_year

            $scope.semanas = r.semanas
            $scope.filters.semana = r.last_week

            /*ventas.index(r => {
                if(r.data){
                    $scope.semanas = r.data.semanas
                    if(r.data.ultimaSemana){
                        $scope.filters.semana = r.data.ultimaSemana
                    }
                }
            },{
                year : r.last_year
            })*/
        })
    }

    $scope.init()

}]);

