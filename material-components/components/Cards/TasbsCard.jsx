import React from "react";
import PropTypes from "prop-types";
import {
  withStyles,
  Card,
  CardContent,
  CardHeader,
  Tabs,
  Tab
} from "@material-ui/core";

import tasbsCardStyle from "./../../assets/jss/material-dashboard-react/tasbsCardStyle";

class TasbsCard extends React.Component {
  state = {
    value: 0
  };
  handleChange = (event, value) => {
    this.setState({ value });
  };
  render() {
    const { classes, tabs, cardTitle, headerColor } = this.props;
    const tabContent = tabs[this.state.value].content
    return (
      <Card className={classes.card}>
        <CardHeader
          classes={{
            root: classes.cardHeader + " " + classes[headerColor + "CardHeader"],
            title: classes.cardTitle,
            content: classes.cardHeaderContent
          }}
          title={cardTitle}
          action={
            <Tabs
              classes={{
                flexContainer: classes.tabsContainer,
                indicator: classes.displayNone
              }}
              value={this.state.value}
              onChange={this.handleChange}
              textColor="inherit"
            >
              {tabs.map((tab, i) => 
                  <Tab
                    key={i}
                    classes={{
                      wrapper: classes.tabWrapper,
                      labelIcon: classes.labelIcon,
                      label: classes.label,
                      textColorInheritSelected: classes.textColorInheritSelected
                    }}
                    icon={tab.icon}
                    label={tab.label}
                  />
              )}
            </Tabs>
          }
        />
        <CardContent>
          { tabContent }
        </CardContent>
      </Card>
    );
  }
}

TasbsCard.defaultProps = {
  headerColor: "green"
};

TasbsCard.propTypes = {
  classes: PropTypes.object.isRequired,
  headerColor: PropTypes.oneOf(["orange", "green", "red", "blue", "purple"]),
};

export default withStyles(tasbsCardStyle)(TasbsCard);
