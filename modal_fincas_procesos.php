<?php
    $session = Session::getInstance();
    $db = DB::getInstance($session->agent_user);
    $fincas = $db->queryAll("SELECT id, nombre, IFNULL((SELECT proceso FROM fincas_dias_proceso WHERE id_finca = fincas.id AND fecha = CURRENT_DATE), 0) selected FROM fincas WHERE status = 1");
?>

<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.9/css/mdb.min.css" rel="stylesheet">
<!-- SWEET ALERT -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style>
    .margin-5 {
        margin : 5px 0 !important;
    }
    .modal-dialog {
        margin-right : 100px !important;
    }
    .modal-dialog, p.heading {
        font-size : 15px !important;
    }
    a.btn {
        font-size: 12px !important;
        border-radius : 5px !important;
    }
    .md-form label {
        top : 0;
    }
    #modalPoll {
        z-index: 9999!important;
    }
</style>

<script>
    $(document).ready(() => {
        let fincas = []
        let modal = $("#modalPoll")

        function saveFincas(){
            $.ajax({
                method: "POST",
                url: "phrapi/access/fincasProceso",
                data: { fincas, comentario : $("#form79textarea").val() }
            })
        }

        function dontSave(){
            $.ajax({
                method: "POST",
                url: "phrapi/access/dontFincasProceso",
                data: { fincas : [], comentario : $("#form79textarea").val() }
            })
        }

        $(".mdb-checkbox").change((e) => {
            let input = $(e.target)
            let id_finca = input.val()
            if(fincas.includes(id_finca)) fincas.splice(fincas.indexOf(id_finca), 1)
            else fincas.push(id_finca)
        })

        $("#save_fincas").click((e) => {
            e.preventDefault()
            modal.modal('hide')
            saveFincas()
        })

        $("#dont_fincas").click((e) => {
            e.preventDefault()
            
            swal({
                text : '¿Estas seguro de esto?',
                icon : 'warning',
                buttons: ['No', 'Si'],
                dangerMode: true,
            })
            .then((r) => {
                if(r){
                    modal.modal('hide')
                    dontSave()
                }
            })
        })
    })
</script>

<div class="modal fade right" id="modalPoll" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <p class="heading lead">Fincas que procesan hoy</p>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="text-center">
                    <p class="margin-5"><strong>Mejor servicio</strong></p>
                    <p class="margin-5">Por favor ayúdanos a saber que fincas procesan hoy</p>
                </div>
                <hr>

                <!-- Fincas -->
                <p class="text-center">
                    <strong>Tus fincas</strong>
                </p>

                <?php foreach($fincas as $f): ?>
                <div class="form-check mb-4">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input mdb-checkbox" value="<?= $f->id ?>" id="f_<?= $f->id ?>" <?= $f->selected > 0 ? 'checked' : '' ?>>
                        <label class="custom-control-label" for="f_<?= $f->id ?>"><?= $f->nombre ?></label>
                    </div>
                </div>
                <?php endforeach ; ?>

                <!-- Fincas -->

                <p class="text-center">
                    <strong>¿Tienes algún inconveniente?</strong>
                </p>
                <!--Basic textarea-->
                <div class="md-form">
                    <textarea type="text" id="form79textarea" class="md-textarea form-control" rows="3"></textarea>
                    <label for="form79textarea">Déjanos un comentario</label>
                </div>

            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <a id="save_fincas" type="button" class="btn btn-success waves-effect waves-light">Guardar</a>
                <a id="dont_fincas" type="button" class="btn btn-danger waves-effect">Hoy no hay proceso</a>
                <a type="button" class="btn btn-warning waves-effect" data-dismiss="modal">Cancelar</a>
            </div>
        </div>
    </div>
</div>