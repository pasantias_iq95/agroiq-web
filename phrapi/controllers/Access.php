<?php
# defined('PHRAPI') or die("Direct access not allowed!");

class Access {
	
	private $config;
	private $session;
	private $db;
	private $api;
	private $apiCyt;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		$this->db = DB::getInstance();
	}

	public function is_logged() {
		return isset($this->session->logged);
	}

	public function if_not_logged_redirect() {
		if (!$this->is_logged()) {
			redirect("login.php");
		}
		if(!$this->session->unidad_racimos || !$this->session->unidad_cajas){
			$this->logout();
		}
	}

	public function if_logged_redirect() {
		if ($this->is_logged()) {
			redirect("index.php");
		}
	}

	public function logout($redirect = "login.php") {
		$this->session->kill();
		redirect($redirect);
	}


	public function login()
	{
		$data = [
			'user' => preg_replace("~[^\-\_\@\.a-z0-9]~i", "", trim(strtolower(postString('username')))),
			'pass' => preg_replace("/[^a-z0-9\,\.\-\_]/i", "", postString('password'))
		];

		if (empty($data['user']) OR empty($data['pass'])) {
			redirect("login.php");
		}

		$consulta = "SELECT * FROM users WHERE usuario = '".$data['user']."'  AND pass = PASSWORD('".$data['pass']."');";
		$account = $this->db->queryRow($consulta);
		if(isset($account->id)){
			$this->session->logged = $account->id;
			$this->session->db = $account->bd;
			$this->session->id_company = $account->id_company;
			$this->session->company_alias = $this->db->queryOne("SELECT alias FROM companies WHERE id = $account->id_company");
			$this->session->nombre = $account->nombre;
			$this->session->usuario = $account->usuario;
			$this->session->agent_user = $account->access;
			$this->session->alias = $account->alias;
			$this->session->type_users = $account->type_users;
			$this->session->languages = "ES";
			$this->session->memberships = new stdClass;
			$this->session->memberships = $this->loadMemberships($account->id_company);
			$this->session->settings = new stdClass;
			$this->session->settings = $this->loadSettings($account->id_company);
			$this->session->privileges = new stdClass;
			$this->session->privileges = $this->loadPrivileges($account->id);
			$this->session->umbrals = new stdClass;
            $this->session->umbrals = $this->loadUmbrals($account->id_company);
			$this->session->cultivos = $this->getCultivos($account->id_company);

			// BEGIN UNIDADES
			$this->session->const_unidad_racimos = $this->db->queryOne("SELECT balanza_racimos_unidad FROM companies WHERE id = {$this->session->id_company}");
			$this->session->unidad_racimos = $this->db->queryOne("SELECT balanza_racimos_unidad FROM companies WHERE id = {$this->session->id_company}");
			$this->session->unidad_cajas = $this->db->queryOne("SELECT balanza_cajas_unidad FROM companies WHERE id = {$this->session->id_company}");
			// END UNIDADES

			$this->session->routes = $this->db->queryAllSpecial("SELECT route as id, module as label FROM companies_routes WHERE id_company = '{$account->id_company}'");
			if($this->session->agent_user == "marcel"){
				$this->session->menu_modules = new stdClass;
				$this->session->menu_modules = $this->db->queryAllSpecial("SELECT name as id, users_modules_assigns.status as label
					FROM users_modules_assigns 
					INNER JOIN companies_menu_modules ON id_module = companies_menu_modules.id
					WHERE id_company = {$account->id_company} AND id_user = '{$account->id}'");

				$this->session->references = new stdClass;
				$this->session->references->brands = $this->getBrands($account->access, $account->bd);

				if($account->type_users != 'ADMIN'){
					$this->session->settings = $this->loadSettingsUsers($account->id);
					$this->session->references->clients = $this->loadReference($account->access , $account->bd);
				}
			}
			if($this->session->agent_user == "marlon"){
				$this->session->settings = $this->loadSettingsUsers($account->id);
			}
			redirect($this->loadSystem());
		}else{
			redirect("login.php");
		}
	}

	public function ping() {
		echo date("Y-m-d H:i:s");
	}
	private function loadMemberships($id_company){
		$memberships = [];
		$consulta = "SELECT LOWER(membresia) AS id ,LOWER(membresia) AS label FROM memberships  WHERE id_company = '{$id_company}' AND CURRENT_TIMESTAMP BETWEEN fecha_start AND fecha_end";
		$memberships = $this->db->queryAllSpecial($consulta);
		return $memberships;
	}

	private function loadUmbrals($id_company){
		$settings = [];
		$consulta = "SELECT * FROM umbral  WHERE id_company = '{$id_company}'";
		$settings = $this->db->queryRow($consulta);
		return $settings;
	}

	private function loadPrivileges($account_id){
		$privileges = [];
		$consulta = "SELECT * FROM users_privileges WHERE id_user = {$account_id}";
		$privileges = $this->db->queryRow($consulta);
		return $privileges;
	}

	private function loadSettings($id_company){
		$settings = [];
		$consulta = "SELECT * FROM companies_modules WHERE id_company = '{$id_company}'";
		$settings = $this->db->queryRow($consulta);
		return $settings;
	}

	private function loadSettingsUsers($account_id){
		$settings = [];
		$consulta = "SELECT * FROM users_modules WHERE id_user = '{$account_id}'";
		$settings = $this->db->queryRow($consulta);
		return $settings;
	}

	private function loadReference($agent_user , $bd){
		$db_user = DB::getInstance($agent_user);
		$modules = [];
		$consulta = "SELECT id_reference AS id , 
			    (SELECT {$bd}.clients.nombre FROM {$bd}.clients WHERE id = id_reference) AS label 
		    FROM users_modules_clients WHERE id_user = '{$this->session->logged}'";
		$modules = $this->db->queryAllSpecial($consulta);
		return $modules;
	}

	private function getBrands($agent_user, $bd){
		$db_user = DB::getInstance($agent_user);
		$modules = [];
		$condition = "";
		if($this->session->type_users != 'ADMIN'){
			$condition = "INNER JOIN users_modules_clients ON {$bd}.clients.id = users_modules_clients.id_reference
					WHERE id_user = '{$this->session->logged}'";
		}
		$consulta = "SELECT {$bd}.brands.id_client , (SELECT {$bd}.clients.nombre FROM {$bd}.clients WHERE id = {$bd}.brands.id_client) AS cliente ,
					{$bd}.brands.id , {$bd}.brands.nombre AS marca
					FROM {$bd}.brands
					INNER JOIN {$bd}.clients ON {$bd}.brands.id_client = {$bd}.clients.id
					$condition";
		$modules = $this->db->queryAll($consulta);
		return $modules;
	}

	private function loadSystem(){
		$page = "start.php";
		$count = 0;
		foreach ($this->session->settings as $key => $value) {
			if($value == 'Activo'){
				$count++;
				$page = $key;
			}
		}
		if($page == "lancofruit"){
			$page = "rastreoLancofruit";
		}
		if($this->session->logged == 49){
			$page = "perchas";
		} else if($count > 1 || $this->session->type_users == "ADMIN"){
			$page = "start.php";
        }
        if($this->session->type_users == "USER" && count($this->session->references->brands) > 0){
            $brand = $this->session->references->brands[0];
            $page = "calidad?c=$brand->cliente&m=$brand->marca";
        }
		return $page;
    }

    public function getInfoDia(){
        $response = ["status" => "failure"];
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $usuario = getValueFrom($postdata, "usuario", '');
        
        $exist = $this->db->queryOne("SELECT COUNT(1) FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
        if($exist){
            $response["status"] = "success";
            $response["horas_totales"] = $this->db->queryOne("SELECT TIME_TO_SEC(tiempo_laborado) * 1000 FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["horas_extra"] = $this->db->queryOne("SELECT TIME_TO_SEC(horas_extra) * 1000 FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["hora_inicio"] = $this->db->queryOne("SELECT hora_entrada FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["hora_fin"] = $this->db->queryOne("SELECT hora_salida FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["milis_hora_fin"] = $this->db->queryOne("SELECT IFNULL(TIME_TO_SEC(hora_salida), 0) * 1000 FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["milis_hora_inicio"] = $this->db->queryOne("SELECT TIME_TO_SEC(hora_entrada) * 1000 FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["horas"] =  $this->db->queryOne("SELECT FLOOR((TIME_TO_SEC(TIME(NOW())) - TIME_TO_SEC(hora_entrada)) / 60 / 60)
                                                        FROM itinerario
                                                        WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["minutos"] = $this->db->queryOne("SELECT FLOOR(((TIME_TO_SEC(TIME(NOW())) - TIME_TO_SEC(hora_entrada)) - ({$response["horas"]} * 60 * 60)) / 60)
                                                        FROM itinerario
                                                        WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["segundos"] = $this->db->queryOne("SELECT FLOOR(((TIME_TO_SEC(TIME(NOW())) - TIME_TO_SEC(hora_entrada)) - ({$response["horas"]} * 60 * 60) - ({$response["minutos"]} * 60)))
                                                        FROM itinerario
                                                        WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["observaciones"] = $this->db->queryOne("SELECT IFNULL(observaciones,  '') FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["hora_comida"] = $this->db->queryOne("SELECT hora_comida FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            $response["finish"] = $this->db->queryOne("SELECT COUNT(1) FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE AND hora_salida IS NOT NULL");
        }else{
            $response["message"] = "El dia todavia no inicia";
        }
        return $response;
    }
    
    public function iniciarJornada(){
        $response = ["status" => "failure", "message" => ""];
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $usuario = getValueFrom($postdata, "usuario", '');

        if(!empty($usuario)){
            $inicio = $this->db->queryOne("SELECT COUNT(1) FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            if($inicio == 0){
                $this->db->query("INSERT INTO itinerario SET usuario = '{$usuario}', fecha = CURRENT_DATE, hora_entrada = TIME(NOW()), semana = WEEK(CURRENT_DATE), anio = YEAR(CURRENT_DATE)");
                $response = ["status" => "success", "message" => "Se inicio la jornada"];
            }else{
                $response["message"] = "La jornada ya esta iniciada";
            }
        }else{
            $response["message"] = 'El usuario es requerido';
        }
        return $response;
    }

    public function terminarJornada(){
        $response = ["status" => "failure"];
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $usuario = getValueFrom($postdata, "usuario", '');
        
        if(!empty($usuario)){
            $inicio = (int) $this->db->queryOne("SELECT COUNT(1) FROM itinerario WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            if($inicio > 0){
                $this->db->query("UPDATE itinerario SET 
                                        hora_salida = TIME(NOW()), 
                                        tiempo_laborado = TIMEDIFF(NOW(), CONCAT(fecha, ' ', hora_entrada)),
                                        horas_extra = SUBTIME(TIMEDIFF(NOW(), CONCAT(fecha, ' ', hora_entrada)), IF(hora_comida = 0, '08:00:00', '09:00:00'))
                                WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
                $response["status"] = "success";
                $response["message"] = "Finalizo jornada";
            }else{
                $response["message"] = "No se puede terminar la jornada";
            }
        }else{
            $response["message"] = "Usuario requerido";
        }
        return $response;
    }

    public function observaciones(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $usuario = getValueFrom($postdata, "usuario", '');
        $observaciones = getValueFrom($postdata, 'observaciones', '');

        if(!empty($usuario) && !empty($observaciones)){
            $this->db->query("UPDATE itinerario SET observaciones = '{$observaciones}' WHERE usuario = '{$usuario}' AND fecha = CURRENT_DATE");
            return 'Listo';
        }else{
            return 'Las observaciones no pueden estar vacias';
        }
    }

    private function getCultivos($id_company){
        $cultivos = $this->db->queryRow("SELECT cacao, banano, palma FROM companies_cultivos WHERE id_company = $id_company");
        return $cultivos;
    }

    public function setModule(){
        $this->session->moduleSystem = $_POST['module'];
        return $this->session->moduleSystem;
	}
	
	public function saveFincasProceso(){
		$response = new stdClass;
		$response->status = 200;

		$db = DB::getInstance($this->session->agent_user);

		$sql = "SELECT id, nombre FROM fincas WHERE status = 1";
		$fincas = $db->queryAll($sql);

		foreach($fincas as $f){
			$proceso = in_array($f->id, $_POST['fincas']) ? 1 : 0;
			$e = (int) $db->queryOne("SELECT COUNT(1) FROM fincas_dias_proceso WHERE id_finca = {$f->id} AND fecha = CURRENT_DATE");
			if($e == 0){
				$sql = "INSERT INTO fincas_dias_proceso SET
							id_finca = $f->id,
							proceso = $proceso,
							fecha =  CURRENT_DATE";
			}else{
				$sql = "UPDATE fincas_dias_proceso SET
							proceso = {$proceso}
						WHERE fecha = CURRENT_DATE AND id_finca = {$f->id}";
			}
			$db->query($sql);
		}

		if($_POST['comentario'] != ''){
			$sql = "INSERT INTO fincas_dias_proceso_comentario SET
						fecha = CURRENT_DATE,
						comentario = '{$_POST['comentario']}'";
			$db->query($sql);
		}

		return $response;
	}

	public function dontFincasProceso(){
		$response = new stdClass;
		$response->status = 200;

		$db = DB::getInstance($this->session->agent_user);

		$sql = "SELECT id, nombre FROM fincas WHERE status = 1";
		$fincas = $db->queryAll($sql);

		foreach($fincas as $f){
			$sql = "INSERT INTO fincas_dias_proceso SET
						id_finca = $f->id,
						proceso = 0,
						fecha =  CURRENT_DATE";
			$db->query($sql);
		}

		if($_POST['comentario'] != ''){
			$sql = "INSERT INTO fincas_dias_proceso_comentario SET
						fecha = CURRENT_DATE,
						comentario = '{$_POST['comentario']}'";
			$db->query($sql);
		}

		return $response;
	}
}
