<?php defined('PHRAPI') or die("Direct access not allowed!");

class Agroaudit {
    public $name;
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        // D($this->session);
        // $this->token = '289150995ed28e8860f9161d3cc9f259';
        // $this->Service = new ServicesNode('http://procesos-iq.com:3000/' , $this->token);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;
        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        return $response;
    }
    
    /*===============================================
    =            SECCION NIVEL HACIENDA [ ZONAS ]            =
    ===============================================*/
    
    public function getHaciendaZonas(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $lote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);


        $response = new stdClass; 
        
        // $response = $this->Service->post("zonas/",$postdata);
        // D($response);

        // $months = ["Ene" , "Feb" , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep", "Oct" , "Nov", "Dic"];
        $sql = "SELECT nombre ,'' AS value FROM labores";
        $laboresFilter = $this->db->queryAll($sql);
        $labores = [];
        $labor_ = ["nombre" => "" , "labores" => []];
        foreach ($laboresFilter as $key => $value) {
            $value = (object)$value;
            $labor_[$value->nombre] = "";
            $labores[$value->nombre] = ["filter" => $value->nombre , "value" => ""] ;
        }

        $response->labores_mes = $months;
        $response->causas->zonas = [];
        $response->data_labores = [];
        $response->data_labores_zonas = [];
        $response->labores = [];
        $response->filterLabores = $labores;
        $response->tittle = "TODO";
        $lote = "";
        $labor = "";
        $tipo_labor = "";
        $promedio = 0;


        $sql = "SELECT STRAIGHT_JOIN idZona , (SELECT nombre FROM zonas WHERE id = idZona) AS nombre , AVG(promedio) AS promedio ,
                (SELECT CONCAT_WS(' ',nombre,apellidos) AS nombre FROM personal WHERE id = (SELECT idAdministrador FROM zonas WHERE id = idZona)) AS personal
                FROM 
                (SELECT STRAIGHT_JOIN m.idZona , m.idFinca , AVG(Lotes.promedio) AS promedio FROM muestras_anual AS m
                INNER JOIN  (SELECT AVG(promedio) AS promedio , idLote , idFinca , idZona FROM (SELECT promedio ,idLote , idFinca , idZona
                            FROM muestras_anual
                            WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                            GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                            ORDER BY idLote) AS temp
                            GROUP BY idLote        
                            ORDER BY idLote) as Lotes 
                ON Lotes.idZona = m.idZona AND Lotes.idFinca = m.idFinca 
                WHERE m.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                GROUP BY m.idZona , m.idFinca) AS fincas
                GROUP BY idZona";
        $response->causas->zonas = $this->db->queryAll($sql);

        $sql_labores = "SELECT STRAIGHT_JOIN 
                        idLabor, (SELECT nombre FROM labores WHERE id = idLabor) AS labor,idTipoLabor,
                        IF(idTipoLabor = 0 , 'LABORES',(SELECT nombre FROM tipo_labor WHERE id = idTipoLabor)) AS tipo_labor, AVG(promedio) AS promedio 
                        FROM 
                        (SELECT STRAIGHT_JOIN m.idZona ,(SELECT nombre FROM zonas WHERE id = m.idZona) AS zona, m.idLabor, (SELECT nombre FROM labores WHERE id = m.idLabor) AS labor , 
                        AVG(Lotes.promedio) AS promedio , idTipoLabor FROM muestras_anual AS m
                        INNER JOIN  (SELECT AVG(promedio) AS promedio , idLote , idFinca , idZona , idLabor FROM (
                                    SELECT promedio ,idLote , idFinca , idZona , idLabor
                                    FROM muestras_anual
                                    WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                                    GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                                    ORDER BY idLabor) AS temp
                                    GROUP BY idLote , idLabor        
                                    ORDER BY idLabor) as Lotes 
                        ON Lotes.idZona = m.idZona AND m.idLabor = Lotes.idLabor
                        WHERE m.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                        GROUP BY m.idZona ,m.idLabor
                        ORDER BY m.idZona , m.idLabor) AS zonas
                        GROUP BY idLabor
                        ORDER BY AVG(promedio)";
        $respuesta = $this->db->queryAll($sql_labores);
        
        $low_labor = [];
        foreach ($respuesta as $key => $value) {
            $low_labor[] = $value;
            $response->data_labores[$value->tipo_labor][$value->idLabor] = $value;
        }

        $sql_labores_zonas = "SELECT STRAIGHT_JOIN m.idZona ,(SELECT nombre FROM zonas WHERE id = m.idZona) AS zona, m.idLabor, (SELECT nombre FROM labores WHERE id = m.idLabor) AS labor , 
                        TRUNCATE(AVG(Lotes.promedio), 2) AS promedio FROM muestras_anual AS m
                        INNER JOIN  (SELECT AVG(promedio) AS promedio , idLote , idFinca , idZona , idLabor FROM (
                                    SELECT promedio ,idLote , idFinca , idZona , idLabor
                                    FROM muestras_anual
                                    WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                                    GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                                    ORDER BY idLabor) AS temp
                                    GROUP BY idLote , idLabor        
                                    ORDER BY idLabor) as Lotes 
                        ON Lotes.idZona = m.idZona AND m.idLabor = Lotes.idLabor
                        WHERE m.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' 
                        GROUP BY m.idZona ,m.idLabor
                        ORDER BY m.idZona , m.idLabor";
        $respuesta = $this->db->queryAll($sql_labores_zonas);
        foreach ($respuesta as $key => $value) {
            if(!in_array($value->zona, $response->data_labores_zonas["zonas"][$value->zona])){
                $response->data_labores_zonas["zonas"][$value->zona] = $labor_;
                $response->data_labores_zonas["zonas"][$value->zona]["labores"] = $labores;
            }
            $response->data_labores_zonas["zonas"][$value->zona]["nombre"] = $value->zona;
            $response->data_labores_zonas["zonas"][$value->zona]["labores"][$value->labor]["filter"] = $value->labor;
            $response->data_labores_zonas["zonas"][$value->zona]["labores"][$value->labor]["value"] = $value->promedio;
        }

        $sql_lote_bajo = "SELECT zona ,finca , lote , labor , AVG(promedio) AS prom  FROM
        (SELECT       
        (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
        WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'  #AND idZona = 1 #AND idFinca = 1
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY zona ,finca , lote
        ORDER BY AVG(promedio) ASC, zona ,finca , lote
        LIMIT 1";
        $response->low_lote = $this->db->queryRow($sql_lote_bajo);

        $response->low_labor = $low_labor[0];

        // $response->principal = [];
        // $labor = ""; 
        // $labores = "";
        // foreach ($months as $key => $value) {
        //     $labor = $this->getPromMonthLote(($key+1),$lote , $idLabor  , $idfinca);
        //     $labores = $this->getPromMonthGeneralLote(($key+1) ,$lote , $idLabor , $idfinca );
        //     // D($labor);
        //     // D($labores);
        //     $response->principal[] = [ 
        //         $value , 
        //         strtoupper($labor->lote) => $labor->promedioLote,
        //         strtoupper($labores->lotes) => $labores->promedioLote,
        //     ];
        // }

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }


        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY semana";
        $sql_zones_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from zonas where id = idZona) AS zona,idZona FROM muestras_anual
         WHERE YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idZona , mes";
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from zonas where id = idZona) AS zona,idZona  FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY idZona , semana";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas = new stdClass;
        $response->zonas->zonas = [0 => $this->session->alias] + $this->db->queryAllSpecial("SELECT (SELECT nombre from zonas where id = idZona) AS id  , (SELECT nombre from zonas where id = idZona) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY idZona");
        $months_zones = $this->db->queryAll($sql_zones_month);
        $weeks_zones = $this->db->queryAll($sql_zones_week);
        $response->zonas->auditoria = $this->db->queryAll($sql_zones_month);
        $response->auditoria = [];

        foreach ($months_zones as $key => $value) {
            $response->zonas->months[$value->zona][$value->id] = $this->getPromZonaMonth($value->idZona , $value->mes)->promedio;
        }

        foreach ($weeks_zones as $key => $value) {
            $response->zonas->weeks[$value->zona][$value->label] = $this->getPromZonaWeek($value->idZona , $value->label)->promedio;
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        // D($this->session);
        return $response;
    }
    
    private function getPromZonaMonth($idZona , $month){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE YEAR(fecha) = YEAR(CURRENT_DATE) AND idZona = '{$idZona}' AND mes = '{$month}' 
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromZonaWeek($idZona , $week){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE YEAR(fecha) = YEAR(CURRENT_DATE) AND idZona = '{$idZona}' AND semana = '{$week}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }

    /*=====  End of SECCION NIVEL HACIENDA [ ZONAS ]  ======*/

    /*============================================
    =            SECCION NIVEL ZONAS [FINCAS]            =
    ============================================*/
    public function getZonaFinca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $lote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }


        $sql = "SELECT nombre ,'' AS value FROM labores";
        $laboresFilter = $this->db->queryAll($sql);
        $labores = [];
        $labor_ = ["nombre" => "" , "labores" => []];
        foreach ($laboresFilter as $key => $value) {
            $value = (object)$value;
            $labor_[$value->nombre] = 0;
            $labores[$value->nombre] = ["filter" => $value->nombre , "value" => ""] ;
        }

        $response = new stdClass; 
        /*----------  TITTLE  ----------*/
        $tittle = new stdClass;
        $sql_tittle = "SELECT nombre AS zona FROM zonas WHERE id = {$idZona}";
        $tittle = $this->db->queryRow($sql_tittle);
        if($this->session->id_company == 4){
            $response->tittle = "Orodelti ";
        }elseif($this->session->id_company == 7){
            $response->tittle = "";
        }else{
            $response->tittle = "ZONA ".$tittle->zona;
        }

        $response->labores_mes = $months;
        $response->causas = new stdClass;
        $response->data_labores = [];
        $response->filterLabores = $labores;
        $response->type_labor = [];
        // $response->data_labores_lotes = [];
        $response->labores = [];
        $lote = "";
        $labor = "";
        $tipo_labor = "";
        $promedio = 0;

        $sql = "SELECT STRAIGHT_JOIN m.idZona ,(SELECT nombre FROM zonas WHERE id = m.idZona) AS zona, m.idFinca ,
                (SELECT nombre FROM fincas WHERE id = m.idFinca) AS nombre, AVG(Lotes.promedio) AS promedio ,
                (SELECT CONCAT_WS(' ',nombre,apellidos) AS nombre FROM personal WHERE id = (SELECT idJefeSector FROM fincas WHERE id = m.idFinca)) AS personal
                FROM muestras_anual AS m
                INNER JOIN  (SELECT AVG(promedio) AS promedio , idLote , idFinca , idZona FROM (SELECT promedio ,idLote , idFinca , idZona
                            FROM muestras_anual
                            WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                            -- GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                            ORDER BY idLote) AS temp
                            GROUP BY idLote        
                            ORDER BY idLote) as Lotes 
                ON Lotes.idZona = m.idZona AND Lotes.idFinca = m.idFinca 
                WHERE m.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND m.idZona = '{$idZona}'
                GROUP BY m.idZona , m.idFinca";
            // D($sql);
        $response->causas->fincas = $this->db->queryAll($sql);

        $sql_labores = "SELECT STRAIGHT_JOIN idZona , (SELECT nombre FROM zonas WHERE id = idZona) AS zona ,idTipoLabor,idFinca,(SELECT nombre FROM fincas WHERE id = idFinca) AS finca,
                        IF(idTipoLabor = 0 , 'LABORES',(SELECT nombre FROM tipo_labor WHERE id = idTipoLabor)) AS tipo_labor,
                        idLabor, (SELECT nombre FROM labores WHERE id = idLabor) AS labor, AVG(promedio) AS promedio 
                        FROM 
                        (SELECT STRAIGHT_JOIN m.idZona ,(SELECT nombre FROM zonas WHERE id = m.idZona) AS zona, m.idLabor,m.idFinca, (SELECT nombre FROM labores WHERE id = m.idLabor) AS labor , 
                        AVG(Lotes.promedio) AS promedio , m.idTipoLabor FROM muestras_anual AS m
                        INNER JOIN  (SELECT AVG(promedio) AS promedio , idLote , idFinca , idZona , idLabor FROM (
                                    SELECT IFNULL(promedio,0) AS promedio ,idLote , idFinca , idZona , idLabor
                                    FROM muestras_anual
                                    WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                                    -- GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                                    ORDER BY idLabor) AS temp
                                    GROUP BY idLote , idLabor        
                                    ORDER BY idLabor) as Lotes 
                        ON Lotes.idZona = m.idZona AND m.idLabor = Lotes.idLabor AND m.idFinca = Lotes.idFinca 
                        WHERE m.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND m.idZona = '{$idZona}'
                        GROUP BY m.idZona ,m.idLabor
                        ORDER BY m.idZona , m.idLabor) AS zonas
                        GROUP BY idLabor 
                        ORDER BY idTipoLabor";
        
        $response->sql_labores = $sql_labores;
        $respuesta = $this->db->queryAll($sql_labores);
        $low_labor = [];
        $prom_type_labor = [];
        foreach ($respuesta as $key => $value) {
            $low_labor[] = $value;
            $response->data_labores[$value->tipo_labor][$value->idLabor] = $value;
            $prom_type_labor[$value->tipo_labor][] = $value->promedio;
        }

        foreach ($prom_type_labor as $key => $value) {
            $response->type_labor[$key] = array_sum($prom_type_labor[$key]) / count($prom_type_labor[$key]);
        }

         $sql_labores_zonas = "SELECT STRAIGHT_JOIN m.idZona  , m.idFinca ,(SELECT nombre FROM fincas WHERE id = m.idFinca) AS finca, m.idLabor, (SELECT nombre FROM labores WHERE id = m.idLabor) AS labor , 
                        TRUNCATE(AVG(Lotes.promedio), 2) AS promedio FROM muestras_anual AS m
                        INNER JOIN  (SELECT AVG(promedio) AS promedio , idLote , idFinca , idZona , idLabor FROM (
                                    SELECT promedio ,idLote , idFinca , idZona , idLabor
                                    FROM muestras_anual
                                    WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                                    -- GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                                    ORDER BY idLabor) AS temp
                                    GROUP BY idLote , idLabor        
                                    ORDER BY idLabor) as Lotes 
                        ON Lotes.idZona = m.idZona AND Lotes.idFinca = m.idFinca AND m.idLabor = Lotes.idLabor
                        WHERE m.fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND m.idZona = '{$idZona}'
                        GROUP BY m.idFinca ,m.idLabor
                        ORDER BY m.idFinca , m.idLabor";

        $respuesta = $this->db->queryAll($sql_labores_zonas);
        foreach ($respuesta as $key => $value) {
            if(!in_array($value->finca, $response->data_labores_fincas["fincas"][$value->finca])){
                $response->data_labores_fincas["fincas"][$value->finca] = $labor_;
                $response->data_labores_fincas["fincas"][$value->finca]["labores"] = $labores;
            }
            $response->data_labores_fincas["fincas"][$value->finca]["nombre"] = $value->finca;
            $response->data_labores_fincas["fincas"][$value->finca]["labores"][$value->labor]["filter"] = $value->labor;
            $response->data_labores_fincas["fincas"][$value->finca]["labores"][$value->labor]["value"] = $value->promedio;
        }

        $sql_lote_bajo = "SELECT zona ,finca , lote , labor , AVG(promedio) AS prom  FROM
        (SELECT       
        (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
        WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND idZona = '{$idZona}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY zona ,finca , lote
        ORDER BY AVG(promedio) ASC, zona ,finca , lote
        LIMIT 1";
        $response->low_lote = $this->db->queryRow($sql_lote_bajo);

        $response->low_labor = $low_labor[0];

        $response->prom_general = 0;

        /* VICTOR LOW LABOR */
        $response->tags->labor = $this->db->queryRow("SELECT STRAIGHT_JOIN 
                                                        idFinca , CONCAT((SELECT nombre FROM fincas WHERE id = idFinca),'.',(SELECT nombre FROM labores WHERE id = idLabor)) AS label ,
                                                        IdLote , (SELECT nombre FROM lotes WHERE id = idLote) AS lote ,
                                                        idLabor , ROUND(AVG(promedio),2) AS VALUE
                                                        FROM (SELECT STRAIGHT_JOIN fecha , idZona ,idFinca , idTipoLabor,idLote , idLabor ,promedio
                                                        FROM muestras_anual
                                                        WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                                                        ORDER BY idLote) AS lote
                                                        GROUP BY idFinca, idLabor
                                                        ORDER BY value
                                                        LIMIT 1");
        /* VICTOR LOW LABOR */

        $sql_prom = "SELECT 
        AVG(promedio) AS promedio
        FROM  muestras_anual as m
        WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'";
        $response->prom_general = $this->db->queryRow($sql_prom);

        $response->low_personal = [];
        $sql_low_personal = "
        SELECT 
        (SELECT nombre FROM fincas WHERE id = idFinca) AS finca,
        (SELECT nombre FROM lotes WHERE id = idLote AND idFinca = details.idFinca) AS lote,
        (SELECT nombre FROM tipo_labor WHERE id = idTipoLabor) AS tipo_labor , 
        (SELECT personal FROM lotelabor_personal WHERE idLote = details.idLote AND idFinca = details.idFinca AND idTipoLabor =  details.idTipoLabor AND personal != '' Limit 1)
        AS personal,
        min(promedio) AS promedio 
        FROM
        (SELECT 
        idFinca,
        idLote,
        idTipoLabor,
        AVG(promedio) AS promedio
        FROM  muestras_anual as m
        WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
        GROUP BY idFinca , idLote , idTipoLabor) AS details";
        $response->low_personal = $this->db->queryRow($sql_low_personal);

        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 1 THEN 'Ene'
                WHEN mes = 2 THEN 'Feb'
                WHEN mes = 3 THEN 'Mar'
                WHEN mes = 4 THEN 'Abr'
                WHEN mes = 5 THEN 'May'
                WHEN mes = 6 THEN 'Jun'
                WHEN mes = 7 THEN 'Jul'
                WHEN mes = 8 THEN 'Ago'
                WHEN mes = 9 THEN 'Sep'
                WHEN mes = 10 THEN 'Oct'
                WHEN mes = 11 THEN 'Nov'
                WHEN mes = 12 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY semana";
        $sql_fincas_month = "SELECT CASE 
                WHEN mes = 1 THEN 'Ene'
                WHEN mes = 2 THEN 'Feb'
                WHEN mes = 3 THEN 'Mar'
                WHEN mes = 4 THEN 'Abr'
                WHEN mes = 5 THEN 'May'
                WHEN mes = 6 THEN 'Jun'
                WHEN mes = 7 THEN 'Jul'
                WHEN mes = 8 THEN 'Ago'
                WHEN mes = 9 THEN 'Sep'
                WHEN mes = 10 THEN 'Oct'
                WHEN mes = 11 THEN 'Nov'
                WHEN mes = 12 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from fincas where id = idFinca) AS finca,idFinca , idZona FROM muestras_anual
         WHERE YEAR(fecha) = YEAR(CURRENT_DATE) AND idZona = '{$idZona}'
        GROUP BY mes , idFinca";
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from fincas where id = idFinca) AS finca,idFinca , idZona  FROM muestras_anual
        WHERE idZona = '{$idZona}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idFinca, semana";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas = new stdClass;
        $response->zonas->fincas = [0 => $this->session->alias] + $this->db->queryAllSpecial("SELECT (SELECT nombre from fincas where id = idFinca) AS id  , (SELECT nombre from fincas where id = idFinca) AS label FROM muestras_anual 
            WHERE idZona = '{$idZona}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idFinca");
        $months_fincas = $this->db->queryAll($sql_fincas_month);
        $weeks_fincas = $this->db->queryAll($sql_zones_week);
        $response->zonas->auditoria = $this->db->queryAll($sql_fincas_month);
        $response->auditoria = [];

        foreach ($months_fincas as $key => $value) {
            $response->zonas->months[$value->finca][$value->id] = $this->getPromFincaMonth($value->idZona , $value->mes , $value->idFinca)->promedio;
        }

        foreach ($weeks_fincas as $key => $value) {
            $response->zonas->weeks[$value->finca][$value->label] = $this->getPromFincaWeek($value->idZona , $value->label , $value->idFinca)->promedio;
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        // D($this->session);
        return $response;
    }

    private function getPromFincaMonth($idZona , $month , $idFinca){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND mes = '{$month}' AND idFinca = '{$idFinca}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromFincaWeek($idZona , $week , $idFinca){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND semana = '{$week}' AND idFinca = '{$idFinca}' AND YEAR(fecha) = YEAR(CURRENT_DATE) 
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    /*=====  End of SECCION NIVEL ZONAS [FINCAS]  ======*/
    
    /*============================================
    =            SECCION NIVEL LOTES [LOTES]            =
    ============================================*/
    public function getFincaLotes(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $lote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }


        $sql = "SELECT nombre ,'' AS value FROM labores";
        $laboresFilter = $this->db->queryAll($sql);
        $labores = [];
        $labor_ = ["nombre" => "" , "labores" => []];
        foreach ($laboresFilter as $key => $value) {
            $value = (object)$value;
            $labor_[$value->nombre] = 0;
            $labores[$value->nombre] = ["filter" => $value->nombre , "value" => ""] ;
        }
        $response = new stdClass; 
        /*----------  TITTLE  ----------*/
        $tittle = new stdClass;
        $sql_tittle = "SELECT
                        (SELECT nombre FROM zonas WHERE id = '{$idZona}') AS zona,
                        (SELECT nombre FROM fincas WHERE id = '{$idfinca}') AS finca";
        $tittle = $this->db->queryRow($sql_tittle);
        if($this->session->id_company == 4){
            $response->tittle = "Orodelti . ".$tittle->finca;
        }elseif($this->session->id_company == 7){
            $response->tittle = "".$tittle->finca;
        }else{
            $response->tittle = "ZONA ".$tittle->zona.". ".$tittle->finca;
        }

        $response->labores_mes = $months;
        $response->causas = new stdClass;
        $response->personal = new stdClass;
        $response->data_labores = [];
        $response->filterLabores = $labores;
        // $response->data_labores_lotes = [];
        $response->labores = [];
        $lote = "";
        $labor = "";
        $tipo_labor = "";
        //
        $sql = "SELECT STRAIGHT_JOIN idZona , (SELECT nombre FROM zonas WHERE id = idZona) AS Zona ,
                idFinca , (SELECT nombre FROM fincas WHERE id = idFinca) AS Finca ,
                idLote , (SELECT nombre FROM lotes WHERE id = idLote) AS lote ,
                (SELECT idSupervisorCampo FROM lotes WHERE id = idLote) AS idPersonal , 
                CASE 
                    WHEN (SELECT idSupervisorCampo FROM lotes WHERE id = idLote) > 0 THEN
                        (SELECT CONCAT_WS(' ',nombre,apellidos) AS nombre FROM personal WHERE id = (SELECT idSupervisorCampo FROM lotes WHERE id = idLote))
                    ELSE NULL
                END AS personal ,
                CASE 
                    WHEN (SELECT idSupervisorCampo2 FROM lotes WHERE id = idLote) > 0 THEN
                        (SELECT CONCAT_WS(' ',nombre,apellidos) AS nombre FROM personal WHERE id = (SELECT idSupervisorCampo2 FROM lotes WHERE id = idLote))
                    ELSE NULL
                END AS personal2,
                AVG(promedio) AS promedio
                FROM (SELECT STRAIGHT_JOIN fecha , idZona ,idFinca , idLote , idLabor ,promedio
                FROM muestras_anual
                WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                AND idZona = '{$idZona}' AND idFinca = '$idfinca'
                -- GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                ORDER BY idLote) AS lote
                GROUP BY idLote
                ORDER BY idLote";
            // D($sql);
        $response->causas->lote = $this->db->queryAll($sql);

        /*----------  PERSONAL  ----------*/
        $sql_personal = "SELECT STRAIGHT_JOIN
                (SELECT idSupervisorCampo FROM lotes WHERE id = idLote) AS idPersonal , 
                CASE 
                    WHEN (SELECT idSupervisorCampo FROM lotes WHERE id = idLote) > 0 THEN
                        (SELECT CONCAT_WS(' ',nombre,apellidos) AS nombre FROM personal WHERE id = (SELECT idSupervisorCampo FROM lotes WHERE id = idLote))
                    ELSE NULL
                END AS personal ,
                CASE 
                    WHEN (SELECT idSupervisorCampo2 FROM lotes WHERE id = idLote) > 0 THEN
                        (SELECT CONCAT_WS(' ',nombre,apellidos) AS nombre FROM personal WHERE id = (SELECT idSupervisorCampo2 FROM lotes WHERE id = idLote))
                    ELSE NULL
                END AS personal2,
                AVG(promedio) AS promedio
                FROM (SELECT STRAIGHT_JOIN fecha , idZona ,idFinca , idLote , idLabor ,promedio
                FROM muestras_anual
                WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                AND idZona = '{$idZona}' AND idFinca = '$idfinca'
                -- GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                ORDER BY idLote) AS lote
                GROUP BY (SELECT idSupervisorCampo FROM lotes WHERE id = idLote)
                ORDER BY (SELECT idSupervisorCampo FROM lotes WHERE id = idLote)";
        $response->personal = $this->db->queryAll($sql_personal);

        $sql_labores = "SELECT STRAIGHT_JOIN idZona , (SELECT nombre FROM zonas WHERE id = idZona) AS Zona ,
                        idFinca , (SELECT nombre FROM fincas WHERE id = idFinca) AS Finca ,
                        idLabor , (SELECT nombre FROM labores WHERE id = idLabor) AS labor ,
                        IF(idTipoLabor = 0 , 'LABORES',(SELECT nombre FROM tipo_labor WHERE id = idTipoLabor)) AS tipo_labor,
                        AVG(promedio) AS promedio
                        FROM (SELECT STRAIGHT_JOIN fecha , idZona ,idFinca , idTipoLabor,idLote , idLabor ,promedio
                        FROM muestras_anual
                        WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                        AND idZona = '{$idZona}' AND idFinca = '{$idfinca}'
                        -- GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                        ORDER BY idLote) AS lote
                        GROUP BY idLabor
                        ORDER BY idLabor ";
        
        $respuesta = $this->db->queryAll($sql_labores);
        $low_labor = [];
        $prom_type_labor = [];
        $response->type_labor = [];
        foreach ($respuesta as $key => $value) {
            $low_labor[] = $value;
            $response->data_labores[$value->tipo_labor][$value->idLabor] = $value;
            $prom_type_labor[$value->tipo_labor][] = $value->promedio;
        }

        foreach ($prom_type_labor as $key => $value) {
            $response->type_labor[$key] = array_sum($prom_type_labor[$key]) / count($prom_type_labor[$key]);
        }
         $sql_labores_zonas = "SELECT STRAIGHT_JOIN idZona , (SELECT nombre FROM zonas WHERE id = idZona) AS Zona ,
                        idFinca , (SELECT nombre FROM fincas WHERE id = idFinca) AS finca ,
                        IdLote , (SELECT nombre FROM lotes WHERE id = idLote) AS lote ,
                        idLabor , (SELECT nombre FROM labores WHERE id = idLabor) AS labor ,
                        IF(idTipoLabor = 0 , 'LABORES',(SELECT nombre FROM tipo_labor WHERE id = idTipoLabor)) AS tipo_labor,
                        AVG(promedio) AS promedio
                        FROM (SELECT STRAIGHT_JOIN fecha , idZona ,idFinca , idTipoLabor,idLote , idLabor ,promedio
                        FROM muestras_anual
                        WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                        AND idZona = '{$idZona}' AND idFinca = '{$idfinca}'
                        -- GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                        ORDER BY idLote) AS lote
                        GROUP BY idLote ,idLabor
                        ORDER BY idLote ,idLabor ";
        $respuesta = $this->db->queryAll($sql_labores_zonas);
        foreach ($respuesta as $key => $value) {
            if(!in_array($value->lote, $response->data_labores_lotes["lotes"][$value->lote])){
                $response->data_labores_lotes["lotes"][$value->lote] = $labor_;
                $response->data_labores_lotes["lotes"][$value->lote]["labores"] = $labores;
            }
            $response->data_labores_lotes["lotes"][$value->lote]["nombre"] = $value->lote;
            $response->data_labores_lotes["lotes"][$value->lote]["labores"][$value->labor]["filter"] = $value->labor;
            $response->data_labores_lotes["lotes"][$value->lote]["labores"][$value->labor]["value"] = $value->promedio;
        }

        $sql_lote_bajo = "SELECT zona ,finca , lote , labor , AVG(promedio) AS prom  FROM
        (SELECT       
        (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
        WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND idZona = '{$idZona}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY zona ,finca , lote
        ORDER BY AVG(promedio) ASC, zona ,finca , lote
        LIMIT 1";
        $response->low_lote = $this->db->queryRow($sql_lote_bajo);

        $response->low_labor = $low_labor[0];

        $response->low_personal = [];
        $sql_low_personal = "
        SELECT 
        (SELECT nombre FROM fincas WHERE id = idFinca) AS finca,
        (SELECT nombre FROM lotes WHERE id = idLote AND idFinca = details.idFinca) AS lote,
        (SELECT nombre FROM tipo_labor WHERE id = idTipoLabor) AS tipo_labor , 
        (SELECT personal FROM lotelabor_personal WHERE idLote = details.idLote AND idFinca = details.idFinca AND idTipoLabor =  details.idTipoLabor AND personal != '' Limit 1)
        AS personal,
        min(promedio) AS promedio 
        FROM
        (SELECT 
        idFinca,
        idLote,
        idTipoLabor,
        AVG(promedio) AS promedio
        FROM  muestras_anual as m
        WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
        GROUP BY idFinca , idLote , idTipoLabor) AS details";
        $response->low_personal = $this->db->queryRow($sql_low_personal);

        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY semana";
        $sql_fincas_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from fincas where id = idFinca) AS finca,idFinca , idZona FROM muestras_anual
         WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}'  AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY mes , idFinca";

        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from fincas where id = idFinca) AS finca,idFinca , idZona  FROM muestras_anual
        WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}'  AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idFinca, semana";

        /* VICTOR */
        $response->idStepName = "fincas";

        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas = new stdClass;
        $response->zonas->fincas = [0 => $this->session->alias] + $this->db->queryAllSpecial("SELECT (SELECT nombre from fincas where id = idFinca) AS id  , (SELECT nombre from fincas where id = idFinca) AS label FROM muestras_anual 
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idFinca");
        $months_fincas = $this->db->queryAll($sql_fincas_month);
        $weeks_fincas = $this->db->queryAll($sql_zones_week);
        $response->zonas->auditoria = $this->db->queryAll($sql_fincas_month);
        $response->auditoria = [];

        foreach ($months_fincas as $key => $value) {
            $response->zonas->months[$value->finca][$value->id] = $this->getPromFincaMonth($value->idZona , $value->mes , $value->idFinca)->promedio;
        }

        foreach ($weeks_fincas as $key => $value) {
            $response->zonas->weeks[$value->finca][$value->label] = $this->getPromFincaWeek($value->idZona , $value->label , $value->idFinca)->promedio;
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        // D($this->session);
        return $response;

    }
    /*=====  End of SECCION NIVEL LOTES [LOTES]  ======*/
    /*============================================
    =            SECCION NIVEL LABOR [LABORES]            =
    ============================================*/
    public function getLaboresLotes(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $idLote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }


        $sql = "SELECT nombre ,'' AS value FROM labores";
        $laboresFilter = $this->db->queryAll($sql);
        $labores = [];
        $labor_ = ["nombre" => "" , "labores" => []];
        foreach ($laboresFilter as $key => $value) {
            $value = (object)$value;
            $labor_[$value->nombre] = 0;
            $labores[$value->nombre] = ["filter" => $value->nombre , "value" => ""] ;
        }
        $response = new stdClass; 
        /*----------  TITTLE  ----------*/
        $tittle = new stdClass;
        $sql_tittle = "SELECT
                        (SELECT nombre FROM zonas WHERE id = '{$idZona}') AS zona,
                        (SELECT nombre FROM fincas WHERE id = '{$idfinca}') AS finca,
                        (SELECT nombre FROM lotes WHERE id = '{$idLote}') AS lote";
        $tittle = $this->db->queryRow($sql_tittle);
        if($this->session->id_company == 4){
            $response->tittle = "Orodelti . ".$tittle->finca.". LOTE ".$tittle->lote;
        }elseif($this->session->id_company == 7){
            $response->tittle = "".$tittle->finca.". LOTE ".$tittle->lote;
        }else{
            $response->tittle = "ZONA ".$tittle->zona.". ".$tittle->finca.". LOTE ".$tittle->lote;
        }

        $response->nameFinca = $tittle->finca;
        $response->nameLote = $tittle->lote;
        $response->labores_mes = $months;
        $response->causas = [];
        $response->data_labores = [];
        $response->filterLabores = $labores;
        // $response->data_labores_lotes = [];
        $response->labores = [];
        // $lote = "";
        $labor = "";
        $tipo_labor = "";

        // $sql = "SELECT idZona , zona , idFinca , finca, idLote, lote , idLabor , labor ,  idResponsableLabor , 
        // UPPER(ResponsableLabor) AS ResponsableLabor ,
        // idTipoLabor , 
        // IF(idTipoLabor = 0 , 'LABORES',tipo_labor) AS tipo_labor
        // FROM muestras 
        // WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND idLote = '{$idLote}' AND idFinca = '{$idfinca}'
        // GROUP BY idLote ,idLabor ORDER BY lote , labor";
        $sql = "SELECT STRAIGHT_JOIN idZona , (SELECT nombre FROM zonas WHERE id = idZona) AS Zona ,
                idFinca , (SELECT nombre FROM fincas WHERE id = idFinca) AS finca ,
                idLote , (SELECT nombre FROM lotes WHERE id = idLote) AS lote ,
                (SELECT id FROM personal WHERE id = (SELECT idSupervisorCampo FROM lotes WHERE id = idLote)) AS idResponsableLabor ,
                (SELECT CONCAT_WS(' ',nombre,apellidos) FROM personal WHERE id = (SELECT idSupervisorCampo FROM lotes WHERE id = idLote)) AS ResponsableLabor ,
                idLabor , (SELECT nombre FROM labores WHERE id = idLabor) AS labor ,
                idTipoLabor,
                IF(idTipoLabor = 0 , 'LABORES',(SELECT nombre FROM tipo_labor WHERE id = idTipoLabor)) AS tipo_labor,
                AVG(promedio) AS promedio
                FROM (SELECT STRAIGHT_JOIN fecha , idZona ,idFinca , idTipoLabor,idLote , idLabor ,promedio
                FROM muestras_anual
                WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                AND idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}'
                -- GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                ORDER BY idLote) AS lote
                GROUP BY idLote ,idLabor
                ORDER BY idLote ,idLabor";
        // D($sql);
        $LaboresExist = $this->db->queryAll($sql);
        $lote = "";
        $promedio = 0;
        $response->personal_type_labor = [];
        $personal = "";
        foreach ($LaboresExist as $key => $value) {
            $value = (object)$value;
            // $promedio = $this->getLaborCausa($value->idLabor , $value->idLote , $fecha_inicio , $fecha_fin ,$value->idFinca  )->porcentaje;
            if($value->promedio > -1){
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["idZona"] = $value->idZona;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["idFinca"] = $value->idFinca;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["idLote"] = $value->idLote;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["idLabor"] = $value->idLabor;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["zona"] = $value->Zona;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["finca"] = $value->finca;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["lote"] = $value->lote;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["labor"] = $value->labor;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["promedio"] =  (float)$value->promedio;
                
                $personal = $this->getOperador($value->idFinca , $value->idLote , $value->idTipoLabor);

                if(!in_array($personal, $response->personal_type_labor[$value->tipo_labor])){
                    $response->personal_type_labor[$value->tipo_labor] = $personal;
                }

                if((int)$value->idResponsableLabor > 0){
                    $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["operador"] = $value->ResponsableLabor;
                    $value->ResponsableLabor = trim($value->ResponsableLabor);
                    $response->causas["lote"]["operador"][$value->ResponsableLabor]["idResponsableLabor"] = $value->idResponsableLabor;
                    $response->causas["lote"]["operador"][$value->ResponsableLabor]["ResponsableLabor"] = $value->ResponsableLabor;
                    $response->causas["lote"]["operador"][$value->ResponsableLabor]["promedio"] +=  (float)$value->promedio;
                    $response->causas["lote"]["operador"][$value->ResponsableLabor]["avg"]++;
                }
            }
        }

        $response->personal = [];
        $sql_personal = "SELECT STRAIGHT_JOIN
                (SELECT idSupervisorCampo FROM lotes WHERE id = idLote) AS idPersonal , 
                CASE 
                    WHEN (SELECT idSupervisorCampo FROM lotes WHERE id = idLote) > 0 THEN
                        (SELECT CONCAT_WS(' ',nombre,apellidos) AS nombre FROM personal WHERE id = (SELECT idSupervisorCampo FROM lotes WHERE id = idLote))
                    ELSE NULL
                END AS personal ,
                CASE 
                    WHEN (SELECT idSupervisorCampo2 FROM lotes WHERE id = idLote) > 0 THEN
                        (SELECT CONCAT_WS(' ',nombre,apellidos) AS nombre FROM personal WHERE id = (SELECT idSupervisorCampo2 FROM lotes WHERE id = idLote))
                    ELSE NULL
                END AS personal2,
                AVG(promedio) AS promedio
                FROM (SELECT STRAIGHT_JOIN fecha , idZona ,idFinca , idLote , idLabor ,promedio
                FROM muestras_anual
                WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin'
                AND idZona = '{$idZona}' AND idFinca = '$idfinca' AND idLote = '{$idLote}'
                -- GROUP BY fecha ,idZona , idFinca , idLote , idLabor , mes 
                ORDER BY idLote) AS lote
                GROUP BY (SELECT idSupervisorCampo FROM lotes WHERE id = idLote)
                ORDER BY (SELECT idSupervisorCampo FROM lotes WHERE id = idLote)";
                // D($sql_personal);
        $response->personal = $this->db->queryAll($sql_personal);

        /* GRAFICA PRINCIPAL LINEA DE LOTE */
            $sql_hacienda_month = "SELECT CASE 
                    WHEN mes = 0 THEN 'Ene'
                    WHEN mes = 1 THEN 'Feb'
                    WHEN mes = 2 THEN 'Mar'
                    WHEN mes = 3 THEN 'Abr'
                    WHEN mes = 4 THEN 'May'
                    WHEN mes = 5 THEN 'Jun'
                    WHEN mes = 6 THEN 'Jul'
                    WHEN mes = 7 THEN 'Ago'
                    WHEN mes = 8 THEN 'Sep'
                    WHEN mes = 9 THEN 'Oct'
                    WHEN mes = 10 THEN 'Nov'
                    WHEN mes = 11 THEN 'Dic'
                END
            AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY mes";
            $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY semana";
        
            $sql_fincas_month = "SELECT CASE 
                    WHEN mes = 0 THEN 'Ene'
                    WHEN mes = 1 THEN 'Feb'
                    WHEN mes = 2 THEN 'Mar'
                    WHEN mes = 3 THEN 'Abr'
                    WHEN mes = 4 THEN 'May'
                    WHEN mes = 5 THEN 'Jun'
                    WHEN mes = 6 THEN 'Jul'
                    WHEN mes = 7 THEN 'Ago'
                    WHEN mes = 8 THEN 'Sep'
                    WHEN mes = 9 THEN 'Oct'
                    WHEN mes = 10 THEN 'Nov'
                    WHEN mes = 11 THEN 'Dic'
                END
            AS id,mes,(SELECT nombre from lotes where id = idLote) AS finca,idFinca , idZona , idLote FROM muestras_anual
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}'  AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY mes , idLote";
            
            $sql_zones_week = "SELECT semana AS label,(SELECT nombre from lotes where id = idLote) AS finca,idFinca , idZona , idLote  FROM muestras_anual
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
                GROUP BY idLote, semana";


            $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
            $response->main = $this->db->queryAllSpecial($sql_hacienda_month);

            /* VICTOR */
            $response->idStepName = "lotes";

            $response->categories = new stdClass;
            $response->categories->months = $months_des;
            $response->categories->weeks = $weeks;
            $response->zonas = new stdClass;
            $response->zonas->fincas = [0 => $this->session->alias] + 
                $this->db->queryAllSpecial("SELECT nombre as id, nombre as label FROM fincas WHERE id = '{$idfinca}'")
                +
                $this->db->queryAllSpecial("SELECT (SELECT nombre from lotes where id = idLote) AS id  , (SELECT nombre from lotes where id = idLote) AS label FROM muestras_anual 
                    WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}'  AND YEAR(fecha) = YEAR(CURRENT_DATE)
                    GROUP BY idLote");
            $months_fincas = $this->db->queryAll($sql_fincas_month);
            $weeks_fincas = $this->db->queryAll($sql_zones_week);
            $response->zonas->auditoria = $this->db->queryAll($sql_fincas_month);
            $response->auditoria = [];

            foreach ($months_fincas as $key => $value) {
                $response->zonas->months[$value->finca][$value->id] = $this->getPromFincaLaborMonth($value->idZona , $value->mes , $value->idFinca , $value->idLote)->promedio;
            }

            foreach ($weeks_fincas as $key => $value) {
                $response->zonas->weeks[$value->finca][$value->label] = $this->getPromFincaLaborWeek($value->idZona , $value->label , $value->idFinca , $value->idLote)->promedio;
            }
        /* END LINEA LOTE */
        /* END GRAFICA PRINCIPAL */

        if($this->session->id_company == 5){
            $response->zonas->fincas += $this->db->queryAllSpecial("SELECT (SELECT nombre from labores where id = idLabor) AS id  , (SELECT nombre from labores where id = idLabor) AS label FROM muestras_anual 
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idLabor");
            /*----------  LABORES  ----------*/
            $response->zonas->labores = new stdClass;
            // $response->zonas->labores->months = $months_des;
            // $response->zonas->labores->weeks = $weeks;

            $sql_labores_months = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
            AS id,mes,(SELECT nombre from labores where id = idLabor) AS finca,idFinca , idZona , idLote , idLabor FROM muestras_anual
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY mes , idLabor";

            $sql_labores_weeks = "SELECT semana AS id,semana,(SELECT nombre from labores where id = idLabor) AS finca,idFinca , idZona , idLote , idLabor FROM muestras_anual
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY semana , idLabor";

            $months_labores = $this->db->queryAll($sql_labores_months);
            $weeks_labores = $this->db->queryAll($sql_labores_weeks);

            foreach ($months_labores as $key => $value) {
                $response->zonas->labores->months[$value->finca][$value->id] = $this->getPromLaborMonth($value->idZona , $value->mes , $value->idFinca ,$value->idLote , $value->idLabor)->promedio;
            }

            foreach ($weeks_labores as $key => $value) {
                $response->zonas->labores->weeks[$value->finca][$value->id] = $this->getPromLaborWeek($value->idZona , $value->id , $value->idFinca , $value->idLote , $value->idLabor)->promedio;
            }
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;
        return $response;
    }

    private function getOperador($idFinca , $idlote ,$idTipoLabor){
        $sql = "SELECT personal AS id , personal AS label FROM lotelabor_personal WHERE idFinca = '{$idFinca}' AND idTipoLabor = '{$idTipoLabor}' AND idLote = '{$idlote}'";
        $response = new stdClass;
        $response = "";
        // D($sql);
        $response = $this->db->queryRow($sql);
        // $response = $this->db->queryAllSpecial($sql);
        return $response->id;
    }

    private function getPromFincaLaborMonth($idZona , $month , $idFinca , $idLote){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND mes = '{$month}' AND idFinca = '{$idFinca}' AND idlote = '{$idLote}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        // D($sql);
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromFincaLaborWeek($idZona , $week , $idFinca , $idLote){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND semana = '{$week}' AND idFinca = '{$idFinca}' AND idlote = '{$idLote}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromLaborMonth($idZona , $month , $idFinca , $idLote , $idLabor){
        $response->promedio = 0;
        $sql = "SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio,
        idZona , idFinca , idlote , idLabor
        FROM muestras_anual
         WHERE idZona = '{$idZona}' AND mes = '{$month}' AND idFinca = '{$idFinca}' AND idLote = '{$idLote}' AND idLabor = '{$idLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote , idLabor , mes";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromLaborWeek($idZona , $week , $idFinca , $idLote , $idLabor){
        $response->promedio = 0;
        $sql = "SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio,
        idZona , idFinca , idlote , idLabor
        FROM muestras_anual
         WHERE idZona = '{$idZona}' AND semana = '{$week}' AND idFinca = '{$idFinca}' AND idLote = '{$idLote}' AND idLabor = '{$idLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY idZona , idFinca , idLote , idLabor , mes";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    /*=====  End of SECCION NIVEL LABOR [LABORES]  ======*/
 
    /*=============================================
    =            SECCION NIVEL CAUSAS [CAUSAS]            =
    =============================================*/
    public function getCausas(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $idLote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);
        $response = new stdClass;

        if($idfinca <= 0){
            $idfinca = getValueFrom($postdata,'idFincaLabor','',FILTER_SANITIZE_PHRAPI_INT);
        }
        if($idLabor <= 0){
            $idLabor = getValueFrom($postdata,'idZonaLabor','',FILTER_SANITIZE_PHRAPI_INT);
        }
        $sql_tittle = "SELECT
                        (SELECT nombre FROM zonas WHERE id = '{$idZona}') AS zona,
                        (SELECT nombre FROM fincas WHERE id = '{$idfinca}') AS finca,
                        (SELECT nombre FROM lotes WHERE id = '{$idLote}') AS lote,
                        (SELECT nombre FROM labores WHERE id = '{$idLabor}') AS labor";
        $tittle = $this->db->queryRow($sql_tittle);
        if($this->session->id_company == 4){
            $response->tittle = "Orodelti ".$tittle->zona.". ".$tittle->finca.". LOTE ".$tittle->lote.". ".$tittle->labor;
        }elseif($this->session->id_company == 7){
            $response->tittle = "".$tittle->finca.". LOTE ".$tittle->lote.". ".$tittle->labor;
        }else{
            $response->tittle = "ZONA ".$tittle->zona.". ".$tittle->finca.". LOTE ".$tittle->lote.". ".$tittle->labor;
        }
        $response->nameFinca = $tittle->finca;
        $response->nameLote = $tittle->lote;

        /**
        
            CONSULA DE CAUSAS:
            - idFinca
            - idLabor
            - idLote
            - Fecha
        
         */
        $Where = "";
        if($idLote > 0){
            $Where = "AND idLote ='{$idLote}'";
        }
        if($this->session->id_company == 4){
            $sql = "SELECT campo AS causa ,  
                    CASE
                        WHEN (SELECT valor FROM labores_causas WHERE causa LIKE CONCAT('%',muestras_detalles.campo,'%') AND idLabor = muestras_detalles.idLabor ) > 0 THEN AVG(valor)
                        ELSE NULL
                    END AS porcentaje,
                    CASE
                        WHEN (SELECT valor FROM labores_causas WHERE causa LIKE CONCAT('%',muestras_detalles.campo,'%') AND idLabor = muestras_detalles.idLabor ) > 0 THEN NULL
                        ELSE AVG(valor)
                    END AS porcentaje2
                    FROM muestras_detalles
                    WHERE valor > 0 AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idFinca = '{$idfinca}' AND idLabor = '{$idLabor}' {$Where}
                    GROUP BY campo";
            // D($sql);
        }else{
            if($this->session->id_company == 7){
                switch($idLabor){
                    case 1:
                        $labor = 'Enfunde';
                        break;
                    case 2:
                        $labor = 'Protección';
                        break;
                    case 3:
                        $labor = 'Desvío Hijos';
                        break;
                    case 4:
                        $labor = 'Puntal';
                        break;
                    case 5:
                        $labor = 'Deshoje';
                        break;
                    case 6:
                        $labor = 'Selección';
                        break;
                    case 7:
                        $labor = 'Limpieza';
                        break;
                    case 8:
                        $labor = 'Malezas';
                        break;
                    case 9:
                        $labor = 'Drenaje';
                        break;
                    case 10:
                        $labor = 'Fertilización';
                        break;
                    case 11:
                        $labor = 'Riego';
                        break;
                }
                if(!in_array($labor, ["Malezas","Drenaje","Fertilización",'Riego'])){
                    $sql = "
                    SELECT TYPE, causa, col1 AS buenas, malas, ROUND(col1 / todas * 100,2) AS porcentaje, ROUND(malas / todasMalas * 100,2) AS porcentaje2
                    FROM (
                    SELECT 
                        TYPE, 
                        causa, 
                        
                        (SELECT COUNT(*) AS col1
                        FROM muestras_plantas
                        INNER JOIN muestras_main ON muestras_main.id = id_muestra
                        WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND causa = plantas_main.`causa` AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS col1,
                        
                        (SELECT COUNT(*) AS todas
                        FROM muestras_plantas
                        INNER JOIN muestras_main ON muestras_main.id = id_muestra
                        WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS todas,
                        
                        (SELECT COUNT(*) AS malas
                        FROM muestras_plantas
                        INNER JOIN muestras_main ON muestras_main.id = id_muestra
                        WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND causa = plantas_main.`causa` AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}'AND valor = 0) AS malas,
                        
                        (SELECT COUNT(*) AS malas
                        FROM muestras_plantas
                        INNER JOIN muestras_main ON muestras_main.id = id_muestra
                        WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND valor = 0) AS todasMalas
                        
                    FROM muestras_plantas plantas_main
                    INNER JOIN muestras_main ON muestras_main.id = id_muestra
                    WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}'
                    GROUP BY causa) AS tbl
                    ORDER BY malas, causa";
                    #D($sql);
                }else{
                    $sql = "
                        SELECT TYPE, causa, col1 AS buenas, malas, ROUND(col1 / todas * 100,2) AS porcentaje, ROUND(malas / todasMalas * 100,2) AS porcentaje2
                        FROM (
                        SELECT 
                            TYPE, 
                            causa, 
                            
                            (SELECT COUNT(*) AS col1
                            FROM muestras_labores
                            INNER JOIN muestras_main ON muestras_main.id = id_muestra
                            WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND causa = plantas_main.`causa` AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS col1,
                            
                            (SELECT COUNT(*) AS todas
                            FROM muestras_labores
                            INNER JOIN muestras_main ON muestras_main.id = id_muestra
                            WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS todas,
                            
                            (SELECT COUNT(*) AS malas
                            FROM muestras_labores
                            INNER JOIN muestras_main ON muestras_main.id = id_muestra
                            WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND causa = plantas_main.`causa` AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}'AND valor = 0) AS malas,
                            
                            (SELECT COUNT(*) AS malas
                            FROM muestras_labores
                            INNER JOIN muestras_main ON muestras_main.id = id_muestra
                            WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND valor = 0) AS todasMalas
                            
                        FROM muestras_labores plantas_main
                        INNER JOIN muestras_main ON muestras_main.id = id_muestra
                        WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}'
                        GROUP BY causa) AS tbl
                        ORDER BY malas, causa";
                        #D($sql);
                }
            }else{
                $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
                    CASE
                        WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                        ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
                    END AS porcentaje,
                    CASE
                        WHEN valor > 0 THEN  NULL
                        ELSE (CausasIndividual / malas) * 100
                    END AS porcentaje2
                    FROM 
                    (SELECT laborCausaDesc AS causa , COUNT(causa) AS CausasIndividual,valor,
                    (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND valor > 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
                    (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND valor = 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
                    (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                            fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                            idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}') AS muestras
                    FROM muestras 
                    WHERE 
                        fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                        idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'
                    GROUP BY idLaborCausa) as t
                    ORDER BY valor DESC , causa ASC";
            }
        }
        // D($sql);
        $response->sql_causas = $sql;
        $response->causas = $this->db->queryAll($sql);

        $response->photos = $this->photos($idfinca , $idLote , $idLabor , $fecha_inicio , $fecha_fin);

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;
        /**
        
            CONSULTA POR MES GRAFICA DE BARRAS:
            - listado por meses
            - Obtener valor por mes por labores
            - Obtener valor por mes por todas las labores
        
         */
        
        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY semana";
       
        $sql_fincas_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from labores where id = idLabor) AS finca,idFinca , idZona , idLote , idLabor FROM muestras_anual
         WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' {$Where} AND idLabor = '{$idLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
        GROUP BY mes , idLabor";
        // D($sql_fincas_month);
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from labores where id = idLabor) AS finca,idFinca , idZona , idLote , idLabor FROM muestras_anual
        WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' {$Where} AND idLabor = '{$idLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
            GROUP BY idLabor, semana";

        /* VICTOR */
        $response->idStepName = "labores";

        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas = new stdClass;
        $response->zonas->fincas = 
            [0 => $this->session->alias] + 
            $this->db->queryAllSpecial("SELECT nombre as id, nombre as label FROM fincas WHERE id =  {$idfinca}") +
            $this->db->queryAllSpecial("SELECT nombre as id, nombre as label FROM lotes WHERE id =  {$idLote}") +
            $this->db->queryAllSpecial("SELECT (SELECT nombre from labores where id = idLabor) AS id  , (SELECT nombre from labores where id = idLabor) AS label FROM muestras_anual 
                WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' {$Where} AND idLabor = '{$idLabor}' AND YEAR(fecha) = YEAR(CURRENT_DATE)
                GROUP BY idLabor");
        $months_fincas = $this->db->queryAll($sql_fincas_month);
        // D($sql_fincas_month);
        $weeks_fincas = $this->db->queryAll($sql_zones_week);
        $response->zonas->auditoria = $this->db->queryAll($sql_fincas_month);
        $response->auditoria = [];

        foreach ($months_fincas as $key => $value) {
            $response->zonas->months[$value->finca][$value->id] = $this->getPromLaborMonth($value->idZona , $value->mes , $value->idFinca ,$value->idLote , $value->idLabor)->promedio;
        }

        foreach ($weeks_fincas as $key => $value) {
            $response->zonas->weeks[$value->finca][$value->label] = $this->getPromLaborWeek($value->idZona , $value->label , $value->idFinca , $value->idLote , $value->idLabor)->promedio;
        }
        return $response;
    }
    
    private function photos($idfinca , $idLote , $idLabor , $fecha_inicio , $fecha_fin){
        $sql = "SELECT imagen , muestras.labor , muestras.laborCausaDesc AS causa FROM muestras_imagenes
                INNER JOIN muestras ON muestras_imagenes.idMuestra = muestras.id
                WHERE 
                fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'";
        if($this->session->id_company == 7){
            $sql = "SELECT imagen , labor , causa , idLote , idFinca , idLabor FROM
                    (SELECT 
                    planta ,
                    causa ,
                    type AS labor,
                    lote,
                    main.idFinca ,
                    main.idLote,
                    @idLabor := (SELECT id FROM labores WHERE nombre = type) AS idLabor,
                    imagen 
                    FROM muestras_imagenes AS img
                    INNER JOIN muestras_plantas AS plan ON img.idMuestra = plan.id
                    INNER JOIN muestras_main AS main ON plan.id_muestra = main.id
                    WHERE fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS details
                    WHERE details.idFinca = '{$idfinca}' AND details.idLote ='{$idLote}' AND details.idLabor = '{$idLabor}'";
        }

        // D($sql);
        $response = $this->db->queryAll($sql);

        return $response;
    }
    
    /*=====  End of SECCION NIVEL CAUSAS [CAUSAS]  ======*/
    

}