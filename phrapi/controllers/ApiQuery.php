 <?php defined('PHRAPI') or die("Direct access not allowed!");

class ApiQuery {
	public $name;
	private $db;
	private $agent_user;

	public function __construct(){
		$this->agent_user = getString('agent' , 0);
        // $this->db = DB::getInstance($this->agent_user);
		// D($this->db);
	}

	public function mermaMarun(){
        $this->db = DB::getInstance("marun");
		if(isset($_POST["token"]) && strpos(base64_decode($_POST["token"]), "?procesosiq?") !== FALSE){
			if(isset($_POST["json"])){
				$json = json_decode($_POST["json"], true);
				$filename = $_POST["filename"];

                $user = $json['user'];
                $user_identifier = $user['identifier'].'';;
                $user_username = $user['username'].'';;
                $referenceNumber = $json["referenceNumber"];
                $pages = $json['pages'];
                $geoStamp = $json["geoStamp"];
                $coordinates = $geoStamp["coordinates"];
                $latitude = $coordinates["latitude"];
                $longitude = $coordinates["longitude"];

                $sql_muestra_causas = array();
                $pagina_nombre = "";
                $labor = "";
                $objectMuestras = new stdClass;
                $objectErros = new stdClass;
                $objectMuestras->json = $filename;
                $objectMuestras->indentifier = $user_identifier;
                $objectMuestras->referenceNumber = $referenceNumber;
                $objectMuestras->auditor = $user["username"];
                $objectMuestras->auditorName = $user["displayName"];
                $objectMuestras->details["Images"] = [];
                $contador = 0;
                $existZona = 0;
                $existHacienda = 0;
                $existLote = 0;
                $type = "";
                //if($filename == "20160901-1846707147.json"){
                if($filename == $filename){
                    foreach($pages as $key => $page_data){
                        $pagina = $key+1;
                        $sql_muestra_causas[] = [];
                        if($pagina_nombre != $this->limpiar(trim(strtoupper($page_data['name'])))){
                            $pagina_nombre = $this->limpiar(trim(strtoupper($page_data['name'])));
                            $contador++;
                        }
                        $answers = $page_data["answers"];
                        foreach($answers as $x => $answer){
                            $label    = $answer['label'];
                            $dataType = $answer['dataType'];
                            $question = $answer['question'];
                            $values   = $answer['values'];
                            $labelita = $this->limpiar(trim(strtolower($label)));
                            
                            /*
                            D($pagina_nombre);
                            D($question);
                            D(isset($answer["values"][0]) ? $answer["values"][0] : '---');
                            */
                            if($pagina_nombre == "ENCABEZADO"){
                                if($question == "Fecha"){
                                    $objectMuestras->details["Encabezado"]["Fecha"] = "'".str_replace("T", " ", substr($values[0]["provided"]["time"],0,-6))."'";
                                }
                                if($question != "Fecha" && $question != "Geolocalización"){
                                    $objectMuestras->details["Encabezado"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                            if($pagina_nombre == $this->limpiar("PESO RACIMOS SAN JOSÉ")){
                                if($question != "Fecha" && $question != "Geolocalización"){
                                    $objectMuestras->details["sanJose"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                            if($pagina_nombre == $this->limpiar("PESO RACIMOS CAROLINA")){
                                if($question != "Fecha" && $question != "Geolocalización"){
                                    if($question == "Racimos Cortados"){
                                        $objectMuestras->details["carolina"]["cortados"] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                    }
                                    if($question == "Racimos Recusados"){
                                        $objectMuestras->details["carolina"]["recusados"] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                    }
                                    if($question == "Racimos Procesados"){
                                        $objectMuestras->details["carolina"]["procesados"] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                    }
                                }
                            }
                            if($pagina_nombre == "EMPAQUE"){
                                if(isset($answer["values"][0]["filename"])){
                                    foreach ($answer["values"] as $key => $value) {
                                        $objectMuestras->details["Images"]["Empaque"][] = $objectMuestras->referenceNumber."_".$value["filename"];
                                    }
                                }else{
                                    if($question != "Evidencia")
                                        $objectMuestras->details["Empaque"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                            if($pagina_nombre == "DESHOJE"){
                                if(isset($answer["values"][0]["filename"])){
                                    foreach ($answer["values"] as $key => $value) {
                                        $objectMuestras->details["Images"]["Deshoje"][] = $objectMuestras->referenceNumber."_".$value["filename"];
                                    }
                                }else{
                                    if($question != "Evidencia")
                                        $objectMuestras->details["Deshoje"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                            if($pagina_nombre == "COSECHA"){
                                if(isset($answer["values"][0]["filename"])){
                                    foreach ($answer["values"] as $key => $value) {
                                        $objectMuestras->details["Images"]["Cosecha"][] = $objectMuestras->referenceNumber."_".$value["filename"];
                                    }
                                }else{
                                    if($question != "Evidencia")
                                        $objectMuestras->details["Cosecha"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                            if($pagina_nombre == "LOTERO AEREO"){
                                if(isset($answer["values"][0]["filename"])){
                                    foreach ($answer["values"] as $key => $value) {
                                        $objectMuestras->details["Images"]["Lotero"][] = $objectMuestras->referenceNumber."_".$value["filename"];
                                    }
                                }else{
                                    if($question != "Evidencia")
                                        $objectMuestras->details["Lotero"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                            if($pagina_nombre == "APUNTALADOR"){
                                if(isset($answer["values"][0]["filename"])){
                                    foreach ($answer["values"] as $key => $value) {
                                        $objectMuestras->details["Images"]["Apuntalador"][] = $objectMuestras->referenceNumber."_".$value["filename"];
                                    }
                                }else{
                                    if($question != "Evidencia")
                                        $objectMuestras->details["Apuntalador"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                            if($pagina_nombre == "ADMINISTRACION"){
                                if(isset($answer["values"][0]["filename"])){
                                    foreach ($answer["values"] as $key => $value) {
                                        $objectMuestras->details["Images"]["Administracion"][] = $objectMuestras->referenceNumber."_".$value["filename"];
                                    }
                                }else{
                                    if($question != "Evidencia")
                                        $objectMuestras->details["Administracion"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                            if($pagina_nombre == "FISIOLOGICOS"){
                                if(isset($answer["values"][0]["filename"])){
                                    foreach ($answer["values"] as $key => $value) {
                                        $objectMuestras->details["Images"]["Fisiologicos"][] = $objectMuestras->referenceNumber."_".$value["filename"];
                                    }
                                }else{
                                    if($question != "Evidencia")
                                        $objectMuestras->details["Fisiologicos"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                            if($pagina_nombre == "RESULTADOS GENERALES"){
                                $objectMuestras->details["Resultados"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                            }
                            if($pagina_nombre == "FIRMA ELECTRONICA"){
                                D($answer["values"]);
                                if(isset($answer["values"][0]["filename"])){
                                    foreach ($answer["values"] as $key => $value) {
                                        $objectMuestras->details["FirmaElectronica"][] = $objectMuestras->referenceNumber."_".$value["filename"];
                                    }
                                }else{
                                    $objectMuestras->details["FirmaElectronica"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
                                }
                            }
                        }
                    }


                    $objectMuestras->json = "json/completados/marun/merma/".$objectMuestras->json;
                    D($objectMuestras);
                    $Encabezado         = new stdClass;
                    $Empaque            = new stdClass;
                    $Images             = new stdClass;
                    $Deshoje            = new stdClass;
                    $Cosecha            = new stdClass;
                    $Lotero             = new stdClass;
                    $Administracion     = new stdClass;
                    $Fisiologicos       = new stdClass;
                    $Resultados         = new stdClass;
                    $SanJose            = new stdClass;
                    $Carolina           = new stdClass;
                    $FirmaElectronica   = new stdClass;
                    $Images             = (object)$objectMuestras->details["Images"];
                    $Apuntalador        = (object)$objectMuestras->details["Apuntalador"];
                    $Encabezado         = (object)$objectMuestras->details["Encabezado"];
                    $Empaque            = (object)$objectMuestras->details["Empaque"];
                    $Deshoje            = (object)$objectMuestras->details["Deshoje"];
                    $Cosecha            = (object)$objectMuestras->details["Cosecha"];
                    $Lotero             = (object)$objectMuestras->details["Lotero"];
                    $Administracion     = (object)$objectMuestras->details["Administracion"];
                    $Fisiologicos       = (object)$objectMuestras->details["Fisiologicos"];
                    $Resultados         = (object)$objectMuestras->details["Resultados"];
                    $SanJose            = (object)$objectMuestras->details["sanJose"];
                    $Carolina           = (object)$objectMuestras->details["carolina"];
                    $FirmaElectronica   = (object)$objectMuestras->details["FirmaElectronica"];
                    
                    // $sql_finca = "SELECT * FROM fincas WHERE auditor = '{$objectMuestras->auditor}' LIMIT 1";  
                    // $res_finca = $mysqli->query($sql_finca);
                    // $finca = $res_finca->fetch_assoc();

                    //D($finca);
                    //D($Resultados->{'Total Defectos'});
                    // peso                    = {$Resultados->{'PESO (lb)'}},
                    // peso_protectores        = {$Resultados->{'Protectores y Fundas (lb)'}},
                    $PesoTotalRacimos = (double)str_replace("'","",$Resultados->{'Peso Total Racimos (Kg)'});
                    $Tallo = (double)str_replace("'","",$Resultados->{'Tallo (Kg)'});

                    $peso_neto = ($PesoTotalRacimos - $Tallo);
                    $Palanca = $Encabezado->Palanca;
                    
                    if($Encabezado->Palanca == "Otro" || $Encabezado->Palanca == "''" || $Encabezado->Palanca == "'Otro'"){
                        $Palanca = $Encabezado->{'Nombre nuevo (Palanca)'};
                    }

                    if(!isset($finca['id'])){
                        $finca = [
                            "id" => (double)str_replace("'","",$Encabezado->{'Hacienda - id'}),
                            "nombre" => $Encabezado->Hacienda,
                            "idCliente" => 0,
                        ];
                    }

                    $sql_racimos = "";
                    if($finca['id'] == 1){
                        $sql_racimos .= "racimos_procesados = {$Carolina->procesados},";
                        $sql_racimos .= "racimos_cortados = {$Carolina->cortados},";
                        $sql_racimos .= "racimos_recusados = {$Carolina->recusados},";
                    }else{
                        $sql_racimos .= "racimos_procesados = {$SanJose->{'Racimos Procesados'}},";
                        $sql_racimos .= "racimos_cortados = {$SanJose->{'Racimos Cortados'}},";
                        $sql_racimos .= "racimos_recusados = {$SanJose->{'Racimos Recusados'}},";
                        $sql_racimos .= "peso_racimos_antes = {$SanJose->{'Peso Racimos acumulado antes del muestreo'}},";
                        $sql_racimos .= "peso_racimos_despues = {$SanJose->{'Peso Racimos acumulado después del muestreo'}},";
                    }

                    if(isset($objectMuestras->details["Resultados"]["% MERMA"])){
                        $sql_master = "INSERT INTO 
                            merma SET
                                bloque                  = {$Encabezado->Lote},
                                finca                   = {$finca['nombre']},
                                id_finca                = {$finca['id']},
                                id_cliente              = {$finca['idCliente']},
                                palanca                 = {$Palanca},
                                fecha                   = {$Encabezado->Fecha},
                                timestamp               = CURRENT_TIMESTAMP,
                                archivoJson             = '{$objectMuestras->json}',
                                auditor                 = {$Encabezado->Evaluador},
                                defectos                = {$Resultados->DEFECTOS},
                                total_defectos          = {$Resultados->{'Total Defectos'}},
                                porcentaje_merma        = {$Resultados->{'% MERMA'}},
                                porcentaje_empaque      = {$Resultados->{'% Merma de Empaque'}},
                                porcentaje_cosecha      = {$Resultados->{'% Merma de Cosecha'}},
                                porcentaje_deshoje      = {$Resultados->{'% Merma de Deshoje'}},
                                porcentaje_lotero       = {$Resultados->{'% Merma de Lotero Aéreo'}},
                                porcentaje_administracion  = {$Resultados->{'% Merma de Administración'}},
                                porcentaje_fisiologicos = {$Resultados->{'% Merma de Fisiológicos'}},
                                tallo                   = {$Resultados->{'Tallo (Kg)'}},
                                racimo                  = {$Resultados->{'Peso Total Racimos (Kg)'}},
                                peso_neto               = '{$peso_neto}',
                                {$sql_racimos}
                                total_peso_merma        = {$Resultados->{'Total Peso Merma (Kg)'}}";
                    }else{
                        $sql_master = "INSERT INTO 
                            merma SET
                                bloque                  = {$Encabezado->Lote},
                                finca                   = {$finca['nombre']},
                                id_finca                = {$finca['id']},
                                id_cliente              = {$finca['idCliente']},
                                palanca                 = {$Palanca},
                                fecha                   = {$Encabezado->Fecha},
                                timestamp               = CURRENT_TIMESTAMP,
                                archivoJson             = '{$objectMuestras->json}',
                                auditor                 = {$Encabezado->Evaluador},
                                defectos                = {$Resultados->DEFECTOS},
                                total_defectos          = {$Resultados->{'Total Defectos'}},
                                porcentaje_merma        = {$Resultados->{'% MERMA NETA'}},
                                porcentaje_merma_procesada  = {$Resultados->{'% MERMA PROCESADA'}},
                                porcentaje_empaque      = {$Resultados->{'% Merma de Empaque'}},
                                porcentaje_cosecha      = {$Resultados->{'% Merma de Cosecha'}},
                                porcentaje_deshoje      = {$Resultados->{'% Merma de Deshoje'}},
                                porcentaje_lotero       = {$Resultados->{'% Merma de Lotero Aéreo'}},
                                porcentaje_administracion  = {$Resultados->{'% Merma de Administración'}},
                                porcentaje_fisiologicos = {$Resultados->{'% Merma de Fisiológicos'}},
                                tallo                   = {$Resultados->{'Tallo (Kg)'}},
                                racimo                  = {$Resultados->{'Peso Total Racimos (Kg)'}},
                                peso_neto               = '{$peso_neto}',
                                {$sql_racimos}
                                total_peso_merma        = {$Resultados->{'Total Peso Merma (Kg)'}}";
                    }
                    
                    if(!isset($Encabezado->idCliente)){
                        $Encabezado->idCliente = 3;
                    }
                    if(!isset($Encabezado->idMarca)){
                        $Encabezado->idMarca = 6;
                    }
                    
                    D($sql_master);
                    $this->db->query($sql_master);
                    // $idMuestra = 1;
                    $idMuestra = $this->db->getLastID();
                    $path = "json/image/marun/merma/";

                    foreach ($Images as $key => $value) {
                        foreach ($value as $llave => $img) {
                            $sql = "INSERT INTO merma_images SET id_merma = $idMuestra, type = '{$key}', image = '{$img}', json = '{$objectMuestras->json}'";
                            D($sql);
                            $this->db->query($sql);
                        }
                    }

                    foreach ($Apuntalador as $key => $value) {
                        $sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'APUNTALADOR', campo = '{$key}', cantidad = {$value}";
                        D($sql);
                        $this->db->query($sql);
                    }
                    foreach ($Empaque as $key => $value) {
                        $sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'EMPAQUE', campo = '{$key}', cantidad = {$value}";
                        D($sql);
                        $this->db->query($sql);
                    }
                    foreach ($Deshoje as $key => $value) {
                        $sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'DESHOJE', campo = '{$key}', cantidad = {$value}";
                        D($sql);
                        $this->db->query($sql);
                    }
                    foreach ($Cosecha as $key => $value) {
                        $sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'COSECHA', campo = '{$key}', cantidad = {$value}";
                        D($sql);
                        $this->db->query($sql);
                    }        
                    foreach ($Lotero as $key => $value) {
                        $sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'LOTERO AEREO', campo = '{$key}', cantidad = {$value}";
                        D($sql);
                        $this->db->query($sql);
                    }
                    foreach ($Administracion as $key => $value) {
                        $sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'ADMINISTRACION', campo = '{$key}', cantidad = {$value}";
                        D($sql);
                        $this->db->query($sql);
                    }       
                    foreach ($Fisiologicos as $key => $value) {
                        $sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'FISIOLOGICOS', campo = '{$key}', cantidad = {$value}";
                        D($sql);
                        $this->db->query($sql);
                    }        
                    foreach ($Resultados as $key => $value) {
                        $sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'RESULTADOS', campo = '{$key}', cantidad = {$value}";
                        D($sql);
                        $this->db->query($sql);
                    }       
                    foreach ($FirmaElectronica as $key => $value) {
                        $sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'FIRMA ELECTRONICA', campo = '{$key}', cantidad = {$value}";
                        D($sql);
                        $this->db->query($sql);
                    }


                    $sql_flag1 = "UPDATE merma_detalle SET flag = 1 
                                WHERE 
                                campo  NOT LIKE '%(# Daños)%' AND campo NOT LIKE '%Total Peso%' AND campo NOT LIKE '%Total Merma%'
                                AND campo NOT LIKE '%Observaciones%'
                                AND id_merma > 67 AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS')";
                    $this->db->query($sql_flag1);
                            

                    $sql_flag2 = "UPDATE merma_detalle SET flag = 2
                                WHERE 
                                campo  LIKE '%(# Daños)%' AND campo NOT LIKE '%Total Peso%' AND campo NOT LIKE '%Total Merma%'
                                AND campo NOT LIKE '%Observaciones%'
                                AND id_merma > 67 AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS')";
                    $this->db->query($sql_flag2);

                    $sql_flag3 = "UPDATE merma_detalle SET
                                    flag = 3
                                    WHERE 
                                    campo LIKE '%Racimos sin protectores%' OR
                                    campo LIKE '%Racimos sin corbata%' OR
                                    campo LIKE '%Información adicional%'";
                    $this->db->query($sql_flag3);
                }
            }
        }
    }
}