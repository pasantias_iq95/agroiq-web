<?php defined('PHRAPI') or die("Direct access not allowed!");

class Calidad {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$cliente = "";
		$marca   = "";
		if(isset($postdata->cliente) && isset($postdata->marca)){
			$cliente = mb_strtoupper($postdata->cliente);
			$marca   = mb_strtoupper($postdata->marca);
		}

		$sql = "SELECT (fecha) AS fecha , calidad FROM calidad WHERE marca = 'REWE' 
		GROUP BY fecha
		ORDER BY  fecha";
		$response->data = $this->db->queryAll($sql);
		$response->danhos->series = $this->getDanhos($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->danhos->seleccion = $this->getDetails("SELECCION", $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->danhos->empaque = $this->getDetails("EMPAQUE", $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->danhos->otros = $this->getDetails("OTROS", $postdata->fecha_inicial, $postdata->fecha_final);
		$response->id_company = $this->session->id_company;
		$response->umbrals = $this->session->umbrals;
		
		// manuel
		$response->tags = $this->tags($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_principal_calidad = $this->grafica_principal_calidad($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_principal_de = $this->grafica_principal_de($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_principal_peso = $this->grafica_principal_peso($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_principal_cluster = $this->grafica_principal_cluster($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_principal_peso_cluster = $this->grafica_principal_peso_cluster($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		#victor
		$response->grafica_calidad_historico_marcas = $this->grafica_calidad_historico_marcas($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		
		$response->grafica_danos_total 	   = $this->grafica_danos_total($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_seleccion = $this->grafica_danos_seleccion($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_seleccion_campos = $this->grafica_danos_get_campos('SELECCION');
		$response->grafica_danos_empaque   = $this->grafica_danos_empaque($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_empaque_campos = $this->grafica_danos_get_campos('EMPAQUE', $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_otros     = $this->grafica_danos_otros($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->grafica_danos_otros_campos = $this->grafica_danos_get_campos('OTROS', $postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);

		$response->tabla_principal_calidad = $this->tabla_principal_calidad($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->tabla_principal_danos = $this->tabla_principal_danos($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		$response->tabla_principal = $this->tabla_principal($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);

		$response->type = $this->session->type_users;

		if(!empty($cliente) && !empty($marca)){
			$response->data_header = $this->data_header($postdata->fecha_inicial, $postdata->fecha_final, $cliente, $marca);
		}

		return $response;
	}

	private function data_header($inicio, $fin, $cliente, $marca){
		$sql = "SELECT DISTINCT(peso_referencial) as cantidad
				FROM calidad
				WHERE UPPER(cliente) = '$cliente' AND UPPER(marca) = '$marca' AND peso_referencial IS NOT NULL AND DATE(fecha) BETWEEN '$inicio' AND '$fin'";
		$rp1 = $this->db->queryRow($sql);
		$sql = "SELECT DISTINCT(gajos_referencial) as cantidad
				FROM calidad
				WHERE UPPER(cliente) = '$cliente' AND UPPER(marca) = '$marca' AND gajos_referencial IS NOT NULL AND DATE(fecha) BETWEEN '$inicio' AND '$fin'";
		$rp2 = $this->db->queryRow($sql);
		$sql = "SELECT * FROM brands WHERE nombre = '$marca'";
		$rp3 = $this->db->queryRow($sql);
		$logo = $rp3->logotipo;
		if(!file_exists($_SERVER['DOCUMENT_ROOT']."/".$logo)){
			$logo = "logos/marcas/no-available.png";
		}
		$sql = "SELECT marca FROM calidad WHERE cliente = '$cliente' AND DATE(fecha) BETWEEN '$inicio' AND '$fin' GROUP BY marca";
		$res = $this->db->queryAll($sql);
		$marcas = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$marcas[] = $value;
		}
		return array(
			'peso' => $rp1->cantidad,
			'cluster' => $rp2->cantidad,
			'logo' => $logo,
			'marcas' => $marcas,
		);
	}
	
	private function tabla_principal($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}
		$sql = "SELECT * FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sql_add";
		$res = $this->db->queryAll($sql);
		$data = [];
		$data2 = [];
		foreach ($res as $key => $value) {
			$data2 = [];
			$value = (object)$value;
				
			$sql2 = "SELECT campo, type, SUM(cantidad) AS cantidad FROM calidad_detalle WHERE 
			id_calidad = $value->id AND type='SELECCION' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->seleccion = $data2;
			$data2 = [];
			$sql2 = "SELECT campo, type, SUM(cantidad) AS cantidad FROM calidad_detalle WHERE 
			id_calidad = $value->id AND type='EMPAQUE' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->empaque = $data2;
			$data2 = [];
			$sql2 = "SELECT campo, type, SUM(cantidad) AS cantidad FROM calidad_detalle WHERE 
			id_calidad = $value->id AND type='OTROS' GROUP BY campo";
			$res2 = $this->db->queryAll($sql2);
			foreach ($res2 as $key2 => $value2) {
				$value2 = (object)$value2;
				$data2[] = $value2;
			}
			$value->otros = $data2;
			$data[] = $value;
		}
		return $data;
	}

	private function tags($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT AVG(calidad) AS calidad FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 $sql_add";
		$rp1 = $this->db->queryRow($sql);
		$sql = "SELECT MAX(calidad) AS calidad_maxima FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0  $sql_add";
		$rp2 = $this->db->queryRow($sql);
		$sql = "SELECT MIN(calidad) AS calidad_minima FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 $sql_add";
		$rp3 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(desviacion_estandar) AS desviacion_estandar FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND desviacion_estandar > 0 $sql_add";
		$rp4 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(peso) AS peso FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND peso > 0 $sql_add";
		$rp5 = $this->db->queryRow($sql);
		$sql = "SELECT SUM(cantidad_gajos) AS cluster FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_gajos > 0 $sql_add";
		$rp6 = $this->db->queryRow($sql);
		$sql = "SELECT ((SUM(calidad_dedos) * 100) / (SELECT SUM(calidad_dedos) FROM calidad)) AS calidad_dedos FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_gajos > 0 $sql_add";
		$rp7 = $this->db->queryRow($sql);
		$sql = "SELECT ((SUM(cantidad_gajos) * 100) / (SELECT SUM(cantidad_gajos) FROM calidad)) AS calidad_cluster FROM calidad 	WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_gajos > 0 $sql_add";
		$rp8 = $this->db->queryRow($sql);
		$sql = "SELECT AVG(calidad_dedos) AS dedos_promedio FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_gajos > 0 $sql_add";
		$rp9 = $this->db->queryRow($sql);
		$tags = [];
		$tags = array(
			'calidad' => $rp1->calidad,
			'calidad_maxima' => $rp2->calidad_maxima,
			'calidad_dedos' => $rp7->calidad_dedos,
			'calidad_minima' => $rp3->calidad_minima,
			'desviacion_estandar' => $rp4->desviacion_estandar,
			'peso' => $rp5->peso,
			'cluster' => $rp6->cluster,
			'calidad_cluster' => $rp8->calidad_cluster,
			'dedos_promedio' => $rp9->dedos_promedio
		);
		return $tags;
	}

	private function grafica_principal_calidad($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(calidad) AS calidad, MAX(calidad) AS calidad_maxima, MIN(calidad) AS calidad_minima
				FROM calidad
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 AND desviacion_estandar > 0 AND peso > 0 AND cantidad_gajos > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	private function grafica_principal_de($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(desviacion_estandar) AS desviacion_estandar
				FROM calidad
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 AND desviacion_estandar > 0 AND peso > 0 AND cantidad_gajos > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	private function grafica_principal_peso($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(peso) AS peso
				FROM calidad
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 AND desviacion_estandar > 0 AND peso > 0 AND cantidad_gajos > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}
	
	private function grafica_principal_peso_cluster($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(peso_cluster) AS peso_cluster
				FROM calidad
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 AND desviacion_estandar > 0 AND peso > 0 AND cantidad_gajos > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	private function grafica_principal_cluster($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(cantidad_gajos) AS cluster
				FROM calidad
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 AND desviacion_estandar > 0 AND peso > 0 AND cantidad_gajos > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	#victor
	private function grafica_calidad_historico_marcas($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$sql = "SELECT DATE(fecha) AS fecha, AVG(calidad) AS rewe, MAX(calidad) AS pinalinda, MIN(calidad) AS palmar_aldi
				FROM calidad
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 AND desviacion_estandar > 0 AND peso > 0 AND cantidad_gajos > 0
				$sql_add
				GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		$data = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha] = $value;
		}
		return $data;
	}

	private function grafica_danos_total($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT fecha, AVG(seleccion) AS seleccion FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND seleccion > 0  $sql_add GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha]['seleccion'] = (float)$value->seleccion;
			$data[$value->fecha]['empaque'] = 0;
			$data[$value->fecha]['otros'] = 0;
		}

		$sql = "SELECT fecha, AVG(empaque) AS empaque FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND empaque > 0 $sql_add GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha]['empaque'] = (float)$value->empaque;
		}

		$sql = "SELECT fecha, AVG(otros) AS otros FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND otros > 0 $sql_add GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha]['otros'] = (float)$value->otros;
		}

		return $data;
	}

	// get all campos
	private function grafica_danos_get_campos($type){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$data = [];
		$type = strtoupper($type);
		$sql = "SELECT campo FROM calidad_detalle WHERE TYPE = '$type'GROUP BY campo";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[] = $value->campo;
		}
		return $data;
	}

	private function grafica_danos_seleccion($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT c.fecha, cd.campo, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'SELECCION' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY cd.campo";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha][$value->campo] = (float)$value->cantidad;
		}
		return $data;
	}

	private function grafica_danos_empaque($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT c.fecha, cd.campo, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'EMPAQUE' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY cd.campo";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha][$value->campo] = (float)$value->cantidad;
		}
		return $data;
	}

	private function grafica_danos_otros($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT c.fecha, cd.campo, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'OTROS' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY cd.campo";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->fecha][$value->campo] = (float)$value->cantidad;
		}
		return $data;
	}


	private function tabla_principal_calidad($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT cliente, marca, AVG(calidad) AS calidad FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND calidad > 0 $sql_add GROUP BY DATE(fecha), cliente, marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['calidad']['total'] += $value->calidad;
			$data[$value->cliente][$value->marca]['calidad']['registros']++;
		}

		$sql = "SELECT cliente, marca, AVG(cantidad_gajos) AS cluster FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND cantidad_gajos > 0 $sql_add GROUP BY DATE(fecha), cliente, marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['cluster']['total'] += $value->cluster;
			$data[$value->cliente][$value->marca]['cluster']['registros']++;
		}

		$sql = "SELECT cliente, marca, AVG(peso) AS peso FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND peso > 0 $sql_add GROUP BY DATE(fecha), cliente, marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['peso']['total'] += $value->peso;
			$data[$value->cliente][$value->marca]['peso']['registros']++;
		}

		$data_final = [];
		$c= 0;
		foreach($data as $cliente => $record){
			$data_final[$c]['id'] = $c;
			$data_final[$c]['cliente_marca'] = $cliente;
			$record_keys = array_keys($record);

			$suma_calidad = 0;
			$avg_calidad  = 0;
			$suma_cluster = 0;
			$avg_cluster  = 0;
			$suma_peso    = 0;
			$avg_peso     = 0;
			$d = 0;
			foreach($record_keys as $marca){
				$data_final[$c]['marcas'][$d]['marca'] = $marca;
				$data_final[$c]['marcas'][$d]['calidad'] = $data[$cliente][$marca]['calidad']['total'] / $data[$cliente][$marca]['calidad']['registros'];
				$suma_calidad += $data_final[$c]['marcas'][$d]['calidad'];
				$avg_calidad++;
				$data_final[$c]['marcas'][$d]['cluster'] = $data[$cliente][$marca]['cluster']['total'] / $data[$cliente][$marca]['cluster']['registros'];
				$suma_cluster += $data_final[$c]['marcas'][$d]['cluster'];
				$avg_cluster++;
				$data_final[$c]['marcas'][$d]['peso'] = $data[$cliente][$marca]['peso']['total'] / $data[$cliente][$marca]['peso']['registros'];
				$suma_peso += $data_final[$c]['marcas'][$d]['peso'];
				$avg_peso++;
				$d++;
			}
			$data_final[$c]['calidad'] = $suma_calidad / $avg_calidad;
			$data_final[$c]['cluster'] = $suma_cluster / $avg_cluster;
			$data_final[$c]['peso']    = $suma_peso / $avg_peso;
			$c++;
		}

		return $data_final;
	}

	private function tabla_principal_danos($inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'SELECCION' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['seleccion']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['seleccion']['registros']++;
		}

		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'EMPAQUE' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['empaque']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['empaque']['registros']++;
		}

		$sql = "SELECT c.cliente, c.marca, AVG(cd.cantidad) AS cantidad
				FROM calidad_detalle cd
				INNER JOIN calidad AS c ON c.id = cd.id_calidad
				WHERE cd.type = 'OTROS' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' AND cantidad > 0
				$sql_add
				GROUP BY c.cliente, c.marca";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[$value->cliente][$value->marca]['otros']['total'] += $value->cantidad;
			$data[$value->cliente][$value->marca]['otros']['registros']++;
		}

		$data_final = [];
		$c= 0;
		foreach($data as $cliente => $record){
			$data_final[$c]['id'] = $c;
			$data_final[$c]['cliente_marca'] = $cliente;
			$record_keys = array_keys($record);

			$suma_calidad = 0;
			$avg_calidad  = 0;
			$suma_cluster = 0;
			$avg_cluster  = 0;
			$suma_peso    = 0;
			$avg_peso     = 0;
			$d = 0;

			foreach($record_keys as $marca){
				$data_final[$c]['marcas'][$d]['marca'] = $marca;
				$data_final[$c]['marcas'][$d]['seleccion'] = $data[$cliente][$marca]['seleccion']['total'] / $data[$cliente][$marca]['seleccion']['registros'];
				$suma_calidad += $data_final[$c]['marcas'][$d]['seleccion'];
				$avg_calidad++;
				$data_final[$c]['marcas'][$d]['empaque'] = $data[$cliente][$marca]['empaque']['total'] / $data[$cliente][$marca]['empaque']['registros'];
				$suma_cluster += $data_final[$c]['marcas'][$d]['empaque'];
				$avg_cluster++;
				$data_final[$c]['marcas'][$d]['otros'] = $data[$cliente][$marca]['otros']['total'] / $data[$cliente][$marca]['otros']['registros'];
				$suma_peso += $data_final[$c]['marcas'][$d]['otros'];
				$avg_peso++;
				$d++;
			}
			$data_final[$c]['seleccion'] = (float)($suma_calidad / $avg_calidad);
			$data_final[$c]['empaque'] = (float)($suma_cluster / $avg_cluster);
			$data_final[$c]['otros']    = (float)($suma_peso / $avg_peso);
			$c++;
		}

		return $data_final;
	}



	private function getDanhos($inicio, $fin, $cliente, $marca){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND cliente = '$cliente' AND marca = '$marca'";
		}

		$data = [];
		$sql = "SELECT fecha, AVG(seleccion) AS seleccion FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND seleccion > 0 $sql_add GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[0]['name']  = "SELECCION";
			$data[0]['value'] = (float)$value->seleccion;
			$data[1]['name'] = "EMPAQUE";
			$data[1]['value'] = 0;
			$data[2]['name'] = "OTROS";
			$data[2]['value'] = 0;
		}

		$sql = "SELECT fecha, AVG(empaque) AS empaque FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND empaque > 0 $sql_add GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[1]['value'] = (float)$value->empaque;
		}

		$sql = "SELECT fecha, AVG(otros) AS otros FROM calidad WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND otros > 0 $sql_add GROUP BY DATE(fecha)";
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$data[2]['value'] = (float)$value->otros;
		}

		return $data;
		/*
		$sql = "SELECT AVG(seleccion) AS seleccion,
		AVG(empaque) AS empaque,
		AVG(otros) AS otros
		FROM calidad
		WHERE seleccion > 0 AND empaque > 0 AND otros > 0 AND DATE(fecha) BETWEEN '$inicio' AND '$fin'";
		$danhos = $this->db->queryAll($sql);
		$data = [];
		foreach ($danhos as $key => $value) {
			$value = (object)$value;
			$data = [
				[
					"value" => $value->seleccion,
					"name" => "SELECCION"
				],
				[
					"value" => $value->empaque,
					"name" => "EMPAQUE"
				],
				[	
					"value" => $value->otros,
					"name" => "OTROS"
				]
			];
		}
		return $data;
		*/
	}

	private function getDetails($type = "SELECCION", $inicio, $fin){
		$sql_add = "";
		if(!empty($cliente) && !empty($marca)){
			$sql_add = " AND c.cliente = '$cliente' AND c.marca = '$marca'";
		}

		$sql = "SELECT cd.campo, SUM(cd.cantidad) as cantidad FROM calidad_detalle cd INNER JOIN calidad AS c ON c.id = cd.id_calidad WHERE cd.type = '$type' AND DATE(c.fecha) BETWEEN '$inicio' AND '$fin' $sql_add GROUP BY cd.campo";
		$data = $this->db->queryAll($sql);
		$response = [];
		foreach ($data as $key => $value) {
			$response[] = [
				"value" => $value->cantidad,
				"name" => $value->campo
			];
		}

		return $response;
	}
}