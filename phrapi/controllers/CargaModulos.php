<?php defined('PHRAPI') or die("Direct access not allowed!");

class CargaModulos
{
	private $session;
	public $modulo = "dashboard";
	private $archivo_vista = "";
	private $archivo_vista_js = "";
	private $notAccess = "";
	private $modules;
	private $configModules;
	public $controller;


	public function __construct(){
		$this->session = Session::getInstance();
		$this->configModules = $GLOBALS['config']['modules'];
		$this->reloadModule();
		$this->modules = $this->getData($this->configModules);
		$modulo = preg_replace('~/~', '_', getValueFrom($_GET,'page', $this->modulo));
        $this->modulo = $modulo;
        $cultivoModule = $this->getRuleModule($this->modulo, $this->configModules);
		$this->controller = $modulo;
		$this->archivo_vista    = PHRAPI_PATH . "../views/{$modulo}.php";
		$this->archivo_vista_js = PHRAPI_PATH . "../views/{$modulo}.js.php";
		$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$modulo}.js";
        $this->notAccess = PHRAPI_PATH . "../views/not_access.php";
		$this->session->moduleSystem = $cultivoModule;
		$this->maintenance = $this->getMaintenanceModules($this->session->id_company);
		
		if($GLOBALS['config']['env'] != 'DEV' && in_array($this->modulo, $this->maintenance)){
			$this->archivo_vista = PHRAPI_PATH . "../views/maintenance.php";
			$this->archivo_vista_js = '';
			$this->archivo_vista_file_js = '';
			return ;
		}
		
		/*
			NOTA :
				Modificacion a nivel vista para el usuario Marun debido a que no existe ningun otro usuario parecido a el
				se copio el archivo del cliente DOLE que se encuentra en el servidor Digital Ocean, por lo cual NO MOVER NADA 
				de este codigo sin avisar a Arturo o Javier esto solo sera mientras no se migre a Digital Ocean
		*/
        // 03/03/2017 - TAG: No Mover Nunca de los Nuncas
        $modules_marlon = ["reportes", "laboresAgricolas", "merma", "produccionsemana", "laboresAgricolasDia"];
        // 02/06/2017 - TAG: MODULOS SEPARADOS MARLON
		if($this->session->id_company == 4 && in_array($this->modulo,$modules_marlon)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		$modules_reiset = ["merma", "bonificacion", "mermadia", "mermaDedos", "climaDiario", "produccionDemo", "produccionReporteAcumulado"];
		// 02/06/2017 - TAG: MODULOS SEPARADOS REISET
		if($this->session->id_company == 10 && in_array($this->modulo,$modules_reiset)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		$modules_mario = ["merma","labores","laboresAgricolas", "reportes", "geoposicion", "laboresAgricolasDia"];
		// 02/06/2017 - TAG: MODULOS SEPARADOS MARIO
		if($this->session->id_company == 3 && in_array($this->modulo,$modules_mario)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		// 02/06/2017 - TAG: MODULOS SEPARADOS MARUN
		$modules_marun = ["calidad" , "bonificacion", "merma", "bonificacionViejo", "produccion", "f", "analisisSensorial", "asignacionMallas",
			"formularioAsignacionArea", "climaDiario", "produccionDemo", "balanzaCosecha", "cacaoReporteCosecha", "comparativoCosecha", "consolidadoCosecha", "cacaotthhPersonal", "cacaotthhFPersonal",
			"cacaoReporteCosechaPeriodal", "cacaoReporteCosechaSector", "cosechaLotes", "cacaoConfigLotes", "calidad2", "calidadComparacion", "calidadTendencia"];
		// 02/06/2017 - TAG: MODULOS SEPARADOS MARUN
		if($this->session->id_company == 7 && in_array($this->modulo,$modules_marun)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}
		
		// 16/06/2017 - TAG: MODULOS QUINTANA
        $modules_quintana = ["revisionAsistencia" , "asistencia", "calidad" , 
			"produccionrecobro", "producciongerencia", "merma", "clima", "climaDiario",
			"laboresAgricolas", "laboresAgricolasEmpaque", "rpersonal"];
		if($this->session->id_company == 6 && in_array($this->modulo,$modules_quintana)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}
		// 13/06/2017 - TAG: MODULOS QUINTANA
		// 03/03/2017 - TAG: No Mover Nunca de los Nuncas

        $modules_marcel = ["tthhFPersonal", 'tthhFPersonalExpediente', "tthhPersonal", "tthhExpediente", "revisionAsistencia", 
            "agregarAsistencia", "tthhEnfunde", "produccionrecobro", "producciongerencia", "produccion", 
            "produccionsemana", "calidad", "tthhNuevoEnfunde", "calidadComparativo", "calidadFotos", "recobro", "produccionReporte",
			"climaDiario", "merma", "produccionResumenCorte", "produccionComparacion", "produccionPrecalibracion", "produccionPrecalibracion2",
			"produccionComparacionAnalisis", "produccionRacimosFormularios"];

		if(($this->session->id_company == 2 || $this->session->id_company == 14) && in_array($this->modulo, $modules_marcel)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		$modules_sumifru = ["merma", "mermadia", "calidad", "produccionComparacion", "calidad2", 
			"calidadComparacion", "calidadTendencia"];

		if($this->session->id_company == 11 && in_array($this->modulo, $modules_sumifru)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}
		
		$modules_arregui = ["merma", "calidad"];

		if($this->session->id_company == 13 && in_array($this->modulo, $modules_arregui)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
        }
        
		$modules_palmar = ["produccion", "merma", "mermadia", "calidad", "laboresAgricolas", "calidad", "calidad2",
			"calidadComparacion", "calidadTendencia"];
		if($this->session->id_company == 12 && in_array($this->modulo, $modules_palmar)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		$modules_agroban = ["produccionsemana"];
		if($this->session->id_company == 14 && in_array($this->modulo, $modules_palmar)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		$modules_clementina = ["merma"];
		if($this->session->id_company == 15 && in_array($this->modulo, $modules_clementina)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		$modules_orodelti = ["merma", "bonificacion"];
		if($this->session->id_company == 16 && in_array($this->modulo, $modules_orodelti)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		$modules_agroaereo = ["palmaLaboresAgricolasDia", "laboresAgricolasDia"];
		if($this->session->id_company == 17 && in_array($this->modulo, $modules_agroaereo)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		$modules_rufcorp = ["merma", "mermadia", "calidad", "calidad2", "calidadComparacion", "calidadTendencia"];
		if($this->session->id_company == 20 && in_array($this->modulo, $modules_rufcorp)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		$modules_eugenia = [];
		if($this->session->id_company == 18 && in_array($this->modulo, $modules_eugenia)){
			$this->archivo_vista    = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.php";
			$this->archivo_vista_js = PHRAPI_PATH . "../views/{$this->session->agent_user}/{$modulo}.js.php";
			$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$this->session->agent_user}/{$modulo}.js";
		}

		// 08/05/2017 - TAG: CAMBIO DE CARPETA POR MODULO DEL SISTEMA [SIGAT]
		if($this->session->settings->sigat_module == 'Activo'){
			if(array_key_exists($this->modulo , $this->session->routes)){
				$route =  $this->session->routes[$this->modulo];
				$this->archivo_vista    = PHRAPI_PATH . "../views/{$route}/{$this->session->agent_user}/{$modulo}.php";
				$this->archivo_vista_js = PHRAPI_PATH . "../views/{$route}/{$this->session->agent_user}/{$modulo}.js.php";
				$this->archivo_vista_file_js = PHRAPI_PATH . "../js/{$route}/{$this->session->agent_user}/{$modulo}.js";
			}else{
				$this->archivo_vista = $this->notAccess;
				$this->archivo_vista_js = "";
				$this->archivo_vista_file_js = "";
			}
		}
		// 08/05/2017 - TAG: CAMBIO DE CARPETA POR MODULO DEL SISTEMA [SIGAT]

		if (!file_exists($this->archivo_vista))
		{
			$notFound = true;
			$this->archivo_vista = PHRAPI_PATH . "../views/404.php";
			if (!file_exists($this->archivo_vista))
			{
				$this->archivo_vista = '';
			}
		}else{
			$notFound = false;
		}

		if (!file_exists($this->archivo_vista_js))
		{
			$this->archivo_vista_js = '';
		}
		
		if($modulo != "dashboard" && !$this->in_array_r($modulo , $this->modules) && !$notFound){
			$this->archivo_vista = $this->notAccess;
			$this->archivo_vista_js = "";
		}
	}

	private function getMaintenanceModules($id_company){
		$db = DB::getInstance();
		$modules = $db->queryAllOne("SELECT module FROM maintenance WHERE id_company = $id_company");
		return $modules;
	}

	private function getData($array){
		if(!is_array($array)) return false;
		$data = [];
		foreach ($array as $key => $value) {
			$data['views'][] = $value['views'];

		}

		return $data;
	}

	public function mostrar()
	{
		$factory = $GLOBALS['factory'];
		$archivos = [$this->archivo_vista, $this->archivo_vista_js];
		foreach ($archivos as $archivo) {
			if (!empty($archivo))
			{
				include_once $archivo;
				#yield $archivo;
			}
		}
	}

	public function vistas()
	{
		$factory = $GLOBALS['factory'];
		$vistas = ['vista'=>'','vista_js' => '' , 'vista_file_js' => ''];
		$modulo = $this->modulo;
		if (!empty($this->archivo_vista))
		{
			ob_start();
			include_once $this->archivo_vista;
			$vistas['vista'] = ob_get_clean();
		}

		if (!empty($this->archivo_vista_js))
		{
			ob_start();
			include_once $this->archivo_vista_js;
			$vistas['vista_js'] = ob_get_clean();
		}

		if (!empty($this->archivo_vista_file_js))
		{
			ob_start();
			include_once $this->archivo_vista_file_js;
			$vistas['vista_file_js'] = ob_get_clean();
		}

		return $vistas;
	}

	public function vista_js()
	{
		$factory = $GLOBALS['factory'];
		if (!empty($this->archivo_vista_js))
		{
			include_once $this->archivo_vista_js;
		}
	}

	private function in_array_r($needle, $haystack, $strict = false) {
	    foreach ($haystack as $item) {
	        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
	               // return $item;
	        	return true;
	        }
	    }

	    return false;
	}

	private function  reloadModule(){
		$modules = $this->session->settings;
		foreach ($modules as $key => $value) {
			if($key != "id_company" && $key != "id" && $key != "token"){
				if(($key == "membresias" || $key == "configuracion") && $value != "Activo"){
					unset($this->configModules[$key]);
				}elseif(!in_array($key, $this->session->memberships) && $value != 'Activo' && ($key != "membresias" || $key != "configuracion")){
					unset($this->configModules[$key]);
				}
			}
		}
    }
    
    private function getRuleModule($module, $array){
        $data = $this->getData($array);
        foreach($array as $modules){
            if(in_array($module, $modules['views']))
                return $modules['moduleSystem'];
        }
    }

}