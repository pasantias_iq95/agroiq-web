<?php defined('PHRAPI') or die("Direct access not allowed!");

class Cercas {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }


    private function getParams(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "id" => (int) getValueFrom($postdata , "id", "", FILTER_SANITIZE_STRING),
            "name" =>  getValueFrom($postdata , "nombre", "", FILTER_SANITIZE_STRING),
            "seller" =>  getValueFrom($postdata , "seller", "", FILTER_SANITIZE_STRING),
            "polygon" =>  implode("|",$postdata->paths),
            "route" => getValueFrom($postdata , "route", '', FILTER_SANITIZE_STRING),
        ];
        return $data;
    }

    public function index(){
        $filters = $this->getParams();
        $sql = "SELECT * FROM geo_cercas WHERE status = 'Activo'";
        $response = (object)['code' => 400 , 'message' => "Error al consultar" , 'response' => []];
        $response->response = $this->db->queryAll($sql);
        if(count($response->response) > 0){
            $response->code = 200;
            $response->message = "Consultado con exito";
        }
        return $response;
    }

    public function save(){
        $data =$this->getParams();
        $response = (object)['code' => 400 , 'message' => "Error al consultar" , 'response' => []];
        if($data->polygon != "" && $data->name != ""){
            $sql = "INSERT INTO `geo_cercas` SET
                `name` = '{$data->name}',
                `seller` = '$data->selle',
                `route` = '{$data->route}',
                `polygon` = '{$data->polygon}'";
            if($data->id > 0){
                $sql = "UPDATE `geo_cercas` SET
                    `name` = '{$data->name}',
                    `seller` = '$data->selle',
                    `route` = '{$data->route}',
                    `polygon` = '{$data->polygon}'
                    WHERE id = {$data->id}";
            }
            $this->db->query($sql);
            $response->code = 200;
            $response->message = "Registrado con exito";
        }
        return $response;
    }
}