<?php defined('PHRAPI') or die("Direct access not allowed!");

class ConfigLancofruit {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);

        $Factory = new Factory();
        $this->ApiProntoforms = $Factory->ApiProntoforms;
    }

    private function getParams(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "id" => (int) getValueFrom($postdata , "id", 0, FILTER_SANITIZE_PHRAPI_INT),
            "nombre" => getValueFrom($postdata , "nombre", "", FILTER_SANITIZE_STRING),
            "subcategorias" => getValueFrom($postdata , "subcategorias", [], FILTER_SANITIZE_PHRAPI_ARRAY),
            "id_categoria" => (int) getValueFrom($postdata , "id_categoria", 0, FILTER_SANITIZE_PHRAPI_INT),
        ];
        return $data;
    }

    public function index(){
        $filters = $this->getParams();
        $response = new stdClass;

        $response->categorias = $this->db->queryAll("SELECT id, nombre, status FROM lancofruit_categorias cat WHERE status != 'Archivado'");
        $response->subcategorias = $this->db->queryAll("SELECT id, nombre, id_categoria, status, (SELECT nombre FROM lancofruit_categorias WHERE id = id_categoria) AS categoria FROM lancofruit_subcategorias WHERE status != 'Archivado'");
        
        return $response;
    }

    public function guardarCategoria(){
        $filters = $this->getParams();
        $response = new stdClass;
        $response->status = 400;

        if($filters->nombre != ""){
            // UPDATE CATEGORIA
            if($filters->id > 0){
                $exist = (int) $this->db->queryOne("SELECT COUNT(1) FROM lancofruit_categorias WHERE nombre = '{$filters->nombre}' AND id != $filters->id");
                if($exist > 0){
                    $response->message = "Esta nombre ya esta en uso";
                    return $response;
                }
                else {
                    $sql = "UPDATE lancofruit_categorias SET
                                nombre = UPPER('{$filters->nombre}')
                            WHERE id = $filters->id";
                    $this->db->query($sql);
                    $response->id = $filters->id;
                }
            }
            // INSERT NEW CATERGOIA
            else{
                $exist = (int) $this->db->queryOne("SELECT COUNT(1) FROM lancofruit_categorias WHERE nombre = '{$filters->nombre}'");
                if($exist > 0){
                    $response->message = "Esta categoria ya existe";
                    return $response;
                }else{
                    $sql = "INSERT INTO lancofruit_categorias SET
                                nombre = UPPER('{$filters->nombre}')";
                    $this->db->query($sql);
                    $response->id = $this->db->getLastID();
                }
            }

            //DESASIGNAR SUBCATEGORIAS
            $nuevas_subcategorias = $filters->subcategorias;
            $subcategorias_asignadas = $this->db->queryAllOne("SELECT id FROM lancofruit_subcategorias WHERE id_categoria = $response->id");
            foreach($subcategorias_asignadas as $id_sub){
                $position = array_search($id_sub, $filters->subcategorias);
                if(!$position >= 0){
                    $this->db->query("UPDATE lancofruit_subcategorias SET id_categoria = NULL WHERE id = $id_sub");
                }else{
                    unset($filters->subcategorias[$position]);
                }
            }

            //ASIGNAR NUEVAS SUBCATEGORIAS
            foreach($nuevas_subcategorias as $id_sub){
                $estaAsignada = (int) $this->db->queryOne("SELECT COUNT(1) FROM lancofruit_subcategorias WHERE id = $id_sub AND id_categoria > 0");
                if(!$estaAsignada){
                    $this->db->query("UPDATE lancofruit_subcategorias SET id_categoria = $response->id WHERE id = $id_sub");
                }
            }

            $response->status = 200;
            $response->nombre = strtoupper($filters->nombre);
            $response->subcategorias = $filters->subcategorias;
            $this->ApiProntoforms->actualizarCategoriasLancofruit();
        }
        
        return $response;
    }

    public function archivarCategoria(){
        $filters = $this->getParams();
        $response = new stdClass;
        $response->status = 400;

        if($filters->id > 0){
            $response->status = 200;
            $this->db->query("UPDATE lancofruit_categorias SET status = 'Archivado' WHERE id = $filters->id");
            $this->ApiProntoforms->actualizarCategoriasLancofruit();
        }
        
        return $response;
    }

    public function toggleStatusCategoria(){
        $filters = $this->getParams();
        $response = new stdClass;
        $response->status = 400;

        if($filters->id > 0){
            $response->status = 200;
            $this->db->query("UPDATE lancofruit_categorias SET status = IF(status = 'Activo', 'Inactivo', 'Activo') WHERE id = $filters->id");
            $this->ApiProntoforms->actualizarCategoriasLancofruit();
        }
        
        return $response;
    }

    public function guardarSubcategoria(){
        $filters = $this->getParams();
        $response = new stdClass;
        $response->status = 400;

        if(!$filters->id_categoria > 0) $filters->id_categoria = 'NULL';

        if($filters->nombre != ""){
            if($filters->id > 0){
                $sql = "UPDATE lancofruit_subcategorias SET
                            nombre = UPPER('{$filters->nombre}'),
                            id_categoria = {$filters->id_categoria}
                        WHERE id = $filters->id";
                $this->db->query($sql);
            }else{
                $sql = "INSERT INTO lancofruit_subcategorias SET
                            nombre = UPPER('{$filters->nombre}'),
                            id_categoria = {$filters->id_categoria}";
                $this->db->query($sql);
                $response->id = $this->db->getLastID();
            }

            $response->status = 200;
            $response->nombre = strtoupper($filters->nombre);
            if($filters->id_categoria > 0){
                $response->id_categoria = $filters->id_categoria;
                $response->categoria = $this->db->queryOne("SELECT nombre FROM lancofruit_categorias WHERE id = $filters->id_categoria");
            }
            else{
                $response->categoria = "";
                $response->id_categoria = null;
            }
            $this->ApiProntoforms->actualizarCategoriasLancofruit();
        }
        
        return $response;
    }

    public function archivarSubcategoria(){
        $filters = $this->getParams();
        $response = new stdClass;
        $response->status = 400;

        if($filters->id > 0){
            $response->status = 200;
            $this->db->query("UPDATE lancofruit_subcategorias SET status = 'Archivado' WHERE id = $filters->id");
            $this->ApiProntoforms->actualizarCategoriasLancofruit();
        }
        
        return $response;
    }

    public function toggleStatusSubcategoria(){
        $filters = $this->getParams();
        $response = new stdClass;
        $response->status = 400;

        if($filters->id > 0){
            $response->status = 200;
            $this->db->query("UPDATE lancofruit_subcategorias SET status = IF(status = 'Activo', 'Inactivo', 'Activo') WHERE id = $filters->id");
            $this->ApiProntoforms->actualizarCategoriasLancofruit();
        }
        
        return $response;
    }
}