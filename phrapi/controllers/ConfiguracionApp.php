<?php defined('PHRAPI') or die("Direct access not allowed!");

class ConfiguracionApp {
    private $db;
    private $config;
    private $token;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function last(){
        $response = new stdClass;
        $table = $this->session->id_company == 2 ? 'produccion_historica' : 'racimo_web';
        $response->anios = $this->db->queryAllOne("SELECT anio FROM {$table} WHERE anio > 0 GROUP BY anio ORDER BY anio DESC");
        $response->anio = $this->db->queryOne("SELECT getYear(CURRENT_DATE)");
        $response->semana = $this->db->queryOne("SELECT getWeek(CURRENT_DATE)");
        $response->fincas = $this->db->queryAll("SELECT id, nombre FROM fincas WHERE status = 1 AND status_produccion = 'Activo'");
        return $response;
    }

    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        return (object) [
            'anio' => getValueFrom($postdata, 'anio', date('Y'), FILTER_SANITIZE_PHRAPI_INT),
            'semana' => getValueFrom($postdata, 'semana', 0, FILTER_SANITIZE_PHRAPI_INT),
            'id_lote' => getValueFrom($postdata, 'id_lote', 0, FILTER_SANITIZE_PHRAPI_INT),
            'hectareas' => getValueFrom($postdata, 'hectareas', 0, FILTER_SANITIZE_PHRAPI_FLOAT),
            'id_finca' => getValueFrom($postdata, 'id_finca', 0, FILTER_SANITIZE_PHRAPI_INT),
            'sector' => getValueFrom($postdata, 'sector', '', FILTER_SANITIZE_PHRAPI_MYSQL),
            'type_save' => getValueFrom($postdata, 'type_save', '', FILTER_SANITIZE_PHRAPI_MYSQL)
        ];
    }

    public function index(){
        $response = new stdClass;
        $filters = $this->params();

        $sem_act = $this->db->queryOne("SELECT getWeek(CURRENT_DATE)");
        $lotes = $this->db->queryAll("SELECT l.id AS id_lote, l.nombre AS lote, f.nombre AS finca, l.sector FROM lotes l INNER JOIN fincas f ON idFinca = f.id WHERE l.idFinca = $filters->id_finca ORDER BY l.nombre+0");
        $semanas = [];
        $total = [
            "id_lote" => 0,
            "lote" => "TOTAL",
            "finca" => "",
            "sector" => "",
        ];
        foreach($lotes as $row){
            $lastValue = 0;
            for($x = 1; $x <= $sem_act; $x++){
                if(!in_array($x, $semanas)) $semanas[] = $x;
                $val = $this->db->queryOne("SELECT getHectareasLote($row->id_lote, $filters->anio, $x)");
                $row->{"sem_$x"} = $val > 0 ? round($val, 3) : '';

                if($row->{"sem_$x"} != ''){
                    $row->{"class_sem_$x"} = 'bg-green-haze bg-font-green-haze';
                    if($x > 1){
                        if($lastValue != $val) $row->{"class_sem_$x"} = 'bg-red-thunderbird bg-font-red-thunderbird';
                    }
                }
                $lastValue = $val;

                if(!isset($total["sem_$x"])) $total["sem_$x"] = 0;
                $total["sem_$x"] += $val;
            }
        }
        foreach($total as $key => $value){
            if(strpos($key, 'sem_') !== false){
                $sem = (int) explode('sem_', $key)[1];
                if($sem > 1){
                    if($total["sem_".($sem-1)] != $value){
                        $total["class_sem_$sem"] = 'bg-red-thunderbird bg-font-red-thunderbird';
                    }else{
                        $total["class_sem_$sem"] = 'bg-green-haze bg-font-green-haze';    
                    }
                }else{
                    $total["class_sem_$sem"] = 'bg-green-haze bg-font-green-haze';
                }
            }
        }
        $lotes[] = $total;

        $response->data = $lotes;
        $response->semanas = $semanas;
        
        return $response;
    }

    public function saveHa(){
        $response = new stdClass;
        $response->status = 400;
        $filters = $this->params();

        if($filters->type_save == 'sector'){
            $sql = "UPDATE lotes SET
                        sector = '{$filters->sector}'
                    WHERE id = $filters->id_lote";
            $this->db->query($sql);
            $response->status = 200;
        }
        else if($filters->id_lote > 0 && $filters->semana > 0){
            $e = (int) $this->db->queryOne("SELECT COUNT(1) FROM configuracion_lotes_hectareas WHERE id_lote = $filters->id_lote AND anio = $filters->anio AND semana = $filters->semana");
            if($e){
                $sql = "UPDATE configuracion_lotes_hectareas SET
                            hectareas = $filters->hectareas
                        WHERE anio = $filters->anio AND semana = $filters->semana AND id_lote = $filters->id_lote";
                $this->db->query($sql);
            }else{
                $sql = "INSERT INTO configuracion_lotes_hectareas SET
                            id_lote = $filters->id_lote,
                            hectareas = $filters->hectareas,
                            anio = $filters->anio,
                            semana = $filters->semana";
                $this->db->query($sql);
            }
            $response->status = 200;
        }

        return $response;
    }
}