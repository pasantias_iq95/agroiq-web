<?php defined('PHRAPI') or die("Direct access not allowed!");

class CronJobs {
    public $name;
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        // D($this->session);
        $this->token = getString('token' , 'marlon');
        // $this->Service = new ServicesNode('http://procesos-iq.com:3000/' , $this->token);
        if($this->token == "EBE47646297F98EE83F4B6F067B0F65E804EAD12"){
            $this->agent = 'marlon';
        }else{
            $this->agent = $this->session->agent_user;
        }
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->agent);
        // $this->db = DB::getInstance($this->session->agent_user);
    }


    public function index(){
        $response = new stdClass; 
        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $week = [];

        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
        }

        $date = new stdClass;
        $date->startdate  = ""; 
        $date->enddate    = "";


        foreach ($months_des as $key => $value) {
            $month = (($key + 1) <= 9) ? "0".($key + 1) : ($key + 1);
            $date->startdate  = "2016-".$month."-01";
            $date->enddate = date('Y-m-t', strtotime($date->startdate));   

                $sql_detalle = "SELECT 
                    fecha,
                    MONTH(fecha) AS Mes,
                    WEEK(fecha) AS semana,
                    idLote, 
                    idFinca,
                    finca, 
                    idZona, 
                    zona,
                    lote , 
                    idLabor , 
                    labor,
                    idTipoLabor , 
                    IF(idTipoLabor = 0 , 'LABORES',tipo_labor) AS tipo_labor,
                    idResponsableLabor , UPPER(ResponsableLabor) AS ResponsableLabor,
                    '-1' AS MesActual 
                    FROM muestras 
                    WHERE YEAR(fecha) = YEAR(CURRENT_DATE)  
                    AND fecha BETWEEN '{$date->startdate}' AND '{$date->enddate}' AND idLote > 0
                    GROUP BY idLote ,idLabor 
                    ORDER BY MONTH(fecha)  , fecha, lote , labor";
                $LaboresExistMonth = $this->db->queryAll($sql_detalle);
                $response->main = [];
                $response->week = [];
                $response->auditoria = [];
                $Lpromedio = [];
                $response->main = $months;
                $response->week = $week;
                // $response->lote_name = "";
                D($this->agent);
                if(count($LaboresExistMonth) > 0){
                    foreach ($LaboresExistMonth as $key => $value) {
                        $value = (object)$value;
                        $Lpromedio = $this->getPromMonthLoteAvg($value->Mes,$value->idLote , $value->idLabor  , $value->idFinca);
                        if($value->idFinca == 1 AND $value->Mes == 6){
                            echo '<pre>';
                            D($value);
                            echo '</pre>';
                            D($Lpromedio);
                        }

                        if($Lpromedio->porcentaje > -1){
                            $sql = "INSERT INTO muestras_anual SET
                                    mes = '".($value->Mes-1)."',
                                    fecha = '".$value->fecha."',
                                    semana = '".$value->semana."',
                                    idZona = '".$value->idZona."',
                                    idLote = '".$value->idLote."',
                                    idTipoLabor = '".$value->idTipoLabor."',
                                    idLabor = '".$value->idLabor."',
                                    idFinca = '".$value->idFinca."',
                                    promedio = '".$Lpromedio->porcentaje."'";
                                    echo '<pre>';
                                        D($sql);
                                    echo '</pre>';
                            $this->setPromedio($sql);
                        }
                    }
                }
        }

        // D($date);
             

        $response->main["labels"] = array_unique($response->main["labels"]);
        $response->week["labels"] = array_unique($response->week["labels"]);
        $response->auditoria["labels"] = array_unique($response->auditoria["labels"]);
        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        return $response;
    }

    private function setPromedio($sql){
        if(!empty($sql)){
            $this->db->query($sql);
        }
        return true;
    }

    private function getPromMonthLoteAvg($month , $lote , $labor , $idfinca){
        $lote = $lote;
        $idfinca = $idfinca;
        $idLabor = $labor;
        $response = new stdClass;
        $response->data = 0;
        $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
                CASE
                    WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                    ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
                END AS porcentaje,
                CASE
                    WHEN valor > 0 THEN  NULL
                    ELSE (CausasIndividual / malas) * 100
                END AS porcentaje2
                FROM 
                (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor > 0 AND idLabor = '{$labor}' AND MONTH(fecha) = '{$month}') AS buenas,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor = 0 AND idLabor = '{$labor}' AND MONTH(fecha) = '{$month}')  AS malas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                        MONTH(fecha) = '{$month}' AND
                        idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$labor}') AS muestras
                FROM muestras 
                WHERE 
                    MONTH(fecha) = '{$month}' AND
                    idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$labor}'
                GROUP BY idLaborCausa) as t
                WHERE valor > 0
                ORDER BY valor DESC , causa ASC";

                // D($sql);
        $response->data = $this->db->queryRow($sql);
        if(!isset($response->data->porcentaje)){
            $response->data->porcentaje = -1;
        }
        return $response->data;
    }
}
