<?php defined('PHRAPI') or die("Direct access not allowed!");

class Data {
	public $name;
	private $db;
	private $agent_user;

	public function __construct(){
		$this->agent_user = getString('agent' , 0);
        $this->db = DB::getInstance($this->agent_user);
        $this->allweights = DB::getInstance('allweights');
    }
    
    public function execute(){
        $func = getString('func' , 0);
        return $this->{$func}();
    }

    public function calidad(){
        $sql = "SELECT c.nombre as Categorias, CONCAT(d.siglas, '-', d.defecto) as Defectos
                FROM calidad_categorias c
                INNER JOIN calidad_defectos d ON id_categoria = c.id";
        return $this->db->queryAll($sql);
    }

	public function labores(){
		$response = new stdClass;
        $sql = "SELECT *,(SELECT nombre FROM tipo_labor WHERE id = id_tipo_labor ) AS nombre_tipo_labor FROM labores ORDER BY nombre";
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
        	$response = $fincas;
        }
        return $response;
	}

    public function TipoLabores(){
        $response = new stdClass;
        $sql = "SELECT id ,nombre FROM tipo_labor ORDER BY nombre";
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
            $response = $fincas;
        }
        return $response;
    }

	public function lotes(){
        $response = new stdClass;
        if($this->agent_user == "agroaereo"){
            $sql = "SELECT lotes.id, lotes.nombre, lotes.idFinca FROM fincas INNER JOIN lotes ON idFinca = fincas.id WHERE fincas.status = 1 AND cultivo = 'Banano'";
        }else{
            $sql = "SELECT * FROM lotes";
        }
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
        	$response = $fincas;
        }
        return $response;
    }
    
    public function lotesName(){
        $sql = "SELECT fincas.nombre AS finca, lotes.nombre AS lote FROM lotes INNER JOIN fincas ON idFinca = fincas.id WHERE fincas.id IN(1, 4)";
        return $this->db->queryAll($sql);
    }

    public function fincas(){
        $response = new stdClass;
        if($this->agent_user == "sumifru"){
            $sql = "SELECT id, nombre, numeroLotes, hectareas FROM fincas WHERE status = 1";
        }
        else if($this->agent_user == "agroaereo"){
            $sql = "SELECT id, nombre FROM fincas WHERE status = 1 AND cultivo = 'Banano'";
        }
        else if($this->agent_user == "dole"){
            $sql = "SELECT id ,zona AS idZona , nombre, protectores_fundas  FROM fincas";
        }else{
            $sql = "SELECT *  FROM fincas";
        }
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
            $response = $fincas;
        }
        return $response;
    }

    public function subfincas(){
        $response = new stdClass;
        $sql = "SELECT id , id_finca as idFinca, nombre  FROM subfincas";
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
            $response = $fincas;
        }
        return $response;
    }

    public function zonas(){
        $response = new stdClass;
        $sql = "SELECT id , nombre FROM zonas";
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
                $response = $fincas;
        }
        return $response;
    }

	public function causas(){
	    $response = new stdClass;
        $sql = "SELECT CONCAT_WS('-' , id , causa) AS id , CONCAT_WS('-' , id , causa) AS causa,idLabor,valor,tipo,status  FROM labores_causas WHERE status = 1 ORDER BY causa";
        if($this->agent_user == 'quintana' || $this->agent_user == 'agroaereo'){
            $sql = "SELECT causa AS id , causa AS causa, idLabor, valor, tipo, status FROM labores_causas WHERE status = 1 ORDER BY causa";
        }
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
        	$response = $fincas;
        }
        return $response;
    }
    
    public function racimosCausas(){
        $sql = "SELECT id, nombre FROM produccion_racimos_causas ORDER BY nombre";
        return $this->db->queryAll($sql);
	}

    public function fincasMerma(){
        $response = new stdClass;
        if($this->agent_user == 'marun'){
            $sql = "SELECT id, nombre, tara_merma FROM fincas";
        }else{
            $sql = "SELECT id, nombre FROM fincas";
        }
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
                $response = $fincas;
        }
        return $response;
    }

    public function Cables(){
        $response = new stdClass;
        $sql = "SELECT id_finca , finca , lote, cable FROM produccion GROUP BY cable";
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
                $response = $fincas;
        }
        return $response;
    }

    public function Palanca(){
        $response = new stdClass;

        if($this->agent_user == "marun"){
            $Ohter = [
                [
                    "id_finca" => "1",
                    "finca" => "Carolina",
                    "palanca" => "Otro",
                ],            
                [
                    "id_finca" => "2",
                    "finca" => "San José 1",
                    "palanca" => "Otro",
                ],
                [
                    "id_finca" => "3",
                    "finca" => "San José 2",
                    "palanca" => "Otro",
                ],
            ];
            $sql = "SELECT idFinca AS id_finca , 'Carolina' AS finca , nombre AS palanca FROM personal
                    WHERE idFinca = 1
                    UNION ALL
                    SELECT 2 AS id_finca , 'San José 1' AS finca , nombre AS palanca FROM personal
                    WHERE idFinca = 3
                    UNION ALL
                    SELECT  idFinca AS id_finca , 'San José 2' AS finca , nombre AS palanca FROM personal
                    WHERE idFinca = 3";
            $fincas = $Ohter + $this->db->queryAll($sql);
        }else{
            $sql = "SELECT id_finca , finca , palanca FROM produccion GROUP BY palanca";
            $fincas = $this->db->queryAll($sql);
        }
        
        if(count($fincas) > 0){
                $response = $fincas;
        }
        return $response;
    }

    public function clientes(){
        $response = new stdClass;

        /*
        SACAR EXPORTADORAS DE ALLWEIGHTS
        if($this->agent_user == 'sumifru'){
            $sql = "SELECT Exportadora.`exportadora` nombre, Exportadora.id, 'Calidad 3' prontoforms
                    FROM tbempacadora
                    INNER JOIN tbtipodecaja ON tbtipodecaja.idfinca = tbempacadora.`idfinca`
                    INNER JOIN marca ON tbtipodecaja.`idmarca` = marca.`id`
                    INNER JOIN Exportadora ON marca.`idexportadora` = Exportadora.`id`
                    WHERE idempacadora IN (17,18,22,25,26,28,31,32,33,34,35,36,43)
                    GROUP BY Exportadora.id";
            return $this->allweights->queryAll($sql);
        }*/

        $sql = "SELECT id , nombre , prontoforms  FROM clients WHERE id > 0";
        if($this->agent_user == "marun"){
            $sql = "SELECT id_cliente, id_exportador, CONCAT(id_cliente, '-', id_exportador) as client_exportador, nombre , prontoforms  FROM exportadores_clientes LEFT JOIN clients ON id_cliente = clients.id WHERE id_cliente > 1";
        }
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
            $response = $fincas;
        }
        return $response;
    }

    public function marcas(){
        $response = new stdClass;

        /*
        SACAR MARCAS DE ALLWEIGHTS
        if($this->agent_user == 'sumifru'){
            $sql = "SELECT Exportadora.`exportadora` cliente, Exportadora.id id_cliente, marca.id, 'Calidad 3' prontoforms, marca.nombre
                    FROM tbempacadora
                    INNER JOIN tbtipodecaja ON tbtipodecaja.idfinca = tbempacadora.`idfinca`
                    INNER JOIN marca ON tbtipodecaja.`idmarca` = marca.`id`
                    INNER JOIN Exportadora ON marca.`idexportadora` = Exportadora.`id`
                    WHERE idempacadora IN (17,18,22,25,26,28,31,32,33,34,35,36,43)
                    GROUP BY marca.id";
            return $this->allweights->queryAll($sql);
        }*/

        $sql = "SELECT id , id_client, nombre, prontoforms FROM brands ORDER BY nombre";
        if($this->agent_user == "marun"){
            $sql = "SELECT id , CONCAT(id_client, '-', id_exportador) as client_exportador, nombre , id_exportador, prontoforms FROM brands WHERE status = 'Activo' ORDER BY nombre";
        }
        if($this->agent_user == "sumifru"){
            $sql = "SELECT id , id_client, nombre, prontoforms, (SELECT nombre FROM clients WHERE id = id_client) AS cliente FROM brands";
        }
        $fincas = $this->db->queryAll($sql);
        if(count($fincas) > 0){
            $response = $fincas;
        }
        return $response;
    }

    public function perchas(){
        $data = $this->db->queryAll("SELECT id, cliente, local FROM lancofruit_clientes WHERE status = 'Activo' ORDER BY LOCAL");
        if(count($data) == 0){
            $data = ["id" => 0, "local" => "NO HAY"];
        }
        return $data;
    }

    public function categorias(){
        $data = $this->db->queryAll("SELECT id, nombre FROM calidad_categorias WHERE status = 1 ORDER BY nombre");
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "NO HAY"];
        }
        return $data;
    }

    public function defectos(){
        $data = $this->db->queryAll("SELECT id, id_categoria, nombre, valor, siglas FROM calidad_defectos WHERE status = 1 ORDER BY nombre");
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "NO HAY"];
        }
        return $data;
    }

    public function cajas(){
        $data = $this->db->queryAll("SELECT id, id_marca, nombre FROM cajas WHERE status = 1");
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "NO HAY"];
        }
        return $data;
    }

    public function exportadores(){
        $data = $this->db->queryAll("SELECT id, nombre FROM exportadores WHERE status = 'Activo' ORDER BY nombre");
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "NO HAY"];
        }
        return $data;
    }

    public function destinos(){
        $data = $this->db->queryAll("SELECT id, nombre FROM destinos WHERE status = 1 ORDER BY nombre");
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "NO HAY"];
        }
        return $data;
    }

    public function personal(){
        if($this->agent_user == "marcel"){
            $data = $this->db->queryAll("SELECT id, nombre, cargo, perfil FROM personal WHERE status = 1 ORDER BY nombre");
        }else{
            $data = $this->db->queryAll("SELECT id, nombre FROM personal WHERE status = 1 ORDER BY nombre");
        }        
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "NO HAY"];
        }
        return $data;
    }

    public function laboresAsistencia(){
        $data = $this->db->queryAll("SELECT labores_asistencia.id, labores_asistencia.nombre, codigo, labores_labor_campo.id as id_labor_campo, labores_labor_campo.labor_campo
                                    FROM labores_asistencia 
                                    INNER JOIN labores_labor_campo ON idLabor = labores_asistencia.id
                                    ORDER BY nombre");
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "NO HAY"];
        }
        return $data;
    }

    public function fincasRacimos(){
        $sql = "SELECT id, nombre, hectareas
                FROM fincas
                WHERE racimos = 'Activo'";
        $data = $this->db->queryAll($sql);
        if(count($data) == 0){
            $data[] = [
                "id" => 0,
                "nombre" => "NO HAY FINCAS",
                "hectareas" => 0
            ];
        }
        return $data;
    }

    // MARCEL
    public function lancofruitRutas(){
        $data = $this->db->queryAll("SELECT id, nombre FROM lancofruit_rutas WHERE status = 1");
        if(count($data) == 0){
            $data[] = ["id" => 0, "nombre" => "NO HAY"];
        }
        return $data;
    }
    public function lancofruitVendedores(){
        $data = $this->db->queryAll("SELECT id, nombre, id_ruta FROM lancofruit_vendedores WHERE status = 1");
        if(count($data) == 0){
            $data[] = ["id" => 0, "nombre" => "NO HAY"];
        }
        return $data;
    }

    public function lancofruitCategorias(){
        $sql = "SELECT id AS id_categoria, nombre AS categoria
                FROM lancofruit_categorias
                WHERE status = 'Activo'";
        $data = $this->db->queryAll($sql);

        if(count($data) == 0){
            $data[] = [
                "id_categoria" => 0,
                "categoria" => "NO HAY CATEGORIAS"
            ];
        }
        return $data;
    }

    public function lancofruitSubcategorias(){
        $sql = "SELECT id AS id_subcategoria, nombre AS subcategoria, id_categoria
                FROM lancofruit_subcategorias
                WHERE id_categoria > 0 AND status = 'Activo'";
        $data = $this->db->queryAll($sql);

        if(count($data) == 0){
            $data[] = [
                "id_subcategoria" => 0,
                "subcategoria" => "NO HAY SUBCATEGORIAS",
                "id_categoria" => 0
            ];
        }
        return $data;
    }

    public function causasRacimos(){
        $sql = "SELECT id, nombre, codigo
                FROM produccion_racimos_causas
                WHERE status = 'Activo'";
        return $this->db->queryAll($sql);
    }

    public function sectoresCacaoMarun(){
        $sql = "SELECT *
                FROM cat_sectores
                WHERE status = 'Activo'";
        return $this->db->queryAll($sql);
    }
    public function lotesCacaoMarun(){
        $sql = "SELECT *
                FROM cat_lotes
                WHERE status = 'Activo'";
        return $this->db->queryAll($sql);
    }
    public function sublotesCacaoMarun(){
        $sql = "SELECT s.*, l.id_sector
                FROM cat_sublotes s
                LEFT JOIN cat_lotes l ON s.id_lote = l.id";
        return $this->db->queryAll($sql);
    }

    public function tipoCajas(){
        $sql = "SELECT codigo_caja as id, nombre
                FROM produccion_tipo_caja
                WHERE status = 'Activo' AND year = YEAR(CURRENT_DATE)";
        return $this->db->queryAll($sql);
    }
}