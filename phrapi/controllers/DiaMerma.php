<?php defined('PHRAPI') or die("Direct access not allowed!");

class DiaMerma {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);

        $this->search = "Total Peso%";
        $this->MermaTypePeso = "Kg";
    }
    
    public function last(){
		$response = new stdClass;
		
		$response->days = $this->db->queryAllOne("SELECT DISTINCT date_fecha FROM merma");
		$response->fecha = $this->db->queryOne("SELECT MAX(date_fecha) FROM merma");
        return $response;
    }

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}

		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}

		if(isset($postdata->statusLbKg) && $postdata->statusLbKg == 1){
			$this->MermaTypePeso = "Kg";
		}else{
			$this->MermaTypePeso = "Lb";
		}

        if($this->session->id_company == 7){
            $this->MermaTypePeso = "Lb";
        }


		if($postdata->categoria == 'ENFUNDE' && $this->session->id_company == 7){
			$postdata->categoria = 'LOTERO AEREO';
		}elseif($postdata->categoria == 'ENFUNDE' && $this->session->id_company == 3){
			$postdata->categoria = 'LOTERO AEREO';
		}elseif($postdata->categoria == 'ENFUNDE' && ($this->session->id_company == 4 || $this->session->id_company == 9)){
			$postdata->categoria = 'LOTERO AEREO';
		}

		// if($this->session->id_company == 4){
		// 	$postdata->categoria = 'COSECHA';
		// }

		$defectos = $this->getDefectos($postdata);
		$peso = $this->getPeso($postdata);
		$merma = $this->getMerma($postdata);
		/*----------  MERMA  ----------*/
		$response = new stdClass;
		$response->merma = $merma->data;
		$response->merma_travel  = $merma->maxTravel;
		$response->merma_details  = $merma->details;
		$response->merma_widthCell  = $merma->widthCell;
		$response->merma_campos  = $merma->details_campos;
		/*----------  MERMA  ----------*/

		/*----------  DEFECTOS  ----------*/
		$response->defectos = $defectos->data;
		$response->defectos_travel  = $defectos->maxTravel;
		$response->defectos_details  = $defectos->details;
		$response->details_campos  = $defectos->details_campos;
		/*----------  DEFECTOS  ----------*/
		/*----------  PESO  ----------*/
		$response->peso = $peso->data;
		$response->peso_travel  = $peso->maxTravel;
		$response->peso_details  = $peso->details;
		#dedos promedio
		$response->dedos_prom = $peso->data;
		foreach($response->dedos_prom as $key => $value){
			$value = (array) $value;
			$sum = 0;
			$cont = 0;
			foreach($response->peso_travel as $keyPeso => $valuePeso){
				$valuePeso = (object) $valuePeso;
				if(isset($value[$valuePeso->viaje]) && $valuePeso->viaje != "TOTAL"){
					$sum = $sum + ((float) $value[$valuePeso->viaje]["promedio"] / $value[$valuePeso->viaje]["racimos_procesados"]) * 5;
					$cont++;
				}
			}
			$response->dedos_prom[$key]["TOTAL"]["promedio_dedos"] = $sum / $cont;			
		}
		#dedos promedio
		#dedos details
		$sum_campos = 0;
		$response->dedos_details = $peso->details;
		foreach($response->dedos_details as $key => $value){
			$value = (array) $value;
			foreach($value as $keyLabor => $valueLabor){
				foreach($response->peso_travel as $keyPeso => $valuePeso){
					$valuePeso = (object) $valuePeso;
					if(isset($value[$valuePeso->viaje][$keyLabor]) && $valuePeso->viaje != "TOTAL"){
						$$sum_campos += 
							((float) $value[$valuePeso->viaje][$keyLabor]["label"] / $value[$valuePeso->viaje]["racimos_procesados"]) * 5;
						// $cont += 1;
					}
				}
				// $response->dedos_details["TOTAL"][$keyLabor]["promedio_dedos"] = $sum_campos;
			}
		}
		#dedos details
		$response->peso_widthCell  = $peso->widthCell;
		$response->peso_campos  = $peso->details_campos;
		/*----------  PESO  ----------*/

		$response->id_company = (int)$this->session->id_company;
		$response->adicional = [];

		if($this->session->id_company == 7 && $postdata->categoria == 'LOTERO AEREO'){
			/*----------  LOTERO AEREO  ----------*/
			$response->adicional = $this->getLoteroAereo($postdata);
			/*----------  LOTERO AEREO  ----------*/
		}elseif($this->session->id_company == 3 && $postdata->categoria == 'LOTERO AEREO'){
			/*----------  LOTERO AEREO  ----------*/
			$response->adicional = $this->getLoteroAereo($postdata);
			/*----------  LOTERO AEREO  ----------*/
		}
		

		$response->palanca = ["" => "Todos"] + $this->getPalanca($postdata);
		$response->fincas = [];
		if($this->session->id_company == 7 || $this->session->id_company == 6 || $this->session->id_company == 11){
			$response->fincas = ["" => "Todas"] + $this->getFincas($postdata);
		}

		return $response;
	}

	private function getLoteroAereo($filters = []){
		if(isset($filters->fecha_inicial)){
			$fecha = $filters->fecha_inicial;
		}
		$sWhere = "";
		if(isset($filters->idFinca) && $filters->idFinca != ""){
			$id_finca = $filters->idFinca;
			if($id_finca != ""){
				if($id_finca == "9999999" && $this->session->id_company == 7){
					$sWhere .= " AND id_finca IN (2,3)";
				}else{
					$sWhere .= " AND id_finca = {$id_finca}";
				}
			}
		}

		$cantidad = "(cantidad)";
		if($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$peso_neto = "((racimo - tallo) / 2.2)";
				$cantidad = "(cantidad / 2.2)";
			}else{
				$peso_neto = "(racimo - tallo)";
				$cantidad = "(cantidad)";
			}
		}


		$response = new stdClass;
		$sqlTotales = "SELECT id_merma ,campo AS id, bloque , TIME(fecha),campo,SUM({$cantidad}) AS cantidad FROM merma_detalle 
			INNER JOIN merma ON merma_detalle.id_merma = merma.id
			WHERE type = 'LOTERO AEREO' AND flag = 3 AND DATE(fecha) = '{$fecha}' AND campo != 'Información adicional'
			{$sWhere}
			GROUP BY bloque 
			ORDER BY bloque , id_merma";
			// F($sqlTotales);
		$dataTotales = $this->db->queryAll($sqlTotales);

		$sql = "SELECT id_merma , bloque AS bloque, TIME(fecha) AS fecha,campo,SUM({$cantidad}) AS cantidad FROM merma_detalle 
				INNER JOIN merma ON merma_detalle.id_merma = merma.id
				WHERE type = 'LOTERO AEREO' AND flag = 3 AND DATE(fecha) = '{$fecha}' AND campo != 'Información adicional' 
				{$sWhere}
				GROUP BY (fecha)
				ORDER BY fecha ,bloque";
		$data = $this->db->queryAll($sql);


		$sqlTotalesDetalle = "SELECT bloque AS bloque, merma_detalle.campo AS id ,SUM({$cantidad}) AS cantidad FROM merma_detalle, merma 
						WHERE id_merma = merma.id 
						AND type = 'LOTERO AEREO' AND flag = 3 AND DATE(fecha) = '{$fecha}' AND campo != 'Información adicional' 
						{$sWhere}
						GROUP BY bloque, campo";

		$dataTotalesDetalle = $this->db->queryAll($sqlTotalesDetalle);

		$bloque = "";
		$fecha_bloque = "";
		$viaje = "VIAJE";
		$count = [];
		$detalle = [];
		$viajes = [];
		$promedio = [];
		$class = "default";
		$indicador = new stdClass;
		$indicador->default = "empty";
		$indicador->mayor =  "fa-arrow-up font-red-thunderbird";
		$indicador->menor =  "fa-arrow-down font-green-jungle";
		$indicador->igual =  "fa-minus font-yellow-lemon";
		foreach ($data as $key => $value) {
			$value = (object)$value;
			if($bloque != $value->bloque){
				$bloque = $value->bloque;
				$detalle[trim($bloque)] = [];
				$$count[trim($bloque)] = [];
				$class = "default";
			}
			if($fecha_bloque != $value->fecha){
				$fecha_bloque = $value->fecha;
				$count[trim($bloque)]++;
				$viaje = "VIAJE ".$count[trim($bloque)];

				$promedio[$bloque][$count[trim($bloque)]] = (float)$value->cantidad;
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				if($count[trim($bloque)] > 1){
					if($promedio[$bloque][$count[trim($bloque)]] == $promedio[$bloque][($count[trim($bloque)]-1)]){
						$class = "igual";
					}elseif($promedio[$bloque][$count[trim($bloque)]] > $promedio[$bloque][($count[trim($bloque)]-1)]){
						$class = "mayor";
					}elseif($promedio[$bloque][$count[trim($bloque)]] < $promedio[$bloque][($count[trim($bloque)]-1)]){
						$class = "menor";
					}
				}
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/


				$viajes[trim($bloque)][] = [
					"viaje" => $viaje,
					"palanca" => trim($bloque),
				];

				// $detalle[trim($bloque)][$viaje]["Total"] = []
				$detalle[trim($bloque)][$viaje] = [
					"viaje" => $viaje,
					"campo" => $value->campo,
					"palanca" => trim($bloque),
					"fecha" => $fecha_bloque,
					"promedio" => (float)$value->cantidad,
					"indicador" => $indicador->{$class}
				];
				$details[trim($bloque)][$viaje] = $this->getDetalleLoteroAereo($value->id_merma);
				// $response->details_campos[trim($bloque)] = $this->getDetalleLoteroAereoCampos($value->id_merma);
			}else{
				$promedio[$bloque][$count[trim($bloque)]] = (float)$value->cantidad;
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				if($count[trim($bloque)] > 1){
					if($promedio[$bloque][$count[trim($bloque)]] == $promedio[$bloque][($count[trim($bloque)]-1)]){
						$class = "igual";
					}elseif($promedio[$bloque][$count[trim($bloque)]] > $promedio[$bloque][($count[trim($bloque)]-1)]){
						$class = "mayor";
					}elseif($promedio[$bloque][$count[trim($bloque)]] < $promedio[$bloque][($count[trim($bloque)]-1)]){
						$class = "menor";
					}
				}
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/

				$detalle[trim($bloque)][$viaje] = [
					"viaje" => $viaje,
					"campo" => $value->campo,
					"palanca" => trim($bloque),
					"fecha" => $fecha_bloque,
					"promedio" => (float)$value->cantidad,
					"indicador" => $indicador->{$class}
				];
			}
		}

		/*----------  REVISAMOS EL DETALLE PARA SACAR LA ICONERA  ----------*/
		$details_promedio = [];
		$count_rows = [];
		$viaje = "";
		foreach ($details as $key => $value) {
			foreach ($value as $llave => $valor) {
				if($viaje != $llave){
					$viaje = $llave;
					$count_rows[$key]++;
					$class = "default";
				}
				foreach ($valor as $k => $v) {
					$details_promedio[$count_rows[$key]][$k] = (float)$v->label;
					if($count_rows[$key] > 1){
						if($details_promedio[$count_rows[$key]][$k] == $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "igual";
						}elseif($details_promedio[$count_rows[$key]][$k] > $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "mayor";
						}elseif($details_promedio[$count_rows[$key]][$k] < $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "menor";
						}
					}
					$v->indicador = $indicador->{$class};
				}
			}
		}
		/*----------  REVISAMOS EL DETALLE PARA SACAR LA ICONERA  ----------*/

		foreach($dataTotales as $key => $value) {
			$value = (object)$value;
			$bloque = $value->bloque;
			$viaje = 'TOTAL';

			$viajes[trim($bloque)][$viaje ] = [
				"viaje" => $viaje,
				"palanca" => trim($bloque),
			];

			$detalle[trim($bloque)][$viaje] = [
				"viaje" => $viaje,
				"palanca" => trim($bloque),
				"fecha" => $fecha,
				"promedio" => (float)$value->cantidad,
				"indicador" => 'default'
			];

			$value->id = $viaje;
			$value->indicador = 'default';
			$value->label = $value->enfunde;

			// $response->details_campos[trim($palanca)][$viaje] = $viaje;
		}

		foreach($dataTotalesDetalle as $key => $value){
			$viaje = 'TOTAL';
			$bloque = $value->bloque;
			$obj = new stdClass;
			$obj->indicador = 'default';
			$obj->id = $value->id;
			$obj->label = $value->cantidad;

			$details[trim($bloque)][$viaje][$value->id] = $obj;
		}
		$maxViaje = max($viajes);
		$response->maxTravel = $maxViaje;
		$response->total = $detalle_total;
		$response->details = $details;
		$response->data = $detalle;
		$response->details_campos = $this->getDetalleLoteroAereoCampos();	
		return $response;
	}

	private function getDetalleLoteroAereoCampos($id_merma = 0 , $type = "ENFUNDE"){
			$sql = "
				SELECT campo AS id,campo AS label FROM merma_detalle 
					INNER JOIN merma ON merma_detalle.id_merma = merma.id
					WHERE type = 'LOTERO AEREO' AND flag = 3 AND campo != 'Información adicional'
					GROUP BY campo
					ORDER BY fecha ,bloque";
			$data = $this->db->queryAllSpecial($sql);
		return $data;
    }
    
	private function getDetalleLoteroAereo($id_merma = 0){
		$id_merma = (int)$id_merma;
		$response = new stdClass;
		$response->data = [];
		if($id_merma > 0){
			$cantidad = "(cantidad)";
			if($this->session->id_company == 3){
				if($this->MermaTypePeso == "Kg"){
					$peso_neto = "((racimo - tallo) / 2.2)";
					$cantidad = "(cantidad / 2.2)";
				}else{
					$peso_neto = "(racimo - tallo)";
					$cantidad = "(cantidad)";
				}
			}
			$sql = "SELECT id_merma , bloque , TIME(fecha),campo AS id,({$cantidad}) AS cantidad , {$cantidad} AS label FROM merma_detalle 
					INNER JOIN merma ON merma_detalle.id_merma = merma.id
					WHERE type = 'LOTERO AEREO' AND flag = 3 AND id_merma = {$id_merma} AND campo != 'Información adicional'
					-- GROUP BY TIME(fecha)
					ORDER BY fecha ,bloque";
			// D($sql);
			$data = $this->db->queryAll($sql);
			$class = "default";
			$count = 0;
			$label = 0;
			$indicador = new stdClass;
			$indicador->default =  "empty";
			$indicador->mayor =  "fa-arrow-up font-red-thunderbird";
			$indicador->menor =  "fa-arrow-down font-green-jungle";
			$indicador->igual =  "fa-minus font-yellow-lemon";
			foreach ($data as $key => $value) {
				$count++;
				$promedio[$count] = (float)$value->cantidad;
				$value->label = (float)$value->cantidad;
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				if($count > 1){
					if($promedio[$count] == $promedio[($count-1)]){
						$class = "igual";
					}elseif($promedio[$count] > $promedio[($count-1)]){
						$class = "mayor";
					}elseif($promedio[$count] < $promedio[($count-1)]){
						$class = "menor";
					}
				}
				$value->indicador = $indicador->{$class};
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				$response->data[$value->id] = $value;
			}
		}
		return $response->data;
	}

	private function getPalanca($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";
		if(isset($filters->idFinca) && $filters->idFinca != ""){
			$id_finca = $filters->idFinca;
			if($id_finca != ""){
				if($id_finca == "9999999" && $this->session->id_company == 7){
					$sWhere .= " AND id_finca IN (2,3)";
				}else{
					$sWhere .= " AND id_finca = {$id_finca}";
				}
			}
		}
		
		$response = new stdClass;
		$sql = "SELECT palanca AS id , palanca AS label  FROM merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND palanca != '' $sWhere
				GROUP BY TRIM(palanca)";
		$response->data = [];
		$response->data = $this->db->queryAllSpecial($sql);
		return $response->data;
	}	

	private function getFincas($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		
		$response = new stdClass;
		$sql = "SELECT id_finca AS id , finca AS label  FROM merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND finca != ''
				GROUP BY id_finca";
		$response->data = [];
		if($this->session->id_company == 7){
			$response->data = ["9999999" => "San José"] + $this->db->queryAllSpecial($sql);
		}else{
			$response->data = $this->db->queryAllSpecial($sql);
		}
		return $response->data;
	}

	private function getMerma($filters = []){
		$fecha = "";
		if(isset($filters->fecha_inicial)){
			$fecha = $filters->fecha_inicial;
		}

		$sWhere = "";

		if(isset($filters->idFinca) && $filters->idFinca != ""){
			$id_finca = $filters->idFinca;
			if($id_finca != ""){
				if($id_finca == "9999999" && $this->session->id_company == 7){
					$sWhere .= " AND id_finca IN (2,3)";
				}else{
					$sWhere .= " AND id_finca = {$id_finca}";
				}
			}
		}
		$response = new stdClass;

		$response->data = [];

		$categoria = "ENFUNDE";


		$campo = [
			"ENFUNDE" => "enfunde",
			"COSECHA" => "cosecha",
		];
		$peso_neto = "(peso_neto)";
		$cantidad = "(cantidad)";
		$campo_lotePalanca = "TRIM(palanca)";

		if($this->session->id_company == 7 || $this->session->id_company == 4 || $this->session->id_company == 9){
			$peso_neto = "(racimo - tallo)";
			$categoria = "LOTERO AEREO";

			$campo = [
				"LOTERO AEREO" => "lotero",
				"COSECHA" => "cosecha"
			];
			if($this->session->id_company == 7){
				$campo["DESHOJE"] = "deshoje";
			}

			if(isset($filters->categoria) && $filters->categoria == "LOTERO AEREO"){
				$campo_lotePalanca = "TRIM(bloque)";
			}
		}

		if($this->session->id_company == 3){
			$categoria = "LOTERO AEREO";

			$campo = [
				"LOTERO AEREO" => "lotero",
				"COSECHA" => "cosecha",
			];

			if(isset($filters->categoria) && $filters->categoria == "LOTERO AEREO"){
				$campo_lotePalanca = "TRIM(bloque)";
			}
		}

		if(isset($filters->categoria)){
			$categoria = $filters->categoria;
		}

		/*----------  AGRUPAR POR : Lote , Palanca  ----------*/
		if($categoria == 'ENFUNDE'){
			$campo_lotePalanca = "TRIM(bloque)";
		}
		/*----------  AGRUPAR POR : Lote , Palanca  ----------*/

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$condition = "AND flag = 1";
		if($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$peso_neto = "((racimo - tallo) / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$peso_neto = "(racimo - tallo)";
			}
			if($categoria == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}
		}
		
		$sqlTotalesDetalle = "SELECT {$campo_lotePalanca} AS palanca  , campo AS id, ROUND(AVG(total),2) AS cantidad 
				FROM (
					SELECT fecha ,{$campo_lotePalanca} AS palanca , bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad, {$peso_neto} , (({$cantidad} / {$peso_neto}) * 100) as Total , porcentaje_merma  
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE DATE(fecha) = '{$fecha}' AND type = '{$categoria}' {$condition} AND {$campo_lotePalanca} != '' {$typeMerma}
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS') {$sWhere} 
				) as detalle
				GROUP BY {$campo_lotePalanca} ,campo";

		$sqlTotales = "SELECT id_merma  AS id, {$campo_lotePalanca} AS palanca,TIME(fecha) AS fecha,ROUND(AVG(Total),2) AS '{$campo[$categoria]}' FROM (
			SELECT fecha ,palanca , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , {$peso_neto} , (({$cantidad}) / {$peso_neto}) * 100 as Total , porcentaje_merma  FROM merma
			INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
			WHERE DATE(fecha) = '{$fecha}' AND campo like '%Total Peso%' AND type = '{$categoria}' AND {$campo_lotePalanca} != '' {$typeMerma}
			AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS') {$sWhere} ) as detalle
			GROUP BY {$campo_lotePalanca}";
		// D($sqlTotales);
		$sql = "SELECT id_merma AS id,{$campo_lotePalanca} AS palanca,TIME(fecha) AS fecha,ROUND(AVG(Total),2) AS '{$campo[$categoria]}' FROM (
					SELECT fecha ,{$campo_lotePalanca} AS palanca , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , {$peso_neto} , (({$cantidad}) / {$peso_neto}) * 100 as Total , porcentaje_merma 
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE DATE(fecha) = '{$fecha}' AND campo like '%Total Peso%' AND type = '{$categoria}' AND {$campo_lotePalanca} != '' {$typeMerma}
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS') {$sWhere} 
			) as detalle
			GROUP BY {$campo_lotePalanca} , fecha
			ORDER BY {$campo_lotePalanca} , fecha"; 
		
		$data = $this->db->queryAll($sql);
		$dataTotales = $this->db->queryAll($sqlTotales);
		$dataTotalesDetalle = $this->db->queryAll($sqlTotalesDetalle);
		$palanca = "";
		$fecha_palanca = "";
		$viaje = "V";
		$count = [];
		$detalle = [];
		$viajes = [];
		$promedio = [];
		$class = "default";
		$indicador = new stdClass;
		$indicador->default = "empty";
		$indicador->mayor =  "fa-arrow-up font-red-thunderbird";
		$indicador->menor =  "fa-arrow-down font-green-jungle";
		$indicador->igual =  "fa-minus font-yellow-lemon";
		foreach ($data as $key => $value) {
			$value = (object)$value;
			if($palanca != $value->palanca){
				$palanca = $value->palanca;
				$detalle[trim($palanca)] = [];
				$$count[trim($palanca)] = [];
				$class = "default";
			}

			if($fecha_palanca != $value->fecha){
				$fecha_palanca = $value->fecha;
				$count[trim($palanca)]++;
				$viaje = "V ".$count[trim($palanca)];

				$promedio[$palanca][$count[trim($palanca)]] = (float)$value->{$campo[$categoria]};
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				if($count[trim($palanca)] > 1){
					if($promedio[$palanca][$count[trim($palanca)]] == $promedio[$palanca][($count[trim($palanca)]-1)]){
						$class = "igual";
					}elseif($promedio[$palanca][$count[trim($palanca)]] > $promedio[$palanca][($count[trim($palanca)]-1)]){
						$class = "mayor";
					}elseif($promedio[$palanca][$count[trim($palanca)]] < $promedio[$palanca][($count[trim($palanca)]-1)]){
						$class = "menor";
					}
				}
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/


				$viajes[trim($palanca)][] = [
					"viaje" => $viaje,
					"palanca" => trim($palanca),
				];


				$detalle[trim($palanca)][$viaje] = [
					"viaje" => $viaje,
					"palanca" => trim($palanca),
					"fecha" => $fecha_palanca,
					"promedio" => (float)$value->{$campo[$categoria]},
					"indicador" => $indicador->{$class}
				];
				
				$details[trim($palanca)][$viaje] = $this->getDetalleMerma($value->id , $categoria);
				#D("palanca $palanca id $value->id categoria $categoria");
				$response->details_campos[trim($palanca)] = $this->getDetalleMermaCampos($value->id , $categoria);
			}
		}

		/*----------  REVISAMOS EL DETALLE PARA SACAR LA ICONERA  ----------*/
		$details_promedio = [];
		$count_rows = [];
		$viaje = "";
		foreach ($details as $key => $value) {
			foreach ($value as $llave => $valor) {
				if($viaje != $llave){
					$viaje = $llave;
					$count_rows[$key]++;
					$class = "default";
				}
				foreach ($valor as $k => $v) {
					$details_promedio[$count_rows[$key]][$k] = (float)$v->label;
					if($count_rows[$key] > 1){
						if($details_promedio[$count_rows[$key]][$k] == $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "igual";
						}elseif($details_promedio[$count_rows[$key]][$k] > $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "mayor";
						}elseif($details_promedio[$count_rows[$key]][$k] < $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "menor";
						}
					}
					$v->indicador = $indicador->{$class};
				}
			}
		}
		/*----------  REVISAMOS EL DETALLE PARA SACAR LA ICONERA  ----------*/

		foreach($dataTotales as $key => $value) {
			$value = (object)$value;
			$palanca = $value->palanca;
			$viaje = 'TOTAL';

			$viajes[trim($palanca)][] = [
				"viaje" => $viaje,
				"palanca" => trim($palanca),
			];

			$detalle[trim($palanca)][$viaje] = [
				"viaje" => $viaje,
				"palanca" => trim($palanca),
				"fecha" => $fecha,
				"promedio" => (float)$value->{$campo[$categoria]},
				"indicador" => 'default'
			];

			$value->id = $viaje;
			$value->indicador = 'default';
			$value->label = $value->enfunde;

			// $response->details_campos[trim($palanca)][$viaje] = $viaje;
		}
		
		foreach($dataTotalesDetalle as $key => $value){
			$viaje = 'TOTAL';
			$palanca = $value->palanca;
			$obj = new stdClass;
			$obj->indicador = 'default';
			$obj->id = $value->id;
			$obj->label = $value->cantidad;

			$details[trim($palanca)][$viaje][$value->id] = $obj;
		}

		$maxViaje = max($viajes);

		$response->maxTravel = $maxViaje;
		$response->widthCell = round((95 / count($maxViaje)),2);
		$response->details = $details;
		$response->data = $detalle;

		//D($response);
		return $response;
	}

	private function getDetalleMermaCampos($id_merma = 0 , $type = "ENFUNDE"){

		$condition = "AND flag = 1";
		if($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$peso_neto = "(racimo - tallo)";
				$cantidad = "(cantidad)";
			}else{
				$peso_neto = "((racimo - tallo) / 2.2)";
				$cantidad = "(cantidad / 2.2)";
			}
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}
		}

		$sql = "SELECT campo AS id ,campo AS label  
				FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE id_merma = {$id_merma} 
					AND type = '{$type}' $condition
					AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS') 
				GROUP BY type ,campo";
		$data = $this->db->queryAllSpecial($sql);
		return $data;
	}

	private function getDetalleMerma($id_merma = 0 , $type = "ENFUNDE"){
		$id_merma = (int)$id_merma;
		$response = new stdClass;
		$response->data = [];
		if($id_merma > 0){
			$categoria = "ENFUNDE";

			$campo = [
				"ENFUNDE" => "enfunde",
				"COSECHA" => "cosecha",
			];

			$peso_neto = "(peso_neto)";
			$cantidad = "(cantidad)";
			if($this->session->id_company == 7  || $this->session->id_company == 4 || $this->session->id_company == 9){
				$peso_neto = "(racimo - tallo)";
			}

			$condition = "AND flag = 1";
			if($this->session->id_company == 3){
				if($this->MermaTypePeso == "Kg"){
					$cantidad = "(cantidad / 2.2)";
					$peso_neto = "((racimo - tallo) / 2.2)";
				}else{
					$cantidad = "(cantidad)";
					$peso_neto = "(racimo - tallo)";
				}
				if($type == "LOTERO AEREO"){
					$condition = "AND flag = 3";
				}
			}

			$sql = "SELECT campo AS id, campo AS label,SUM(cantidad), (Total) AS cantidad , AVG(porcentaje_merma) 
					FROM (
						SELECT fecha ,palanca , bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , {$peso_neto} , ({$cantidad} / {$peso_neto}) * 100 as Total , porcentaje_merma 
						FROM merma
						INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
						WHERE id_merma = {$id_merma} AND type = '{$type}' {$condition}
							AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS') 
					) as detalle
					GROUP BY type ,campo";
					
			$data = $this->db->queryAll($sql);
			$class = "default";
			$count = 0;
			$label = 0;
			$indicador = new stdClass;
			$indicador->default =  "empty";
			$indicador->mayor =  "fa-arrow-up font-red-thunderbird";
			$indicador->menor =  "fa-arrow-down font-green-jungle";
			$indicador->igual =  "fa-minus font-yellow-lemon";
			foreach ($data as $key => $value) {
				$count++;
				$promedio[$count] = (float)$value->cantidad;
				$value->label = (float)$value->cantidad;
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				if($count > 1){
					if($promedio[$count] == $promedio[($count-1)]){
						$class = "igual";
					}elseif($promedio[$count] > $promedio[($count-1)]){
						$class = "mayor";
					}elseif($promedio[$count] < $promedio[($count-1)]){
						$class = "menor";
					}
				}
				$value->indicador = $indicador->{$class};
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				$response->data[$value->id] = $value;
			}
		}
		return $response->data;
	}

	private function getPeso($filters = []){
		$fecha = "";
		if(isset($filters->fecha_inicial)){
			$fecha = $filters->fecha_inicial;
		}

		$sWhere = "";

		if(isset($filters->idFinca) && $filters->idFinca != ""){
			$id_finca = $filters->idFinca;
			if($id_finca != ""){
				if($id_finca == "9999999" && $this->session->id_company == 7){
					$sWhere .= " AND id_finca IN (2,3)";
				}else{
					$sWhere .= " AND id_finca = {$id_finca}";
				}
			}
		}

		$categoria = "ENFUNDE";

		$campo = [
			"ENFUNDE" => "enfunde",
			"COSECHA" => "cosecha",
		];

		if(isset($filters->categoria)){
			$categoria = $filters->categoria;
		}


		if($this->MermaTypePeso == "Kg"){
			$cantidad = "(cantidad / 2.2)";
		}else{
			$cantidad = "(cantidad)";
		}

		$response = new stdClass;

		$response->data = [];

		$campo = "TRIM(palanca)";

		$typeMerma = "";
		$type = "Total Daños";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
			$type = "Total Merma";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad)";
			}else{
				$cantidad = "(cantidad / 2.2)";
			}
		}


		$condition = "AND flag = 1";
		if($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$peso_neto = "((racimo - tallo) / 2.2)";
				$cantidad = "(cantidad / 2.2)";
			}else{
				$peso_neto = "(racimo - tallo)";
				$cantidad = "(cantidad)";
			}
			if($filters->categoria == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}
		}

		if($this->session->id_company == 7 || $this->session->id_company == 6 || $this->session->id_company == 3 || $this->session->id_company == 4 || $this->session->id_company == 9){
			if(isset($filters->categoria) && $filters->categoria == "LOTERO AEREO" || $filters->categoria == "ENFUNDE"){
				$campo = "TRIM(bloque)";
			}
			$sql = "SELECT id_merma , racimos_procesados, type , fecha , {$campo} AS palanca, SUM(cantidad) AS danhos_peso, AVG(Total) AS cantidad , AVG(porcentaje_merma) FROM (
				SELECT fecha  ,{$campo} AS palanca, bloque,merma_detalle.id_merma , racimos_procesados, type ,campo , {$cantidad} AS cantidad , peso_neto , ({$cantidad} / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) = '{$fecha}' AND campo like '%Total Peso%' AND {$campo} != '' {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS') {$sWhere} ) as detalle
				WHERE type = '{$categoria}'
				GROUP BY {$campo} , fecha";
		#D($sql);
		$sqlTotales = "SELECT {$campo} AS palanca, merma_detalle.campo AS id ,SUM({$cantidad}) AS danhos_total, racimos_procesados FROM merma_detalle, merma 
					WHERE id_merma = merma.id 
					AND DATE(merma.fecha) = '{$fecha}'
					AND type = '{$categoria}'
					{$condition}
					AND campo NOT LIKE '%Total Peso%' 
					AND campo NOT LIKE '%".$type."%'
					AND campo != 'Observaciones'
					AND {$campo} != ''
					{$sWhere}
					{$typeMerma}
					GROUP BY {$campo}";
		#D($sqlTotales);
		$sqlTotalesDetalle = "SELECT {$campo} AS palanca, merma_detalle.campo AS id ,AVG({$cantidad}) AS danhos_total, racimos_procesados
					FROM merma_detalle, merma 
					WHERE id_merma = merma.id 
					AND DATE(merma.fecha) = '{$fecha}'
					AND type = '{$categoria}'
					{$condition}
					AND campo NOT LIKE '%Total Peso%' 
					AND campo NOT LIKE '%".$type."s%'
					AND campo != 'Observaciones'
					AND {$campo} != ''
					{$sWhere}
					{$typeMerma}
					GROUP BY {$campo}, campo";
		// D($sqlTotalesDetalle);
		}else{
			/*----------  AGRUPAR POR : Lote , Palanca  ----------*/
			if($categoria == 'ENFUNDE'){
				$campo = "TRIM(bloque)";
			}
			/*----------  AGRUPAR POR : Lote , Palanca  ----------*/
			$sql = "SELECT id_merma , type , fecha , {$campo} AS palanca, SUM(cantidad) AS danhos_peso, AVG(Total) AS cantidad , AVG(porcentaje_merma) 
					FROM (
						SELECT fecha  ,{$campo} AS palanca, bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , peso_neto , ({$cantidad} / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
						INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
						WHERE DATE(fecha) = '{$fecha}' AND campo like '%Total Peso%' AND {$campo} != '' {$typeMerma}
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS') {$sWhere}
					) as detalle
					WHERE type = '{$categoria}'
					GROUP BY {$campo} , fecha";
			$condition = "AND campo LIKE '%Peso%' ";
			
			if($this->session->id_company == 4 || $this->session->id_company == 9){
				$condition = "AND flag = 1";
			}
			$sqlTotales = "SELECT {$campo} AS palanca, merma_detalle.campo AS id ,SUM({$cantidad}) AS danhos_total, racimos_procesados FROM merma_detalle, merma 
						WHERE id_merma = merma.id 
						AND DATE(merma.fecha) = '{$fecha}'
						AND type = '{$categoria}'
						{$condition}
						AND campo NOT LIKE '%Total Peso%' 
						AND campo NOT LIKE '%Total Daños%'
						AND campo != 'Observaciones'
						AND {$campo} != ''
						{$sWhere}
						{$typeMerma}
						GROUP BY {$campo}";

			$sqlTotalesDetalle = "SELECT {$campo} AS palanca, merma_detalle.campo AS id ,SUM({$cantidad}) AS danhos_total, racimos_procesados FROM merma_detalle, merma 
						WHERE id_merma = merma.id 
						AND DATE(merma.fecha) = '{$fecha}'
						AND type = '{$categoria}'
						{$condition}
						AND campo NOT LIKE '%Total Peso%' 
						AND campo NOT LIKE '%Total Daños%'
						AND campo != 'Observaciones'
						AND {$campo} != ''
						{$typeMerma}
						GROUP BY {$campo}, campo";
			// D($sqlTotalesDetalle);
		}

		$data = $this->db->queryAll($sql);
		$dataTotales = $this->db->queryAll($sqlTotales);
		$dataTotalesDetalle = $this->db->queryAll($sqlTotalesDetalle);
		// D($sql);
		$palanca = "";
		$fecha_palanca = "";
		$viaje = "VIAJE";
		$count = [];
		$detalle = [];
		$viajes = [];
		$promedio = [];
		$class = "default";
		$indicador = new stdClass;
		$indicador->default =  "empty";
		$indicador->mayor =  "fa-arrow-up font-red-thunderbird";
		$indicador->menor =  "fa-arrow-down font-green-jungle";
		$indicador->igual =  "fa-minus font-yellow-lemon";
		foreach ($data as $key => $value) {
			$value = (object)$value;
			if($palanca != $value->palanca){
				$palanca = $value->palanca;
				$detalle[trim($palanca)] = [];
				$$count[trim($palanca)] = [];
				$class = "default";
			}

			if($fecha_palanca != $value->fecha){
				$fecha_palanca = $value->fecha;
				$count[trim($palanca)]++;
				$viaje = "VIAJE ".$count[trim($palanca)];

				$promedio[$count[trim($palanca)]] = (float)$value->danhos_peso;
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				if($count[trim($palanca)] > 1){
					if($promedio[$count[trim($palanca)]] == $promedio[($count[trim($palanca)]-1)]){
						$class = "igual";
					}elseif($promedio[$count[trim($palanca)]] > $promedio[($count[trim($palanca)]-1)]){
						$class = "mayor";
					}elseif($promedio[$count[trim($palanca)]] < $promedio[($count[trim($palanca)]-1)]){
						$class = "menor";
					}
				}
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/

				$viajes[trim($palanca)][] = [
					"viaje" => $viaje,
					"palanca" => trim($palanca),
				];

				$detalle[trim($palanca)][$viaje] = [
					"viaje" => $viaje,
					"palanca" => trim($palanca),
					"fecha" => $fecha_palanca,
					"promedio" => (float)$value->danhos_peso,
					"indicador" => $indicador->{$class},
					"racimos_procesados" => (float)$value->racimos_procesados
				];

				
				$details[trim($palanca)][$viaje] = $this->getDetallePeso($value->id_merma , $categoria);
			}
		}

		/*----------  REVISAMOS EL DETALLE PARA SACAR LA ICONERA  ----------*/
		$details_promedio = [];
		$count_rows = [];
		$viaje = "";
		foreach ($details as $key => $value) {
			foreach ($value as $llave => $valor) {
				if($viaje != $llave){
					$viaje = $llave;
					$count_rows[$key]++;
					$class = "default";
				}
				foreach ($valor as $k => $v) {
					$details_promedio[$count_rows[$key]][$k] = (float)$v->label;
					if($count_rows[$key] > 1){
						if($details_promedio[$count_rows[$key]][$k] == $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "igual";
						}elseif($details_promedio[$count_rows[$key]][$k] > $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "mayor";
						}elseif($details_promedio[$count_rows[$key]][$k] < $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "menor";
						}
					}
					$v->indicador = $indicador->{$class};
				}
			}
		}
		/*----------  REVISAMOS EL DETALLE PARA SACAR LA ICONERA  ----------*/

		foreach($dataTotales as $key => $value) {
			$value = (object)$value;
			$palanca = $value->palanca;
			$viaje = 'TOTAL';

			$viajes[trim($palanca)][] = [
				"viaje" => $viaje,
				"palanca" => trim($palanca),
			];

			$detalle[trim($palanca)][$viaje] = [
				"viaje" => $viaje,
				"palanca" => trim($palanca),
				"fecha" => $fecha,
				"promedio" => (float)$value->danhos_total,
				"indicador" => 'default',
				"racimos_procesados" => (int)$value->racimos_procesados
			];

			$value->id = $viaje;
			$value->indicador = 'default';
			$value->label = $value->enfunde;

			$response->details_campos[trim($palanca)][$viaje] = $viaje;
		}
		
		foreach($dataTotalesDetalle as $key => $value){
			$viaje = 'TOTAL';
			$palanca = $value->palanca;
			$obj = new stdClass;
			$obj->indicador = 'default';
			$obj->id = $value->id;
			$obj->label = (double)$value->danhos_total;

			$details[trim($palanca)][$viaje][$value->id] = $obj;
		}


		$maxViaje = max($viajes);
		// D($details);
		$response->maxTravel = $maxViaje;
		$response->widthCell = round((95 / count($maxViaje)),2);
		$response->dedos_prom = $details;
		$response->details = $details;
		$response->details_campos = $this->getDetallePesoCampos($categoria , $fecha);		
		$response->data = $detalle;
		return $response;

	}

	private function getDetallePesoCampos($type = "ENFUNDE" , $fecha){
		$condition = "AND flag = 1";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}
		}


		if($this->session->id_company == 7 || $this->session->id_company == 6 || $this->session->id_company == 3){
            $condition .= " AND DATE(fecha) = '{$fecha}'";
			$sql = "SELECT  campo AS id ,campo AS label FROM merma_detalle , merma
					WHERE merma.id = merma_detalle.id_merma AND type = '{$type}'
					$condition
					AND campo NOT LIKE '%Total Peso%' 
					AND campo NOT LIKE '%Total Daños%'
					AND campo != 'Observaciones'
					GROUP BY campo";
		}else if($this->session->id_company == 11){
			$condition .= " AND DATE(fecha) = '{$fecha}'";
			$sql = "SELECT  campo AS id ,campo AS label FROM merma_detalle , merma
					WHERE merma.id = merma_detalle.id_merma 
						AND type = '{$type}'
						$condition
						AND campo NOT LIKE '%Total Peso%' 
						AND campo NOT LIKE '%Total Daños%'
						AND campo != 'Observaciones'
						AND flag = 1
					GROUP BY campo";
		}else{
			$condition = "AND campo LIKE '%Peso%' ";
			if($this->session->id_company == 4 || $this->session->id_company == 9){
				$condition = "AND flag = 1";
			}
			$sql = "SELECT  campo AS id ,campo AS label FROM merma_detalle 
					WHERE type = '{$type}' 
					{$condition}
					AND campo NOT LIKE '%Total Peso%' 
					AND campo NOT LIKE '%Total Daños%'
					AND campo != 'Observaciones'
					GROUP BY campo";
		}
        // D($sql);
		$data = $this->db->queryAllSpecial($sql);
		return $data;
	}

	private function getDetallePeso($id_merma = 0 , $type = "ENFUNDE"){
		$id_merma = (int)$id_merma;
		$response = new stdClass;
		$response->data = [];
		if($this->MermaTypePeso == "Kg"){
			$cantidad = "(cantidad / 2.2)";
		}else{
			$cantidad = "(cantidad)";
		}

		$condition = "AND flag = 1";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}
		}

		if($id_merma > 0){
			if($this->session->id_company == 7 || $this->session->id_company == 6 || $this->session->id_company == 3){
				$type_filter = "Total Daños";
				if($this->session->id_company == 6){
					$type_filter = "Total Merma";
					if($this->MermaTypePeso == "Kg"){
						$cantidad = "(cantidad)";
					}else{
						$cantidad = "(cantidad / 2.2)";
					}
				}elseif($this->session->id_company == 3){
					if($this->MermaTypePeso == "Kg"){
						$cantidad = "(cantidad / 2.2)";
					}else{
						$cantidad = "(cantidad)";
					}
				}

				$sql = "SELECT  campo AS id ,{$cantidad} AS label FROM merma_detalle 
						WHERE id_merma = {$id_merma} AND type = '{$type}' 
						$condition
						AND campo NOT LIKE '%Total Peso%' 
						AND campo NOT LIKE '%".$type_filter."%'
						AND campo != 'Observaciones'
						GROUP BY campo";
			}else if($this->session->id_company == 11){
				$condition = "AND flag = 1";
				$sql = "SELECT campo AS id ,{$cantidad} AS label FROM merma_detalle 
						WHERE id_merma = {$id_merma} AND 
							type = '{$type}' 
							{$condition}
							AND campo NOT LIKE '%Total Peso%' 
							AND campo NOT LIKE '%Total Daños%'
							AND campo != 'Observaciones'";
				
			}else{
				$condition = "AND campo LIKE '%Peso%' ";
				if(in_array($this->session->id_company, [4,9,12]) ){
					$condition = "AND flag = 1";
				}
				$sql = "SELECT campo AS id ,{$cantidad} AS label FROM merma_detalle 
						WHERE id_merma = {$id_merma} AND 
							type = '{$type}' 
							{$condition}
							AND campo NOT LIKE '%Total Peso%' 
							AND campo NOT LIKE '%Total Daños%'
							AND campo != 'Observaciones'";
			}
			
			$data = $this->db->queryAll($sql);

			$class = "default";
			$count = 0;
			$indicador = new stdClass;
			$indicador->default =  "empty";
			$indicador->mayor =  "fa-arrow-up font-red-thunderbird";
			$indicador->menor =  "fa-arrow-down font-green-jungle";
			$indicador->igual =  "fa-minus font-yellow-lemon";
			foreach ($data as $key => $value) {
				$count++;
				$promedio[$count] = (float)$value->label;
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				if($count > 1){
					if($promedio[$count] == $promedio[($count-1)]){
						$class = "igual";
					}elseif($promedio[$count] > $promedio[($count-1)]){
						$class = "mayor";
					}elseif($promedio[$count] < $promedio[($count-1)]){
						$class = "menor";
					}
				}
				$value->indicador = $indicador->{$class};
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				$response->data[$value->id] = $value;
			}
		}

		return $response->data;
	}

	private function getDefectos($filters = []){
		$fecha = "";
		if(isset($filters->fecha_inicial)){
			$fecha = $filters->fecha_inicial;
		}
		$categoria = "ENFUNDE";

		$sWhere = "";

		if(isset($filters->idFinca) && $filters->idFinca != ""){
			$id_finca = $filters->idFinca;
			if($id_finca != ""){
				if($id_finca == "9999999" && $this->session->id_company == 7){
					$sWhere .= " AND id_finca IN (2,3)";
				}else{
					$sWhere .= " AND id_finca = {$id_finca}";
				}
			}
		}

		$campo = [
			"ENFUNDE" => "enfunde",
			"COSECHA" => "cosecha",
		];

		if(isset($filters->categoria)){
			$categoria = $filters->categoria;
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		/*----------  AGRUPAR POR : Lote , Palanca  ----------*/
		$sGroup = "TRIM(palanca)";
		if($categoria == 'ENFUNDE' || $categoria == 'LOTERO AEREO'){
			$sGroup = "TRIM(bloque)";
		}
		/*----------  AGRUPAR POR : Lote , Palanca  ----------*/

		$response = new stdClass;

		$response->data = [];

		$sql = "SELECT id_merma , type , fecha , $sGroup AS palanca ,AVG(cantidad) AS danhos_peso, AVG(Total) AS cantidad , AVG(porcentaje_merma) 
                FROM (
                    SELECT fecha  ,TRIM(palanca) AS palanca , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  
                    FROM merma
                    INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                    WHERE DATE(fecha) = '{$fecha}' AND campo like '%{$this->search}%'  AND palanca != '' {$typeMerma}
                    AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS','RECUSADOS', 'PESO RACIMOS') {$sWhere} 
                ) as detalle
				WHERE type = '{$categoria}' AND palanca != ''
                GROUP BY $sGroup , fecha";
                
				// D($sql);
		$sqlTotales = "SELECT  $sGroup AS palanca, campo AS id ,SUM(cantidad) AS danhos_total 
                    FROM merma_detalle, merma 
					WHERE DATE(merma.fecha) = '{$fecha}'
                        AND merma.id = id_merma
                        AND type = '{$categoria}'
                        AND campo NOT LIKE '%Peso%' 
                        AND campo NOT LIKE '%Total Peso%' 
                        AND campo NOT LIKE '%Total Daños%'
                        AND campo != 'Observaciones'
                        AND TRIM(palanca) != ''
                        {$sWhere}
                        {$typeMerma}
					GROUP BY $sGroup";
				// D($sqlTotales);
		$sqlTotalesDetalle = "SELECT  $sGroup AS palanca, campo AS id ,SUM(cantidad) AS danhos_total 
                    FROM merma_detalle, merma 
					WHERE DATE(merma.fecha) = '{$fecha}'
                        AND merma.id = id_merma
                        AND type = '{$categoria}'
                        AND campo NOT LIKE '%Peso%' 
                        AND campo NOT LIKE '%Total%' 
                        AND campo != 'Observaciones'
                        AND $sGroup != ''
					    {$sWhere}
					    {$typeMerma}
					GROUP BY $sGroup, campo";
		// D($sqlTotalesDetalle);
		$data = $this->db->queryAll($sql);
		$dataTotales = $this->db->queryAll($sqlTotales);
		$dataTotalesDetalle = $this->db->queryAll($sqlTotalesDetalle);
		$palanca = "";
		$fecha_palanca = "";
		$viaje = "VIAJE";
		$count = [];
		$detalle = [];
		$viajes = [];
		$details = [];
		$details_campos = [];
		$promedio = [];
		$class = "default";
		$indicador = new stdClass;
		$indicador->default =  "empty";
		$indicador->mayor =  "fa-arrow-up font-red-thunderbird";
		$indicador->menor =  "fa-arrow-down font-green-jungle";
		$indicador->igual =  "fa-minus font-yellow-lemon";
		foreach ($data as $key => $value) {
			$value = (object)$value;
			if($palanca != $value->palanca){
				$palanca = $value->palanca;
				$detalle[trim($palanca)] = [];
				$count[trim($palanca)] = 0;
				$class = "default";
			}

			if($fecha_palanca != $value->fecha){
				$fecha_palanca = $value->fecha;
				$count[trim($palanca)]++;
				$viaje = "VIAJE ".$count[trim($palanca)];

				$promedio[$count[trim($palanca)]] = (float)$value->danhos_peso;
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				// D($count);
				if($count[trim($palanca)] > 1){
					// D(trim($palanca));
					// D($count[trim($palanca)]);
					// D($promedio[$count[trim($palanca)]] . "    " .  $promedio[($count[trim($palanca)]-1)]);
					if($promedio[$count[trim($palanca)]] == $promedio[($count[trim($palanca)]-1)]){
						$class = "igual";
					}elseif($promedio[$count[trim($palanca)]] > $promedio[($count[trim($palanca)]-1)]){
						$class = "mayor";
					}elseif($promedio[$count[trim($palanca)]] < $promedio[($count[trim($palanca)]-1)]){
						$class = "menor";
					}
				}
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				$viajes[trim($palanca)][] = [
					"viaje" => $viaje,
					"palanca" => trim($palanca),
				];

				$detalle[trim($palanca)][$viaje] = [
					"viaje" => $viaje,
					"palanca" => trim($palanca),
					"fecha" => $fecha_palanca,
					"promedio" => (float)$value->danhos_peso,
					"indicador" => $indicador->{$class}
				];

				$details[trim($palanca)][$viaje] = $this->getDetalleDefectos($value->id_merma , $categoria);
			}
		}


		/*----------  REVISAMOS EL DETALLE PARA SACAR LA ICONERA  ----------*/
		$details_promedio = [];
		$count_rows = [];
		$viaje = "";
		foreach ($details as $key => $value) {
			foreach ($value as $llave => $valor) {
				if($viaje != $llave){
					$viaje = $llave;
					$count_rows[$key]++;
					$class = "default";
				}
				foreach ($valor as $k => $v) {
					$details_promedio[$count_rows[$key]][$k] = (float)$v->label;
					if($count_rows[$key] > 1){
						if($details_promedio[$count_rows[$key]][$k] == $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "igual";
						}elseif($details_promedio[$count_rows[$key]][$k] > $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "mayor";
						}elseif($details_promedio[$count_rows[$key]][$k] < $details_promedio[($count_rows[$key]-1)][$k]){
							$class = "menor";
						}
					}
					$v->indicador = $indicador->{$class};
				}
			}
		}
		/*----------  REVISAMOS EL DETALLE PARA SACAR LA ICONERA  ----------*/

		foreach($dataTotales as $key => $value) {
			$value = (object)$value;
			$palanca = $value->palanca;
			$viaje = 'TOTAL';

			$viajes[trim($palanca)][] = [
				"viaje" => $viaje,
				"palanca" => trim($palanca),
			];

			$detalle[trim($palanca)][$viaje] = [
				"viaje" => $viaje,
				"palanca" => trim($palanca),
				"fecha" => $fecha,
				"promedio" => (float)$value->danhos_total,
				"indicador" => 'default'
			];

			$value->id = $viaje;
			$value->indicador = 'default';
			$value->label = $value->enfunde;

			$response->details_campos[trim($palanca)][$viaje] = $viaje;
		}
		
		foreach($dataTotalesDetalle as $key => $value){
			$viaje = 'TOTAL';
			$palanca = $value->palanca;
			$obj = new stdClass;
			$obj->indicador = 'default';
			$obj->id = $value->id;
			$obj->label = $value->danhos_total;

			$details[trim($palanca)][$viaje][$value->id] = $obj;
		}

		

		$maxViaje = max($viajes);
		// D($maxViaje);
		$response->maxTravel = $maxViaje;
		$response->details = $details;
		$response->details_campos = $this->getDetalleDefectosCampos($categoria);
		$response->data = $detalle;

		return $response;

	}

	private function getDetalleDefectosCampos($type = "ENFUNDE"){
			$sWhere = "";
			if($this->session->id_company == 7){
				$sWhere = " AND campo LIKE '%(# Daños)%' AND campo NOT LIKE '%Total Merma%'";
			}

			$sql = "SELECT  campo AS id ,campo AS label 
                    FROM merma_detalle 
					WHERE type = '{$type}' 
                        AND flag = 1
                        {$sWhere}
					GROUP BY campo";
			// D($sql);
			$data = $this->db->queryAllSpecial($sql);
		return $data;
	}

	private function getDetalleDefectos($id_merma = 0 , $type = "ENFUNDE"){
		$id_merma = (int)$id_merma;
		$response = new stdClass;
		$response->data = [];
		if($id_merma > 0){
			$sql = "SELECT  campo AS id ,cantidad AS label 
                    FROM merma_detalle 
					WHERE id_merma = {$id_merma} AND 
                        type = '{$type}' 
                        AND flag = 1
                        AND campo != 'Observaciones'";
                    
			$data = $this->db->queryAll($sql);
			$class = "default";
			$count = 0;
			$indicador = new stdClass;
			$indicador->default =  "empty";
			$indicador->mayor =  "fa-arrow-up font-red-thunderbird";
			$indicador->menor =  "fa-arrow-down font-green-jungle";
			$indicador->igual =  "fa-minus font-yellow-lemon";
			foreach ($data as $key => $value) {
				$count++;
				$promedio[$count] = (float)$value->label;
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				if($count > 1){
					if($promedio[$count] == $promedio[($count-1)]){
						$class = "igual";
					}elseif($promedio[$count] > $promedio[($count-1)]){
						$class = "mayor";
					}elseif($promedio[$count] < $promedio[($count-1)]){
						$class = "menor";
					}
				}
				$value->indicador = $indicador->{$class};
				/*----------  REVISAMOS SI EL PROMEDIO ANTERIOS ES MAYOR AL ACTUAL  ----------*/
				$response->data[$value->id] = $value;
			}
		}

		return $response->data;
	}
}