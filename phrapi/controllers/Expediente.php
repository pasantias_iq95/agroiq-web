<?php defined('PHRAPI') or die("Direct access not allowed!");

class Expediente {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$response = new stdClass;
		$response->data = [];
		$sql = "SELECT id as label, nombre as id FROM personal WHERE status = 1 ORDER BY nombre";
		$personal = $this->db->queryAllSpecial($sql);
		
		foreach($personal as $nombre => $id){
			$data[$id] = $this->db->queryAll("SELECT id, IFNULL((SELECT status FROM personal_expediente WHERE id_documento = documento.id AND id_personal = '{$id}'), 'NO') AS status
				FROM documento
				WHERE STATUS = 'ACTIVO'");
			$data[$id]["total"] = $this->db->queryRow("SELECT ROUND(COUNT(1)/(SELECT COUNT(1) FROM documento WHERE STATUS = 'ACTIVO')*100, 2) AS por
				FROM personal_expediente
				WHERE id_personal = '{$id}' AND status = 'SI'")->por;
		}
		$response->documentos = $this->db->queryAll("SELECT id, id as label FROM documento WHERE status = 'ACTIVO' ORDER BY id");

		$sum = 0;
		foreach($response->documentos as $doc){
			$data["totales"][$doc->id] = $this->db->queryRow("SELECT ROUND(COUNT(1)/(SELECT COUNT(1) FROM personal WHERE STATUS = 1)*100, 2) as por
				FROM personal_expediente
				WHERE id_documento = '{$doc->id}' AND STATUS = 'SI'")->por;
			$sum += $data["totales"][$doc->id];
		}
		$sum = $sum / count($data["totales"]);
		$data["totales"]["total"] = ROUND($sum, 2);

		$response->personal = $personal;
		$response->data = $data;
		return $response;
	}

	public function show(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;

		if((int)$postdata->id_personal > 0 && (int)$postdata->id_doc > 0){
			$per = $this->db->queryRow("SELECT nombre as personal FROM personal WHERE id = '{$postdata->id_personal}'");
			$doc = $this->db->queryRow("SELECT descripcion FROM documento WHERE id = '{$postdata->id_doc}'");
			$doc_per = $this->db->queryRow("SELECT status, observaciones, ext FROM personal_expediente WHERE id_personal = '{$postdata->id_personal}' AND id_documento = '{$postdata->id_doc}'");

			if($doc_per){
				$response->status = $doc_per->status;
				$response->observaciones = $doc_per->observaciones;
				$response->ext = $doc_per->ext;
			}else{
				$response->status = "NO";
			}
			$response->descripcion = $doc->descripcion;
			$response->personal = $per->personal;
		}

		return $response;
	}

	public function delete(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		if($postdata->id_personal > 0 && $postdata->id_doc > 0){
			$data = $this->db->queryRow("SELECT * FROM personal_expediente WHERE id_personal = '{$postdata->id_personal}' AND id_documento = '{$postdata->id_doc}'");

			$dir_subida = realpath(PHRAPI_PATH.'../expediente_documentos/'.$this->session->agent_user);
			$nameFile = "/expediente_d={$postdata->id_doc}_p={$postdata->id_personal}.{$data->ext}";
			unlink($nameFile);

			$this->db->query("UPDATE personal_expediente
				SET
					ext = '',
					doc = ''
				WHERE id_personal = '{$postdata->id_personal}' AND id_documento = '{$postdata->id_doc}'");
		}
		return true;
	}

	public function save(){
		//$postdata = (object)json_decode(file_get_contents("php://input"));
		$postdata = (object) $_POST;
		$nameFile = "";
		$ext = "";

		if(isset($_FILES["archivo"])){
			$ext = explode(".", $_FILES["archivo"]["name"]);
			$ext = $ext[count($ext)-1];

			if(in_array($ext, ["pdf", "png", "jpg", "jpeg"])){
				$dir_subida = realpath(PHRAPI_PATH.'../expediente_documentos/'.$this->session->agent_user."/");
				$nameFile = "/expediente_d={$_POST['id_doc']}_p={$_POST['id_personal']}.{$ext}";

				$fichero_subido = $dir_subida . $nameFile;

				move_uploaded_file($_FILES['archivo']['tmp_name'], $fichero_subido);
			}
		}

		if((int)$postdata->id_personal > 0 && (int)$postdata->id_doc > 0){
			$exist = $this->db->queryRow("SELECT * FROM personal_expediente WHERE id_personal = '{$postdata->id_personal}' AND id_documento = '{$postdata->id_doc}'");
			if(!isset($exist->id)){
				$this->db->query("INSERT INTO personal_expediente
					SET
						id_documento = '{$postdata->id_doc}',
						id_personal = '{$postdata->id_personal}',
						status = '{$postdata->estatus}',
						".($ext != '' ? "ext = '{$ext}'," : '')."
						".($nameFile != '' ? "doc = '{$nameFile}'," : '')."
						fecha_actual = CURRENT_DATE,
						observaciones = '{$postdata->observaciones}'");
			}else{
				$this->db->query("UPDATE personal_expediente
					SET
						status = '{$postdata->estatus}',
						".($ext != '' ? "ext = '{$ext}'," : '')."
						".($nameFile != '' ? "doc = '{$nameFile}'," : '')."
						fecha_actual = CURRENT_DATE,
						observaciones = '{$postdata->observaciones}'
					WHERE id_personal = '{$postdata->id_personal}' AND id_documento = '{$postdata->id_doc}'");
			}
		}

		header("Location: http://app.procesos-iq.com/tthhFPersonal?idPersonal={$postdata->id_personal}");
		return true;
	}

}






