<?php defined('PHRAPI') or die("Direct access not allowed!");

class Exportar {
    public $name;
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        // D($this->session);
        $this->token = '289150995ed28e8860f9161d3cc9f259';
        $this->Service = new ServicesNode('http://procesos-iq.com:3000/' , $this->token);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function pdf(){
        include PHRAPI_PATH.'libs/utilities/dompdf/dompdf_config.inc.php';
        $html = $_POST["table"];

        $cut = strpos($html, '<tr role="row" class="filter">');
        $cut2 = strpos($html, '</thead>');
        
        $part1 = substr($html, 0, $cut);
        $part2 = substr($html, $cut2, strlen($html));
        $html = $part1.$part2;
        
        $cut = strpos($html, "<tfoot");
        $html = utf8_decode("<table>".substr($html, 0, $cut)."</table>");
        echo $html;
        return;
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream("Información General.pdf");
    }

}