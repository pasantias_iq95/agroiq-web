<?php defined('PHRAPI') or die("Direct access not allowed!");

class Geoposicion {
	public $name;
	private $db;
	private $table;

	public function __construct(){
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->table = "muestras";
        if($this->session->id_company == 7){
        	$this->table = "muestras_anual";
        }
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;

		$filters = (object)[
			"type" => getValueFrom($postdata,'type','MES',FILTER_SANITIZE_STRING),
			"auditoria" => getValueFrom($postdata,'auditoria',0,FILTER_SANITIZE_PHRAPI_INT)
		];

		$filters->type = strtolower($filters->type);
		$response->id_company = $this->session->id_company;
		$response->auditorias = $this->{$filters->type}();
		$response->fincas = $this->fincas();
		return $response;
	}

	public function Markers(){
		$response = new stdClass;
		$response->coordenadas = $this->coordenadas();
		return $response;
	}

	private function periodo(){
		$sql = "SELECT periodo AS id, periodo AS label FROM {$this->table} WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY periodo";
		$response = $this->db->queryAll($sql);
		return $response;
	}

	private function fincas(){
		$sql = "SELECT id AS id , nombre AS label FROM fincas";
		$response = $this->db->queryAllSpecial($sql);
		return $response;
	}

	private function semana(){
		$sql = "SELECT WEEK(fecha) AS id , WEEK(fecha) AS label FROM {$this->table} WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY WEEK(fecha)";
		if($this->session->id_company == 7){
			$sql = "SELECT WEEK(fecha) AS id , WEEK(fecha) AS label FROM {$this->table} WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY WEEK(fecha)";
		}
		$response = $this->db->queryAllSpecial($sql);
		return $response;
	}

	private function mes(){
		$sql = "SELECT MONTH(fecha) AS id , MONTH(fecha) AS label FROM {$this->table} WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY MONTH(fecha)";
		if($this->session->id_company == 7){
			$sql = "SELECT (mes + 1) AS id , (mes + 1) AS label FROM {$this->table} WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY mes";
		}
		$response = $this->db->queryAllSpecial($sql);
		return $response;
	}

	private function auditoria(){
		$sql = "SELECT WEEK(fecha) AS id , WEEK(fecha) AS label FROM {$this->table} WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY WEEK(fecha)";
		if(in_array($this->session->id_company, [7, 12])){
			$sql = "SELECT periodo AS id , periodo AS label FROM {$this->table} WHERE YEAR(fecha) = YEAR(CURRENT_DATE) GROUP BY periodo";
		}
		$response = $this->db->queryAllSpecial($sql);
		return $response;
	}

	private function coordenadas(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$auditoria = getValueFrom($postdata,'auditorias',0,FILTER_SANITIZE_PHRAPI_INT);
		$idFinca = getValueFrom($postdata,'idFinca',1,FILTER_SANITIZE_PHRAPI_INT);
		$type = getValueFrom($postdata,'type','MES',FILTER_SANITIZE_STRING);
		$markers = [];

		$filters = [
			"MES" => "MONTH(fecha) = {$auditoria}",
			"SEMANA" => "WEEK(fecha) = {$auditoria}",
			"AUDITORIA" => "periodo = {$auditoria}",
			"PERIODO" => "periodo = {$auditoria}"
		];

		$sWhere = "";
		if($idFinca > 0){
			$sWhere = " AND {$this->table}.idFinca = {$idFinca}";
		}

		if($auditoria > 0){
			$sql = "SELECT muestras_coords.id AS muestras ,lat , lng , 
						{$this->table}.labor , 
						{$this->table}.laborCausaDesc AS nombre_muestra , 
						{$this->table}.archivoJson_contador,
						{$this->table}.archivoJson
					FROM muestras_coords
					INNER JOIN {$this->table} ON muestras_coords.id_muestra = {$this->table}.id
					WHERE {$filters[$type]}";
			if($this->session->id_company == 7){
				$sql = "SELECT muestras_coords.id AS muestras ,lat , lng ,(SELECT nombre FROM labores WHERE id = {$this->table}.idLabor) AS labor , 
						(SELECT nombre FROM labores WHERE id = {$this->table}.idLabor) AS 	nombre_muestra, 
						{$this->table}.id  AS archivoJson_contador,muestras_coords.archivoJson
						FROM muestras_coords
						INNER JOIN {$this->table} ON muestras_coords.id_muestra = {$this->table}.id
						WHERE {$filters[$type]} {$sWhere} ";
			}
			// D($sql);
			$data = $this->db->queryAll($sql);

			foreach ($data as $key => $value) {
				// D($value);
				$markers[$value->archivoJson_contador."".$value->archivoJson] = (object)[
					'lat' => $value->lat,
					'lng' => $value->lng,
					'nombre_muestra' => $value->nombre_muestra,
					'mlat' => $value->lat,
					'mlng' => $value->lng,
				];
			}
		}
		return $markers;
	}
}
