<?php defined('PHRAPI') or die("Direct access not allowed!");

class LaboresAgricolasTendencia {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
        $this->num_plantas_per_json = 10;
        $this->colors = ['#c23531','#2f4554', '#61a0a8', '#d48265', '#91c7ae','#749f83',  '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'];
    }

    public function last(){
        $response = new stdClass;
        $response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM muestras");
        return $response;
    }

    public function index(){
        $response = new stdClass;
        
        $response->calidad_periodo = $this->calidadPeriodo();
        $response->causas_periodo = $this->causasPeriodo();
        return $response;
    }

    public function calidadPeriodo(){
        $response = new stdClass;
        $sWhere = "";
        if(isset($this->postdata->calidadPeriodo)){
            if(isset($this->postdata->calidadPeriodo->finca) && $this->postdata->calidadPeriodo->finca > 0){
                $sWhere .= " AND m.idFinca = {$this->postdata->calidadPeriodo->finca}";
            }
            if(isset($this->postdata->calidadPeriodo->labor) && $this->postdata->calidadPeriodo->labor > 0){
                $sWhere .= " AND m.idLabor = {$this->postdata->calidadPeriodo->labor}";
            }
        }

        $sql = "SELECT j.periodo
                FROM muestras_jsons j
                INNER JOIN muestras_coords c ON id_muestra_json = j.id
                INNER JOIN muestras m ON m.id_muestra_json = j.id
                WHERE 1=1 $sWhere
                GROUP BY j.periodo";
        $response->periodos = $this->db->queryAllOne($sql);

        $sql = "SELECT idFinca, fincas.nombre
                FROM muestras_jsons j
                INNER JOIN muestras m ON id_muestra_json = j.id
                INNER JOIN fincas ON idFinca = fincas.id
                WHERE 1=1 $sWhere
                GROUP BY idFinca";
        $response->fincas = $this->db->queryAll($sql);

        $sql = "SELECT idLabor, labores.nombre
                FROM muestras_jsons j
                INNER JOIN muestras m ON id_muestra_json = j.id
                INNER JOIN labores ON idLabor = labores.id
                WHERE 1=1 $sWhere $sWhereFinca
                GROUP BY idLabor";
        $response->labores = $this->db->queryAll($sql);

        $response->data = [];

        if($this->postdata->calidadPeriodo->mode == 'fincas'){
            foreach($response->{$this->postdata->calidadPeriodo->mode} as $finca){
                $fila = [
                    "idFinca" => $finca->idFinca,
                    "finca" => $finca->nombre
                ];
    
                $fila["children"] = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote, CONCAT('LOTE ', lotes.nombre) AS finca, 'LOTE' as tipo FROM muestras_jsons j INNER JOIN muestras ON id_muestra_json = j.id INNER JOIN lotes ON idLote = lotes.id WHERE lotes.idFinca = $finca->idFinca GROUP BY idLote");
                foreach($fila["children"] as $lote){
                    foreach($response->periodos as $periodo){
                        $calidad = $this->db->queryOne("SELECT
                                ROUND(AVG(100 - (suma_ponderacion / ((SELECT SUM(valor) FROM labores_causas WHERE idLabor = tbl.idLabor)*{$this->num_plantas_per_json}) * 100)), 2)
                            FROM (
                                SELECT idLabor, SUM(valor) suma_ponderacion
                                FROM muestras_jsons j
                                INNER JOIN muestras m ON id_muestra_json = j.id
                                WHERE idLote = {$lote->idLote} 
                                    AND j.periodo = {$periodo}
                                    $sWhere
                                GROUP BY idLabor
                            ) tbl
                        ");
                        $lote->{"periodo_{$periodo}"} = round($calidad, 2);
                        $lote->valores[] = $lote->{"periodo_{$periodo}"};
                    }
    
                    $lote->max = max($lote->valores);
                    $lote->min = min($lote->valores);
                    $lote->avg = round(array_sum($lote->valores)/count($lote->valores), 2);
                }
    
                $values = [];
                foreach($response->periodos as $periodo){
                    $fila["periodo_{$periodo}"] = round(avgOfValue($fila["children"], "periodo_{$periodo}"), 2);
                    $values[] = $fila["periodo_{$periodo}"];
                }
                $fila["max"] = max($values);
                $fila["min"] = min($values);
                $fila["avg"] = round(array_sum($values)/count($values), 2);
    
                $response->data[] = $fila;
            }
        }

        if($this->postdata->calidadPeriodo->mode == 'labores'){
            foreach($response->{$this->postdata->calidadPeriodo->mode} as $labor){
                $fila = [
                    "idFinca" => $labor->idLabor,
                    "finca" => $labor->nombre
                ];
    
                $fila["children"] = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote, CONCAT('LOTE ', lotes.nombre) AS finca, 'LOTE' as tipo FROM muestras_jsons j INNER JOIN muestras ON id_muestra_json = j.id INNER JOIN lotes ON idLote = lotes.id WHERE 1=1 GROUP BY idLote");
                foreach($fila["children"] as $lote){
                    foreach($response->periodos as $periodo){
                        $calidad = $this->db->queryOne("SELECT
                                ROUND(AVG(100 - (suma_ponderacion / ((SELECT SUM(valor) FROM labores_causas WHERE idLabor = tbl.idLabor)*{$this->num_plantas_per_json}) * 100)), 2)
                            FROM (
                                SELECT idLabor, SUM(valor) suma_ponderacion
                                FROM muestras_jsons j
                                INNER JOIN muestras m ON id_muestra_json = j.id
                                WHERE idLote = {$lote->idLote} 
                                    AND idLabor = {$labor->idLabor}
                                    AND j.periodo = {$periodo}
                                    $sWhere
                                GROUP BY idLabor
                            ) tbl
                        ");
                        $lote->{"periodo_{$periodo}"} = round($calidad, 2);
                        $lote->valores[] = $lote->{"periodo_{$periodo}"};
                    }
    
                    $lote->max = max($lote->valores);
                    $lote->min = min($lote->valores);
                    $lote->avg = round(array_sum($lote->valores)/count($lote->valores), 2);
                }
    
                $values = [];
                foreach($response->periodos as $periodo){
                    $fila["periodo_{$periodo}"] = round(avgOfValue($fila["children"], "periodo_{$periodo}"), 2);
                    $values[] = $fila["periodo_{$periodo}"];
                }
                $fila["max"] = max($values);
                $fila["min"] = min($values);
                $fila["avg"] = round(array_sum($values)/count($values), 2);
    
                $response->data[] = $fila;
            }
        }

        return $response;
    }

    public function variablesCausas(){
        $response = new stdClass;
        $sWhere = "";
        $sWhereLabor = "";
        $sWhereFinca = "";
        if(isset($this->postdata->causasPeriodo)){
            if(isset($this->postdata->causasPeriodo->labor) && $this->postdata->causasPeriodo->labor > 0){
                $sWhereLabor .= " AND m.idLabor = {$this->postdata->causasPeriodo->labor}";
            }
            if(isset($this->postdata->causasPeriodo->finca) && $this->postdata->causasPeriodo->labor > 0){
                $sWhereFinca .= " AND j.id_finca = {$this->postdata->causasPeriodo->finca}";
            }
        }

        $sql = "SELECT idFinca, fincas.nombre
                FROM muestras_jsons j
                INNER JOIN muestras m ON id_muestra_json = j.id
                INNER JOIN fincas ON idFinca = fincas.id
                WHERE 1=1 $sWhere $sWhereLabor
                GROUP BY idFinca";
        $response->fincas = $this->db->queryAll($sql);

        $sql = "SELECT idLabor, labores.nombre
                FROM muestras_jsons j
                INNER JOIN muestras m ON id_muestra_json = j.id
                INNER JOIN labores ON idLabor = labores.id
                WHERE 1=1 $sWhere $sWhereFinca
                GROUP BY idLabor";
        $response->labores = $this->db->queryAll($sql);

        return $response;
    }

    public function causasPeriodo(){
        $response = new stdClass;
        $sWhere = "";
        $sWhereLabor = "";
        if(isset($this->postdata->causasPeriodo)){
            if(isset($this->postdata->causasPeriodo->labor) && $this->postdata->causasPeriodo->labor > 0){
                $sWhereLabor .= " AND m.idLabor = {$this->postdata->causasPeriodo->labor}";
            }
            if(isset($this->postdata->causasPeriodo->finca) && $this->postdata->causasPeriodo->labor > 0){
                $sWhere .= " AND j.id_finca = {$this->postdata->causasPeriodo->finca}";
            }
        }

        $sql = "SELECT idLaborCausa, m.idLabor, causas.causa as nombre
                FROM muestras_jsons j
                INNER JOIN muestras m ON id_muestra_json = j.id
                INNER JOIN labores_causas causas ON idLaborCausa = causas.id
                WHERE 1=1 $sWhere $sWhereLabor
                GROUP BY idLaborCausa";
        $response->causas = $this->db->queryAll($sql);

        $sql = "SELECT j.periodo
                FROM muestras_jsons j
                INNER JOIN muestras_coords c ON id_muestra_json = j.id
                INNER JOIN muestras m ON m.id_muestra_json = j.id
                WHERE 1=1 $sWhere $sWhereLabor
                GROUP BY j.periodo";
        $response->periodos = $this->db->queryAllOne($sql);

        $sql = "SELECT idFinca, fincas.nombre
                FROM muestras_jsons j
                INNER JOIN muestras m ON id_muestra_json = j.id
                INNER JOIN fincas ON idFinca = fincas.id
                WHERE 1=1 $sWhere $sWhereLabor
                GROUP BY idFinca";
        $response->fincas = $this->db->queryAll($sql);

        $response->data = [];
        foreach($response->causas as $causa){
            $fila_causa = (object)[
                "idLaborCausa" => $causa->idLaborCausa,
                "causa" => $causa->nombre
            ];
            $fila_causa->detalle = [];

            foreach($response->fincas as $finca){
                $fila = [
                    "idFinca" => $finca->idFinca,
                    "finca" => $finca->nombre
                ];
    
                $fila["children"] = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote, CONCAT('LOTE ', lotes.nombre) AS finca, 'LOTE' as tipo FROM muestras_jsons j INNER JOIN muestras m ON id_muestra_json = j.id INNER JOIN lotes ON idLote = lotes.id WHERE m.idFinca = {$finca->idFinca} $sWhere $sWhereLabor GROUP BY idLote");
                foreach($fila["children"] as $lote){
                    foreach($response->periodos as $periodo){
                        $calidad = $this->db->queryOne("SELECT
                                ROUND(AVG(100 - (suma_ponderacion / ((SELECT SUM(valor) FROM labores_causas WHERE idLabor = tbl.idLabor)*{$this->num_plantas_per_json}) * 100)), 2)
                            FROM (
                                SELECT idLabor, SUM(valor) suma_ponderacion
                                FROM muestras_jsons j
                                INNER JOIN muestras m ON id_muestra_json = j.id
                                WHERE idLote = {$lote->idLote} 
                                    AND idFinca = {$finca->idFinca}
                                    AND j.periodo = {$periodo}
                                    AND idLaborCausa = {$causa->idLaborCausa}
                                    $sWhere $sWhereLabor
                                GROUP BY idLabor
                            ) tbl
                        ");
                        $lote->{"periodo_{$periodo}"} = round($calidad, 2);
                        $lote->valores[] = $lote->{"periodo_{$periodo}"};
                    }
    
                    $lote->max = max($lote->valores);
                    $lote->min = min($lote->valores);
                    $lote->avg = round(array_sum($lote->valores)/count($lote->valores), 2);
                }
    
                $values = [];
                foreach($response->periodos as $periodo){
                    $fila["periodo_{$periodo}"] = round(avgOfValue($fila["children"], "periodo_{$periodo}"), 2);
                    $values[] = $fila["periodo_{$periodo}"];
                }
                $fila["max"] = max($values);
                $fila["min"] = min($values);
                $fila["avg"] = round(array_sum($values)/count($values), 2);
    
                $fila_causa->detalle[] = $fila;
            }

            $fila_causa->valores = [];
            foreach($response->periodos as $periodo){
                $fila_causa->{"periodo_{$periodo}"} = 100 - round(avgOfValue($fila_causa->detalle, 'avg'), 2);
                $fila_causa->valores[] = $fila_causa->{"periodo_{$periodo}"};
            }
            $fila_causa->max = max($fila_causa->valores);
            $fila_causa->min = min($fila_causa->valores);
            $fila_causa->avg = round(array_sum($fila_causa->valores)/count($fila_causa->valores), 2);
            $response->data[] = $fila_causa;
        }
        return $response;
    }
}