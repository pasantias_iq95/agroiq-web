<?php defined('PHRAPI') or die("Direct access not allowed!");

class Lancofruit {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $Factory = new Factory();
        $this->ApiProntoforms = $Factory->ApiProntoforms;
    }

    private function genereteWhere($filters){
        $sWhere = "";
        foreach($filters as $key => $value){
            if(!empty($value)){
                $sWhere .= " AND {$key} LIKE '%{$value}%'";
            }
        }
        return $sWhere;
    }

    public function index(){
        $response = new stdClass;
        $filters = $this->getParams();
        $sWhere = "";
        if($filters->sector != ""){
            $sWhere = " AND sector = '{$filters->sector}'";
        }
        if($filters->ruta != ""){
            $sWhere .= " AND ruta = '{$filters->ruta}'";
        }
        if($filters->status != ""){
            $sWhere .= " AND status = '{$filters->status}'";
        }
        if($filters->tipo != ""){
            $sWhere .= " AND id_tipo_local_categoria = '{$filters->tipo}'";
        }
        if($filters->subtipo != ""){
            $sWhere .= " AND id_tipo_local_subcategoria = '{$filters->subtipo}'";
        }

        $sql = "SELECT  
                    lancofruit.id, 
                    DATE(lancofruit.fecha) as fecha, 
                    IFNULL(lancofruit.direccion, '') as direccion, 
                    IFNULL(lancofruit.status, '') as status, 
                    IFNULL(lancofruit.tipo_local, '') as tipo_local, 
                    IFNULL(lancofruit.nombre_local, '') as nombre_local, 
                    IFNULL(lancofruit.nombre_propietario, '') as nombre_propietario, 
                    IFNULL(lancofruit.ruc, '') as ruc, 
                    IFNULL(lancofruit.referencia, '') as referencia, 
                    lancofruit.foto_lugar, 
                    IFNULL(lancofruit.telefono, '') as telefono, 
                    IFNULL(lancofruit.correo,'') as correo, 
                    IFNULL(lancofruit_rutas.nombre, 'SIN ASIGNAR') as ruta,
                    IFNULL(lancofruit.status,'') AS status , 
                    IFNULL(lancofruit.sector, '') as sector, 
                    IFNULL(lancofruit.tendero,  '') AS tendero, 
                    IFNULL(lancofruit.tipo_local_subcategoria, '') as tipo_local_subcategoria
                FROM lancofruit 
                LEFT JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit.id_ruta
                WHERE 1=1 {$sWhere}";
        $response->table = $this->db->queryAll($sql);
        return $response;
    }

    public function edit(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
			"id" => getValueFrom($postdata , "id" , 0),
		];
        $response->data = [];
        if($data->id > 0){
            $sql = "SELECT id , id AS codigo ,
                        IFNULL(status,'') AS status,
                        IFNULL(TRIM(direccion),'') AS direccion , 
                        IFNULL(nombre_local,'') AS nombre_local ,
                        IFNULL(nombre_propietario,'') AS nombre_propietario ,
                        IFNULL(ruta,'') AS ruta,
                        IFNULL(id_ruta, 0) AS id_ruta,
                        IFNULL(sector,'') AS sector ,
                        IFNULL(TRIM(tipo_local),'') AS tipo_local,
                        IFNULL(TRIM(tipo_local_subcategoria),'') AS tipo_local_subcategoria,
                        IFNULL(TRIM(ruc),'') AS ruc,
                        IFNULL(TRIM(referencia),'') AS referencia,
                        IFNULL(TRIM(telefono),'') AS telefono,
                        IFNULL(TRIM(correo),'') AS correo,
                        IFNULL(TRIM(vendedor), '') AS vendedor,
                        IFNULL(id_vendedor, 0) AS id_vendedor,
                        IFNULL(tendero, '') AS tendero,
                        id_tipo_local_categoria,
                        id_tipo_local_subcategoria
                    FROM lancofruit
                    WHERE id = {$data->id}";
            $response->data = $this->db->queryRow($sql);
            $response->data->ruta = $response->data->ruta;
            $response->data->id = (double)$response->data->id;
            $response->data->codigo = (double)$response->data->codigo;
        }
        return $response;
    }

    public function save(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
			"id" => (int)getValueFrom($postdata , "id" , 0),
			"status" => getValueFrom($postdata , "status" , ""),
			"direccion" => getValueFrom($postdata , "direccion" , ""),
			"nombre_local" => getValueFrom($postdata , "nombre_local" , ""),
			"nombre_propietario" => getValueFrom($postdata , "nombre_propietario" , ""),
			"tipo_local" => getValueFrom($postdata , "tipo_local" , ""),
			"ruc" => getValueFrom($postdata , "ruc" , ""),
			"referencia" => getValueFrom($postdata , "referencia" , ""),
			"telefono" => getValueFrom($postdata , "telefono" , ""),
			"ruta" => getValueFrom($postdata , "ruta" , ""),
			"sector" => getValueFrom($postdata , "sector" , ""),
            "correo" => getValueFrom($postdata , "correo" , ""),
            "tendero" => getValueFrom($postdata , "tendero" , ""),
            "id_tipo_local_categoria" => getValueFrom($postdata , "id_tipo_local_categoria" , ""),
            "id_tipo_local_subcategoria" => getValueFrom($postdata , "id_tipo_local_subcategoria" , ""),
        ];

        /*if(isset($postdata->tipo_local->originalObject)){
            $data->tipo_local = $postdata->tipo_local->originalObject->local;
        }*/
        // D($data);
        $response->data = 0;
        if($data->id > 0){
            $sql = "UPDATE lancofruit SET
                        direccion = '{$data->direccion}',
                        status = '{$data->status}',
                        nombre_local = '{$data->nombre_local}',
                        tipo_local = (SELECT UPPER(nombre) FROM lancofruit_categorias WHERE id = '{$data->id_tipo_local_categoria}'),
                        id_tipo_local_categoria = '{$data->id_tipo_local_categoria}',
                        tipo_local_subcategoria = (SELECT UPPER(nombre) FROM lancofruit_subcategorias WHERE id = '{$data->id_tipo_local_subcategoria}'),
                        id_tipo_local_subcategoria = '{$data->id_tipo_local_subcategoria}',
                        nombre_propietario = '{$data->nombre_propietario}',
                        ruc = '{$data->ruc}',
                        referencia = '{$data->referencia}',
                        telefono = '{$data->telefono}',
                        correo = '{$data->correo}',
                        ruta = '{$data->ruta}',
                        sector = '{$data->sector}',
                        tendero = '{$data->tendero}'
                    WHERE id = $data->id";
                    
            $this->db->query($sql);
            $response->data = $data->id;
        }
        return $response;
    }

    private function getParams(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "sector" => getValueFrom($postdata , "sector", "", FILTER_SANITIZE_STRING),
            "ruta" => getValueFrom($postdata , "ruta", "", FILTER_SANITIZE_STRING),
            "status" => getValueFrom($postdata , "status", "", FILTER_SANITIZE_STRING),
            "tipo" => getValueFrom($postdata , "tipo", "", FILTER_SANITIZE_PHRAPI_INT),
            "subtipo" => getValueFrom($postdata , "subtipo", "", FILTER_SANITIZE_PHRAPI_INT),
            "id_categoria_pastel" => getValueFrom($postdata, 'id_categoria_pastel', 0, FILTER_SANITIZE_PHRAPI_INT)
        ];

        return $filters;
    }

    public function filters(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = $this->getParams();

        $sWhere = "";
        if($filters->sector != ""){
            $sWhere = " AND sector = '{$filters->sector}'";
        }
        if($filters->ruta != ""){
            $sWhere .= " AND ruta = '{$filters->ruta}'";
        }
        if($filters->status != ""){
            $sWhere .= " AND lancofruit.status = '{$filters->status}'";
        }
        if($filters->tipo != ""){
            $sWhere .= " AND id_tipo_local_categoria = '{$filters->tipo}'";
        }
        if($filters->subtipo != ""){
            $sWhere .= " AND id_tipo_local_subcategoria = '{$filters->subtipo}'";
        }
        
        $sql = "SELECT sector AS id , sector AS label FROM lancofruit WHERE sector != '' {$sWhere} GROUP BY sector";
        $response->sectores = $this->db->queryAllSpecial($sql);

        $sql = "SELECT id AS id , nombre AS label FROM lancofruit_rutas";
        $response->rutas = $this->db->queryAll($sql);
        foreach($response->rutas as $ruta){
            $ruta->vendedores = $this->db->queryAll("SELECT id, nombre FROM lancofruit_vendedores WHERE id_ruta = '{$ruta->id}'");
        }

        $sql = "SELECT status AS id, status AS label FROM lancofruit WHERE status != '' {$sWhere} GROUP BY status";
        $response->status = $this->db->queryAllSpecial($sql);

        $sql = "SELECT id_tipo_local_categoria AS id, tipo_local AS nombre FROM lancofruit WHERE tipo_local != '' {$sWhere} GROUP BY tipo_local";
        $response->tipos = $this->db->queryAll($sql);

        $sql = "SELECT sub.id, sub.nombre, id_categoria FROM lancofruit INNER JOIN lancofruit_subcategorias sub ON sub.id = id_tipo_local_subcategoria WHERE 1=1 {$sWhere} GROUP BY sub.id";
        $response->subtipos = $this->db->queryAll($sql);
       
        /*$sql = "SELECT id AS id, nombre AS label FROM lancofruit_vendedores WHERE status > 0";
        $response->vendedores = $this->db->queryAllSpecial($sql);*/

        $sql = "SELECT id, nombre AS label FROM lancofruit_categorias WHERE status = 'Activo'";
        $response->categorias = $this->db->queryAllSpecial($sql);

        $sql = "SELECT id, nombre, id_categoria FROM lancofruit_subcategorias WHERE status = 'Activo' AND id_categoria > 0";
        $response->subcategorias = $this->db->queryAll($sql);
        return $response;
    }

    public function markers(){
        $filters = $this->getParams();
        $sWhere = "";
        if($filters->sector != ""){
            $sWhere = " AND sector = '{$filters->sector}'";
        }
        if($filters->ruta != ""){
            $sWhere .= " AND ruta = '{$filters->ruta}'";
        }
        if($filters->status != ""){
            $sWhere .= " AND status = '{$filters->status}'";
        }
        if($filters->tipo != ""){
            $sWhere .= " AND id_tipo_local_categoria = '{$filters->tipo}'";
        }
        if($filters->subtipo != ""){
            $sWhere .= " AND id_tipo_local_subcategoria = '{$filters->subtipo}'";
        }
        $sql = "SELECT id , geolocalizacion , 
                IFNULL(TRIM(nombre_propietario), '') AS propietario,
                IFNULL(status,'') AS status,
                IFNULL(TRIM(direccion),'') AS direccion , 
                IFNULL(nombre_local,'') AS cliente ,
                IFNULL(TRIM(tipo_local),'') AS tipo_local,
                IFNULL(TRIM(ruc),'') AS ruc,
                IFNULL(TRIM(referencia),'') AS referencia,
                IFNULL(TRIM(telefono),'') AS telefono,
                IFNULL(TRIM(correo),'') AS correo,
                IFNULL(TRIM(sector),'') AS sector,
                IF(id <= 74 ,CONCAT('./lancofruit/',id,'.jpg'),foto_lugar) AS foto_lugar
                FROM lancofruit
                WHERE 1 = 1 {$sWhere}
                ORDER BY nombre_local";
        $response = new stdClass;
        $response->data = $this->db->queryAll($sql);
        return $response;
    }

    public function pasteles(){
        $response = new stdClass;
        $filters = $this->getParams();
        $sWhere = "";
        if($filters->sector != ""){
            $sWhere = " AND sector = '{$filters->sector}'";
        }
        if($filters->ruta != ""){
            $sWhere .= " AND ruta = '{$filters->ruta}'";
        }
        if($filters->status != ""){
            $sWhere .= " AND status = '{$filters->status}'";
        }
        if($filters->tipo != ""){
            $sWhere .= " AND id_tipo_local_categoria = '{$filters->tipo}'";
        }
        if($filters->subtipo != ""){
            $sWhere .= " AND id_tipo_local_subcategoria = '{$filters->subtipo}'";
        }

        $sql = "SELECT status as label, COUNT(1) as value 
                FROM lancofruit
                WHERE TRIM(status) != '' {$sWhere}
                GROUP BY TRIM(status)";
        $response->pie_status = $this->pie($this->db->queryAll($sql), "50%", "Status");

        $sql = "SELECT cat.id, cat.nombre
                FROM lancofruit_categorias cat
                INNER JOIN lancofruit ON id_tipo_local_categoria = cat.id
                WHERE 1=1 {$sWhere}
                GROUP BY cat.id";
        $response->categorias = $this->db->queryAll($sql);

        foreach($response->categorias as $cat){
            $sql = "SELECT tipo_local_subcategoria as label, COUNT(1) as value 
                    FROM lancofruit
                    WHERE TRIM(tipo_local_subcategoria) != '' {$sWhere} AND id_tipo_local_categoria = {$cat->id}
                    GROUP BY TRIM(tipo_local_subcategoria)";
            $response->pie_tipo[$cat->id] = $this->pie($this->db->queryAll($sql), "50%", "");
        }

        return $response;
    }

    public function asignarVendedor(){
        $response = new stdClass;
        $response->status = 400;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "id" => getValueFrom($postdata , "id", 0, FILTER_SANITIZE_PHRAPI_INT),
            "id_ruta" => getValueFrom($postdata , "id_ruta", 0, FILTER_SANITIZE_PHRAPI_INT),
            "id_vendedor" => getValueFrom($postdata , "id_vendedor", 0, FILTER_SANITIZE_PHRAPI_INT),
        ];

        if($data->id > 0){
            if($data->id_ruta > 0 && $data->id_vendedor > 0){
                $response->status = 200;
                $this->db->query("UPDATE lancofruit
                    SET
                        id_ruta = '{$data->id_ruta}',
                        ruta = (SELECT nombre FROM lancofruit_rutas WHERE id = '{$data->id_ruta}'),
                        id_vendedor = '{$data->id_vendedor}',
                        vendedor = (SELECT nombre FROM lancofruit_vendedores WHERE id = '{$data->id_vendedor}')
                    WHERE id = '{$data->id}'");
            }else{
                $response->message = "Seleccione Ruta y Vendedor";
            }
        }else{
            $response->message = "Seleccionar Tienda";
        }

        return $response;
    }

    public function getConfiguracion(){
        $response = new stdClass;
        $response->status = 200;
        $response->rutas = $this->db->queryAll("SELECT id, nombre FROM lancofruit_rutas");
        $response->no_asignados = $this->db->queryAll("SELECT id, nombre FROM lancofruit_vendedores WHERE id_ruta = 0");
        foreach($response->rutas as $ruta){
            $ruta->vendedores = $this->db->queryAll("SELECT vendedores.id, vendedores.nombre 
                FROM lancofruit_vendedores vendedores 
                INNER JOIN lancofruit_rutas rutas ON id_ruta = rutas.id
                WHERE rutas.id = '{$ruta->id}'");
        }
        return $response;
    }
    public function addRuta(){
        $response = new stdClass;
        $response->status = 400;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "ruta" => getValueFrom($postdata , "name", 0, FILTER_SANITIZE_PHRAPI_INT),
        ];

        if($data->ruta > 0){
            $exist = $this->db->queryAll("SELECT * FROM lancofruit_rutas WHERE nombre = '{$data->ruta}'");
            if(count($exist) > 0){
                $response->message = "Ruta ya existe";
            }else{
                $this->db->query("INSERT INTO lancofruit_rutas SET nombre = '{$data->ruta}'");
                $this->ApiProntoforms->actualizarLancofruit();
                $response = $this->getConfiguracion();
            }
        }
        return $response;
    }
    public function addVendedor(){
        $response = new stdClass;
        $response->status = 400;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "name" => getValueFrom($postdata , "name", "", FILTER_SANITIZE_PHRAPI_STRING),
        ];
        if($data->name != ""){
            $exist = $this->db->queryAll("SELECT * FROM lancofruit_vendedores WHERE nombre = '{$data->name}'");
            if(count($exist) > 0){
                $response->message = "Vendedor ya existe";
            }else{
                $this->db->query("INSERT INTO lancofruit_vendedores SET nombre = '{$data->name}'");
                $this->ApiProntoforms->actualizarLancofruit();
                $response = $this->getConfiguracion();
            }
        }else{
            $response->message = "Escriba el nombre del Vendedor";
        }
        return $response;
    }
    public function saveConfig(){
        $response = new stdClass;
        $response->status = 400;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "vendedores" => getValueFrom($postdata , "vendedores", [], FILTER_SANITIZE_PHRAPI_ARRAY),
            "ruta" => getValueFrom($postdata , "ruta", 0, FILTER_SANITIZE_PHRAPI_INT),
        ];
        
        if($data->ruta > 0){
            $this->db->query("UPDATE lancofruit_vendedores SET id_ruta = 0 WHERE id_ruta = '{$data->ruta}'");
            foreach($data->vendedores as $vendedor){
                $this->db->query("UPDATE lancofruit_vendedores SET id_ruta = '{$data->ruta}' WHERE id = '{$vendedor}'");
            }
            $this->ApiProntoforms->actualizarLancofruit();
            $response = $this->getConfiguracion();
        }else{
            $response->message = "Seleccione Ruta";
        }
        return $response;
    }

    private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}