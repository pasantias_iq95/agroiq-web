<?php defined('PHRAPI') or die("Direct access not allowed!");

class Membresias {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function index(){
        $sWhere = "";
        $sOrder = " ORDER BY nombre";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sWhere = "WHERE 1 = 1 ";
        if($this->session->id_company > 1){
           $sWhere = "WHERE id_company = '{$this->session->id_company}'";
        }
        // print_r($_POST);
        if(isset($_POST)){

            /*----------  ORDER BY ----------*/
            
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY users.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY users.fecha {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY users.nombre {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY users.usuario {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY users.status {$DesAsc}";
            }
            /*----------  ORDER BY ----------*/

            if(isset($_POST['order_id']) && trim($_POST['order_id']) != ""){
                $sWhere .= " AND users.id = ".$_POST["order_id"];
            }               
            if((isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != "") && (isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != "")){
                $sWhere .= " AND users.fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }
            if(isset($_POST['order_customer_name']) && trim($_POST['order_customer_name']) != ""){
                $sWhere .= " AND users.nombre LIKE '%".$_POST['order_customer_name']."%'";
            }
            if(isset($_POST['order_ciudad']) && trim($_POST['order_ciudad']) != ""){
                $sWhere .= " AND users.usuario LIKE '%".$_POST['order_ciudad']."%'";
            }
            if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
                $sWhere .= " AND users.status = ".$_POST["order_status"];
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

		$sql = "SELECT  * , (SELECT COUNT(*) FROM users WHERE id_company = 1) AS totalRows FROM users 
		$sWhere 
        $sOrder $sLimit";
        // D($sql);
		$organizadores = $this->maindb->queryAll($sql);
        $datos = (object)[];
        $datos->data = [];
        foreach ($organizadores as $key => $fila) {
            $fila = (object)$fila;
            $datos->data[] = array (
               $fila->id,
                $fila->id,
                $fila->fecha,
                strtoupper($fila->nombre),
                $fila->usuario,
                '<button class="btn btn-sm '.(($fila->status==1)?'green-jungle':'red').'" id="status">'.(($fila->status==1)?'ACTIVO':'INACTIVO').'</button>',
                '<button id="edit" data-action="/organizacion?id='.$fila->id.'" class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-plus"></i> Editar</button>'
            );
            $datos->recordsTotal = $fila->totalRows;
        }

        $datos->recordsFiltered = count($datos->data);
        #$datos->customActionMessage = "Informacion completada con exito";
        $datos->customActionStatus = "OK";

        return $datos;
	}

	public function edit(){
		$response = new stdClass;

		$response = $this->privileges();

		return $response;

	}

	private function privileges(){
		$response = new stdClass;
		$response->data = [];
		$not_views = ['id' ,'id_user'];
		$sql = "SHOW COLUMNS FROM users_privileges";
		$privilegios = [];
		$privilegios = $this->maindb->queryAll($sql);
		// D($sql);
		foreach ($privilegios as $key => $value) {
			if(!in_array(trim($value->Field) , $not_views)){
				$response->data['Privilegios'][trim($value->Field)] = [
					"description" => trim($value->Field),
					"value" => $this->getDataforEnum($value->Type)
				];
			}
		}
		return $response;
	}

	private function getDataforEnum($data){
		$matches = "";
		$enum = [];
		if($data != ""){
			preg_match("/^enum\(\'(.*)\'\)$/", $data, $matches);
			$enum = explode("','", $matches[1]);
		}
		return $enum;
	}
}