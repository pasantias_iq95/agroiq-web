<?php defined('PHRAPI') or die("Direct access not allowed!");

class Merma {
	public $name;
	private $db;
	private $MermaTypePeso;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);

        $this->search = "Total Merma";
        if($this->session->id_company == 2){
        	$this->search = "Total Daños";
        }

        $this->MermaTypePeso = "Kg";
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}

		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}

		if(isset($postdata->idFincaDia)){
			$id_fincaDia = $postdata->idFincaDia;
		}

		if(isset($postdata->statusLbKg) && $postdata->statusLbKg == 1){
			$this->MermaTypePeso = "Kg";
		}else{
			$this->MermaTypePeso = "Lb";
		}

		$response->id_company = (int)$this->session->id_company;

		$response->tags = $this->tags($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		$response->tabla_lote_merma = $this->tabla_lote_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		// Cargamos la data una sola ves 
		$danos = $this->tabla_danos_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		// Cargamos la data del historico una sola ves 
		$historico = $this->grafica_historico($palanca , $id_finca);
		// Cargamos la data de tendencia una sola ves 
		$tendencia = $this->tendencia($palanca , $id_finca , $postdata);
		// Asignamos la data 'Tabla'
		$response->tabla_danos_merma = $danos->data;
		// Asignamos la data 'Tabla' Daños
		$response->tabla_danos_merma_danhos_merma = $danos->danhos_merma;
		// Asignamos la data 'Grafica de daños'
		$response->danos = $danos->grafica;
		// Asignamos la data 'Grafica del detalle de daños'
		$response->danos_detalle = $danos->grafica_detalle;
		// Asignamos la data 'Grafica del historico'
		$response->historico = $historico->data;
		// Asignamos la data 'Grafica del dia'
		$response->dia = $this->grafica_dia($palanca , $id_finca , $id_fincaDia , $postdata)->data;
		// Agregamos el titulo de la 'Grafica del dia'
		$response->dia_title = $this->grafica_dia($palanca , $id_finca , $id_fincaDia , $postdata)->title;
		// Agregamos el promedio del historico
		$response->historico_avg = $historico->avg;
		$response->historico_legends = $historico->legend;

		// Asignamos la data 'Grafica del historico' y Legend
		$response->tendencia = $tendencia->series;
		$response->tendencia_legends = $tendencia->legend;

		$response->palanca = ["" => "Todos"] + $this->getPalanca($postdata);
		$response->fincas = [];
		if($this->session->id_company == 7 || $this->session->id_company == 6){
			$response->fincas = ["" => "Todas"] + $this->getFincas($postdata);
		}

		return $response;
	}

	private function getPalanca($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";
		if(isset($filters->idFinca) && $filters->idFinca != ""){
			// $sWhere .= " AND id_finca = {$filters->idFinca}";
			if($filters->idFinca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$filters->idFinca}";
			}
		}
		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT palanca AS id , palanca AS label  FROM merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND palanca != '' $sWhere {$typeMerma}
				GROUP BY TRIM(palanca)";
		// D($sql);
		$response->data = [];
		$response->data = $this->db->queryAllSpecial($sql);
		return $response->data;
	}	

	private function getFincas($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		
		$response = new stdClass;
		$sql = "SELECT id_finca AS id , finca AS label  FROM merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND finca != ''
				GROUP BY id_finca";
		$response->data = [];
		if($this->session->id_company == 7){
			$response->data = ["9999999" => "San José"] + $this->db->queryAllSpecial($sql);
		}else{
			$response->data = $this->db->queryAllSpecial($sql);
		}
		return $response->data;
	}

	private function tags($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$sql = "SELECT IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS merma  FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
		$rp1 = $this->db->queryRow($sql);
		
		$peso_neto = "peso_neto";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
			}else{
				$cantidad = "(cantidad)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 7){
			$peso_neto = "(racimo - tallo)";
			$cantidad = "(cantidad)";
		}else{
			$cantidad = "(cantidad)";
		}

		$total_merma = 0;
		$data = "";
		if($this->session->id_company == 2 || $this->session->id_company == 8){
			// $sql = "SELECT AVG(porcentaje_enfunde) AS enfunde FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
			// $rp2 = $this->db->queryRow($sql);
			// $sql = "SELECT AVG(porcentaje_cosecha) AS cosecha FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
			// $rp4 = $this->db->queryRow($sql);

			// $sql = "SELECT AVG(porcentaje_campo) AS campo FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
			// $rp3 = $this->db->queryRow($sql);
			// $sql = "SELECT AVG(porcentaje_animales) AS animales FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
			// $rp5 = $this->db->queryRow($sql);
			// $sql = "SELECT AVG(porcentaje_hongos) AS hongos FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
			// $rp6 = $this->db->queryRow($sql);
			// $sql = "SELECT AVG(porcentaje_empacadora) AS empacadora FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
			// $rp7 = $this->db->queryRow($sql);
			// $sql = "SELECT AVG(porcentaje_fisiologicos) AS fisiologicos FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
			// $rp8 = $this->db->queryRow($sql);
			

			$sql ="SELECT LOWER(type) AS id,ROUND(AVG(Total),2) AS label FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , peso_neto , (({$cantidad}) / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) BETWEEN '{$inicio}' AND '{$fin}' AND campo like '%Total Peso%' {$sWhere}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle
				GROUP BY type";
			$data = $this->db->queryAllSpecial($sql);
			if(is_array($data)){
				$total_merma = array_sum($data);
				$data = (object)$data;
			}


			foreach ($data as $key => $value) {
				$data->{$key} = round((($data->{$key} * 100) / $total_merma), 2);
			}

			$tags = [];
			$tags = array(
				'merma' 		=> $total_merma,
				'enfunde' 		=> $data->enfunde,
				'campo' 		=> $data->campo,
				'cosecha' 		=> $data->cosecha,
				'animales' 		=> $data->animales,
				'hongos' 		=> $data->hongos,
				'empacadora' 	=> $data->empacadora,
				'fisiologicos' 	=> $data->fisiologicos
			);
		}
		if($this->session->id_company == 6){
			
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";

			$sql = "SELECT AVG(porcentaje_enfunde) AS enfunde FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}";
			// D($sql);
			$rp2 = $this->db->queryRow($sql);
			$sql = "SELECT AVG(porcentaje_cosecha) AS cosecha FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}";
			$rp4 = $this->db->queryRow($sql);

			$sql = "SELECT AVG(porcentaje_adm) AS adm FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}";
			$rp3 = $this->db->queryRow($sql);
			$sql = "SELECT AVG(porcentaje_natural) AS naturals FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}";
			// D($sql);
			$rp5 = $this->db->queryRow($sql);
			$sql = "SELECT AVG(porcentaje_proceso) AS proceso FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}";
			$rp6 = $this->db->queryRow($sql);

			$sql = "SELECT AVG(porcentaje_merma_procesada) AS merma_procesada FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}";
			$rp7 = $this->db->queryRow($sql);

			$tags = [];
			$tags = array(
				'merma' 		=> $rp1->merma,
				'merma_procesada' 	=> $rp7->merma_procesada,
				'enfunde' 		=> $rp2->enfunde,
				'adm' 			=> $rp3->adm,
				'cosecha' 		=> $rp4->cosecha,
				'natural' 		=> $rp5->naturals,
				'proceso' 		=> $rp6->proceso,
			);
		}

		if($this->session->id_company == 4){
			
			$sql = "SELECT AVG(porcentaje_merma) AS merma_neta ,AVG(porcentaje_merma_procesada) AS merma_procesada 
				 FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
			$rp1 = $this->db->queryRow($sql);

			$sql2 = "SELECT AVG((SELECT (cantidad)
					FROM merma_detalle
					WHERE merma_detalle.id_merma = merma.id
					AND campo = '% MERMA CORTADA')) AS merma_cortada,
					AVG((SELECT (cantidad)
					FROM merma_detalle
					WHERE merma_detalle.id_merma = merma.id
					AND campo = '% TALLO')) AS tallo
					FROM merma
					WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
			$rp2 = $this->db->queryRow($sql2);

			$sql ="SELECT LOWER(type) AS id,ROUND(AVG(Total),2) AS label FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , {$peso_neto} , (({$cantidad}) / {$peso_neto}) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) BETWEEN '{$inicio}' AND '{$fin}' AND campo like '%Total Peso%' {$sWhere}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle
				GROUP BY type";
			// D($sql);
			$data = $this->db->queryAllSpecial($sql);
			if(is_array($data)){
				$total_merma = array_sum($data);
				$data = (object)$data;
			}


			foreach ($data as $key => $value) {
				$data->{$key} = round((($data->{$key} * 100) / $total_merma), 2);
			}

			$tags = [];
			$tags = array(
				'merma' 		  => $rp1->merma_neta,
				'merma_procesada' => $rp1->merma_procesada,
				'merma_cortada'	  => $rp2->merma_cortada,
				'tallo'			  => $rp2->tallo,
				'empacadora' 	  => $data->empacadora,
				'virus'  		  => $data->virus,
				'cosecha' 		  => $data->cosecha,
				'hongos' 		  => $data->hongos,
				'viejos' 		  => $data->{'daños viejos'},
				'fisiologicos' 	  => $data->fisiologicos,
				'insectos' 	  	  => $data->insectos,
				'animal' 	  	  => $data->animal,
				'bacteria' 	  	  => $data->bacteria,
			);
		}
		if($this->session->id_company == 7){
			
			$sql = "SELECT AVG(porcentaje_merma) AS merma_neta ,AVG(porcentaje_merma_procesada) AS merma_procesada 
				 FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
			$rp1 = $this->db->queryRow($sql);

			$sql2 = "SELECT AVG((SELECT (cantidad)
					FROM merma_detalle
					WHERE merma_detalle.id_merma = merma.id
					AND campo = '% MERMA CORTADA')) AS merma_cortada,
					AVG((SELECT (cantidad)
					FROM merma_detalle
					WHERE merma_detalle.id_merma = merma.id
					AND campo = '% TALLO')) AS tallo
					FROM merma
					WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' $sWhere";
			$rp2 = $this->db->queryRow($sql2);

			$sql ="SELECT LOWER(type) AS id,ROUND(AVG(Total),2) AS label FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , {$peso_neto} , (({$cantidad}) / {$peso_neto}) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) BETWEEN '{$inicio}' AND '{$fin}' AND campo like '%Total Peso%' {$sWhere}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle
				GROUP BY type";
			// D($sql);
			$data = $this->db->queryAllSpecial($sql);
			if(is_array($data)){
				$total_merma = array_sum($data);
				$data = (object)$data;
			}


			foreach ($data as $key => $value) {
				$data->{$key} = round((($data->{$key} * 100) / $total_merma), 2);
			}

			$tags = [];
			$tags = array(
				'merma' 		  => $rp1->merma_neta,
				'merma_procesada' => $rp1->merma_procesada,
				'merma_cortada'	  => $rp2->merma_cortada,
				'tallo'			  => $rp2->tallo,
				'empaque' 		  => $data->empaque,
				'administracion'  => $data->administracion,
				'cosecha' 		  => $data->cosecha,
				'deshoje' 		  => $data->deshoje,
				'lotero' 		  => $data->{'lotero aereo'},
				'fisiologicos' 	  => $data->fisiologicos
			);
		}


		return $tags;
	}

	private function tabla_lote_merma($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$data = [];
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "AVG(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "AVG(total_peso_merma)";
			}
			$sql = "SELECT id , bloque AS lote ,{$total_peso_merma} AS total_peso_merma , AVG(total_defectos) AS total_defectos , IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS merma  FROM merma
			WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere GROUP BY bloque";
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
			$total_peso_merma = "total_peso_merma";
			if($this->session->id_company == 7){
				$total_peso_merma = "(total_peso_merma-tallo) / racimos_procesados";
			}

			$typeMerma = "";
			if($this->session->id_company == 6){
				$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
			}

			$sql = "SELECT id , bloque AS lote ,AVG({$total_peso_merma}) AS total_peso_merma , AVG(total_defectos) AS total_defectos , IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS merma  FROM merma
			WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere {$typeMerma} GROUP BY bloque";
		}
		// D($sql);
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->detalle = $this->tabla_lote_merma_detalle($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
			$value->detalle_danhos = $this->getDanhos($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
		}
		//D($res);
		return $res;
	}

	private function getDanhos($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label FROM (
				SELECT merma_detalle.id,merma.id AS id_m , fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE bloque = {$bloque} AND DATE(fecha) BETWEEN '$inicio' AND '$fin' AND campo like '%".$this->search."%'  {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
				GROUP BY type";
		// D($sql);
		$res = $this->db->queryAllSpecial($sql);

		return $res;
	}

	private function tabla_lote_merma_detalle($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "", $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$data = [];
		$peso_neto = "peso_neto";
		$racimos_procesados = "";
		$cantidad_racimos = "(cantidad)";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$$cantidad_racimos = "(cantidad / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$$cantidad_racimos = "(cantidad)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad)";
			$cantidad_racimos = "(cantidad / racimos_procesados)";
			$peso_neto = "(racimo - tallo)";
			$racimos_procesados = "racimos_procesados,";
		}else{
				$cantidad = "(cantidad)";
				$$cantidad_racimos = "(cantidad)";
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$sql = "SELECT type , bloque ,AVG((cantidad)) AS danhos_peso, AVG(Total) AS cantidad , AVG(porcentaje_merma) FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , 
				{$racimos_procesados} type ,campo ,{$cantidad_racimos} AS cantidad , {$peso_neto} , ({$cantidad} / {$peso_neto}) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE bloque = {$bloque} AND DATE(fecha) BETWEEN '$inicio' AND '$fin' AND campo like '%Total Peso%' {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
				GROUP BY type";
		// D($sql);
		$res = $this->db->queryAll($sql);

		return $res;
	}

	private function grafica_historico($palanca = "", $id_finca = ""){
		$sWhere = "";
		$sWhere_PromFincas = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}


		$Umbral = 2;

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}

		$sql = "SELECT TRIM(finca) AS finca ,id_finca, WEEK(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere
				GROUP BY id_finca , WEEK(fecha)
				ORDER BY id_finca , WEEK(fecha)";


		// $typeMerma = "";
		// if($this->session->id_company == 6){
		// 	$Umbral = 10;
		// 	$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";

		// 	$sql = "SELECT TRIM(tipo_merma) AS finca ,tipo_merma AS id_finca, WEEK(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
		// 		FROM merma
		// 		WHERE porcentaje_merma > 0 $sWhere
		// 		GROUP BY tipo_merma , WEEK(fecha)
		// 		ORDER BY tipo_merma , WEEK(fecha)";
		// }

		if($this->session->id_company == 7 || $this->session->id_company == 6){
			$Umbral = 10;
			if((int)$id_finca > 1 && $palanca != ""){
				$sWhere_PromFincas = " AND palanca = '{$palanca}'";
			}elseif((int)$id_finca == 1){
				$sWhere_PromFincas = $sWhere;
			}
			// D($sWhere_PromFincas);
			$sql = "(SELECT TRIM(finca) AS finca ,id_finca, WEEK(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere
				GROUP BY id_finca , WEEK(fecha)
				ORDER BY id_finca , WEEK(fecha))
				UNION ALL
				(SELECT 'San José' AS finca, '9999999' AS id_finca, fecha , AVG(porcentaje_merma) ,total_peso_merma FROM (
				SELECT TRIM(finca) AS finca ,id_finca, WEEK(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
								FROM merma
								WHERE porcentaje_merma > 0 AND finca LIKE '%San%' $sWhere_PromFincas
								GROUP BY id_finca , WEEK(fecha)
								ORDER BY id_finca , WEEK(fecha)) as detalle
				GROUP BY (fecha)
				ORDER BY (fecha))";
			// D($sql);
		}

		if($this->session->id_company == 4){
			$Umbral = 5;
		}

		$response = new stdClass;
		// D($sql);
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->porcentaje_merma = (float)$value->porcentaje_merma;
			$sum += $value->porcentaje_merma;
			$response->legend[(int)$value->fecha] = (int)$value->fecha;
			if($finca != $value->id_finca){
				$series = new stdClass;
				$finca = $value->id_finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->data = [];
				$series->data[] = round($value->porcentaje_merma ,2);
				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series->markLine = new stdClass;
					$series->markLine->data = [];
					$markLine->name = "Umbral";
					$markLine->value = $Umbral;
					$markLine->xAxis = -1;
					$markLine->yAxis = $Umbral;
					$series->markLine->data[0][] = $markLine;
					
					$markLine = new stdClass;
					$markLine->xAxis = 52;
					$markLine->yAxis = $Umbral;

					$series->markLine->data[0][] = $markLine;
					$flag_count++;
				}
				
				$response->data[$value->id_finca] = $series;
			}else{
				if($finca == $value->id_finca){
					// D($response->data[$value->id_finca]);
					$response->data[$value->id_finca]->data[] = round($value->porcentaje_merma ,2);
				}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}

	private function historico_general($palanca = "" , $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$Umbral = 10;
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT TRIM(finca) AS finca ,id_finca, WEEK(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere {$typeMerma}
				GROUP BY WEEK(fecha)";
		$res = $this->db->queryAll($sql);
	}

	private function grafica_dia($palanca = "", $id_finca = ""  , $id_fincaDia = "" , $filters = []){
		$sWhere = "";
		$fincaDia = 0;
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		if($id_fincaDia != ""){
			$fincaDia = $id_fincaDia;
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}


		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT id , id_finca,TIME_FORMAT(TIME(fecha) , '%H:%i') AS time ,DATE(fecha) as fecha ,IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma,
				CONCAT_WS(' ','Última Actualización : ' , DATE(fecha)) as title FROM merma
				WHERE 1 = 1 $sWhere {$typeMerma}
				GROUP BY id
				ORDER BY fecha desc";
		// D($sql);
		$res = $this->db->queryAll($sql);
		$count = 1;
		$fecha = "";
		$flag = 0;
		$response->data = new stdClass;
		$response->data->data = [];
		$response->data->label = [];
		$response->data->series = [];
		$response->title = "";
		$label = "";
		foreach ($res as $key => $value) {
			$value = (object)$value;
			if($value->fecha != $fecha){
				$fecha = $value->fecha;
				$flag++;
				if($flag == 2){
					break;
				}
			}

			if($fincaDia > 0){
				if($value->id_finca == $fincaDia){
					$response->title = $value->title;
					$value->porcentaje_merma = (float)$value->porcentaje_merma;
					if($value->time == '00:00'){
						$label = "Hora ".$count;
					}else{
						$label = $value->time;
					}
					$response->data->label[$value->fecha." ".$label] = $label;
					$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
					$response->data->data[$label] = $value;
					$count++;
				}
			}else{
				$response->title = $value->title;
				$value->porcentaje_merma = (float)$value->porcentaje_merma;
				if($value->time == '00:00'){
					$label = "Hora ".$count;
				}else{
					$label = $value->time;
				}
				$response->data->label[$value->fecha." ".$label] = $label;
				$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
				$response->data->data[$label] = $value;
				$count++;
			}
		}
		return $response; 
	}

	private function tabla_danos_merma($inicio, $fin , $palanca , $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$response = new stdClass;
		$peso_neto = "peso_neto";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
			}else{
				$cantidad = "(cantidad)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad)";
			$peso_neto = "(racimo - tallo)";
		}else{
				$cantidad = "(cantidad)";
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$sql ="SELECT type , bloque ,AVG(cantidad) AS danhos_peso, ROUND(AVG(Total),2) AS cantidad , AVG(porcentaje_merma) FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , {$peso_neto} , ({$cantidad} / {$peso_neto}) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND campo like '%Total Peso%' {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
				GROUP BY type";
		// D($sql);
		// $sql = "SELECT type , bloque , AVG(Total) AS cantidad , AVG(porcentaje_merma) FROM (
		// 		SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
		// 		INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
		// 		WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND campo like '%Total Peso%' 
		// 		AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') ) as detalle
		// 		GROUP BY type";
		// D($sql);
		// D($sql_2);
		$response->data = $this->db->queryAll($sql);
		$response->grafica = $this->pie($response->data , '50%' ,"Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		// D($response);
		$response->grafica_detalle = [];
		$detalle = "";
		foreach ($response->data as $key => $value) {
			$value = (object)$value;
			$detalle = $this->tabla_danos_merma_detalle($inicio, $fin , $value->type, $palanca, $id_finca , $filters);
			$value->detalle = $detalle->data;
			$value->detalle_danhos = $detalle->danhos_merma;
			$response->grafica_detalle[$value->type] = $detalle->grafica;
		}
		$response->danhos_merma = $this->getDanhosMerma($inicio, $fin , $palanca , $id_finca , $filters);
		return $response;
	}

	private function getDanhosMerma($inicio ,$fin  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$sWhereConsulta = "AND campo like '%".$this->search."%'";
		if($this->session->id_company == 4){
			$sWhereConsulta = "AND flag = 1";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label FROM (
				SELECT merma_detalle.id,merma.id AS id_m , fecha  ,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' {$sWhereConsulta} {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
				GROUP BY type";
		// D($sql);
		$res = $this->db->queryAllSpecial($sql);

		return $res;
	}

	private function tabla_danos_merma_detalle($inicio, $fin , $type , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$peso_neto = "peso_neto";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
			}else{
				$cantidad = "(cantidad)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad)";
			$peso_neto = "(racimo - tallo)";
		}else{
				$peso_neto = "peso_neto";
				$cantidad = "(cantidad)";
		}


		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT campo AS type, campo ,AVG(cantidad) AS danhos_peso, AVG(Total) AS cantidad , AVG(porcentaje_merma) FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,REPLACE(campo,'Peso ' , '') AS campo , {$cantidad} AS cantidad , {$peso_neto} , ({$cantidad} / {$peso_neto}) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND type = '{$type}' AND flag = 1 {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere) as detalle
				GROUP BY type ,campo";
		// D($sql);
		$response->data = $this->db->queryAll($sql);
		// foreach ($response->data as $key => $value) {
		// 	$value = (object)$value;
		// }
		$response->danhos_merma = $this->getDanhosMermaDetalle($inicio, $fin , $type , $palanca , $id_finca , $filters);
		$response->grafica = $this->pie($response->data , '50%' ,"Detalle de Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		return $response;
	}

	private function getDanhosMermaDetalle($inicio ,$fin , $type  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$sql = "SELECT campo AS id ,AVG(cantidad) AS label FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND type = '{$type}' AND flag = 2 {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere) as detalle
				GROUP BY type ,campo";
		// D($sql);
		$res = $this->db->queryAllSpecial($sql);

		return $res;
	}

	private function tendencia($palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$Umbral = 1;
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$Umbral = 50;

			$sql = "SELECT WEEK(fecha) AS semana , type AS labor,ROUND(({$cantidad} / {$total_peso_merma}) * 100,2) AS cantidad FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , porcentaje_merma , total_peso_merma FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE campo like '%Total Peso%' {$sWhere}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle
				GROUP BY type , WEEK(fecha) 
				ORDER BY type , WEEK(fecha)";

		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad)";
			$total_peso_merma = $this->db->queryAllSpecial("
				SELECT semana AS id , SUM(total_peso_merma) AS label FROM (
				SELECT fecha ,semana , dia , type ,SUM(cantidad) AS total_peso_merma FROM (				
					SELECT merma_detalle.id_merma ,fecha , WEEK(fecha) as semana ,DAY(fecha) as dia , type , campo,cantidad FROM merma 
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE campo like '%Total Peso%' AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') {$sWhere}
					) AS detalle
				GROUP BY type , dia) AS detalle
				GROUP BY semana");
				$Umbral = 50;
				// D($total_peso_merma);

			$sql = "SELECT semana , type AS labor, SUM(total_peso_merma) AS cantidad FROM (
					SELECT fecha ,semana , dia , type ,SUM(cantidad) AS total_peso_merma FROM (				
						SELECT merma_detalle.id_merma ,fecha , WEEK(fecha) as semana ,DAY(fecha) as dia , type , campo,cantidad FROM merma 
						INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
						WHERE campo like '%Total Peso%' AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') {$sWhere}
						) AS detalle
					GROUP BY type , dia) AS detalle
					GROUP BY type , semana";
			
		}else{
				$cantidad = "(cantidad)";
				$total_peso_merma = "(total_peso_merma)";
				$Umbral = 50;

				$typeMerma = "";
				if($this->session->id_company == 6){
					$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
				}

				$sql = "SELECT WEEK(fecha) AS semana , type AS labor,ROUND(({$cantidad} / {$total_peso_merma}) * 100,2) AS cantidad FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , porcentaje_merma , total_peso_merma FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE campo like '%Total Peso%' {$sWhere} {$typeMerma} 
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle
				GROUP BY type , WEEK(fecha) 
				ORDER BY type , WEEK(fecha)";
		}

		$data = [];

		// $sql = "SELECT WEEK(fecha) AS semana , type AS labor,ROUND(({$cantidad} / {$total_peso_merma}) * 100,2) AS cantidad FROM (
		// 		SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , porcentaje_merma , total_peso_merma FROM merma
		// 		INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
		// 		WHERE campo like '%Total Peso%' {$sWhere}
		// 		AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle
		// 		GROUP BY type , WEEK(fecha) 
		// 		ORDER BY type , WEEK(fecha)";
		// D($sql);
		$data = $this->db->queryAll($sql);
		// D($sql);
		// $sql = "SELECT type AS id,ROUND(AVG(Total),2) AS label FROM (
		// 		SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , peso_neto , (({$cantidad}) / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
		// 		INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
		// 		WHERE campo like '%Total Peso%' {$sWhere}
		// 		AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle
		// 		GROUP BY type , WEEK(fecha) 
		// 		ORDER BY type , WEEK(fecha)";
		// $data_merma = $this->db->queryAllSpecial($sql);
		// // D($sql);
		// $total_merma = array_sum($data_merma);
		// D($total_merma);
		$response = new stdClass;
		$series = new stdClass;
		$flag_count = 0;
		$labor = "";

		$response->series = [];
		$cantidad = 0;
		foreach ($data as $key => $value) {
			$response->legend[(int)$value->semana] = (int)$value->semana;
			if($this->session->id_company == 7){
				// if($value->semana == 40){
					// D("CANTIDAD : ".$value->cantidad);
					// D("SEMANA : ".$value->semana);
					// D("PESO SEMANAL : ".$total_peso_merma[$value->semana]);
					$cantidad = round(($value->cantidad / $total_peso_merma[$value->semana])*100 , 2);
					// D("CANTIDAD PESO : ".$cantidad);
				// }
			}else{
				$cantidad = (float)$value->cantidad;

			}
			// D($value);
			if($labor != $value->labor){
				$labor = $value->labor;
				$series = new stdClass;
				$series->name = $value->labor;
				$series->type = "line";
				$series->data = [];
				$series->data[] = $cantidad;
				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series->markLine = new stdClass;
					$series->markLine->data = [];
					$markLine->name = "Umbral";
					$markLine->value = $Umbral;
					$markLine->xAxis = -1;
					$markLine->yAxis = $Umbral;
					$series->markLine->data[0][] = $markLine;
					
					$markLine = new stdClass;
					$markLine->xAxis = 52;
					$markLine->yAxis = $Umbral;

					$series->markLine->data[0][] = $markLine;
					$flag_count++;
				}	

				$response->series[$value->labor] = $series;
			}else{
				// $cantidad = (float)$cantidad;
				$response->series[$value->labor]->data[] = $cantidad;
			}
			// D(round((($value->cantidad * 100) / $total_merma) , 2));
		}

		return $response;
	}

	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "DAÑOS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['60%', '60%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = "Merma";
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["orient"] = "vertical";
			$response->pie["legend"]["x"] = "left";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];

			foreach ($data as $key => $value) {
				$value = (object)$value;
				$response->pie["legend"]["data"][] = $value->type;
				if($version === 3){
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)$value->cantidad , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							]
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2)];
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2),
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}