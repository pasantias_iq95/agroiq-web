<?php defined('PHRAPI') or die("Direct access not allowed!");

if (!function_exists('stats_standard_deviation')) {
    /**
     * This user-land implementation follows the implementation quite strictly;
     * it does not attempt to improve the code or algorithm in any way. It will
     * raise a warning if you have fewer than 2 values in your array, just like
     * the extension does (although as an E_USER_WARNING, not E_WARNING).
     * 
     * @param array $a 
     * @param bool $sample [optional] Defaults to false
     * @return float|bool The standard deviation or false on error.
     */
    function stats_standard_deviation(array $a, $sample = false) {
        $n = count($a);
        if ($n === 0) {
            trigger_error("The array has zero elements", E_USER_WARNING);
            return false;
        }
        if ($sample && $n === 1) {
            trigger_error("The array has only 1 element", E_USER_WARNING);
            return false;
        }
        $mean = array_sum($a) / $n;
        $carry = 0.0;
        foreach ($a as $val) {
            $d = ((double) $val) - $mean;
            $carry += $d * $d;
        };
        if ($sample) {
           --$n;
        }
        return round(sqrt($carry / $n),2);
    }
}

class Perchas {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function index(){
        $filters = $this->getFilters();
        $response = new stdClass;
        $response = $this->getTags($filters);
        $main = $this->mainTable($filters);
        $response->table = $main->data;
        $response->grafica = $main->grafica;
        $response->table_history = $this->historyTable($filters);
        $comparation = $this->comparationTable($filters , $main->data["total"]);
        $response->table_comparation = $comparation->data;
        $response->grafica_comparation = $comparation->grafica;
        $historica = $this->historieTable($filters);
        $response->history = $historica->data;
        $response->legend_history = $historica->legend;
        return $response;
    }
    

    private function getFilters(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $filters = (object)[
            "id" => getValueFrom($postdata , "id" , -1 , FILTER_SANITIZE_PHRAPI_INT),
        ];

        return $filters;
    }

    private function comparationTable($filters , $params){
    	$response = new stdClass;
    	$agencies = ["RIO CENTRO LOS CEIBOS" , "RIOCENTRO NORTE" , "RIOCENTRO LA PUNTILLA" , "CIUDAD EL DORADO"];
    	$titles = ["coronas_rotas" , "dedos_rajados" , "otros_danios"];
    	$data = [];
    	foreach ($agencies as $key => $value) {
    		$response->data[$value]["agencia"] = $value;
	    	foreach ($titles as $llave => $valor) {
	    		$response->data[$value][$valor] = rand(1,10);
	    		$data[$valor][] = $response->data[$value][$valor];
	    	}
    	}

    	$response->data["promedio"]["agencia"] = "PROM";
		$response->data["promedio"]["coronas_rotas"] = round(($params->coronas_rotas / $params->manos) * 100 , 2);
		$response->data["promedio"]["dedos_rajados"] = round(($params->dedos_rajados / $params->dedos) * 100 , 2);
		$response->data["promedio"]["otros_danios"] = round(($params->otros_danios / $params->dedos) * 100 , 2);

    	$response->grafica = [];
    	$response->grafica[] = ["label" => "CORONAS" , "value" => (double)$response->data["promedio"]["coronas_rotas"] ];
    	$response->grafica[] = ["label" => "RAJADOS" , "value" => (double)$response->data["promedio"]["dedos_rajados"] ];
    	$response->grafica[] = ["label" => "OTROS" , "value" => (double)$response->data["promedio"]["otros_danios"] ];

    	return $response;
    }

    private function historyTable($filters){
    	$response = new stdClass;
    	$months = ["ENE" , "FEB" , "MAR" , "ABR" , "MAY" , "JUN" , "JUL" , "AGO" , "SEP" , "OCT" , "NOV" , "DIC"];
		$grados = 7;
    	$data = [];

    	for ($i=1; $i < $grados; $i++) { 
    		$response->data[$i]["grado"] = $i;
	    	foreach ($months as $key => $value) {
	    		$response->data[$i][$value] = (double)rand(1,99);
	    		$data[$value][] = $response->data[$i][$value];
	    	}
	    }

	    $response->data["total"]["grado"] = "TOTAL";
	    $response->data["promedio"]["grado"] = "PROM";
	    $response->data["max"]["grado"] = "MAX";
	    $response->data["min"]["grado"] = "MIN";
	    $response->data["desv"]["grado"] = "DESV";

	    foreach ($months as $key => $value) {
    		$response->data["total"][$value] = (double)array_sum($data[$value]);
    		$response->data["promedio"][$value] = (double)$this->array_avarage($data[$value]);
    		$response->data["max"][$value] = (double)max($data[$value]);
    		$response->data["min"][$value] = (double)min($data[$value]);
    		$response->data["desv"][$value] = (double)stats_standard_deviation($data[$value]);
    	}

    	return $response->data;
    }

    private function historieTable($filters){
        $response = new stdClass;
        $cities = ["ENE" , "FEB" , "MAR" , "ABR" , "MAY" , "JUN" , "JUL" , "AGO" , "SEP" , "OCT" , "NOV" , "DIC"];
        $titles = ["Machala" , "Guayaquil" , "Termigrafos" , "Camaras Refrig" , "Bodegas" , "Perchas" , "Min" , "Max"];
        $promedio = 0;
        $response->data = [];
        $response->legend = $cities;
        foreach ($cities as $key => $value) {
            foreach ($titles as $llave => $valor) {
                if($valor == "Max"){
                    $promedio =  30;
                }
                elseif($valor == "Min"){
                    $promedio =  10;
                }else{
                    $promedio =  rand(1,70);
                }

                if(!isset($response->data[$valor]->itemStyle)){
                    $series = new stdClass;
                    $series->name = $valor;
                    $series->type = "line";
                    $series->connectNulls = true;
                    foreach($titles as $lbl){
                        $series->data[] = null;
                    }
                    $series->data[array_search($value, $cities)] = round($promedio ,2);
                    $series->itemStyle = (object)[
                        "normal" => [
                            "barBorderWidth" => 6,
                            "barBorderRadius" => 0,
                            "label" => [ "show" => true , "position" => "insideTop"]
                        ]
                    ];
                    $response->data[$valor] = $series;
                }else{
                    $response->data[$valor]->data[array_search($value, $cities)] = round($promedio ,2);
                }
            }
        }
        return $response;
    }

    private function mainTable($filters){
    	$response = new stdClass;

    	$grados = 7;
    	$data = [];

    	for ($i=1; $i < $grados; $i++) { 
	    	$response->data[$i] = [
	            "grado" => $i,
	            "manos" => rand(1,99),
	            "dedos" => rand(1,99),
	            "dedos_rajados" => rand(1,10),
	            "otros_danios" => rand(1,10),
	            "coronas_rotas" => rand(1,10),
	        ];

	        $data["manos"][] = $response->data[$i]["manos"];
	        $data["dedos"][] = $response->data[$i]["dedos"];
	        $data["dedos_rajados"][] = $response->data[$i]["dedos_rajados"];
	        $data["otros_danios"][] = $response->data[$i]["otros_danios"];
	        $data["coronas_rotas"][] = $response->data[$i]["coronas_rotas"];
    	}

    	$response->data["total"] = (object)[
    		"grado" => "TOTAL",
            "manos" => array_sum($data["manos"]),
            "dedos" => array_sum($data["dedos"]),
            "dedos_rajados" => array_sum($data["dedos_rajados"]),
            "otros_danios" => array_sum($data["otros_danios"]),
            "coronas_rotas" => array_sum($data["coronas_rotas"]),
    	];

    	$response->data["promedio"] = (object)[
    		"grado" => "PROM",
            "manos" => $this->array_avarage($data["manos"]),
            "dedos" => $this->array_avarage($data["dedos"]),
            "dedos_rajados" => $this->array_avarage($data["dedos_rajados"]),
            "otros_danios" => $this->array_avarage($data["otros_danios"]),
            "coronas_rotas" => $this->array_avarage($data["coronas_rotas"]),
    	];

    	$response->data["max"] = (object)[
    		"grado" => "MAX",
            "manos" => max($data["manos"]),
            "dedos" => max($data["dedos"]),
            "dedos_rajados" => max($data["dedos_rajados"]),
            "otros_danios" => max($data["otros_danios"]),
            "coronas_rotas" => max($data["coronas_rotas"]),
    	];

    	$response->data["min"] = (object)[
    		"grado" => "MIN",
            "manos" => min($data["manos"]),
            "dedos" => min($data["dedos"]),
            "dedos_rajados" => min($data["dedos_rajados"]),
            "otros_danios" => min($data["otros_danios"]),
            "coronas_rotas" => min($data["coronas_rotas"]),
    	];

    	$response->data["desv"] = (object)[
    		"grado" => "DESV",
            "manos" => stats_standard_deviation($data["manos"]),
            "dedos" => stats_standard_deviation($data["dedos"]),
            "dedos_rajados" => stats_standard_deviation($data["dedos_rajados"]),
            "otros_danios" => stats_standard_deviation($data["otros_danios"]),
            "coronas_rotas" => stats_standard_deviation($data["coronas_rotas"]),
    	];


    	$response->grafica = [];
    	$response->grafica[] = ["label" => "DEDOS RAJADOS" , "value" => (double)$response->data["total"]->dedos_rajados ];
    	$response->grafica[] = ["label" => "OTROS DAÑOS" , "value" => (double)$response->data["total"]->otros_danios ];

    	return $response;
    }

    private function array_avarage($array){
 		return round(array_sum($array) / count($array),2);
    }

    private function getTags($filters){
        $response = new stdClass;

        $response->tags[] = (object)[
            "tittle" => "Total Manos",
            "subtittle" => "",
            "valor" => (double)100,
            "promedio" => (double)100,
            "cssClass" => "blue-steel"
        ];
        $response->tags[] = (object)[
            "tittle" => "Total Dedos",
            "subtittle" => "",
            "valor" => (double)100,
            "promedio" => (double)100,
            "cssClass" => "green-jungle"
        ];
        $response->tags[] = (object)[
            "tittle" => "% CORONAS RAJADAS",
            "subtittle" => "",
            "valor" => (double)100,
            "promedio" => (double)100,
            "cssClass" => "yellow-gold"
        ];

        return $response;
    }
}