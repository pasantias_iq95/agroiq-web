<?php defined('PHRAPI') or die("Direct access not allowed!");

class PerchasBD {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);

        $this->availableClientes = '';
		if(in_array($this->session->logged, [36, 50, 51]))
			$this->availableClientes = "'". implode(['Tia'], "','") . "'";
		if($this->session->logged == 0)
			$this->availableClientes = "'". implode(['Mi Comisariato'], "','") . "'";
    }

    private function ProcessParams(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        return $postdata;
    }

    public function listado(){
        $params = $this->ProcessParams();
        $response = new stdClass;
        $sWhere = "";

        if($params->fecha_inicial != "" && $params->fecha_final != ""){
            $sWhere .= " AND lancofruit_perchas.fecha BETWEEN '{$params->fecha_inicial}' AND '{$params->fecha_final}'";
        }
        if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
        if($params->cliente != ""){
            $sWhere .= " AND lancofruit_perchas.cliente = '{$params->cliente}'";
        }

        if($params->sucursal != ""){
            $sWhere .= " AND lancofruit_perchas.local = '{$params->sucursal}'";
        }
    
        $sql = "SELECT 
                    lancofruit_perchas.fecha,
                    lancofruit_perchas.hora, 
                    lancofruit_perchas.cliente,
                    lancofruit_perchas.local,
                    lancofruit_perchas.responsable,
                    lancofruit_perchas_pdf.url
                FROM lancofruit_perchas 
                INNER JOIN lancofruit_perchas_pdf ON lancofruit_perchas_pdf.id_percha = lancofruit_perchas.id
                WHERE lancofruit_perchas.status = 'Activo' $sWhere
                ORDER BY fecha DESC";
        $response->listado = $this->db->queryAll($sql);
        return $response;
    }
}