<?php defined('PHRAPI') or die("Direct access not allowed!");

class PerchasComparacion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->session = Session::getInstance();
		$this->db = DB::getInstance($this->session->agent_user);
		$this->postdata = (object)json_decode(file_get_contents("php://input"));

		$this->availableClientes = '';
		if(in_array($this->session->logged, [36, 50, 51]))
			$this->availableClientes = "'". implode(['Tia'], "','") . "'";
		if($this->session->logged == 0)
			$this->availableClientes = "'". implode(['Mi Comisariato'], "','") . "'";
    }
    
    public function last(){
		$response = new stdClass;

		$sWhere = "";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$response->days = $this->db->queryAllOne("SELECT fecha FROM lancofruit_perchas WHERE 1=1 $sWhere GROUP BY fecha");
		$response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM lancofruit_perchas WHERE 1=1 $sWhere");
        return $response;
	}

	public function variables(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$response->clientes = $this->db->queryAllOne("SELECT cliente FROM lancofruit_perchas WHERE 1=1 {$sWhere} GROUP BY cliente");

		if($this->postdata->cliente != ''){
			$sWhere .= " AND cliente = '{$this->postdata->cliente}'";
		}
		$response->sucursales = $this->db->queryAllOne("SELECT local FROM lancofruit_perchas WHERE 1=1 {$sWhere} GROUP BY local");
		return $response;
	}
	
	public function principal(){
		$response = new stdClass;
		$response->status = 200;
		$response->general = $this->general();
		$response->defectos = $this->defectos();
		$response->temperatura = $this->temperatura();
		$response->cluster = $this->clusterGrado();
		return $response;
	}

	public function graficaZona(){
		$response = new stdClass;
		$response->defectos = $this->graficaDefectosZona();
		$response->cluster = $this->graficaClusterZona();
		#$response->empaque = $this->graficaEmpaqueZona();

		$response->defectos_finca = $this->graficaDefectosFincas();
		$response->cluster_finca = $this->graficaClusterFincas();
		#$response->empaque_finca = $this->graficaEmpaqueFincas();
		return $response;
	}

	// tabla principal
	private function general(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$sql = "SELECT
					cliente AS cliente,
					SUM(gavetas) AS recibidas,
					SUM(kilos_totales) kilos_totales,
					SUM(kilos_destruidos) kilos_destruidos,
					SUM(porc_fruta_lancofruit)/SUM(porc_fruta_lancofruit+porc_fruta_terceros+porc_percha_vacia)*100 AS porc_lancofruit,
					SUM(porc_fruta_terceros)/SUM(porc_fruta_lancofruit+porc_fruta_terceros+porc_percha_vacia)*100 AS porc_otros,
					SUM(porc_percha_vacia)/SUM(porc_fruta_lancofruit+porc_fruta_terceros+porc_percha_vacia)*100 AS porc_vacia
				FROM lancofruit_perchas
				WHERE 1=1 $sWhere
				GROUP BY cliente";
		$response = $this->db->queryAll($sql);
		foreach($response as $row){
			$sql = "SELECT
						local AS cliente,
						SUM(gavetas) AS recibidas,
						SUM(kilos_totales) kilos_totales,
						SUM(kilos_destruidos) kilos_destruidos,
						SUM(porc_fruta_lancofruit)/SUM(porc_fruta_lancofruit+porc_fruta_terceros+porc_percha_vacia)*100 AS porc_lancofruit,
						SUM(porc_fruta_terceros)/SUM(porc_fruta_lancofruit+porc_fruta_terceros+porc_percha_vacia)*100 AS porc_otros,
						SUM(porc_percha_vacia)/SUM(porc_fruta_lancofruit+porc_fruta_terceros+porc_percha_vacia)*100 AS porc_vacia
					FROM lancofruit_perchas
					WHERE cliente = '{$row->cliente}' $sWhere
					GROUP BY local";
			$row->detalle = $this->db->queryAll($sql);
		}

		return $response;
	}

	public function cluster(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT cluster_dedos
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				WHERE 1=1 {$sWhere}
				GROUP BY cluster_dedos
				ORDER BY cluster_dedos";
		$response->tipos = $this->db->queryAllOne($sql);

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere}
				GROUP BY zona";
		$response->data = $this->db->queryAll($sql);
		foreach($response->data as $zona){
			$zona->total = 0;
			foreach($response->tipos as $tipo){
				$zona->{"tipo_{$tipo}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM calidad INNER JOIN fincas ON id_finca = fincas.id INNER JOIN calidad_dedos ON id_calidad = calidad.id AND cluster_dedos = {$tipo} WHERE zona = '{$zona->zona}' {$sWhere}");
				$zona->total += $zona->{"tipo_{$tipo}"};
			}

			$sql = "SELECT finca as zona, id_finca
					FROM calidad
					INNER JOIN fincas ON id_finca = fincas.id
					WHERE zona = '{$zona->zona}'
					GROUP BY id_finca";
			$zona->detalle = $this->db->queryAll($sql);
			foreach($zona->detalle as $finca){
				$finca->total = 0;
				foreach($response->tipos as $tipo){
					$finca->{"tipo_{$tipo}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM calidad INNER JOIN calidad_dedos ON id_calidad = calidad.id AND cluster_dedos = {$tipo} WHERE id_finca = $finca->id_finca {$sWhere}");
					$finca->total += $finca->{"tipo_{$tipo}"};
				}
			}
		}

		return $response;
	}

	public function temperatura(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$sql = "SELECT
					cliente AS cliente
				FROM lancofruit_perchas
				WHERE 1=1 $sWhere
				GROUP BY cliente";
		$response = $this->db->queryAll($sql);
		foreach($response as $row){
			$sql = "SELECT ROUND(AVG(temp), 2)
					FROM (
						SELECT temp_bodega_1 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_bodega_1 > 0
						UNION ALL 
						SELECT temp_bodega_2 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_bodega_2 > 0
						UNION ALL 
						SELECT temp_bodega_3 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_bodega_3 > 0
						UNION ALL 
						SELECT temp_bodega_4 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_bodega_4 > 0
						UNION ALL 
						SELECT temp_bodega_5 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_bodega_5 > 0
					) tbl";
			$row->temp_bodega = (float) $this->db->queryOne($sql);
			$sql = "SELECT ROUND(AVG(temp), 2)
					FROM (
						SELECT temp_percha_1_1 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_1_1 > 0
						UNION ALL 
						SELECT temp_percha_1_2 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_1_2 > 0
						UNION ALL 
						SELECT temp_percha_1_3 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_1_3 > 0
						UNION ALL 
						SELECT temp_percha_1_4 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_1_4 > 0
						UNION ALL 
						SELECT temp_percha_1_5 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_1_5 > 0
					) tbl";
			$row->temp_percha_1 = (float) $this->db->queryOne($sql);
			$sql = "SELECT ROUND(AVG(temp), 2)
					FROM (
						SELECT temp_percha_2_1 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_2_1 > 0
						UNION ALL 
						SELECT temp_percha_2_2 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_2_2 > 0
						UNION ALL 
						SELECT temp_percha_2_3 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_2_3 > 0
						UNION ALL 
						SELECT temp_percha_2_4 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_2_4 > 0
						UNION ALL 
						SELECT temp_percha_2_5 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND temp_percha_2_5 > 0
					) tbl";
			$row->temp_percha_2 = (float) $this->db->queryOne($sql);

			$sql = "SELECT
						local AS cliente
					FROM lancofruit_perchas
					WHERE cliente = '{$row->cliente}' $sWhere
					GROUP BY local";
			$row->detalle = $this->db->queryAll($sql);
			foreach($row->detalle as $row2){
				$sql = "SELECT ROUND(AVG(temp), 2)
						FROM (
							SELECT temp_bodega_1 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_bodega_1 > 0
							UNION ALL 
							SELECT temp_bodega_2 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_bodega_2 > 0
							UNION ALL 
							SELECT temp_bodega_3 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_bodega_3 > 0
							UNION ALL 
							SELECT temp_bodega_4 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_bodega_4 > 0
							UNION ALL 
							SELECT temp_bodega_5 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_bodega_5 > 0
						) tbl";
				$row2->temp_bodega = (float) $this->db->queryOne($sql);
				$sql = "SELECT ROUND(AVG(temp), 2)
						FROM (
							SELECT temp_percha_1_1 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_1_1 > 0
							UNION ALL 
							SELECT temp_percha_1_2 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_1_2 > 0
							UNION ALL 
							SELECT temp_percha_1_3 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_1_3 > 0
							UNION ALL 
							SELECT temp_percha_1_4 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_1_4 > 0
							UNION ALL 
							SELECT temp_percha_1_5 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_1_5 > 0
						) tbl";
				$row2->temp_percha_1 = (float) $this->db->queryOne($sql);
				$sql = "SELECT ROUND(AVG(temp), 2)
						FROM (
							SELECT temp_percha_2_1 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_2_1 > 0
							UNION ALL 
							SELECT temp_percha_2_2 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_2_2 > 0
							UNION ALL 
							SELECT temp_percha_2_3 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_2_3 > 0
							UNION ALL 
							SELECT temp_percha_2_4 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_2_4 > 0
							UNION ALL 
							SELECT temp_percha_2_5 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND cliente = '{$row->cliente}' AND local = '{$row2->cliente}' AND temp_percha_2_5 > 0
						) tbl";
				$row2->temp_percha_2 = (float) $this->db->queryOne($sql);
			}
		}

		return $response;
	}

	public function defectos(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		
		$sql = "SELECT categoria AS type
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
				WHERE 1=1 {$sWhere} AND cantidad > 0
				GROUP BY categoria";
		$response->categorias = $this->db->queryAll($sql);
		foreach($response->categorias as $categoria){
			$sql = "SELECT defecto campo
					FROM lancofruit_perchas percha
					INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
					WHERE categoria = '{$categoria->type}' {$sWhere} AND cantidad > 0
					GROUP BY defecto";
			$categoria->defectos = $this->db->queryAll($sql);
			foreach($categoria->defectos as $defecto){
				$defecto->siglas = trim(substr($defecto->campo, 0, 2));
				$defecto->descripcion = trim(substr($defecto->campo, 3, strlen($defecto->campo)-3));
			}
		}

		$sql = "SELECT cliente 
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
				WHERE 1=1 {$sWhere} AND cantidad > 0
				GROUP BY cliente";
		$response->data = $this->db->queryAll($sql);

		foreach($response->data as $cliente){
			$cliente->total_defectos = 0;
			foreach($response->categorias as $categoria){
				foreach($categoria->defectos as $defecto){
					$sql = "SELECT SUM(cantidad) 
							FROM lancofruit_perchas percha
							INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id AND categoria = '{$categoria->type}' AND defecto = '{$defecto->campo}' 
							WHERE cliente = '{$cliente->cliente}' {$sWhere} AND cantidad > 0";
					$cliente->{"{$categoria->type}_{$defecto->siglas}"} = $this->db->queryOne($sql);
					$cliente->total_defectos += $cliente->{"{$categoria->type}_{$defecto->siglas}"};
				}
			}

			$sql = "SELECT local, local AS cliente
					FROM lancofruit_perchas percha
					INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
					WHERE cliente = '{$cliente->cliente}' {$sWhere} AND cantidad > 0
					GROUP BY local";
			$cliente->detalle = $this->db->queryAll($sql);
			foreach($cliente->detalle as $sucursal){
				$sucursal->total_defectos = 0;
				foreach($response->categorias as $categoria){
					foreach($categoria->defectos as $defecto){
						$sql = "SELECT SUM(cantidad) 
								FROM lancofruit_perchas percha
								INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id AND categoria = '{$categoria->type}' AND defecto = '{$defecto->campo}' 
								WHERE cliente = '{$cliente->cliente}' AND local = '{$sucursal->local}' {$sWhere}";
						$sucursal->{"{$categoria->type}_{$defecto->siglas}"} = $this->db->queryOne($sql);
						$sucursal->total_defectos += $sucursal->{"{$categoria->type}_{$defecto->siglas}"};
					}
				}
			}
		}
		return $response;
	}

	public function clusterGrado(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$sql = "SELECT
					cliente AS cliente,
					AVG(IF(grado_1_cantidad_cluster > 0, grado_1_cantidad_cluster, NULL)) grado_1,
					AVG(IF(grado_2_cantidad_cluster > 0, grado_2_cantidad_cluster, NULL)) grado_2,
					AVG(IF(grado_3_cantidad_cluster > 0, grado_3_cantidad_cluster, NULL)) grado_3,
					AVG(IF(grado_4_cantidad_cluster > 0, grado_4_cantidad_cluster, NULL)) grado_4,
					AVG(IF(grado_5_cantidad_cluster > 0, grado_5_cantidad_cluster, NULL)) grado_5,
					AVG(IF(grado_6_cantidad_cluster > 0, grado_6_cantidad_cluster, NULL)) grado_6
				FROM lancofruit_perchas
				WHERE 1=1 $sWhere
				GROUP BY cliente";
		$response = $this->db->queryAll($sql);
		foreach($response as $row){
			$sql = "SELECT
						local AS cliente,
						AVG(IF(grado_1_cantidad_cluster > 0, grado_1_cantidad_cluster, NULL)) grado_1,
						AVG(IF(grado_2_cantidad_cluster > 0, grado_2_cantidad_cluster, NULL)) grado_2,
						AVG(IF(grado_3_cantidad_cluster > 0, grado_3_cantidad_cluster, NULL)) grado_3,
						AVG(IF(grado_4_cantidad_cluster > 0, grado_4_cantidad_cluster, NULL)) grado_4,
						AVG(IF(grado_5_cantidad_cluster > 0, grado_5_cantidad_cluster, NULL)) grado_5,
						AVG(IF(grado_6_cantidad_cluster > 0, grado_6_cantidad_cluster, NULL)) grado_6
					FROM lancofruit_perchas
					WHERE cliente = '{$row->cliente}' $sWhere
					GROUP BY local";
			$row->detalle = $this->db->queryAll($sql);
		}

		return $response;
	}

	// graficas zonas
	public function graficaDefectosZona(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$response->data = new stdClass;
		$sql = "SELECT categoria type
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id AND categoria IS NOT NULL
				WHERE cantidad > 0 {$sWhere}
				GROUP BY categoria";
		$response->categorias = $this->db->queryAllOne($sql);
		foreach($response->categorias as $categoria){
			$response->data->{$categoria} = ["series" => []];

			$sql = "SELECT cliente
					FROM lancofruit_perchas percha
					INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
					WHERE cantidad > 0 AND categoria = '{$categoria}' {$sWhere}
					GROUP BY cliente";
			$clientes = $this->db->queryAllOne($sql);

			$sql = "SELECT defecto
					FROM lancofruit_perchas percha
					INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
					WHERE cantidad > 0 AND categoria = '{$categoria}' {$sWhere}
					GROUP BY defecto";
			$defectos = $this->db->queryAll($sql);

			foreach($defectos as $defecto){
				$siglas = trim(explode("-", $defecto->defecto)[0]);
				$response->data->{$categoria}["series"][] = [
					"name" => $siglas,
					"type" => 'bar',
					"stack" => 'barra',
					"data" => []
				];
				
				foreach($clientes as $cliente){
					$sql = "SELECT SUM(cantidad)
							FROM lancofruit_perchas percha
							INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
							WHERE cliente = '{$cliente}' AND defecto = '{$defecto->defecto}' AND categoria = '{$categoria}' {$sWhere}";
					$val = (int) $this->db->queryOne($sql);
					$response->data->{$categoria}["series"][count($response->data->{$categoria}["series"])-1]["data"][] = $val;
				}
			}
			$response->data->{$categoria}["legends"] = $clientes;
		}

		return $response;
	}

	public function graficaClusterZona(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$response->data = new stdClass;

		$sql = "SELECT cliente
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
				WHERE 1=1 {$sWhere}
				GROUP BY cliente";
		$clientes = $this->db->queryAllOne($sql);

		
		$defectos = [
			[
				"grado" => 1,
				"field" => "grado_1_cantidad_cluster"
			],
			[
				"grado" => 2,
				"field" => "grado_2_cantidad_cluster"
			],
			[
				"grado" => 3,
				"field" => "grado_3_cantidad_cluster"
			],
			[
				"grado" => 4,
				"field" => "grado_4_cantidad_cluster"
			],
			[
				"grado" => 5,
				"field" => "grado_5_cantidad_cluster"
			],
			[
				"grado" => 6,
				"field" => "grado_6_cantidad_cluster"
			]
		];

		$response->data->series = [];
		foreach($defectos as $defecto){
			$defecto = (object) $defecto;

			$response->data->series[] = [
				"name" => "Grado {$defecto->grado}",
				"type" => 'bar',
				"stack" => 'barra',
				"data" => []
			];
			
			foreach($clientes as $cliente){
				$sql = "SELECT SUM({$defecto->field})
						FROM lancofruit_perchas
						WHERE cliente = '{$cliente}' {$sWhere}";
				$val = (int) $this->db->queryOne($sql);
				$response->data->series[count($response->data->series)-1]["data"][] = $val;
			}
		}
		$response->data->legends = $clientes;

		return $response;
	}

	public function graficaEmpaqueZona(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$response->data = new stdClass;

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere}
				GROUP BY zona";
		$zonas = $this->db->queryAllOne($sql);

		$sql = "SELECT detalle
				FROM calidad
				INNER JOIN fincas ON id_finca = fincas.id
				INNER JOIN calidad_empaque ON calidad.id = id_calidad
				WHERE cantidad > 0 {$sWhere}
				GROUP BY detalle";
		$defectos = $this->db->queryAll($sql);

		$response->data->series = [];
		foreach($defectos as $defecto){
			$response->data->series[] = [
				"name" => $defecto->detalle,
				"type" => 'bar',
				"stack" => 'barra',
				"data" => []
			];
			
			foreach($zonas as $zona){
				$sql = "SELECT SUM(cantidad)
						FROM calidad
						INNER JOIN fincas ON id_finca = fincas.id
						INNER JOIN calidad_empaque ON calidad.id = id_calidad
						WHERE zona = '{$zona}' AND detalle = '{$defecto->detalle}' {$sWhere}";
				$val = (int) $this->db->queryOne($sql);
				$response->data->series[count($response->data->series)-1]["data"][] = $val;
			}
		}
		$response->data->legends = $zonas;

		return $response;
	}

	// graficas fincas
	public function graficaDefectosFincas(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$sql = "SELECT cliente
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
				WHERE 1=1 {$sWhere}
				GROUP BY cliente";
		$response->clientes = $this->db->queryAllOne($sql);
		$response->data = new stdClass;

		foreach($response->clientes as $cliente){
			$sql = "SELECT categoria
					FROM lancofruit_perchas percha
					INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
					WHERE cantidad > 0 AND cliente = '{$cliente}' {$sWhere}
					GROUP BY categoria";
			$response->categorias = $this->db->queryAllOne($sql);
			foreach($response->categorias as $categoria){
				if(!$response->data->{$cliente}){
					$response->data->{$cliente} =  new stdClass;
				}
				$response->data->{$cliente}->{$categoria} = ["series" => []];

				$sql = "SELECT local as sucursal
						FROM lancofruit_perchas percha
						INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
						WHERE cantidad > 0 AND categoria = '{$categoria}' AND cliente = '{$cliente}' {$sWhere}
						GROUP BY local";
				$sucursales = $this->db->queryAllOne($sql);

				$sql = "SELECT defecto
						FROM lancofruit_perchas percha
						INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
						WHERE cantidad > 0 AND categoria = '{$categoria}' AND cliente = '{$cliente}' {$sWhere}
						GROUP BY defecto";
				$defectos = $this->db->queryAll($sql);

				foreach($defectos as $defecto){
					$siglas = trim(explode("-", $defecto->defecto)[0]);
					$response->data->{$cliente}->{$categoria}["series"][] = [
						"name" => $siglas,
						"type" => 'bar',
						"stack" => 'barra',
						"data" => []
					];
					
					foreach($sucursales as $sucursal){
						$sql = "SELECT SUM(cantidad)
								FROM lancofruit_perchas percha
								INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
								WHERE local = '{$sucursal}' AND defecto = '{$defecto->defecto}' AND categoria = '{$categoria}' AND cliente = '{$cliente}' {$sWhere}";
						$val = (int) $this->db->queryOne($sql);
						$response->data->{$cliente}->{$categoria}["series"][count($response->data->{$cliente}->{$categoria}["series"])-1]["data"][] = $val;
					}
				}
				$response->data->{$cliente}->{$categoria}["legends"] = $sucursales;
			}
		}

		return $response;
	}

	public function graficaClusterFincas(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$sql = "SELECT cliente
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
				WHERE 1=1 {$sWhere}
				GROUP BY cliente";
		$response->clientes = $this->db->queryAllOne($sql);

		$response->data = new stdClass;
		foreach($response->clientes as $cliente){
			$response->data->{$cliente} = new stdClass;

			$sql = "SELECT local as sucursal
					FROM lancofruit_perchas percha
					INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
					WHERE cliente = '{$cliente}' {$sWhere}
					GROUP BY local";
			$sucursales = $this->db->queryAllOne($sql);

			$defectos = [
				[
					"grado" => 1,
					"field" => "grado_1_cantidad_cluster"
				],
				[
					"grado" => 2,
					"field" => "grado_2_cantidad_cluster"
				],
				[
					"grado" => 3,
					"field" => "grado_3_cantidad_cluster"
				],
				[
					"grado" => 4,
					"field" => "grado_4_cantidad_cluster"
				],
				[
					"grado" => 5,
					"field" => "grado_5_cantidad_cluster"
				],
				[
					"grado" => 6,
					"field" => "grado_6_cantidad_cluster"
				]
			];

			$response->data->{$cliente}->series = [];
			foreach($defectos as $defecto){
				$defecto = (object) $defecto;

				$response->data->{$cliente}->series[] = [
					"name" => "Grado {$defecto->grado}",
					"type" => 'bar',
					"stack" => 'barra',
					"data" => []
				];
				
				foreach($sucursales as $sucursal){
					$sql = "SELECT SUM({$defecto->field})
							FROM lancofruit_perchas percha
							INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
							WHERE local = '{$sucursal}' AND cliente = '{$cliente}' {$sWhere}";
					$val = (int) $this->db->queryOne($sql);
					$response->data->{$cliente}->series[count($response->data->{$cliente}->series)-1]["data"][] = $val;
				}
			}
			$response->data->{$cliente}->legends = $sucursales;
		}

		return $response;
	}

	public function graficaEmpaqueFincas(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere}
				GROUP BY zona";
		$zonas = $this->db->queryAllOne($sql);

		$response->data = new stdClass;
		foreach($zonas as $zona){

			$sql = "SELECT finca
					FROM calidad
					INNER JOIN calidad_dedos ON calidad.id = id_calidad
					INNER JOIN fincas ON id_finca = fincas.id
					WHERE zona = '{$zona}' {$sWhere}
					GROUP BY id_finca";
			$fincas = $this->db->queryAllOne($sql);

			$sql = "SELECT detalle
					FROM calidad
					INNER JOIN fincas ON id_finca = fincas.id
					INNER JOIN calidad_empaque ON calidad.id = id_calidad
					WHERE zona = '{$zona}' AND cantidad > 0 {$sWhere}
					GROUP BY detalle";
			$defectos = $this->db->queryAll($sql);

			if(!$response->data->{$zona}){
				$response->data->{$zona} = new stdClass;
			}
			$response->data->{$zona}->series = [];
			foreach($defectos as $defecto){
				$response->data->{$zona}->series[] = [
					"name" => $defecto->detalle,
					"type" => 'bar',
					"stack" => 'barra',
					"data" => []
				];
				
				foreach($fincas as $finca){
					$sql = "SELECT SUM(cantidad)
							FROM calidad
							INNER JOIN fincas ON id_finca = fincas.id
							INNER JOIN calidad_empaque ON calidad.id = id_calidad
							WHERE zona = '{$zona}' AND finca = '{$finca}' AND detalle = '{$defecto->detalle}' {$sWhere}";
					$val = (int) $this->db->queryOne($sql);
					$response->data->{$zona}->series[count($response->data->{$zona}->series)-1]["data"][] = $val;
				}
			}
			$response->data->{$zona}->legends = $fincas;
		}
		$response->zonas = $zonas;

		return $response;
	}
}