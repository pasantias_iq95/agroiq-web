<?php defined('PHRAPI') or die("Direct access not allowed!");

class PerchasDia {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->session = Session::getInstance();
		$this->db = DB::getInstance($this->session->agent_user);
		$this->postdata = (object)json_decode(file_get_contents("php://input"));

		$this->availableClientes = '';
		if(in_array($this->session->logged, [36, 50, 51]))
			$this->availableClientes = "'". implode(['Tia'], "','") . "'";
		if($this->session->logged == 0)
			$this->availableClientes = "'". implode(['Mi Comisariato'], "','") . "'";
    }
    
    public function last(){
		$response = new stdClass;
		$sWhere = "";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$response->days = $this->db->queryAllOne("SELECT fecha FROM lancofruit_perchas WHERE 1=1 $sWhere GROUP BY fecha");
		$response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM lancofruit_perchas WHERE 1=1 $sWhere");
        return $response;
	}

	public function variables(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$response->clientes = $this->db->queryAllOne("SELECT cliente FROM lancofruit_perchas WHERE 1=1 {$sWhere} GROUP BY cliente");

		if($this->postdata->cliente != ''){
			$sWhere .= " AND cliente = '{$this->postdata->cliente}'";
		}
		$response->sucursales = $this->db->queryAllOne("SELECT local FROM lancofruit_perchas WHERE 1=1 {$sWhere} GROUP BY local");
		return $response;
	}
	
	public function index(){
		$response = new stdClass;
		$response->status = 200;
		$response->tags = $this->tags();
		$response->defectos = $this->defectos();
		$response->clusterGrado = $this->clusterGrado();
		$response->porCategoria = $this->porCategoria();
		$response->participacionMercado = $this->participacionMercado();
		$response->fotos = $this->fotos();
		return $response;
	}

	private function participacionMercado(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		if($this->postdata->cliente != ''){
			$sWhere .= " AND cliente = '{$this->postdata->cliente}'";
		}
		if($this->postdata->sucursal != ''){
			$sWhere .= " AND local = '{$this->postdata->sucursal}'";
		}

		$sql = "SELECT *
				FROM (
					SELECT '% Lancofruit' as participante, SUM(porc_fruta_lancofruit)/SUM(porc_fruta_lancofruit+porc_fruta_terceros+porc_percha_vacia)*100 porcentaje FROM lancofruit_perchas WHERE 1=1 $sWhere
					UNION ALL
					SELECT '% Terceros' as participante, SUM(porc_fruta_terceros)/SUM(porc_fruta_lancofruit+porc_fruta_terceros+porc_percha_vacia)*100 porcentaje FROM lancofruit_perchas WHERE 1=1 $sWhere
					UNION ALL
					SELECT '% Vacia' as participante, SUM(porc_percha_vacia)/SUM(porc_fruta_lancofruit+porc_fruta_terceros+porc_percha_vacia)*100 porcentaje FROM lancofruit_perchas WHERE 1=1 $sWhere
				) tbl";
		$response->data = $this->db->queryAll($sql);

		return $response;
	}

	private function fotos(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		if($this->postdata->cliente != ''){
			$sWhere .= " AND cliente = '{$this->postdata->cliente}'";
		}
		if($this->postdata->sucursal != ''){
			$sWhere .= " AND local = '{$this->postdata->sucursal}'";
		}

		$sql = "SELECT id_percha, pagina AS type, url AS path, fecha, observaciones
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_images ON id_percha = percha.id
				WHERE 1=1 {$sWhere}
				ORDER BY percha.id";
		$response->data = $this->db->queryAll($sql);
		return $response;
	}

	private function porCategoria(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		if($this->postdata->cliente != ''){
			$sWhere .= " AND cliente = '{$this->postdata->cliente}'";
		}
		if($this->postdata->sucursal != ''){
			$sWhere .= " AND local = '{$this->postdata->sucursal}'";
		}

		$response->data = [];

		$sql = "SELECT categoria
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
				WHERE cantidad > 0 {$sWhere}
				GROUP BY categoria";
		$response->categorias = $this->db->queryAllOne($sql);
		foreach($response->categorias as $cat){
			$sql = "SELECT defecto, SUM(cantidad) cantidad
					FROM lancofruit_perchas percha
					INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
					WHERE cantidad > 0 AND categoria = '{$cat}' {$sWhere}
					GROUP BY defecto";
			$response->data[$cat] = $this->db->queryAll($sql);
		}
		return $response;
	}

	private function tags(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		if($this->postdata->cliente != ''){
			$sWhere .= " AND cliente = '{$this->postdata->cliente}'";
		}
		if($this->postdata->sucursal != ''){
			$sWhere .= " AND local = '{$this->postdata->sucursal}'";
		}

		$response->gavetas_recibidas = (float) $this->db->queryOne("SELECT ROUND(SUM(gavetas), 2) FROM lancofruit_perchas WHERE 1=1 $sWhere");
		$response->kilos_totales = (float) $this->db->queryOne("SELECT ROUND(SUM(kilos_totales), 2) FROM lancofruit_perchas WHERE 1=1 $sWhere");
		$response->kilos_destruidos = (float) $this->db->queryOne("SELECT ROUND(SUM(kilos_destruidos), 2) FROM lancofruit_perchas WHERE 1=1 $sWhere");
		$sql = "SELECT ROUND(AVG(temp), 2)
				FROM (
					SELECT temp_bodega_1 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_bodega_1 > 0
					UNION ALL 
					SELECT temp_bodega_2 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_bodega_2 > 0
					UNION ALL 
					SELECT temp_bodega_3 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_bodega_3 > 0
					UNION ALL 
					SELECT temp_bodega_4 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_bodega_4 > 0
					UNION ALL 
					SELECT temp_bodega_5 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_bodega_5 > 0
				) tbl";
		$response->temperatura_bodega = (float) $this->db->queryOne($sql);
		$sql = "SELECT ROUND(AVG(temp), 2)
				FROM (
					SELECT temp_percha_1_1 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_1_1 > 0
					UNION ALL 
					SELECT temp_percha_1_2 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_1_2 > 0
					UNION ALL 
					SELECT temp_percha_1_3 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_1_3 > 0
					UNION ALL 
					SELECT temp_percha_1_4 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_1_4 > 0
					UNION ALL 
					SELECT temp_percha_1_5 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_1_5 > 0
				) tbl";
		$response->temperatura_percha_1 = (float) $this->db->queryOne($sql);
		$sql = "SELECT ROUND(AVG(temp), 2)
				FROM (
					SELECT temp_percha_2_1 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_2_1 > 0
					UNION ALL 
					SELECT temp_percha_2_2 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_2_2 > 0
					UNION ALL 
					SELECT temp_percha_2_3 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_2_3 > 0
					UNION ALL 
					SELECT temp_percha_2_4 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_2_4 > 0
					UNION ALL 
					SELECT temp_percha_2_5 temp FROM lancofruit_perchas WHERE 1=1 $sWhere AND temp_percha_2_5 > 0
				) tbl";
		$response->temperatura_percha_2 = (float) $this->db->queryOne($sql);
		return $response;
	}

	private function defectos(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		if($this->postdata->cliente != ''){
			$sWhere .= " AND cliente = '{$this->postdata->cliente}'";
		}
		if($this->postdata->sucursal != ''){
			$sWhere .= " AND local = '{$this->postdata->sucursal}'";
		}

		// DATA
		$sql = "SELECT percha.*
                FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos d ON id_percha = percha.id AND d.cantidad > 0
				WHERE 1=1 {$sWhere}
				GROUP BY percha.id
				ORDER BY percha.id";
		$response->data = $this->db->queryAll($sql);

		foreach($response->data as $row){
			$sql = "SELECT categoria AS type, defecto AS campo, SUM(cantidad) cantidad
					FROM lancofruit_perchas percha
					INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
					WHERE percha.id = $row->id {$sWhere} AND cantidad > 0
					GROUP BY categoria, defecto";
			$defectos = $this->db->queryAll($sql);
			$_defectos = [];

			$row->total_defectos = 0;
			foreach($defectos as $d){
				$siglas = substr($d->campo, 0, 2);
				$_defectos[$d->type.'_'.trim($siglas)] = $d->cantidad;
				$row->total_defectos += (int) $d->cantidad;
			}
			$row->defectos = $_defectos;
		}

		// CATEGORIAS
		$sql = "SELECT categoria AS type
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
				WHERE 1=1 {$sWhere}
				GROUP BY categoria";
		$response->categorias = $this->db->queryAll($sql);
		foreach($response->categorias as $categoria){
			$sql = "SELECT defecto AS campo
					FROM lancofruit_perchas percha
					INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
					WHERE categoria = '{$categoria->type}' {$sWhere} AND cantidad > 0
					GROUP BY defecto";
			$categoria->defectos = $this->db->queryAll($sql);
			foreach($categoria->defectos as $defecto){
				$defecto->siglas = trim(substr($defecto->campo, 0, 2));
				$defecto->descripcion = trim(substr($defecto->campo, 3, strlen($defecto->campo)-3));
			}
		}

		return $response;
	}

    public function clusterGrado(){
        $response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}
		if($this->postdata->cliente != ''){
			$sWhere .= " AND cliente = '{$this->postdata->cliente}'";
		}
		if($this->postdata->sucursal != ''){
			$sWhere .= " AND local = '{$this->postdata->sucursal}'";
		}

		$sql = "SELECT grado AS label, SUM(cantidad) value
				FROM (
					SELECT 'Grado 1' as grado, grado_1_cantidad_cluster AS cantidad FROM lancofruit_perchas WHERE grado_1_cantidad_cluster > 0 $sWhere
					UNION ALL
					SELECT 'Grado 2' as grado, grado_2_cantidad_cluster AS cantidad FROM lancofruit_perchas WHERE grado_2_cantidad_cluster > 0 $sWhere
					UNION ALL
					SELECT 'Grado 3' as grado, grado_3_cantidad_cluster AS cantidad FROM lancofruit_perchas WHERE grado_3_cantidad_cluster > 0 $sWhere
					UNION ALL
					SELECT 'Grado 4' as grado, grado_4_cantidad_cluster AS cantidad FROM lancofruit_perchas WHERE grado_4_cantidad_cluster > 0 $sWhere
					UNION ALL
					SELECT 'Grado 5' as grado, grado_5_cantidad_cluster AS cantidad FROM lancofruit_perchas WHERE grado_5_cantidad_cluster > 0 $sWhere
					UNION ALL
					SELECT 'Grado 6' as grado, grado_6_cantidad_cluster AS cantidad FROM lancofruit_perchas WHERE grado_6_cantidad_cluster > 0 $sWhere
				) tbl
				GROUP BY grado";
        $response->data = $this->db->queryAll($sql);
        return $response;
    }
}