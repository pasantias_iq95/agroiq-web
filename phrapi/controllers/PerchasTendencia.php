<?php defined('PHRAPI') or die("Direct access not allowed!");

class PerchasTendencia {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->session = Session::getInstance();
		$this->db = DB::getInstance($this->session->agent_user);
		$this->postdata = (object)json_decode(file_get_contents("php://input"));

		$this->availableClientes = '';
		if(in_array($this->session->logged, [36, 50, 51]))
			$this->availableClientes = "'". implode(['Tia'], "','") . "'";
		if($this->session->logged == 0)
			$this->availableClientes = "'". implode(['Mi Comisariato'], "','") . "'";
    }
    
    public function last(){
		$response = new stdClass;

		$sWhere = "";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$response->anio = $this->db->queryOne("SELECT MAX(anio) FROM lancofruit_perchas WHERE 1=1 $sWhere");
		$response->clientes = $this->db->queryAllOne("SELECT cliente FROM lancofruit_perchas WHERE anio = {$response->anio} $sWhere GROUP BY cliente");
		$response->sucursales = [];
		foreach($response->clientes as $c){
			$response->sucursales[$c] = $this->db->queryAllOne("SELECT local FROM lancofruit_perchas WHERE anio = {$response->anio} AND cliente = '{$c}' GROUP BY local");
		}
        return $response;
	}

	public function index(){
		$response = new stdClass;

		$response->tabla = $this->tabla();
		$response->grafica = $this->grafica();

		$sql = "SELECT categoria, defecto
				FROM lancofruit_perchas percha
				INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
				WHERE cliente = '{$this->postdata->cliente}' AND anio = {$this->postdata->anio} AND cantidad > 0
				GROUP BY categoria, defecto";
		$response->defectos = $this->db->queryAll($sql);

		switch($this->postdata->var){
			case 'defecto':
				$sql = "SELECT local
						FROM lancofruit_perchas percha 
						INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
						WHERE cliente = '{$this->postdata->cliente}' AND anio = {$this->postdata->anio} AND cantidad > 0 AND defecto = '{$this->postdata->defecto->defecto}' AND categoria = '{$this->postdata->defecto->categoria}'
						GROUP BY local";
				$response->sucursales_available = $this->db->queryAllOne($sql);
				break;
			case 'total_defectos':
				$sql = "SELECT local 
						FROM lancofruit_perchas percha 
						INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
						WHERE cliente = '{$this->postdata->cliente}' AND anio = {$this->postdata->anio} AND cantidad > 0 
						GROUP BY local";
				$response->sucursales_available = $this->db->queryAllOne($sql);
				break;
			case 'temp_bodega':
				$sql = "SELECT local 
						FROM lancofruit_perchas percha 
						WHERE cliente = '{$this->postdata->cliente}' AND anio = {$this->postdata->anio} AND (
							temp_bodega_1 > 0 OR 
							temp_bodega_2 > 0 OR 
							temp_bodega_3 > 0 OR 
							temp_bodega_4 > 0 OR 
							temp_bodega_5 > 0
						)
						GROUP BY local";
				$response->sucursales_available = $this->db->queryAllOne($sql);
				break;
			case 'temp_percha_1':
				$sql = "SELECT local 
						FROM lancofruit_perchas percha 
						WHERE cliente = '{$this->postdata->cliente}' AND anio = {$this->postdata->anio} AND (
							temp_percha_1_1 > 0 OR 
							temp_percha_1_2 > 0 OR 
							temp_percha_1_3 > 0 OR 
							temp_percha_1_4 > 0 OR 
							temp_percha_1_5 > 0
						)
						GROUP BY local";
				$response->sucursales_available = $this->db->queryAllOne($sql);
				break;
			case 'temp_percha_2':
				$sql = "SELECT local 
						FROM lancofruit_perchas percha 
						WHERE cliente = '{$this->postdata->cliente}' AND anio = {$this->postdata->anio} AND (
							temp_percha_2_1 > 0 OR 
							temp_percha_2_2 > 0 OR 
							temp_percha_2_3 > 0 OR 
							temp_percha_2_4 > 0 OR 
							temp_percha_2_5 > 0
						)
						GROUP BY local";
				$response->sucursales_available = $this->db->queryAllOne($sql);
				break;
			default :
				$sql = "SELECT local FROM lancofruit_perchas WHERE cliente = '{$this->postdata->cliente}' AND anio = {$this->postdata->anio} AND {$this->postdata->var} > 0 AND {$this->postdata->var} IS NOT NULL GROUP BY local";
				$response->sucursales_available = $this->db->queryAllOne($sql);
				break;
		}

		return $response;
	}

	public function grafica(){
		$response = new stdClass;

		$sWhere = "";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$sql = "SELECT {$this->postdata->period}
				FROM lancofruit_perchas
				WHERE anio = {$this->postdata->anio} {$sWhere}
				GROUP BY {$this->postdata->period}
				ORDER BY {$this->postdata->period}";
		$response->legends = $this->db->queryAllOne($sql);

		$sSucursales = "";
		if(isset($this->postdata->sucursales) && count($this->postdata->sucursales) > 0){
			$sSucursales .= " AND local IN ('".implode($this->postdata->sucursales, "', '")."')";
		}

		$sql = "SELECT *
				FROM (
					SELECT cliente as detalle, 'cliente' mode
					FROM lancofruit_perchas
					WHERE anio = {$this->postdata->anio} AND cliente = '{$this->postdata->cliente}' {$sWhere}
					GROUP BY cliente
					".
					($sSucursales != '' ? (
					"UNION ALL
					SELECT local as detalle, 'local' mode
					FROM lancofruit_perchas
					WHERE anio = {$this->postdata->anio} AND cliente = '{$this->postdata->cliente}' {$sSucursales} {$sWhere}
					GROUP BY local
					") : '').
					"
				) tbl";
		$zonas = $this->db->queryAll($sql);

		$response->series = [];
		foreach($zonas as $zona){
			$response->series[] = [
                "name" => $zona->detalle,
				"connectNulls" => true,
				"min" => 'dataMin',
				"type" => "line",
				"label" => [
					"normal" => [
						"show" => false,
						"position" => "inside"
					]
				],
				"data" => [],
			];
			
			foreach($response->legends as $sem){
				switch($this->postdata->var){
					case 'defecto':
						$sql = "SELECT SUM(cantidad)
								FROM lancofruit_perchas percha 
								INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
								WHERE cliente = '{$this->postdata->cliente}' AND anio = {$this->postdata->anio} AND cantidad > 0 AND defecto = '{$this->postdata->defecto->defecto}' AND categoria = '{$this->postdata->defecto->categoria}' AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = $this->db->queryOne($sql);
						break;
					case 'total_defectos':
						$sql = "SELECT ROUND(SUM(cantidad), 2)
								FROM lancofruit_perchas percha
								INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
					case 'temp_bodega':
						$sql = "SELECT ROUND(AVG(
									(
										IFNULL(temp_bodega_1, 0) + 
										IFNULL(temp_bodega_2, 0) + 
										IFNULL(temp_bodega_3, 0) + 
										IFNULL(temp_bodega_4, 0) + 
										IFNULL(temp_bodega_5, 0))/ 
									(
										IF(temp_bodega_1 IS NOT NULL AND temp_bodega_1 > 0, 1, 0) + 
										IF(temp_bodega_2 IS NOT NULL AND temp_bodega_2 > 0, 1, 0) + 
										IF(temp_bodega_3 IS NOT NULL AND temp_bodega_3 > 0, 1, 0) + 
										IF(temp_bodega_4 IS NOT NULL AND temp_bodega_4 > 0, 1, 0) + 
										IF(temp_bodega_5 IS NOT NULL AND temp_bodega_5 > 0, 1, 0)
									)
								), 2) 
								FROM lancofruit_perchas
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
					case 'temp_percha_1':
						$sql = "SELECT ROUND(AVG(
									(
										IFNULL(temp_percha_1_1, 0) + 
										IFNULL(temp_percha_1_2, 0) + 
										IFNULL(temp_percha_1_3, 0) + 
										IFNULL(temp_percha_1_4, 0) + 
										IFNULL(temp_percha_1_5, 0)
									)/ 
									(
										IF(temp_percha_1_1 IS NOT NULL AND temp_percha_1_1 > 0, 1, 0) + 
										IF(temp_percha_1_2 IS NOT NULL AND temp_percha_1_2 > 0, 1, 0) + 
										IF(temp_percha_1_3 IS NOT NULL AND temp_percha_1_3 > 0, 1, 0) + 
										IF(temp_percha_1_4 IS NOT NULL AND temp_percha_1_4 > 0, 1, 0) + 
										IF(temp_percha_1_5 IS NOT NULL AND temp_percha_1_5 > 0, 1, 0)
									)
								), 2) 
								FROM lancofruit_perchas
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
					case 'temp_percha_2':
						$sql = "SELECT ROUND(AVG(
									(
										IFNULL(temp_percha_2_1, 0) + 
										IFNULL(temp_percha_2_2, 0) + 
										IFNULL(temp_percha_2_3, 0) + 
										IFNULL(temp_percha_2_4, 0) + 
										IFNULL(temp_percha_2_5, 0)
									)/ 
									(
										IF(temp_percha_2_1 IS NOT NULL AND temp_percha_2_1 > 0, 1, 0) + 
										IF(temp_percha_2_2 IS NOT NULL AND temp_percha_2_2 > 0, 1, 0) + 
										IF(temp_percha_2_3 IS NOT NULL AND temp_percha_2_3 > 0, 1, 0) + 
										IF(temp_percha_2_4 IS NOT NULL AND temp_percha_2_4 > 0, 1, 0) + 
										IF(temp_percha_2_5 IS NOT NULL AND temp_percha_2_5 > 0, 1, 0))
								), 2) 
								FROM lancofruit_perchas
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
					default : 
						$sql = "SELECT ROUND(AVG({$this->postdata->var}), 2)
								FROM lancofruit_perchas
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
				}
				$response->series[count($response->series)-1]["data"][] = $val > 0 ? round($val, 2) : null;
			}
		}

		return $response;
	}

	public function tabla(){
		$response = new stdClass;

		$sWhere = "";
		if($this->availableClientes != ''){
			$sWhere .= " AND cliente IN ({$this->availableClientes})";
		}

		$sql = "SELECT {$this->postdata->period}
				FROM lancofruit_perchas
				WHERE anio = {$this->postdata->anio} {$sWhere}
				GROUP BY {$this->postdata->period}
				ORDER BY {$this->postdata->period}";
		$response->semanas = $this->db->queryAllOne($sql);

		$sSucursales = "";
		if(isset($this->postdata->sucursales) && count($this->postdata->sucursales) > 0){
			$sSucursales .= " AND local IN ('".implode($this->postdata->sucursales, "', '")."')";
		}

		$sql = "SELECT *
				FROM (
					SELECT cliente as detalle, 'cliente' mode
					FROM lancofruit_perchas
					WHERE anio = {$this->postdata->anio} AND cliente = '{$this->postdata->cliente}' {$sWhere}
					GROUP BY cliente
					".
					($sSucursales != '' ? (
					"UNION ALL
					SELECT local as detalle, 'local' mode
					FROM lancofruit_perchas
					WHERE anio = {$this->postdata->anio} AND cliente = '{$this->postdata->cliente}' {$sSucursales} {$sWhere}
					GROUP BY local
					") : '').
					"
				) tbl";
		$zonas = $this->db->queryAll($sql);

		foreach($zonas as $zona){

			$sum = 0;
			$count = 0;
			$max = null;
			$min = null;

			foreach($response->semanas as $sem){
				switch($this->postdata->var){
					case 'defecto':
						$sql = "SELECT SUM(cantidad)
								FROM lancofruit_perchas percha 
								INNER JOIN lancofruit_perchas_defectos ON percha.id = id_percha
								WHERE cliente = '{$this->postdata->cliente}' AND anio = {$this->postdata->anio} AND cantidad > 0 AND defecto = '{$this->postdata->defecto->defecto}' AND categoria = '{$this->postdata->defecto->categoria}' AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = $this->db->queryOne($sql);
						break;
					case 'total_defectos':
						$sql = "SELECT ROUND(SUM(cantidad), 2)
								FROM lancofruit_perchas percha
								INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
					case 'temp_bodega':
						$sql = "SELECT ROUND(AVG(
									(
										IFNULL(temp_bodega_1, 0) + 
										IFNULL(temp_bodega_2, 0) + 
										IFNULL(temp_bodega_3, 0) + 
										IFNULL(temp_bodega_4, 0) + 
										IFNULL(temp_bodega_5, 0))/ 
									(
										IF(temp_bodega_1 IS NOT NULL AND temp_bodega_1 > 0, 1, 0) + 
										IF(temp_bodega_2 IS NOT NULL AND temp_bodega_2 > 0, 1, 0) + 
										IF(temp_bodega_3 IS NOT NULL AND temp_bodega_3 > 0, 1, 0) + 
										IF(temp_bodega_4 IS NOT NULL AND temp_bodega_4 > 0, 1, 0) + 
										IF(temp_bodega_5 IS NOT NULL AND temp_bodega_5 > 0, 1, 0)
									)
								), 2) 
								FROM lancofruit_perchas
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
					case 'temp_percha_1':
						$sql = "SELECT ROUND(AVG(
									(
										IFNULL(temp_percha_1_1, 0) + 
										IFNULL(temp_percha_1_2, 0) + 
										IFNULL(temp_percha_1_3, 0) + 
										IFNULL(temp_percha_1_4, 0) + 
										IFNULL(temp_percha_1_5, 0)
									)/ 
									(
										IF(temp_percha_1_1 IS NOT NULL AND temp_percha_1_1 > 0, 1, 0) + 
										IF(temp_percha_1_2 IS NOT NULL AND temp_percha_1_2 > 0, 1, 0) + 
										IF(temp_percha_1_3 IS NOT NULL AND temp_percha_1_3 > 0, 1, 0) + 
										IF(temp_percha_1_4 IS NOT NULL AND temp_percha_1_4 > 0, 1, 0) + 
										IF(temp_percha_1_5 IS NOT NULL AND temp_percha_1_5 > 0, 1, 0)
									)
								), 2) 
								FROM lancofruit_perchas
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
					case 'temp_percha_2':
						$sql = "SELECT ROUND(AVG(
									(
										IFNULL(temp_percha_2_1, 0) + 
										IFNULL(temp_percha_2_2, 0) + 
										IFNULL(temp_percha_2_3, 0) + 
										IFNULL(temp_percha_2_4, 0) + 
										IFNULL(temp_percha_2_5, 0)
									)/ 
									(
										IF(temp_percha_2_1 IS NOT NULL AND temp_percha_2_1 > 0, 1, 0) + 
										IF(temp_percha_2_2 IS NOT NULL AND temp_percha_2_2 > 0, 1, 0) + 
										IF(temp_percha_2_3 IS NOT NULL AND temp_percha_2_3 > 0, 1, 0) + 
										IF(temp_percha_2_4 IS NOT NULL AND temp_percha_2_4 > 0, 1, 0) + 
										IF(temp_percha_2_5 IS NOT NULL AND temp_percha_2_5 > 0, 1, 0))
								), 2) 
								FROM lancofruit_perchas
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
					default : 
						$sql = "SELECT ROUND(AVG({$this->postdata->var}), 2)
								FROM lancofruit_perchas
								WHERE anio = {$this->postdata->anio} AND {$this->postdata->period} = {$sem} AND {$zona->mode} = '{$zona->detalle}' {$sWhere}";
						$val = (float) $this->db->queryOne($sql);
						break;
				}
				$zona->{"sem_{$sem}"} = $val > 0 ? number_format($val, 2) : '';

				if($val){
					$sum += $val;
					$count++;
					if($max == null || $val > $max) $max = $val;
					if($min == null || $val < $min) $min = $val;
				}
			}

			$zona->max = $max ? $max : '';
			$zona->min = $min ? $min : '';
			$zona->prom = round($sum/$count, 2);
		}

		$response->data = $zonas;
		return $response;
	}
}