<?php defined('PHRAPI') or die("Direct access not allowed!");

class Produccion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}


	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		$sWhere = "";

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$response = new stdClass;

		$sql = "SELECT lote , 
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					'false' AS expanded,
					(SELECT COUNT(*) FROM produccion_muestra WHERE lote = produccion.lote {$sWhere}) AS muestrados,
					(SELECT AVG(peso) FROM produccion_muestra WHERE lote = produccion.lote {$sWhere}) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE lote = produccion.lote {$sWhere}) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_muestra WHERE lote = produccion.lote {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_muestra WHERE lote = produccion.lote {$sWhere}) AS manos
				FROM produccion
				WHERE 1 = 1 {$sWhere}
				GROUP BY lote";

		$response->data = $this->db->queryAll($sql);

		foreach ($response->data as $key => $value) {
			$value = (object)$value;
			$value->expanded = false;
			$value->muestrados = (int)$value->muestrados;
			$value->peso = (int)$value->peso;
			$value->largo_dedos = (int)$value->largo_dedos;
			$value->calibracion = (int)$value->calibracion;
			$value->manos = (int)$value->manos;
			$value->cables = $this->db->queryAll("SELECT lote , cable, 
							SUM(total_cosechados) AS total_cosechados , 
							SUM(total_recusados) AS total_recusados,
							(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
							'false' AS expanded,
							(SELECT COUNT(*) FROM produccion_muestra WHERE lote = '{$value->lote}' AND cable = produccion.cable {$sWhere}) AS muestrados,
							(SELECT AVG(peso) FROM produccion_muestra WHERE lote = '{$value->lote}' AND cable = produccion.cable {$sWhere}) AS peso,
							(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE lote = '{$value->lote}' AND cable = produccion.cable {$sWhere}) AS largo_dedos,
							(SELECT AVG(calibracion) FROM produccion_muestra WHERE lote = '{$value->lote}' AND cable = produccion.cable {$sWhere}) AS calibracion,
							(SELECT AVG(manos) FROM produccion_muestra WHERE lote = '{$value->lote}' AND cable = produccion.cable {$sWhere}) AS manos
							FROM produccion
							WHERE 1 = 1 {$sWhere} AND lote = '{$value->lote}'
							GROUP BY lote , cable");
			foreach ($value->cables as $llave => $valor) {
					$valor->expanded = false;
					$valor->muestrados = (float)$valor->muestrados;
					$valor->peso = (float)$valor->peso;
					$valor->largo_dedos = (float)$valor->largo_dedos;
					$valor->calibracion = (float)$valor->calibracion;
					$valor->manos = (float)$valor->manos;
					$valor->cintas = $this->getCintas($value->lote , $valor->cable , $filters);
			}
		}

		$response->totales = $this->db->queryAll("SELECT finca , 
				SUM(total_cosechados) AS total_cosechados , 
				SUM(total_recusados) AS total_recusados,
				(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
				(SELECT COUNT(*) FROM produccion_muestra WHERE 1 = 1 {$sWhere}) AS muestrados,
				((SELECT COUNT(*) FROM produccion_muestra WHERE 1 = 1 {$sWhere}) / SUM(total_cosechados)) * 100 AS porcen,
				(SELECT AVG(peso) FROM produccion_muestra WHERE 1 = 1 {$sWhere}) AS peso,
				(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE 1 = 1 {$sWhere}) AS largo_dedos,
				(SELECT AVG(calibracion) FROM produccion_muestra WHERE 1 = 1 {$sWhere}) AS calibracion,
				(SELECT AVG(manos) FROM produccion_muestra WHERE 1 = 1 {$sWhere}) AS manos
				FROM produccion
				WHERE 1 = 1 {$sWhere}");

		$response->colores = $this->getColors($filters);
		$response->palancas = $this->getPalancas($filters);
		$response->id_company = $this->session->id_company;
		return $response;
	}

	private function getPalancas($filters){
		$sWhere = "";
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}


		$response = new stdClass;
		$sql = "SELECT palanca ,lote,cable, 
					SUM(total_cosechados) AS total_cosechados , 
					SUM(total_recusados) AS total_recusados,
					(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
					(SELECT COUNT(*) FROM produccion_muestra WHERE palanca = produccion.palanca {$sWhere}) AS muestrados,
					(SELECT AVG(peso) FROM produccion_muestra WHERE palanca = produccion.palanca {$sWhere}) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE palanca = produccion.palanca {$sWhere}) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_muestra WHERE palanca = produccion.palanca {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_muestra WHERE palanca = produccion.palanca {$sWhere}) AS manos
				FROM produccion
				WHERE 1 = 1 AND palanca != '' {$sWhere}
				GROUP BY palanca";
		// D($sql);
		$data = $this->db->queryAll($sql);
		$response->data = [];
		foreach ($data as $key => $value) {
			$value = (object)$value;
			$response->data[] = [
				"palanca" => $value->palanca,
				"lote" => $value->lote,
				"cable" => $value->cable,
				"total_cosechados" => $value->total_cosechados,
				"total_recusados" => $value->total_recusados,
				"total_procesada" => $value->total_procesada,
				"muestrados" => (float)$value->muestrados,
				"peso" => (float)$value->peso,
				"largo_dedos" => (float)$value->largo_dedos,
				"calibracion" => (float)$value->calibracion,
				"manos" => (float)$value->manos,
				"expanded" => false,
				"lotes" => $this->getLotes($filters , $value->palanca)
			];
		}

		return $response->data;
	}

	private function getLotes($filters = [] , $palanca = ""){
		$sWhere = "";
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$response = new stdClass;

		$data = $this->db->queryAll("SELECT palanca ,lote,cable, 
				SUM(total_cosechados) AS total_cosechados , 
				SUM(total_recusados) AS total_recusados,
				(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
				(SELECT COUNT(*) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = produccion.lote {$sWhere}) AS muestrados,
				(SELECT AVG(peso) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = produccion.lote {$sWhere}) AS peso,
				(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = produccion.lote {$sWhere}) AS largo_dedos,
				(SELECT AVG(calibracion) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = produccion.lote {$sWhere}) AS calibracion,
				(SELECT AVG(manos) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = produccion.lote {$sWhere}) AS manos
				FROM produccion
				WHERE 1 = 1 AND palanca = '{$palanca}' {$sWhere} 
				GROUP BY lote");
		foreach ($data as $key => $value) {
			$value = (object)$value;
			$response->data[] = [
				"lote" => $value->lote,
				"total_cosechados" => (int)$value->total_cosechados,
				"total_recusados" => (int)$value->total_recusados,
				"total_procesada" => (int)$value->total_procesada,
				"expanded" => false,
				"muestrados" => (float)$value->muestrados,
				"peso" => (float)$value->peso,
				"largo_dedos" => (float)$value->largo_dedos,
				"calibracion" => (float)$value->calibracion,
				"manos" => (float)$value->manos,
				"cables" => $this->getCablePalancas($filters , $palanca , $value->lote),
			];
		}

		return $response->data;
	}

	private function getCablePalancas($filters  = "", $palanca = "" ,$lote = ""){
		$sWhere = "";
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}
		$response = new stdClass;

		$sql = "SELECT palanca ,lote,cable, 
				SUM(total_cosechados) AS total_cosechados , 
				SUM(total_recusados) AS total_recusados,
				(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
				(SELECT COUNT(*) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = '$lote' AND cable = produccion.cable {$sWhere}) AS muestrados,
				(SELECT AVG(peso) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = '$lote' AND cable = produccion.cable {$sWhere}) AS peso,
				(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = '$lote' AND cable = produccion.cable {$sWhere}) AS largo_dedos,
				(SELECT AVG(calibracion) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = '$lote' AND cable = produccion.cable {$sWhere}) AS calibracion,
				(SELECT AVG(manos) FROM produccion_muestra WHERE palanca = '{$palanca}' AND lote = '$lote' AND cable = produccion.cable {$sWhere}) AS manos
				FROM produccion
				WHERE 1 = 1 AND palanca = '{$palanca}' AND lote = '$lote' {$sWhere} 
				GROUP BY lote";
		$data = $this->db->queryAll($sql);
		foreach ($data as $key => $value) {
			$value = (object)$value;
			$response->data[] = [
				"cable" => $value->cable,
				"total_cosechados" => (int)$value->total_cosechados,
				"total_recusados" => (int)$value->total_recusados,
				"total_procesada" => (int)$value->total_procesada,
				"muestrados" => (float)$value->muestrados,
				"peso" => (float)$value->peso,
				"largo_dedos" => (float)$value->largo_dedos,
				"calibracion" => (float)$value->calibracion,
				"manos" => (float)$value->manos,
			];
		}

		return $response->data;
	}

	private function getColors($filters){
		$color = [
			"blanco" => "bg-grey-cararra bg-font-grey-cararra",
			"negro" => "bg-dark bg-font-dark",
			"lila" => "bg-purple bg-font-purple",
			"rojo" => "bg-red-thunderbird bg-font-red-thunderbird",
			"cafe" => "bg-yellow bg-font-yellow",
			"amarillo" => "bg-yellow-lemon bg-font-yellow-lemon",
			"verde" => "bg-green-jungle bg-font-green-jungle",
			"azul" => "bg-blue bg-font-blue",
		];
		
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}
		$sql = "SELECT 
				lote,cable,
				SUM(cosechados_blanco) AS cosechados_blanco,
				SUM(cosechados_negro) AS cosechados_negro,
				SUM(cosechados_lila) AS cosechados_lila,
				SUM(cosechados_rojo) AS cosechados_rojo,
				SUM(cosechados_cafe) AS cosechados_cafe,
				SUM(cosechados_amarillo) AS cosechados_amarillo,
				SUM(cosechados_verde) AS cosechados_verde,
				SUM(cosechados_azul) AS cosechados_azul,
				SUM(recusados_blanco) AS recusados_blanco,
				SUM(recusados_negro) AS recusados_negro,
				SUM(recusados_lila) AS recusados_lila,
				SUM(recusados_rojo) AS recusados_rojo,
				SUM(recusados_cafe) AS recusados_cafe,
				SUM(recusados_amarillo) AS recusados_amarillo,
				SUM(recusados_verde) AS recusados_verde,
				SUM(recusados_azul) AS recusados_azul,
				(SUM(cosechados_blanco) - SUM(recusados_blanco)) AS blanco,
				(SUM(cosechados_negro) - SUM(recusados_negro)) AS negro,
				(SUM(cosechados_lila) - SUM(recusados_lila)) AS lila,
				(SUM(cosechados_rojo) - SUM(recusados_rojo)) AS rojo,
				(SUM(cosechados_cafe) - SUM(recusados_cafe)) AS cafe,
				(SUM(cosechados_amarillo) - SUM(recusados_amarillo)) AS amarillo,
				(SUM(cosechados_verde) - SUM(recusados_verde)) AS verde,
				(SUM(cosechados_azul) - SUM(recusados_azul)) AS azul
				FROM produccion 
				WHERE 1 = 1 {$sWhere}";
				$data = $this->db->queryRow($sql);

		$response = new stdClass;
		$response->data = [];
		$nombre = "";
		$assign = 0;

		$yWhere = "AND lote IN((SELECT lote FROM produccion WHERE 1 = 1{$sWhere}))";

		foreach ($color as $key => $value) {
			$nombre = $key;
			if($key == "cafe"){
				$nombre = "café";
			}
			$assign = 0;
			if((int)$data->{"cosechados_".$key} > 0){
				$assign++;
			}
			if((int)$data->{"recusados_".$key} > 0){
				$assign++;
			}
			if((int)$data->$key > 0){
				$assign++;
			}
			if($assign > 0){
				$response->data[$key] = [
					"nombre" => $nombre,
					"color" => $value,
					"cosechados" => $data->{"cosechados_".$key},
					"recusados" => $data->{"recusados_".$key},
					"procesados" => $data->$key,
					"expanded" => false,
					"muestrados" => (float)$this->db->queryOne("SELECT COUNT(*) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' {$yWhere} {$sWhere}"),
					"peso" => (float)$this->db->queryOne("SELECT AVG(peso) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' {$yWhere} {$sWhere}"),
					"largo_dedos" => (float)$this->db->queryOne("SELECT AVG(largo_dedos) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' {$yWhere} {$sWhere}"),
					"calibracion" => (float)$this->db->queryOne("SELECT AVG(calibracion) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' {$yWhere} {$sWhere}"),
					"manos" => (float)$this->db->queryOne("SELECT AVG(manos) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' {$yWhere} {$sWhere}"),
					"lotes" => $this->getColorsLotesCable($filters , $key)
				];
			}
		}

		return $response->data;
	}

	private function getColorsLotesCable($filters , $cinta = "" ){
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$color = [
			"blanco" => "bg-grey-cararra bg-font-grey-cararra",
			"negro" => "bg-dark bg-font-dark",
			"lila" => "bg-purple bg-font-purple",
			"rojo" => "bg-red-thunderbird bg-font-red-thunderbird",
			"cafe" => "bg-yellow bg-font-yellow",
			"amarillo" => "bg-yellow-lemon bg-font-yellow-lemon",
			"verde" => "bg-green-jungle bg-font-green-jungle",
			"azul" => "bg-blue bg-font-blue",
		];

		$sql = "SELECT 
				lote,cable,
				SUM(cosechados_blanco) AS cosechados_blanco,
				SUM(cosechados_negro) AS cosechados_negro,
				SUM(cosechados_lila) AS cosechados_lila,
				SUM(cosechados_rojo) AS cosechados_rojo,
				SUM(cosechados_cafe) AS cosechados_cafe,
				SUM(cosechados_amarillo) AS cosechados_amarillo,
				SUM(cosechados_verde) AS cosechados_verde,
				SUM(cosechados_azul) AS cosechados_azul,
				SUM(recusados_blanco) AS recusados_blanco,
				SUM(recusados_negro) AS recusados_negro,
				SUM(recusados_lila) AS recusados_lila,
				SUM(recusados_rojo) AS recusados_rojo,
				SUM(recusados_cafe) AS recusados_cafe,
				SUM(recusados_amarillo) AS recusados_amarillo,
				SUM(recusados_verde) AS recusados_verde,
				SUM(recusados_azul) AS recusados_azul,
				(SUM(cosechados_blanco) - SUM(recusados_blanco)) AS blanco,
				(SUM(cosechados_negro) - SUM(recusados_negro)) AS negro,
				(SUM(cosechados_lila) - SUM(recusados_lila)) AS lila,
				(SUM(cosechados_rojo) - SUM(recusados_rojo)) AS rojo,
				(SUM(cosechados_cafe) - SUM(recusados_cafe)) AS cafe,
				(SUM(cosechados_amarillo) - SUM(recusados_amarillo)) AS amarillo,
				(SUM(cosechados_verde) - SUM(recusados_verde)) AS verde,
				(SUM(cosechados_azul) - SUM(recusados_azul)) AS azul
				FROM produccion 
				WHERE 1 = 1 {$sWhere}
				GROUP BY lote";
		// D($cinta);
		$data = $this->db->queryAll($sql);
		$assign = 0;
		foreach ($data as $key => $value) {
			$value = (object)$value;
			$assign = 0;
			if((int)$value->{"cosechados_".$cinta} > 0){
				$assign++;
			}
			if((int)$value->{"recusados_".$cinta} > 0){
				$assign++;
			}
			if((int)$value->$cinta > 0){
				$assign++;
			}
			// D($assign);
			if($assign > 0){
				$response->data[$value->lote] = [
					"lote" => $value->lote,
					"cosechados" => $value->{"cosechados_".$cinta},
					"recusados" => $value->{"recusados_".$cinta},
					"procesados" => $value->$cinta,
					"expanded" => false,
					"muestrados" => (float)$this->db->queryOne("SELECT COUNT(*) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$value->lote}' {$sWhere}"),
					"peso" => (float)$this->db->queryOne("SELECT AVG(peso) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$value->lote}' {$sWhere}"),
					"largo_dedos" => (float)$this->db->queryOne("SELECT AVG(largo_dedos) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$value->lote}' {$sWhere}"),
					"calibracion" => (float)$this->db->queryOne("SELECT AVG(calibracion) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$value->lote}' {$sWhere}"),
					"manos" => (float)$this->db->queryOne("SELECT AVG(manos) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$value->lote}' {$sWhere}"),
					"cables" => $this->getColorsCinta($filters , $value->lote, $cinta)
				];
			}
		}

		return $response->data;
	}

	private function getColorsCinta($filters , $lote , $cinta){
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$color = [
			"blanco" => "bg-grey-cararra bg-font-grey-cararra",
			"negro" => "bg-dark bg-font-dark",
			"lila" => "bg-purple bg-font-purple",
			"rojo" => "bg-red-thunderbird bg-font-red-thunderbird",
			"cafe" => "bg-yellow bg-font-yellow",
			"amarillo" => "bg-yellow-lemon bg-font-yellow-lemon",
			"verde" => "bg-green-jungle bg-font-green-jungle",
			"azul" => "bg-blue bg-font-blue",
		];

		$sql = "SELECT 
				lote,cable,
				SUM(cosechados_blanco) AS cosechados_blanco,
				SUM(cosechados_negro) AS cosechados_negro,
				SUM(cosechados_lila) AS cosechados_lila,
				SUM(cosechados_rojo) AS cosechados_rojo,
				SUM(cosechados_cafe) AS cosechados_cafe,
				SUM(cosechados_amarillo) AS cosechados_amarillo,
				SUM(cosechados_verde) AS cosechados_verde,
				SUM(cosechados_azul) AS cosechados_azul,
				SUM(recusados_blanco) AS recusados_blanco,
				SUM(recusados_negro) AS recusados_negro,
				SUM(recusados_lila) AS recusados_lila,
				SUM(recusados_rojo) AS recusados_rojo,
				SUM(recusados_cafe) AS recusados_cafe,
				SUM(recusados_amarillo) AS recusados_amarillo,
				SUM(recusados_verde) AS recusados_verde,
				SUM(recusados_azul) AS recusados_azul,
				(SUM(cosechados_blanco) - SUM(recusados_blanco)) AS blanco,
				(SUM(cosechados_negro) - SUM(recusados_negro)) AS negro,
				(SUM(cosechados_lila) - SUM(recusados_lila)) AS lila,
				(SUM(cosechados_rojo) - SUM(recusados_rojo)) AS rojo,
				(SUM(cosechados_cafe) - SUM(recusados_cafe)) AS cafe,
				(SUM(cosechados_amarillo) - SUM(recusados_amarillo)) AS amarillo,
				(SUM(cosechados_verde) - SUM(recusados_verde)) AS verde,
				(SUM(cosechados_azul) - SUM(recusados_azul)) AS azul
				FROM produccion 
				WHERE 1 = 1 AND lote = '{$lote}' {$sWhere}
				GROUP BY cable";
		// D($cinta);
		$data = $this->db->queryAll($sql);
		$assign = 0;
		foreach ($data as $key => $value) {
			$value = (object)$value;
			$assign = 0;
			if((int)$value->{"cosechados_".$cinta} > 0){
				$assign++;
			}
			if((int)$value->{"recusados_".$cinta} > 0){
				$assign++;
			}
			if((int)$value->$cinta > 0){
				$assign++;
			}
			// D($assign);
			if($assign > 0){
				$response->data[$value->cable] = [
					"cable" => $value->cable,
					"cosechados" => $value->{"cosechados_".$cinta},
					"recusados" => $value->{"recusados_".$cinta},
					"procesados" => $value->$cinta,
					"muestrados" => (float)$this->db->queryOne("SELECT COUNT(*) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$lote}'  AND cable = '{$value->cable}' {$sWhere}"),
					"peso" => (float)$this->db->queryOne("SELECT AVG(peso) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$lote}'  AND cable = '{$value->cable}' {$sWhere}"),
					"largo_dedos" => (float)$this->db->queryOne("SELECT AVG(largo_dedos) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$lote}'  AND cable = '{$value->cable}' {$sWhere}"),
					"calibracion" => (float)$this->db->queryOne("SELECT AVG(calibracion) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$lote}'  AND cable = '{$value->cable}' {$sWhere}"),
					"manos" => (float)$this->db->queryOne("SELECT AVG(manos) FROM produccion_muestra WHERE LOWER(cinta) = '{$cinta}' AND lote = '{$lote}'  AND cable = '{$value->cable}' {$sWhere}"),
					"expanded" => false
				];
			}
		}

		return $response->data;
	}

	private function getCintas($Lote , $cable , $filters){
		$response = new stdClass;
		$response->data = [];
		$sWhere = "";
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}
		if($Lote != "" && $cable != ""){
			$sql = "SELECT 
				lote,cable,
				SUM(cosechados_blanco) AS cosechados_blanco,
				SUM(cosechados_negro) AS cosechados_negro,
				SUM(cosechados_lila) AS cosechados_lila,
				SUM(cosechados_rojo) AS cosechados_rojo,
				SUM(cosechados_cafe) AS cosechados_cafe,
				SUM(cosechados_amarillo) AS cosechados_amarillo,
				SUM(cosechados_verde) AS cosechados_verde,
				SUM(cosechados_azul) AS cosechados_azul,
				SUM(recusados_blanco) AS recusados_blanco,
				SUM(recusados_negro) AS recusados_negro,
				SUM(recusados_lila) AS recusados_lila,
				SUM(recusados_rojo) AS recusados_rojo,
				SUM(recusados_cafe) AS recusados_cafe,
				SUM(recusados_amarillo) AS recusados_amarillo,
				SUM(recusados_verde) AS recusados_verde,
				SUM(recusados_azul) AS recusados_azul,
				(SUM(cosechados_blanco) - SUM(recusados_blanco)) AS blanco,
				(SUM(cosechados_negro) - SUM(recusados_negro)) AS negro,
				(SUM(cosechados_lila) - SUM(recusados_lila)) AS lila,
				(SUM(cosechados_rojo) - SUM(recusados_rojo)) AS rojo,
				(SUM(cosechados_cafe) - SUM(recusados_cafe)) AS cafe,
				(SUM(cosechados_amarillo) - SUM(recusados_amarillo)) AS amarillo,
				(SUM(cosechados_verde) - SUM(recusados_verde)) AS verde,
				(SUM(cosechados_azul) - SUM(recusados_azul)) AS azul
				FROM produccion 
				WHERE 1 = 1 {$sWhere} AND lote = '{$Lote}' AND cable = '{$cable}'";
				$data = $this->db->queryRow($sql);
			$color = [
				"blanco" => "bg-grey-cararra bg-font-grey-cararra",
				"negro" => "bg-dark bg-font-dark",
				"lila" => "bg-purple bg-font-purple",
				"rojo" => "bg-red-thunderbird bg-font-red-thunderbird",
				"cafe" => "bg-yellow bg-font-yellow",
				"amarillo" => "bg-yellow-lemon bg-font-yellow-lemon",
				"verde" => "bg-green-jungle bg-font-green-jungle",
				"azul" => "bg-blue bg-font-blue",
			];
			$nombre = "";
			$assign = 0;
			foreach ($color as $key => $value) {
				$nombre = $key;
				if($key == "cafe"){
					$nombre = "café";
				}
				$assign = 0;
				if((int)$data->{"cosechados_".$key} > 0){
					$assign++;
				}
				if((int)$data->{"recusados_".$key} > 0){
					$assign++;
				}
				if((int)$data->$key > 0){
					$assign++;
				}
				if($assign > 0){
					$response->data[$key] = [
						"nombre" => $nombre,
						"color" => $value,
						"cosechados" => $data->{"cosechados_".$key},
						"recusados" => $data->{"recusados_".$key},
						"expanded" => false,
						"procesados" => $data->$key,
						"muestrados" => (float)$this->db->queryOne("SELECT COUNT(*) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' AND lote = '{$Lote}' AND cable = '{$cable}' {$sWhere}"),
						"peso" => (float)$this->db->queryOne("SELECT AVG(peso) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' AND lote = '{$Lote}' AND cable = '{$cable}' {$sWhere}"),
						"largo_dedos" => (float)$this->db->queryOne("SELECT AVG(largo_dedos) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' AND lote = '{$Lote}' AND cable = '{$cable}' {$sWhere}"),
						"calibracion" => (float)$this->db->queryOne("SELECT AVG(calibracion) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' AND lote = '{$Lote}' AND cable = '{$cable}' {$sWhere}"),
						"manos" => (float)$this->db->queryOne("SELECT AVG(manos) FROM produccion_muestra WHERE LOWER(cinta) = '{$key}' AND lote = '{$Lote}' AND cable = '{$cable}' {$sWhere}"),
					];
				}
			}
		}

		return $response->data;
	}
	
}
