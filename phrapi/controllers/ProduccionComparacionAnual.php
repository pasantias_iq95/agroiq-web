<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionComparacionAnual {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();		
        $this->sigat = DB::getInstance("sigat");
        $this->db = DB::getInstance($this->session->agent_user);
	}

    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
            'variable' => getValueFrom($postdata, 'variable', 'RACIMOS CORTADOS'),
            'id_finca' => getValueFrom($postdata, 'id_finca', 1),
        ];
        return $filters;
	}
	
	public function variables(){
		$response = new stdClass;

		$sql = "SELECT variable FROM produccion_resumen_tabla WHERE anio = 2017 GROUP BY variable ORDER BY orden";
		$response->variables = $this->db->queryAllOne($sql);

		return $response;
	}

    public function grafica(){
        $response = new stdClass;
        $filters = $this->params();

        $year = $this->db->queryOne("SELECT getYear('{$filters->fecha_inicial}')");

		$response->chart = new stdClass;
		$sql = "SELECT semana
				FROM produccion_historica
				WHERE year > 0 AND semana > 0
				GROUP BY semana
				ORDER BY semana";
        $response->chart->legend = $this->db->queryAllOne($sql);

        $sql = "SELECT ROUND(AVG(edad), 2) 
                FROM produccion_historica
                WHERE year = $year";
        $response->chart->umbral = $this->db->queryOne($sql);

		$sql = "SELECT anios.anio, semanas.semana
				FROM (
					SELECT semana
					FROM produccion_historica
					WHERE year > 0 AND semana > 0
					GROUP BY semana
				) semanas
				JOIN (
					SELECT anio
					FROM produccion_resumen_tabla
					WHERE id_finca = 1 AND variable = '{$filters->variable}'
					GROUP BY anio
				) anios
				ORDER BY anios.anio, semanas.semana";
        $data_series = $this->db->queryAll($sql);
        $semanas = [];
        foreach($data_series as $row){
			$sql = "SELECT sem_{$row->semana} FROM produccion_resumen_tabla WHERE anio = {$row->anio} AND variable = '{$filters->variable}'";
			$valor = (float) $this->db->queryOne($sql);
			$row->valor = $valor > 0 ? $valor : null;

            if(!isset($response->chart->data[$row->anio])){
                $response->chart->data[$row->anio] = [
                    "connectNulls" => true,
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => "0",
                            "barBorderWidth" => "6",
                            "label" => [
                                "show" => false,
                                "position" => "insideTop"
                            ]
                        ]
                    ],
                    "type" => "line",
                    "name" => $row->anio,
                    "data" => []
                ];
            }
            $response->chart->data[$row->anio]["data"][] = $row->valor;
        }
        return $response;
    }
}
