<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionWeek {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}


	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"idLote" => getValueFrom($postdata , "idLote" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

		$sWhere = "";

		// if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
		// 	$sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		// }

		if($filters->idLote != "" && (int)$filters->idLote > 0){
			$sWhere .= " AND lote = '{$filters->idLote}'";
		}

		$sYear = " AND YEAR(fecha) = YEAR(CURRENT_DATE)";
		$sYearCampo = "YEAR(fecha) AS year";
		$sSemana = "WEEK(p.fecha)";
		if($this->session->id_company == 2){
			$sYear = " AND year = YEAR(CURRENT_DATE)";
			$sSemana = "semana";
			$sYearCampo = "year";
		}

		$response = new stdClass;

		$sql = "SELECT $sSemana AS semana , $sYearCampo,
				SUM(total_cosechados) AS total_cosechados , 
				SUM(total_recusados) AS total_recusados,
				(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
				'false' AS expanded,
				(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS muestrados,
				(SELECT AVG(peso) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS peso,
				(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS largo_dedos,
				(SELECT AVG(calibracion) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS calibracion,
				(SELECT AVG(manos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear}) AS manos
				FROM produccion AS p
				WHERE 1 = 1 {$sWhere}
				{$sYear}
				GROUP BY $sSemana";

		$data = $this->db->queryAll($sql);
		$response->data = [];
		$response->lotes = [];
		/*----------  GRAFICAS  ----------*/
		$racimos = [];
		$manos = [];
		$peso = [];
		$calibre = [];
		$dedo = [];
		$response->racimos = [];
		$response->manos = [];
		$response->calibre = [];
		$response->dedo = [];
		/*----------  GRAFICAS  ----------*/
		$dataMax = [];
		foreach ($data as $key => $value) {
			$value = (object)$value;
			$response->data["totales"]["semanas"][$value->semana]["campo"] = (int)$value->semana;
			$response->data["totales"]["semanas"][$value->semana]["expanded"] = false;
			$response->data["totales"]["semanas"][$value->semana]["lotes"] = $this->getLotes((int)$value->semana);
			$response->data["totales"]["semanas"][$value->semana]["muestrados"] = (float)$value->muestrados;
			$response->data["totales"]["semanas"][$value->semana]["peso"] = (float)$value->peso;
			$response->data["totales"]["semanas"][$value->semana]["manos"] = (float)$value->manos;
			$response->data["totales"]["semanas"][$value->semana]["largo_dedos"] = (float)$value->largo_dedos;
			$response->data["totales"]["semanas"][$value->semana]["calibracion"] = (float)$value->calibracion;
			$response->data["totales"]["semanas"][$value->semana]["total_cosechados"] = (float)$value->total_cosechados;
			$response->data["totales"]["semanas"][$value->semana]["total_recusados"] = (float)$value->total_recusados;
			$response->data["totales"]["semanas"][$value->semana]["total_procesada"] = (float)$value->total_procesada;
			
			$response->data["totales"]["campo"] = "2016";
			$response->data["totales"]["expanded"] = false;
			$response->data["totales"]["muestrados"] += (float)$value->muestrados;
			$response->data["totales"]["peso"] += (float)$value->peso;
			$response->data["totales"]["largo_dedos"] += (float)$value->largo_dedos;
			$response->data["totales"]["calibracion"] += (float)$value->calibracion;
			$response->data["totales"]["manos"] += (float)$value->manos;
			$response->data["totales"]["total_cosechados"] += (float)$value->total_cosechados;
			$response->data["totales"]["total_recusados"] += (float)$value->total_recusados;
			$response->data["totales"]["total_procesada"] += (float)$value->total_procesada;

			if((float)$value->muestrados > 0){
				$dataMax["muestrados"][] = (float)$value->muestrados;
			}
			if((float)$value->peso > 0){
				$dataMax["peso"][] = (float)$value->peso;
			}
			if((float)$value->largo_dedos > 0){
				$dataMax["largo_dedos"][] = (float)$value->largo_dedos;
			}
			if((float)$value->calibracion > 0){
				$dataMax["calibracion"][] = (float)$value->calibracion;
			}
			if((float)$value->total_cosechados > 0){
				$dataMax["total_cosechados"][] = (float)$value->total_cosechados;
			}
			if((float)$value->total_recusados > 0){
				$dataMax["total_recusados"][] = (float)$value->total_recusados;
			}
			if((float)$value->total_procesada > 0){
				$dataMax["total_procesada"][] = (float)$value->total_procesada;
			}
			if((float)$value->manos > 0){
				$dataMax["manos"][] = (float)$value->manos;
			}

			/*----------  GRAFICAS  ----------*/
			$racimos[] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Cortados",
				"value" => (double)$value->total_cosechados,
			];
			$racimos[] = [
				"label" => $value->semana,
				"position" => 1,
				"legend" => "Recusados",
				"value" => (double)$value->total_recusados,
			];
			$racimos[] = [
				"label" => $value->semana,
				"position" => 2,
				"legend" => "Procesados",
				"value" => (double)$value->total_procesada,
			];

			$peso[$value->semana] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Peso",
				"value" => (double)$value->peso,
			];
			$manos[] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Manos",
				"value" => (double)$value->manos,
			];
			$calibre[] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Calibración",
				"value" => (double)$value->calibracion,
			];
			$dedo[] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Dedos",
				"value" => (double)$value->largo_dedos,
			];
			/*----------  GRAFICAS  ----------*/
		}

		$response->data["max"]["campo"] = "MAX";
		$response->data["max"]["muestrados"] = max($dataMax["muestrados"]);
		$response->data["max"]["peso"] = max($dataMax["peso"]);
		$response->data["max"]["largo_dedos"] = max($dataMax["largo_dedos"]);
		$response->data["max"]["calibracion"] = max($dataMax["calibracion"]);
		$response->data["max"]["manos"] = max($dataMax["manos"]);
		$response->data["max"]["total_cosechados"] = max($dataMax["total_cosechados"]);
		$response->data["max"]["total_recusados"] = max($dataMax["total_recusados"]);
		$response->data["max"]["total_procesada"] = max($dataMax["total_procesada"]);

		$response->data["min"]["campo"] = "MIN";
		$response->data["min"]["muestrados"] = min($dataMax["muestrados"]);
		$response->data["min"]["peso"] = min($dataMax["peso"]);
		$response->data["min"]["largo_dedos"] = min($dataMax["largo_dedos"]);
		$response->data["min"]["calibracion"] = min($dataMax["calibracion"]);
		$response->data["min"]["manos"] = min($dataMax["manos"]);
		$response->data["min"]["total_cosechados"] = min($dataMax["total_cosechados"]);
		$response->data["min"]["total_recusados"] = min($dataMax["total_recusados"]);
		$response->data["min"]["total_procesada"] = min($dataMax["total_procesada"]);

		$response->data["avg"]["campo"] = "AVG";
		$response->data["avg"]["muestrados"] = (array_sum($dataMax["muestrados"]) / count($dataMax["muestrados"]));
		$response->data["avg"]["peso"] = (array_sum($dataMax["peso"]) / count($dataMax["peso"]));
		$response->data["totales"]["peso"] = (array_sum($dataMax["peso"]) / count($dataMax["peso"]));
		$response->data["avg"]["largo_dedos"] = (array_sum($dataMax["largo_dedos"]) / count($dataMax["largo_dedos"]));
		$response->data["totales"]["largo_dedos"] = (array_sum($dataMax["largo_dedos"]) / count($dataMax["largo_dedos"]));
		$response->data["avg"]["calibracion"] = (array_sum($dataMax["calibracion"]) / count($dataMax["calibracion"]));
		$response->data["totales"]["calibracion"] = (array_sum($dataMax["calibracion"]) / count($dataMax["calibracion"]));
		$response->data["avg"]["manos"] = (array_sum($dataMax["manos"]) / count($dataMax["manos"]));
		$response->data["totales"]["manos"] = (array_sum($dataMax["manos"]) / count($dataMax["manos"]));
		$response->data["avg"]["total_cosechados"] = (array_sum($dataMax["total_cosechados"]) / count($dataMax["total_cosechados"]));
		$response->data["avg"]["total_recusados"] = (array_sum($dataMax["total_recusados"]) / count($dataMax["total_recusados"]));
		$response->data["avg"]["total_procesada"] = (array_sum($dataMax["total_procesada"]) / count($dataMax["total_procesada"]));


		$response->peso = $this->generateDataPeso();

		if((int)$response->data["avg"]["peso"] <= 0){
			$response->data["avg"]["peso"] = $response->peso->promedio;
		}

		$response->manos = $this->chartInit($manos , "vertical" , "Mano");
		$response->calibre = $this->chartInit($calibre , "vertical" , "Calibración");
		$response->dedo = $this->chartInit($dedo , "vertical" , "Dedos");
		$response->racimos["cortados"] = $this->chartInit($racimos , "vertical" , "Racimos");
		// $response->racimos["recusados"] = $this->chartInit($racimos["recusados"] , "vertical" , "Recusados");
		// $response->racimos["procesados"] = $this->chartInit($racimos["procesados"] , "vertical" , "Procesados");
		$cosechados_year = (double)$this->db->queryOne("SELECT SUM(cosechados_ha_sem) AS cosechados_ha_sem FROM (
							SELECT semana, SUM(cosechados_sn) AS cosechados_sn ,(SELECT SUM(ha) AS ha FROM produccion_ha
							WHERE year = YEAR(CURRENT_DATE)  AND semana = p.semana
							) AS ha ,
							(
							SUM(cosechados_sn) /
							(SELECT SUM(ha) AS ha FROM produccion_ha
							WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana)
							) AS cosechados_ha_sem
							FROM produccion AS p
							WHERE year = YEAR(CURRENT_DATE)
							GROUP BY semana) AS details");

		$procesados_year = (double)$this->db->queryOne("SELECT SUM(cosechados_ha_sem) AS cosechados_ha_sem FROM (
							SELECT semana, SUM(cosechados_sn) AS cosechados_sn, SUM(recusados_sn) AS recusados_sn,(SELECT SUM(ha) AS ha FROM produccion_ha
							WHERE year = YEAR(CURRENT_DATE)  AND semana = p.semana
							) AS ha ,
							(
							(SUM(cosechados_sn)-SUM(recusados_sn)) /
							(SELECT SUM(ha) AS ha FROM produccion_ha
							WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana)
							) AS cosechados_ha_sem
							FROM produccion AS p
							WHERE year = YEAR(CURRENT_DATE)
							GROUP BY semana) AS details");
		
		$weekofYear = (int)$this->db->queryOne("SELECT MAX(semana) AS semana FROM produccion WHERE year = YEAR(CURRENT_DATE)");

		$response->resumen = [
			"cosechados" => $response->data["totales"]["total_cosechados"],
			"cosechados_year" => $cosechados_year,
			"cosechados_week" => ($cosechados_year / $weekofYear),
			"recusados" => $response->data["totales"]["total_recusados"],
			"procesados" => $response->data["totales"]["total_procesada"],
			"procesados_year" => $procesados_year,
			"procesados_week" => ($procesados_year / $weekofYear),
			"cajas" => (float)$this->db->queryOne("SELECT ROUND(SUM(caja),2) AS cajas FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE)"),
		];

		$response->lotes = $this->db->queryAllSpecial("SELECT lote AS id , lote AS label FROM produccion GROUP BY lote");

		$response->id_company = $this->session->id_company;
		$response->edades = $this->getPromedioEdadWeek();

		return $response;
	}

	private function getLotes($semana = 0){
		$response = new stdClass;
		$response->data = [];
		if($semana > 0){
			$sYear = " AND YEAR(fecha) = YEAR(CURRENT_DATE)";
			$sSemana = "WEEK(p.fecha)";
			if($this->session->id_company == 2){
				$sYear = " AND year = YEAR(CURRENT_DATE)";
				$sSemana = "semana";
			}


			$sql = "SELECT $sSemana AS semana , lote,
				SUM(total_cosechados) AS total_cosechados , 
				SUM(total_recusados) AS total_recusados,
				(SUM(total_cosechados) - SUM(total_recusados)) AS total_procesada,
				'false' AS expanded,
				(SELECT COUNT(*) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS muestrados,
				(SELECT AVG(peso) FROM produccion_muestra WHERE WEEK(fecha) = {$semana} AND lote = p.lote {$sYear}) AS peso,
				(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS largo_dedos,
				(SELECT AVG(calibracion) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS calibracion,
				(SELECT AVG(manos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS manos
				FROM produccion AS p
				WHERE 1 = 1 AND $sSemana = {$semana} {$sYear}
				GROUP BY lote";
			$data = $this->db->queryAll($sql);
			foreach ($data as $key => $value) {
				$response->data[$value->lote]["campo"] = (int)$value->lote;
				$response->data[$value->lote]["muestrados"] = (int)$value->muestrados;
				$response->data[$value->lote]["peso"] = (int)$value->peso;
				$response->data[$value->lote]["manos"] = (int)$value->manos;
				$response->data[$value->lote]["largo_dedos"] = (int)$value->largo_dedos;
				$response->data[$value->lote]["calibracion"] = (int)$value->calibracion;
				$response->data[$value->lote]["total_cosechados"] = (int)$value->total_cosechados;
				$response->data[$value->lote]["total_recusados"] = (int)$value->total_recusados;
				$response->data[$value->lote]["total_procesada"] = (int)$value->total_procesada;
			}
		}

		return $response->data;
	}

	private function getPromedioEdadWeek(){
		$sql = "SELECT WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY WEEK(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->semana]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->semana]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$response->total[$value->semana]["sum"] += $value->cosechados_blanco * $response->data[$value->semana]["blanco"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_blanco;
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->semana]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->semana]["negra"]["cosechadas"] = $value->cosechados_negro;
				$response->total[$value->semana]["sum"] += $value->cosechados_negro * $response->data[$value->semana]["negra"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_negro;
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->semana]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->semana]["lila"]["cosechadas"] = $value->cosechados_lila;
				$response->total[$value->semana]["sum"] += $value->cosechados_lila * $response->data[$value->semana]["lila"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_lila;
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->semana]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->semana]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$response->total[$value->semana]["sum"] += $value->cosechados_rojo * $response->data[$value->semana]["roja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_rojo;
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->semana]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->semana]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$response->total[$value->semana]["sum"] += $value->cosechados_cafe * $response->data[$value->semana]["cafe"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_cafe;
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->semana]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->semana]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$response->total[$value->semana]["sum"] += $value->cosechados_amarillo * $response->data[$value->semana]["amarilla"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_amarillo;
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->semana]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->semana]["verde"]["cosechadas"] = $value->cosechados_verde;
				$response->total[$value->semana]["sum"] += $value->cosechados_verde * $response->data[$value->semana]["verde"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_verde;
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->semana]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->semana]["azul"]["cosechadas"] = $value->cosechados_azul;
				$response->total[$value->semana]["sum"] += $value->cosechados_azul * $response->data[$value->semana]["azul"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_azul;
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->semana]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->semana]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$response->total[$value->semana]["sum"] += $value->cosechados_naranja * $response->data[$value->semana]["naranja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_naranja;
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->semana]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->semana]["gris"]["cosechadas"] = $value->cosechados_gris;
				$response->total[$value->semana]["sum"] += $value->cosechados_gris * $response->data[$value->semana]["gris"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_gris;
			}
			$response->edad_promedio[$value->semana] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Edad Promedio",
				"value" => (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]),
			];

			$response->edad_prom[$value->semana] = (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]);
		}

		$response->edad = array_sum($response->edad_prom) / count($response->edad_prom);
		$minMax = [
			"min" => 9,
			"max" => 15,
		];
		$response->edad_promedio = $this->chartInit($response->edad_promedio , "vertical" , "Edad Promedio" , "line" , $minMax);
		return $response;
	}

	private function getPromedioEdad(){
		$sql = "SELECT DATE(fecha) AS fecha,WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY DATE(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		$edad_promedio = [];
		$sum_edadxcosecha = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->fecha]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->fecha]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["blanco"]["edad"] * $response->data[$value->fecha]["blanco"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["blanco"]["cosechadas"];
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->fecha]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->fecha]["negra"]["cosechadas"] = $value->cosechados_negro;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["negra"]["edad"] * $response->data[$value->fecha]["negra"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["negra"]["cosechadas"];
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->fecha]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->fecha]["lila"]["cosechadas"] = $value->cosechados_lila;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["lila"]["edad"] * $response->data[$value->fecha]["lila"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["lila"]["cosechadas"];
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->fecha]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->fecha]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["roja"]["edad"] * $response->data[$value->fecha]["roja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["roja"]["cosechadas"];
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->fecha]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->fecha]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["cafe"]["edad"] * $response->data[$value->fecha]["cafe"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["cafe"]["cosechadas"];
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->fecha]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->fecha]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["amarilla"]["edad"] * $response->data[$value->fecha]["amarilla"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["amarilla"]["cosechadas"];
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->fecha]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->fecha]["verde"]["cosechadas"] = $value->cosechados_verde;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["verde"]["edad"] * $response->data[$value->fecha]["verde"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["verde"]["cosechadas"];
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->fecha]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->fecha]["azul"]["cosechadas"] = $value->cosechados_azul;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["azul"]["edad"] * $response->data[$value->fecha]["azul"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["azul"]["cosechadas"];
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->fecha]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->fecha]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["naranja"]["edad"] * $response->data[$value->fecha]["naranja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["naranja"]["cosechadas"];
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->fecha]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->fecha]["gris"]["cosechadas"] = $value->cosechados_gris;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["gris"]["edad"] * $response->data[$value->fecha]["gris"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["gris"]["cosechadas"];
			}

			$response->edad_promedio[$value->semana][$value->fecha] = $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
			$response->data[$value->semana] += $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
		}

		return $response;
	}

	private function getEdad($color = "" , $semana = 1){
		$edad = 0;
		$sql = "SELECT
				({$semana} - (
				SELECT semana FROM semanas_colores
				WHERE YEAR(CURRENT_DATE) = year AND color = UPPER('{$color}') AND semana < ({$semana} - 8) ORDER BY semana DESC LIMIT 1
				)) + 1 AS edad";
		$edad = (int)$this->db->queryOne($sql);
		return $edad;
	}

	private function generateDataPeso(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(peso) AS peso FROM (
						SELECT semana ,AVG(peso) as peso FROM (
						(SELECT lote,semana , AVG(peso) AS peso FROM produccion_peso
						WHERE year = YEAR(CURRENT_DATE)
						GROUP BY year,lote,semana)) AS p
						GROUP BY semana) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(peso),2) AS value FROM (
							SELECT year , semana ,lote,AVG(peso) AS peso FROM produccion_peso
							GROUP BY year,lote,semana) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 50,
			"max" => 100,
		];
		$response->historico = $this->chartInit($data,"vertical","Peso","line",$minMax);

		return $response;
	}

	private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = "10%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
			}
		}

		return $response->chart;
	}
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
	private function clear($String){
		$String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	    $String = str_replace(array('í','ì','î','ï'),"i",$String);
	    $String = str_replace(array('é','è','ê','ë'),"e",$String);
	    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
	    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	    $String = str_replace("ç","c",$String);
	    $String = str_replace("Ç","C",$String);
	    $String = str_replace("ñ","n",$String);
	    $String = str_replace("Ñ","N",$String);
	    $String = str_replace("Ý","Y",$String);
	    $String = str_replace("ý","y",$String);
	    $String = str_replace(".","",$String);
	    $String = str_replace("&aacute;","a",$String);
	    $String = str_replace("&Aacute;","A",$String);
	    $String = str_replace("&eacute;","e",$String);
	    $String = str_replace("&Eacute;","E",$String);
	    $String = str_replace("&iacute;","i",$String);
	    $String = str_replace("&Iacute;","I",$String);
	    $String = str_replace("&oacute;","o",$String);
	    $String = str_replace("&Oacute;","O",$String);
	    $String = str_replace("&uacute;","u",$String);
	    $String = str_replace("&Uacute;","U",$String);
	    return $String;
	}
	
}
