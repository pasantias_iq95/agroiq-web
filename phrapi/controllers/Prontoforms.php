<?php defined('PHRAPI') or die("Direct access not allowed!");

class Prontoforms {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
    }

    public function getPersonalMario(){
        $this->db = DB::getInstance("mario");

        $data = $this->db->queryAll("SELECT id, nombre, cedula, sector, lote, responsable, afiliado FROM personal WHERE status = 1");
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "SIN PERSONAL", "responsable" => "", "cedula" => "", "sector" => ""];
        }
        return $data;
    }

    public function getLaboresMario(){
        $this->db = DB::getInstance("mario");

        $data = $this->db->queryAll("SELECT id, nombre FROM cat_labores WHERE status = 1");
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "SIN CARGOS"];
        }
        return $data;
    }

    public function getCablesMario(){
        $this->db = DB::getInstance("mario");

        $data = $this->db->queryAll("SELECT id, nombre, (SELECT nombre FROM cat_lotes WHERE id = id_lote) as lote FROM cat_cables WHERE status = 1");
        if(count($data) == 0){
            $data = ["id" => 0, "nombre" => "SIN CARGOS"];
        }
        return $data;   
    }

    /* BEGIN MIGRAR */
    public function getCategoriasMarun(){
        #$this->db = DB::getInstance("marun");
        echo "hola";
        /*$data = $this->db->queryAll("SELECT * FROM calidad_categorias WHERE status = 1");
        if(count($data) == 0){
            $data = ["id" =>, "nombre" => "NO HAY"];
        }
        return $data;*/
    }

    public function getDefectosMarun(){
        $this->db = DB::getInstance("marun");

        $data = $this->db->queryAll("SELECT * FROM calidad_defectos WHERE status = 1");
        if(count($data) == 0){
            $data = ["id" => "", "nombre" => "NO HAY"];
        }
        return $data;   
    }
    /* END MIGRAR */
}