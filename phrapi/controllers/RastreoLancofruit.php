<?php defined('PHRAPI') or die("Direct access not allowed!");

class RastreoLancofruit extends ReactGrafica {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function last(){
        $filters = $this->getFilters();
        $response = new stdClass;
        $sql = "SELECT razon_social_comprador As id, razon_social_comprador As label FROM lancofruit_ventas WHERE status = 'Activo' GROUP BY razon_social_comprador";
        $response->clientes = $this->db->queryAllOne($sql);

        $sql = "SELECT lancofruit_rutas.id AS id, lancofruit_rutas.nombre AS label 
                FROM lancofruit_rutas 
                INNER JOIN lancofruit_ventas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta WHERE lancofruit_rutas.status = '1'
                GROUP BY lancofruit_rutas.nombre";
        $response->rutas = $this->db->queryAll($sql);
        return $response;
    }

    private function getFilters(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "total_modificado" => (object) getValueFrom($postdata , "total_modificado", [] , FILTER_SANITIZE_PHRAPI_ARRAY),
            "id_venta" => (int) getValueFrom($postdata , "id_venta", "", FILTER_SANITIZE_INT),
            "semana" => (int) getValueFrom($postdata , "semana", "", FILTER_SANITIZE_STRING),
            "year" => (int) getValueFrom($postdata , "year", date('Y'), FILTER_SANITIZE_STRING),
            "hora" => getValueFrom($postdata , "hora", '', FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha", '', FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial", '', FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final", '', FILTER_SANITIZE_STRING),
        ];
        return $filters;
    }

    public function posiciones(){
        $response = new stdClass;

        $filters = $this->getFilters();
        if($filters->hora != ''){
            $sWhere .= " AND TIME(fecha) > '{$filters->hora}'";
        }

        $sql = "SELECT id, CONCAT(latitude,' ',north_south) AS lat, CONCAT(longitude,' ',east_west) AS lng, TIME(fecha) AS 'time', ROUND(speed * 1.852, 2) AS speed
                FROM gps_pos
                WHERE DATE(fecha) = '{$filters->fecha}' AND track_made != '0.0' $sWhere";

        /*$sql = "SELECT id, latitude AS lat, longitude AS lng, hora AS 'time', speed
                FROM lancofruit_coordenadas
                WHERE fecha = '{$filters->fecha}' AND track_made != '0.0' $sWhere";*/
        $response->data = $this->db->queryAll($sql);

        foreach($response->data as $row){
            $row->lng = $this->LngTo2D($row->lng);
            $row->lat = $this->LatTo2D($row->lat);
        }

        return $response;
    }

    private function LatTo2D($coord){
        $lng = $coord;
        $brk = strpos($lng,".") - 2;
        if($brk < 0){ $brk = 0; }

        $minutes = substr($lng, $brk);
        $degrees = substr($lng, 0, $brk);
        $newLng = $degrees + $minutes / 60;
        if(stristr($lng, "S")){
            $newLng = -1 * $newLng;
        }
        return $newLng;
    }

    private function LngTo2D($coord){
        $lng = $coord;
        #D($lng);
        $brk = strpos($lng,".") - 2;
        #D($brk);
        if($brk < 0){ $brk = 0; }

        $minutes = substr($lng, $brk);
        #D($minutes);
        $degrees = substr($lng, 0,$brk);
        #D($degrees);
        $newLng = $degrees + $minutes/60;
        #D("newLng = $degrees + $minutes/60");
        #F($newLng);
        if(stristr($lng, "W")){
            $newLng = -1 * $newLng;
        }
        return $newLng;
    }

    public function indicadores(){
        $filters = $this->getFilters();
        $response = new stdClass;
        $sWhere = "";
        $sWhereAnio = "";
        if($filters->fecha_inicial != "" && $filters->fecha_final != ""){
            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        } 
        if($filters->year != ""){
            $sWhere .= " AND lancofruit_ventas.anio = '{$filters->year}'";
            $sWhereAnio .= " AND anio = '{$filters->year}'";
        }
        if($filters->semana != ""){
            $sWhere .= " AND lancofruit_ventas.semana = '{$filters->semana}'";
        }
        $response->tags = [];
        $total = "";
        $total_puntos = "";

        $sql = "SELECT SUM(total) AS total FROM lancofruit_ventas WHERE status = 'Activo' $sWhere";
        $total = $this->db->queryOne($sql);

        $sql = "SELECT COUNT(1) AS puntos FROM lancofruit WHERE status = 'Cliente'";
        $total_puntos = $this->db->queryOne($sql);
        
        $sql = "SELECT SUM(total) AS ventas FROM lancofruit_ventas WHERE status = 'Activo' $sWhere";
        $val = $this->db->queryOne($sql);
        if($val == null) $val = 0;
        $gavetas = ROUND($val / 6, 2). " GAVETAS";
		$response->tags[] = [
            "tittle" => "TOTAL VENTAS",
            "subtittle" => "{$gavetas}",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "dark",
            "miles" => true
        ];

        $sql = "SELECT COUNT(1) AS puntos FROM lancofruit WHERE status = 'Cliente'";
        $val = $this->db->queryOne($sql);
        if($val == null) $val = 0;
        $response->tags[] = [
            "tittle" => "TOTAL PUNTOS",
            "subtittle" => "",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total_puntos * 100,
            "cssClass" => "blue"
        ];
        
        $val = $total / $total_puntos;
        if($val == null) $val = 0;
        $gavetas = ROUND($val / 6, 2). " GAVETAS";
        $response->tags[] = [
            "tittle" => "PROMEDIO VENTAS/PUNTOS",
            "subtittle" => "{$gavetas}",
            "valor" => (double) $val,
            "promedio" => (double)  $val / $total * 100,
            "cssClass" => "green",
            "miles" => true
        ];

        $sql = "SELECT SUM(total) AS total FROM lancofruit_ventas WHERE status = 'Activo' AND id_ruta = 1 $sWhere";
        $val = $this->db->queryOne($sql);
        if($val == null) $val = 0;
        $gavetas = ROUND($val / 6, 2). " GAVETAS";
        $response->tags[] = [
            "tittle" => "TOTAL RUTA 1",
            "subtittle" => "{$gavetas}",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "red",
            "miles" => true
        ];

        $sql = "SELECT SUM(total) AS total FROM lancofruit_ventas WHERE status = 'Activo' AND id_ruta = 2 $sWhere";
        $val = $this->db->queryOne($sql);
        if($val == null) $val = 0;
        $gavetas = ROUND($val / 6, 2). " GAVETAS";
        $response->tags[] = [
            "tittle" => "TOTAL RUTA 2",
            "subtittle" => "{$gavetas}",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "yellow-saffron",
            "miles" => true
        ];

        $sql = "SELECT SUM(total) AS total FROM lancofruit_ventas WHERE status = 'Activo' AND id_ruta = 3 $sWhere";
        $val = $this->db->queryOne($sql);

        if($val == null) $val = 0;
        $gavetas = ROUND($val / 6, 2). " GAVETAS";
        $response->tags[] = [
            "tittle" => "TOTAL RUTA 3",
            "subtittle" => "{$gavetas}",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "purple",
            "miles" => true
        ];

        
        $sql = "SELECT SUM(total) AS total FROM lancofruit_ventas WHERE status = 'Activo' AND id_ruta = 4 $sWhere";
        $val = $this->db->queryOne($sql);

        if($val == null) $val = 0;
        $gavetas = ROUND($val / 6, 2). " GAVETAS";
        $response->tags[] = [
            "tittle" => "TOTAL RUTA 4",
            "subtittle" => "{$gavetas}",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "green-seagreen",
            "miles" => true
        ];

        
        $sql = "SELECT MAX(semana) FROM lancofruit_ventas WHERE status = 'Activo' $sWhereAnio";
        $semanas =  $this->db->queryOne($sql);
        
        $val = $total / 4;
        $gavetas = $val / 6;
        if($val == null) $val = 0;
        $response->tags[] = [
            "tittle" => "PROMEDIO VENTAS/RUTA",
            "subtittle" => "GAVETAS ".$gavetas,
            "valor" => (double) $total / 4,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "green-jungle",
            "miles" => true
        ];

        $sql = "SELECT
                    MAX(total) AS total,
                    mes
                FROM(
                SELECT 
                    SUM(total) AS total,
                    CASE mes
                        WHEN 1 THEN 'ENERO'
                        WHEN 2 THEN 'FEBRERO'
                        WHEN 3 THEN 'MARZO'
                        WHEN 4 THEN 'ABRIL'
                        WHEN 5 THEN 'MAYO'
                        WHEN 6 THEN 'JUNIO'
                        WHEN 7 THEN 'JULIO'
                        WHEN 8 THEN 'AGOSTO'
                        WHEN 9 THEN 'SEPTIEMBRE'
                        WHEN 10 THEN 'OCTUBRE'
                        WHEN 11 THEN 'NOVIEMBRE'
                        WHEN 12 THEN 'DICIEMBRE'
                    END AS mes,
                    id_ruta
                    FROM lancofruit_ventas 
                    WHERE status = 'Activo' AND total != 0 $sWhereAnio
                    GROUP BY mes
                ) AS tbl";
        $result = $this->db->queryRow($sql);
        $val = $result->total;
        $mes = $result->mes;
        
        if($val == null) $val = 0;
        $response->tags[] = [
            "tittle" => "MAX VENTAS/MES",
            "subtittle" => "{$mes}",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "red-thunderbird",
            "miles" => true
        ];

        $sql = "SELECT
                    MIN(total) AS total,
                    mes
                FROM(
                SELECT 
                    SUM(total) AS total,
                    CASE mes
                        WHEN 1 THEN 'ENERO'
                        WHEN 2 THEN 'FEBRERO'
                        WHEN 3 THEN 'MARZO'
                        WHEN 4 THEN 'ABRIL'
                        WHEN 5 THEN 'MAYO'
                        WHEN 6 THEN 'JUNIO'
                        WHEN 7 THEN 'JULIO'
                        WHEN 8 THEN 'AGOSTO'
                        WHEN 9 THEN 'SEPTIEMBRE'
                        WHEN 10 THEN 'OCTUBRE'
                        WHEN 11 THEN 'NOVIEMBRE'
                        WHEN 12 THEN 'DICIEMBRE'
                    END AS mes,
                    id_ruta
                    FROM lancofruit_ventas 
                    WHERE lancofruit_ventas.status = 'Activo' AND total != 0 $sWhereAnio
                    GROUP BY mes
                    ORDER BY total
                ) AS tbl";
        $result = $this->db->queryRow($sql);
        $val = $result->total;
        $mes = $result->mes;
        
        if($val == null) $val = 0;
        $response->tags[] = [
            "tittle" => "MIN VENTAS/MES",
            "subtittle" => "{$mes}",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "purple-soft",
            "miles" => true
        ];

        $sql = "SELECT semana FROM lancofruit_ventas WHERE status = 'Activo' $sWhereAnio GROUP BY semana";
        $semanas = $this->db->queryAll($sql);
        $semanas = count($semanas);
        $val = $total / $semanas;
        
        if($val == null) $val = 0;
        $response->tags[] = [
            "tittle" => "PROMEDIO VENTAS/SEMANA",
            "subtittle" => "",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "yellow-mint",
            "miles" => true
        ];

        $sql = "SELECT 
                    MAX(total) AS total, 
                    semana, 
                    id_ruta 
                FROM (
                    SELECT 
                        SUM(total) AS total,
                        semana,
                        id_ruta
                    FROM lancofruit_ventas 
                    WHERE status = 'Activo' AND total != 0 $sWhereAnio
                    GROUP BY semana
                    ) AS tbl";
        $result = $this->db->queryRow($sql);
        $val = $result->total;
        $semana = "SEMANA ".$result->semana;

        if($val == null) $val = 0;
        $response->tags[] = [
            "tittle" => "MAX VENTAS/SEMANA",
            "subtittle" => "{$semana}",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "yellow-gold",
            "miles" => true
        ];

        $sql = "SELECT 
                    MIN(total) AS total, 
                    semana, 
                    id_ruta 
                FROM (
                    SELECT 
                        SUM(total) AS total,
                        semana,
                        id_ruta
                    FROM lancofruit_ventas 
                    WHERE status = 'Activo' AND total != 0 $sWhereAnio
                    GROUP BY semana
                    ORDER BY total
                    ) AS tbl";
        $result = $this->db->queryRow($sql);
        $val = $result->total;
        $semana = "SEMANA ".$result->semana;

        if($val == null) $val = 0;
        $response->tags[] = [
            "tittle" => "MIN VENTAS/SEMANA",
            "subtittle" => "{$semana}",
            "valor" => (double) $val,
            "promedio" => (double) $val / $total * 100,
            "cssClass" => "grey-mint",
            "miles" => true
        ];

		return $response;
    }
    
    public function eliminar(){
        $params = $this->getFilters();
        $response = new stdClass;
        $response->status = 400;

        $sql = "UPDATE lancofruit_ventas SET status = 'Eliminado' WHERE id = {$params->id_venta}";
        if($this->db->query($sql)){
            $response->status = 200;
        }

        return $response;
    }

    public function editar(){
        $params = $this->getFilters();
        $response = new stdClass;

        if(count($params->total_modificado) > 0){
            $response->status = 200;
            foreach($params->total_modificado as $key => $value){
                $sql = "UPDATE lancofruit_ventas SET total = '{$value}', total_sin_impuestos = '{$value}', importe_total = '{$value}' WHERE id = {$key}";
                $this->db->query($sql);
            }
        }

        return $response;
    }

    public function ventaDia(){
        $response = new stdClass();
        $filters = $this->getFilters();
        $sWhere = "";
        if($filters->fecha_inicial != "" && $filters->fecha_final != ""){
            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        } else if($filters->semana == 0 && $filters->year != ""){
            $sWhere .= " AND semana = (SELECT MAX(semana) FROM lancofruit_ventas WHERE anio = '{$filters->year}')";
        }
        if($filters->year != ""){
            $sWhere .= " AND lancofruit_ventas.anio = '{$filters->year}'";
        }
        if($filters->semana != ""){
            $sWhere .= " AND lancofruit_ventas.semana = '{$filters->semana}'";
        }

        $sql = "SELECT 
                    anio,
                    semana,
                    CASE DAYOFWEEK(lancofruit_ventas.fecha) 
                        WHEN 1 THEN 'DOM'
                        WHEN 2 THEN 'LUN'
                        WHEN 3 THEN 'MAR'
                        WHEN 4 THEN 'MIÉ'
                        WHEN 5 THEN 'JUE'
                        WHEN 6 THEN 'VIE'
                        WHEN 7 THEN 'SAB'
                    END AS dia,
                    COUNT(1) AS puntos,
                    SUM(total) AS venta,
                    fecha
                FROM lancofruit_ventas
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY dia
                ORDER BY fecha";
        $response->data_venta_dia = $this->db->queryAll($sql);
        foreach($response->data_venta_dia as $row){
            $sql = "SELECT
            	        anio,
                        semana,
                        id_ruta,
                        nombre AS ruta,
                        COUNT(1) AS puntos_t,
                        SUM(total) AS venta,
                        fecha
                    FROM lancofruit_ventas
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                    WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$row->anio}' AND semana = '{$row->semana}' AND fecha = '{$row->fecha}'
                    GROUP BY id_ruta";
            $row->rutas = $this->db->queryAll($sql);
            foreach($row->rutas as $rowdos){
                    $sql = "SELECT 
                                id AS id_venta,
                                razon_social_comprador AS puntos, 
                                total 
                            FROM lancofruit_ventas 
                            WHERE lancofruit_ventas.status = 'Activo' AND id_ruta = {$rowdos->id_ruta} AND fecha = '{$rowdos->fecha}'
                            GROUP BY razon_social_comprador";
                $rowdos->puntos = $this->db->queryAll($sql);
            }
        }

        $sql = "SELECT 
                    lancofruit_rutas.nombre AS ruta,
                    SUM(lancofruit_ventas.total) AS total
                FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY lancofruit_rutas.nombre";
        $response->rutas = $this->db->queryAll($sql);

        $sql = "SELECT 
                    lancofruit_rutas.nombre AS legend,
                    CASE DAYOFWEEK(lancofruit_ventas.fecha) 
                        WHEN 1 THEN 'DOM'
                        WHEN 2 THEN 'LUN'
                        WHEN 3 THEN 'MAR'
                        WHEN 4 THEN 'MIÉ'
                        WHEN 5 THEN 'JUE'
                        WHEN 6 THEN 'VIE'
                        WHEN 7 THEN 'SAB'
                    END AS label,
                    SUM(lancofruit_ventas.total) AS value,
                    true AS selected
                FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY label, lancofruit_rutas.nombre
                ORDER BY fecha";
        $response->data = $this->generateSeries($this->db, $sql, "line", null, true);
        return $response;   
    }

    public function ventaSemana(){
        $response = new stdClass();
        $filters = $this->getFilters();
        $sWhere = "";
        if($filters->fecha_inicial != "" && $filters->fecha_final != ""){
            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->year != ""){
            $sWhere .= " AND lancofruit_ventas.anio = '{$filters->year}'";
        }
        if($filters->semana != ""){
            $sWhere .= " AND lancofruit_ventas.semana = '{$filters->semana}'";
        }

        $sql = "SELECT 
                    anio,
                    semana,
                    COUNT(1) AS puntos,
                    SUM(total) AS venta,
                    fecha
                FROM lancofruit_ventas
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY semana";
        $response->data_venta_semana = $this->db->queryAll($sql);
        foreach($response->data_venta_semana as $row){
            $sql = "SELECT
            	        anio,
                        semana,
                        id_ruta,
                        nombre AS ruta,
                        COUNT(1) AS puntos,
                        SUM(total) AS venta
                    FROM lancofruit_ventas
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                    WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$row->anio}' AND semana = '{$row->semana}'
                    GROUP BY id_ruta";
            $row->rutas = $this->db->queryAll($sql);
            foreach($row->rutas as $rowdos){
                $sql = "SELECT 
                            anio,
                            semana,
                            id_ruta,
                            COUNT(1) AS puntos_t,
                            lancofruit_ventas.fecha AS dia,
                            fecha,
                            SUM(total) AS venta
                        FROM lancofruit_ventas
                        INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                        WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$rowdos->anio}' AND semana = '{$rowdos->semana}' AND id_ruta = {$rowdos->id_ruta}
                        GROUP BY fecha";
                $rowdos->dias = $this->db->queryAll($sql);
                foreach($rowdos->dias as $rowtres){
                        $sql = "SELECT 
                                    razon_social_comprador AS puntos, 
                                    total 
                                FROM lancofruit_ventas 
                                WHERE lancofruit_ventas.status = 'Activo' AND id_ruta = {$rowtres->id_ruta} AND fecha = '{$rowtres->fecha}'";
                    $rowtres->puntos = $this->db->queryAll($sql);
                }
            }
        }

        
        $sql = "SELECT 
                    lancofruit_rutas.nombre AS ruta,
                    SUM(lancofruit_ventas.total) AS total
                FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY lancofruit_rutas.nombre";
        $response->rutas = $this->db->queryAll($sql);

        $sql = "SELECT
                    semanas.semana AS label,
                    rutas.ruta AS legend,
                    true AS selected,
                    ventas.total AS value
                FROM (
                    SELECT semana
                    FROM lancofruit_ventas
                    WHERE lancofruit_ventas.status = 'Activo' $sWhere
                    GROUP BY semana
                ) semanas
                JOIN (
                    SELECT lancofruit_rutas.nombre AS ruta
                    FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                    WHERE lancofruit_ventas.status = 'Activo' $sWhere
                    GROUP BY lancofruit_rutas.nombre
                ) rutas
                LEFT JOIN (
                    SELECT lancofruit_rutas.nombre AS ruta, semana, SUM(total) total
                    FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                    WHERE lancofruit_ventas.status = 'Activo' $sWhere
                    GROUP BY lancofruit_ventas.semana, lancofruit_ventas.id_ruta
                ) ventas ON semanas.semana = ventas.semana AND rutas.ruta = ventas.ruta
                ORDER BY semanas.semana, rutas.ruta";
        $response->data = $this->generateSeries($this->db, $sql, "line", null, true);
        return $response;
    }

    public function ventaMes(){
        $response = new stdClass();
        $filters = $this->getFilters();
        $sWhere = "";
        if($filters->fecha_inicial != "" && $filters->fecha_final != ""){
            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->year != ""){
            $sWhere .= " AND lancofruit_ventas.anio = '{$filters->year}'";
        }
        if($filters->semana != ""){
            $sWhere .= " AND lancofruit_ventas.semana = '{$filters->semana}'";
        } 

        $sql = "SELECT 
                    anio,
                    semana,
                    mes,
                    CASE mes 
                            WHEN 1 THEN 'ENE'
                            WHEN 2 THEN 'FEB'
                            WHEN 3 THEN 'MAR'
                            WHEN 4 THEN 'ABR'
                            WHEN 5 THEN 'MAY'
                            WHEN 6 THEN 'JUN'
                            WHEN 7 THEN 'JUL'
                            WHEN 8 THEN 'AGO'
                            WHEN 9 THEN 'SEP'
                            WHEN 10 THEN 'OCT'
                            WHEN 11 THEN 'NOV'
                            WHEN 12 THEN 'DIC'
                        END AS name_mes,
                    COUNT(1) AS puntos,
                    SUM(total) AS venta,
                    fecha
                FROM lancofruit_ventas
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY mes";
        $response->data_venta_mes = $this->db->queryAll($sql);
        foreach($response->data_venta_mes as $row){
            $sql = "SELECT 
                    anio,
                    semana,
                    id_ruta,
                    nombre AS ruta,
                    COUNT(1) AS puntos,
                    mes,
                    SUM(total) AS venta,
                    fecha
                FROM lancofruit_ventas
                INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$row->anio}' AND mes = '{$row->mes}'
                GROUP BY semana";
            $row->semanas = $this->db->queryAll($sql);
            foreach($row->semanas as $rowdos){
                $sql = "SELECT
                            anio,
                            semana,
                            id_ruta,
                            nombre AS ruta,
                            COUNT(1) AS puntos,
                            SUM(total) AS venta,
                            mes,
                            fecha
                        FROM lancofruit_ventas
                        INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                        WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$rowdos->anio}' AND mes = '{$rowdos->mes}' AND semana = '{$rowdos->semana}'
                        GROUP BY id_ruta";
                $rowdos->rutas = $this->db->queryAll($sql);
                foreach($rowdos->rutas as $rowtres){
                    $sql = "SELECT 
                                anio,
                                semana,
                                id_ruta,
                                COUNT(1) AS puntos_t,
                                fecha,
                                lancofruit_ventas.fecha AS dia,
                                SUM(total) AS venta,
                                fecha
                            FROM lancofruit_ventas
                            INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                            WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$rowtres->anio}' AND semana = '{$rowtres->semana}' AND id_ruta = {$rowtres->id_ruta}
                            GROUP BY fecha";
                    $rowtres->dias = $this->db->queryAll($sql);
                    foreach($rowtres->dias as $rowcuatro){
                            $sql = "SELECT 
                                        razon_social_comprador AS puntos, 
                                        total 
                                    FROM lancofruit_ventas 
                                    WHERE lancofruit_ventas.status = 'Activo' AND fecha = '{$rowcuatro->fecha}' AND id_ruta = {$rowcuatro->id_ruta}";
                        $rowcuatro->puntos = $this->db->queryAll($sql);
                    }
                }
            }    
        }

        $sql = "SELECT 
                    lancofruit_rutas.nombre AS ruta,
                    SUM(lancofruit_ventas.total) AS total
                FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY lancofruit_rutas.nombre";
        $response->rutas = $this->db->queryAll($sql);

        $sql = "SELECT
                    CASE meses.mes 
                        WHEN 1 THEN 'ENE'
                        WHEN 2 THEN 'FEB'
                        WHEN 3 THEN 'MAR'
                        WHEN 4 THEN 'ABR'
                        WHEN 5 THEN 'MAY'
                        WHEN 6 THEN 'JUN'
                        WHEN 7 THEN 'JUL'
                        WHEN 8 THEN 'AGO'
                        WHEN 9 THEN 'SEP'
                        WHEN 10 THEN 'OCT'
                        WHEN 11 THEN 'NOV'
                        WHEN 12 THEN 'DIC'
                    END AS label,
                    rutas.ruta AS legend,
                    true AS selected,
                    ventas.total AS value
                FROM (
                    SELECT mes
                    FROM lancofruit_ventas
                    WHERE lancofruit_ventas.status = 'Activo' $sWhere
                    GROUP BY mes
                ) meses
                JOIN (
                    SELECT lancofruit_rutas.nombre AS ruta
                    FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                    WHERE lancofruit_ventas.status = 'Activo' $sWhere
                    GROUP BY lancofruit_rutas.nombre
                ) rutas
                LEFT JOIN (
                    SELECT lancofruit_rutas.nombre AS ruta, mes, SUM(total) total
                    FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                    WHERE lancofruit_ventas.status = 'Activo' $sWhere
                    GROUP BY lancofruit_ventas.mes, lancofruit_ventas.id_ruta
                ) ventas ON meses.mes = ventas.mes AND rutas.ruta = ventas.ruta
                ORDER BY meses.mes, rutas.ruta";
        $response->data = $this->generateSeries($this->db, $sql, "line", null, true);

        return $response;
    }

    public function ventaAnio(){
        $response = new stdClass();

        $filters = $this->getFilters();
        $sWhere = "";
        if($filters->fecha_inicial != "" && $filters->fecha_final != ""){
            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->year != ""){
            $sWhere .= " AND lancofruit_ventas.anio = '{$filters->year}'";
        }
        if($filters->semana != ""){
            $sWhere .= " AND lancofruit_ventas.semana = '{$filters->semana}'";
        }

        $sql = "SELECT 
                    anio,
                    semana,
                    mes,
                    COUNT(1) AS puntos,
                    SUM(total) AS venta,
                    fecha
                FROM lancofruit_ventas
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY anio";
            $response->data_venta_anio = $this->db->queryAll($sql);
            foreach($response->data_venta_anio as $anio){
                $sql = "SELECT 
                            anio,
                            semana,
                            mes,
                            CASE mes 
                                    WHEN 1 THEN 'ENE'
                                    WHEN 2 THEN 'FEB'
                                    WHEN 3 THEN 'MAR'
                                    WHEN 4 THEN 'ABR'
                                    WHEN 5 THEN 'MAY'
                                    WHEN 6 THEN 'JUN'
                                    WHEN 7 THEN 'JUL'
                                    WHEN 8 THEN 'AGO'
                                    WHEN 9 THEN 'SEP'
                                    WHEN 10 THEN 'OCT'
                                    WHEN 11 THEN 'NOV'
                                    WHEN 12 THEN 'DIC'
                                END AS name_mes,
                            COUNT(1) AS puntos,
                            SUM(total) AS venta,
                            fecha
                        FROM lancofruit_ventas
                        WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$anio->anio}'
                        GROUP BY mes";
                $anio->data_venta_mes = $this->db->queryAll($sql);
                foreach($anio->data_venta_mes as $row){
                    $sql = "SELECT 
                            anio,
                            semana,
                            id_ruta,
                            nombre AS ruta,
                            COUNT(1) AS puntos,
                            mes,
                            SUM(total) AS venta,
                            fecha
                        FROM lancofruit_ventas
                        INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                        WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$row->anio}' AND mes = '{$row->mes}'
                        GROUP BY semana";
                    $row->semanas = $this->db->queryAll($sql);
                    foreach($row->semanas as $rowdos){
                        $sql = "SELECT
                                    anio,
                                    semana,
                                    id_ruta,
                                    nombre AS ruta,
                                    COUNT(1) AS puntos,
                                    SUM(total) AS venta,
                                    mes,
                                    fecha
                                FROM lancofruit_ventas
                                INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                                WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$rowdos->anio}' AND mes = '{$rowdos->mes}' AND semana = '{$rowdos->semana}'
                                GROUP BY id_ruta";
                        $rowdos->rutas = $this->db->queryAll($sql);
                        foreach($rowdos->rutas as $rowtres){
                            $sql = "SELECT 
                                        anio,
                                        semana,
                                        id_ruta,
                                        COUNT(1) AS puntos_t,
                                        fecha,
                                        lancofruit_ventas.fecha AS dia,
                                        SUM(total) AS venta,
                                        fecha
                                    FROM lancofruit_ventas
                                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                                    WHERE lancofruit_ventas.status = 'Activo' AND anio = '{$rowtres->anio}' AND semana = '{$rowtres->semana}' AND id_ruta = {$rowtres->id_ruta}
                                    GROUP BY fecha";
                            $rowtres->dias = $this->db->queryAll($sql);
                            foreach($rowtres->dias as $rowcuatro){
                                    $sql = "SELECT 
                                                razon_social_comprador AS puntos, 
                                                total 
                                            FROM lancofruit_ventas 
                                            WHERE lancofruit_ventas.status = 'Activo' AND fecha = '{$rowcuatro->fecha}' AND id_ruta = {$rowcuatro->id_ruta}";
                                $rowcuatro->puntos = $this->db->queryAll($sql);
                            }
                        }
                    }    
                }
            }

        $sql = "SELECT 
                    lancofruit_rutas.nombre AS ruta,
                    SUM(lancofruit_ventas.total) AS total
                FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY lancofruit_rutas.nombre";
        $response->rutas = $this->db->queryAll($sql);

        $sql = "SELECT 
                    lancofruit_rutas.nombre AS legend,
                    lancofruit_ventas.anio AS label,
                    SUM(lancofruit_ventas.total) AS value,
                    true AS selected
                FROM lancofruit_ventas 
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY lancofruit_ventas.anio, lancofruit_rutas.nombre";
        $response->data = $this->generateSeries($this->db, $sql, "line", null, true);

        return $response;
    }

    public function dia(){
        $response = new stdClass();

        $filters = $this->getFilters();
        $sWhere = "";
        if($filters->semana != ""){
           $sWhere = "  AND getWeek(fecha) = $filters->semana"; 
        }
        if($filters->year > 0){
            $sWhere .= " AND anio = {$filters->year}";
        }
        
        $sql = "SELECT DAYOFWEEK(fecha) AS id, 
                        CASE DAYOFWEEK(fecha) 
                            WHEN 0 THEN 'DOM'
                            WHEN 1 THEN 'LUN'
                            WHEN 2 THEN 'MAR'
                            WHEN 3 THEN 'MIE'
                            WHEN 4 THEN 'JUE'
                            WHEN 5 THEN 'VIE'
                            WHEN 6 THEN 'SAB'
                        END AS name
                FROM lancofruit_ventas
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY id";
        $response->data_dia = $this->db->queryAll($sql);
        return $response;
    }

    public function semana(){
        $response = new stdClass();

        $filters = $this->getFilters();
        $sWhere = "";
        if($filters->year > 0){
            $sWhere .= " AND anio = {$filters->year}";
        }

        $sql = "SELECT semana AS id,
                        semana AS label 
                FROM lancofruit_ventas
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY semana";
        $response->data_semana = $this->db->queryAllSpecial($sql);
        return $response;
    }

    public function mes(){
        $response = new stdClass();

        $filters = $this->getFilters();
        $sWhere = "";
        if($filters->year > 0){
            $sWhere .= " AND anio = {$filters->year}";
        }
        
        $sql = "SELECT mes AS id, 
                        CASE mes 
                            WHEN 1 THEN 'ENE'
                            WHEN 2 THEN 'FEB'
                            WHEN 3 THEN 'MAR'
                            WHEN 4 THEN 'ABR'
                            WHEN 5 THEN 'MAY'
                            WHEN 6 THEN 'JUN'
                            WHEN 7 THEN 'JUL'
                            WHEN 8 THEN 'AGO'
                            WHEN 9 THEN 'SEP'
                            WHEN 10 THEN 'OCT'
                            WHEN 11 THEN 'NOV'
                            WHEN 12 THEN 'DIC'
                        END AS label
                FROM lancofruit_ventas 
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY mes";
        $response->data_mes = $this->db->queryAll($sql);
        return $response;
    }

    public function anio(){
        $response = new stdClass();
        $sWhere = "";

        $sql = "SELECT anio AS id, 
                        anio AS label 
                FROM lancofruit_ventas
                WHERE lancofruit_ventas.status = 'Activo'
                GROUP BY anio";
        $response->data_anio = $this->db->queryAll($sql);
        return $response;
    }
}