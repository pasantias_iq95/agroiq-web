<?php defined('PHRAPI') or die("Direct access not allowed!");
class Reader
{
    private $db;
	private $session;
	private $agent;
	private $i;

	public function __construct()
	{
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->i = 0;
	}	

	public function index(){
		$this->session->agent_user = 'marun';
		$this->db = DB::getInstance($this->session->agent_user);

        $path = realpath(PHRAPI_PATH.'utilities/'.$this->session->agent_user."/new");
		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);
		foreach($objects as $name => $object){
		    if('.' != $object->getFileName() && '..' != $object->getFileName()){
	            $ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
	            if('xlsx' == $ext){
	                switch ($ext){
                        case 'xlsx':
	                        $ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
	                        $carpetas = explode("/", $ruta);
	                        $num_dir = count($carpetas)-1;
							$name = pathinfo($object->getPathName(), PATHINFO_BASENAME);
							$filename = $path."/".$name;

							$this->enfundeMarun($filename);
							//unlink($filename);
                            break;
	                    default:
	                        break;
                    }
                    break;
	            }
		    }
		}
	}

	private function calidadSumifru(){
		$sql = "SELECT * FROM calidad";
		$data = $this->db->queryAll($sql);
		foreach($data as $row){
			$json = json_decode(file_get_contents("http://app.procesos-iq.com/".$row->archivoJson), true);
			$responsable = $json['pages'][0]['answers'][2]['values'][0];
			$sql = "UPDATE calidad SET auditor = '{$responsable}' WHERE id = {$row->id}";
			$this->db->query($sql);
		}
	}

	private function fotosLancofruit(){
		$sql = "SELECT *
				FROM lancofruit_perchas
				WHERE json != ''";
		$data = $this->db->queryAll($sql);

		foreach($data as $row){
			$json = json_decode(file_get_contents("http://app.procesos-iq.com/{$row->json}"), true);
			if($json){
				$referenceNumber = $json["referenceNumber"];
				$pages = $json["pages"];
				$cluster_por_grado = $pages[1];
				if($cluster_por_grado["name"] == 'Cluster por Grado'){
					foreach($cluster_por_grado["answers"] as $q){
						if($q["question"] == 'Foto' || $q["question"] == 'Adjuntar fotos generales'){
							foreach($q["values"] as $f){
								$filename = $f["filename"];
								$url = "json/lancofruit/image/perchas/{$referenceNumber}_{$filename}";
								#$this->DD($url);

								$sql = "INSERT INTO lancofruit_perchas_images SET
											id_percha = {$row->id},
											pagina = 'General',
											url = '{$url}',
											observaciones = ''";
								#$this->db->query($sql);
							}
						}

						if($q['question'] == 'Observaciones'){
							$sql = "UPDATE lancofruit_perchas_images SET
										observaciones = '{$q["values"][0]}'
									WHERE id_percha = {$row->id} AND pagina = 'Cluster por Grado'";
							$this->DD($sql);
							$this->db->query($sql);
						}
					}
				}
			}
		}
	}

	private function pdfLancofruit(){
		$sql = "SELECT *
				FROM lancofruit_perchas
				WHERE json != ''";
		$data = $this->db->queryAll($sql);

		foreach($data as $row){
			$json = json_decode(file_get_contents("http://app.procesos-iq.com/{$row->json}"), true);
			if($json){
				$referenceNumber = $json["referenceNumber"];
				$url = "banano/marcel/lancofruitPerchas/".$referenceNumber.".pdf";
				
				$sql = "INSERT INTO lancofruit_perchas_pdf 
						SET
						id_percha = '{$row->id}',
						url = '{$url}'";
				$this->db->query($sql);
			}
		}
	}

	private function marunSem3435($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$libros = $xlsx->sheetNames();
		
		$days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		$cintas = ['VERDE', 'ROJA', 'CAFE', 'AMARILLA', 'AZUL', 'BLANCA', 'LILA', 'NEGRA', 'SIN CINTA'];
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);

			foreach($rows as $fila){
				$a_s = trim($fila[0]);
				$anio = explode("-", $a_s)[0];
				$semana = explode("-", $a_s)[1];
				
				if($semana == 34 || $semana == 35){
					$finca = $fila[3];
					$id_finca = $finca == 'VIVIANA' ? 4 : 1;

					$UNIX_DATE = ($fila[2] - 25569) * 86400;
					$fecha = gmdate("Y-m-d", $UNIX_DATE);

					$marca = $fila[4];
					$cantidad = $fila[5];
					$peso = $fila[6];

					$sql = "INSERT INTO produccion_cajas SET
								semana = $semana,
								year = $anio,
								id_finca = $id_finca,
								fecha = '{$fecha}',
								marca = '{$marca}',
								lb = {$peso},
								kg = ROUND({$peso}*0.4536, 2),
								conver = ROUND({$peso}/41.5, 2),
								convertidas_415 = {$peso}/41.5,
								finca = '{$finca}'";
					$this->DD($sql);

					for($i = 1; $i <= $cantidad; $i++){
						$this->db->query($sql);
					}
				}
			}
		}
	}

	private function palmarHistorico($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$libros = $xlsx->sheetNames();
		
		$days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		$cintas = ['VERDE', 'ROJA', 'CAFE', 'AMARILLA', 'AZUL', 'BLANCA', 'LILA', 'NEGRA', 'SIN CINTA'];
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);

			$semana = (int) $rows[3][1];
			$peso_prom = (float) $rows[56][11];
			$racimos = [];
	
			// RACIMOS

			/*foreach($rows as $key => $fila){
				$cinta = str_replace("É", "E", strtoupper(trim($fila[0])));
				if(in_array($cinta, $cintas)){
					for($i = 1; $i <= 7; $i++){
						$fecha = $this->db->queryOne("SELECT DATE_SUB(STR_TO_DATE('2018{$semana} {$days[$i-1]}', '%X%V %W'), INTERVAL 1 WEEK)");
						$cosechados = $fila[$i+1];
						$racimos[$fecha][$fila[0]] = $cosechados;
					}
				}
			}

			foreach($racimos as $fecha => $values) {
				foreach($values as $cinta => $value){
					if($value > 0){
						$sql = "INSERT INTO produccion_racimos_formularios SET
									year = 2018,
									semana = getWeek('{$fecha}'),
									fecha = '{$fecha}',
									cinta = getCinta('{$cinta}'),
									edad = getEdadCinta(getWeek('{$fecha}'), getCinta('{$cinta}'), 2018),
									tipo = 'PROC',
									peso = {$peso_prom}";
						$this->DD($sql);


						for($i = 1; $i <= $value; $i++)
							$this->db->query($sql);
					}
				}
			}*/

			// CAJAS

			/*$cajas = [];
			for($j = 1; $j <= 15; $j++){
				$fila = $rows[9+$j];

				$marca = str_replace("'"," ",trim($fila[0]));
				if(strpos($marca, "TOTAL") === false)
				for($i = 1; $i <= 7; $i++){
					$fecha = $this->db->queryOne("SELECT DATE_SUB(STR_TO_DATE('2018{$semana} {$days[$i-1]}', '%X%V %W'), INTERVAL 1 WEEK)");
					$valor = $fila[$i+1];
					$cajas[$fecha][$marca] = $valor;
				}
			}

			foreach($cajas as $fecha => $values) {
				foreach($values as $marca => $value){
					if($value > 0){
						$sql = "INSERT INTO produccion_cajas SET
									year = 2018,
									id_finca = 1,
									finca = 'AGRICOLA SEBASTIAN',
									semana = getWeek('{$fecha}'),
									fecha = '{$fecha}',
									marca = '{$marca}',
									lb = 43,
									convertidas = round(43/41.5, 2)";
						$this->DD($sql);


						for($i = 1; $i <= $value; $i++)
							$this->db->query($sql);
					}
				}
			}*/


			// RECUSADOS DE CADA DIA
			$fila_recu = $rows[7];
			for($i = 1; $i <= 7; $i++){
				$fecha = $this->db->queryOne("SELECT DATE_SUB(STR_TO_DATE('2018{$semana} {$days[$i-1]}', '%X%V %W'), INTERVAL 1 WEEK)");
				$recu = (int) $fila_recu[$i+1];
				
				if($recu > 0)
					$this->db->query("UPDATE produccion_racimos_formularios SET tipo = 'RECU' WHERE fecha = '{$fecha}' LIMIT {$recu}");
			}
		}
	}

	private function enfundeMarun($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key >= 3){
					$lote = $fila[0];

					if($lote != '')
					for($i = 1; $i <= 52; $i++){
						$val = (int) $fila[$i];
						if($val > 0){
							$sql = "INSERT INTO produccion_enfunde SET
										lote = '{$lote}',
										id_finca = 1,
										id_enfundador = 1,
										semana = $i,
										years = 2017,
										usadas = {$val},
										entregadas = {$val}";
							$this->db->query($sql);
							$this->DD($sql);
						}
					}
				}
			}
		}
	}

	private function insertarCinta($cinta, $edad, $cantidad, $recu, $fecha){
		for($x = 1; $x <= $cantidad; $x++){
			$tipo = 'PROC';
			if($x <= $recu) $tipo = 'RECU';

			$sql = "INSERT INTO produccion_racimos SET
						fecha = '{$fecha}',
						id_finca = 4,
						finca = 'Viviana',
						semana =  getWeek('{$fecha}'),
						year = YEAR('{$fecha}'),
						semana_enfundada = getSemanaEnfundada({$edad}, getWeek('{$fecha}'), YEAR('{$fecha}')),
						cinta = '{$cinta}',
						edad = '{$edad}',
						tipo = '$tipo'";
			$this->db->query($sql);
		}
	}

	private function cajasMarun($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$libros = $xlsx->sheetNames();

		$fechas = ['', '','','','','',''];
		$id_finca = null;
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			if($name == 'CAROLINA') $id_finca = 1;
			if($name == 'VIVIANA') $id_finca = 4;

			foreach($rows as $key => $fila){
				$marca = trim($fila[0]);
				if($marca == 'COMPAÑÍA TROPICAL FRUIT'){
					for($x =  4; $x <= 10; $x++){
						$fecha = $fila[$x];
						if($fecha != ''){
							$UNIX_DATE = ($fecha - 25569) * 86400;
							$fecha = gmdate("Y-m-d", $UNIX_DATE);
							$fechas[$x-4] = $fecha;
						}else{
							$fechas[$x-4] = '';
						}
					}
					$this->DD($fechas);
				}
				else if($marca != ''){
					for($x =  4; $x <= 10; $x++){
						$cajas = (int) $fila[$x];
						if($cajas > 0){
							$sql = "INSERT INTO produccion_cajas SET
										marca = '{$marca}',
										id_finca = $id_finca,
										fecha = '{$fechas[$x-4]}',
										semana = getWeek('{$fechas[$x-4]}'),
										year = 2018,
										lb = $fila[1],
										kg = $fila[1]*0.4536,
										convertidas_415 = $fila[1]/41.5
										;";
							for($i = 1; $i <= $cajas ; $i++){
								$this->db->query($sql);
							}
							$this->DD($sql);
						}
					}
				}
			}
		}
	}

	private function enfundeSumifru($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);

			$finca = $name;
			if($name == 'SUERTE') $finca = 'LA SUERTE';

			foreach($rows as $key => $fila){
				if($key > 0){
					$lote = $fila[0];
					if($lote != '' && strpos($lote, "Sector") === false && strpos($lote, "Total") === false){
						for($x = 1; $x <= 34; $x++){
							$val = $fila[$x+1];
							if($val > 0){
								$limit = 1;
								if($finca == 'SAN LUIS'){
									if($key > 15) $limit = "1,1";
								}

								$sql = "INSERT INTO produccion_enfunde SET
											id_finca = (SELECT fincas.id FROM fincas INNER JOIN lotes ON idFinca = fincas.id AND lotes.nombre = '{$lote}' WHERE fincas.sector = '{$finca}' AND id_empacadora IS NOT NULL LIMIT {$limit}),
											fecha = STR_TO_DATE('2018{$x} Monday', '%X%V %W'),
											years = 2018,
											semana = $x,
											usadas = {$val},
											entregadas = {$val},
											lote = '{$lote}'";
								$this->DD($sql);
								$this->db->query($sql);
							}
						}
					}
				}
			}
		}
	}

	private function readerAgroaereo($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				$this->DD($fila);
				if($key > 0){
					$sql = "SELECT id FROM fincas WHERE nombre = '{$fila[0]}'";
					$id_finca = (int) $this->db->queryOne($sql);
					if(!$id_finca){
						$this->db->query("INSERT INTO fincas SET nombre = '{$fila[0]}'");
						$id_finca = $this->db->getLastID();
					}

					$sql = "SELECT id FROM lotes WHERE nombre = '{$fila[2]}'";
					$id_lote = (int) $this->db->query($sql);
					if(!$id_lote == 0){
						$sql = "INSERT INTO lotes SET idFinca = $id_finca, nombre = '{$fila[2]}', edad_palma = '{$fila[5]}', hectareas = '{$fila[3]}', palmas = '{$fila[4]}'";
						D($sql);
						$this->db->query($sql);
					}
				}
			}
		}
	}

	private function driveSeimalsa($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$fila_0 = trim($fila[0]);
					$fila_1 = trim($fila[1]);
					$fila_2 = trim($fila[2]);
					$fila_3 = trim($fila[3]);
					$fila_4 = trim($fila[4]);
					$fila_5 = trim($fila[5]);
					$fila_6 = trim($fila[6]);// fecha
					$fila_7 = trim($fila[7]);// fecha
					$fila_8 = trim($fila[8]);// fecha
					$fila_9 = trim($fila[9]);// fecha
					$fila_10 = trim($fila[10]);// fecha
					$fila_11 = trim($fila[11]);
					$fila_12 = trim($fila[12]);
					$fila_13 = trim($fila[13]);
					$fila_14 = trim($fila[14]);
					$fila_15 = trim($fila[15]);
					$fila_16 = trim($fila[16]);
					$fila_17 = trim($fila[17]);
					$fila_18 = trim($fila[18]);
					$fila_19 = trim($fila[19]);
					$fila_20 = trim($fila[20]);
					$fila_21 = trim($fila[21]);
					$fila_22 = trim($fila[22]);
					$fila_23 = trim($fila[23]);
					$fila_24 = trim($fila[24]);
					$fila_25 = trim($fila[25]);
					$fila_26 = trim($fila[26]);
					$fila_27 = trim($fila[27]);
					$fila_28 = trim($fila[28]);// fecha
					$fila_29 = trim($fila[29]);// fecha
					$fila_30 = trim($fila[30]);// fecha
					$fila_31 = trim($fila[31]);// hora
					$fila_32 = trim($fila[32]);
					$fila_33 = trim($fila[33]);
					$fila_34 = trim($fila[34]);
					$fila_35 = trim($fila[35]);
					$fila_36 = trim($fila[36]);
					$fila_37 = trim($fila[37]);
					$fila_38 = trim($fila[38]);// fecha
					$fila_39 = trim($fila[39]);// fecha
					$fila_40 = trim($fila[40]);// hora
					$fila_41 = trim($fila[41]);// hora
					$fila_42 = trim($fila[42]);// hora
					$fila_43 = trim($fila[43]);
					$fila_44 = trim($fila[44]);
					$fila_45 = trim($fila[45]);
					$fila_46 = trim($fila[46]);

                    if(is_numeric($fila_6)){
						$UNIX_DATE = ($fila_6 - 25569) * 86400;
						$fila_6 = gmdate("Y-m-d", $UNIX_DATE);
					}
					if(is_numeric($fila_7)){
						$UNIX_DATE = ($fila_7 - 25569) * 86400;
						$fila_7 = gmdate("Y-m-d", $UNIX_DATE);
					}
					if(is_numeric($fila_8)){
						$UNIX_DATE = ($fila_8 - 25569) * 86400;
						$fila_8 = gmdate("Y-m-d", $UNIX_DATE);
					}
					if(is_numeric($fila_9)){
						$UNIX_DATE = ($fila_9 - 25569) * 86400;
						$fila_9 = gmdate("Y-m-d", $UNIX_DATE);
					}
					if(is_numeric($fila_10)){
						$UNIX_DATE = ($fila_10 - 25569) * 86400;
						$fila_10 = gmdate("Y-m-d", $UNIX_DATE);
					}
					if(is_numeric($fila_28)){
						$UNIX_DATE = ($fila_28 - 25569) * 86400;
						$fila_28 = gmdate("Y-m-d", $UNIX_DATE);
					}
					if(is_numeric($fila_29)){
						$UNIX_DATE = ($fila_29 - 25569) * 86400;
						$fila_29 = gmdate("Y-m-d", $UNIX_DATE);
					}
					if(is_numeric($fila_30)){
						$UNIX_DATE = ($fila_30 - 25569) * 86400;
						$fila_30 = gmdate("Y-m-d", $UNIX_DATE);
					}
					if(is_numeric($fila_38)){
						$UNIX_DATE = ($fila_38 - 25569) * 86400;
						$fila_38 = gmdate("Y-m-d", $UNIX_DATE);
					}
					if(is_numeric($fila_39)){
						$UNIX_DATE = ($fila_39 - 25569) * 86400;
						$fila_39 = gmdate("Y-m-d", $UNIX_DATE);
					}

					if($fila_4 != ""){
						$sql = "SELECT COUNT(1) AS item FROM reportes_tm_format WHERE item = '{$fila_4}'";
						$items = $this->db->queryOne($sql);
						
						if($items > 0){
							$where = " WHERE item = '{$fila_4}'";
						}
						
						$sql = "INSERT INTO reportes_tm_format
								SET 
								estatus = '{$fila_0}',
								sucursal = '{$fila_1}',
								cliente =  '{$fila_2}',
								marca = '{$fila_3}',
								item = '{$fila_4}',
								mes = '{$fila_5}',
								fecha_contacto_pepsi = '{$fila_6}',
								fecha_recepcion_pepsi = '{$fila_7}',
								fecha_autorizacion_pepsi = '{$fila_8}',
								fecha_entrega_pepsi = '{$fila_9}',
								fecha_compromiso_pepsi = '{$fila_10}',
								ruta = '{$fila_11}',
								codigo_cliente = '{$fila_12}',
								coordinador = '{$fila_13}',
								oficina_ventas = '{$fila_14}',
								agencia = '{$fila_15}',
								region_ventas = '{$fila_16}',
								nombre_pdv = '{$fila_17}',
								nombre_cliente = '{$fila_18}',
								direccion = '{$fila_19}',
								tipo_equipo = '{$fila_20}',
								tipo_operacion = '{$fila_21}',
								status_equipo = '{$fila_22}',
								modelo = '{$fila_23}',
								serie = '{$fila_24}',
								activo = '{$fila_25}',
								bol = '{$fila_26}',
								comodato = '{$fila_27}',
								fecha_entrega = '{$fila_28}',
								fecha_recepcion_papeleria = '{$fila_29}',
								fecha_ejecutado = '{$fila_30}',
								hora_ejecucion = '{$fila_31}',
								piloto_responsable = '{$fila_32}',
								responsable_transporte = '{$fila_33}',
								status_movimiento = '{$fila_34}',
								motivo_movimiento = '{$fila_35}',
								motivo_fletes = '{$fila_36}',
								status_caso = '{$fila_37}',
								fecha_recibido = '{$fila_38}',
								fecha_ejecutado_copy = '{$fila_39}',
								hora_recibido = '{$fila_40}',
								hora_recibido_copy = '{$fila_41}',
								hora_ejecutado = '{$fila_42}',
								real_horas = '{$fila_43}',
								real_dias = '{$fila_44}',
								rango_tiempo_resp = '{$fila_45}',
								status_movimiento_copy = '{$fila_46}'
								$where";				
						if($items > 0){
							$sql = str_replace("INSERT INTO", "UPDATE", $sql);
						}
						D($sql);
						//$this->db->query($sql);
					}
				}
			}
		}
	}

	private function seimalsaInventario($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$fecha = $fila[12];
					$serie = $fila[4];
					$placa = $fila[4];
					$id_marca_modelo = $fila[1]." - ".$fila[3];
					$ubicacion_excel = $fila[9];
					$ubicacion = "";
					$insertado_desde = "";

					if($fecha != ''){
						if(is_numeric($fecha)){
							$UNIX_DATE = ($fecha - 25569) * 86400;
							$fecha = gmdate("Y-m-d", $UNIX_DATE);
						} else {
							$fecha = explode('-', $fecha)[2]."-".explode('-', $fecha)[1]."-".explode('-', $fecha)[0];
						}
					} else {
						$fecha = "CURRENT_DATE()";
					}

					if($ubicacion_excel == 'BAJA' && $ubicacion_excel != 'REPARACION'){
						$insertado_desde = "EXCEL - EQUIPO DE BAJA - DISPONIBLE";
						$ubicacion = '12';

						$sql = "SELECT serie FROM inventario_equipos WHERE insertado_desde = 'EQUIPO DE BAJA - TERCEROS' OR insertado_desde = 'EQUIPO DE BAJA - EN TRANSITO'";
						$result = $this->db->queryAll($sql);

						if(in_array($serie, $result)){
							$status = 'Inactivo';
						} else {
							$status = 'Activo';
						}

					} else if($ubicacion_excel == 'LISTO' && $ubicacion_excel != 'REPARACION'){
						$insertado_desde = "EXCEL - DESPACHO DE EQUIPOS - DISPONIBLE";
						$ubicacion = '9';

						$sql = "SELECT serie FROM inventario_equipos WHERE insertado_desde = 'DESPACHO DE EQUIPOS - EN TRANSITO' OR insertado_desde = 'DESPACHO DE EQUIPOS - TERCEROS'";
						$result = $this->db->queryAll($sql);

						if(in_array($serie, $result)){
							$status = 'Inactivo';
						} else {
							$status = 'Activo';
						}

					} else if($ubicacion_excel == 'SEMILISTOS' && $ubicacion_excel != 'REPARACION'){
						$insertado_desde = "EXCEL - SEMILISTO";
						$ubicacion = '11';

						$sql = "SELECT serie FROM inventario_equipos WHERE insertado_desde = 'SEMILISTO'";
						$result = $this->db->queryAll($sql);

						if(in_array($serie, $result)){
							$status = 'Inactivo';
						} else {
							$status = 'Activo';
						}
					}

					if($fecha == 'CURRENT_DATE()'){
						$sql = "INSERT INTO inventario_equipos 
								SET
								id_sucursal = '1', 
								fecha = {$fecha}, 
								hora = CURRENT_TIME, 
								id_proceso_ingreso = 0, 
								serie = '{$serie}', 
								placa = '{$placa}', 
								id_marca_modelo = '{$id_marca_modelo}', 
								cliente = 'TESALIA CBC', 
								id_cliente = 1, 
								ubicacion = '{$ubicacion}', 
								id_insertado_desde = 0, 
								insertado_desde = '{$insertado_desde}', 
								otros_servicios =  'NO', 
								status = '{$status}'; ";
						$this->DD($sql);
						//$this->db->query($sql);
					} else {
						/*
							$sql = "INSERT INTO inventario_equipos 
									SET
									id_sucursal = '1', 
									fecha = '{$fecha}', 
									hora = CURRENT_TIME, 
									id_proceso_ingreso = 0, 
									serie = '{$serie}', 
									placa = '{$placa}', 
									id_marca_modelo = '{$id_marca_modelo}', 
									cliente = 'TESALIA CBC', 
									id_cliente = 1, 
									ubicacion = '{$ubicacion}', 
									id_insertado_desde = 0, 
									insertado_desde = '{$insertado_desde}', 
									otros_servicios =  'NO', 
									status = '{$status}'; ";
							$this->DD($sql);
							//$this->db->query($sql);
						*/
					}
				}
			}
		}
	}

	private function seimalsaInventario2($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$fecha = "CURRENT_DATE()";
					$serie = $fila[1];
					$placa = $fila[1];
					$id_marca_modelo = $fila[3]." - ".$fila[2];
					$ubicacion_excel = $fila[0];
					$observaciones = $fila[4];
					$ubicacion = "";
					$insertado_desde = "";

					if($ubicacion_excel == 'BAJA'){
						$insertado_desde = "EXCEL / EQUIPO DE BAJA - DISPONIBLE";
						$ubicacion = '12';

						$sql = "INSERT INTO inventario_equipos 
								SET
								id_sucursal = '1', 
								fecha = {$fecha}, 
								hora = CURRENT_TIME, 
								id_proceso_ingreso = 0, 
								serie = '{$serie}', 
								placa = '{$placa}', 
								id_marca_modelo = '{$id_marca_modelo}', 
								cliente = 'BIELESA', 
								id_cliente = 1, 
								ubicacion = '{$ubicacion}', 
								id_insertado_desde = 0, 
								insertado_desde = '{$insertado_desde}', 
								otros_servicios =  'NO', 
								observaciones = '{$observaciones}',
								status = 'Activo';";
						$this->DD($sql);
						//$this->db->query($sql);

					} else if($ubicacion_excel == 'EVALUACION'){
						$insertado_desde = "EXCEL / EVALUACION";
						$ubicacion = '3';

						$sql = "INSERT INTO inventario_equipos 
								SET
								id_sucursal = '1', 
								fecha = {$fecha}, 
								hora = CURRENT_TIME, 
								id_proceso_ingreso = 0, 
								serie = '{$serie}', 
								placa = '{$placa}', 
								id_marca_modelo = '{$id_marca_modelo}', 
								cliente = 'BIELESA', 
								id_cliente = 1, 
								ubicacion = '{$ubicacion}', 
								id_insertado_desde = 0, 
								insertado_desde = '{$insertado_desde}', 
								otros_servicios =  'NO', 
								observaciones = '{$observaciones}',
								status = 'Activo';";
						$this->DD($sql);
						//$this->db->query($sql);

					} else if($ubicacion_excel == 'SEMILISTO'){
						$insertado_desde = "EXCEL / SEMILISTO";
						$ubicacion = '11';

						$sql = "INSERT INTO inventario_equipos 
								SET
								id_sucursal = '1', 
								fecha = {$fecha}, 
								hora = CURRENT_TIME, 
								id_proceso_ingreso = 0, 
								serie = '{$serie}', 
								placa = '{$placa}', 
								id_marca_modelo = '{$id_marca_modelo}', 
								cliente = 'BIELESA', 
								id_cliente = 1, 
								ubicacion = '{$ubicacion}', 
								id_insertado_desde = 0, 
								insertado_desde = '{$insertado_desde}', 
								otros_servicios =  'NO',
								observaciones = '{$observaciones}', 
								status = 'Activo';";
						$this->DD($sql);
						//$this->db->query($sql);

					} else if($ubicacion_excel == 'DISPONIBLE'){
						$insertado_desde = "EXCEL / DESPACHO DE EQUIPOS - DISPONIBLE";
						$ubicacion = '9';

						$sql = "INSERT INTO inventario_equipos 
								SET
								id_sucursal = '1', 
								fecha = {$fecha}, 
								hora = CURRENT_TIME, 
								id_proceso_ingreso = 0, 
								serie = '{$serie}', 
								placa = '{$placa}', 
								id_marca_modelo = '{$id_marca_modelo}', 
								cliente = 'BIELESA', 
								id_cliente = 1, 
								ubicacion = '{$ubicacion}', 
								id_insertado_desde = 0, 
								insertado_desde = '{$insertado_desde}', 
								otros_servicios =  'NO',
								observaciones = '{$observaciones}', 
								status = 'Activo';";
						$this->DD($sql);
						//$this->db->query($sql);

					}
				}
			}
		}
	}

	private function cajasClementina($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 2){
					$fecha = $fila[0];
					if(is_numeric($fecha)){
						$UNIX_DATE = ($fecha - 25569) * 86400;
						$fecha = gmdate("Y-m-d", $UNIX_DATE);
					}
					$cantidad = $fila[2];
					$marca = $fila[3];
					
					for($i = 1; $i <= $cantidad; $i++){
						$sql = "INSERT INTO produccion_cajas SET
		fecha = '{$fecha}',
		year = 2018,
		semana = getWeek('{$fecha}'),
		id_finca = 1,
		finca = 'Patricia',
		marca = '{$marca}'";
						$this->db->query($sql);
					}
					$this->DD($sql);
				}
			}
		}
	}

	private function racimosClementina($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		$cintas = ['VERDE', 'AZUL', 'BLANCA', 'NEGRA', 'LILA', 'ROJA', 'AMARILLA', 'CAFE'];
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 2){
					$fecha = $fila[0];
					if(is_numeric($fecha)){
						$UNIX_DATE = ($fecha - 25569) * 86400;
						$fecha = gmdate("Y-m-d", $UNIX_DATE);
					}
					$cantidad = $fila[2];
					$cinta = $fila[3];
					if($cinta == 'Amarilla') $cinta = 'AMARILLO';
					if($cinta == 'Roja') $cinta = 'ROJO';
					if($cinta == 'Café') $cinta = 'CAFE';
					if($cinta == 'Negra') $cinta = 'NEGRO';
					$peso = $fila[4];
					$tipo = $fila[8] == 'Procesado' ? 'PROC' : 'RECUSADO';
					$calibre = $tipo == 'PROC' ? $fila[5] : 'NULL';
					$manos = $tipo == 'PROC' ? $fila[6] : 'NULL';
					$causa = $tipo == 'PROC' ? '' : $fila[9];
					
					for($i = 1; $i <= $cantidad; $i++){
						$sql = "INSERT INTO produccion_historica SET
		fecha = '{$fecha}',
		year = 2018,
		semana = getWeek('{$fecha}'),
		id_finca = 1,
		finca = 'Patricia',
		cinta = '{$cinta}',
		peso = $peso,
		calibre = $calibre,
		manos = $manos,
		tipo = '{$tipo}',
		causa = '{$causa}'";
						#$this->db->query($sql);
					}
					$this->DD($sql);
				}
			}
		}
	}

	private function pesosMarun($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$sql = "INSERT INTO balanza_agrosoft_cacao SET
								fecha = '2018-07-31',
								codigo = '{$fila[1]}',
								sector = '{$fila[2]}',
								lote = '{$fila[3]}',
								peso = '{$fila[5]}'";
					$this->DD($sql);
					#$this->db->query($sql);
				}
			}
		}
	}

	private function diff($current, $last){
		$e = false;
		if($current->grupo_racimo != $last->grupo_racimo) return true;
		return $e;
    }

	private function cintasCaidasMarcel($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		$cintas = ['VERDE', 'AZUL', 'BLANCA', 'NEGRA', 'LILA', 'ROJA', 'AMARILLA', 'CAFE'];
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			$lote = '';
			foreach($rows as $key => $fila){
				if($key > 0){
					if(strpos($fila[4], 'SEC#') !== false){
						$lote = (int) str_replace('SEC#', '', $fila[4]);
					}
					if(in_array(strtoupper(trim($fila[11])), $cintas)){
						$racimoscaidos = (int) $fila[21];
						$cinta = strtoupper(trim($fila[11]));
						$semana = (int) $fila[0];
						$this->insertRacimosCaidos($semana, 2017, $cinta, $lote, $racimoscaidos);
					}
				}
			}
		}
	}

	private function insertRacimosCaidos($sem, $anio, $cinta, $lote, $cantidad){
		$sql = "INSERT INTO produccion_racimos_caidos SET
					semana_enfundada = $sem,
					anio_enfundado = $anio,
					cinta = '$cinta',
					lote = '$lote',
					cantidad = $cantidad";
		$this->db->query($sql);
	}

	private function historicoCacao($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					if(in_array($key, [6,7,8,9,10,11,12,13,14,15,16,17,18])){
						$periodo = $key-5;
						$this->insertDataSectorPeriodo($periodo, 1, 2008, $fila[2]);
						$this->insertDataSectorPeriodo($periodo, 1, 2009, $fila[3]);
						$this->insertDataSectorPeriodo($periodo, 1, 2010, $fila[4]);
						$this->insertDataSectorPeriodo($periodo, 1, 2011, $fila[5]);
						$this->insertDataSectorPeriodo($periodo, 1, 2012, $fila[6]);
						$this->insertDataSectorPeriodo($periodo, 1, 2013, $fila[7]);
						$this->insertDataSectorPeriodo($periodo, 1, 2014, $fila[8]);
						$this->insertDataSectorPeriodo($periodo, 1, 2015, $fila[9]);
						$this->insertDataSectorPeriodo($periodo, 1, 2016, $fila[10]);
						$this->insertDataSectorPeriodo($periodo, 1, 2017, $fila[11]);
						$this->insertDataSectorPeriodo($periodo, 1, 2018, $fila[12]);

						$this->insertDataSectorPeriodo($periodo, 2, 2008, $fila[13]);
						$this->insertDataSectorPeriodo($periodo, 2, 2009, $fila[14]);
						$this->insertDataSectorPeriodo($periodo, 2, 2010, $fila[15]);
						$this->insertDataSectorPeriodo($periodo, 2, 2011, $fila[16]);
						$this->insertDataSectorPeriodo($periodo, 2, 2012, $fila[17]);
						$this->insertDataSectorPeriodo($periodo, 2, 2013, $fila[18]);
						$this->insertDataSectorPeriodo($periodo, 2, 2014, $fila[19]);
						$this->insertDataSectorPeriodo($periodo, 2, 2015, $fila[20]);
						$this->insertDataSectorPeriodo($periodo, 2, 2016, $fila[21]);
						$this->insertDataSectorPeriodo($periodo, 2, 2017, $fila[22]);
						$this->insertDataSectorPeriodo($periodo, 2, 2018, $fila[23]);
					}

					if(in_array($key, [26,27,28,29,30,31,32,33,34,35,36,37,38])){
						$periodo = $key-25;
						$this->insertDataSectorPeriodo($periodo, 3, 2008, $fila[2]);
						$this->insertDataSectorPeriodo($periodo, 3, 2009, $fila[3]);
						$this->insertDataSectorPeriodo($periodo, 3, 2010, $fila[4]);
						$this->insertDataSectorPeriodo($periodo, 3, 2011, $fila[5]);
						$this->insertDataSectorPeriodo($periodo, 3, 2012, $fila[6]);
						$this->insertDataSectorPeriodo($periodo, 3, 2013, $fila[7]);
						$this->insertDataSectorPeriodo($periodo, 3, 2014, $fila[8]);
						$this->insertDataSectorPeriodo($periodo, 3, 2015, $fila[9]);
						$this->insertDataSectorPeriodo($periodo, 3, 2016, $fila[10]);
						$this->insertDataSectorPeriodo($periodo, 3, 2017, $fila[11]);
						$this->insertDataSectorPeriodo($periodo, 3, 2018, $fila[12]);

						$this->insertDataSectorPeriodo($periodo, 5, 2008, $fila[13]);
						$this->insertDataSectorPeriodo($periodo, 5, 2009, $fila[14]);
						$this->insertDataSectorPeriodo($periodo, 5, 2010, $fila[15]);
						$this->insertDataSectorPeriodo($periodo, 5, 2011, $fila[16]);
						$this->insertDataSectorPeriodo($periodo, 5, 2012, $fila[17]);
						$this->insertDataSectorPeriodo($periodo, 5, 2013, $fila[18]);
						$this->insertDataSectorPeriodo($periodo, 5, 2014, $fila[19]);
						$this->insertDataSectorPeriodo($periodo, 5, 2015, $fila[20]);
						$this->insertDataSectorPeriodo($periodo, 5, 2016, $fila[21]);
						$this->insertDataSectorPeriodo($periodo, 5, 2017, $fila[22]);
						$this->insertDataSectorPeriodo($periodo, 5, 2018, $fila[23]);
					}
				}
			}
		}
	}

	private function insertDataSectorPeriodo($periodo, $sector, $anio, $cantidad){
		$cantidad = (float) $cantidad;
		if($cantidad > 0){
			$sql = "INSERT INTO resumen_cosecha_periodo_sector SET
						periodo = $periodo,
						id_sector = (SELECT id FROM cat_sectores WHERE nombre = '{$sector}'),
						anio = $anio,
						cantidad = ROUND($cantidad * 100 * 0.4536, 4)";
			$this->DD($sql);
			$this->db->query($sql);
		}
	}

	private function socialbalance($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$id = trim($fila[1]);
					$fecha = trim($fila[2]);
					$lugar = trim($fila[3]);
					$edad = trim($fila[4]);
					$genero = trim($fila[5]);
					$barrio = trim($fila[6]);
					$vivienda = trim($fila[7]);
					$miembros_familiares = trim($fila[8]);
					$actividad = trim($fila[9]);
					$ingresos_familiares = trim($fila[10]);
					$familiares_discapacidad = trim($fila[11]);
					$servicios_basicos = trim($fila[12]);
					$nivel_estudios = trim($fila[13]);
					$pregunta_1 = trim($fila[14]);
					$pregunta_2 = trim($fila[15]);
					$pregunta_3 = trim($fila[16]);
					$pregunta_4 = trim($fila[17]);
					$pregunta_5 = trim($fila[18]);
					$pregunta_6_1 = trim($fila[19]);
					$pregunta_6_2 = trim($fila[20]);
					$pregunta_6_3 = trim($fila[21]);
					$pregunta_6_4 = trim($fila[22]);
					$pregunta_6_5 = trim($fila[23]);
					$pregunta_7_1 = trim($fila[24]);
					$pregunta_7_2 = trim($fila[25]);
					$pregunta_7_3 = trim($fila[26]);
					$pregunta_8_1 = trim($fila[27]);
					$pregunta_8_2 = trim($fila[28]);
					$pregunta_8_3 = trim($fila[29]);
					$pregunta_8_4 = trim($fila[30]);
					$pregunta_8_5 = trim($fila[31]);
					$pregunta_8_6 = trim($fila[32]);
					$pregunta_8_7 = trim($fila[33]);
					$pregunta_9_1 = trim($fila[34]);
					$pregunta_9_2 = trim($fila[35]);
					$pregunta_9_3 = trim($fila[36]);
					$pregunta_10 = trim($fila[37]);
					$pregunta_11_1 = trim($fila[38]);
					$pregunta_11_2 = trim($fila[39]);
					$pregunta_11_3 = trim($fila[40]);
					$telefono = trim($fila[41]);
					$red_social = trim($fila[42]);
					$pregunta_12 = trim($fila[43]);

					if(is_numeric($fecha)){
						$UNIX_DATE = ($fecha - 25569) * 86400;
						$fecha = gmdate("Y-m-d", $UNIX_DATE);
					}
					
					if($fecha != ""){
						$sql = "UPDATE encuestas
								SET
								fecha = '{$fecha}',
								hora = CURRENT_TIME,
								lugar = '{$lugar}',
								edad = '{$edad}',
								genero = '{$genero}',
								barrio = '{$barrio}',
								vivienda = '{$vivienda}',
								miembros_familia = '{$miembros_familiares}',
								actividad = '{$actividad}',
								ingresos_familiares = '{$ingresos_familiares}',
								familiares_discapacidad = '{$familiares_discapacidad}',
								servicios_basicos = '{$servicios_basicos}',
								nivel_estudios = '{$nivel_estudios}',
								pregunta_uno = '{$pregunta_1}',
								pregunta_dos = '{$pregunta_2}',
								pregunta_tres = '{$pregunta_3}',
								pregunta_cuatro = '{$pregunta_4}',
								pregunta_cinco = '{$pregunta_5}',
								pregunta_seis_uno = '{$pregunta_6_1}',
								pregunta_seis_dos = '{$pregunta_6_2}',
								pregunta_seis_tres = '{$pregunta_6_3}',
								pregunta_seis_cuatro = '{$pregunta_6_4}',
								pregunta_seis_cinco = '{$pregunta_6_5}',
								pregunta_siete = '{$pregunta_7_1}',
								pregunta_siete_nombre_grupo = '{$pregunta_7_2}',
								pregunta_siete_cargo_funcion = '{$pregunta_7_3}',
								pregunta_ocho_junta_parroquial = '{$pregunta_8_1}',
								pregunta_ocho_junta_civica = '{$pregunta_8_2}',
								pregunta_ocho_iglesia = '{$pregunta_8_3}',
								pregunta_ocho_seguridad = '{$pregunta_8_4}',
								pregunta_ocho_municipio = '{$pregunta_8_5}',
								pregunta_ocho_perfectura = '{$pregunta_8_6}',
								pregunta_ocho_actor_importante = '{$pregunta_8_7}',
								pregunta_nueve_uno = '{$pregunta_9_1}',
								pregunta_nueve_dos = '{$pregunta_9_2}',
								pregunta_nueve_tres = '{$pregunta_9_3}',
								pregunta_diez = '{$pregunta_10}',
								pregunta_once_uno = '{$pregunta_11_1}',
								pregunta_once_dos = '{$pregunta_11_2}',
								pregunta_once_tres = '{$pregunta_11_3}',
								telefono = '{$telefono}',
								facebook_twitter = '{$red_social}',
								recibir_noticias = '{$pregunta_12}'
						WHERE id = '{$id}'";
						$this->DD($sql);
						//$this->db->query($sql);
					}
				}
			}
		}
	}

	private function enfundePalmar($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){

					$fecha = $fila[1];
					if(is_numeric($fecha)){
						$UNIX_DATE = ($fecha - 25569) * 86400;
						$fecha = gmdate("Y-m-d", $UNIX_DATE);
					}
					$lote = (int) $fila[3];

					$sql = "INSERT INTO produccion_enfunde SET	
								id_finca = 1,
								years = 2018,
								fecha = DATE('{$fecha}'),
								semana = {$fila[5]},
								cinta = (SELECT color FROM semanas_colores WHERE year = 2018 AND semana = {$fila[5]}),
								usadas = $fila[7],
								entregadas = $fila[7],
								lote = '{$lote}'";
					#$this->DD($sql);
					#$this->db->query($sql);
				}
			}
		}
	}

	private function seimalsaRepuestos($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$descripcion = trim($fila[0]);
					$codigo = trim($fila[1]);
					$repuesto = trim($fila[2]);

					if(is_numeric($fecha)){
						$UNIX_DATE = ($fecha - 25569) * 86400;
						$fecha = gmdate("Y-m-d", $UNIX_DATE);
					}
					
					if($descripcion != ""){
						$sql = "INSERT INTO cat_repuestos
								SET 
								codigo = '{$codigo}',
								descripcion = '{$descripcion}',
								repuesto = '".addslashes($repuesto)."';";
						$this->DD($sql);
						//$this->db->query($sql);
					}
				}
			}
		}
	}

	private function racimosCosechadosSumifru($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();

		$marcascajas = [
			'JAPON 32P'
			,'JAPON 25P'
			,'JAPON 28P'
			,'JAPON CQ 32P'
			,'JAPON TV 32P'
			,'Japón  manos (como China)'
			,'JAPON 25P FARMIRD'
			,'JAPON 32P FARMIRD'
			,'JAPON 25P N FARMIRD'
			,'JAPON 40P 2 FARMIRD'
			,'COREA 10P'
			,'COREA 8P'
			,'JAPON 25P N'
			,'COREA 9P'
			,'DOMENICA'
			,'BOOGUIE'
			,'BANASORF'
			,'TASTY'
			,'NATURES GEM'
			,'FAVORITA'
			,'YELLOW'
			,'BANAGOLD'
			,'DELIBAN'
			,'EXTRABAN'
			,'KAIDE BANANA'
			,'INTERBANANA'
			,'VICTORIA'
		];
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			$semana = 0;
			$dia = "";
			$day = "";
			$date = "";
			$resumen = 0;
			$cajas = 0;

			if($name == "Inf. Cosecha")
			foreach($rows as $key => $fila){

				if($key == 3){
					$semana = (int) $fila[21];
					$sem = $semana - 1;
				}

				if(in_array($fila[1], ["LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO", "DOMINGO"])){
					$dia = $fila[1];
					if($dia == "LUNES") $day = "Monday";
					if($dia == "MARTES") $day = "Tuesday";
					if($dia == "MIERCOLES") $day = "Wednesday";
					if($dia == "JUEVES") $day = "Thursday";
					if($dia == "VIERNES") $day = "Friday";
					if($dia == "SABADO") $day = "Saturday";
					if($dia == "DOMINGO") $day = "Sunday";
					$date = $this->db->queryOne("SELECT STR_TO_DATE('2018{$sem} {$day}', '%X%V %W');");
				}

				if($fila[2] == "PRODUCCIÓN DE CAJAS DE PRIMERA"){
					$cajas = 1;
				}
				if(in_array(trim($fila[1]), $marcascajas)){
					$this->insertDataCajasSumifru("LA SUERTE", $date, $fila[2], str_replacE(",", ".", trim($fila[0])), trim($fila[1]));
					$this->insertDataCajasSumifru("SAN MARCOS", $date, $fila[3], str_replacE(",", ".", trim($fila[0])), trim($fila[1]));
					$this->insertDataCajasSumifru("MATILDE", $date, $fila[8], str_replacE(",", ".", trim($fila[0])), trim($fila[1]));
					$this->insertDataCajasSumifru("TRIPLE A", $date, $fila[9], str_replacE(",", ".", trim($fila[0])), trim($fila[1]));
					$this->insertDataCajasSumifru("SAN LUIS", $date, $fila[11], str_replacE(",", ".", trim($fila[0])), trim($fila[1]));
					$this->insertDataCajasSumifru("SIGAL", $date, $fila[12], str_replacE(",", ".", trim($fila[0])), trim($fila[1]));
					$this->insertDataCajasSumifru("CLEMENCIA", $date, $fila[16], str_replacE(",", ".", trim($fila[0])), trim($fila[1]));
					$this->insertDataCajasSumifru("DOÑA LUISA", $date, $fila[17], str_replacE(",", ".", trim($fila[0])), trim($fila[1]));
				}

				/*$edad = $fila[0];
				$color = strtoupper ($this->limpiar($fila[1]));
				if(in_array($color, ['AZUL', 'BLANCA', 'NEGRA', 'LILA', 'ROJA', 'CAFE', 'AMARILLA', 'VERDE', 'AZUL'])){
					$lasuerte[$color] = $fila[2];
					$sanmarcos[$color] = $fila[3];
					$matilde[$color] = $fila[8];
					$tiplea[$color] = $fila[9];
					$sanluis[$color] = $fila[11];
					$sigal[$color] = $fila[12];
					$clemencia[$color] = $fila[16];
					$donaluisa[$color] = $fila[17];
				}

				if($fila[0] == "RECHAZADOS"){
					$lasuerte["S/C"] = $fila[2];
					$sanmarcos["S/C"] = $fila[3];
					$matilde["S/C"] = $fila[8];
					$tiplea["S/C"] = $fila[9];
					$sanluis["S/C"] = $fila[11];
					$sigal["S/C"] = $fila[12];
					$clemencia["S/C"] = $fila[16];
					$donaluisa["S/C"] = $fila[17];
				}

				if($fila[0] == "PESO PROM."){
					#$this->insertDataRacimosSumifru($lasuerte, "LA SUERTE", $date, $fila[2]);
					#$this->insertDataRacimosSumifru($sanmarcos, "SAN MARCOS", $date, $fila[3]);
					#$this->insertDataRacimosSumifru($matilde, "MATILDE", $date, $fila[8]);
					$this->insertDataRacimosSumifru($tiplea, "TRIPLE A", $date, $fila[9]);
					#$this->insertDataRacimosSumifru($sanluis, "SAN LUIS", $date, $fila[11]);
					#$this->insertDataRacimosSumifru($sigal, "SIGAL", $date, $fila[12]);
					#$this->insertDataRacimosSumifru($clemencia, "CLEMENCIA", $date, $fila[16]);
					#$this->insertDataRacimosSumifru($donaluisa, "DOÑA LUISA", $date, $fila[17]);
				}

				/*if($fila[0] == "CONSOLIDADO SEMANAL"){
					$resumen = 1;
				}

				if($resumen == 1 && $semana > 0){
					if($fila[0] == "CALIBRACION"){
						$this->actualizarCalibracion("LA SUERTE", $semana, $fila[2]);
						$this->actualizarCalibracion("SAN MARCOS", $semana, $fila[3]);
						$this->actualizarCalibracion("MATILDE", $semana, $fila[8]);
						$this->actualizarCalibracion("TRIPLE A", $semana, $fila[9]);
						$this->actualizarCalibracion("SAN LUIS", $semana, $fila[11]);
						$this->actualizarCalibracion("SIGAL", $semana, $fila[12]);
						$this->actualizarCalibracion("CLEMENCIA", $semana, $fila[16]);
						$this->actualizarCalibracion("DOÑA LUISA", $semana, $fila[17]);
					}
					if($fila[0] == "MANOS PROMEDIO"){
						$this->actualizarManos("LA SUERTE", $semana, $fila[2]);
						$this->actualizarManos("SAN MARCOS", $semana, $fila[3]);
						$this->actualizarManos("MATILDE", $semana, $fila[8]);
						$this->actualizarManos("TRIPLE A", $semana, $fila[9]);
						$this->actualizarManos("SAN LUIS", $semana, $fila[11]);
						$this->actualizarManos("SIGAL", $semana, $fila[12]);
						$this->actualizarManos("CLEMENCIA", $semana, $fila[16]);
						$this->actualizarManos("DOÑA LUISA", $semana, $fila[17]);
					}
				}*/
			}
		}

		echo "
		<script> 
			setTimeout(function(){
				window.location.reload()
			}, 1000)
		</script>";
	}

	private function insertDataCajasSumifru($finca, $fecha, $cantidad, $peso_prom, $marca){
		if($cantidad > 0){
			for($x = 1; $x <= $cantidad; $x++){
				$sql = "INSERT INTO produccion_cajas SET
							finca = '$finca',
							lb = $peso_prom,
							fecha = '{$fecha}',
							marca = '{$marca}',
							year = 2018,
							semana = getWeek('{$fecha}')";
				$this->db->query($sql);
			}
			$this->DD($sql);
		}
	}

	private function actualizarCalibracion($finca, $semana, $value){
		$sql = "UPDATE produccion_resumen_fincas_semanas
				SET calibre_prom = '$value'
				WHERE semana = $semana AND finca = '$finca'";
		$this->db->query($sql);

		$sql = "UPDATE produccion_resumen_fincas
				SET sem_$semana = '$value'
				WHERE finca = '$finca' AND variable = 'CALIBRE PROM'";
		$this->db->query($sql);
	}

	private function actualizarManos($finca, $semana, $value){
		$sql = "UPDATE produccion_resumen_fincas_semanas
				SET manos_prom = '$value'
				WHERE semana = $semana AND finca = '$finca'";
		$this->db->query($sql);

		$sql = "UPDATE produccion_resumen_fincas
				SET sem_$semana = '$value'
				WHERE finca = '$finca' AND variable = 'MANOS PROM'";
		$this->db->query($sql);
	}

	private function insertDataRacimosSumifru($data, $finca, $fecha, $peso_prom){
		foreach($data as $cinta => $cantidad){
			if($cantidad > 0){
				$tipo = "PROC";
				if($cinta == 'S/C') $tipo = "RECU";

				for($x = 1; $x <= $cantidad; $x++){
					$sql = "INSERT INTO produccion_historica SET
								finca = '$finca',
								peso = $peso_prom,
								fecha = '{$fecha}',
								cinta = '{$cinta}',
								tipo = '$tipo',
								year = 2018,
								semana = getWeek('{$fecha}')";
					$this->db->query($sql);
				}
				$this->DD($sql);
			}
		}
	}

	private function cegaservices($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                foreach($fila as $i => $campo){
                    if($campo != ""){
                        $mes = explode(",", $campo);
                        $dia = explode(" ", $mes[0]);
                        $mes = $dia[0];
                        $dia = $dia[1];
                        switch($mes){
                            case 'Jan':
                                $mes = '01';
                                break;
                            case 'Feb':
                                $mes = '02';
                                break;
                            case 'Mar':
                                $mes = '03';
                                break;
                            case 'Apr':
                                $mes = '04';
                                break;
                            case 'May':
                                $mes = '05';
                                break;
							case 'Jun':
                                $mes = '06';
                                break;
                            case 'Jul':
                                $mes = '07';
								break;
							case 'Aug':
                                $mes = '08';
								break;
							case 'Sep':
                                $mes = '09';
								break;
							case 'Oct':
                                $mes = '10';
								break;
							case 'Nov':
                                $mes = '11';
								break;
							case 'Dec':
                                $mes = '12';
                                break;
						}
						
						$año = "2018";
						$fecha = $año.'-'.$mes.'-'.$dia;

						$total = "0.00";
						if($rows[50][$i] != '') $total = $rows[50][$i];
						
						$sql = "INSERT INTO lancofruit_ventas 
								SET
								fecha = '{$fecha}',
								anio = '{$año}',
								mes = '{$mes}',
								semana = WEEK('{$fecha}'),
								total = {$total},
								razon_social = 'Corporacion Lancofruit SA',
								nombre_comercial = 'Corporacion Lancofruit SA',
								ruc = '0791765935001',
								dir_matriz = 'Via Santa Rosa S/N, Frente al Correccional de Menores, 070210, Machala, Ecuador',
								fecha_emision = '{$fecha}',
								razon_social_comprador = 'CONSUMIDOR FINAL',
								identificacion_comprador = '9999999999999',
								total_sin_impuestos = {$total},
								importe_total = {$total},
								moneda = 'DOLAR',
								forma_pago = 1,
								id_ruta = 1,
								pto_emi = '001'";
						$this->DD($sql);
						//$this->db->query($sql);
                    }
                }
                break;
            }
        }
    }

	private function palosantoPersonal($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$nombre = $fila[0];
					$area = $fila[2];
					$grupo = $fila[3];
					$empresa = $fila[4];

					$sql = "SELECT COUNT(1) FROM personal WHERE nombre = '{$nombre}'";
					$e = $this->db->queryOne($sql);

					if(!$e){
						$this->DD("$nombre no existe");
						$this->db->query("INSERT INTO personal SET nombre = '{$nombre}', grupo = '{$grupo}'");
					}
				}
			}
		}
	}

	private function clean($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$cliente = trim($fila[0]);
					$sucursal = trim($fila[1]);
					$localidad = trim($fila[2]);

					if(!in_array($cliente, $cat_clientes)){
						$cat_clientes[] = $cliente;
						$id_cliente = array_search($cliente, $cat_clientes) + 1;
						$this->DD("INSERT INTO cat_clientes SET id = '{$id_cliente}', nombre = UPPER('{$cliente}');");	
					}
					//$this->DD("INSERT INTO cat_clientes_sucursales SET sucursal = UPPER('{$sucursal}'), localidad = UPPER('{$localidad}'), id_cliente = '{$id_cliente}';");
				}
			}
		}
	}

	private function plus($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$cliente = trim($fila[0]);
					$servicio = trim($fila[1]);

					/*$count = $count + 1 ;
					$this->DD("INSERT INTO cat_supervisores SET id = '{$count}', nombre = UPPER('{$nombre}');");
					$this->DD("INSERT INTO cat_supervisor_formularios SET formulario = '{$formulario}', id_supervisor = '{$count}';");*/

					if(!in_array($cliente, $cat_clientes)){
						$cat_clientes[] = $cliente;
						$id_cliente = array_search($cliente, $cat_clientes) + 1;
						$this->DD("INSERT INTO cat_clientes SET id = '{$id_cliente}', nombre = UPPER('{$cliente}');");	
					}
					$this->DD("INSERT INTO cat_clientes_servicios SET servicio = UPPER('{$servicio}'), id_cliente = '{$id_cliente}';");
				}
			}
		}
	}
    
    private function compararMarun($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
		$libros = $xlsx->sheetNames();

        $fecha = null;
        $cajas = 0;
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 5){
                    if(is_numeric($fila[1])){
                        $UNIX_DATE = ($fila[1] - 25569) * 86400;
                        $f = gmdate("Y-m-d", $UNIX_DATE);
                        $valid = true;
                    }
                    if($fecha == null){
                        $fecha = $f;
                    }
                    if($f != $fecha){
                        $cajas_db = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_cajas WHERE fecha = '{$fecha}'");
                        $diff = $cajas_db - $cajas;
                        $this->DD($fecha." $cajas $cajas_db / $diff");

                        $cajas = 0;
                        $fecha = $f;
                    }

                    $cajas += (int) $fila[4];
                }
            }
        }
    }

	private function requerimientosClimat($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$cliente = trim($fila[0]);
					$codigo = trim($fila[1]);
					$nombre = trim($fila[2]);
					$direccion = trim($fila[3]);
					$ciudad = trim($fila[4]);
					$zona = trim($fila[5]);
					$establecimiento = trim($fila[6]);
					$tipo_establecimiento = trim($fila[7]);
					$telefono_uno = trim($fila[8]);
					$telefono_dos = trim($fila[9]);
					$correo = trim($fila[10]);
					$region = trim($fila[11]);
					$provincia = trim($fila[12]);
					$agencia = trim($fila[13]);
					$supervisor_uno = trim($fila[14]);
					$tel_sup_uno = trim($fila[15]);
					$supervisor_dos = trim($fila[16]);
					$tel_sup_dos = trim($fila[17]);
					$serie = trim($fila[18]);
					$placa = trim($fila[19]);
					$marca_modelo = trim($fila[20]);
					$logo = trim($fila[21]);

					$sql = "SELECT id FROM cat_sucursales WHERE sucursal = '{$ciudad}'";
					$id_sucursal = $this->db->queryOne($sql);

					$sql = "INSERT INTO cat_clientes
							SET 
							id_sucursal = '{$id_sucursal}',
							codigo = UPPER('{$codigo}'),
							cliente = UPPER('{$cliente}'),
							nombre = UPPER('{$nombre}'),
							telefono_uno = '{$telefono_uno}',
							telefono_dos = '{$telefono_dos}',
							correo = UPPER('{$correo}'),
							establecimiento = '',
							tipo_establecimiento = '{$tipo_establecimiento_id}'";
					$this->DD($sql);
					$this->db->query($sql);
					$id_cliente = $this->db->getLastID();

					$sql = "INSERT INTO cat_clientes_direcciones
							SET 
							id_cliente = {$id_cliente},
							nombre = UPPER('{$nombre}'),
							direccion = UPPER('{$direccion}'),
							ciudad = UPPER('{$ciudad}'),
							provincia = UPPER('{$provincia}'),
							region = UPPER('{$region}'),
							zona = UPPER('{$agencia}'),
							direccion_principal = 'NO'";
					$this->DD($sql);
					$this->db->query($sql);
					
					$sql = "SELECT id FROM cat_tipo_establecimiento WHERE nombre = '{$tipo_establecimiento}'";
					$tipo_establecimiento_id = $this->db->queryOne($sql);

					if($tipo_establecimiento_id == ""){
						$tipo_establecimiento_id = 0;
						$otro = "otro_establecimiento = UPPER('{$establecimiento}'),";
					}

					$sql = "INSERT INTO reporte_movimientos
							SET 
							id_sucursal = '{$id_sucursal}',
							direccion = UPPER('".addslashes($direccion)."'),
							id_cliente = {$id_cliente},
							ciudad = (SELECT id FROM cat_ciudades WHERE nombre = '{$ciudad}' LIMIT 1),
							cliente = UPPER('".addslashes($cliente)."'),
							codigo_cliente = UPPER('{$codigo}'),
							nombre = UPPER('{$nombre}'),
							establecimiento = '',
							$otro
							tipo_establecimiento = $tipo_establecimiento_id,
							supervisor_uno = UPPER('{$supervisor_uno}'),
							telefono_sup_uno = '{$tel_sup_uno}',
							supervisor_dos = UPPER('{$supervisor_dos}'),
							telefono_sup_dos = '{$tel_sup_dos}',
							serie = UPPER('{$serie}'),
							placa = upper('{$placa}'),
							logo = UPPER('{$logo}'),
							marca_modelo = UPPER('{$marca_modelo}'),
							region = UPPER('{$region}'),
							provincia = UPPER('{$provincia}'),
							status = 'tecnico'";
					$this->DD($sql);
					$this->db->query($sql);

					$sql = "INSERT INTO reporte_tecnico
							SET 
							id_sucursal = '{$id_sucursal}',
							direccion = UPPER('".addslashes($direccion)."'),
							id_cliente = {$id_cliente},
							ciudad = (SELECT id FROM cat_ciudades WHERE nombre = '{$ciudad}' LIMIT 1),
							cliente = UPPER('".addslashes($cliente)."'),
							codigo_cliente = UPPER('{$codigo}'),
							nombre = UPPER('{$nombre}'),
							establecimiento = '',
							$otro
							tipo_establecimiento = $tipo_establecimiento_id,
							supervisor_uno = UPPER('{$supervisor_uno}'),
							telefono_sup_uno = '{$tel_sup_uno}',
							supervisor_dos = UPPER('{$supervisor_dos}'),
							telefono_sup_dos = '{$tel_sup_dos}',
							serie = UPPER('{$serie}'),
							placa = UPPER('{$placa}'),
							logo = UPPER('{$logo}'),
							marca_modelo = UPPER('{$marca_modelo}'),
							region = UPPER('{$region}'),
							provincia = UPPER('{$provincia}'),
							status = 'movimientos'";
					$this->DD($sql);
					$this->db->query($sql);
				}
			}
		}
	}

    private function perchasLancofruit($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 0){
                    $id = $fila[0];
					$fecha = $fila[1];
					$hora = $fila[2];
					$local = $fila[3];
					$responsable = $fila[4];
					$kilos_totales = $fila[5];
					$kilos_destruidos = $fila[6];
					$temp_bodega_1 = $fila[7];
					$temp_bodega_2 = $fila[8];
					$temp_bodega_3 = $fila[9];
					$temp_bodega_4 = $fila[10];
					$temp_bodega_5 = $fila[11];
					$temp_percha_1_1 = $fila[12];
					$temp_percha_1_2 = $fila[13];
					$temp_percha_1_3 = $fila[14];
					$temp_percha_1_4 = $fila[15];
					$temp_percha_1_5 = $fila[16];
					$temp_percha_2_1 = $fila[17];
					$temp_percha_2_2 = $fila[18];
					$temp_percha_2_3 = $fila[19];
					$temp_percha_2_4 = $fila[20];
					$temp_percha_2_5 = $fila[21];
					$porc_fruta_lancofruit = $fila[22];
					$porc_fruta_terceros = $fila[23];
					$porc_percha_vacia = $fila[24];
					$total_perchas_frutas_verduras = $fila[25];
					$total_perchas_solo_banano = $fila[26];
					$grado_1_cantidad_cluster = $fila[27];
					$grado_2_cantidad_cluster = $fila[28];
					$grado_3_cantidad_cluster = $fila[29];
					$grado_4_cantidad_cluster = $fila[30];
					$grado_5_cantidad_cluster = $fila[31];
					$grado_6_cantidad_cluster = $fila[32];
					$json = $fila[33];
					$categoria_1 = $fila[34];
					$defecto_1 = $fila[35];
					$cantidad_1 = $fila[36];
					$categoria_2 = $fila[37];
					$defecto_2 = $fila[38];
					$cantidad_2 = $fila[39];
					$categoria_3 = $fila[40];
					$defecto_3 = $fila[41];
					$cantidad_3 = $fila[42];

					if($kilos_totales == '') $kilos_totales = 0;
					if($kilos_destruidos == '') $kilos_destruidos = 0;
					if($temp_bodega_1 == '') $temp_bodega_1 = 0;
					if($temp_bodega_2 == '') $temp_bodega_2 = 0;
					if($temp_bodega_3 == '') $temp_bodega_3 = 0;
					if($temp_bodega_4 == '') $temp_bodega_4 = 0;
					if($temp_bodega_5 == '') $temp_bodega_5 = 0;
					if($temp_percha_1_1 == '') $temp_percha_1_1 = 0;
					if($temp_percha_1_2 == '') $temp_percha_1_2 = 0;
					if($temp_percha_1_3 == '') $temp_percha_1_3 = 0;
					if($temp_percha_1_4 == '') $temp_percha_1_4 = 0;
					if($temp_percha_1_5 == '') $temp_percha_1_5 = 0;
					if($temp_percha_2_1 == '') $temp_percha_2_1 = 0;
					if($temp_percha_2_2 == '') $temp_percha_2_2 = 0;
					if($temp_percha_2_3 == '') $temp_percha_2_3 = 0;
					if($temp_percha_2_4 == '') $temp_percha_2_4 = 0;
					if($temp_percha_2_5 == '') $temp_percha_2_5 = 0;
					if($porc_fruta_lancofruit == '') $porc_fruta_lancofruit = 0;
					if($porc_fruta_terceros == '') $porc_fruta_terceros = 0;
					if($porc_percha_vacia == '') $porc_percha_vacia = 0;
					if($total_perchas_frutas_verduras == '') $total_perchas_frutas_verduras = 0;
					if($total_perchas_solo_banano == '') $total_perchas_solo_banano = 0;
					if($grado_1_cantidad_cluster == '') $grado_1_cantidad_cluster = 0;
					if($grado_2_cantidad_cluster == '') $grado_2_cantidad_cluster = 0;
					if($grado_3_cantidad_cluster == '') $grado_3_cantidad_cluster = 0;
					if($grado_4_cantidad_cluster == '') $grado_4_cantidad_cluster = 0;
					if($grado_5_cantidad_cluster == '') $grado_5_cantidad_cluster = 0;
					if($grado_6_cantidad_cluster == '') $grado_6_cantidad_cluster = 0;
					if($cantidad_1 == '') $cantidad_1 = 0;
					if($cantidad_2 == '') $cantidad_2 = 0;
					if($cantidad_3 == '') $cantidad_3 = 0;

					if(is_numeric($fecha)){
						$UNIX_DATE = ($fecha - 25569) * 86400;
						$fecha = gmdate("Y-m-d", $UNIX_DATE);
					}

					$mes = $this->db->queryOne("SELECT MONTH('{$fecha}')");
					if($id != '' && $mes == 1){
						$visita = $this->db->queryRow("SELECT * FROM lancofruit_perchas WHERE local = '{$local}' AND fecha = '{$fecha}'");
						$cuadraBodega = (int) $this->db->queryOne("SELECT COUNT(1) FROM lancofruit_perchas WHERE local = '{$local}' AND fecha = '{$fecha}' AND temp_bodega_1 = '{$temp_bodega_1}' AND temp_bodega_2 = '{$temp_bodega_2}' AND temp_bodega_3 = '{$temp_bodega_3}' AND temp_bodega_4 = '{$temp_bodega_4}' AND temp_bodega_5 = '{$temp_bodega_5}'");
						$cuadraPercha1 = (int) $this->db->queryOne("SELECT COUNT(1) FROM lancofruit_perchas WHERE local = '{$local}' AND fecha = '{$fecha}' AND temp_percha_1_1 = '{$temp_percha_1_1}' AND temp_percha_1_2 = '{$temp_percha_1_2}' AND temp_percha_1_3 = '{$temp_percha_1_3}' AND temp_percha_1_4 = '{$temp_percha_1_4}' AND temp_percha_1_5 = '{$temp_percha_1_5}'");
						$cuadraPercha2 = (int) $this->db->queryOne("SELECT COUNT(1) FROM lancofruit_perchas WHERE local = '{$local}' AND fecha = '{$fecha}' AND temp_percha_2_1 = '{$temp_percha_2_1}' AND temp_percha_2_2 = '{$temp_percha_2_2}' AND temp_percha_2_3 = '{$temp_percha_2_3}' AND temp_percha_2_4 = '{$temp_percha_2_4}' AND temp_percha_2_5 = '{$temp_percha_2_5}'");
						$this->DD($visita->local."    (".$visita->fecha.")  ".($cuadraBodega ? 'OK' : 'FAIL').' '.($cuadraPercha1 ? 'OK' : 'FAIL').' '.($cuadraPercha2 ? 'OK' : 'FAIL'));
						$this->DD("bod1    {$visita->temp_bodega_1}-{$temp_bodega_1}    {$visita->temp_bodega_2}-{$temp_bodega_2}    {$visita->temp_bodega_3}-{$temp_bodega_3}    {$visita->temp_bodega_4}-{$temp_bodega_4}    {$visita->temp_bodega_5}-{$temp_bodega_5}");
						$this->DD("per1    {$visita->temp_percha_1_1}-{$temp_percha_1_1}	{$visita->temp_percha_1_2}-{$temp_percha_1_2}    {$visita->temp_percha_1_3}-{$temp_percha_1_3}    {$visita->temp_percha_1_4}-{$temp_percha_1_4}    {$visita->temp_percha_1_5}-{$temp_percha_1_5}");
						$this->DD("per2    {$visita->temp_percha_2_1}-{$temp_percha_2_1}	{$visita->temp_percha_2_2}-{$temp_percha_2_2}    {$visita->temp_percha_2_3}-{$temp_percha_2_3}    {$visita->temp_percha_2_4}-{$temp_percha_2_4}    {$visita->temp_percha_2_5}-{$temp_percha_2_5}");
						$this->DD("<hr>");
					}

					/*if($id != ""){
						$sql = "INSERT INTO lancofruit_perchas
								SET 
								id = '{$id}',
								fecha = '{$fecha}',
								hora = '{$hora}',
								local = '{$local}',
								responsable = '{$responsable}',
								kilos_totales = '{$kilos_totales}', 
								kilos_destruidos = '{$kilos_destruidos}',
								temp_bodega_1 = '{$temp_bodega_1}',
								temp_bodega_2 = '{$temp_bodega_2}',
								temp_bodega_3 = '{$temp_bodega_3}',
								temp_bodega_4 = '{$temp_bodega_4}',
								temp_bodega_5 = '{$temp_bodega_5}',
								temp_percha_1_1 = '{$temp_percha_1_1}',
								temp_percha_1_2 = '{$temp_percha_1_2}',
								temp_percha_1_3 = '{$temp_percha_1_3}',
								temp_percha_1_4 = '{$temp_percha_1_4}',
								temp_percha_1_5 = '{$temp_percha_1_5}',
								temp_percha_2_1 = '{$temp_percha_2_1}',
								temp_percha_2_2 = '{$temp_percha_2_2}',
								temp_percha_2_3 = '{$temp_percha_2_3}',
								temp_percha_2_4 = '{$temp_percha_2_4}',
								temp_percha_2_5 = '{$temp_percha_2_5}',
								porc_fruta_lancofruit = '{$porc_fruta_lancofruit}',
								porc_fruta_terceros = '{$porc_fruta_terceros}',
								porc_percha_vacia = '{$porc_percha_vacia}',
								total_perchas_frutas_verduras = '{$total_perchas_frutas_verduras}',
								total_perchas_solo_banano = '{$total_perchas_solo_banano}',
								grado_1_cantidad_cluster = '{$grado_1_cantidad_cluster}',
								grado_2_cantidad_cluster = '{$grado_2_cantidad_cluster}',
								grado_3_cantidad_cluster = '{$grado_3_cantidad_cluster}',
								grado_4_cantidad_cluster = '{$grado_4_cantidad_cluster}',
								grado_5_cantidad_cluster = '{$grado_5_cantidad_cluster}',
								grado_6_cantidad_cluster = '{$grado_6_cantidad_cluster}',
								json = '{$json}'";
						$this->DD($sql);
						//$this->db->query($sql);

						//categoria 1
						if($categoria_1 != ''){
							$sql = "INSERT INTO lancofruit_perchas_defectos
									SET
									id_percha = '{$id}',
									categoria = '{$categoria_1}',
									defecto = '{$defecto_1}',
									cantidad = '{$cantidad_1}'";
							$this->DD($sql);
							//$this->db->query($sql);
						}
						//catrgoria 2
						if($categoria_2 != ''){
							$sql = "INSERT INTO lancofruit_perchas_defectos
									SET
									id_percha = '{$id}',
									categoria = '{$categoria_2}',
									defecto = '{$defecto_2}',
									cantidad = '{$cantidad_2}'";
							$this->DD($sql);
							//$this->db->query($sql);
						}
						//catrgoria 3
						if($categoria_3 != ''){
							$sql = "INSERT INTO lancofruit_perchas_defectos
									SET
									id_percha = '{$id}',
									categoria = '{$categoria_3}',
									defecto = '{$defecto_3}',
									cantidad = '{$cantidad_3}'";
							$this->DD($sql);
							//$this->db->query($sql);
						}
					}*/
                }
            }
        }
    }

    private function clientesLancofruit($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 0){
                    $eliminar = $fila[0];
                    $duplicado = (int) $fila[1];
                    $id = (int) $fila[2];
                    
                    $categoria = strtoupper($fila[5]);
                    $subcategoria = strtoupper($fila[6]);
                    $nombre = $fila[9];
                    $ruc = $fila[12];

                    if($subcategoria == 'GYMNASIO') $subcategoria = 'GIMNASIO';

                    if($duplicado > 0){
                        $duplicado[] = $duplicado;
                    }

                    if($id > 0 && !in_array($id, $duplicado) && $subcategoria != ''){
                        $local = $this->db->queryRow("SELECT tipo_local, UPPER(tipo_local_subcategoria) tipo_local_subcategoria FROM lancofruit WHERE id = $id");
                        $id_subcategoria = $this->db->queryOne("SELECT id FROM lancofruit_subcategorias WHERE nombre = '{$subcategoria}' AND status = 'Activo'");
                        #$this->db->query("UPDATE lancofruit SET tipo_local_subcategoria = '{$subcategoria}', id_tipo_local_subcategoria = $id_subcategoria WHERE id = $id");
                    }
                }
            }
        }
    }

    private function racimosSumifru($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                $values = $fila[0];
                $values = explode(";", $values);

                $sql = "INSERT INTO produccion_historica SET
                            peso = {$values[1]},
                            edad = {$values[2]},
                            cuadrilla = '{$values[3]}',
                            manos = '{$values[4]}',
                            lote = '{$values[5]}',
                            dedos = '{$values[6]}',
                            calibre = '{$values[7]}',
                            fecha = DATE('{$values[8]}'),
                            hora = TIME('{$values[8]}'),
                            tipo = 'PROC'";
                $this->db->query($sql);
            }
        }
    }
    
    private function laboresTareasPalosanto($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 0){
                    if($name == 'BD 1'){
                        $finca = trim($fila[0]);
                        $finca = $finca == 'PALO SANTO' ? 'PALOSANTO' : $finca;
                        $id_finca = (int) $this->db->queryOne("SELECT id FROM cat_fincas WHERE nombre = '{$finca}'");
                        $cultivo = trim($fila[1]);
                        $id_cultivo = (int) $this->db->queryOne("SELECT id FROM cat_cultivos WHERE nombre = '{$cultivo}'");
                        $subcultivo = trim($fila[2]);
                        $id_subcultivo = (int) $this->db->queryOne("SELECT id FROM cat_subcultivos WHERE nombre = '{$subcultivo}' AND id_cultivo = {$id_cultivo} AND id_finca = $id_finca");
                        $lote = trim($fila[3]);
                        $id_lote = (int) $this->db->queryOne("SELECT id FROM cat_lotes WHERE nombre = '{$lote}' AND id_finca = {$id_finca}");
                        $tarea = trim($fila[4]);
                        $id_tarea = (int) $this->db->queryOne("SELECT id FROM cat_tareas WHERE nombre = '{$tarea}'");

                        if(!$id_lote > 0 && $lote != ''){
                            $sql = "INSERT INTO cat_lotes SET nombre = '{$lote}', id_finca = {$id_finca}";
                            $this->db->query($sql);
                            $this->DD($sql);
                            $id_lote = $this->db->getLastID();
                        }
                        if(!$id_subcultivo > 0){
                            $sql = "INSERT INTO cat_subcultivos SET nombre = '{$subcultivo}', id_cultivo = {$id_cultivo}, id_finca = {$id_finca}";
                            $this->db->query($sql);
                            $this->DD($sql);
                            $id_subcultivo = $this->db->getLastID();
                        }
                        if(!$id_tarea > 0){
                            $sql = "INSERT INTO cat_tareas SET nombre = '{$tarea}'";
                            $this->db->query($sql);
                            $this->DD($sql);
                            $id_tarea = $this->db->getLastID();
                        }

                        $id_lote_subcultivo = (int) $this->db->queryOne("SELECT id FROM lotes_subcultivos WHERE id_lote = {$id_lote} AND id_subcultivo = {$id_subcultivo}");
                        if(!$id_lote_subcultivo > 0 && $id_lote > 0 && $id_subcultivo > 0){
                            $sql = "INSERT INTO lotes_subcultivos SET id_lote = {$id_lote}, id_subcultivo = {$id_subcultivo}";
                            $this->db->query($sql);
                            $this->DD($sql);
                        }
                        $cultivo_tarea = (int) $this->db->queryOne("SELECT COUNT(1) FROM cultivos_tareas WHERE id_cultivo = {$id_cultivo} AND id_tarea = {$id_tarea} AND id_subcultivo = {$id_subcultivo}");
                        if(!$cultivo_tarea > 0 && $id_cultivo > 0 && $id_tarea > 0 && $id_subcultivo > 0){
                            $sql = "INSERT INTO cultivos_tareas SET id_cultivo = {$id_cultivo}, id_subcultivo = {$id_subcultivo}, id_tarea = {$id_tarea}";
                            $this->db->query($sql);
                            $this->DD($sql);
                        }
                    }
                    if($name == 'BD 2'){
                        $cultivo = trim($fila[0]);
                        $id_cultivo = (int) $this->db->queryOne("SELECT id FROM cat_cultivos WHERE nombre = '{$cultivo}'");
                        $tarea = trim($fila[1]);
                        $id_tarea = (int) $this->db->queryOne("SELECT id FROM cat_tareas WHERE nombre = '{$tarea}'");
                        $subtarea = trim($fila[2]);
                        $id_subtarea = (int) $this->db->queryOne("SELECT id FROM cat_subtareas WHERE nombre = '{$subtarea}'");

                        if(!$id_subtarea > 0 && $subtarea != ''){
                            $sql = "INSERT INTO cat_subtareas SET nombre = '{$subtarea}'";
                            $this->DD($sql);
                            $this->db->query($sql);
                            $id_subtarea = $this->db->getLastID();
                        }

                        $cultivo_tarea = (int) $this->db->queryOne("SELECT COUNT(1) FROM cultivos_tareas WHERE id_cultivo = {$id_cultivo} AND id_tarea = {$id_tarea}");
                        if(!$cultivo_tarea > 0){
                            $sql = "INSERT INTO cultivos_tareas SET id_cultivo = {$id_cultivo}, id_tarea = {$id_tarea}";
                            #$this->db->query($sql);
                            $this->DD($sql);
                        }
                        $subtareas_cultivos_tareas = (int) $this->db->queryOne("SELECT COUNT(1) FROM subtareas_cultivos_tareas WHERE id_subtarea = {$id_subtarea} AND id_tarea = {$id_tarea} AND id_cultivo = {$id_cultivo}");
                        if(!$subtareas_cultivos_tareas > 0 && $id_subtarea > 0){
                            $sql = "INSERT INTO subtareas_cultivos_tareas SET id_subtarea = {$id_subtarea}, id_tarea = {$id_tarea}, id_cultivo = {$id_cultivo}";
                            $this->DD($sql);
                            $this->db->query($sql);
                        }
                    }
                }
            }
        }
	}
	
	private function personal($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			$semana = 0;

            foreach($rows as $key => $fila){
				if((int) $fila[2] > 0){
					$sql = "INSERT INTO personal SET
							codigo = '{$fila[2]}',
							nombre = '{$fila[3]}',
							cedula = '{$fila[4]}',
							cargo = '{$fila[5]}',
							supervisor = '{$fila[6]}',
							status = '{$fila[7]}',
							modo_cobro = '{$fila[8]}'";
					$this->db->query($sql);
				}
            }
        }
    }

	private function cajasReiset($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			$semana = 0;

            foreach($rows as $key => $fila){
                $marca = trim($fila[1]);
                
                if($marca == 'CAJA' || $marca == 'C A J A') $semana++;
                if($marca == 'DON MARIO CLOSTER 40.5 LB') $marca = 'DON MARIO CLOSTER';
                if($marca == 'EXTRABAN 31.5 - 2016') $marca = 'EXTRABAN 31.5 2016';
                if($marca == 'SABROSTAR 42.5 2016') $marca = 'SABROSTAR 2016';
                if($marca == 'SABROSTAR MANOS 31.5 2016') $marca = 'SABROSTAR MANOS 2016';
                if($marca == 'SABROSTAR REC') $marca = 'SABROSTAR REC 46';
                if($marca == 'SELVATICA SIM EUROPEA 40.5') $marca = 'SELVATICA SIM EUROPEA';
                if($marca == 'TUCAN 38 LBRS TROPICAL FRUIT') $marca = 'TUCAN';
                //if($semana == 2) die();
				if(!in_array($marca, ['', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'CAJA', 'C A J A', 'Total'])){
                    
                    foreach([2, 3, 4, 5, 6, 7] as $col){
                        $dia = $col-1;
                        $sem = $semana - 1;
                        $fecha = $this->db->queryOne("SELECT STR_TO_DATE('2018 {$sem} {$dia}', '%Y %U %w')");
                        $cajas = $fila[$col];
                        $cajas_blz = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_cajas WHERE fecha = '{$fecha}' AND marca = '{$marca}'");
                        if($cajas != $cajas_blz){
                            //$this->DD("DIFF {$marca} {$fecha} {$cajas} != {$cajas_blz}");
                        }
                    }

                    $total = (int) $fila[8];

                    /*if($fila[4] != ''){
                        $cantidad = (int) trim($fila[4]);
                        for($x = 1; $x <= $cantidad; $x++){
                            $sql = "INSERT INTO produccion_cajas SET
                                        finca = 'GUARUMAL',
                                        id_finca = 1,
                                        semana = $semana, 
                                        year = 2018,
                                        marca = '{$marca}',
                                        tipo_caja = '{$marca}',
                                        fecha = '2018-01-03',
                                        lb =  {$peso}";
                            $this->DD($sql);
                            #$this->db->query($sql);
                        }
                    }
                    if($fila[5] != ''){
                        $cantidad = (int) trim($fila[5]);
                        for($x = 1; $x <= $cantidad; $x++){
                            $sql = "INSERT INTO produccion_cajas SET
                                        finca = 'GUARUMAL',
                                        id_finca = 1,
                                        semana = $semana, 
                                        year = 2018,
                                        marca = '{$marca}',
                                        tipo_caja = '{$marca}',
                                        fecha = '2018-01-04',
                                        lb =  {$peso}";
                            $this->DD($sql);
                            #$this->db->query($sql);
                        }
                    }
                    if($fila[6] != ''){
                        $cantidad = (int) trim($fila[6]);
                        for($x = 1; $x <= $cantidad; $x++){
                            $sql = "INSERT INTO produccion_cajas SET
                                        finca = 'GUARUMAL',
                                        id_finca = 1,
                                        semana = $semana, 
                                        year = 2018,
                                        marca = '{$marca}',
                                        tipo_caja = '{$marca}',
                                        fecha = '2018-01-05',
                                        lb =  {$peso}";
                            $this->DD($sql);
                            #$this->db->query($sql);
                        }
                    }*/
				}
            }
        }
    }
	
	private function enfundeReiset($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		$year = 2018;

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
				if(trim($fila[11]) == 'SEMANA' && trim($fila[16]) != ''){
					$week = (int) trim($fila[16]);
					$cinta = trim($fila[26]);
					if($cinta == 'BLANCA') $cinta = 'BLANCO';
					if($cinta == 'NEGRA') $cinta = 'NEGRO';
				}

                if(trim($fila[2]) == 'OPERADOR :' && trim($fila[11]) != ''){
					$operador = trim($fila[11]);
					$operadorID = (int) $this->db->queryOne("SELECT id FROM cat_enfundadores WHERE nombre = '{$operador}'");
					if(!$operadorID > 0){
						$this->db->query("INSERT INTO cat_enfundadores SET nombre = '{$operador}'");
						$operadorID = $this->db->getLastID();
					}
				}

				if(strpos(trim($fila[21]), 'LOTE') !== false){
					$lote = trim(str_replace("LOTE", "", $fila[21]));
                    $usadas = (int) trim($fila[30]);
                    

                        $sql = "INSERT INTO produccion_enfunde SET 
                id_enfundador  = $operadorID, 
                fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                years = {$year},
                semana = {$week},
                usadas = {$usadas},
                lote = '{$lote}',
                cinta = '{$cinta}'";
                        #$this->db->query($sql);
                        $this->DD($sql);
                    
				}
            }
        }
    }

    private function cajasRatioHistorico($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 20){
                    $valid = false;
                    if(is_numeric($fila[0])){
                        $UNIX_DATE = ($fila[0] - 25569) * 86400;
                        $fecha = gmdate("Y-m-d", $UNIX_DATE);
                        $valid = true;
                    }

                    if($valid){
                        
                    }else{
                        if($fila[75] > 0){
                            //$this->DD($fila);
                            $ratio_cortado = round($fila[70], 2);
                            if($ratio_cortado == 0) $ratio_cortado = round($fila[71], 2);
                            $ratio_procesado = round($fila[75], 2);
                            $this->DD("INSERT INTO produccion_cajas_ratios_preprocesados SET ratio_procesado = '{$ratio_procesado}', ratio_cortado = '{$ratio_cortado}', fecha = '{$fecha}';");
                        }
                    }
                }
            }
        }
    }

    private function cajasMarcelHistorico($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 20){
                    $fecha = $fila[5];
                    if(is_numeric($fecha)){
                        $UNIX_DATE = ($fecha - 25569) * 86400;
                        $fecha = gmdate("Y-m-d", $UNIX_DATE);
                        $valid = true;
                    }else{
                        $valid = false;
                    }
                    if($valid){
                        $cantidad = (int) $fila[34];
                        $marca = trim($fila[24]);
                        if($marca == 'CAJA LANCOFRUIT') $marca = 'MI COMISARIATO';
                        else if($marca == 'CAJA REWE  40.5') $marca = 'COBANA REWE';
                        else if($marca == 'PINALINDA NORMAL') $marca = 'PINALINDA';
                        else if($marca == 'MADERO 40.5') $marca = 'COBANA MADERA';
                        else if($marca == 'COBANA MADERO') $marca = 'COBANA MADERA';
                        else if($marca == 'COBANA 101 40.5 LB') $marca = 'COBANA 101';
                        else if($marca == 'CAJA LANCO TIA') $marca = 'TIA';
                        else if($marca == 'GAVETAS DEDOS PROALIMENTOS') $marca = 'PROALIMENTOS';

                        $table = 'produccion_cajas';
                        if(in_array($marca, ["MI COMISARIATO", "TIA", "PROALIMENTOS"])){
                            $table = 'produccion_gavetas';
                        }

                        $isValidFecha = (int) $this->db->queryOne("SELECT IF('{$fecha}' < '2017-08-25', 1, 0)");
                        if($isValidFecha == 1){
                            $sql = "INSERT INTO {$table} SET fecha = '{$fecha}', year = YEAR('{$fecha}'), semana = WEEK('{$fecha}'), id_finca = 1, finca = 'NUEVA PUBENZA', marca = '{$marca}';";
                            $this->DD($sql);
                            for($x = 0; $x < $cantidad; $x++){
                                $this->db->query($sql);
                            }
                        }
                    }
                }
            }
        }
    }

    private function sustentoMinimo($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 0){
                    $fecha = $fila[1];
                    if(is_numeric($fecha)){
                        $UNIX_DATE = ($fecha - 25569) * 86400;
                        $fecha = gmdate("Y-m-d", $UNIX_DATE);
                    }

                    $this->DD("INSERT INTO sustento_minimo SET fecha = '{$fecha}', acuerdo = '{$fila[0]}', precio = '{$fila[2]}';");
                }
            }
        }
    }

    private function preciosSpot($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 1){
                    $semana = $fila[0];
                    $valores = [
                        [
                            "año" => 2006,
                            "valor" => $fila[1]
                        ],
                        [
                            "año" => 2007,
                            "valor" => $fila[2]
                        ],
                        [
                            "año" => 2008,
                            "valor" => $fila[3]
                        ],
                        [
                            "año" => 2009,
                            "valor" => $fila[4]
                        ],
                        [
                            "año" => 2010,
                            "valor" => $fila[5]
                        ],
                        [
                            "año" => 2011,
                            "valor" => $fila[6]
                        ],
                        [
                            "año" => 2012,
                            "valor" => $fila[7]
                        ],
                        [
                            "año" => 2013,
                            "valor" => $fila[8]
                        ],
                        [
                            "año" => 2014,
                            "valor" => $fila[9]
                        ],
                        [
                            "año" => 2015,
                            "valor" => $fila[10]
                        ],
                    ];
                    foreach($valores as $row){
                        if($row["valor"] > 0){
                            $sql = "INSERT INTO precio_spot SET anio = {$row["año"]}, semana = $semana, precio = ROUND({$row["valor"]}, 2);";
                            $this->DD($sql);
                        }
                    }
                }
            }
        }
    }

    private function racimosCortadosQuintana($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
        header("Content-type: text/html; charset=UTF-8");
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
				$fecha = $fila[8];
				$hora = '';
                if(is_numeric($fecha)){
                    $UNIX_DATE = ($fecha - 25569) * 86400;
					$fecha = gmdate("Y-m-d", $UNIX_DATE);
					$hora = gmdate("HH:mm:ss", $UNIX_DATE);
                }else{
                    $fecha = explode("-", $fecha);
                    $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                }

                if($fila[4] > 0){
                    $sql = "INSERT INTO produccion_historica SET 
								id_finca = 1, 
								finca = 'Maria Maria', 
								idracimo = '{$fila[1]}', 
								manos = '$fila[3]', 
								calibre = '$fila[4]', 
								edad = '$fila[5]', 
								palanca = '$fila[6]', 
								lote = '{$fila[7]}', 
								fecha = '{$fecha}', 
								hora = '{$hora}', 
								peso = $fila[9], 
								semana = getWeek('{$fecha}'), 
								year = YEAR('{$fecha}'),
								tipo = 'PROC'";
                    $this->DD($sql);
                    $this->db->query($sql);
                }
            }
        }
	}
	
	private function racimosRecusadosQuintana($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        header("Content-type: text/html; charset=UTF-8");
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){

				$e = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE idracimo = '{$fila[1]}'");
				if($e){
					$sql = "UPDATE produccion_historica
							SET tipo = 'RECUSADO', causa = '{$fila[12]}'
							WHERE idracimo = '{$fila[1]}'";
					$this->db->query($sql);
				}else{
					$fecha = $fila[9];
					$hora = '';
					if(is_numeric($fecha)){
						$UNIX_DATE = ($fecha - 25569) * 86400;
						$fecha = gmdate("Y-m-d", $UNIX_DATE);
						$hora = gmdate("HH:mm:ss", $UNIX_DATE);
					}else{
						$fecha = explode("-", $fecha);
						$fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
					}

					if($fila[4] > 0){
						$sql = "INSERT INTO produccion_historica SET 
									id_finca = 1, 
									finca = 'Maria Maria', 
									idracimo = '{$fila[1]}', 
									manos = '$fila[4]', 
									calibre = '$fila[5]', 
									edad = '$fila[6]', 
									palanca = '$fila[7]', 
									lote = '{$fila[8]}', 
									fecha = '{$fecha}', 
									hora = '{$hora}', 
									peso = $fila[10], 
									semana = getWeek('{$fecha}'), 
									year = YEAR('{$fecha}'),
									tipo = 'RECUSADO',
									causa = '{$fila[12]}'";
						$this->DD($sql);
						$this->db->query($sql);
					}
				}
            }
        }
	}

    private function racimosAgrosoftReiset($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			
			$viaje = 1;
			$racimo = 0;
            foreach($rows as $key => $fila){
                if($key == 2){
                    $fecha = $fila[0];
                    $fecha = trim(str_replace("Fecha:", "", $fecha));
                    if(is_numeric($fecha)){
                        $UNIX_DATE = ($fecha - 25569) * 86400;
                        $fecha = gmdate("Y-m-d", $UNIX_DATE);
                    }else{
                        $fecha = explode("-", $fecha);
                        foreach($fecha as $part){
                            $part = trim($part);
                        }
                        $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                    }
                }
                if($key > 4){
					// cambiar viaje
					if($key > 5){
						$prev_racimo = $rows[$key-1];
						$p_palanca = $prev_racimo[8];
						$p_lote = $prev_racimo[2] != '' ? $prev_racimo[2] : $prev_racimo[4];

						$lote = $fila[2] != '' ? $fila[2] : $fila[4];
						$palanca = $fila[8];

						if($p_palanca != $palanca || $p_lote != $lote){
							$viaje++;
							$racimo = 0;
						}
					}
					$racimo++;

                    if($fila[1] > 0){
                        $cinta = $fila[3];
                        if($cinta == 'AMARI') $cinta = 'AMARILLO';
                        if($cinta == 'BLANC') $cinta = 'BLANCO';
						if($cinta == 'NEGRA') $cinta = 'NEGRO';
						
						$lote = $fila[2] != '' ? $fila[2] : $fila[4];
						$id_lote = $this->db->queryOne("SELECT id FROM lotes WHERE nombre = '{$lote}'");
						$id_finca = $this->db->queryOne("SELECT idFinca FROM lotes WHERE id = {$id_lote}");
						$palanca = $fila[8];
                        $manos = (int) $fila[5];
                        $calibre = (int) $fila[6];
                        $dedos = (int) $fila[7];
						$isRecusado = $fila[9] != "" || $fila[10] != "";
						$peso = str_replace(",", ".", $fila[1]);
						
                        $sql = "INSERT INTO racimo_balanza_procesado SET 
                                    fecha = '{$fecha}',
                                    id_finca = $id_finca,
                                    peso = {$peso},
                                    id_lote = '{$id_lote}',
									palanca = '{$palanca}',
                                    id_cinta = (SELECT id FROM cinta WHERE color = '{$cinta}'),
                                    edad = getEdadCinta(getWeek('{$fecha}'), '{$cinta}', getYear('{$fecha}')),
                                    semana = getWeek('{$fecha}'),
									anio = getYear('{$fecha}'),
									viaje = $viaje,
									racimo = $racimo";
                        if($isRecusado){
                            $sql .= ", tipo = 'RECU', causa = '{$fila[9]}'";
                        }else{
                            $sql .= ", tipo = 'PROC'";
                        }
                        $this->DD($sql);
                    	$this->db->query($sql);
                    }
                }
            }
        }

		/*echo "<script>
				setTimeout(function(){
					window.location.reload()
				}, 5000)
			</script>";*/
	}
	
	private function racimosCortadosReiset($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key == 2){
                    $fecha = $fila[0];
                    $fecha = trim(str_replace("Fecha:", "", $fecha));
                    if(is_numeric($fecha)){
                        $UNIX_DATE = ($fecha - 25569) * 86400;
                        $fecha = gmdate("Y-m-d", $UNIX_DATE);
                    }else{
                        $fecha = explode("-", $fecha);
                        foreach($fecha as $part){
                            $part = trim($part);
                        }
                        $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                    }
                }
                if($key > 4){
                    if($fila[1] > 0){
                        $cinta = $fila[3];
                        if($cinta == 'AMARI') $cinta = 'AMARILLO';
                        if($cinta == 'BLANC') $cinta = 'BLANCO';
                        if($cinta == 'NEGRA') $cinta = 'NEGRO';
                        $manos = (int) $fila[5];
                        $calibre = (int) $fila[6];
						$dedos = (int) $fila[7];
						$lote = $fila[2] != '' ? $fila[2] : $fila[4];
                        $isRecusado = $fila[9] != "" || $fila[10] != "";
                        $sql = "INSERT INTO produccion_racimos_formularios SET 
                                    fecha = '{$fecha}',
                                    id_finca = (SELECT idFinca FROM lotes WHERE nombre = '{$lote}'),
                                    finca = (SELECT fincas.nombre FROM lotes INNER JOIN fincas ON idFinca = fincas.id WHERE lotes.nombre = '{$lote}'),
                                    peso = ROUND({$fila[1]}, 2),
									cuadrilla = '{$fila[8]}',
                                    lote = '{$lote}',
                                    cinta = '{$cinta}',
                                    edad = getEdadCinta(getWeek('{$fecha}'), '{$cinta}', getYear('{$fecha}')),
                                    manos = IF({$manos} > 0, {$manos}, NULL),
                                    calibre = IF({$calibre} > 0, {$calibre}, NULL),
                                    dedos = IF({$dedos} > 0, {$dedos}, NULL),
                                    semana = getWeek('{$fecha}'),
                                    year = getYear('{$fecha}')";
                        if($isRecusado){
                            $sql .= ", tipo = 'RECU', causa = '{$fila[10]}'";
                        }else{
                            $sql .= ", tipo = 'PROC'";
                        }
                        $this->DD($sql);
                    	$this->db->query($sql);
                    }
                }
            }
        }
    }

    private function racimosCortadosArturo($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                #if($key > 0){
                    if($fila[3] > 0){
                        $fecha = $fila[1];
                        if(is_numeric($fecha)){
                            $UNIX_DATE = ($fecha - 25569) * 86400;
                            $fecha = gmdate("Y-m-d", $UNIX_DATE);
                        }else{
                            $fecha = explode("-", $fecha);
                            foreach($fecha as $part){
                                $part = trim($part);
                            }
                            $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                        }
                        
                        $semana = $fila[0];
                        $cinta = $fila[5];
                        if($cinta == 'AMARI') $cinta = 'AMARILLO';
                        if($cinta == 'BLANC') $cinta = 'BLANCO';
                        if($cinta == 'NEGRA') $cinta = 'NEGRO';
                        $manos = (int) $fila[6];
                        $calibre = (int) $fila[7];
                        $dedos = (int) $fila[8];
                        $isRecusado = $fila[10] != "" || $fila[11] != "";
                        $sql = "INSERT INTO produccion_racimos SET 
                                    fecha = '{$fecha}',
                                    id_finca = 1,
                                    finca = 'GUARUMAL',
                                    peso = ROUND({$fila[3]}, 2),
                                    sector = (SELECT sector FROM lotes WHERE nombre = '{$fila[4]}'),
                                    lote = '{$fila[4]}',
                                    cinta = '{$cinta}',
                                    edad = getEdadCinta(getWeek('{$fecha}'), '{$cinta}', getYear('{$fecha}')),
                                    manos = IF({$manos} > 0, {$manos}, NULL),
                                    calibre = IF({$calibre} > 0, {$calibre}, NULL),
                                    dedos = IF({$dedos} > 0, {$dedos}, NULL),
                                    semana = getWeek('{$fecha}'),
                                    year = getYear('{$fecha}')";
                        if($isRecusado){
                            $sql .= ", tipo = 'RECU', causa = '{$fila[11]}'";
                        }else{
                            $sql .= ", tipo = 'PROC'";
                        }
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                #}
            }
            break;
        }
    }

    private function cajasQuintana($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        header("Content-type: text/html; charset=UTF-8");
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 1){
                    $fecha = $fila[3];
                    if(is_numeric($fecha)){
                        $UNIX_DATE = ($fecha - 25569) * 86400;
                        $fecha = gmdate("Y-m-d", $UNIX_DATE);
                    }else{
                        $fecha = explode("/", $fecha);
                        $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                    }

					$sql = "INSERT INTO produccion_cajas SET
								id_finca = 1,
								finca = 'Maria Maria',
								semana = getWeek('{$fecha}'),
								year = YEAR('{$fecha}'),
								caja = '{$fila[4]}',
								lb = '{$fila[4]}',
								kg = ROUND({$fila[4]} * 0.4536, 2),
								conv = {$fila[4]} / 41.5,
								tipo_caja = '{$fila[1]}',
								marca = '{$fila[1]}',
								fecha = '{$fecha}'";
					$this->db->query($sql);
                }
            }
            break;
        }
	}
	
	private function enfundeQuintana($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        header("Content-type: text/html; charset=UTF-8");
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 3){
					$anio = $fila[0];
					$semana = $fila[1] + 1;
					$enfundador = $fila[2];
					$id_enfundador = 0;
					$e = (int) $this->db->queryOne("SELECT id FROM cat_enfundadores WHERE nombre = '{$enfundador}'");
					if(!$e > 0){
						$this->db->query("INSERT INTO cat_enfundadores SET nombre = '{$enfundador}'");
						$id_enfundador = $this->db->getLastID();
					}else{
						$id_enfundador = $e;
					}
					$lote = $fila[3];

					$jueves = $fila[6];
					$viernes = $fila[7];
					$sabado = $fila[8];
					$lunes = $fila[9];
					$martes = $fila[10];
					$miercoles = $fila[11];

					if($lunes > 0){
						$sql = "SELECT STR_TO_DATE('{$anio}{$semana} Monday', '%X%V %W')";
						$date = $this->db->queryOne($sql);
						$this->db->query("INSERT INTO produccion_enfunde SET id_enfundador = $id_enfundador, id_finca = 1, fecha = '{$date}', years = $anio, semana = $semana-1, usadas = $lunes, lote = '{$lote}'");
					}
					if($martes > 0){
						$sql = "SELECT STR_TO_DATE('{$anio}{$semana} Tuesday', '%X%V %W')";
						$date = $this->db->queryOne($sql);
						$this->db->query("INSERT INTO produccion_enfunde SET id_enfundador = $id_enfundador, id_finca = 1, fecha = '{$date}', years = $anio, semana = $semana-1, usadas = $martes, lote = '{$lote}'");
					}
					if($miercoles > 0){
						$sql = "SELECT STR_TO_DATE('{$anio}{$semana} Wednesday', '%X%V %W')";
						$date = $this->db->queryOne($sql);
						$this->db->query("INSERT INTO produccion_enfunde SET id_enfundador = $id_enfundador, id_finca = 1, fecha = '{$date}', years = $anio, semana = $semana-1, usadas = $miercoles, lote = '{$lote}'");
					}
					if($jueves > 0){
						$sql = "SELECT STR_TO_DATE('{$anio}{$semana} Thursday', '%X%V %W')";
						$date = $this->db->queryOne($sql);
						$this->db->query("INSERT INTO produccion_enfunde SET id_enfundador = $id_enfundador, id_finca = 1, fecha = '{$date}', years = $anio, semana = $semana-1, usadas = $jueves, lote = '{$lote}'");
					}
					if($viernes > 0){
						$sql = "SELECT STR_TO_DATE('{$anio}{$semana} Friday', '%X%V %W')";
						$date = $this->db->queryOne($sql);
						$this->db->query("INSERT INTO produccion_enfunde SET id_enfundador = $id_enfundador, id_finca = 1, fecha = '{$date}', years = $anio, semana = $semana-1, usadas = $viernes, lote = '{$lote}'");
					}
					if($sabado > 0){
						$sql = "SELECT STR_TO_DATE('{$anio}{$semana} Saturday', '%X%V %W')";
						$date = $this->db->queryOne($sql);
						$this->db->query("INSERT INTO produccion_enfunde SET id_enfundador = $id_enfundador, id_finca = 1, fecha = '{$date}', years = $anio, semana = $semana-1, usadas = $sabado, lote = '{$lote}'");
					}
				}
            }
            break;
        }
    }

    private function banano($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
        include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            if(in_array($name, ['Minoristas', 'Mayoristas EU', 'Mayorista USA'])){
                $rows = $xlsx->rows($x);
                foreach($rows as $key => $fila){
                    if($key > 0){
                        if($name == 'Minoristas'){
                            $fecha = $fila[0];
                            if(is_numeric($fecha)){
                                $UNIX_DATE = ($fecha - 25569) * 86400;
                                $fecha = gmdate("Y-m-d", $UNIX_DATE);
                            } else {
                                $fecha = explode("-", $fecha);
                                $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                            }
                            $this->DD("INSERT INTO minoristas SET fecha = '{$fecha}', region = '{$fila[1]}', unidad = '{$fila[7]}', numero_tiendas = '{$fila[8]}', menor_precio = '{$fila[10]}', mayor_precio = '{$fila[11]}';");
                        }else if($name == 'Mayoristas EU'){
                            $fecha = $fila[7];
                            if(is_numeric($fecha)){
                                $UNIX_DATE = ($fecha - 25569) * 86400;
                                $fecha = gmdate("Y-m-d", $UNIX_DATE);
                            } else {
                                $fecha = explode("-", $fecha);
                                $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                            }
                            $this->DD("INSERT INTO mayoristas SET fecha = '{$fecha}', paquete = '{$fila[3]}', menor_precio = '{$fila[8]}', mayor_precio = '{$fila[9]}', origen = '{$fila[12]}', ciudad = '{$fila[1]}';");
                        }else if($name == 'Mayorista USA'){
                            $fecha = $fila[7];
                            if(is_numeric($fecha)){
                                $UNIX_DATE = ($fecha - 25569) * 86400;
                                $fecha = gmdate("Y-m-d", $UNIX_DATE);
                            } else {
                                $fecha = explode("-", $fecha);
                                $fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
                            }
                            $this->DD("INSERT INTO mayoristas SET fecha = '{$fecha}', paquete = '{$fila[3]}', menor_precio = '{$fila[8]}', mayor_precio = '{$fila[9]}', origen = '{$fila[12]}', ciudad = '{$fila[1]}', tipo = 'USA';");
                        }
                    }
                }
            }
        } 
	}
	
	private function climat($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();

		$cat_marcas = [];
		$cat_modelos = [];
		$agregados = [];
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 0){
					$marca = trim($fila[0]);
					$modelo = trim($fila[1]);
					
					if(!in_array($modelo, $cat_modelos)){
						$cat_modelos[] = $modelo;
						//$this->DD("INSERT INTO cat_modelos SET modelo = '{$modelo}';");
						$id_modelo = array_search($modelo, $cat_modelos) + 1;
						if(!in_array($marca, $cat_marcas)){
							$cat_marcas[] = $marca;
							//$this->DD("INSERT INTO cat_marcas SET marca = '{$marca}';");
						}
						$id_marca = array_search($marca, $cat_marcas) + 1;
	
						$this->DD("INSERT INTO cat_marcas_modelos SET id_marca = '{$id_marca}', id_modelo = '{$id_modelo}';");
					}
                }
            }
        } 
	}

    private function historicoEnfundeMarcel($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
        $libros = $xlsx->sheetNames();

        $year = 2018;
        $lote2015 = true;
        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            $begin = false;
            $week = substr($name, -2);
            
            foreach($rows as $key => $fila){
                if($fila[1] == 'ENFUNDADOR'){
                    $begin = true;
                    $lote2015 = $fila[12] == 'TOTAL' ? false : true;
                }
                if($fila[1] == 'SUMAN'){
                    $begin = false;
                }

                if($begin && $fila[1] != 'ENFUNDADOR'){
                    $id_enfundador = $this->db->queryRow("SELECT id FROM cat_enfundadores WHERE nombre = '{$fila[1]}'");
                    if(isset($id_enfundador->id)){
                        $id_enfundador = $id_enfundador->id;
                    }else{
                        $this->db->query("INSERT INTO cat_enfundadores SET nombre = '{$fila[1]}'");
                        $id_enfundador = $this->db->getLastID();
                    }
                    if(trim($fila[2]) != ""){
                        /*$sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[2],
                            lote = 1";
                        $this->DD($sql);
                        $this->db->query($sql);*/
                    }
                    if(trim($fila[3]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[3],
                            lote = 1";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                    if(trim($fila[4]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[4],
                            lote = 2";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                    if(trim($fila[5]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[5],
                            lote = 3";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                    if(trim($fila[6]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[6],
                            lote = 4";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                    if(trim($fila[7]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[7],
                            lote = 5";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                    if(trim($fila[8]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[8],
                            lote = 6";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                    if(trim($fila[9]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[9],
                            lote = 7";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                    if(trim($fila[10]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[10],
                            lote = 8";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                    if(trim($fila[11]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[11],
                            lote = 9";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                    if(trim($fila[12]) != ""){
                        $sql = "INSERT INTO produccion_enfunde SET 
                            id_enfundador = $id_enfundador,
                            fecha = STR_TO_DATE('{$year}{$week} Monday', '%X%V %W'),
                            semana = $week,
                            usadas = $fila[12],
                            lote = 10";
                        $this->DD($sql);
                        $this->db->query($sql);
                    }
                }
            }
        }
    }

    private function ventasLancofruit($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                foreach($fila as $i => $campo){
                    if($campo != ""){
                        $mes = explode(",", $campo);
                        $dia = explode(" ", $mes[0]);
                        $mes = $dia[0];
                        $dia = $dia[1];
                        switch($mes){
                            case 'Jan':
                                $mes = '01';
                                break;
                            case 'Feb':
                                $mes = '02';
                                break;
                            case 'Mar':
                                $mes = '03';
                                break;
                            case 'Apr':
                                $mes = '04';
                                break;
                            case 'May':
                                $mes = '05';
                                break;
							case 'Jun':
                                $mes = '06';
                                break;
                            case 'Jul':
                                $mes = '07';
								break;
							case 'Aug':
                                $mes = '08';
								break;
							case 'Sep':
                                $mes = '09';
								break;
							case 'Oct':
                                $mes = '10';
								break;
							case 'Nov':
                                $mes = '11';
								break;
							case 'Dec':
                                $mes = '12';
                                break;
						}
						
						$año = "2018";
						$fecha = $año.'-'.$mes.'-'.$dia;

						$total = "0.00";
						if($rows[50][$i] != '') $total = $rows[50][$i];
						
						$sql = "INSERT INTO lancofruit_ventas 
								SET
								fecha = '{$fecha}',
								anio = '{$año}',
								mes = '{$mes}',
								semana = WEEK('{$fecha}'),
								total = {$total},
								razon_social = 'Corporacion Lancofruit SA',
								nombre_comercial = 'Corporacion Lancofruit SA',
								ruc = '0791765935001',
								dir_matriz = 'Via Santa Rosa S/N, Frente al Correccional de Menores, 070210, Machala, Ecuador',
								fecha_emision = '{$fecha}',
								razon_social_comprador = 'CONSUMIDOR FINAL',
								identificacion_comprador = '9999999999999',
								total_sin_impuestos = {$total},
								importe_total = {$total},
								moneda = 'DOLAR',
								forma_pago = 1,
								id_ruta = 1,
								pto_emi = '001'";
						$this->DD($sql);
						//$this->db->query($sql);
                    }
                }
                break;
            }
        }
    }

    private function provincias($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();
		

		
		$cat_ciudades = [];
		$cat_provincias= [];
		$cat_regiones = [];

        foreach ($libros as $x => $name) {
            $rows = $xlsx->rows($x);
            foreach($rows as $key => $fila){
                if($key > 0){
                    $ciudad = trim($fila[1]);
                    $provincia = trim($fila[0]);
                    $region = trim($fila[2]);

					if(!in_array($region, $cat_regiones)){
						$cat_regiones[] = $region;
						$this->DD("INSERT INTO cat_regiones SET nombre = UPPER('{$region}');");
					}
					$id_region = array_search($region, $cat_regiones) + 1;
					
					if(!in_array($provincia, $cat_provincias)){
                        $cat_provincias[] = $provincia;
                        $this->DD("INSERT INTO cat_provincias SET nombre = UPPER('{$provincia}');");
                    }
					$id_provincia = array_search($provincia, $cat_provincias) + 1;
					
					if(!in_array($ciudad, $cat_ciudades)){
						$cat_ciudades[] = $ciudad;
						$this->DD("INSERT INTO cat_ciudades SET nombre = UPPER('{$ciudad}'), id_provincia = '{$id_provincia}', id_region = '{$id_region}';");
					}
                }
            }
        }
	}

	private function inventario($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
		$xlsx = new SimpleXLSX($file);
		$rowConfig = new stdClass;
		$response = new stdClass;
	    $count = 0;
	    $head = [];
		$libros = $xlsx->sheetNames();
		
		foreach ($libros as $x => $name) {
			$rows = $xlsx->rows($x);
			foreach($rows as $key => $fila){
				if($key > 0){
					$lotes = trim($fila[0]);
					$fecha = trim($fila[1]);
					$id_proveedores = trim($fila[2]);
					$proveedores = trim($fila[3]);
					$bodega = trim($fila[4]);
					$sacos_real = trim($fila[5]);
					$kg = trim($fila[6]);
					$tn = trim($fila[7]);
					$lbs = trim($fila[8]);
					$qq = trim($fila[9]);
					$observaciones = trim($fila[10]);
					$calidad = trim($fila[11]);
					$id_tipo_cacao = trim($fila[12]);
					$tipo_cacao = trim($fila[13]);				

					if(is_numeric($fecha)){
						$UNIX_DATE = ($fecha - 25569) * 86400;
						$fecha = gmdate("Y-m-d", $UNIX_DATE);
					} else {
						$fecha = explode("-", $fecha);
						$fecha = "{$fecha[2]}-{$fecha[0]}-{$fecha[1]}";
					}
					
					$sql = "INSERT INTO inventario_dos
							SET
								lote = '{$lotes}',
								fecha = '{$fecha}',
								id_proveedor = '{$id_proveedores}',
								proveedor = '{$proveedores}',
								sacos = '{$sacos_real}',
								kg = '{$kg}',
								tn = '{$tn}',
								lb = '{$lbs}',
								qq = '{$qq}',
								ubicacion = '{$bodega}',
								calidad = '{$calidad}',
								id_tipo_cacao = '{$id_tipo_cacao}',
								tipo_cacao = '{$tipo_cacao}'";
					$this->db->query($sql);
					$this->DD($sql);

					/*
					$sql = "INSERT INTO inventario
					SET
						lote = '{$lotes}',
						fecha_fact = '{$fecha}',
						proveedor = '{$proveedores}',
						bodega = '{$bodega}',
						sacos_real = '{$sacos_real}',
						kg = '{$kg}',
						tn = '{$tn}',
						lbs = '{$lbs}',
						qq = '{$qq}',
						observaciones = '{$observaciones}',
						calidad = '{$calidad}',
						id_tipo_cacao = '{$id_tipo_cacao}',
						tipo_cacao = '{$tipo_cacao}',
						id_proveedor = (SELECT id FROM proveedores WHERE nombre = '{$proveedores}' LIMIT 1)";
					$this->db->query($sql);
					$this->DD($sql);
					*/
				}
			}
		}
	}

    public function quality($file){
        include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
        $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();
        $table = [];

        foreach ($libros as $x => $name) {
			#$this->DD($name);
	    	if(($name != "" || $name !== NULL)){
		        $rows = $xlsx->rows($x);
		        foreach ($rows as $key => $fila) {
                    if($key > 0){
                        $fecha = $fila[13];
                        if(is_numeric($fecha)){
                            $UNIX_DATE = ($fecha - 25569) * 86400;
                            $fecha = gmdate("Y-m-d H:i:s", $UNIX_DATE);
                        }

                        $sql = "INSERT INTO quality SET 
                                    fecha = '{$fecha}',
                                    barco = '{$fila[1]}',
                                    marca = '{$fila[11]}',
                                    cliente = '{$fila[11]}',
                                    muestra = '',
                                    cluster = '{$fila[26]}',
                                    peso = '{$fila[24]}',
                                    calibre_minimo = '{$fila[28]}',
                                    calibre_maximo = '{$fila[29]}',
                                    n = '',
                                    dedos = '{$fila[27]}',
                                    fallas_total = '',
                                    dedos_buenos = '',
                                    calidad = ''";
                        #$this->DD($sql);
                        $this->db->query($sql);
                        $id_calidad = $this->db->getLastID();
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'SCARS/BRUISES', campo = 'sr', cantidad = '{$fila[38]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'SCARS/BRUISES', campo = 'psr', cantidad = '{$fila[39]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'SCARS/BRUISES', campo = 'br', cantidad = '{$fila[40]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'OTHER MAJOR DEFECTS', campo = 'fr', cantidad = '{$fila[41]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'LATEX', campo = 'ls', cantidad = '{$fila[42]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'OTHER OBSERVATIONS', campo = 're', cantidad = '{$fila[43]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'ROTS/MOLDS', campo = 'ni', cantidad = '{$fila[44]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'OTHER MAJOR DEFECTS', campo = 'dc', cantidad = '{$fila[45]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'OTHER OBSERVATIONS', campo = 'pt', cantidad = '{$fila[46]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'ROTS/MOLDS', campo = 'cr', cantidad = '{$fila[47]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'ROTS/MOLDS', campo = 'cm', cantidad = '{$fila[48]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'OTHER OBSERVATIONS', campo = 'wp', cantidad = '{$fila[49]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'OTHER OBSERVATIONS', campo = 'm', cantidad = '{$fila[50]}'");
                        $this->db->query("INSERT INTO quality_detalle_unit SET id_quality = $id_calidad, type = 'OTHER OBSERVATIONS', campo = 'tm', cantidad = '{$fila[51]}'");

                        $suma = 0;
                        if($fila[38] > 0)
                            $suma += $fila[38];
                        if($fila[39] > 0)
                            $suma += $fila[39];
                        if($fila[40] > 0)
                            $suma += $fila[40];
                        if($fila[41] > 0)
                            $suma += $fila[41];
                        if($fila[42] > 0)
                            $suma += $fila[42];
                        if($fila[43] > 0)
                            $suma += $fila[43];
                        if($fila[44] > 0)
                            $suma += $fila[44];
                        /*if($fila[45] > 0)
                            $suma += $fila[45];
                        if($fila[46] > 0)
                            $suma += $fila[46];
                        if($fila[47] > 0)
                            $suma += $fila[47];
                        if($fila[48] > 0)
                            $suma += $fila[48];
                        if($fila[49] > 0)
                            $suma += $fila[49];
                        if($fila[50] > 0)
                            $suma += $fila[50];
                        if($fila[51] > 0)
                            $suma += $fila[51];
                        if($fila[53] > 0)
                            $suma += $fila[53];*/

                        $dedos = $fila[27];
                        if(!$dedos > 0) $dedos = 0;
                        $dedos_buenos = $dedos - $suma;
                        $calidad = 100 - (($suma / $dedos) * 100);
                        #if(!$calidad > 0) $this->DD("dedos = $dedos, fallas = $suma");
                        $sqls[] = "UPDATE quality SET fallas_total = '{$suma}', dedos_buenos = '{$dedos_buenos}', calidad = $calidad  WHERE id =  $id_calidad";
                        $this->db->query("UPDATE quality SET fallas_total = '{$suma}', dedos_buenos = '{$dedos_buenos}', calidad = $calidad  WHERE id =  $id_calidad");
                    }
				}
			}
			break;
        }
        return $sqls;
    }

	private function estimacion($file, $name){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();
		$table = [];

	    foreach ($libros as $x => $name) {
			$this->DD($name);
	    	if(($name != "" || $name !== NULL)){
		        $rows = $xlsx->rows($x);
		        foreach ($rows as $key => $fila) {
					$ts = $fila[2];
					$ts = ($ts - 25569)*86400;
                    $fecha = date('Y-m-d', $ts);

					$ts = $fila[10];
					$ts = ($ts - 25569)*86400;
                    $fecha_entrega = date('Y-m-d', $ts);

					#echo $fecha."<br>";
					$sql = "INSERT INTO estimacion SET fecha = '{$fecha}', semana '{$fila[0]}', cosechado = '{$fila[3]}', lote = '{$fila[4]}', valvula = '{$fila[5]}', sacos = '{$fila[6]}', baba_lb = '{$fila[7]}', seco_lb = '{$fila[8]}', rendimiento = '{$fila[9]}', fecha_entrega = '{$fecha_entrega}';";
					echo utf8_decode($sql)."<br>";
				}
			}
			break;
		}
	}

	private function historico_marcel_racimos($file, $name){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();
		$table = [];

		$this->DD($name);
		$name = explode(" ", $name);
		$semana = (int) str_replace("S", "",explode("_", $name[0])[1]);
		$marca = explode(".", $name[3])[0];

	    foreach ($libros as $x => $name) {			
	    	if(($name != "" || $name !== NULL)){
		        $rows = $xlsx->rows($x);
		        foreach ($rows as $key => $fila) {
					if($key == 2){
						$fecha = str_replace("Fecha: ", "", $fila[0]);
						$fecha = explode("-", $fecha);
						$fecha = "{$fecha[2]}-{$fecha[1]}-{$fecha[0]}";
					}
					if($key > 4 && $fila[0] != ""){

                        $insert = "";
                        $tipo = "tipo = 'PROC'";
						if((float)$fila[4] > 0){
							$insert .= "manos = '{$fila[4]}',";
						}
						if((float)$fila[5] > 0){
							$insert .= "calibre = '{$fila[5]}',";
						}
						if((float)$fila[6] > 0){
							$insert .= "dedos = '{$fila[6]}',";
						}
						/*if($fila[7] != ""){
							$insert .= "tipo = '{$fila[7]}',";
						}*/
						if($fila[8] != ""){
							$insert .= "cuadrilla = REPLACE('{$fila[8]}', 'CUA-', ''),";
						}
						if($fila[9] != ""){
                            $insert .= "causa = '{$fila[9]}',";
                            $tipo = "tipo = 'RECUSADO'";
						}

		$sql = "INSERT INTO produccion_historica SET 
            {$insert}
            fecha = '{$fecha}', 
            id_finca = 1, 
            finca = 'GUARUMAL', 
            idv = '{$fila[0]}',
            peso = '{$fila[1]}',
            lote = '{$fila[2]}',
            cinta = '{$fila[3]}',
            semana = getWeek('{$fecha}'),
            edad = getEdadCinta(getWeek('{$fecha}'), '{$fila[3]}', YEAR('{$fecha}')),
            year =  YEAR('{$fecha}'),
            {$tipo}";
						$this->DD($sql);
						#if($sql != "")
							#$this->db->query($sql);
						
					}
				}
			}
		}
	}

	private function upala($file){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();
		$table = [];

	    foreach ($libros as $x => $name) {
	    	if(($name != "" || $name !== NULL)){
		        $rows = $xlsx->rows($x);
		        foreach ($rows as $key => $fila) {
					if($key > 0){
						$fecha = $fila[2];
						$fecha = explode("T", $fecha);
						$fecha = $fecha[0]." ".substr($fecha[1], 0, 8);

						$newRow = [
							"fecha" => $fecha,
							"evaluador" => $fila[4],
							"finca" => $fila[5],
							"subfinca" => $fila[6],
							"lote" => $fila[7],
							"forza" => $fila[8],
							"cantidad_muestreo" => $fila[9]
						];

						if($fila[11] != null && $fila[11] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[11],
								"categoria" => $fila[12],
								"4" => $fila[13],
								"5" => $fila[14],
								"6" => $fila[15],
								"7" => $fila[16],
								"8" => $fila[17],
								"9" => $fila[18],
								"10" => $fila[19],
								"11" => $fila[20],
								"12" => $fila[21],
								"total" => $fila[22],
								"evidencia" => $fila[23],
								"observacion" => $fila[24]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[26] != null && $fila[26] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[26],
								"categoria" => $fila[27],
								"4" => $fila[28],
								"5" => $fila[29],
								"6" => $fila[30],
								"7" => $fila[31],
								"8" => $fila[32],
								"9" => $fila[33],
								"10" => $fila[34],
								"11" => $fila[35],
								"12" => $fila[36],
								"total" => $fila[37],
								"evidencia" => $fila[38],
								"observacion" => $fila[39]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[41] != null && $fila[41] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[41],
								"categoria" => $fila[42],
								"4" => $fila[43],
								"5" => $fila[44],
								"6" => $fila[45],
								"7" => $fila[46],
								"8" => $fila[47],
								"9" => $fila[48],
								"10" => $fila[49],
								"11" => $fila[50],
								"12" => $fila[51],
								"total" => $fila[52],
								"evidencia" => $fila[53],
								"observacion" => $fila[54]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[56] != null && $fila[56] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[56],
								"categoria" => $fila[57],
								"4" => $fila[58],
								"5" => $fila[59],
								"6" => $fila[60],
								"7" => $fila[61],
								"8" => $fila[62],
								"9" => $fila[63],
								"10" => $fila[64],
								"11" => $fila[65],
								"12" => $fila[66],
								"total" => $fila[67],
								"evidencia" => $fila[68],
								"observacion" => $fila[69]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[71] != null && $fila[71] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[71],
								"categoria" => $fila[72],
								"4" => $fila[73],
								"5" => $fila[74],
								"6" => $fila[75],
								"7" => $fila[76],
								"8" => $fila[77],
								"9" => $fila[78],
								"10" => $fila[79],
								"11" => $fila[80],
								"12" => $fila[81],
								"total" => $fila[82],
								"evidencia" => $fila[83],
								"observacion" => $fila[84]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[86] != null && $fila[86] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[86],
								"categoria" => $fila[87],
								"4" => $fila[88],
								"5" => $fila[89],
								"6" => $fila[90],
								"7" => $fila[91],
								"8" => $fila[92],
								"9" => $fila[93],
								"10" => $fila[94],
								"11" => $fila[95],
								"12" => $fila[96],
								"total" => $fila[97],
								"evidencia" => $fila[98],
								"observacion" => $fila[99]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[101] != null && $fila[101] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[101],
								"categoria" => $fila[102],
								"4" => $fila[103],
								"5" => $fila[104],
								"6" => $fila[105],
								"7" => $fila[106],
								"8" => $fila[107],
								"9" => $fila[108],
								"10" => $fila[109],
								"11" => $fila[110],
								"12" => $fila[111],
								"total" => $fila[112],
								"evidencia" => $fila[113],
								"observacion" => $fila[114]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[116] != null && $fila[116] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[116],
								"categoria" => $fila[117],
								"4" => $fila[118],
								"5" => $fila[119],
								"6" => $fila[120],
								"7" => $fila[121],
								"8" => $fila[122],
								"9" => $fila[123],
								"10" => $fila[124],
								"11" => $fila[125],
								"12" => $fila[126],
								"total" => $fila[127],
								"evidencia" => $fila[128],
								"observacion" => $fila[129]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[131] != null && $fila[131] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[131],
								"categoria" => $fila[132],
								"4" => $fila[133],
								"5" => $fila[134],
								"6" => $fila[135],
								"7" => $fila[136],
								"8" => $fila[137],
								"9" => $fila[138],
								"10" => $fila[139],
								"11" => $fila[140],
								"12" => $fila[141],
								"total" => $fila[142],
								"evidencia" => $fila[143],
								"observacion" => $fila[144]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[146] != null && $fila[146] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[146],
								"categoria" => $fila[147],
								"4" => $fila[148],
								"5" => $fila[149],
								"6" => $fila[150],
								"7" => $fila[151],
								"8" => $fila[152],
								"9" => $fila[153],
								"10" => $fila[154],
								"11" => $fila[155],
								"12" => $fila[156],
								"total" => $fila[157],
								"evidencia" => $fila[158],
								"observacion" => $fila[159]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[161] != null && $fila[161] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[161],
								"categoria" => $fila[162],
								"4" => $fila[163],
								"5" => $fila[164],
								"6" => $fila[165],
								"7" => $fila[166],
								"8" => $fila[167],
								"9" => $fila[168],
								"10" => $fila[169],
								"11" => $fila[170],
								"12" => $fila[171],
								"total" => $fila[172],
								"evidencia" => $fila[173],
								"observacion" => $fila[174]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[176] != null && $fila[176] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[176],
								"categoria" => $fila[177],
								"4" => $fila[178],
								"5" => $fila[179],
								"6" => $fila[180],
								"7" => $fila[181],
								"8" => $fila[182],
								"9" => $fila[183],
								"10" => $fila[184],
								"11" => $fila[185],
								"12" => $fila[186],
								"total" => $fila[187],
								"evidencia" => $fila[188],
								"observacion" => $fila[189]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[191] != null && $fila[191] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[191],
								"categoria" => $fila[192],
								"4" => $fila[193],
								"5" => $fila[194],
								"6" => $fila[195],
								"7" => $fila[196],
								"8" => $fila[197],
								"9" => $fila[198],
								"10" => $fila[199],
								"11" => $fila[200],
								"12" => $fila[201],
								"total" => $fila[202],
								"evidencia" => $fila[203],
								"observacion" => $fila[204]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[206] != null && $fila[206] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[206],
								"categoria" => $fila[207],
								"4" => $fila[208],
								"5" => $fila[209],
								"6" => $fila[210],
								"7" => $fila[211],
								"8" => $fila[212],
								"9" => $fila[213],
								"10" => $fila[214],
								"11" => $fila[215],
								"12" => $fila[216],
								"total" => $fila[217],
								"evidencia" => $fila[218],
								"observacion" => $fila[219]
							];
						}
						// AGREGAR OTRO : SI
						if($fila[221] != null && $fila[221] != ""){
							$newRow["danos"][] = [
								"dano" => $fila[221],
								"categoria" => $fila[222],
								"4" => $fila[223],
								"5" => $fila[224],
								"6" => $fila[225],
								"7" => $fila[226],
								"8" => $fila[227],
								"9" => $fila[228],
								"10" => $fila[229],
								"11" => $fila[230],
								"12" => $fila[231],
								"total" => $fila[232],
								"evidencia" => $fila[233],
								"observacion" => $fila[234]
							];
						}

						$table[] = $newRow;
					}
				}
			}
		}
		
		foreach($table as $index => $row){
			$i = $index + 1 + 80;
			$sql = utf8_encode("INSERT INTO danos_calibre SET evaluador = '{$row['evaluador']}', finca = '{$row['subfinca']}', subfinca = '{$row['subfinca']}', lote = '{$row['lote']}', grupo_forza = '{$row['forza']}', cantidad_muestreo = '{$row['cantidad_muestreo']}';");
			echo "{$sql}<br>";
			foreach($row["danos"] as $dano){
				$sql = "INSERT INTO danos_calibre_detalle SET id_daño_calibre = '{$i}', daño = '{$dano['dano']}', categoria = '{$dano['categoria']}', ";
				if($dano['4'] != null && $dano['4'] != ""){
					$sql .= "c4 = '{$dano['4']}',";
				}
				if($dano['5'] != null && $dano['5'] != ""){
					$sql .= "c5 = '{$dano['5']}',";
				}
				if($dano['6'] != null && $dano['6'] != ""){
					$sql .= "c6 = '{$dano['6']}',";
				}
				if($dano['7'] != null && $dano['7'] != ""){
					$sql .= "c7 = '{$dano['7']}',";
				}
				if($dano['8'] != null && $dano['8'] != ""){
					$sql .= "c8 = '{$dano['8']}',";
				}
				if($dano['9'] != null && $dano['9'] != ""){
					$sql .= "c9 = '{$dano['9']}',";
				}
				if($dano['10'] != null && $dano['10'] != ""){
					$sql .= "c10 = '{$dano['10']}',";
				}
				if($dano['11'] != null && $dano['11'] != ""){
					$sql .= "c11 = '{$dano['11']}',";
				}
				if($dano['12'] != null && $dano['12'] != ""){
					$sql .= "c12 = '{$dano['12']}',";
				}
				$sql .= " evidencia = '{$dano['evidencia']}', observacion = '{$dano['observacion']}';";
				$sql = utf8_decode($sql);
				echo "{$sql}<br>";
			}
			echo "<br><br>";
		}
	}

	private function move_file($file , $filename){
		if(!rename($file, PHRAPI_PATH."utilities/read/$filename")){
	        echo 'error';
	    }
	}

	private function historicoProduccion($file_name){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file_name);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();
	    foreach ($libros as $x => $name) {
	    	if(($name != "" || $name !== NULL)){
		        $rows = $xlsx->rows($x);
		        foreach ($rows as $key => $fila) {
					if($key > 1){
						$reg = (object)[
							"year" => $name,
							"semana" => $fila[0],
							"peso_kg" => $fila[1],
							"calibracion" => $fila[2],
							"manos" => $fila[3],
							"dedos_cm" => $fila[4],
							"racimos_cortados" => $fila[5],
							"racimos_procesados" => $fila[6],
							"racimos_recusados" => $fila[7],
							"cajas_procesadas" => $fila[8],
							"cajas_segunda" => $fila[9],
							"total_cajas" => $fila[10],
							"porc_racimos_recusados" => $fila[11],
							"porc_merma_primera" => $fila[12],
							"conversion_primera" => $fila[13],
							"merma_segunda" => $fila[14],
							"conversion_segunda" => $fila[15]
						];
						$this->DD($reg);

						$cajas = "INSERT INTO produccion_cajas SET id_finca = 1, finca = 'NUEVA PUBENZA', semana = '{$reg->semana}', year = '{$reg->year}', caja = '{$reg->cajas_procesadas}'";
						$calibracion = "INSERT INTO produccion_calibracion SET id_finca = 1, finca = 'NUEVA PUBENZA', semana = '{$reg->semana}', year = '{$reg->year}', calibracion = '{$reg->calibracion}'";
						$manos = "INSERT INTO produccion_manos SET id_finca = 1, finca = 'NUEVA PUBENZA', semana = '{$reg->semana}', year = '{$reg->year}', manos = '{$reg->manos}'";
						$peso = "INSERT INTO produccion_peso SET id_finca = 1, finca = 'NUEVA PUBENZA', semana = '{$reg->semana}', year = '{$reg->year}', peso = '{$reg->peso_kg}'";

						$produccion = "INSERT INTO produccion SET cajas_primera = '{$reg->cajas_procesadas}', cajas_segunda = '{$reg->cajas_segunda}', racimos_cortados = '{$reg->racimos_cortados}', semana = '{$reg->semana}', year = '{$reg->year}', racimos_procesados = '{$reg->racimos_procesados}', racimos_recusados = '{$reg->racimos_recusados}', peso = '{$reg->peso_kg}', calibracion = '{$reg->calibracion}', manos = '{$reg->manos}'";
						if($reg->year != NULL && $reg->year != "")
							$this->db->query($produccion);

						#$this->db->query($cajas);
						#$this->db->query($calibracion);
						#$this->db->query($manos);
						#$this->db->query($peso);
						#$ha = "INSERT INTO produccion_ha SET id_finca = 1, finca = 'NUEVA PUBENZA', semana = '{$reg->semana}', year = '{$reg->year}', ";
					}
				}
			}
		}
	}

	private function procesar($file_name){
		include PHRAPI_PATH.'libs/utilities/simplexlsx.class.php';
		include PHRAPI_PATH.'libs/utilities/Encoding.php';	
	    $xlsx = new SimpleXLSX($file_name);
	    $rowConfig = new stdClass;
	    $count = 0;
	    $head = [];
	    $libros = $xlsx->sheetNames();
		D($libros);
	    foreach ($libros as $x => $name) {	    	
	    	if(($name != "" || $name !== NULL)){
		        $rows = $xlsx->rows($x);
		        foreach ($rows as $key => $fila) {
					/*EXCEL 1*/
					/*if($key > 6 && $fila[2] != NULL && trim($fila[2]) != ""){
						$personal = new stdClass;
						$personal->nombre = trim($fila[2]);
						$personal->labor = explode("/", $fila[3]);
						$personal->finca = trim($fila[4]);
						$personal->sector = trim($fila[5]);
						$personal->lotes = [trim($fila[6]), trim($fila[7]), trim($fila[8])];
						$personal->jefe = trim($fila[9]);

						#$this->insertPersonalExcel1($personal);
					}*/

					if($key > 5 && $fila[1] != NULL && ($fila[2] == NULL || $fila[2] == "")){
						$personal->labor = trim($fila[1]);
					}
					if($key > 6 && $fila[2] != NULL && $fila[2] != "" && $personal->labor != "AREA DE PLATANO"){
						switch(trim($fila[2])){
							case "MJ-1":
							case "M.J 1":
							case "M.J-1":
								$fila[2] = "María José 1";
								break;
							case "MJ-2":
							case "MJ2":
								$fila[2] = "María José 2";
								break;
						}

						$personal->nombre = trim($fila[1]);
						$personal->subfinca = trim($fila[2]);
						$personal->codigo = trim($fila[3]);

						#$this->DD($personal);
						if(strtoupper((trim($fila[2]))) == "MJ"){
							$personal->subfinca = "María José 1";
							$this->insertarPersonal($personal);

							$personal->subfinca = "María José 2";
							$this->insertarPersonal($personal);
						}else{
							$this->insertarPersonal($personal);
						}
					}

					/* IZQUIEDA DERECHA */
					/*if($key > 7){
						if($fila[11] != NULL && $fila[11] != ""){
							$personal->labor = trim($fila[11]);
						}
						if($fila[10] != NULL && trim($fila[10]) != ""){
							if($personal->labor == "FINQUERO"){
								$this->insertPersonalIzquierda(trim($fila[10]), "Potrero", "ENFUNDE");
								$this->insertPersonalIzquierda(trim($fila[10]), "Potrero", "PROTECCION");
								$this->insertPersonalIzquierda(trim($fila[10]), "Potrero", "DESFLORE");
							}else{
								if($personal->labor == "AMARRADORES")
									$personal->labor = "AMARRE";
								if($personal->labor == "DESHOJADOR FITO")
									$personal->labor = "DESHOJE";
								if($personal->labor == "DESCHANTADOR")
									$personal->labor = "DESCHANTE";

								$this->insertPersonalIzquierda(trim($fila[10]), "Potrero", $personal->labor);
							}
						}
						if($fila[11] != NULL && trim($fila[11]) != ""){
							if($personal->labor == "FINQUERO"){
								$this->insertPersonalIzquierda(trim($fila[11]), "Elba 3", "ENFUNDE");
								$this->insertPersonalIzquierda(trim($fila[11]), "Elba 3", "PROTECCION");
								$this->insertPersonalIzquierda(trim($fila[11]), "Elba 3", "DESFLORE");
							}else{
								if($personal->labor == "AMARRADORES")
									$personal->labor = "AMARRE";
								if($personal->labor == "DESHOJADOR FITO")
									$personal->labor = "DESHOJE";
								if($personal->labor == "DESCHANTADOR")
									$personal->labor = "DESCHANTE";

								$this->insertPersonalIzquierda(trim($fila[11]), "Elba 3", $personal->labor);
							}
						}
					}*/
		        }
	    	}
	    }
	}

	private function insertPersonalExcel1($personal = []){
		$persona = $this->db->queryRow("SELECT * FROM personal WHERE nombre = '{$personal->nombre}'");
		if(!isset($persona->id)){
			$this->db->query("INSERT INTO personal SET nombre = '{$personal->nombre}', sector = '{$personal->sector}', responsable = '{$personal->jefe}'");
			$persona->id = $this->db->getLastID();
		}

		$finca = $this->db->queryRow("SELECT * FROM fincas WHERE nombre = '{$personal->finca}'");

		foreach($personal->labor as $labor){
			$labor = trim($labor);
			$labor = $this->db->queryRow("SELECT * FROM labores WHERE nombre = '{$labor}'");
			foreach($personal->lotes as $lote){
				if($lote != ""){
					$sql = "SELECT * FROM lotes WHERE nombre = '{$lote}' AND idFinca = '{$finca->id}'";
					#$this->DD($sql);
					$l = $this->db->queryRow($sql);

					$sql = "SELECT * FROM personal_labores WHERE id_personal = {$persona->id} AND idLote = '{$l->id}'";
					$this->DD($sql);
					$r = $this->db->queryRow($sql);
					if(!isset($r->id)){
						$sql = "INSERT INTO personal_labores SET id_personal = {$persona->id}, idFinca = '{$finca->id}', idSubFinca = '{$l->idSubFinca}', idLote = '$l->id', idLabor = '{$labor->id}'";
						$this->DD($sql);
						#$this->db->query($sql);
					}
				}
			}
		}
	}

	private function insertPersonalIzquierda($nombre, $subfinca, $labor){
		$this->DD([$nombre, $subfinca, $labor]);

		$subfinca = $this->db->queryRow("SELECT * FROM subfincas WHERE nombre = '{$subfinca}'");
		$lotes = $this->db->queryAll("SELECT * FROM lotes WHERE idSubFinca = '{$subfinca->id}'");

		$sql = "SELECT * FROM personal WHERE nombre = '{$nombre}'";
		$persona = $this->db->queryRow($sql);
		if(!isset($persona->id)){
			$this->db->query("INSERT INTO personal SET nombre = '{$nombre}'");
			$persona = $this->db->getLastID();
		}

		$sql = "SELECT * FROM labores WHERE nombre = '{$labor}'";
		$labor = $this->db->queryRow($sql);

		foreach($lotes as $l){
			$sql = "SELECT * FROM personal_labores WHERE id_personal = '{$persona->id}' AND idSubFinca = '{$subfinca->id}' AND idLote = '{$l->id}' AND idLabor = '{$labor->id}'";
			$r = $this->db->queryRow($sql);
			if(!isset($r->id)){
				$sql = "INSERT INTO personal_labores SET id_personal = '{$persona->id}', idFinca = '{$l->idFinca}', idSubFinca = '{$subfinca->id}', idLote = '{$l->id}', idLabor = '{$labor->id}'";
				#$this->db->query($sql);
			}
		}
	}

	private function insertarPersonal2($personal = []){
		$sql = "SELECT * FROM personal WHERE nombre = '{$personal->nombre}'";
		$persona = $this->db->queryRow($sql);
		if(!isset($persona->id)){
			$sql = "INSERT INTO personal SET nombre = '{$personal->nombre}', responsable = '{$personal->responsable}', cargo = 'OPERADOR AGRICOLA'";
			$this->DD($sql);
			$this->db->query($sql);
			$persona->id = $this->db->getLastID();
		}

		foreach($personal->lotes as $lote){
			if($lote != "" && $lote != NULL){
				$l = $this->db->queryRow("SELECT * FROM personal_lotes WHERE id_personal = '{$persona->id}' AND idFinca = '2' AND idLote = (SELECT id FROM lotes WHERE nombre = '{$lote}' AND idFinca = '2')");
				if(!isset($l->id)){
					$sql = "INSERT INTO personal_lotes SET id_personal = '{$persona->id}' , idFinca = '2' , idLote = (SELECT id FROM lotes WHERE nombre = '{$lote}' AND idFinca = '2')";
					$this->DD($sql);
					$this->db->query($sql);
				}
			}
		}

		foreach($personal->labores as $labor){
			$labor = trim($labor);
			if($labor != "" && $labor != NULL){
				$l = $this->db->queryRow("SELECT * FROM personal_labores WHERE id_personal = '{$persona->id}' AND idLabor = (SELECT id FROM labores WHERE nombre = '{$labor}')");
				if(!isset($l->id)){
					$sql = "INSERT INTO personal_labores SET id_personal = '{$persona->id}' , idLabor = (SELECT id FROM labores WHERE nombre = '{$labor}')";
					$this->DD($sql);
					$this->db->query($sql);
				}
			}
		}
	}

	private function insertarPersonal($personal = []){
		if($personal->subfinca != "" && $personal->subfinca != NULL && $personal->nombre != "Nombres"){
			$sql = "SELECT * FROM personal WHERE nombre = '{$personal->nombre}'";
			$persona = $this->db->queryRow($sql);
			$sql = "SELECT * FROM lotes WHERE idSubFinca = (SELECT id FROM subfincas WHERE nombre = '{$personal->subfinca}')";
			$this->DD($sql);
			$lote = $this->db->queryRow($sql);

			if(!isset($persona->id)){
				$sql = "INSERT INTO personal SET nombre = '{$personal->nombre}', cargo = 'OPERADOR AGRICOLA', codigo = '{$personal->codigo}'";
				$this->DD($sql);
				#$this->db->query($sql);
				$persona->id = $this->db->getLastID();
			}else if(!isset($persona->codigo) || $persona->codigo != ""){
				$sql = "UPDATE personal SET codigo = '{$personal->codigo}' WHERE id = '{$persona->id}'";
				$this->DD($sql);
				#$this->db->query($sql);
			}

			/*foreach($lotes as $lote){
				$sql = "SELECT * FROM personal_lotes WHERE id_personal = {$persona->id} AND idLote = {$lote->id}";
				$l = $this->db->queryRow($sql);
				if(!isset($l->id)){
					$sql = "INSERT INTO personal_lotes SET id_personal = {$persona->id} , idLote = {$lote->id}, idFinca = {$lote->idFinca}";
					$this->DD($sql);
					$this->db->query($sql);
				}
			}*/

			switch($personal->labor){
				case "AMARRADORES":
				case "AMARRADOR":
					$personal->labor = "AMARRE";
					break;
				case "DESHOJADOR FITO":
				case "DESHOJE DE FITO":
				case "DESHOJADORES":
					$personal->labor = "DESHOJE";
					break;
				case "DESCHANTADOR":
					$personal->labor = "DESCHANTE";
					break;
				case "CALIBRADOR":
				case "PRECALIBRADOR":
					$personal->labor = "CALIBRACIÓN";
					break;
				case "DESTALLADOR":
				case "DESTALLE":
					$personal->labor = "DESTALLAR";
					break;
				case "SELECTOR":
					$personal->labor = "SELECCIÓN";
					break;
				case "ROZA DE CANALES":
					$personal->labor = "ROZA DE CANALES";
					break;
				case "VARIOS":
					$personal->labor = "LABORES VARIAS";
					break;
			}
			if(in_array($personal->labor, ["FINQUERO", "FINQUEROS"])){
				$labores = ["ENFUNDE", "PROTECCION", "DESFLORE"];
				foreach($labores as $labor){
					$sql = "SELECT * FROM personal_labores WHERE id_personal = {$persona->id} AND idLabor = (SELECT id FROM labores WHERE nombre = '{$labor}') AND idLote = '{$lote->id}'";
					$this->DD($sql);
					$l = $this->db->queryRow($sql);
					if(!isset($l->id)){
						$sql = "INSERT INTO personal_labores SET id_personal = {$persona->id}, idLabor = (SELECT id FROM labores WHERE nombre = '{$labor}'), idFinca = '{$lote->idFinca}', idSubFinca = '{$lote->idSubFinca}', idLote = '{$lote->id}'";
						$this->DD($sql);
						#$this->db->query($sql);
					}
				}
			}else{
				$sql = "SELECT * FROM personal_labores WHERE id_personal = {$persona->id} AND idLabor = (SELECT id FROM labores WHERE nombre = '{$personal->labor}')";
				$l = $this->db->queryRow($sql);
				if(!isset($l->id)){
					$sql = "SELECT id FROM labores WHERE nombre = '{$personal->labor}'";
					$this->DD($sql);
					$idLabor = $this->db->queryRow($sql)->id;
					$sql = "INSERT INTO personal_labores SET id_personal = {$persona->id}, idLabor = {$idLabor}, idFinca = '{$lote->idFinca}', idSubFinca = '{$lote->idSubFinca}', idLote = '{$lote->id}'";
					$this->DD($sql);
					#$this->db->query($sql);
				}
			}
		}
	}

	private function laboresDole($fila = [], $key){
		if(isset($fila[0]) && isset($fila[1])){ #ENFUNDE
			if($key < 24){
				$sql = "INSERT INTO labores_causas SET valor = '{$fila[1]}', causa = '".utf8_decode($fila[0])."', idLabor = 1;";
				#$this->db->query($sql);
				echo $sql."<br>";
			}
		}
		if(isset($fila[3]) && isset($fila[4])){ #AMARRE
			$sql = "INSERT INTO labores_causas SET valor = '{$fila[4]}', causa = '".utf8_decode($fila[3])."', idLabor = 2;";
			#$this->db->query($sql);
			echo $sql."<br>";
		}
		if(isset($fila[6]) && isset($fila[7])){ #DESFLORE
			$sql = "INSERT INTO labores_causas SET valor = '{$fila[7]}', causa = '".utf8_decode($fila[6])."', idLabor = 3;";
			#$this->db->query($sql);
			echo $sql."<br>";
		}
		if(isset($fila[9]) && isset($fila[10])){ #PROTECCION
			$sql = "INSERT INTO labores_causas SET valor = '{$fila[10]}', causa = '".utf8_decode($fila[9])."', idLabor = 4;";
			#$this->db->query($sql);
			echo $sql."<br>";
		}
		if(isset($fila[12]) && isset($fila[13])){ #DESHOJE
			$sql = "INSERT INTO labores_causas SET valor = '{$fila[13]}', causa = '".utf8_decode($fila[12])."', idLabor = 5;";
			#$this->db->query($sql);
			echo $sql."<br>";
		}
		if(isset($fila[15]) && isset($fila[16])){ #COSECHA
			$sql = "INSERT INTO labores_causas SET valor = '{$fila[16]}', causa = '".utf8_decode($fila[15])."', idLabor = 6;";
			#$this->db->query($sql);
			echo $sql."<br>";
		}
		if(isset($fila[18]) && isset($fila[19])){ #FERTILIZACION
			$sql = "INSERT INTO labores_causas SET valor = '{$fila[19]}', causa = '".utf8_decode($fila[18])."', idLabor = 7;";
			#$this->db->query($sql);
			echo $sql."<br>";
		}
		if(isset($fila[21]) && isset($fila[22])){ #SELECCION
			$sql = "INSERT INTO labores_causas SET valor = '{$fila[22]}', causa = '".utf8_decode($fila[21])."', idLabor = 8;";
			#$this->db->query($sql);
			echo $sql."<br>";
		}
		if(isset($fila[24]) && isset($fila[25])){ #LIMPIEZA
			$sql = "INSERT INTO labores_causas SET valor = '{$fila[25]}', causa = '".utf8_decode($fila[24])."', idLabor = 9;";
			#$this->db->query($sql);
			echo $sql."<br>";
		}
		if(isset($fila[27]) && isset($fila[28])){ #OPA
			$sql = "INSERT INTO labores_causas SET valor = '{$fila[28]}', causa = '".utf8_decode($fila[27])."', idLabor = 10;";
			#$this->db->query($sql);
			echo $sql."<br>";
		}
	}

	private function DD($Data){
		echo '<pre>';
			if(is_string($Data)) print_r(utf8_decode($Data));
			else print_r($Data);
			//print_r($Data);
		echo '</pre>';
	}

	private function getSql($fila = []){
		$id_calidad = 0;
		$id_finca = (int) $this->db->queryOne("SELECT id FROM fincas WHERE nombre LIKE '{$fila[2]}'");
		$id_cliente = (int) $this->db->queryOne("SELECT id FROM clients WHERE nombre LIKE '{$fila[3]}'");
		$id_marca = (int) $this->db->queryOne("SELECT id FROM brands WHERE id_client = {$id_cliente} AND nombre LIKE '{$fila[4]}'");

		$sql = "INSERT INTO calidad  SET
				id_finca = '{$id_finca}',
				auditor = '{$fila[1]}',
				finca = '{$fila[2]}',
				id_cliente = '{$id_cliente}',
				cliente = '{$fila[3]}',
				id_marca = '{$id_marca}',
				marca = '{$fila[4]}',
				contenedor = '{$fila[5]}',
				peso_caja = '{$fila[6]}',
				identificador_empaque = '{$fila[7]}',				
				cantidad_cluster_caja = '{$fila[8]}',
				cluster_totales_caja = '{$fila[55]}',
				cluster_totales_defectos = '{$fila[56]}',
				cantidad_dedos_caja = '{$fila[58]}',
				dedos_totales_defectos = '{$fila[59]}',
				calidad_dedos = '{$fila[61]}',
				calidad_cluster = '{$fila[62]}',
				fecha = '{$fila[0]}',
				timestamp = CURRENT_TIMESTAMP,
				archivoJson = ''";
			// $this->db->query($sql);
			$id_calidad = (int)$this->db->getLastID();
			$this->DD($sql);

		// 03/03/2017 - TAG: Cluster por Dedos
		$cluster = [
			11 => "Cluster de 3 dedos",
			12 => "Cluster de 4 dedos",
			13 => "Cluster de 5 dedos", 
			14 => "Cluster de 6 dedos",
			15 => "Cluster de 7 dedos",
			16 => "Cluster de 8 dedos",
			17 => "Cluster de 9 dedos",
		];

		foreach ($cluster as $key => $value) {
			$sql = "INSERT INTO calidad_cluster SET
			id_calidad = '$id_calidad',
			cluster = '{$cluster[$key]}',
			cantidad = '{$fila[$key]}'";
			// $this->db->query($sql);
			$this->DD($sql);
		}
		// 03/03/2017 - TAG: Cluster por Dedos
		
		// 03/03/2017 - TAG: Calidad Dedos
		$dedos = [
			20 => "Dedos con menos de 8 pulgadas",
			21 => "Dedos entre 8 y 9 pulgadas",
			22 => "Dedos mayor a 9 pulgadas", 
		];

		foreach ($dedos as $key => $value) {
			$sql = "INSERT INTO calidad_dedos SET
			id_calidad = '$id_calidad',
			dedos = '{$dedos[$key]}',
			cantidad = '{$fila[$key]}'";
			// $this->db->query($sql);
			$this->DD($sql);
		}
		// 03/03/2017 - TAG: Calidad Dedos

		// 03/03/2017 - TAG: Calidad Detalle

		// 03/03/2107 - TAG: defecto 1
		$id_categoria = (int) $this->db->queryOne("SELECT id FROM calidad_categorias WHERE nombre LIKE '{$this->limpiar($fila[32])}'");
		if($id_categoria > 0){
			$sql = "INSERT INTO calidad_detalle SET
			id_calidad = '$id_calidad',
			id_categoria = '{$id_categoria}',
			campo = '".utf8_decode($fila[33])."',
			cantidad = 1,
			num_defecto = '{$fila[31]}'";
			// $this->db->query($sql);
			$this->DD($sql);
		}

		// 03/03/2107 - TAG: defecto 2
		$id_categoria = (int) $this->db->queryOne("SELECT id FROM calidad_categorias WHERE nombre LIKE '{$this->limpiar($fila[38])}'");
		if($id_categoria > 0){
			$sql = "INSERT INTO calidad_detalle SET
			id_calidad = '$id_calidad',
			id_categoria = '{$id_categoria}',
			campo = '".utf8_decode($fila[39])."',
			cantidad = 1,
			num_defecto = '{$fila[37]}'";
			// $this->db->query($sql);
			$this->DD($sql);
		}

		// 03/03/2107 - TAG: defecto 3
		$id_categoria = (int) $this->db->queryOne("SELECT id FROM calidad_categorias WHERE nombre LIKE '{$this->limpiar($fila[44])}'");
		if($id_categoria > 0){
			$sql = "INSERT INTO calidad_detalle SET
			id_calidad = '$id_calidad',
			id_categoria = '{$id_categoria}',
			campo = '".utf8_decode($fila[45])."',
			cantidad = 1,
			num_defecto = '{$fila[43]}'";
			// $this->db->query($sql);
			$this->DD($sql);
		}

		// 03/03/2107 - TAG: defecto 4
		$id_categoria = (int) $this->db->queryOne("SELECT id FROM calidad_categorias WHERE nombre LIKE '{$this->limpiar($fila[50])}'");
		if($id_categoria > 0){
			$sql = "INSERT INTO calidad_detalle SET
			id_calidad = '$id_calidad',
			id_categoria = '{$id_categoria}',
			campo = '".utf8_decode($fila[51])."',
			cantidad = 1,
			num_defecto = '{$fila[49]}'";
			// $this->db->query($sql);
			$this->DD($sql);
		}

		// 03/03/2017 - TAG: Calidad Detalle

		return $id_calidad;
	}

	// private function saveMuestra($data , $key){
	// 	$ts = ($data[13] - 25569)*86400;
	// 	$fecha = '2016-06-18';
	// 	$labor = $this->getLaborId($data[1]);
	// 	$lote = $this->getLoteId($data[0]);
	// 	$causa = $this->getLaborCausaId($data[2]);
	// 	$data[1] = strtoupper($this->limpiar(trim($data[1])));
	// 	$data[2] = strtoupper($this->limpiar(trim($data[2])));
	// 	$sql_master = "INSERT INTO 
 	//            muestras SET
	// 				idZona = 1,
	// 				idFinca = 1,
	// 				idLote = {$data[0]},
	// 				zona = 1,
	// 				administrador = '',
	// 				finca = 'NUEVA PUBENZA',
	// 				fecha = '{$fecha}',
	// 				timestamp = CURRENT_TIMESTAMP,
	// 				lote = {$lote},
	// 				supervisorCampo = '',
	// 				archivoJson = '{$key}',
	// 				auditor = '',
	// 				causa = 'CAUSAS NUEVA',
	// 				idResponsableLabor = '',
	// 				observaciones = '', 
	// 				archivoJson_contador = '{$key}', 
	// 				idLabor = {$labor},
	// 				labor = '{$data[1]}',
	// 				idLaborCausa = {$causa},
	// 				laborCausaDesc = '{$data[2]}',
	// 				valor = {$data[3]}";
	// 	// D($sql_master);
	// 	// $this->db->query($sql_master);
	// }

	// private function getLaborId($nombre){
	// 	$nombre = strtoupper($this->limpiar(trim($nombre)));
	// 	D($nombre);
	// 	if($nombre == "DRENAJE"){
	// 		$nombre = "DRENAJES";
	// 	}
	// 	$labor = "";
	// 	$sql = "SELECT id FROM labores WHERE nombre = '{$nombre}'";
	// 	$labor = $this->db->queryOne($sql);
	// 	return $labor;
	// }

	// private function getLaborCausaId($nombre){
	// 	$nombre = strtoupper($this->limpiar(trim($nombre)));
	// 	D($nombre);
	// 	$labor = "";
	// 	if($nombre == "BUEN DRENAJE"){
	// 		$nombre = "BUENA DRENAJE";
	// 	}
	// 	if($nombre == "BUEN CONTROL DE MALEZAS"){
	// 		$nombre = "BUENA CONTROL MALEZAS";
	// 	}
	// 	if($nombre == "BUENA FERTILZACION"){
	// 		$nombre = "BUENA FERTILIZACION";
	// 	}
	// 	if($nombre == "BUEN DESHOJE FITO"){
	// 		$nombre = "BUEN DESHOJE FITOSANITARIO";
	// 	}
	// 	$sql = "SELECT id FROM labores_causas WHERE causa = '{$nombre}'";
	// 	$labor = $this->db->queryOne($sql);
	// 	return $labor;
	// }

	// private function getLoteId($nombre){
	// 	$nombre = trim($nombre);
	// 	$labor = "";
	// 	$sql = "SELECT id FROM lotes WHERE nombre = '{$nombre}'";
	// 	$labor = $this->db->queryOne($sql);
	// 	return $labor;
	// }

	// private function saveCalidad($data){
	// 	$monts = [
	// 		"ENE" => "01",
	// 		"FEB" => "02",
	// 		"MAR" => "03",
	// 		"ABR" => "04",
	// 		"MAY" => "05",
	// 		"JUN" => "06",
	// 		"JUL" => "07",
	// 		"AGO" => "08",
	// 		"SEP" => "09",
	// 		"OCT" => "10",
	// 		"NOV" => "11",
	// 		"DIV" => "12"
	// 	];

	// 	$day = (int)$data[2] < 10 ? "0".$data[2] : $data[2];
	// 	$fecha = $data[0]."-".$monts[$data[1]]."-".$day;
	// 	$sql = "INSERT INTO calidad SET calidad = '".$data[5]."', fecha = '{$fecha}'";
	// 	$this->db->query($sql);

	// }

	// private function saveMuestras($data){
	// 	$ts = ($data[13] - 25569)*86400;
	// 	$fehca = date('Y-m-d', $ts);
	// 	$labor = $this->getLabor($data[4]);
	// 	$causa = $this->getLaborCausa($data[26]);
	// 	$sql_master = "INSERT INTO 
 //            muestras SET
	// 				idZona = 1,
	// 				idFinca = 1,
	// 				idLote = {$data[3]},
	// 				zona = 1,
	// 				administrador = '',
	// 				finca = '{$data[7]}',
	// 				fecha = '{$fehca}',
	// 				timestamp = CURRENT_TIMESTAMP,
	// 				lote = {$data[15]},
	// 				supervisorCampo = '',
	// 				archivoJson = '{$data[23]}',
	// 				auditor = '{$data[25]}',
	// 				causa = 'CAUSAS NUEVA',
	// 				idResponsableLabor = '{$data[29]}',
	// 				observaciones = '{$data[22]}', 
	// 				archivoJson_contador = '{$data[24]}', 
	// 				idLabor = {$data[4]},
	// 				labor = '{$labor}',
	// 				idLaborCausa = {$data[26]},
	// 				laborCausaDesc = '{$causa}',
	// 				valor = {$data[28]}";
	// 	$this->db->query($sql_master);
	// }	

	// private function getLabor($id){
	// 	$labor = "";
	// 	$sql = "SELECT nombre FROM labores WHERE id = {$id}";
	// 	$labor = $this->db->queryOne($sql);
	// 	return $labor;
	// }

	// private function getLaborCausa($id){
	// 	$labor = "";
	// 	$sql = "SELECT causa FROM labores_causas WHERE id = {$id}";
	// 	$labor = $this->db->queryOne($sql);
	// 	return $labor;
	// }

	// private function configuracion($data){
	// 	if($data->id_organization > 0 && $data->id_category > 0 && $data->id_sub_category > 0 && $data->id_detail > 0 && $data->id_specification > 0 ){
	// 		$id = $this->db->queryOne("SELECT id FROM configuracion 
	// 			WHERE 
	// 			id_organization = '{$data->id_organization}' AND 
	// 			id_category = '{$data->id_category}' AND 
	// 			id_sub_category = '{$data->id_sub_category}' AND 
	// 			id_detail = '{$data->id_detail}' AND 
	// 			id_specification = '{$data->id_specification}'
	// 		");
	// 		if((int)$id > 0){
	// 			return $id;
	// 		}else{
	// 			$sql = "INSERT INTO configuracion SET
	// 					id_organization = '{$data->id_organization}',
	// 					id_category = '{$data->id_category}',
	// 					id_sub_category = '{$data->id_sub_category}',
	// 					id_detail = '{$data->id_detail}',
	// 					id_specification = '{$data->id_specification}',
	// 					status = 1";
	// 			$this->db->query($sql);
	// 			return $this->db->getLastID();
	// 		}
	// 	}
	// }

	// private function organization($organization){
	// 	$organization = trim(strtoupper($organization));
	// 	$id = $this->db->queryOne("SELECT id FROM cat_clientes WHERE UPPER(nombre) = UPPER('$organization')");
	// 	if((int)$id > 0){
	// 		return $id;
	// 	}
	// }

	// private function tipo_labor($tipo_labor , $data){
	// 	$tipo_labor = trim($tipo_labor);
	// 	if($tipo_labor != "" && !empty($tipo_labor)){
	// 		$id = $this->db->queryOne("SELECT id FROM tipo_labor WHERE nombre = '$tipo_labor' AND id_finca = 1");
	// 		if((int)$id > 0){
	// 			return $id;
	// 		}else{
	// 			$sql = "INSERT INTO tipo_labor SET nombre = '{$tipo_labor}' , id_finca = 1, fecha_create = CURRENT_TIMESTAMP";
	// 			$this->db->query($sql);
	// 	        return $this->db->getLastID();
	// 		}
	// 	}
	// }

	// private function labores($labor , $data){
	// 	$labor = trim($labor);
	// 	if($labor != "" && !empty($labor)){
	// 		$id = $this->db->queryOne("SELECT id 
	// 				FROM labores 
	// 				WHERE nombre = '$labor' 
	// 				AND id_tipo_labor = {$data->id_tipo_labor}");
	// 		if((int)$id > 0){
	// 			return $id;
	// 		}else{
	// 			$sql = "INSERT INTO labores SET 
	// 				id_tipo_labor = {$data->id_tipo_labor} ,
	// 				nombre = '{$labor}' ,
	// 				fecha_create = CURRENT_TIMESTAMP";
	// 			$this->db->query($sql);
	// 	        return $this->db->getLastID();
	// 		}
	// 	}
	// }

	// private function labores_causas($labores_causas , $data){
	// 	if((int)$data->id_labor > 0){
	// 		$labores_causas = trim($labores_causas);
	// 		$id = $this->db->queryOne("SELECT id 
	// 				FROM labores_causas 
	// 				WHERE causa = '$labores_causas' 
	// 				AND idLabor = {$data->id_labor}");
	// 		if((int)$id > 0){
	// 			return $id;
	// 		}else{
	// 			$sql = "INSERT INTO labores_causas SET 
	// 				idLabor = {$data->id_labor} ,
	// 				causa = '{$labores_causas}' ,
	// 				valor = '{$data->valor}' ,
	// 				fecha_create = CURRENT_TIMESTAMP";
	// 			$this->db->query($sql);
	// 	        return $this->db->getLastID();
	// 		}
	// 	}
	// }

	// private function cable($cable , $data){
	// 	$cable = trim($cable);
	// 	$id = $this->db->queryOne("SELECT id 
	// 			FROM cable 
	// 			WHERE nombre = '$cable' AND id_finca = 1");
	// 	if((int)$id > 0){
	// 		return $id;
	// 	}else{
	// 		$sql = "INSERT INTO cable SET 
	// 			id_finca = 1,
	// 			nombre = '{$cable}' ,
	// 			fecha_create = CURRENT_TIMESTAMP";
	// 		$this->db->query($sql);
	//         return $this->db->getLastID();
	// 	}
	// }

	// private function lotes($lote , $data){
	// 	$lote = trim($lote);
	// 	$id = $this->db->queryOne("SELECT id 
	// 			FROM lotes 
	// 			WHERE nombre = '$lote' 
	// 			AND id_cable = {$data->id_cable} AND idFinca = 1");
	// 	if((int)$id > 0){
	// 		return $id;
	// 	}else{
	// 		$sql = "INSERT INTO lotes SET 
	// 			id_cable = {$data->id_cable} ,
	// 			idFinca = 1,
	// 			nombre = '{$lote}' ,
	// 			fecha_create = CURRENT_TIMESTAMP";
	// 		$this->db->query($sql);
	//         return $this->db->getLastID();
	// 	}
	// }
	
	// }
	private function limpiar($String) {

	    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	    $String = str_replace(array('í','ì','î','ï'),"i",$String);
	    $String = str_replace(array('é','è','ê','ë'),"e",$String);
	    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
	    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	    $String = str_replace("ç","c",$String);
	    $String = str_replace("Ç","C",$String);
	    $String = str_replace("ñ","n",$String);
	    $String = str_replace("Ñ","N",$String);
	    $String = str_replace("Ý","Y",$String);
	    $String = str_replace("ý","y",$String);
	    
	    $String = str_replace("&aacute;","a",$String);
	    $String = str_replace("&Aacute;","A",$String);
	    $String = str_replace("&eacute;","e",$String);
	    $String = str_replace("&Eacute;","E",$String);
	    $String = str_replace("&iacute;","i",$String);
	    $String = str_replace("&Iacute;","I",$String);
	    $String = str_replace("&oacute;","o",$String);
	    $String = str_replace("&Oacute;","O",$String);
	    $String = str_replace("&uacute;","u",$String);
	    $String = str_replace("&Uacute;","U",$String);

	    $String = str_replace("\u00C1;","Á",$String);
	    $String = str_replace("\u00E1;","á",$String);
	    $String = str_replace("\u00C9;","É",$String);
	    $String = str_replace("\u00E9;","é",$String);
	    $String = str_replace("\u00CD;","Í",$String);
	    $String = str_replace("\u00ED;","í",$String);
	    $String = str_replace("\u00D3;","Ó",$String);
	    $String = str_replace("\u00F3;","ó",$String);
	    $String = str_replace("\u00DA;","Ú",$String);
	    $String = str_replace("\u00FA;","ú",$String);
	    $String = str_replace("\u00DC;","Ü",$String);
	    $String = str_replace("\u00FC;","ü",$String);
	    $String = str_replace("\u00D1;","Ṅ",$String);
	    $String = str_replace("\u00F1;","ñ",$String);

	    $String = str_replace("A", "a", $String);
	    return $String;
	}

	private function remove_espacios($String){
		$String = str_replace(" ", "_", trim($String));
		return $String;
	}
}