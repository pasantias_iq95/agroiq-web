<?php defined('PHRAPI') or die("Direct access not allowed!");

header('Content-Type: text/html; charset=utf-8');
// /*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

ini_set('max_execution_time', 300);
ini_set('memory_limit', '-1');

// /*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/
// ini_set('display_errors',1);
error_reporting(E_ALL);

class Refactorizar
{
    private $db;
	private $session;
	private $agent;

	public function __construct()
	{
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
		// $this->db = DB::getInstance("marun");
	}

	private function DD($string){
		echo "<pre>";
			D($string);
		echo "</pre>";
	}

	public function index(){
		$data = $this->db->queryAll("SELECT archivoJson as json FROM merma WHERE id_finca = 4");
		$destination = PHRAPI_PATH.'../json/merma/';
		foreach($data as $row){
			$filename = explode("/", $row->json);
			$filename = $filename[4];
			$path = PHRAPI_PATH.'../json/completados/marun/merma/'.$filename;
			if(!rename($path, $destination.$filename)){
				$this->DD("{$filename} No se pudo mover");
			}else{
				$this->DD("{$filename} Se movio");
			}
		
		}		

		/*$path = "/home/procesosiq/public_html/agroaudit/";
		$sql = "SELECT id AS id_muestra, id_finca, archivoJson FROM calidad WHERE archivoJson != '' AND id < 407 GROUP BY archivoJson ORDER BY id DESC";
		$data = $this->db->queryAll($sql);
		foreach ($data as $key => $value) {
			$value->archivoJson = explode("/quintana", $value->archivoJson);
			$pathJSON = $path.$value->archivoJson[0]."/quintana/calidad".$value->archivoJson[1];
			$json = json_decode(trim(file_get_contents($pathJSON)), true);
			#$this->process_json($json , $value->archivoJson , $value->id);
			#$this->process($value->id);			
			$this->process_json($json , $value->archivoJson , $value->id_muestra);
		}*/
	}

	private function process_json($json , $filename , $id_valor) {
		$pages = $json['pages'];
		$position = 2;
		
		/*if(count($pages) > 0){
			if(is_array($pages[0]["answers"][$position])){
				$val = $pages[14]["answers"][$position]["values"][0];
				$sql = "UPDATE calidad SET cantidad_dedos_caja = '{$val}' WHERE id = '{$id_valor}'";
				#$this->db->query($sql);
				$this->DD($sql);
			}
		}*/
		if(count($pages) > 0){
			if(is_array($pages[2]["answers"])){
				$dedo3 = 0;
				$dedo4 = 0;
				$dedo5 = 0;
				$dedo6 = 0;
				$dedo7 = 0;
				$dedo8 = 0;
				$dedo9 = 0;

				foreach($pages[2]["answers"] as $key => $value){
					if($key > 0 && $key <= 10){
						if(isset($value["values"][0])){
							$dedos = $value["values"][0];
							if($dedos == 3){
								$dedo3++;
							}
							if($dedos == 4){
								$dedo4++;
							}
							if($dedos == 5){
								$dedo5++;
							}
							if($dedos == 6){
								$dedo6++;
							}
							if($dedos == 7){
								$dedo7++;
							}
							if($dedos == 8){
								$dedo8++;
							}
							if($dedos == 9){
								$dedo9++;
							}
						}
					}
				}
				/*if($dedo3 > 0)
					$this->db->query("INSERT INTO calidad_cluster SET cluster = 'Cluster de 3 dedos', cantidad = '{$dedo3}', id_calidad = '{$id_valor}'");
				if($dedo4 > 0)
					$this->db->query("INSERT INTO calidad_cluster SET cluster = 'Cluster de 4 dedos', cantidad = '{$dedo4}', id_calidad = '{$id_valor}'");
				if($dedo5 > 0)
					$this->db->query("INSERT INTO calidad_cluster SET cluster = 'Cluster de 5 dedos', cantidad = '{$dedo5}', id_calidad = '{$id_valor}'");
				if($dedo6 > 0)
					$this->db->query("INSERT INTO calidad_cluster SET cluster = 'Cluster de 6 dedos', cantidad = '{$dedo6}', id_calidad = '{$id_valor}'");
				if($dedo7 > 0)
					$this->db->query("INSERT INTO calidad_cluster SET cluster = 'Cluster de 7 dedos', cantidad = '{$dedo7}', id_calidad = '{$id_valor}'");
				if($dedo8 > 0)
					$this->db->query("INSERT INTO calidad_cluster SET cluster = 'Cluster de 8 dedos', cantidad = '{$dedo8}', id_calidad = '{$id_valor}'");
				if($dedo9 > 0)
					$this->db->query("INSERT INTO calidad_cluster SET cluster = 'Cluster de 9 dedos', cantidad = '{$dedo9}', id_calidad = '{$id_valor}'");*/
			}
		}
	}

	private function espacios($string){
		return $this->limpiar(str_replace(" ", "_", $string));
	}

	private function limpiar($String) {

	    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	    $String = str_replace(array('í','ì','î','ï'),"i",$String);
	    $String = str_replace(array('é','è','ê','ë'),"e",$String);
	    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
	    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	    $String = str_replace("ç","c",$String);
	    $String = str_replace("Ç","C",$String);
	    $String = str_replace("ñ","n",$String);
	    $String = str_replace("Ñ","N",$String);
	    $String = str_replace("Ý","Y",$String);
	    $String = str_replace("ý","y",$String);
	    
	    $String = str_replace("&aacute;","a",$String);
	    $String = str_replace("&Aacute;","A",$String);
	    $String = str_replace("&eacute;","e",$String);
	    $String = str_replace("&Eacute;","E",$String);
	    $String = str_replace("&iacute;","i",$String);
	    $String = str_replace("&Iacute;","I",$String);
	    $String = str_replace("&oacute;","o",$String);
	    $String = str_replace("&Oacute;","O",$String);
	    $String = str_replace("&uacute;","u",$String);
	    $String = str_replace("&Uacute;","U",$String);

	    $String = str_replace("\u00C1;","Á",$String);
	    $String = str_replace("\u00E1;","á",$String);
	    $String = str_replace("\u00C9;","É",$String);
	    $String = str_replace("\u00E9;","é",$String);
	    $String = str_replace("\u00CD;","Í",$String);
	    $String = str_replace("\u00ED;","í",$String);
	    $String = str_replace("\u00D3;","Ó",$String);
	    $String = str_replace("\u00F3;","ó",$String);
	    $String = str_replace("\u00DA;","Ú",$String);
	    $String = str_replace("\u00FA;","ú",$String);
	    $String = str_replace("\u00DC;","Ü",$String);
	    $String = str_replace("\u00FC;","ü",$String);
	    $String = str_replace("\u00D1;","Ṅ",$String);
	    $String = str_replace("\u00F1;","ñ",$String);

	    $String = str_replace("A", "a", $String);
	    return $String;
	}


}