<?php defined('PHRAPI') or die("Direct access not allowed!");

header('Content-Type: text/html; charset=utf-8');
// /*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/

ini_set('max_execution_time', 300);
ini_set('memory_limit', '-1');

// /*----------  SOLO SI EL ARCHIVO ES MUY GRANDE  ----------*/
// ini_set('display_errors',1);
error_reporting(E_ALL);

class RefactorizarAgroaudit
{
    private $db;
	private $session;
	private $agent;

	public function __construct()
	{
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        #$this->db = DB::getInstance($this->session->agent_user);
		$this->db = DB::getInstance("marun");
	}

	private function DD($string){
		echo "<pre>";
			D($string);
		echo "</pre>";
	}

	public function index(){
		/*$data = [];
		$path = "/home/procesosiq/public_html/agroaudit/";
		$sql = "SELECT id , id_muestra, archivoJson FROM muestras_plantas GROUP BY archivoJSON";
		$data = $this->db->queryAll($sql);
		foreach ($data as $key => $value) {
			$json = json_decode(trim(file_get_contents($path.$value->archivoJson)), true);
			#$this->process_json($json , $value->archivoJson , $value->id);
			#$this->process($value->id);
			#$this->DD($value);
			$this->process_labores($json , $value->archivoJson , $value->id_muestra);
			// F("FIN!");
		}*/

		$path = realpath("/home/procesosiq/public_html/agroaudit/json/completados/marun/labores/");
		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::LEAVES_ONLY);
		foreach($objects as $name => $object){
		    if('.' != $object->getFileName() && '..' != $object->getFileName()){
				if(strpos($object->getPathName(), "completados/marun/labores") !== false){
					$ext = pathinfo($object->getPathName(), PATHINFO_EXTENSION);
					if('json' == $ext){
						switch ($ext){
							case 'json':
								$ruta = pathinfo($object->getPathName(), PATHINFO_DIRNAME);
								$carpetas = explode("/", $ruta);
								$num_dir = count($carpetas)-1;
								#$filename = $path."/".pathinfo($object->getPathName(), PATHINFO_BASENAME);
								$filename = pathinfo($object->getPathName(), PATHINFO_BASENAME);
								$json = json_decode(trim(file_get_contents($object->getPathName())), true);

								$this->DD($filename);
								$this->process_labores($json, $filename);

								/*$ch = curl_init();
								curl_setopt($ch, CURLOPT_URL,"http://app.procesosiq.com/phrapi/prontoforms/labores");
								curl_setopt($ch, CURLOPT_POST, 1);
								curl_setopt($ch, CURLOPT_POSTFIELDS, "json=".json_encode($json)."&filename=".$filename);

								curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

								$server_output = curl_exec($ch);

								curl_close($ch);
								echo $server_output;*/
								break;

							default:
								break;
						}
					}
				}
		    }
		}
	}

	private function process_labores($json , $filename ){
		$user = $json['user'];
		$user_identifier = $user['identifier'].'';;
		$user_username = $user['username'].'';;
		$referenceNumber = $json["referenceNumber"];
		$pages = $json['pages'];
		$geoStamp = $json["geoStamp"];
		$coordinates = $geoStamp["coordinates"];
		$latitude = $coordinates["latitude"];
		$longitude = $coordinates["longitude"];
		// $sql = "SELECT nombre,id FROM labores";
		// $resultado = $this->db->queryAll($sql);
		// foreach($resultado as $fila){
		//	$labores[$fila->nombre] =  $this->limpiar(trim(strtoupper($fila->nombre)));
		//	$labores_ids[$fila->nombre] = $fila->id;
		// }

		// $sql_causa = "SELECT id ,valor , causa , (SELECT nombre FROM labores WHERE id = idLabor) AS labor FROM labores_causas WHERE status = 1 AND valor = 1 AND tipo = 'D'";
		// $resultado_causa = $mysqli->query($sql_causa);
		// while($fila_causa = $resultado_causa->fetch_assoc()){
		//     $labor_causa[$fila_causa['labor']] =  strtoupper(limpiar($fila_causa['causa']));
		//     $labor_causa_ids[$fila_causa['labor']] = $fila_causa['id'];
		// }
		// D($labor_causa);
		$sql_muestra_causas = array();
		$pagina_nombre = "";
		$labor = "";
		$objectMuestras = new stdClass;
		$objectErros = new stdClass;
		$objectMuestras->json = $filename;
		$objectMuestras->indentifier = $user_identifier;
		$objectMuestras->referenceNumber = $referenceNumber;
		$objectMuestras->auditor = $user["username"];
		$objectMuestras->auditorName = $user["displayName"];
		$contador = 0;
		$existZona = 0;
		$existHacienda = 0;
		$existLote = 0;
		// if($filename == "20160524-1840781808.json");			
		if($filename == $filename){
			foreach($pages as $key => $page_data){
				$pagina = $key+1;
				$sql_muestra_causas[] = [];
				if($pagina_nombre != strtoupper(trim($this->limpiar($page_data['name'])))){
					$pagina_nombre = strtoupper(trim($this->limpiar($page_data['name'])));
					$contador++;
				}
				
				$answers = $page_data["answers"];
				foreach($answers as $x => $answer){
					$label    = $answer['label'];
					$dataType = $answer['dataType'];
					$question = $answer['question'];
					$values   = $answer['values'];
					$labelita = strtoupper(trim($this->limpiar($label)));
					$question = strtoupper(trim($this->limpiar($question)));
					
					if($pagina_nombre == "ENCABEZADO"){
						if($question == "FECHA"){
							$objectMuestras->details["Encabezado"]["Fecha"] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
						if($question == "HORA"){
							$objectMuestras->details["Encabezado"]["Hora"] = "'".(isset($answer["values"][0]) ? substr($answer["values"][0]["provided"]["time"], 0, -6) : '')."'";   
						}
						if($question == "AUDITOR"){
							$objectMuestras->details["Encabezado"]["Auditor"] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";   
						}
						if($question == "FINCA"){
							$objectMuestras->details["Encabezado"]["Finca"] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";   
						}
						if($question == "LOTE"){
							$objectMuestras->details["Encabezado"]["Lote"] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
						if($question == "SUB FINCA - ID"){
							$objectMuestras->details["Encabezado"]["idSubFinca"] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
						if($question == "SUB FINCA"){
							$objectMuestras->details["Encabezado"]["subFinca"] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}

					if($pagina_nombre == strtoupper("Enfunde")){
						// D($values);
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Enfunde"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Enfunde"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Enfunde"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}
						if(strpos($question, "PLANTA") !== false){

							$value = (isset($answer["values"]) ? $answer["values"] : 0);
							if(sizeof($value) > 0){
								foreach ($value as $valKey => $valueData) {
									if($valueData){
										$objectMuestras->details["Enfunde"][$question][] = $valueData;
									}
								}
							}
						}   
						if(strtoupper($question) == "FOTOS"  && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Enfunde"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Enfunde"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Enfunde"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Enfunde"]["nota_voz"] = "";
							}
						}
					}
					// D($pagina_nombre);
					if($pagina_nombre == strtoupper("Proteccion")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Proteccion"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Proteccion"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Proteccion"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}

						if(strpos($question, "PLANTA") !== false){
							$value = (isset($answer["values"]) ? $answer["values"] : 0);
							if(sizeof($value) > 0){
								foreach ($value as $valKey => $valueData) {
									if($valueData){
										$objectMuestras->details["Proteccion"][$question][] = $valueData;
									}
								}
							}
						}
						
						if(strtoupper($question) == "FOTOS"  && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Proteccion"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Proteccion"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Proteccion"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Proteccion"]["nota_voz"] = "";
							}
						}
					}
					if($pagina_nombre == strtoupper("Desvio Hijos")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Desvio"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Desvio"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Desvio"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}

						if(strpos($question, "PLANTA") !== false){
							$value = (isset($answer["values"]) ? $answer["values"] : 0);
							if(sizeof($value) > 0){
								foreach ($value as $valKey => $valueData) {
									if($valueData){
										$objectMuestras->details["Desvio"][$question][] = $valueData;
									}
								}
							}
						}
						if(strtoupper($question) == "FOTOS"  && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Desvio"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Desvio"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Desvio"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Desvio"]["nota_voz"] = "";
							}
						}
					}
					if($pagina_nombre == strtoupper("Puntal")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Puntal"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Puntal"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Puntal"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}

						if(strpos($question, "PLANTA") !== false){
							$value = (isset($answer["values"]) ? $answer["values"] : 0);
							if(sizeof($value) > 0){
								foreach ($value as $valKey => $valueData) {
									if($valueData){
										$objectMuestras->details["Puntal"][$question][] = $valueData;
									}
								}
							}
						}
						if(strtoupper($question) == "FOTOS"  && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Puntal"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Puntal"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Puntal"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Puntal"]["nota_voz"] = "";
							}
						}
					}
					if($pagina_nombre == strtoupper("Deshoje")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Deshoje"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Deshoje"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Deshoje"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}

						if(strpos($question, "PLANTA") !== false){
							$value = (isset($answer["values"]) ? $answer["values"] : 0);
							if(sizeof($value) > 0){
								foreach ($value as $valKey => $valueData) {
									if($valueData){
										$objectMuestras->details["Deshoje"][$question][] = $valueData;
									}
								}
							}
						}
						if(strtoupper($question) == "FOTOS"  && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Deshoje"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Deshoje"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Deshoje"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Deshoje"]["nota_voz"] = "";
							}
						}
					}
					if($pagina_nombre == strtoupper("Seleccion")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Seleccion"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Seleccion"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Seleccion"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}

						if(strpos($question, "PLANTA") !== false){
							$value = (isset($answer["values"]) ? $answer["values"] : 0);
							if(sizeof($value) > 0){
								foreach ($value as $valKey => $valueData) {
									if($valueData){
										$objectMuestras->details["Seleccion"][$question][] = $valueData;
									}
								}
							}
						}
						if(strtoupper($question) == "FOTOS"  && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Seleccion"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Seleccion"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Seleccion"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Seleccion"]["nota_voz"] = "";
							}
						}
					}
					if($pagina_nombre == strtoupper("Limpieza")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Limpieza"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Limpieza"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Limpieza"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];							
						}

						if(strpos($question, "PLANTA") !== false){
							$value = (isset($answer["values"]) ? $answer["values"] : 0);
							if(sizeof($value) > 0){
								foreach ($value as $valKey => $valueData) {
									if($valueData){
										$objectMuestras->details["Limpieza"][$question][] = $valueData;
									}
								}
							}
						}
						if(strtoupper($question) == "FOTOS"  && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Limpieza"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Limpieza"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Limpieza"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Limpieza"]["nota_voz"] = "";
							}
						}
					}
					if($pagina_nombre == strtoupper("Malezas")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Malezas"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Malezas"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Malezas"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}

						if(strtoupper($question) == "FOTOS" && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Malezas"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Malezas"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Malezas"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Malezas"]["nota_voz"] = "";
							}
						}

						if(strtoupper($question) != "FOTOS" && strtoupper($question) != "COMENTARIOS" && strtoupper($question) != "NOTA DE VOZ" && strtoupper($question) != strtoupper("GEOLOCALIZACIoN")){
							$objectMuestras->details["Malezas"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == strtoupper("Drenaje")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Drenaje"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Drenaje"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Drenaje"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}

						if(strtoupper($question) == "FOTOS" && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Drenaje"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Drenaje"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0])){
								if(isset($values[0]["filename"])){
									#$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
									$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
									$objectMuestras->details["Drenaje"]["nota_voz"] = $audio_comen;
								}else{
									$objectMuestras->details["Drenaje"]["nota_voz"] = "";
								}
							}else{
								$objectMuestras->details["Drenaje"]["nota_voz"] = "";
							}
						}

						if(strtoupper($question) != "FOTOS" && strtoupper($question) != "COMENTARIOS" && strtoupper($question) != "NOTA DE VOZ" && strtoupper($question) != strtoupper("GEOLOCALIZACIoN")){
							$objectMuestras->details["Drenaje"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == strtoupper("Fertilizacion")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Fertilizacion"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Fertilizacion"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Fertilizacion"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}

						if(strtoupper($question) == "FOTOS" && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Fertilizacion"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Fertilizacion"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Fertilizacion"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Fertilizacion"]["nota_voz"] = "";
							}
						}

						if(strpos($question, "PLANTA") !== false){
							$value = (isset($answer["values"]) ? $answer["values"] : 0);
							if(sizeof($value) > 0){
								foreach ($value as $valKey => $valueData) {
									if($valueData){
										$objectMuestras->details["Fertilizacion"][$question][] = $valueData;
									}
								}
							}
						}

						/*if(strtoupper($question) != "FOTOS" && strtoupper($question) != "COMENTARIOS" && strtoupper($question) != "NOTA DE VOZ" && strtoupper($question) != strtoupper("GEOLOCALIZACIoN")){
							$objectMuestras->details["Fertilizacion"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}*/
					}
					if($pagina_nombre == strtoupper("Riego")){
						if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
							$objectMuestras->details["Riego"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
							$objectMuestras->details["Riego"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
							$objectMuestras->details["Riego"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						}

						if(strtoupper($question) == "FOTOS" && isset($values[0]["filename"])){
							foreach($values as $foto){
								$objectMuestras->details["Riego"]["Photos"][] = $this->espacios($foto["filename"]);
							}
						}

						if(strtoupper($question) == "COMENTARIOS"){
							$objectMuestras->details["Riego"]["comentarios"] = "'".(isset($values[0]) ? $values[0] : '')."'";
						}

						if(strtoupper($question) == "NOTA DE VOZ"){
							if(isset($values[0]['filename'])){
								$ext = pathinfo($values[0]['filename'], PATHINFO_EXTENSION);
								$audio_comen = $this->espacios($referenceNumber.'_'.$values[0]['filename']);
								$objectMuestras->details["Riego"]["nota_voz"] = $audio_comen;
							}else{
								$objectMuestras->details["Riego"]["nota_voz"] = "";
							}
						}

						if(strtoupper($question) != "FOTOS" && strtoupper($question) != "COMENTARIOS" && strtoupper($question) != "NOTA DE VOZ" && strtoupper($question) != strtoupper("GEOLOCALIZACIoN")){
							$objectMuestras->details["Riego"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == strtoupper("Observaciones")){

						// if(strtoupper($question) == strtoupper("GEOLOCALIZACIoN") && isset($values[0]["success"])){
						//	$objectMuestras->details["Observaciones"]["Geolocalizacion"]["lat"] = $values[0]["coordinates"]["latitude"];
						//	$objectMuestras->details["Observaciones"]["Geolocalizacion"]["lng"] = $values[0]["coordinates"]["longitude"];
						//	$objectMuestras->details["Observaciones"]["Geolocalizacion"]["alt"] = $values[0]["coordinates"]["altitude"];
						// }
						if($question == strtoupper("Observaciones generales")){
							$objectMuestras->details["Observaciones"]["observaciones_generales"] = (isset($answer["values"][0]) ? $answer["values"][0] : '');
						}
						if($question == strtoupper("Antes de enviar:")){
							$objectMuestras->details["Observaciones"]["antes_enviar"] = (isset($answer["values"][0]) ? $answer["values"][0] : '');
						}
					}
					//----------  MUESTRAS  ----------
				}
			}
			$objectMuestras->json = "json/completados/marun/labores/".$objectMuestras->json;
			$file_json = json_encode($json);
			$Encabezado         = new stdClass;
			$Enfunde            = new stdClass;
			$Proteccion         = new stdClass;
			$Desvio             = new stdClass;
			$Puntal             = new stdClass;
			$Deshoje            = new stdClass;
			$Seleccion          = new stdClass;
			$Limpieza           = new stdClass;
			$Malezas            = new stdClass;
			$Drenaje            = new stdClass;
			$Fertilizacion      = new stdClass;
			$Riego              = new stdClass;
			$Observaciones      = new stdClass;        

			$Encabezado         = (object)$objectMuestras->details["Encabezado"];
			$Enfunde            = (object)$objectMuestras->details["Enfunde"];
			$Proteccion         = (object)$objectMuestras->details["Proteccion"];
			$Desvio             = (object)$objectMuestras->details["Desvio"];
			$Puntal             = (object)$objectMuestras->details["Puntal"];
			$Deshoje            = (object)$objectMuestras->details["Deshoje"];
			$Seleccion          = (object)$objectMuestras->details["Seleccion"];
			$Limpieza           = (object)$objectMuestras->details["Limpieza"];
			$Malezas            = (object)$objectMuestras->details["Malezas"];
			$Drenaje            = (object)$objectMuestras->details["Drenaje"];
			$Fertilizacion      = (object)$objectMuestras->details["Fertilizacion"];
			$Riego              = (object)$objectMuestras->details["Riego"];
			$Observaciones      = (object)$objectMuestras->details["Observaciones"];
			#$this->DD($objectMuestras);
			$costo = 0;
						
			$idFinca = "SELECT id FROM fincas WHERE nombre = {$Encabezado->Finca}";
			$idFinca = $this->db->queryRow($idFinca)->id;

			$idLote = "SELECT id FROM lotes WHERE nombre = {$Encabezado->Lote} AND idFinca = '{$idFinca}'";
			$idLote = $this->db->queryRow($idLote)->id;

			#$sql = "INSERT INTO agroaudit SET fecha = {$Encabezado->Fecha}, hora = {$Encabezado->Hora}, auditor = {$Encabezado->Auditor}, finca = {$Encabezado->Finca}, lote = {$Encabezado->Lote}, idFinca = '{$idFinca}', idLote = '{$idLote}', observaciones_general = '".$this->sanitize($Observaciones->observaciones_generales)."', antes_enviar = '".$this->sanitize($Observaciones->antes_enviar)."'";
			$sql = "INSERT INTO agroaudit SET fecha = {$Encabezado->Fecha}, hora = {$Encabezado->Hora}, auditor = {$Encabezado->Auditor}, finca = {$Encabezado->Finca}, lote = {$Encabezado->Lote}, idFinca = '{$idFinca}', idLote = '{$idLote}', idSubFinca = {$Encabezado->idSubFinca}, subFinca = {$Encabezado->subFinca}";
			$this->DD($sql);
			#$this->db->query($sql);
			$id_muestra = $this->db->getLastID();

			// $detalle = "INSERT INTO muestras_detalle SET id_muestra = {$id_muestra}, ";
			// $detalle .= $this->concatAudioComentario($Enfunde, "enfunde").",";
			// $detalle .= $this->concatAudioComentario($Proteccion, "proteccion").",";
			// $detalle .= $this->concatAudioComentario($Desvio, "desvio").",";
			// $detalle .= $this->concatAudioComentario($Puntal, "puntal").",";
			// $detalle .= $this->concatAudioComentario($Deshoje, "deshoje").",";
			// $detalle .= $this->concatAudioComentario($Seleccion, "seleccion").",";
			// $detalle .= $this->concatAudioComentario($Limpieza, "limpieza").",";
			// $detalle .= $this->concatAudioComentario($Malezas, "malezas").",";
			// $detalle .= $this->concatAudioComentario($Drenaje, "drenaje").",";
			// $detalle .= $this->concatAudioComentario($Riego, "riego").",";
			// $detalle .= $this->concatAudioComentario($Fertilizacion, "fertilizacion");
			#$mysqli->query($detalle);
			#$this->DD($detalle);

			$this->generateInsert2($Enfunde  , "Enfunde", $objectMuestras, $id_muestra, $Encabezado, $filename);
			$this->generateInsert2($Proteccion , 'Protección' , $objectMuestras, $id_muestra, $Encabezado, $filename);
			$this->generateInsert2($Desvio , 'Desvío Hijos' ,  $objectMuestras, $id_muestra, $Encabezado, $filename);
			$this->generateInsert2($Deshoje , 'Deshoje', $objectMuestras, $id_muestra, $Encabezado, $filename);
			$this->generateInsert2($Puntal , 'Puntal', $objectMuestras, $id_muestra, $Encabezado, $filename);
			$this->generateInsert2($Seleccion ,'Selección' ,  $objectMuestras, $id_muestra, $Encabezado. $filename);
			$this->generateInsert2($Limpieza ,'Limpieza' , $objectMuestras, $id_muestra, $Encabezado, $filename);
			// PROCESO 2 -- Victor 
			$this->generateInsert2($Malezas ,'Malezas' , $objectMuestras, $id_muestra, $Encabezado, $filename);
			$this->generateInsert2($Drenaje ,'Drenaje' , $objectMuestras, $id_muestra, $Encabezado, $filename);
			$this->generateInsert2($Riego ,'Riego' , $objectMuestras, $id_muestra, $Encabezado, $filename);
			$this->generateInsert2($Fertilizacion ,'Fertilización' , $objectMuestras, $id_muestra, $Encabezado, $filename);
			// $this->DD(["json" => $objectMuestras->json,"Enfunde" => $Enfunde->Photos, "Proteccion" => $Proteccion->Photos, "Desvio" => $Enfunde->Photos, "Deshoje" => $Deshoje->Photos, "Puntal" => $Puntal->Photos, "Seleccion" => $Seleccion->Photos, "Limpieza" => $Limpieza->Photos, "Malezas" => $Malezas->Photos, "Drenaje" => $Drenaje->Photos, "Riego" => $Riego->Photos, "Fertilizacion" => $Fertilizacion->Photos]);
		}
	}

	private function generateInsert2($data, $type = "Malezas", $objectMuestras, $id_muestra, $Encabezado, $filename){
		$Geolocalizacion = new stdClass;
		if(isset($data->Geolocalizacion)){
			$Geolocalizacion = (object)$data->Geolocalizacion;
		}
		$Photos = new stdClass;
		if(isset($data->Photos)){
			$Photos = $data->Photos;
			$this->DD($Photos);
			foreach($Photos as $foto){
				#$this->db->query("INSERT INTO agroaudit_photos SET id_agroaudit = '{$id_muestra}', type = '{$type}', photo = '{$foto}'");
			}
		}
		foreach ($data as $key => $value) {
			$value = (object)$value;
			if(!in_array($key, ["comentarios","nota_voz","Geolocalizacion","Photos"]))
			foreach ($value as $llave => $valor) {
				switch(trim($valor)){
					case "1":
					case "'1'":
					case "'Si'":
					case "'Bueno'":
					case "'Buena'":
					case "'Buen Estado'":
						$valor2 = 1;
						break;
					case "0":
					case "'0'":
					case "'No'":
					case "'Malo'":
					case "'Mala'":
					case "'Mal estado'":
						$valor2 = 0;
						break;
					default:
						$valor2 = -1;
						break;
				}
				foreach($value as $mCausa){
					$mCausa = $this->getCausa($mCausa, $type);
					#$causas = $this->getLaboresCausas(strtoupper($type), $mCausa->causa);

					$sql_causas = "INSERT INTO agroaudit_causas 
						SET causa = '{$mCausa->causa}', type = '{$type}', id_agroaudit = '{$id_muestra}', valor = '{$mCausa->valor}'";
					$this->DD($sql_causas);
					#$this->db->query($sql_causas);
				}
				$this->updateMuestrasAnual($Encabezado->Fecha, $Encabezado->Finca, $Encabezado->Lote, $type, $filename, $Geolocalizacion , $Photos, $id_muestra , $objectMuestras);
			}
		}
		#$this->updateDetalle($type, $id_muestra);
	}

	private function getCausa($causa, $type){
		if(strpos($causa, "'") !== false){
			$causa = str_replace("'", "", $causa);
		}
		$sql = "SELECT labores_causas.id, causa, valor
				FROM labores_causas
				INNER JOIN labores ON idLabor = labores.id
				WHERE causa = '{$causa}' AND nombre = '{$type}'";
		$exist = $this->db->queryAll($sql);
		if(count($exist) == 0){
			$parts = explode("-", $causa);
			if(count($parts) > 1){
				$tmp = "";
				unset($parts[0]);
				foreach($parts as $key => $c){
					if($key > 1)
						$tmp .= "-";
					$tmp .= $c;
				}
				$sql = "SELECT labores_causas.id, causa, valor
						FROM labores_causas
						INNER JOIN labores ON idLabor = labores.id
						WHERE causa = '{$tmp}' AND nombre = '{$type}'";
				$exist = $this->db->queryAll($sql);
			}
		}
		return $exist[0];
	}

	private function updateDetalle($type, $id_muestra){
		$isUpdate = $this->db->queryAll("SELECT TYPE, promedio
										FROM agroaudit_detalle
										WHERE id_agroaudit = '{$id_muestra}' AND type = '{$type}'
										GROUP BY TYPE");
		$sql = "SELECT TYPE, causa, SUM(valor) AS valor
				FROM agroaudit_causas
				WHERE id_agroaudit = '{$id_muestra}' AND type = '{$type}'
				GROUP BY TYPE";
		$promedio = $this->db->queryRow($sql);		
		if(isset($promedio->valor)){
			$promedio = (100 - $promedio->valor > 0) ? 100 - $promedio->valor : 0;
			if(count($isUpdate) > 0){
				$sql = "UPDATE agrouadit_detalle 
						SET 
							promedio = {$promedio}
						WHERE type = '{$type}' AND id_agroudit = '{$id_muestra}'";			
			}else{
				$sql = "INSERT INTO agroaudit_detalle
						SET
							promedio = {$promedio},
							type = '{$type}',
							id_agroaudit = '{$id_muestra}',
							flag = '-1'";
			}
			$this->DD($sql);
			$this->db->query($sql);
		}
	}

	private function getLaboresCausas($labor, $causa){
		$sql = "SELECT valor AS label , causa AS id FROM labores_causas
                INNER JOIN labores ON labores_causas.idLabor = labores.id
                WHERE nombre = '{$labor}' AND causa = '{$causa}'";		
		return $this->db->queryAll($sql);
	}

	private function sanitize($String){
		$String = str_replace("'","\'",$String);
		return $String;
	}

	private function updateMuestrasAnual($fecha, $finca, $lote, $labor, $json , $Geolocalizacion , $Photos , $idMuestra , $objectMuestras){
		
		$idFinca = "SELECT id FROM fincas WHERE nombre = {$finca}";
		#$res = $mysqli->query($idFinca);
		#$idFinca = $res->fetch_assoc()["id"];
		$idFinca = $this->db->queryRow($idFinca)->id;

		$idLote = "SELECT id FROM lotes WHERE nombre = {$lote} AND idFinca = '{$idFinca}'";
		#$res = $mysqli->query($idLote);
		#$idLote = $res->fetch_assoc()["id"];
		$idLote = $this->db->queryRow($idLote)->id;

		$idLabor = "SELECT id FROM labores WHERE nombre = '{$labor}'";
		#$idLabor = $mysqli->query($idLabor)->fetch_assoc()["id"];
		$idLabor = $this->db->queryRow($idLabor)->id;

		$Resperiodo = "SELECT periodo FROM periodo WHERE fecha BETWEEN DATE({$fecha}) AND DATE({$fecha}) LIMIT 1";
		#$periodo = (double)$mysqli->query($Resperiodo)->fetch_assoc()["periodo"];
		$periodo = $this->db->queryRow($Resperiodo)->periodo;

		
		$isUpdate = "SELECT * FROM muestras_anual WHERE mes = (MONTH({$fecha}) - 1) AND idFinca = '{$idFinca}' AND idLote = '{$idLote}' AND idLabor ='{$idLabor}'";
		$res = $this->db->queryAll($isUpdate);
		if(count($res) > 0){
			$isUpdate = $res[0];
			$update = "UPDATE muestras_anual
			SET promedio = (SELECT AVG(promedio) FROM(
								(SELECT IFNULL(SUM(valor), 5) * (100 / cont) AS promedio FROM 
									(SELECT IF(SUM(valor) > 0,1,0) AS valor, COUNT(1) AS cont
									FROM muestras_plantas
									WHERE valor = 1 AND TYPE = '{$labor}' AND archivoJson = '{$json}'
									GROUP BY TYPE, planta,archivoJson
									ORDER BY TYPE ,planta) AS tbla)
								UNION ALL
								(SELECT IFNULL(SUM(valor), 4) * (100 / cont) AS promedio FROM 
									(SELECT IF(SUM(valor) > 0,1,0) AS valor, COUNT(1) AS cont
									FROM muestras_labores
									WHERE valor = 1 AND TYPE = '{$labor}' AND archivoJson LIKE '%{$json}%'
									GROUP BY TYPE, causa,archivoJson
									ORDER BY TYPE ,causa) AS tbla)) AS tbl)
			WHERE mes = (MONTH({$fecha}) - 1) AND idFinca = '{$idFinca}' AND idLote = '{$idLote}' AND idLabor = '{$idLabor}'";
			$this->db->query($update);
			$this->DD($update);
			$id_muestra_anual = (int)$isUpdate->id;
		}else{
			$insert = "INSERT INTO muestras_anual SET fecha = DATE({$fecha}), mes = (MONTH({$fecha}) - 1), semana = WEEK({$fecha}), idFinca = '{$idFinca}', idLote = '{$idLote}', idLabor = '{$idLabor}',
						idTipoLabor = 0, idZona = '1', 
							promedio = (SELECT AVG(promedio) FROM(
											(SELECT IFNULL(SUM(valor),5) * (100 / cont) AS promedio FROM 
												(SELECT IF(SUM(valor) > 0,1,0) AS valor, COUNT(1) AS cont 
												FROM muestras_plantas
												WHERE valor = 1 AND TYPE = '{$labor}' AND archivoJson = '{$json}'
												GROUP BY TYPE, planta,archivoJson
												ORDER BY TYPE ,planta) AS tbla)
											UNION ALL
											(SELECT IFNULL(SUM(valor), 4) * (100 / cont) AS promedio FROM 
												(SELECT IF(SUM(valor) > 0,1,0) AS valor, COUNT(1) AS cont
												FROM muestras_labores
												WHERE valor = 1 AND TYPE = '{$labor}' AND archivoJson LIKE '%{$json}%'
												GROUP BY TYPE, causa,archivoJson
												ORDER BY TYPE ,causa) AS tbla)) AS tbl),
									periodo = $periodo";
			$this->DD($insert);
			$this->db->query($insert);
			$id_muestra_anual = $this->db->getLastId(); #(int)$mysqli->insert_id;
			if(count($Photos) > 0){
				$pathPotho = "/completados/marun/labores/images/".$objectMuestras->referenceNumber."_";
				foreach ($Photos as $llave => $valor) {
					if($valor != ""){
						$Photo = $pathPotho.$valor;
						$idLabor = $this->db->queryRow("SELECT id FROM labores WHERE nombre = '{$labor}'")->id;
						if(count($this->db->queryAll("SELECT * FROM muestras_imagenes WHERE imagen = '{$Photo}' AND idMuestra = '{$idMuestra}' AND id_muestra_anual = {$id_muestra_anual} AND idLabor = '{$idLabor}'"))==0){
							$sql_photos = "INSERT INTO muestras_imagenes SET
								idMuestra = '{$idMuestra}',
								id_muestra_anual = {$id_muestra_anual},
								imagen = '{$Photo}', idLabor = '{$idLabor}',
								archivoJson = '{$objectMuestras->json}',
								archivoJson_contador = '1';";
							#$mysqli->query($sql_photos);
							#$this->db->query($sql_photos);
							$this->DD($sql_photos);
						}
					}
				}
			}
		}
		if($id_muestra_anual > 0){
			if(isset($Geolocalizacion->lat)){
				$sql_geo = "INSERT INTO muestras_coords 
					SET id_muestra = {$id_muestra_anual}  , idMuestraMain = {$idMuestra}, archivoJson_contador = '1' ,
					archivoJson = '{$json}' , lat = '{$Geolocalizacion->lat}',lng =  '{$Geolocalizacion->lng}'";
				#$this->DD($sql_geo);
				#$this->db->query($sql_geo);
				$this->DD($sql_geo);
			}
		}
	}

	private function process($id_muestra = 0){
		$id_muestra = (int)$id_muestra;
		if($id_muestra > 0){
			$sql = "SELECT 
					pesoRacimos.id_merma , 
					ROUND((tallos / racimos_procesados),2) AS peso_prom_tallo , 
					ROUND(racimos_prom,2) AS peso_prom_racimo, 
					ROUND(tot_racimos,2) AS peso_tot_racimo,
					ROUND(cantidad,2) AS peso_neto,
					ROUND(tallos,2) AS tallo,
					ROUND(( tallos/ tot_racimos) * 100 , 2) AS porcentaje_tallo,
					ROUND((tot_racimos - tallos - 6),2) AS racimo,
					ROUND((cantidad / (tot_racimos - tallos - 6)) * 100,2) AS porcentaje_merma,
					ROUND(((cantidad + tallos + 6) / tot_racimos) * 100,2) AS porcentaje_merma_procesada,
					ROUND(((cantidad + tallos + 6 + racimos_tallo_recusado) / (tot_racimos + racimos_tallo_recusado)) * 100,2) AS porcentaje_merma_cortada
					FROM
					(
						SELECT 
							id_merma , AVG(cantidad) AS racimos_prom , SUM(cantidad) tot_racimos 
						FROM
							merma_racimos
						WHERE campo LIKE '%Peso Racimo%' AND id_merma > 0 AND id_merma = '{$id_muestra}'
						GROUP BY id_merma
					) AS pesoRacimos
					INNER JOIN
					(
						SELECT 
							id_merma , SUM(cantidad) racimos_procesados 
						FROM merma_racimos
						WHERE campo LIKE '%# Racimos muestreados Procesados%' AND id_merma > 0 AND id_merma = '{$id_muestra}'
						GROUP BY id_merma
					) AS racimosProcesados 
					ON pesoRacimos.id_merma = racimosProcesados.id_merma
					INNER JOIN 
					(
						SELECT 
							id_merma , SUM(cantidad) AS cantidad 
						FROM merma_detalle
						WHERE id_merma > 0 AND campo LIKE '%Total Peso%' AND  type NOT IN('RECUSADOS','RESULTADOS','FIRMA ELECTRONICA','OTROS')
						GROUP BY id_merma
					) AS pesoNeto 
					ON racimosProcesados.id_merma = pesoNeto.id_merma
					INNER JOIN
					(
						SELECT 
							id_merma , SUM(cantidad) tallos 
						FROM merma_racimos
						WHERE campo LIKE '%Peso Tallo Racimos%' AND id_merma > 0 AND id_merma = '{$id_muestra}'
						GROUP BY id_merma
					) AS pesoTallos 
					ON pesoNeto.id_merma = pesoTallos.id_merma
					INNER JOIN
					(
						SELECT 
							id_merma , SUM(cantidad) racimos_tallo_recusado 
						FROM merma_detalle
						WHERE campo LIKE '%Total Peso Recusados%' AND id_merma > 0 AND id_merma = '{$id_muestra}' AND type = 'RECUSADOS'
						GROUP BY id_merma
					) AS pesoRecusado 
					ON pesoTallos.id_merma = pesoRecusado.id_merma
					ORDER BY pesoTallos.id_merma DESC";
			$data = $this->db->queryRow($sql);

			$categories = $this->db->queryAllSpecial("SELECT 
												type AS id  , SUM(cantidad) AS label
											FROM merma_detalle
											WHERE id_merma > 0 AND campo LIKE '%Total Peso%' AND  type NOT IN('RECUSADOS','RESULTADOS','FIRMA ELECTRONICA','OTROS')
											AND id_merma = {$id_muestra}
											GROUP BY type ");

			$porcentaje_categories = [
				'COSECHA' => 0,
				'INSECTOS' => 0,
				'HONGOS' => 0,
				'FISIOLOGICOS' => 0,
				'ANIMALES' => 0,
				'LOTERO TERRESTRE' => 0,
				'LOTERO AEREO' => 0,
				'EMPACADORA' => 0
			];

			foreach ($categories as $key => $value) {
				$porcentaje_categories[$key] = (double)round($value,2);
			}


			$sql_muestra = "UPDATE merma SET
							peso_prom_tallo = '{$data->peso_prom_tallo}',
							peso_prom_racimo = '{$data->peso_prom_racimo}',
							peso_tot_racimo = '{$data->peso_tot_racimo}',
							porcentaje_tallo = '{$data->porcentaje_tallo}',
							peso_neto = '{$data->peso_neto}',
							tallo = '{$data->tallo}',
							racimo = '{$data->racimo}',
							porcentaje_merma = '{$data->porcentaje_merma}',
							porcentaje_empacadora = ".$porcentaje_categories['EMPACADORA'].",
							porcentaje_cosecha = ".$porcentaje_categories['COSECHA'].",
							porcentaje_insectos = ".$porcentaje_categories['INSECTOS'].",
							porcentaje_hongos = ".$porcentaje_categories['HONGOS'].",
							porcentaje_fisiologicos = ".$porcentaje_categories['FISIOLOGICOS'].",
							porcentaje_animal = ".$porcentaje_categories['ANIMALES'].",
							porcentaje_aereo = ".$porcentaje_categories['LOTERO AEREO'].",
							porcentaje_terrestre = ".$porcentaje_categories['LOTERO TERRESTRE'].",
							porcentaje_merma_procesada = '{$data->porcentaje_merma_procesada}',
							porcentaje_merma_cortada = '{$data->porcentaje_merma_cortada}'
							WHERE id = {$id_muestra}";
			echo '<pre>';
			D($sql_muestra);
			echo '</pre>';
			$this->db->query($sql_muestra);

		}
	}

	private function getPesoCategories($id_uestra = ""){

	}

	private function process_json($json , $filename , $id_valor) {
	    $user = $json['user'];
		$user_identifier = $user['identifier'].'';;
		$user_username = $user['username'].'';;
		$referenceNumber = $json["referenceNumber"];
		$pages = $json['pages'];
		$geoStamp = $json["geoStamp"];
		$coordinates = $geoStamp["coordinates"];
		$latitude = $coordinates["latitude"];
		$longitude = $coordinates["longitude"];
		$sql_muestra_causas = array();
		$pagina_nombre = "";
		$labor = "";
		$objectMuestras = new stdClass;
		$objectErros = new stdClass;
		$objectMuestras->json = $filename;
		$objectMuestras->indentifier = $user_identifier;
		$objectMuestras->referenceNumber = $referenceNumber;
		$objectMuestras->auditor = $user["username"];
		$objectMuestras->auditorName = $user["displayName"];
		$objectMuestras->details["Images"] = [];
		$contador = 0;
		$existZona = 0;
		$existHacienda = 0;
		$existLote = 0;
		$type = "";
		if($filename == $filename){
			foreach($pages as $key => $page_data){
				$pagina = $key+1;
				$sql_muestra_causas[] = [];
				if($pagina_nombre != strtoupper(trim($this->limpiar($page_data['name'])))){
					$pagina_nombre = strtoupper(trim($this->limpiar($page_data['name'])));
					$contador++;
				}
				$answers = $page_data["answers"];
				foreach($answers as $x => $answer){
					$label    = $answer['label'];
					$dataType = $answer['dataType'];
					$question = $answer['question'];
					$values   = $answer['values'];
					$labelita = $this->limpiar(trim(strtolower($label)));
					
					if($pagina_nombre == "ENCABEZADO"){
						if($question == "Fecha"){
							$objectMuestras->details["Encabezado"]["Fecha"] = "'".str_replace("T", " ", substr($values[0]["provided"]["time"],0,-6))."'";
						}
						if($question != "Fecha" && $question != "Geolocalización"){
							$objectMuestras->details["Encabezado"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "PESO TALLOS"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["PesoTallos"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["PesoTallos"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "PESO RACIMOS"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["PesoRacimos"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["PesoRacimos"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "RECUSADOS"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["Recusados"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["Recusados"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "COSECHA"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["Cosecha"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["Cosecha"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "CIF"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["Cif"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["Cif"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "FORMACION"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["Formacion"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["Formacion"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "EMPACADORA"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["Empacadora"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["Empacadora"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "LONGITUD"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["Longitud"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["Longitud"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "SERV AGRICOLAS"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["ServAgrocolas"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["ServAgrocolas"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "OTROS OPTIMECO"){
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["Images"]["OtrosOptimeco"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							if($question != "Evidencia")
								$objectMuestras->details["OtrosOptimeco"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
					if($pagina_nombre == "RESULTADOS GENERALES"){
						$objectMuestras->details["Resultados"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
					}
					if($pagina_nombre == "FIRMA ELECTRONICA"){					
						if(isset($answer["values"][0]["filename"])){
							foreach ($answer["values"] as $key => $value) {
								$objectMuestras->details["FirmaElectronica"][] = $objectMuestras->referenceNumber."_".$value["filename"];
							}
						}else{
							$objectMuestras->details["FirmaElectronica"][$question] = "'".(isset($answer["values"][0]) ? $answer["values"][0] : '')."'";
						}
					}
				}
			}


			$objectMuestras->json = $objectMuestras->json;
			$Encabezado         = new stdClass;
			$Recusados          = new stdClass;
			$Images             = new stdClass;
			$Aereo              = new stdClass;
			$Cosecha            = new stdClass;
			$Insectos           = new stdClass;
			$Hongos             = new stdClass;
			$Fisiologicos       = new stdClass;
			$Animal             = new stdClass;
			$Terrestre          = new stdClass;
			$Empacadora         = new stdClass;
			$Resultados         = new stdClass;
			$FirmaElectronica   = new stdClass;
			$PesoRacimos            = new stdClass;
			$Images             = (object)$objectMuestras->details["Images"];
			$Encabezado         = (object)$objectMuestras->details["Encabezado"];
			$PesoTallos         = (object)$objectMuestras->details["PesoTallos"];    
			$PesoRacimos        = (object)$objectMuestras->details["PesoRacimos"];
			$Recusados          = (object)$objectMuestras->details["Recusados"];
			$Cosecha            = (object)$objectMuestras->details["Cosecha"];
			$Cif                = (object)$objectMuestras->details["Cif"];
			$Formacion          = (object)$objectMuestras->details["Formacion"];
			$Empacadora         = (object)$objectMuestras->details["Empacadora"];
			$Longitud           = (object)$objectMuestras->details["Longitud"];
			$ServAgrocolas      = (object)$objectMuestras->details["ServAgrocolas"];
			$OtrosOptimeco      = (object)$objectMuestras->details["OtrosOptimeco"];
			$Resultados         = (object)$objectMuestras->details["Resultados"];
			$FirmaElectronica   = (object)$objectMuestras->details["FirmaElectronica"];
			$PesoRacimos        = (object)$objectMuestras->details["PesoRacimos"];
			
			$Palanca = $Encabezado->Palanca;
			
			if($Encabezado->Palanca == "Otro" || $Encabezado->Palanca == "''"){
				$Palanca = $Encabezado->{'Nombre nuevo (Palanca)'};
			}

			$finca = [
				"idFinca" => !isset($finca['id']) ? 1 : $Encabezado->{'Finca - id'},
				"finca" => $Encabezado->{'Finca - nombre'},
				"idSubFinca" => $Encabezado->{'Sub Finca - id'},
				"subFinca" => $Encabezado->{'Sub Finca - nombre'}
			];

			
			if(isset($Encabezado->Lote) && $finca['idSubFinca'] != '0'){
				$finca['idLote'] = (int) $this->db->queryOne("SELECT id FROM lotes WHERE nombre LIKE {$Encabezado->Lote} AND idSubFinca = {$finca['idSubFinca']}");
			}else{
				$finca['idLote'] = 0;
			}

			$sql_master = "UPDATE
				merma SET
					bloque                      = {$Encabezado->Lote},
					lote                        = {$Encabezado->Lote},
					id_lote						= '{$finca['idLote']}',
					finca                       = {$finca['finca']},
					id_finca                    = {$finca['idFinca']},
					subFinca                    = {$finca['subFinca']},
					id_subFinca                 = {$finca['idSubFinca']},
					palanca                     = {$Palanca},
					fecha                       = {$Encabezado->Fecha},
					timestamp                   = CURRENT_TIMESTAMP,
					archivoJson                 = '{$objectMuestras->json}',
					auditor                     = {$Encabezado->Evaluador},
					total_defectos              = {$Resultados->{'Total Defectos'}},
					total_peso_merma_neta       = {$Resultados->{'Total peso Merma Neta (Kg)'}},
					porcentaje_empacadora       = {$Resultados->{'% Merma de Empacadora'}},
					porcentaje_cosecha          = {$Resultados->{'% Merma de Cosecha'}},
					porcentaje_cif              = {$Resultados->{'% Merma de CIF'}},
					porcentaje_formacion        = {$Resultados->{'% Merma de Formación'}},
					porcentaje_longitud         = {$Resultados->{'% Merma de Longitud'}},
					porcentaje_serv_agricolas   = {$Resultados->{'% Merma de Serv. Agrícolas'}},
					porcentaje_otros_optimeco   = {$Resultados->{'% Merma de Otros Optimeco'}},
					peso_prom_racimo            = {$Resultados->{'Peso promedio por Racimo'}},
					peso_prom_tallo             = {$Resultados->{'Peso promedio por Tallo'}},
					calibre                     = {$Resultados->{'Calibre'}},
					manos                       = {$Resultados->{'# de Manos'}},
					largo_dedos                 = {$Resultados->{'Largo de dedos'}},
					peso_tot_racimo             = {$Resultados->{'Peso Total Racimos (Kg)'}},
					tallo                       = {$Resultados->{'Tallo (Kg)'}},
					protectores_fundas          = {$Resultados->{'Protectores y Fundas (Kg)'}},
					peso_neto                   = {$Resultados->{'Peso Neto (Kg)'}},
					total_peso_merma            = {$Resultados->{'Total Peso Merma (Kg)'}},
					porcentaje_tallo            = {$Resultados->{'% TALLO'}},
					porcentaje_merma            = {$Resultados->{'% MERMA NETA'}},
					porcentaje_merma_procesada  = {$Resultados->{'% MERMA PROCESADA'}},
					porcentaje_merma_cortada    = {$Resultados->{'% MERMA CORTADA'}}
			WHERE id = '{$id_valor}'";

			if(isset($Encabezado->Lote) && $Encabezado->Lote != ""){
				$this->DD($sql_master);
				$this->db->query($sql_master);
			}
			// $idMuestra = 1;
			#$idMuestra = $mysqli->insert_id;
			$path = "json/completados/dole/merma/";

			/*foreach ($Images as $key => $value) {
				foreach ($value as $llave => $img) {
					$sql = "INSERT INTO merma_images SET id_merma = $idMuestra, type = '{$key}', image = '{$img}', json = '{$objectMuestras->json}';";
					$this->DD($sql);
					#$mysqli->query($sql);
				}
			}

			foreach ($PesoTallos as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'PESO TALLOS', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			}
			foreach ($PesoRacimos as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'PESO RACIMOS', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			}
			foreach ($Cosecha as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'COSECHA', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			}        
			foreach ($Recusados as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'RECUSADOS', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			}        
			foreach ($Cif as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'CIF', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			}       
			foreach ($Formacion as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'FORMACION', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			}  
			foreach ($Longitud as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'LONGITUD', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			}   
			foreach ($Empacadora as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'EMPACADORA', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			} 
			foreach ($ServAgrocolas as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'SERV AGRICOLAS', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			} 
			foreach ($OtrosOptimeco as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'OTROS OPTIMECO', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			} 
			foreach ($Resultados as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'RESULTADOS', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			}       
			foreach ($FirmaElectronica as $key => $value) {
				$sql = "INSERT INTO merma_detalle SET id_merma = $idMuestra, type = 'FIRMA ELECTRONICA', campo = '{$key}', cantidad = {$value};";
				$this->DD($sql);
				#$mysqli->query($sql);
			}


			if($PesoRacimos->{'Racimos Cortados'} != "" && $PesoRacimos->{'Racimos Procesados'} != "" ){
				$sql = "UPDATE merma SET 
					racimos_procesados = {$PesoRacimos->{'Racimos Procesados'}},
					racimos_cortados = {$PesoRacimos->{'Racimos Cortados'}},
					racimos_recusados = {$PesoRacimos->{'Racimos Recusados'}}
				WHERE id = {$idMuestra};";

				$this->DD($sql);
				#$mysqli->query($sql);
			}

			foreach ($PesoRacimos as $key => $value) {
				if($PesoRacimos->{'Peso total Racimos'} != "''"){
					$sql = "INSERT INTO merma_racimos SET
							id_merma = '{$idMuestra}',
							campo = '{$key}',
							cantidad = {$PesoRacimos->{$key}},
							flag = 0;";
					$this->DD($sql);
					#$mysqli->query($sql);
				}
			}


			$sql_flag1 = "UPDATE merma_detalle SET flag = 1 
						WHERE 
						campo NOT LIKE '%(# Daños)%' AND campo NOT LIKE '%Total Peso%' AND campo NOT LIKE '%Total Merma%'
						AND campo NOT LIKE '%Observaciones%'
						AND type NOT IN ('RECUSADOS','RESULTADOS','FIRMA ELECTRONICA','OTROS')";
			
			$this->DD($sql_flag1);
			#$mysqli->query($sql_flag1);

			$sql_flag3 = "UPDATE merma_detalle SET
							flag = 3
							WHERE 
							campo LIKE '%Racimos sin protectores%' OR
							campo LIKE '%Racimos sin corbata%' OR
							campo LIKE '%Información adicional%'";

			$this->DD($sql_flag3);
			#$mysqli->query($sql_flag3);*/

			/*$token = $this->getToken();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"http://app.procesosiq.com/phrapi/reader/fulljson");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "token=".$token."&json=".json_encode($json)."&filename=".$filename);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);

			curl_close($ch);
			echo $server_output;*/
		}
	}

	private function espacios($string){
		return $this->limpiar(str_replace(" ", "_", $string));
	}

	private function getToken(){
		$prefix = "?procesosiq?";

		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$part1 = '';
		$part2 = '';

		$length1 = rand(1, 15);
		$length2 = rand(1, 15);
		for ($i = 0; $i < $length1; $i++) {
			$part1 .= $characters[rand(0, $charactersLength - 1)];
		}
		for ($i = 0; $i < $length2; $i++) {
			$part2 .= $characters[rand(0, $charactersLength - 1)];
		}
		return base64_encode($part1.$prefix.$part2);
	}

	private function limpiar($String) {

	    $String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	    $String = str_replace(array('í','ì','î','ï'),"i",$String);
	    $String = str_replace(array('é','è','ê','ë'),"e",$String);
	    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
	    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	    $String = str_replace("ç","c",$String);
	    $String = str_replace("Ç","C",$String);
	    $String = str_replace("ñ","n",$String);
	    $String = str_replace("Ñ","N",$String);
	    $String = str_replace("Ý","Y",$String);
	    $String = str_replace("ý","y",$String);
	    
	    $String = str_replace("&aacute;","a",$String);
	    $String = str_replace("&Aacute;","A",$String);
	    $String = str_replace("&eacute;","e",$String);
	    $String = str_replace("&Eacute;","E",$String);
	    $String = str_replace("&iacute;","i",$String);
	    $String = str_replace("&Iacute;","I",$String);
	    $String = str_replace("&oacute;","o",$String);
	    $String = str_replace("&Oacute;","O",$String);
	    $String = str_replace("&uacute;","u",$String);
	    $String = str_replace("&Uacute;","U",$String);

	    $String = str_replace("\u00C1;","Á",$String);
	    $String = str_replace("\u00E1;","á",$String);
	    $String = str_replace("\u00C9;","É",$String);
	    $String = str_replace("\u00E9;","é",$String);
	    $String = str_replace("\u00CD;","Í",$String);
	    $String = str_replace("\u00ED;","í",$String);
	    $String = str_replace("\u00D3;","Ó",$String);
	    $String = str_replace("\u00F3;","ó",$String);
	    $String = str_replace("\u00DA;","Ú",$String);
	    $String = str_replace("\u00FA;","ú",$String);
	    $String = str_replace("\u00DC;","Ü",$String);
	    $String = str_replace("\u00FC;","ü",$String);
	    $String = str_replace("\u00D1;","Ṅ",$String);
	    $String = str_replace("\u00F1;","ñ",$String);

	    $String = str_replace("A", "a", $String);
	    return $String;
	}


}