<?php defined('PHRAPI') or die("Direct access not allowed!");

class Reportes {
    public $name;
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        // D($this->session);
        $this->token = '289150995ed28e8860f9161d3cc9f259';
        $this->Service = new ServicesNode('http://procesos-iq.com:3000/' , $this->token);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;
        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        return $response;
    }
    /*=========================================
    =            PANTALLA NUMERO 3            =
    =========================================*/
    
    /*----------  PANTALLA 3 : TABLA POR CAUSAS - GRAFICA CAUSAS  ----------*/
    public function getCausaLabor(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $idLote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);
        $response = new stdClass;
        /**
        
            CONSULA DE CAUSAS:
            - idFinca
            - idLabor
            - idLote
            - Fecha
        
         */
        
        $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
            CASE
                WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
            END AS porcentaje,
            CASE
                WHEN valor > 0 THEN  NULL
                ELSE (CausasIndividual / malas) * 100
            END AS porcentaje2
            FROM 
            (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
            (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND valor > 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
            (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND valor = 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
            (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                    fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}') AS muestras
            FROM muestras 
            WHERE 
                fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'
            GROUP BY idLaborCausa) as t
            ORDER BY valor DESC , causa ASC";
                // D($sql);
        $response->causas = $this->db->queryAll($sql);
        $response->bandera = 75;

        $response->photos = $this->photos($idfinca , $lote , $idLabor , $fecha_inicio , $fecha_fin);

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;
        /**
        
            CONSULTA POR MES GRAFICA DE BARRAS:
            - listado por meses
            - Obtener valor por mes por labores
            - Obtener valor por mes por todas las labores
        
         */
        
        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY semana";
       
        $sql_fincas_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from labores where id = idLabor) AS finca,idFinca , idZona , idLote , idLabor FROM muestras_anual
         WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}' AND idLabor = '{$idLabor}'
        GROUP BY mes , idLabor";
        
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from labores where id = idLabor) AS finca,idFinca , idZona , idLote , idLabor FROM muestras_anual
        WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}' AND idLabor = '{$idLabor}'
            GROUP BY idLabor, semana";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas = new stdClass;
        $response->zonas->fincas = [0 => $this->session->alias] + $this->db->queryAllSpecial("SELECT (SELECT nombre from labores where id = idLabor) AS id  , (SELECT nombre from labores where id = idLabor) AS label FROM muestras_anual 
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}' AND idLabor = '{$idLabor}'
            GROUP BY idLabor");
        $months_fincas = $this->db->queryAll($sql_fincas_month);
        // D($sql_fincas_month);
        $weeks_fincas = $this->db->queryAll($sql_zones_week);
        $response->zonas->auditoria = $this->db->queryAll($sql_fincas_month);
        $response->auditoria = [];

        foreach ($months_fincas as $key => $value) {
            $response->zonas->months[$value->finca][$value->id] = $this->getPromLaborMonth($value->idZona , $value->mes , $value->idFinca ,$value->idLote , $value->idLabor)->promedio;
        }

        foreach ($weeks_fincas as $key => $value) {
            $response->zonas->weeks[$value->finca][$value->label] = $this->getPromLaborWeek($value->idZona , $value->label , $value->idFinca , $value->idLote , $value->idLabor)->promedio;
        }
        return $response;
    }
    /*----------  PANTALLA 3 : TABLA POR CAUSAS  ----------*/

    private function photos($idfinca , $idLote , $idLabor , $fecha_inicio , $fecha_fin){
        $sql = "SELECT imagen , muestras.labor , muestras.laborCausaDesc AS causa FROM muestras_imagenes
                INNER JOIN muestras ON muestras_imagenes.idMuestra = muestras.id
                WHERE 
                fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'";
        $response = $this->db->queryAll($sql);

        return $response;
    }

    /*----------  PANTALLA 3 : PROMEDIO POR MES GENERAL : GRAFICA DE BARRAS  ----------*/
    private function getPromMonthGeneral($month , $lote , $labor , $idfinca){
        $lote = $lote;
        $idfinca = $idfinca;
        $idLabor = $labor;
        $response = new stdClass;
        $response->data = 0;
        $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
            CASE
                WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
            END AS porcentaje,
            CASE
                WHEN valor > 0 THEN  NULL
                ELSE (CausasIndividual / malas) * 100
            END AS porcentaje2
            FROM 
            (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
            (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor > 0 AND idLabor = '{$idLabor}' AND MONTH(fecha) = '{$month}') AS buenas,
            (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor = 0 AND idLabor = '{$idLabor}' AND MONTH(fecha) = '{$month}')  AS malas,
            (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                    AND MONTH(fecha) = '{$month}' AND
                    idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}') AS muestras
            FROM muestras 
            WHERE 
                AND MONTH(fecha) = '{$month}' AND
                idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}'
            GROUP BY idLaborCausa) as t
            ORDER BY valor DESC , causa ASC";
                // D($sql);
        $response->data = $this->db->queryRow($sql);

        return $response->data;
    }
    /*----------  PANTALLA 3 : PROMEDIO POR MES GENERAL : GRAFICA DE BARRAS  ----------*/

    /*----------  PANTALLA 3 : PROMEDIO POR MES POR LABOR : GRAFICA DE BARRAS  ----------*/
    private function getPromMonth($month , $lote , $labor , $idfinca){
        $lote = $lote;
        $idfinca = $idfinca;
        $idLabor = $labor;
        $response = new stdClass;
        $response->data = 0;
        $sql = "SELECT IFNULL(promedioLabor, 0) AS promedioLabor , IFNULL(labor , (SELECT nombre FROM labores WHERE id = '{$idLabor}')) AS labor
                FROM
                (SELECT valor , buenas , muestras ,
                CASE
                    WHEN AVG(((buenas / muestras) * 100)) > 0 THEN  AVG(((buenas / muestras) * 100))
                    ELSE NULL
                END AS promedioLabor,
                CASE 
                    WHEN labor = '' THEN NULL
                    WHEN labor = NULL THEN NULL
                    ELSE labor
                END AS labor
                FROM 
                (SELECT labor ,causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras 
                    FROM muestras 
                WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}' AND valor > 0 AND MONTH(fecha) = {$month}) AS buenas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas 
                    FROM muestras 
                WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}' AND MONTH(fecha) = {$month}) AS muestras
                FROM muestras 
                WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$idLabor}' AND MONTH(fecha) = {$month}) as t) AS LaboresPromedio";
                // D($sql);
        $response->data = $this->db->queryRow($sql);

        return $response->data;
    }
    /*----------  PANTALLA 3 : PROMEDIO POR MES POR LABOR : GRAFICA DE BARRAS  ----------*/
    
    /*=====  FIN DE LA PANTALLA NUMERO 3  ======*/
    
    /*=========================================
    =            PANTALLA NUMERO 2            =
    =========================================*/
    public function getLoteLabor(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $idLote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);


        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }

        $response = new stdClass;
        $sql = "SELECT idZona , zona , idFinca , finca, idLote, lote , idLabor , labor ,  idResponsableLabor , 
                UPPER(ResponsableLabor) AS ResponsableLabor ,
                idTipoLabor , 
                IF(idTipoLabor = 0 , 'LABORES',tipo_labor) AS tipo_labor
                FROM muestras 
                WHERE fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND idLote = '{$idLote}' AND idFinca = '{$idfinca}'
                GROUP BY idLote ,idLabor ORDER BY lote , labor";
        // D($sql);
        $LaboresExist = $this->db->queryAll($sql);
        $lote = "";
        $promedio = 0;
        foreach ($LaboresExist as $key => $value) {
            $value = (object)$value;
            $promedio = $this->getLaborCausa($value->idLabor , $value->idLote , $fecha_inicio , $fecha_fin ,$value->idFinca  )->porcentaje;
            if($promedio > -1){
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["idZona"] = $value->idZona;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["idFinca"] = $value->idFinca;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["idLote"] = $value->idLote;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["idLabor"] = $value->idLabor;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["zona"] = $value->zona;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["finca"] = $value->finca;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["lote"] = $value->lote;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["labor"] = $value->labor;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["operador"] = $value->ResponsableLabor;
                $response->causas["lote"]["labores"][$value->tipo_labor][$value->labor]["promedio"] =  ($promedio > 0) ? $promedio : 0;
                $value->ResponsableLabor = trim($value->ResponsableLabor);
                $response->causas["lote"]["operador"][$value->ResponsableLabor]["idResponsableLabor"] = $value->idResponsableLabor;
                $response->causas["lote"]["operador"][$value->ResponsableLabor]["ResponsableLabor"] = $value->ResponsableLabor;
                $response->causas["lote"]["operador"][$value->ResponsableLabor]["promedio"] +=  ($promedio > 0) ? $promedio : 0;
                $response->causas["lote"]["operador"][$value->ResponsableLabor]["avg"]++;
            }
        }
        $response->bandera = 311;

        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY semana";
       
        $sql_fincas_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from lotes where id = idLote) AS finca,idFinca , idZona , idLote FROM muestras_anual
         WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}'
        GROUP BY mes , idLote";
        
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from lotes where id = idLote) AS finca,idFinca , idZona , idLote  FROM muestras_anual
        WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}'
            GROUP BY idLote, semana";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas = new stdClass;
        $response->zonas->fincas = [0 => $this->session->alias] + $this->db->queryAllSpecial("SELECT (SELECT nombre from lotes where id = idLote) AS id  , (SELECT nombre from lotes where id = idLote) AS label FROM muestras_anual 
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}'
            GROUP BY idLote");
        $months_fincas = $this->db->queryAll($sql_fincas_month);
        $weeks_fincas = $this->db->queryAll($sql_zones_week);
        $response->zonas->auditoria = $this->db->queryAll($sql_fincas_month);
        $response->auditoria = [];

        foreach ($months_fincas as $key => $value) {
            $response->zonas->months[$value->finca][$value->id] = $this->getPromFincaMonth($value->idZona , $value->mes , $value->idFinca)->promedio;
        }

        foreach ($weeks_fincas as $key => $value) {
            $response->zonas->weeks[$value->finca][$value->label] = $this->getPromFincaWeek($value->idZona , $value->label , $value->idFinca)->promedio;
        }

        if($this->session->id_company == 5){
            $response->zonas->fincas += $this->db->queryAllSpecial("SELECT (SELECT nombre from labores where id = idLabor) AS id  , (SELECT nombre from labores where id = idLabor) AS label FROM muestras_anual 
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}'
            GROUP BY idLabor");
            /*----------  LABORES  ----------*/
            $response->zonas->labores = new stdClass;
            // $response->zonas->labores->months = $months_des;
            // $response->zonas->labores->weeks = $weeks;

            $sql_labores_months = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
            AS id,mes,(SELECT nombre from labores where id = idLabor) AS finca,idFinca , idZona , idLote , idLabor FROM muestras_anual
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}'
            GROUP BY mes , idLabor";

            $sql_labores_weeks = "SELECT semana AS id,semana,(SELECT nombre from labores where id = idLabor) AS finca,idFinca , idZona , idLote , idLabor FROM muestras_anual
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}' AND idLote = '{$idLote}'
            GROUP BY semana , idLabor";

            $months_labores = $this->db->queryAll($sql_labores_months);
            $weeks_labores = $this->db->queryAll($sql_labores_weeks);

            foreach ($months_labores as $key => $value) {
                $response->zonas->labores->months[$value->finca][$value->id] = $this->getPromLaborMonth($value->idZona , $value->mes , $value->idFinca ,$value->idLote , $value->idLabor)->promedio;
            }

            foreach ($weeks_labores as $key => $value) {
                $response->zonas->labores->weeks[$value->finca][$value->id] = $this->getPromLaborWeek($value->idZona , $value->id , $value->idFinca , $value->idLote , $value->idLabor)->promedio;
            }
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;
        return $response;
    }

    private function getPromLaborMonth($idZona , $month , $idFinca , $idLote , $idLabor){
        $response->promedio = 0;
        $sql = "SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio,
        idZona , idFinca , idlote , idLabor
        FROM muestras_anual
         WHERE idZona = '{$idZona}' AND mes = '{$month}' AND idFinca = '{$idFinca}' AND idLote = '{$idLote}' AND idLabor = '{$idLabor}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromLaborWeek($idZona , $week , $idFinca , $idLote , $idLabor){
        $response->promedio = 0;
        $sql = "SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio,
        idZona , idFinca , idlote , idLabor
        FROM muestras_anual
         WHERE idZona = '{$idZona}' AND semana = '{$week}' AND idFinca = '{$idFinca}' AND idLote = '{$idLote}' AND idLabor = '{$idLabor}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }

    private function getPromMonthLoteAvg($month , $lote , $labor , $idfinca){
        $lote = $lote;
        $idfinca = $idfinca;
        $idLabor = $labor;
        $response = new stdClass;
        $response->data = 0;
        $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
                CASE
                    WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                    ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
                END AS porcentaje,
                CASE
                    WHEN valor > 0 THEN  NULL
                    ELSE (CausasIndividual / malas) * 100
                END AS porcentaje2
                FROM 
                (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor > 0 AND idLabor = '{$labor}' AND MONTH(fecha) = '{$month}') AS buenas,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor = 0 AND idLabor = '{$labor}' AND MONTH(fecha) = '{$month}')  AS malas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                        MONTH(fecha) = '{$month}' AND
                        idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$labor}') AS muestras
                FROM muestras 
                WHERE 
                    MONTH(fecha) = '{$month}' AND
                    idFinca = '{$idfinca}' AND idLote ='{$lote}' AND idLabor = '{$labor}'
                GROUP BY idLaborCausa) as t
                WHERE valor > 0
                ORDER BY valor DESC , causa ASC";
                // D($sql);
        $response->data = $this->db->queryRow($sql);

        return $response->data;
    }

    private function getLaborCausa($idLabor , $idLote , $fecha_inicio , $fecha_fin , $idFinca){
        $response = new stdClass;
        $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
                CASE
                    WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                    ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
                END AS porcentaje,
                CASE
                    WHEN valor > 0 THEN  NULL
                    ELSE (CausasIndividual / malas) * 100
                END AS porcentaje2
                FROM 
                (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor > 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                        fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                        idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}') AS muestras
                FROM muestras 
                WHERE 
                    fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'
                GROUP BY idLaborCausa) as t
                WHERE valor > 0
                ORDER BY valor DESC , causa ASC";
        // D($sql);
                // WHERE valor > 0
        $response->data = $this->db->queryRow($sql);
        if(!isset($response->data->porcentaje)){
            $response->data->porcentaje = -1;
        }
        return $response->data;
    }

    /*----------  PANTALLA 2 : PROMEDIO POR MES GENERAL : GRAFICA DE BARRAS  ----------*/
    private function getPromMonthGeneralLote($month , $lote , $labor , $idfinca){
        $lote = $lote;
        $idfinca = $idfinca;
        $idLabor = $labor;
        $response = new stdClass;
        $response->data = 0;
        $sql = "SELECT
                CASE
                    WHEN valor > 0 THEN  (((buenas / muestras) * 100))
                    ELSE 0
                END AS promedioLote , 'lotes'
                FROM 
                (SELECT labor ,causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras 
                    FROM muestras 
                WHERE idFinca = '{$idfinca}' AND valor > 0 AND MONTH(fecha) = {$month}) AS buenas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas 
                    FROM muestras 
                WHERE idFinca = '{$idfinca}' AND MONTH(fecha) = {$month}) AS muestras
                FROM muestras 
                WHERE idFinca = '{$idfinca}' AND MONTH(fecha) = {$month}) as e";
                // D($sql);
        $response->data = $this->db->queryRow($sql);

        return $response->data;
    }
    /*----------  PANTALLA 2 : PROMEDIO POR MES GENERAL : GRAFICA DE BARRAS  ----------*/

    /*----------  PANTALLA 2 : PROMEDIO POR MES POR LABOR : GRAFICA DE BARRAS  ----------*/
    private function getPromMonthLote($month , $lote , $labor , $idfinca){
        $lote = $lote;
        $idfinca = $idfinca;
        $idLabor = $labor;
        $response = new stdClass;
        $response->data = 0;
        $sql = "SELECT IFNULL(promedioLote, 0) AS promedioLote , IFNULL(lote , (SELECT nombre FROM lotes WHERE id = '{$idLabor}')) AS lote
                FROM
                (SELECT valor , buenas , muestras ,
                CASE
                    WHEN valor > 0 THEN  AVG(((buenas / muestras) * 100))
                    ELSE NULL
                END AS promedioLote,
                CASE 
                    WHEN lote = '' THEN NULL
                    WHEN lote = NULL THEN NULL
                    ELSE lote
                END AS lote
                FROM 
                (SELECT lote ,causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras 
                    FROM muestras 
                WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND valor > 0 AND MONTH(fecha) = {$month}) AS buenas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas 
                    FROM muestras 
                WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND MONTH(fecha) = {$month}) AS muestras
                FROM muestras 
                WHERE idFinca = '{$idfinca}' AND idLote ='{$lote}' AND MONTH(fecha) = {$month}) as t) AS LaboresPromedio";
                // D($sql);
        $response->data = $this->db->queryRow($sql);

        return $response->data;
    }
    /*=====  FIN DE LA PANTALLA NUMERO 2  ======*/

    /*=========================================
    =            PANTALLA NUMERO 1            =
    =========================================*/
    
    public function getFincaLote(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $lote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }


        $sql = "SELECT nombre ,'' AS value FROM labores";
        $laboresFilter = $this->db->queryAll($sql);
        $labores = [];
        $labor_ = ["nombre" => "" , "labores" => []];
        foreach ($laboresFilter as $key => $value) {
            $value = (object)$value;
            $labor_[$value->nombre] = -1;
            $labores[$value->nombre] = ["filter" => $value->nombre , "value" => ""] ;
        }

        $sql_columns = "";

        $sql = "SELECT 
            MONTH(fecha) AS Mes,
            idZona, 
            zona, 
            idFinca, 
            finca, 
            idLote, 
            lote , 
            idLabor , 
            labor,
            idTipoLabor , 
            IF(idTipoLabor = 0 , 'LABORES',tipo_labor) AS tipo_labor,
            idResponsableLabor , UPPER(ResponsableLabor) AS ResponsableLabor,
            '-1' AS MesActual 
            FROM muestras 
            WHERE YEAR(fecha) = YEAR(CURRENT_DATE)  
            AND fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND idFinca = '{$idfinca}'
            GROUP BY idLote ,idLabor 
            ORDER BY MONTH(fecha) , lote , labor";
            // IF(MONTH(CURRENT_DATE) = MONTH(fecha) , -1 , MONTH(fecha)) AS MesActual 
            // D($sql);
        $LostesExist = $this->db->queryAll($sql);
        $response = new stdClass; 
        $response->labores_mes = $months;
        $response->causas = [];
        $response->data_labores = [];
        $response->data_labores_lotes = [];
        $response->filterLabores = $labores;
        $response->labores = [];
        $lote = "";
        $labor = "";
        $tipo_labor = "";
        $promedio = 0;

        foreach ($LostesExist as $key => $value) {
            $value = (object)$value;
            $promedio = $this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$value->idFinca )->porcentaje;
            if($promedio > -1){
                $response->causas["lote"][$value->lote]["idZona"] = (int)$value->idZona;
                $response->causas["lote"][$value->lote]["zona"] = $value->zona;
                $response->causas["lote"][$value->lote]["idFinca"] = (int)$value->idFinca;
                $response->causas["lote"][$value->lote]["finca"] = $value->finca;
                $response->causas["lote"][$value->lote]["idLote"] = (int)$value->idLote;
                $response->causas["lote"][$value->lote]["lote"] = (int)$value->lote;
                $response->causas["lote"][$value->lote]["idLabor"] = (int)$value->idLabor;
                $response->causas["lote"][$value->lote]["labores"][$value->labor]["idLote"] = $value->idLabor;
                $response->causas["lote"][$value->lote]["labores"][$value->labor]["idLabor"] = $value->idLabor;
                $response->causas["lote"][$value->lote]["labores"][$value->labor]["promedio"] =  ($promedio > 0) ? $promedio : 0;
                // if($lote != $value->idLote){
                //     $lote = $value->idLote;
                //     $response->data_labores_lotes[$value->lote] = [
                //         "lote" => $value->lote ,
                //         "idLote" => $value->idLote ,
                //         "idfinca" => $idfinca ,
                //         "promedio" => $this->getAvgLote($value->idLote , $fecha_inicio ,$fecha_fin ,$idfinca )->porcentaje
                //     ];
                // }
                // $response->data_labores_lotes[$value->lote][$value->labor] = $value->labor;
                // $response->data_labores_lotes[$value->lote]["labores"][$value->labor] = $promedio;

                if(!in_array($value->lote, $response->data_labores_lotes[$value->lote])){
                    $response->data_labores_lotes[$value->lote] = $labor_;
                    $response->data_labores_lotes[$value->lote]["labores"] = $labores;
                }
                $response->data_labores_lotes[$value->lote]["nombre"] = $value->lote;
                $response->data_labores_lotes[$value->lote][$value->labor] = (float)$promedio;
                $response->data_labores_lotes[$value->lote]["labores"][$value->labor]["filter"] = $value->labor;
                $response->data_labores_lotes[$value->lote]["labores"][$value->labor]["value"] = (float)$promedio;

                if($labor != $value->idLabor){
                    $labor = $value->idLabor;
                    $response->labores[] = $value->labor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["avg"]++;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["labor"] = $value->labor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idLabor"] = $value->idLabor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idTipoLabor"] = $value->idTipoLabor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["tipo_labor"] = $value->tipo_labor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idfinca"] = $value->idFinca;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idZona"] = $value->idZona;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idLote"] = $value->idLote;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["promedio"] += $promedio;
                    $value->ResponsableLabor = trim($value->ResponsableLabor);
                    $response->operador[$value->ResponsableLabor]["idResponsableLabor"] = $value->idResponsableLabor;
                    $response->operador[$value->ResponsableLabor]["ResponsableLabor"] = $value->ResponsableLabor;
                    $response->operador[$value->ResponsableLabor]["promedio"] +=  $promedio;
                    $response->operador[$value->ResponsableLabor]["avg"]++;
                }
            }
        }
        $response->bandera = 748;

        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY semana";
        $sql_fincas_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from fincas where id = idFinca) AS finca,idFinca , idZona FROM muestras_anual
         WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}'
        GROUP BY mes , idFinca";
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from fincas where id = idFinca) AS finca,idFinca , idZona  FROM muestras_anual
        WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}'
            GROUP BY idFinca, semana";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas = new stdClass;
        $response->zonas->fincas = [0 => $this->session->alias] + $this->db->queryAllSpecial("SELECT (SELECT nombre from fincas where id = idFinca) AS id  , (SELECT nombre from fincas where id = idFinca) AS label FROM muestras_anual 
            WHERE idZona = '{$idZona}' AND idFinca = '{$idfinca}'
            GROUP BY idFinca");
        $months_fincas = $this->db->queryAll($sql_fincas_month);
        $weeks_fincas = $this->db->queryAll($sql_zones_week);
        $response->zonas->auditoria = $this->db->queryAll($sql_fincas_month);
        $response->auditoria = [];

        foreach ($months_fincas as $key => $value) {
            $response->zonas->months[$value->finca][$value->id] = $this->getPromFincaMonth($value->idZona , $value->mes , $value->idFinca)->promedio;
        }

        foreach ($weeks_fincas as $key => $value) {
            $response->zonas->weeks[$value->finca][$value->label] = $this->getPromFincaWeek($value->idZona , $value->label , $value->idFinca)->promedio;
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        // D($this->session);
        return $response;
    }

    private function getPromLoteMonth($idZona , $month , $idFinca , $idLote){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND mes = '{$month}' AND idFinca = '{$idFinca}' AND idLote = '{$idLote}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromLoteWeek($idZona , $week , $idFinca , $idLote){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND semana = '{$week}' AND idFinca = '{$idFinca}' AND idLote = '{$idLote}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }

    /*=========================================
    =            PANTALLA FINCAS            =
    =========================================*/
    
    public function getZonaFinca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $lote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }


        $sql = "SELECT nombre ,'' AS value FROM labores";
        $laboresFilter = $this->db->queryAll($sql);
        $labores = [];
        $labor_ = ["nombre" => "" , "labores" => []];
        foreach ($laboresFilter as $key => $value) {
            $value = (object)$value;
            $labor_[$value->nombre] = 0;
            $labores[$value->nombre] = ["filter" => $value->nombre , "value" => ""] ;
        }

        $sql_columns = "";

        $sql = "SELECT 
                MONTH(fecha) AS Mes,
                idFinca,
                idZona,
                zona,
                finca,
                idLote, 
                lote , 
                idLabor , 
                labor,
                idTipoLabor , 
                IF(idTipoLabor = 0 , 'LABORES',tipo_labor) AS tipo_labor,
                idResponsableLabor , UPPER(ResponsableLabor) AS ResponsableLabor,
                '-1' AS MesActual
            FROM muestras 
            WHERE YEAR(fecha) = YEAR(CURRENT_DATE)  
            AND fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND idZona = '{$idZona}'
            GROUP BY idLote ,idLabor 
            ORDER BY MONTH(fecha) ,lote , labor";
            // IF(MONTH(CURRENT_DATE) = MONTH(fecha) , -1 , MONTH(fecha)) AS MesActual 
            // D($sql);
        $LostesExist = $this->db->queryAll($sql);
        $response = new stdClass; 
        $response->labores_mes = $months;
        $response->causas = [];
        $response->data_labores = [];
        $response->filterLabores = $labores;
        // $response->data_labores_lotes = [];
        $response->labores = [];
        $lote = "";
        $labor = "";
        $tipo_labor = "";
        $promedio = 0;

        foreach ($LostesExist as $key => $value) {
            $value = (object)$value;
            $promedio = $this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$value->idFinca )->porcentaje;
            if($promedio > -1){
                $response->causas["fincas"][$value->finca]["idZona"] = $value->idZona;
                $response->causas["fincas"][$value->finca]["zona"] = $value->zona;
                $response->causas["fincas"][$value->finca]["idFinca"] = $value->idFinca;
                $response->causas["fincas"][$value->finca]["nombre"] = $value->finca;
                $response->causas["fincas"][$value->finca]["lote"][$value->lote]["lote"] = (int)$value->lote;
                $response->causas["fincas"][$value->finca]["lote"][$value->lote]["idLabor"] = (int)$value->idLabor;
                $response->causas["fincas"][$value->finca]["lote"][$value->lote]["idLote"] = $value->idLote;
                $response->causas["fincas"][$value->finca]["lote"][$value->lote]["idFinca"] = $value->idFinca;
                $response->causas["fincas"][$value->finca]["lote"][$value->lote]["nombre"] = $value->finca;
                $response->causas["fincas"][$value->finca]["lote"][$value->lote]["labores"][$value->labor]["idLote"] = $value->idLabor;
                $response->causas["fincas"][$value->finca]["lote"][$value->lote]["labores"][$value->labor]["idLabor"] = $value->idLabor;
                $response->causas["fincas"][$value->finca]["lote"][$value->lote]["labores"][$value->labor]["promedio"] =  ($promedio > 0) ? $promedio : 0;
                // if($lote != $value->idLote){
                //     $lote = $value->idLote;
                //     $response->data_labores_lotes[$value->lote] = [
                //         "lote" => $value->lote ,
                //         "idLote" => $value->idLote ,
                //         "idfinca" => $idfinca ,
                //         "promedio" => $this->getAvgLote($value->idLote , $fecha_inicio ,$fecha_fin ,$value->idFinca )->porcentaje
                //     ];
                // }
                // $response->data_labores_lotes[$value->lote][$value->labor] = $value->labor;
                // $response->data_labores_lotes[$value->lote]["labores"][$value->labor] = $this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$value->idFinca )->porcentaje;

                if(!in_array($value->finca, $response->data_labores_fincas["fincas"][$value->finca])){
                    $response->data_labores_fincas["fincas"][$value->finca] = $labor_;
                    $response->data_labores_fincas["fincas"][$value->finca]["labores"] = $labores;
                }
                $response->data_labores_fincas["fincas"][$value->finca]["nombre"] = $value->finca;
                $response->data_labores_fincas["fincas"][$value->finca][$value->labor] = (float)$this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$value->idFinca )->porcentaje;
                $response->data_labores_fincas["fincas"][$value->finca]["labores"][$value->labor]["filter"] = $value->labor;
                $response->data_labores_fincas["fincas"][$value->finca]["labores"][$value->labor]["value"] = (float)$this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$value->idFinca )->porcentaje;

                if($labor != $value->idLabor){
                    $labor = $value->idLabor;
                    if(!in_array($value->labor, $response->labores)){
                        $response->labores[] = $value->labor;
                    }
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["avg"]++;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["labor"] = $value->labor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idLabor"] = $value->idLabor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idTipoLabor"] = $value->idTipoLabor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["tipo_labor"] = $value->tipo_labor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idfinca"] = $value->idFinca;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["promedio"] += $this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$value->idFinca )->porcentaje;
                    $value->ResponsableLabor = trim($value->ResponsableLabor);
                    $response->operador[$value->ResponsableLabor]["idResponsableLabor"] = $value->idResponsableLabor;
                    $response->operador[$value->ResponsableLabor]["ResponsableLabor"] = $value->ResponsableLabor;
                    $response->operador[$value->ResponsableLabor]["promedio"] +=  $this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$idfinca )->porcentaje;
                    $response->operador[$value->ResponsableLabor]["avg"]++;
                }
            }
        }
        $response->bandera = 1014;

        // $response->labores = (array)array_unique($response->labores);

        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label 
         FROM muestras_anual GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY semana";
        $sql_fincas_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from fincas where id = idFinca) AS finca,idFinca , idZona 
        FROM muestras_anual
        WHERE idZona = '{$idZona}'
        GROUP BY mes , idFinca";
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from fincas where id = idFinca) AS finca,idFinca , idZona  FROM muestras_anual
            WHERE idZona = '{$idZona}'
            GROUP BY idFinca, semana";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas = new stdClass;
        $response->zonas->fincas = [0 => $this->session->alias] + $this->db->queryAllSpecial("SELECT (SELECT nombre from fincas where id = idFinca) AS id  , (SELECT nombre from fincas where id = idFinca) AS label FROM muestras_anual 
            WHERE idZona = '{$idZona}'
            GROUP BY idFinca");
        $months_fincas = $this->db->queryAll($sql_fincas_month);
        $weeks_fincas = $this->db->queryAll($sql_zones_week);
        $response->zonas->auditoria = $this->db->queryAll($sql_fincas_month);
        $response->auditoria = [];

        foreach ($months_fincas as $key => $value) {
            $response->zonas->months[$value->finca][$value->id] = $this->getPromFincaMonth($value->idZona , $value->mes , $value->idFinca)->promedio;
        }

        foreach ($weeks_fincas as $key => $value) {
            $response->zonas->weeks[$value->finca][$value->label] = $this->getPromFincaWeek($value->idZona , $value->label , $value->idFinca)->promedio;
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        // D($this->session);
        return $response;
    }

    private function getPromFincaMonth($idZona , $month , $idFinca){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND mes = '{$month}' AND idFinca = '{$idFinca}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromFincaWeek($idZona , $week , $idFinca){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
        (SELECT       
                    CASE 
                        WHEN mes = 0 THEN 'Ene'
                        WHEN mes = 1 THEN 'Feb'
                        WHEN mes = 2 THEN 'Mar'
                        WHEN mes = 3 THEN 'Abr'
                        WHEN mes = 4 THEN 'May'
                        WHEN mes = 5 THEN 'Jun'
                        WHEN mes = 6 THEN 'Jul'
                        WHEN mes = 7 THEN 'Ago'
                        WHEN mes = 8 THEN 'Sep'
                        WHEN mes = 9 THEN 'Oct'
                        WHEN mes = 10 THEN 'Nov'
                        WHEN mes = 11 THEN 'Dic'
                    END
                    AS mes,
                    (SELECT nombre from zonas where id = idZona) AS zona,
        (SELECT nombre from fincas where id = idFinca) AS finca,
        (SELECT nombre from lotes where id = idlote) AS lote,
        (SELECT nombre from labores where id = idLabor) AS labor,
        AVG(promedio) AS promedio FROM muestras_anual
         WHERE idZona = '{$idZona}' AND semana = '{$week}' AND idFinca = '{$idFinca}'
        GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
        GROUP BY finca , lote) AS finca
        GROUP BY finca) AS zona
        GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }

    private function getAvgLote($idLabor , $idLote , $fecha_inicio , $fecha_fin , $idFinca){
        $response = new stdClass;
        $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
                CASE
                    WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                    ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
                END AS porcentaje,
                CASE
                    WHEN valor > 0 THEN  NULL
                    ELSE (CausasIndividual / malas) * 100
                END AS porcentaje2
                FROM 
                (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor > 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                        fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                        idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}') AS muestras
                FROM muestras 
                WHERE 
                    fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'
                GROUP BY idLaborCausa) as t
                ORDER BY valor DESC , causa ASC";
        // D($sql);
        $response->data = $this->db->queryRow($sql);

        return $response->data;
    }

    private function getAvgLoteLabor($idLabor , $idLote , $fecha_inicio , $fecha_fin , $idFinca){
        $response = new stdClass;
        if($this->session->id_company == 7){
            switch($idLabor){
                    case 1:
                        $labor = 'Enfunde';
                        break;
                    case 2:
                        $labor = 'Protección';
                        break;
                    case 3:
                        $labor = 'Desvío Hijos';
                        break;
                    case 4:
                        $labor = 'Puntal';
                        break;
                    case 5:
                        $labor = 'Deshoje';
                        break;
                    case 6:
                        $labor = 'Selección';
                        break;
                    case 7:
                        $labor = 'Limpieza';
                        break;
                    case 8:
                        $labor = 'Malezas';
                        break;
                    case 9:
                        $labor = 'Drenaje';
                        break;
                    case 10:
                        $labor = 'Fertilización';
                        break;
                }
                $sql = "
                    SELECT *,
                    CASE
                        WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                        ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
                    END AS porcentaje,
                    CASE
                        WHEN valor > 0 THEN  NULL
                        ELSE (CausasIndividual / malas) * 100
                    END AS porcentaje2
                    FROM
                    (SELECT causa, COUNT(causa) as CausasIndividual, valor,
                            (SELECT COUNT(*) AS total_muestras FROM muestras_plantas INNER JOIN muestras_main ON muestras_main.id = id_muestra WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor > 0 AND TYPE = '{$labor}' AND muestras_main.fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
                            (SELECT COUNT(*) AS total_muestras FROM muestras_plantas INNER JOIN muestras_main ON muestras_main.id = id_muestra WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 0 AND TYPE = '{$labor}' AND muestras_main.fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS malas,
                            (SELECT COUNT(DISTINCT archivoJson) AS total_causas FROM muestras_plantas WHERE 
                                fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                                idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}') AS muestras
                    FROM muestras_plantas
                    INNER JOIN muestras_main ON muestras_main.id = id_muestra
                    WHERE idFinca = '{$idfinca}' AND idLote ='{$idLote}' AND TYPE = '{$labor}' AND muestras_main.fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}'
                    GROUP BY TYPE, planta, archivoJson) as tbl
                    ORDER BY valor DESC , causa ASC";
        }else{
            $sql = "SELECT causa , CausasIndividual , buenas , malas ,muestras,valor,
                CASE
                    WHEN valor > 0 THEN  ((buenas / muestras) * 100) 
                    ELSE ((((1-(buenas / muestras))*CausasIndividual) / malas)*100)
                END AS porcentaje,
                CASE
                    WHEN valor > 0 THEN  NULL
                    ELSE (CausasIndividual / malas) * 100
                END AS porcentaje2
                FROM 
                (SELECT laborCausaDesc AS causa ,COUNT( causa) AS CausasIndividual,valor,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor > 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}') AS buenas,
                (SELECT COUNT(*) AS total_muestras FROM muestras WHERE idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND valor = 0 AND idLabor = '{$idLabor}' AND fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}')  AS malas,
                (SELECT COUNT(distinct archivoJson_contador , archivoJson) AS total_causas FROM muestras WHERE 
                        fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                        idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}') AS muestras
                FROM muestras 
                WHERE 
                    fecha BETWEEN '{$fecha_inicio}' AND '{$fecha_fin}' AND
                    idFinca = '{$idFinca}' AND idLote ='{$idLote}' AND idLabor = '{$idLabor}'
                GROUP BY idLaborCausa) as t
                WHERE valor > 0
                ORDER BY valor DESC , causa ASC";
        }
        // D($sql);
        $response->data = $this->db->queryRow($sql);
        if(!isset($response->data->porcentaje)){
            $response->data->porcentaje = -1;
        }
        return $response->data;
    }           
    
    /*=====  FIN DE LA PANTALLA NUMERO 1  ======*/
    
    /*===============================================
    =            SECCION NIVEL HACIENDA [ ZONAS ]            =
    ===============================================*/
    
    public function getHaciendaZonas(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $idZona = getValueFrom($postdata,'idZona','',FILTER_SANITIZE_PHRAPI_INT);
        $idfinca = getValueFrom($postdata,'idFinca','',FILTER_SANITIZE_PHRAPI_INT);
        $lote = getValueFrom($postdata,'idLote','',FILTER_SANITIZE_PHRAPI_INT);
        $idLabor = getValueFrom($postdata,'idLabor','',FILTER_SANITIZE_PHRAPI_INT);
        $fecha_fin = getValueFrom($postdata,'fecha_final','',FILTER_SANITIZE_STRING);
        $fecha_inicio = getValueFrom($postdata,'fecha_inicial','',FILTER_SANITIZE_STRING);


        $response = new stdClass; 
        
        // $response = $this->Service->post("zonas/",$postdata);
        // D($response);

        // $months = ["Ene" , "Feb" , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep", "Oct" , "Nov", "Dic"];
        $sql = "SELECT nombre ,'' AS value FROM labores";
        $laboresFilter = $this->db->queryAll($sql);
        $labores = [];
        $labor_ = ["nombre" => "" , "labores" => []];
        foreach ($laboresFilter as $key => $value) {
            $value = (object)$value;
            $labor_[$value->nombre] = 0;
            $labores[$value->nombre] = ["filter" => $value->nombre , "value" => ""] ;
        }
        $sql_columns = "";

        $sql = "SELECT 
            MONTH(fecha) AS Mes,
            idLote, 
            idFinca,
            finca, 
            idZona, 
            zona,
            lote , 
            idLabor , 
            labor,
            idTipoLabor , 
            IF(idTipoLabor = 0 , 'LABORES',tipo_labor) AS tipo_labor,
            idResponsableLabor , UPPER(ResponsableLabor) AS ResponsableLabor,
            '-1' AS MesActual 
            FROM muestras 
            WHERE YEAR(fecha) = YEAR(CURRENT_DATE)  
            AND fecha BETWEEN '$fecha_inicio' AND '$fecha_fin' AND idLote > 0
            GROUP BY idLote ,idLabor 
            ORDER BY MONTH(fecha) , lote , labor";
            // IF(MONTH(CURRENT_DATE) = MONTH(fecha) , -1 , MONTH(fecha)) AS MesActual 
            // D($sql);
        $LostesExist = $this->db->queryAll($sql);
        $response->labores_mes = $months;
        $response->causas = [];
        $response->data_labores = [];
        $response->data_labores_zonas = [];
        $response->labores = [];
        $response->filterLabores = $labores;
        $lote = "";
        $labor = "";
        $tipo_labor = "";
        $promedio = 0;

        foreach ($LostesExist as $key => $value) {
            $value = (object)$value;
            // $promedio = $this->getPromMonthLoteAvg($value->Mes,$value->idLote , $value->idLabor  , $value->idFinca)->porcentaje;
            $promedio = $this->getAvgLoteLabor($value->idLabor ,$value->idLote , $fecha_inicio ,$fecha_fin ,$value->idFinca )->porcentaje;
            if($promedio > -1){
                $response->causas["zonas"][$value->zona]["nombre"] = $value->zona;
                $response->causas["zonas"][$value->zona]["idFinca"] = $value->idFinca;
                $response->causas["zonas"][$value->zona]["idZona"] = $value->idZona;
                $response->causas["zonas"][$value->zona]["fincas"][$value->finca][$value->lote]["idLote"] = $value->idLote;
                $response->causas["zonas"][$value->zona]["fincas"][$value->finca][$value->lote]["labores"][$value->labor]["idLote"] = $value->idLote;
                $response->causas["zonas"][$value->zona]["fincas"][$value->finca][$value->lote]["labores"][$value->labor]["idLabor"] = $value->idLabor;
                $response->causas["zonas"][$value->zona]["fincas"][$value->finca][$value->lote]["labores"][$value->labor]["promedio"] =  ($promedio > 0) ? $promedio : 0;
                // if($lote != $value->idLote){
                //     $lote = $value->idLote;
                //     $response->data_labores_lotes["zonas"][$value->zona] = [
                //         "lote" => $value->lote ,
                //         "idLote" => $value->idLote ,
                //         "idfinca" => $value->idFinca ,
                //         "promedio" => $this->getAvgLote($value->idLote , $fecha_inicio ,$fecha_fin ,$value->idFinca )->porcentaje
                //     ];
                // }
                if(!in_array($value->zona, $response->data_labores_zonas["zonas"][$value->zona])){
                    $response->data_labores_zonas["zonas"][$value->zona] = $labor_;
                    $response->data_labores_zonas["zonas"][$value->zona]["labores"] = $labores;
                }
                $response->data_labores_zonas["zonas"][$value->zona]["nombre"] = $value->zona;
                $response->data_labores_zonas["zonas"][$value->zona][$value->labor] = (float)$promedio;
                $response->data_labores_zonas["zonas"][$value->zona]["labores"][$value->labor]["filter"] = $value->labor;
                $response->data_labores_zonas["zonas"][$value->zona]["labores"][$value->labor]["value"] = (float)$promedio;
                if($labor != $value->idLabor){
                    $labor = $value->idLabor;
                    if(!in_array($value->labor, $response->labores)){
                        $response->labores[] = $value->labor;
                    }
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["avg"]++;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["labor"] = $value->labor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idLabor"] = $value->idLabor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idTipoLabor"] = $value->idTipoLabor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["tipo_labor"] = $value->tipo_labor;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["idfinca"] = $value->idFinca;
                    $response->data_labores[$value->tipo_labor][$value->idLabor]["promedio"] += $promedio;
                    $value->ResponsableLabor = trim($value->ResponsableLabor);
                    $response->operador[$value->ResponsableLabor]["idResponsableLabor"] = $value->idResponsableLabor;
                    $response->operador[$value->ResponsableLabor]["ResponsableLabor"] = $value->ResponsableLabor;
                    $response->operador[$value->ResponsableLabor]["promedio"] +=  $promedio;
                    $response->operador[$value->ResponsableLabor]["avg"]++;
                }
            }

            $LostesExist = array_shift($LostesExist);
        }
        $response->bandera = 1390;

        $response->principal = [];
        $labor = ""; 
        $labores = "";
        foreach ($months as $key => $value) {
            $labor = $this->getPromMonthLote(($key+1),$lote , $idLabor  , $idfinca);
            $labores = $this->getPromMonthGeneralLote(($key+1) ,$lote , $idLabor , $idfinca );
            // D($labor);
            // D($labores);
            $response->principal[] = [ 
                $value , 
                strtoupper($labor->lote) => $labor->promedioLote,
                strtoupper($labores->lotes) => $labores->promedioLote,
            ];
        }

        $months_des = ["Ene" , "Feb"  , "Mar" , "Abr" , "May" , "Jun" , "Jul" ,"Ago", "Sep" , "Oct" , "Nov" , "Dic"];
        $months = ["Ene"  => [], "Feb"  => [] 
        , "Mar"  => [], "Abr"  => [], "May"  => [], "Jun"  => [], "Jul"  => [],"Ago" => [], "Sep"  => [], 
        "Oct"  => [], "Nov"  => [], "Dic" => []];

        $week = [];
        $weeks = [];
        for ($i=1; $i <= 52; $i++) { 
            $week[$i] = [];
            $weeks[] = $i;
        }


        $sql_hacienda_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY mes";
        $sql_hacienda_week = "SELECT semana AS id, AVG(promedio) AS label FROM muestras_anual GROUP BY semana";
        $sql_zones_month = "SELECT CASE 
                WHEN mes = 0 THEN 'Ene'
                WHEN mes = 1 THEN 'Feb'
                WHEN mes = 2 THEN 'Mar'
                WHEN mes = 3 THEN 'Abr'
                WHEN mes = 4 THEN 'May'
                WHEN mes = 5 THEN 'Jun'
                WHEN mes = 6 THEN 'Jul'
                WHEN mes = 7 THEN 'Ago'
                WHEN mes = 8 THEN 'Sep'
                WHEN mes = 9 THEN 'Oct'
                WHEN mes = 10 THEN 'Nov'
                WHEN mes = 11 THEN 'Dic'
            END
         AS id,mes,(SELECT nombre from zonas where id = idZona) AS zona,idZona FROM muestras_anual
            GROUP BY idZona , mes";
        $sql_zones_week = "SELECT semana AS label,(SELECT nombre from zonas where id = idZona) AS zona,idZona  FROM muestras_anual GROUP BY idZona , semana";


        $response->week = $this->db->queryAllSpecial($sql_hacienda_week);
        $response->main = $this->db->queryAllSpecial($sql_hacienda_month);
        $response->categories = new stdClass;
        $response->categories->months = $months_des;
        $response->categories->weeks = $weeks;
        $response->zonas = new stdClass;
        $response->zonas->zonas = [0 => $this->session->alias] + $this->db->queryAllSpecial("SELECT (SELECT nombre from zonas where id = idZona) AS id  , (SELECT nombre from zonas where id = idZona) AS label FROM muestras_anual GROUP BY idZona");
        $months_zones = $this->db->queryAll($sql_zones_month);
        $weeks_zones = $this->db->queryAll($sql_zones_week);
        $response->zonas->auditoria = $this->db->queryAll($sql_zones_month);
        $response->auditoria = [];

        foreach ($months_zones as $key => $value) {
            $response->zonas->months[$value->zona][$value->id] = $this->getPromZonaMonth($value->idZona , $value->mes)->promedio;
        }

        foreach ($weeks_zones as $key => $value) {
            $response->zonas->weeks[$value->zona][$value->label] = $this->getPromZonaWeek($value->idZona , $value->label)->promedio;
        }

        $response->id_company = $this->session->id_company;
        $response->umbrals = $this->session->umbrals;

        // D($this->session);
        return $response;
    }
    
    /*=====  End of SECCION NIVEL HACIENDA [ ZONAS ]  ======*/
    

    private function getPromZonaMonth($idZona , $month){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
                (SELECT       
                            CASE 
                                WHEN mes = 0 THEN 'Ene'
                                WHEN mes = 1 THEN 'Feb'
                                WHEN mes = 2 THEN 'Mar'
                                WHEN mes = 3 THEN 'Abr'
                                WHEN mes = 4 THEN 'May'
                                WHEN mes = 5 THEN 'Jun'
                                WHEN mes = 6 THEN 'Jul'
                                WHEN mes = 7 THEN 'Ago'
                                WHEN mes = 8 THEN 'Sep'
                                WHEN mes = 9 THEN 'Oct'
                                WHEN mes = 10 THEN 'Nov'
                                WHEN mes = 11 THEN 'Dic'
                            END
                            AS mes,
                            (SELECT nombre from zonas where id = idZona) AS zona,
                (SELECT nombre from fincas where id = idFinca) AS finca,
                (SELECT nombre from lotes where id = idlote) AS lote,
                (SELECT nombre from labores where id = idLabor) AS labor,
                AVG(promedio) AS promedio 
            FROM muestras_anual
            WHERE idZona = '{$idZona}' AND mes = '{$month}'
            GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
            GROUP BY finca , lote) AS finca
            GROUP BY finca) AS zona
            GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
    
    private function getPromZonaWeek($idZona , $week){
        $response->promedio = 0;
        $sql = "SELECT zona , AVG(promedio) AS promedio FROM(SELECT zona ,finca , AVG(prom) AS promedio FROM(SELECT zona ,finca , lote , AVG(promedio) AS prom FROM
                    (SELECT       
                                CASE 
                                    WHEN mes = 0 THEN 'Ene'
                                    WHEN mes = 1 THEN 'Feb'
                                    WHEN mes = 2 THEN 'Mar'
                                    WHEN mes = 3 THEN 'Abr'
                                    WHEN mes = 4 THEN 'May'
                                    WHEN mes = 5 THEN 'Jun'
                                    WHEN mes = 6 THEN 'Jul'
                                    WHEN mes = 7 THEN 'Ago'
                                    WHEN mes = 8 THEN 'Sep'
                                    WHEN mes = 9 THEN 'Oct'
                                    WHEN mes = 10 THEN 'Nov'
                                    WHEN mes = 11 THEN 'Dic'
                                END
                                AS mes,
                                (SELECT nombre from zonas where id = idZona) AS zona,
                    (SELECT nombre from fincas where id = idFinca) AS finca,
                    (SELECT nombre from lotes where id = idlote) AS lote,
                    (SELECT nombre from labores where id = idLabor) AS labor,
                    AVG(promedio) AS promedio 
                FROM muestras_anual
                WHERE idZona = '{$idZona}' AND semana = '{$week}'
                GROUP BY idZona , idFinca , idLote , idLabor , mes) AS lote
                GROUP BY finca , lote) AS finca
                GROUP BY finca) AS zona
                GROUP BY zona";
        $response->promedio = $this->db->queryRow($sql);

        return $response->promedio;
    }
}