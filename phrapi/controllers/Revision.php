<?php defined('PHRAPI') or die("Direct access not allowed!");

class Revision {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;

        return $response;
    }

    public function racimos(){
        $response = new stdClass;
        $response->data = [];
        $response->recordsTotal = 0;

        $sWhere = "";
        $sOrder = " ORDER BY fecha DESC, id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sHaving = "";

        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY bloque {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY finca {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY palanca {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY racimo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY tallo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY DATE(fecha) {$DesAsc}";
            }

            if(isset($_POST['id']) && trim($_POST['id']) != ""){
                $sWhere .= " AND id like '%".$_POST["id"]."%'";
            }   
            if(isset($_POST['bloque']) && trim($_POST['bloque']) != ""){
                $sWhere .= " AND bloque like '%".$_POST["bloque"]."%'";
            }   
            if(isset($_POST['finca']) && trim($_POST['finca']) != ""){
                $sWhere .= " AND finca LIKE '%".$_POST["finca"]."%'";
            }    
            if(isset($_POST['palanca']) && trim($_POST['palanca']) != ""){
                $sWhere .= " AND palanca LIKE '%".$_POST["palanca"]."%'";
            } 
            if(isset($_POST['racimo']) && trim($_POST['racimo']) != ""){
                $sWhere .= " AND racimo = '".$_POST["racimo"]."'";
            } 
            if(isset($_POST['tallo']) && trim($_POST['tallo']) != ""){
                $sWhere .= " AND tallo = '".$_POST["tallo"]."'";
            } 
            if(isset($_POST['fecha']) && trim($_POST['fecha']) != ""){
                $sWhere .= " AND DATE(fecha) = '".$_POST["fecha"]."'";
            } 

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT id, bloque, finca, palanca, racimo, tallo, DATE(fecha) as fecha, (SELECT COUNT(*) FROM merma WHERE 1=1 $sWhere) as totalRows 
                FROM merma 
                WHERE 1=1 $sWhere $sOrder $sLimit";
        $data = $this->db->queryAll($sql);

        if(isset($_POST["EnableEdit"]) && (int) $_POST["EnableEdit"] == 1){
            foreach($data as $key => $value){
                $response->data[] = [
                    $this->getInput("text", "id", $value->id, "readonly"),
                    $this->getInput("text", "id", $value->id, "readonly"),
                    $this->getInput("text", "bloque", $value->bloque),
                    $value->finca,
                    $this->getInput("text", "palanca", $value->palanca),
                    $this->getInput("number", "racimo", $value->racimo),
                    $this->getInput("number", "tallo", $value->tallo),
                    $this->getInput("text", "fecha", $value->fecha),
                    ""
                ];
                $response->recordsTotal = $value->totalRows;
            }
        }else{
            foreach($data as $key => $value){
                $response->data[] = [
                    $value->id,
                    $value->id,
                    $value->bloque,
                    $value->finca,
                    $value->palanca,
                    $value->racimo,
                    $value->tallo,
                    $value->fecha,
                    ""
                ];
                $response->recordsTotal = $value->totalRows;
            }
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionStatus = "OK";
        return $response;
    }

    public function saveRacimos(){
        $response = new stdClass;
        $data = getValueFrom($_POST , "data" , [], FILTER_SANITIZE_PHRAPI_ARRAY);

        foreach($data as $key => $value){
            $value = (object)$value;
            if(isset($value->id) && (int)$value->id > 0){
                $this->db->query("UPDATE merma SET 
                    bloque = '{$value->bloque}',
                    palanca = '{$value->palanca}',
                    racimo = '{$value->racimo}',
                    tallo = '{$value->tallo}',
                    fecha = '{$value->fecha}'
                    WHERE id = '{$value->id}'
                    ");
            }
        }

        $response->success = 200;
        return $response;
    }

    public function indexAsistencia(){
        $response = new stdClass;
        $response->data = [];
        $response->recordsTotal = 0;

        $sWhere = "";
        $sOrder = " ORDER BY asistencia.fecha DESC, asistencia.id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sHaving = "";

        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY asistencia.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY fecha {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY hora {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                if($this->session->id_company == 2){
                    $sOrder = " ORDER BY administrador {$DesAsc}";
                }else{
                    $sOrder = " ORDER BY responsable {$DesAsc}";
                }
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                if($this->session->id_company == 2){
                    $sOrder = " ORDER BY personal {$DesAsc}";
                }else{
                    $sOrder = " ORDER BY trabajador {$DesAsc}";
                }
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                if($this->session->id_company == 2){
                    $sOrder = " ORDER BY cedula {$DesAsc}";
                }else{
                    $sOrder = " ORDER BY trabajdor {$DesAsc}";
                }
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                if($this->session->id_company == 2){
                    $sOrder = " ORDER BY cargo {$DesAsc}";
                }else{
                    $sOrder = " ORDER BY nombre_trabajador_nuevo {$DesAsc}";
                }
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 8 && $this->session->id_company != 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cedula {$DesAsc}";
            }
            /*if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 9){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY labor_realizar {$DesAsc}";
            }*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 10){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY lote {$DesAsc}";
            }
            /*if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 11){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cable {$DesAsc}";
            }*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 12){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY almuerzo {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 13){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY observaciones {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 14){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY nombre_trabajador_nuevo {$DesAsc}";
            }


            if(isset($_POST['id']) && trim($_POST['id']) != ""){
                $sWhere .= " AND asistencia.id like '%".$_POST["id"]."%'";
            }   
            if(isset($_POST['tipo_trabajador']) && trim($_POST['tipo_trabajador']) != ""){
                $sWhere .= " AND trabajo like '%".$_POST["tipo_trabajador"]."%'";
            }   
            if(isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != ""
                && isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != ""){
                $sWhere .= " AND fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }    
            if(isset($_POST['hora']) && trim($_POST['hora']) != ""){
                $sWhere .= " AND hora LIKE '%".$_POST["hora"]."%'";
            }
            if(isset($_POST['cargo']) && trim($_POST['cargo']) != ""){
                $sWhere .= " AND cargo LIKE '%".$_POST["cargo"]."%'";
            }
            if(isset($_POST['responsable']) && trim($_POST['responsable']) != ""){
                if($this->session->id_company == 2){
                    $sWhere .= " AND administrador LIKE '%".$_POST["responsable"]."%'";
                }else{
                    $sWhere .= " AND responsable LIKE '%".$_POST["responsable"]."%'";
                }
            }
            if(isset($_POST['cedula']) && trim($_POST['cedula']) != ""){
                $sWhere .= " AND cedula LIKE '%".$_POST["cedula"]."%'";
            } 
            if(isset($_POST['labores']) && trim($_POST['labores']) != ""){
                $sWhere .= " AND labores LIKE '%".$_POST["labores"]."%'";
            } 
            if(isset($_POST['lote']) && trim($_POST['lote']) != ""){
                $sWhere .= " AND lote LIKE '%".$_POST["lote"]."%'";
            }
            if(isset($_POST['cable']) && trim($_POST['cable']) != ""){
                $sWhere .= " AND cable LIKE '%".$_POST["cable"]."%'";
            }
            if(isset($_POST['almuerzo']) && trim($_POST['almuerzo']) != ""){
                $sWhere .= " AND almuerzo = '".$_POST["almuerzo"]."'";
            }
            if(isset($_POST['observaciones']) && trim($_POST['observaciones']) != ""){
                $sWhere .= " AND observaciones LIKE '%".$_POST["observaciones"]."%'";
            }
            if(isset($_POST['trabajador']) && trim($_POST['trabajador']) != ""){
                $sWhere .= " AND personal LIKE '%".$_POST["trabajador"]."%'";
            }
            if(isset($_POST['nombre_trabajador_nuevo']) && trim($_POST['nombre_trabajador_nuevo']) != ""){
                $sWhere .= " AND nombre_trabajador_nuevo LIKE '%".$_POST["nombre_trabajador_nuevo"]."%'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        if($this->session->id_company == 2){
            $sql = "SELECT 
                                asistencia_detalle.id_asistencia,
                                asistencia.fecha, 
                                asistencia.hora, 
                                asistencia_detalle.observaciones,
                                asistencia.administrador as responsable, 
                                personal.cedula,
                                personal.cargo,
                                personal.nombre as nombre_trabajador_nuevo,
                                asistencia.fecha , 
                                (SELECT GROUP_CONCAT(lote SEPARATOR '</li><li>') FROM asistencia_detalle_lotes WHERE id_asistencia_detalle = asistencia_detalle.id) as lote,
                                (SELECT COUNT(*) FROM asistencia_detalle
                                    INNER JOIN asistencia ON id_asistencia = asistencia.id WHERE 1=1 $sWhere) as totalRows 
                    FROM asistencia_detalle
                    INNER JOIN personal ON id_personal = personal.id
                    INNER JOIN asistencia ON id_asistencia = asistencia.id
                    WHERE 1=1 $sWhere $sOrder $sLimit";
        }else{
            $sql = "SELECT asistencia_detalle.* , asistencia.responsable, asistencia.fecha , (SELECT COUNT(*) FROM asistencia_detalle
                                                                        INNER JOIN asistencia ON id_asistencia = asistencia.id WHERE 1=1 $sWhere) as totalRows 
                    FROM asistencia_detalle
                    INNER JOIN asistencia ON id_asistencia = asistencia.id
                    WHERE 1=1 $sWhere $sOrder $sLimit";
        }
        $data = $this->db->queryAll($sql);

        if(isset($_POST["EnableEdit"]) && (int) $_POST["EnableEdit"] == 1){
            foreach($data as $key => $value){
            	$options_labores_full = $this->db->queryAll("SELECT id, nombre as label FROM cat_labores WHERE status = 1");
            	$options_cables_full = $this->db->queryAll("SELECT id, nombre as label FROM cat_cables WHERE status = 1 AND id_lote = (SELECT id FROM cat_lotes WHERE nombre = '{$value->lote}')");
            	
                $options_labores = explode("|", $value->labor_realizar);
                $options_cables = explode("|", $value->cable);

                $lotes = $this->db->queryAll("SELECT id, nombre as label FROM cat_lotes WHERE status = 1");

                $response->data[] = [
                    $this->getInput("text", "id", $value->id_asistencia, "readonly"),
                    $this->getInput("text", "id", $value->id_asistencia, "readonly"),
                    $this->getInput("text", "fecha", $value->fecha, "readonly"),
                    $this->getInput("text", "hora", $value->hora),
                    $this->getInput("text", "responsable", $value->responsable),
                    $this->getInput("text", "trabajador", $value->trabajador),
                    $this->getInput("text", "cedula", $value->cedula),
                    $this->getInput("select-special", "lote", $value->lote, "", $lotes),
                    $this->getInput("select-special", "cables", $options_cables, "multiple", $options_cables_full),
                    $this->getInput("select-special", "labor_realizar", $options_labores, "multiple", $options_labores_full),
                    $this->getInput("checkbox", "almuerzo", $value->almuerzo),
                    $this->getInput("text", "observaciones", $value->observaciones),
                    $this->getInput("text", "nombre_trabajador_nuevo", $value->nombre_trabajador_nuevo),
                    ""
                ];
                $response->recordsTotal = $value->totalRows;
            }
        }else{
            if($this->session->id_company == 2){
                foreach($data as $key => $value){
                    $response->data[] = [
                        $value->id_asistencia,
                        $value->id_asistencia,
                        $value->fecha,
                        $value->hora,
                        $value->responsable,
                        $value->nombre_trabajador_nuevo,
                        $value->cedula,
                        $value->cargo,
                        "<ul><li>".$value->lote."</li></ul>",
                        $value->observaciones,
                        "<a href='agregarAsistencia?id={$value->id_asistencia}' class='btn btn-primary'>Editar</a>"
                    ];
                    $response->recordsTotal = $value->totalRows;
                }
            }else{
                foreach($data as $key => $value){
                    $response->data[] = [
                        $value->id_asistencia,
                        $value->id_asistencia,
                        $value->fecha,
                        $value->hora,
                        $value->responsable,
                        $value->trabajador,
                        $value->nombre_trabajador_nuevo,
                        $value->cedula,
                        $value->lote,
                        str_replace("|", "<br>", $value->cable),
                        str_replace("|", "<br>", $value->labor_realizar),
                        (($value->almuerzo == 1) ? "Si" : "No"),
                        $value->observaciones,
                        ""
                    ];
                    $response->recordsTotal = $value->totalRows;
                }
            }
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionStatus = "OK";
        return $response;
    }

    public function indexAsistencia2(){
        $response = new stdClass;
        $response->data = [];
        $response->recordsTotal = 0;

        $sWhere = "";
        $sOrder = " ORDER BY asistencia.fecha DESC, asistencia.id";
        $DesAsc = "ASC";
        $sOrder .= " {$DesAsc}";
        $sLimit = "";
        $sHaving = "";

        if(isset($_POST)){
            /*----------  ORDER BY ----------*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY asistencia.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY trabajador {$DesAsc}, asistencia.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY fecha {$DesAsc}, asistencia.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY hora {$DesAsc}, asistencia.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cedula {$DesAsc}, asistencia.id {$DesAsc}";
            }
            /*if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 6){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY labor_realizar {$DesAsc}";
            }*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 7){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY lote {$DesAsc}, asistencia.id {$DesAsc}";
            }
            /*if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 8){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY cable {$DesAsc}";
            }*/
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 9){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY almuerzo {$DesAsc}, asistencia.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 10){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY observaciones {$DesAsc}, asistencia.id {$DesAsc}";
            }
            if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 11){
                $DesAsc = $_POST['order'][0]['dir'];
                $sOrder = " ORDER BY nombre_trabajador_nuevo {$DesAsc}, asistencia.id {$DesAsc}";
            }

            if(isset($_POST['id']) && trim($_POST['id']) != ""){
                $sWhere .= " AND asistencia.id like '%".$_POST["id"]."%'";
            }   
            if(isset($_POST['tipo_trabajador']) && trim($_POST['tipo_trabajador']) != ""){
                $sWhere .= " AND trabajo like '%".$_POST["tipo_trabajador"]."%'";
            }   
            if(isset($_POST['order_date_from']) && trim($_POST['order_date_from']) != ""
                && isset($_POST['order_date_to']) && trim($_POST['order_date_to']) != ""){
                $sWhere .= " AND fecha BETWEEN '".$_POST["order_date_from"]."' AND '".$_POST["order_date_to"]."'";
            }    
            if(isset($_POST['hora']) && trim($_POST['hora']) != ""){
                $sWhere .= " AND hora LIKE '%".$_POST["hora"]."%'";
            } 
            if(isset($_POST['cedula']) && trim($_POST['cedula']) != ""){
                $sWhere .= " AND cedula LIKE '%".$_POST["cedula"]."%'";
            } 
            if(isset($_POST['labores']) && trim($_POST['labores']) != ""){
                $sWhere .= " AND labores LIKE '%".$_POST["labores"]."%'";
            } 
            if(isset($_POST['lote']) && trim($_POST['lote']) != ""){
                $sWhere .= " AND lote LIKE '%".$_POST["lote"]."%'";
            }
            if(isset($_POST['cable']) && trim($_POST['cable']) != ""){
                $sWhere .= " AND cable LIKE '%".$_POST["cable"]."%'";
            }
            if(isset($_POST['almuerzo']) && trim($_POST['almuerzo']) != ""){
                $sWhere .= " AND almuerzo = '".$_POST["almuerzo"]."'";
            }
            if(isset($_POST['observaciones']) && trim($_POST['observaciones']) != ""){
                $sWhere .= " AND observaciones LIKE '%".$_POST["observaciones"]."%'";
            }
            if(isset($_POST['nombre_trabajador_nuevo']) && trim($_POST['nombre_trabajador_nuevo']) != ""){
                $sWhere .= " AND nombre_trabajador_nuevo LIKE '%".$_POST["nombre_trabajador_nuevo"]."%'";
            }

            /*----------  LIMIT  ----------*/
            if(isset($_POST['length']) && $_POST['length'] > 0){
                $sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
            }
        }

        $sql = "SELECT asistencia_detalle.* , asistencia.fecha , (SELECT COUNT(*) FROM asistencia_detalle
                                                                    INNER JOIN asistencia ON id_asistencia = asistencia.id WHERE 1=1 $sWhere) as totalRows 
                FROM asistencia_detalle
                INNER JOIN asistencia ON id_asistencia = asistencia.id
                WHERE 1=1 $sWhere $sOrder $sLimit";
        $data = $this->db->queryAll($sql);

        if(isset($_POST["EnableEdit"]) && (int) $_POST["EnableEdit"] == 1){
            foreach($data as $key => $value){
            	$options_labores_full = $this->db->queryAll("SELECT id, nombre as label FROM cat_labores WHERE status = 1");
            	$options_cables_full = $this->db->queryAll("SELECT id, nombre as label FROM cat_cables WHERE status = 1 AND id_lote = (SELECT id FROM cat_lotes WHERE nombre = '{$value->lote}')");
            	
                $options_labores = explode("|", $value->labor_realizar);
                $options_cables = explode("|", $value->cable);

                $lotes = $this->db->queryAll("SELECT id, nombre as label FROM cat_lotes WHERE status = 1");

                $response->data[] = [
                    $this->getInput("text", 			"id", 						$value->id_asistencia, 	"readonly"),
                    $this->getInput("text", 			"id", 						$value->id_asistencia, 	"readonly"),
                    $this->getInput("text", 			"fecha", 					$value->fecha, 			"readonly"),
                    $this->getInput("text", 			"hora", 					$value->hora),
                    $this->getInput("text", 			"trabajador", 				$value->trabajador),
                    $this->getInput("text", 			"nombre_trabajador_nuevo", 	$value->nombre_trabajador_nuevo),
                    $this->getInput("text", 			"cedula", 					$value->cedula),
                    $this->getInput("select-special", 	"lote", 					$value->lote, 			"", 		$lotes),
                    $this->getInput("select-special", 	"cables", 					$options_cables, 		"multiple", $options_cables_full),
                    $this->getInput("select-special", 	"labor_realizar", 			$options_labores, 		"multiple", $options_labores_full),
                    $this->getInput("checkbox", 		"almuerzo", 				$value->almuerzo),
                    $this->getInput("text", 			"observaciones", 			$value->observaciones),
                    ""
                ];
                $response->recordsTotal = $value->totalRows;
            }
        }else{
            foreach($data as $key => $value){
            	$labores = explode("|", $value->labor_realizar);
            	$cables  = explode("|", $value->cable);

            	foreach((($labores > $cables) ? $labores : $cables) as $key2 => $valor){
            		$response->data[] = [
	                    $value->id_asistencia,
	                    $value->id_asistencia,
	                    ($key2 == 0) ? $value->fecha : '',
	                    ($key2 == 0) ? $value->hora : '',
	                    ($key2 == 0) ? $value->trabajador : '',
	                    ($key2 == 0) ? $value->nombre_trabajador_nuevo : '',
	                    ($key2 == 0) ? $value->cedula : '',
	                    ($key2 == 0) ? $value->lote : '',
	                    isset($cables[$key2]) ? $cables[$key2] : '',
	                    isset($labores[$key2]) ? $labores[$key2] : '',
	                    ($key2 == 0) ? (($value->almuerzo == 1) ? "Si" : "No") : '',
	                    ($key2 == 0) ? $value->observaciones : '',
	                    ""
	                ];
            	}
                $response->recordsTotal = $value->totalRows;
            }
        }

        $response->recordsFiltered = count($response->data);
        $response->customActionStatus = "OK";
        return $response;
    }

    public function saveAsistencia(){
    	$response = new stdClass;
        
        $postdata = new stdClass;
        foreach($_POST as $key => $value){
        	foreach($value as $key2 => $value2){
        		if(strpos($key, "labor_realizar_") !== false){
        			$id_labor = (int) explode("_", $key)[2];
        			$postdata->data[$key2]["labor_realizar"][$id_labor] = $value2;
	        	}else if(strpos($key, "cables_") !== false){
	        		$id_cable = (int) explode("_", $key)[1];
        			$postdata->data[$key2]["cables"][$id_cable] = $value2;
	        	}else{
	        		$postdata->data[$key2]{$key} = $value2;
	        	}
        	}
        }
        
        foreach($postdata->data as $data){
        	$data = (object) $data;

        	$str_labores = "";
        	foreach($data->labor_realizar as $key => $labor){
        		if((int)$labor == 1){
        			$str_labores .= trim($this->db->queryRow("SELECT nombre FROM cat_labores WHERE status = 1 AND id = '{$key}'")->nombre);
        			$str_labores .= "|";
        		}
        	}
        	$str_labores = substr($str_labores, 0, -1);

        	$str_cables = "";
        	foreach($data->cables as $key => $cable){
        		if((int)$cable == 1){
        			$str_cables .= trim($this->db->queryRow("SELECT nombre FROM cat_cables WHERE status = 1 AND id = '{$key}'")->nombre);
        			$str_cables .= "|";
        		}
        	}
        	$str_cables = substr($str_cables, 0, -1);

        	$sql = "UPDATE asistencia_detalle
        			SET 
        				trabajador = '{$data->trabajador}',
        				hora = '{$data->hora}',
        				cedula = '{$data->cedula}',
        				labor_realizar = '{$str_labores}',
        				lote = '{$data->lote}',
        				cable = '{$str_cables}',
        				almuerzo = '{$data->almuerzo}',
        				observaciones = '{$data->observaciones}',
        				nombre_trabajador_nuevo = '{$data->nombre_trabajador_nuevo}'
        			WHERE id_asistencia = '{$data->id}'";
        	$this->db->query($sql);
        }

        $response->success = 200;
        return $response;
    }

    #tabla de revision merma
    public function merma(){
        $response = new stdClass;
        $response->data = [];

        $sql = "SELECT * FROM merma";
        $data = $this->db->queryAll($sql);

        if($postdata->EnableEdit == 1){
            foreach($data as $key => $value){
                $response->data[] = [
                    $this->getInput("text", "id", $value->id, "readonly"),
                    $this->getInput("text", "id", $value->id, "readonly"),
                    $this->getInput("text", "bloque", $value->bloque),
                    $this->getInput("text", "finca", $value->finca),
                    $this->getInput("text", "palanca", $value->palanca),
                    $this->getInput("text", "total_defectos", $value->total_defectos),
                    $this->getInput("text", "porcentaje_merma", $value->porcentaje_merma),
                    ""
                ];
            }
        }else{
            $response->data[] = [
                $value->id,
                $value->id,
                $value->bloque,
                $value->finca,
                $value->palanca,
                $value->total_defectos,
                $value->porcentaje_merma,
                ""
            ];
        }

        return $response;
    }

    /*
    *   $type : [text, select, select-special, number, checkbox]
    *   $name : id y name del input
    *   $value : valor que tendra seteado
    *   $options : (opcional) en el caso de select [id, label] ... queryAllSpecial
    */
    private function getInput($type, $name, $value, $config = "", $options = []){
        $input = "";
        switch($type){
            case "text":
                $input = '<input type="text" class="form-control save" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$config.'/>';
                break;
            case "checkbox":
            	$input = '
            	<select class="form-control save" name="'.$name.'">
            		<option value="1" '.(($value)?'selected':'').'>Si</option>
            		<option value="0" '.((!$value)?'selected':'').'>No</option>
            	</select/>';
                break;
            case "select":
            	if($config == "multiple")
            	{
            		#value need it array
            		foreach($options as $key => $opt){
            			$input .= "{$opt}
            				<select class='form-control save' name='{$name}_{$key}'>
            					<option value='1' ".(($value)?'selected':'').">Si</option>
            					<option value='0' ".((!$value)?'selected':'').">No</option>
            				</select>";
            			$input .= "<br>";
            		}
            	}
            	else
            	{
            		$input = "<select class='form-control save' name='{$name}'>";
                	foreach($options as $opt){
                    	$input .= '<option class="form-control" value="'.$opt.'" '.(($opt == $value)?'selected':'').'>'.$opt.'</option>';
	                }
	                $input .= "</select>";
	            }
                break;
            case "select-special":
            	if($config == "multiple")
            	{
            		#value need it array
            		foreach($options as $key => $opt){
            			$input .= "{$opt->label}
            				<select class='form-control save' name='{$name}_{$opt->id}'>
            					<option value='1' ".((in_array(trim($opt->label), $value))?'selected':'').">Si</option>
            					<option value='0' ".((!in_array(trim($opt->label), $value))?'selected':'').">No</option>
            				</select>";
            			$input .= "<br>";
            		}
            	}
            	else
            	{
	                $input = "<select class='form-control save' name='{$name}'>";
	                foreach($options as $opt){
	                    $opt = (object) $opt;
	                    $input .= '<option value="'.$opt->id.'" '.((trim($opt->label) == $value)?'selected':'').'>'.$opt->label.'</option>';
	                }
	                $input .= "</select>";
	            }
                break;
            case "number":
                $input = '<input type="number" class="form-control save" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$config.'/>';
                break;
        }
        return $input;
    }
}