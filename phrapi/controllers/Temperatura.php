<?php defined('PHRAPI') or die("Direct access not allowed!");

class Temperatura {
	public $name;
	private $db;
	private $db_sigat;
	private $config;

	public function __construct(){
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
	}

	public function index(){
        $response = new stdClass;
        $response->temp_maxima = $this->tempMaxima();
        $response->temp_minima = $this->tempMinima();
        $response->temp_promedio = $this->tempPromedio();
        return $response;
    }


    private function tempMaxima(){
        $response = new stdClass;
        $response->series = [];
        
        $response->legends = $this->db->queryAllOne("SELECT semana FROM lancofruit_clima WHERE anio = YEAR(CURRENT_DATE) GROUP BY semana ORDER BY semana");
        $data_series = $this->db->queryAll("
            SELECT semanas.semana, estacion, value
            FROM (
                SELECT semana
                FROM lancofruit_clima 
                WHERE anio = YEAR(CURRENT_DATE) 
                GROUP BY semana
            ) semanas
            JOIN (
                SELECT l.station_id as id, IFNULL(alias.alias, station_name) as estacion
                FROM lancofruit_clima l
                LEFT JOIN lancofruit_clima_estaciones_alias alias ON l.station_id = alias.station_id
                WHERE anio = YEAR(CURRENT_DATE) 
                GROUP BY station_name
            ) estaciones
            LEFT JOIN  (
                SELECT semana, station_id, temp_maxima as value 
                FROM lancofruit_clima 
                WHERE anio = YEAR(CURRENT_DATE)
            ) data ON semanas.semana = data.semana AND estaciones.id = data.station_id
            ORDER BY semanas.semana, estaciones.estacion
        ");
        foreach($data_series as $row){
            if(!isset($response->series[$row->estacion])){
                $response->series[$row->estacion] = [
                    "connectNulls" => true,
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => "0",
                            "barBorderWidth" => "6",
                            "label" => [
                                "show" => false,
                                "position" => "insideTop"
                            ]
                        ]
                    ],
                    "type" => "line",
                    "name" => $row->estacion,
                    "data" => []
                ];
            }
            $response->series[$row->estacion]["data"][] = $row->value;
        }
        
        return $response;
    }

    private function tempMinima(){
        $response = new stdClass;
        $response->series = [];
        
        $response->legends = $this->db->queryAllOne("SELECT semana FROM lancofruit_clima WHERE anio = YEAR(CURRENT_DATE) GROUP BY semana ORDER BY semana");
        $data_series = $this->db->queryAll("
            SELECT semanas.semana, estacion, value
            FROM (
                SELECT semana
                FROM lancofruit_clima 
                WHERE anio = YEAR(CURRENT_DATE) 
                GROUP BY semana
            ) semanas
            JOIN (
                SELECT l.station_id as id, IFNULL(alias.alias, station_name) as estacion
                FROM lancofruit_clima l
                LEFT JOIN lancofruit_clima_estaciones_alias alias ON l.station_id = alias.station_id
                WHERE anio = YEAR(CURRENT_DATE) 
                GROUP BY station_name
            ) estaciones
            LEFT JOIN  (
                SELECT semana, station_id, temp_minima as value 
                FROM lancofruit_clima 
                WHERE anio = YEAR(CURRENT_DATE)
            ) data ON semanas.semana = data.semana AND estaciones.id = data.station_id
            ORDER BY semanas.semana, estaciones.estacion
        ");
        foreach($data_series as $row){
            if(!isset($response->series[$row->estacion])){
                $response->series[$row->estacion] = [
                    "connectNulls" => true,
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => "0",
                            "barBorderWidth" => "6",
                            "label" => [
                                "show" => false,
                                "position" => "insideTop"
                            ]
                        ]
                    ],
                    "type" => "line",
                    "name" => $row->estacion,
                    "data" => []
                ];
            }
            $response->series[$row->estacion]["data"][] = $row->value;
        }
        
        return $response;
    }

    private function tempPromedio(){
        $response = new stdClass;
        $response->series = [];
        
        $response->legends = $this->db->queryAllOne("SELECT semana FROM lancofruit_clima WHERE anio = YEAR(CURRENT_DATE) GROUP BY semana ORDER BY semana");
        $data_series = $this->db->queryAll("
            SELECT semanas.semana, estacion, value
            FROM (
                SELECT semana
                FROM lancofruit_clima 
                WHERE anio = YEAR(CURRENT_DATE) 
                GROUP BY semana
            ) semanas
            JOIN (
                SELECT l.station_id as id, IFNULL(alias.alias, station_name) as estacion
                FROM lancofruit_clima l
                LEFT JOIN lancofruit_clima_estaciones_alias alias ON l.station_id = alias.station_id
                WHERE anio = YEAR(CURRENT_DATE) 
                GROUP BY station_name
            ) estaciones
            LEFT JOIN  (
                SELECT semana, station_id, temp_promedio as value 
                FROM lancofruit_clima 
                WHERE anio = YEAR(CURRENT_DATE)
            ) data ON semanas.semana = data.semana AND estaciones.id = data.station_id
            ORDER BY semanas.semana, estaciones.estacion
        ");
        foreach($data_series as $row){
            if(!isset($response->series[$row->estacion])){
                $response->series[$row->estacion] = [
                    "connectNulls" => true,
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => "0",
                            "barBorderWidth" => "6",
                            "label" => [
                                "show" => false,
                                "position" => "insideTop"
                            ]
                        ]
                    ],
                    "type" => "line",
                    "name" => $row->estacion,
                    "data" => []
                ];
            }
            $response->series[$row->estacion]["data"][] = $row->value;
        }
        
        return $response;
    }
}