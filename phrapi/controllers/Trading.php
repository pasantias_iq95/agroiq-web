<?php defined('PHRAPI') or die("Direct access not allowed!");

class Trading {
	private $db;

	public function __construct(){
        $this->db = DB::getInstance('dagcoin');
	}

	public function open(){
        $symbol = $_POST['symbol'];
        $type = $_POST['type'];

        $this->db->query("INSERT INTO trades_open SET symbol = '{$symbol}', type = '{$type}'");

        $subject = "Nuevo trade abierto";
        $body = "Nuevo trade {$symbol} tipo {$type}";

        $this->sendEmail("victoralvarezsaucedo@gmail.com" , $subject ,"", $body, "victor-señales@pforex.com", "Victor Señales - Forex");
        $this->sendEmail("akalixx.mr@gmail.com" , $subject ,"", $body, "victor-señales@pforex.com", "Victor Señales - Forex");

        //$this->sendWhatsapp("+593998342743", $subject." - ".$body);
        //$this->sendWhatsapp("+593998333294", $subject." - ".$body);
    }

    private function sendWhatsapp($phone, $message){
        // create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, "https://wa.me/+$phone?text=$message");

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch);      
    }

    public function close(){
        $symbol = $_POST['symbol'];
        $type = $_POST['type'];

        $this->db->query("DELETE FROM trades_open WHERE symbol = '{$symbol}' AND type = '{$type}' LIMIT 1");

        $subject = "Trade cerrado";
        $body = "Trade Cerrado {$symbol} tipo {$type}";

        $this->sendEmail("victoralvarezsaucedo@gmail.com" , $subject ,"", $body, "victor-señales@pforex.com", "Victor Señales - Forex");
        $this->sendEmail("akalixx.mr@gmail.com" , $subject ,"", $body, "victor-señales@pforex.com", "Victor Señales - Forex");
    }

    private function sendEmail($to, $subject, $body_text, $body_html, $from, $from_name) {
        // Initialize cURL
        $ch = curl_init();
        
        // Set cURL options
        curl_setopt($ch, CURLOPT_URL, 'https://api.elasticemail.com/mailer/send');
        curl_setopt($ch, CURLOPT_POST, 1);
      
        // Parameter data
        $data = 'username='.urlencode('javiprocesosiq@gmail.com').
            '&api_key='.urlencode('f3e09b72-eed3-4ca8-bd02-03a0080e1a81').
            '&from='.urlencode($from).
            '&from_name='.urlencode($from_name).
            '&to='.urlencode($to).
            '&subject='.urlencode($subject);
      
        if($body_html)  $data .= '&body_html='.urlencode($body_html);
        if($body_text)  $data .= '&body_text='.urlencode($body_text);
        
        // Set parameter data to POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      
        // Header data
            $header = "Content-Type: application/x-www-form-urlencoded\r\n";
            $header .= "Content-Length: ".strlen($data)."\r\n\r\n";
      
        // Set header
        curl_setopt($ch, CURLOPT_HEADER, $header);
        
        // Set to receive server response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        // Set cURL to verify SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        
        // Set the path to the certificate used by Elastic Mail API
        // curl_setopt($ch, CURLOPT_CAINFO, getcwd()."/DOWNLOADED_CERTIFICATE.CRT");
        
        // Get result
        $result = curl_exec($ch);
        
        // Close cURL
        curl_close($ch);
        
        // print_r($result);
        // Return the response or NULL on failure
        return ($result === false) ? NULL : $result;
        
        // Alternative error checking return
        // return ($result === false) ? 'Curl error: ' . curl_error($ch): $result;
      }
}