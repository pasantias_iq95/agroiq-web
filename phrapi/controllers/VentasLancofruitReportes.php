<?php defined('PHRAPI') or die("Direct access not allowed!");

class VentasLancofruitReportes extends ReactGrafica {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    private function ProcessParams(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        return $postdata;
    }

    public function last(){
        $filters = $this->ProcessParams();
        $response = new stdClass;

        $sql = "SELECT MAX(fecha) FROM lancofruit_ventas WHERE status = 'Activo'";
        $response->fecha = $this->db->queryOne($sql);

        $sql = "SELECT fecha FROM lancofruit_ventas WHERE status = 'Activo' GROUP BY fecha";
        $response->fechas_exist = $this->db->queryAllOne($sql);

        if($filters->fecha != "" || $filters->fecha_inicial != "" || $filters->fecha_final != ""){
            if($filters->fecha != ""){
                $fecha_inicial = $filters->fecha;
                $fecha_final = $filters->fecha;
            } else {
                $fecha_inicial = $filters->fecha_inicial;
                $fecha_final = $filters->fecha_final;
            }
            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$fecha_inicial}' AND '{$fecha_final}'";
        } else {
            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$response->fecha}' AND '{$response->fecha}'";
        }

        $sql = "SELECT razon_social_comprador As id, razon_social_comprador As label FROM lancofruit_ventas WHERE status = 'Activo' $sWhere GROUP BY razon_social_comprador ORDER BY label";
        $response->clientes = $this->db->queryAllOne($sql);

        $sql = "SELECT lancofruit_rutas.id AS id, lancofruit_rutas.nombre AS label 
                FROM lancofruit_rutas 
                INNER JOIN lancofruit_ventas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta WHERE lancofruit_rutas.status = '1'
                GROUP BY lancofruit_rutas.nombre";
        $response->rutas = $this->db->queryAll($sql);

        return $response;
    }

    public function indicadores(){
        $filters = $this->ProcessParams();
        $response = new stdClass;
        $response->tags = [];
        $sWhere = "";
        $sWhereAnio = "";
        $total = "";
        $total_puntos = "";
        
        if($filters->fecha != "" || $filters->fecha_inicial != "" || $filters->fecha_final != ""){
            if($filters->fecha != ""){
                $fecha_inicial = $filters->fecha;
                $fecha_final = $filters->fecha;
            } else {
                $fecha_inicial = $filters->fecha_inicial;
                $fecha_final = $filters->fecha_final;
            }

            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$fecha_inicial}' AND '{$fecha_final}'";
        } 
        if($filters->id_ruta != ""){
            $sWhere .= " AND lancofruit_ventas.id_ruta = '{$filters->id_ruta}'";
        }
        if($filters->cliente != ""){
            $sWhere .= " AND lancofruit_ventas.razon_social_comprador = '{$filters->cliente}'";
        } 


        $sql = "SELECT SUM(total) AS total FROM lancofruit_ventas WHERE status = 'Activo' $sWhere";
        $total = $this->db->queryOne($sql);
        
        $sql = "SELECT SUM(total) AS ventas FROM lancofruit_ventas WHERE status = 'Activo' $sWhere";
        $val = $this->db->queryOne($sql);
        if($val == null) $val = 0;
		$response->tags[] = [
            "tittle" => "TOTAL VENTAS",
            "subtittle" => "",
            "valor" => (double) $val,
            "promedio" => 100,
            "cssClass" => "green-jungle",
            "miles" => true
        ];

        $gavetas = ROUND($total / 6, 2). " GAVETAS";
        $response->tags[] = [
            "tittle" => "TOTAL GAVETAS",
            "subtittle" => "",
            "valor" => (double) $gavetas,
            "promedio" => 100,
            "cssClass" => "green-jungle",
            "miles" => true
        ];

        $sql = "SELECT COUNT(1) AS puntos FROM lancofruit WHERE status = 'Cliente'";
        $total_puntos = $this->db->queryOne($sql)." PUNTOS EN TOTAL";
        
        $sql = "SELECT SUM(1) FROM lancofruit_ventas WHERE status = 'Activo' $sWhere";
        $puntos_visitados = $this->db->queryOne($sql);
        
        if($val == null) $val = 0;
        $response->tags[] = [
            "tittle" => "PUNTOS VISITADOS",
            "subtittle" => "{$total_puntos}",
            "valor" => (double) $puntos_visitados,
            "promedio" => (double) 100,
            "cssClass" => "green-jungle"
        ];
        
        $val = $total / $puntos_visitados;
        if($val == null) $val = 0;
        $gavetas = ROUND($val / 6, 2). " GAVETAS";
        
        $response->tags[] = [
            "tittle" => "PROMEDIO VENTAS/PUNTOS",
            "subtittle" => "{$gavetas}",
            "valor" => (double) $val,
            "promedio" => (double)  $val / $total * 100,
            "cssClass" => "green-jungle",
            "miles" => true
        ];
        return $response;
    }
    
    public function eliminar(){
        $params = $this->ProcessParams();
        $response = new stdClass;
        $response->status = 400;

        $sql = "UPDATE lancofruit_ventas SET status = 'Eliminado' WHERE id = {$params->id_venta}";
        if($this->db->query($sql)){
            $response->status = 200;
        }

        return $response;
    }

    public function editar(){
        $params = $this->ProcessParams();
        $response = new stdClass;

        if(count($params->total_modificado) > 0){
            $response->status = 200;
            foreach($params->total_modificado as $key => $value){
                $sql = "UPDATE lancofruit_ventas SET total = '{$value}', total_sin_impuestos = '{$value}', importe_total = '{$value}' WHERE id = {$key}";
                $this->db->query($sql);
            }
        }

        return $response;
    }

    public function ventaDia(){
        $response = new stdClass();
        $filters = $this->ProcessParams();
        $sWhere = "";
        $cli = "";
        $por_gaveta = "";

        if($filters->fecha != ""){
            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$filters->fecha}' AND '{$filters->fecha}'";
        }
        if($filters->cliente != ""){
            $sWhere .= " AND lancofruit_ventas.razon_social_comprador = '{$filters->cliente}'";
            $cli .= " AND lancofruit_ventas.razon_social_comprador = '{$filters->cliente}'";
        } 
        if($filters->type == "GAVETAS"){
            $por_gaveta = " / 6";
        }

        $response->days = $this->db->queryAllOne("SELECT fecha FROM lancofruit_ventas WHERE 1=1 $cli GROUP BY fecha");

        $sql = "SELECT 
                    id As id_venta,
                    1 AS cont,
                    fecha,
                    razon_social_comprador AS punto,
                    SUM(total) AS venta
                FROM lancofruit_ventas
                WHERE lancofruit_ventas.status = 'Activo' AND lancofruit_ventas.id_ruta = '{$filters->id_ruta}' $sWhere
                GROUP BY id ORDER BY fecha";
        $response->data_venta_dia = $this->db->queryAll($sql);
        
        $sql = "SELECT 
                    lancofruit_ventas.razon_social_comprador AS cliente,
                    SUM(lancofruit_ventas.total) {$por_gaveta} AS total
                FROM lancofruit_ventas
                WHERE lancofruit_ventas.status = 'Activo' AND lancofruit_ventas.id_ruta = '{$filters->id_ruta}' $sWhere
                GROUP BY lancofruit_ventas.razon_social_comprador";
        $response->clientes = $this->db->queryAll($sql);

        $sql = "SELECT 
                    lancofruit_ventas.razon_social_comprador AS legend,
                    lancofruit_rutas.nombre AS label,
                    SUM(lancofruit_ventas.total) {$por_gaveta} AS value,
                    true AS selected
                FROM lancofruit_ventas 
                INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' AND lancofruit_ventas.id_ruta = '{$filters->id_ruta}' $sWhere
                GROUP BY legend";
        $response->data = $this->generateSeries($this->db, $sql, "line", null, true);
        return $response;   
    }

    public function ventaComparativo(){
        $response = new stdClass();
        $filters = $this->ProcessParams();
        $sWhere = "";
        $cli = "";
        $por_gaveta = "";

        if($filters->fecha_inicial != "" && $filters->fecha_final != ""){
            $sWhere .= " AND lancofruit_ventas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        } 
        if($filters->cliente != ""){
            $sWhere .= " AND lancofruit_ventas.razon_social_comprador = '{$filters->cliente}'";
            $cli .= " AND lancofruit_ventas.razon_social_comprador = '{$filters->cliente}'";
        } 
        if($filters->type == "GAVETAS"){
            $por_gaveta = " / 6";
        }

        $response->days = $this->db->queryAllOne("SELECT fecha FROM lancofruit_ventas WHERE 1=1 $cli GROUP BY fecha");


        $sql = "SELECT 
                    lancofruit_rutas.id AS id_ruta,
                    lancofruit_ventas.id As id_venta,
                    lancofruit_rutas.nombre AS ruta, 
                    COUNT(1) AS puntos,
                    SUM(lancofruit_ventas.total) AS ventas
                FROM lancofruit_ventas
                INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY lancofruit_ventas.id_ruta ORDER BY lancofruit_rutas.id";
        $response->data = $this->db->queryAll($sql);
        foreach($response->data as $row){
            $sql = "SELECT 
                        lancofruit_ventas.id As id_venta,
                        lancofruit_ventas.razon_social_comprador AS ruta, 
                        COUNT(1) AS puntos,
                        SUM(lancofruit_ventas.total) AS ventas
                    FROM lancofruit_ventas
                    INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                    WHERE lancofruit_ventas.status = 'Activo' AND lancofruit_ventas.id_ruta = '{$row->id_ruta}' $sWhere
                    GROUP BY lancofruit_ventas.razon_social_comprador ORDER BY lancofruit_rutas.id";
            $row->detalle = $this->db->queryAll($sql);
        }
        
        $sql = "SELECT 
                    lancofruit_rutas.nombre AS ruta, 
                    SUM(lancofruit_ventas.total) {$por_gaveta} AS ventas
                FROM lancofruit_ventas
                INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY lancofruit_ventas.id_ruta ORDER BY lancofruit_rutas.id";
        $response->data_pie = $this->db->queryAll($sql);

        $sql = "SELECT 
                    lancofruit_rutas.nombre AS legend,
                    lancofruit_ventas.fecha AS label,
                    SUM(lancofruit_ventas.total) {$por_gaveta} AS value,
                    true AS selected
                FROM lancofruit_ventas 
                INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
                WHERE lancofruit_ventas.status = 'Activo' $sWhere
                GROUP BY label, lancofruit_rutas.nombre
                ORDER BY fecha";
        $response->data_bar = $this->generateSeries($this->db, $sql, "line", null, true);
        return $response;   
    }

    public function ventaTendencia(){
        $response = new stdClass();
        $filters = $this->ProcessParams();
        $sWhere = "";
        $por_gaveta = "";
        
        if($filters->cliente != ""){
            $sWhere .= " AND lancofruit_ventas.razon_social_comprador = '{$filters->cliente}'";
        } 
        if($filters->type == 'GAVETAS'){
            $por_gaveta = '/ 6';
        } 

        $sql = "SELECT semana AS semana 
                FROM lancofruit_ventas 
                WHERE anio = YEAR(CURRENT_DATE) {$sWhere} GROUP BY semana ORDER BY semana"; 
        $response->legends = $this->db->queryAllOne($sql);

		$sql = "SELECT lancofruit_rutas.id, lancofruit_rutas.nombre as detalle
				FROM lancofruit_ventas
				INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
				WHERE anio = YEAR(CURRENT_DATE) {$sWhere}
				GROUP BY lancofruit_rutas.nombre";
        $rutas = $this->db->queryAll($sql);
		$response->series = [];
		foreach($rutas as $ruta){
            $min = $this->db->queryOne("SELECT MIN(total) FROM (SELECT semana, SUM(total) {$por_gaveta} AS total FROM lancofruit_ventas WHERE id_ruta = {$ruta->id} AND anio = YEAR(CURRENT_DATE) {$sWhere} GROUP BY semana) AS tbl");
            $max = $this->db->queryOne("SELECT MAX(total) FROM (SELECT semana, SUM(total) {$por_gaveta} AS total FROM lancofruit_ventas WHERE id_ruta = {$ruta->id} AND anio = YEAR(CURRENT_DATE) {$sWhere} GROUP BY semana) AS tbl");
            $prom = $this->db->queryOne("SELECT AVG(total) FROM (SELECT semana, SUM(total) {$por_gaveta} AS total FROM lancofruit_ventas WHERE id_ruta = {$ruta->id} AND anio = YEAR(CURRENT_DATE) {$sWhere} GROUP BY semana) AS tbl");
            $response->series[] = [
                "name" => $ruta->detalle,
				"connectNulls" => true,
                "min" => 'dataMin',
                "minimo" => number_format($min, 2),
                "max" => number_format($max, 2),
                "prom" => number_format($prom, 2),
				"type" => "line",
				"label" => [
					"normal" => [
						"show" => false,
						"position" => "inside"
					]
				],
				"data" => [],
			];
			
            foreach($response->legends as $sem){
                $sql = "SELECT IFNULL(ROUND(SUM(lancofruit_ventas.total) {$por_gaveta}, 2), 0) 
                        FROM lancofruit_ventas
                        WHERE anio = YEAR(CURRENT_DATE) AND semana = {$sem} AND id_ruta = {$ruta->id} {$sWhere}";
				$val = (float) $this->db->queryOne($sql);
                $response->series[count($response->series)-1]["data"]["{$sem}"] = $val > 0 ? number_format($val, 2) : '';
			}
		}
        
        return $response;
    }

    public function ventaTendenciaGrafica(){
        $response = new stdClass();
        $response->data = new stdClass();
        $filters = $this->ProcessParams();
        $sWhere = "";
        $por_gaveta = "";
        
        if($filters->cliente != ""){
            $sWhere .= " AND lancofruit_ventas.razon_social_comprador = '{$filters->cliente}'";
        } 
        if($filters->type == 'GAVETAS'){
            $por_gaveta = '/ 6';
        } 

        $sql = "SELECT semana AS semana 
        FROM lancofruit_ventas 
        WHERE anio = YEAR(CURRENT_DATE) {$sWhere} GROUP BY semana ORDER BY semana";
        $response->data->legends = $this->db->queryAllOne($sql);

		$sql = "SELECT lancofruit_rutas.id, lancofruit_rutas.nombre as detalle
				FROM lancofruit_ventas
				INNER JOIN lancofruit_rutas ON lancofruit_rutas.id = lancofruit_ventas.id_ruta
				WHERE anio = YEAR(CURRENT_DATE) {$sWhere}
				GROUP BY lancofruit_rutas.nombre";
        $rutas = $this->db->queryAll($sql);

		$response->data->series = [];
		foreach($rutas as $ruta){
			$response->data->series[] = [
                "name" => $ruta->detalle,
				"connectNulls" => true,
                "min" => 'dataMin',
				"type" => "line",
				"label" => [
					"normal" => [
						"show" => false,
						"position" => "inside"
					]
				],
				"data" => [],
			];
			
            foreach($response->data->legends as $sem){
                $sql = "SELECT IFNULL(ROUND(SUM(lancofruit_ventas.total) {$por_gaveta}, 2), 0)
                FROM lancofruit_ventas
                WHERE anio = YEAR(CURRENT_DATE) AND semana = {$sem} AND id_ruta = {$ruta->id} {$sWhere}";
				$val = (float) $this->db->queryOne($sql);
                $response->data->series[count($response->data->series)-1]["data"][] = $val > 0 ? round($val, 2) : null;
			}
        }
        
        return $response;
    }
}