<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionCajas {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function last(){
        $response = new stdClass;
        $response->days = $this->db->queryAllOne("SELECT fecha FROM produccion_cajas WHERE fecha != '0000-00-00' GROUP BY fecha");
        $response->last = $this->db->queryRow("SELECT MAX(fecha) as fecha 
            FROM (
                SELECT MAX(fecha) as fecha 
                FROM produccion_gavetas 
                UNION ALL 
                SELECT MAX(fecha) 
                FROM produccion_cajas) AS tbl");
        return $response;
    }

    public function cuadreCajas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sql = "SELECT *, (SELECT SUM(valor) FROM produccion_cajas_real WHERE marca = tbl.marca AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}') AS suma_real
                FROM(
                    SELECT 'CAJA' AS tipo, produccion_cajas.marca, COUNT(1) AS balanza
                    FROM produccion_cajas 
                    WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                    GROUP BY produccion_cajas.marca
                    UNION ALL
                    SELECT 'CAJA' AS tipo, marca, 0 AS balanza
                    FROM produccion_cajas_real
                    WHERE fecha = '{$postdata->fecha_inicial}'
                ) AS tbl
                GROUP BY marca";
        $response->data = $this->db->queryAll($sql);

        foreach($response->data as $row){
            $row->detalle = $this->db->queryAll("SELECT guia, SUM(valor) AS valor FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$row->marca}' GROUP BY guia");
            if($row->balanza > 0 && $row->suma_real > 0)
                $row->porcentaje = $row->balanza / $row->suma_real * 100;
        }

        $response->guias = $this->db->queryAll("SELECT SUM(valor) AS cajas, guia, fecha, codigo_productor, codigo_magap, sello_seguridad FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' GROUP BY guia");
        foreach($response->guias as $row){
            $row->detalle = $this->db->queryAll("SELECT marca, SUM(valor) AS valor FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND guia = '{$row->guia}' GROUP BY marca");
        }
        return $response;
    }

    public function guardarCuadrar(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if(isset($postdata->fecha) && $postdata->fecha != ""){
            foreach($postdata->marca as $marca => $value){
                if($value > 0){
                    $exits = count($this->db->queryAll("SELECT * FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' AND marca = '{$marca}' AND guia = '{$postdata->guia}'")) > 0;
                    if($exits){
                        $this->db->query("UPDATE produccion_cajas_real SET valor = '{$value}' WHERE marca = '{$marca}' AND fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}'");
                        $response->message = "Se actualizo con éxito";
                    }else{
                        $this->db->query("INSERT INTO produccion_cajas_real SET valor = '{$value}', marca = '{$marca}', fecha = '{$postdata->fecha}', guia = '{$postdata->guia}', codigo_productor = '{$postdata->productor}', codigo_magap = '{$postdata->magap}', sello_seguridad = '{$postdata->sello_seguridad}'");
                        $response->message = "Se inserto con éxito";
                    }
                }
            }
        }
        return $response;
    }

    public function procesar(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $marcas = $this->db->queryAll("SELECT marca, SUM(valor) AS cantidad FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' GROUP BY marca");
        foreach($marcas as $mr){
            $bd[$mr->marca] = $this->db->queryRow("SELECT SUM(cantidad) AS cantidad 
                                                    FROM(
                                                        SELECT COUNT(1) AS cantidad 
                                                        FROM produccion_cajas 
                                                        WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}'
                                                        UNION ALL
                                                        SELECT COUNT(1) AS cantidad
                                                        FROM produccion_gavetas
                                                        WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}'
                                                    ) AS tbl")->cantidad;
            if($bd[$mr->marca] < $mr->cantidad)
                $this->db->query("UPDATE produccion_cajas_real SET status = 'PROCESADO' WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}'");
        }
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $reg){
                if(in_array($reg->marca, ['MI COMISARIATO', 'TIA'])){
                    #D("DELETE FROM produccion_gavetas WHERE id = $reg->id");
                    $this->db->query("DELETE FROM produccion_gavetas WHERE id = $reg->id");
                }else
                    $this->db->query("DELETE FROM produccion_cajas WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }

    public function filters(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->year) && $postdata->year != ""){
            $sYear = " AND year = '{$postdata->year}'";
        }

        $response->years = $this->db->queryAllSpecial("SELECT year as id, year as label FROM(
                SELECT year FROM produccion_cajas GROUP BY year 
            ) AS tbl
            WHERE year > 0
            GROUP BY year");
        
        $sql = "SELECT semana as id, semana as label FROM(
            SELECT semana FROM produccion_cajas  WHERE 1=1 $sYear GROUP BY semana 
            ) AS tbl
            GROUP BY semana";
        $response->semanas = $this->db->queryAll($sql);
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_cajas WHERE fecha = '{$postdata->fecha_inicial}' GROUP BY id_finca");
        return $response;
    }

    public function registros(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
            'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
            'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
        ];

        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT * FROM (
            SELECT id, fecha, marca, {$filters->unidad} AS peso, 'CAJA' AS tipo, hora
            FROM produccion_cajas
            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}') AS tbl
            ORDER BY fecha DESC, hora DESC");
        $response->numPages = $this->db->queryRow("SELECT CEIL(COUNT(1)/10) as num
            FROM (
            SELECT id
            FROM produccion_cajas
            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}') AS tbl")->num;
        $response->marcas = $this->db->queryAllOne("SELECT nombre FROM produccion_tipo_caja WHERE status = 'Activo'");
        
        return $response;   
    }

    public function resumenMarca(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
            'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
            'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
            'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'semana' => (int) getValueFrom($postdata, 'semana', 0),
            'unidad' => getValueFrom($postdata, 'unidad', 'lb'),
        ];
        /*if($filters->semana > 0){
            $sWhere = " AND semana = {$filters->semana}";
        }*/

        $response = new stdClass;
        $sql = "SELECT marca, SUM(cantidad) AS cantidad, SUM(total_kg) AS total_kg, CONV AS 'conv', tipo, promedio, maximo, minimo, desviacion
                FROM(
                    SELECT
                        `cajas`.`marca` AS `marca`,
                        IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS `cantidad`,
                        ROUND(SUM(cajas.`{$filters->unidad}`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.{$filters->unidad}), 0), 2) AS total_kg,
                        SUM(convertidas) AS conv,
                        'CAJA' AS tipo,
                        ROUND(AVG(cajas.`{$filters->unidad}`), 2) AS promedio,
                        MAX(cajas.`{$filters->unidad}`) AS maximo,
                        MIN(cajas.`{$filters->unidad}`) AS minimo,
                        ROUND(STD(cajas.{$filters->unidad}), 2) AS desviacion
                    FROM (`produccion_cajas` `cajas`
                    LEFT JOIN `produccion_cajas_real` `cajas_real`
                        ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                            AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                            AND status = 'PROCESADO'))
                    WHERE cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND (`cajas`.`marca` <> '') $sWhere
                    GROUP BY `cajas`.`marca`, cajas_real.id
                ) AS tbl
                GROUP BY marca";                
        $response->data = $this->db->queryAll($sql);
        
        /* LOAD TAGS */
        $response->tags = $this->tags($filters);
        $response->tags["cajas40"] = $this->sumOfValue($response->data, 'conv');

        return $response;
    }

    private function sumOfValue($data, $prop){
        $sum = 0;
        foreach($data as $row)
            if(is_object($row))
                $sum += (double) $row->{$prop};
            else
                $sum += (double) $row[$prop];
        return $sum;
    }

    public function historicoCajasSemanal(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->year) && $postdata->year != ""){
            $sYear = " AND year = '{$postdata->year}'";
        }

        $data_chart = $this->db->queryAll("SELECT label_x, sum(value) as value, name, index_y
            FROM(
                SELECT semana AS label_x, COUNT(1) AS value, finca AS name, '0' AS index_y
                FROM produccion_cajas cajas
                WHERE 1=1 $sYear
                GROUP BY id_finca, semana
            ) AS tbl
            GROUP BY label_x, name
            ORDER BY label_x");

        $groups = [
            [
                "name" => '',
                "type" => 'line',
                'format' => ''
            ]
        ];
        $response->chart = $this->grafica_z($data_chart, $groups);
        return $response;
    }

    public function getMarcas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->year) && $postdata->year != ""){
            $sYear = " AND year = '{$postdata->year}'";
        }

        $response->data = $this->db->queryAllSpecial("SELECT marca as id, marca as label FROM(
            SELECT marca
            FROM produccion_cajas
            WHERE 1=1 $sYear
            GROUP BY marca
        ) AS tbl");
        return $response;
    }

    public function getGuias(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        if($postdata->guia != "" && $postdata->fecha != ""){
            $response->marcas = $this->db->queryAll("SELECT marca, valor FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}'");
        }
        return $response;
    }

    public function tags($filters){
        $response = [];
        /*if($filters->semana > 0){
            $sWhere = " AND semana = {$filters->semana}";
        }*/
        $rows = $this->db->queryAll("SELECT hora, fecha
                                    FROM(
                                        SELECT hora, fecha
                                        FROM produccion_cajas 
                                        WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != '' $sWhere
                                        UNION ALL
                                        SELECT hora, fecha
                                        FROM produccion_gavetas 
                                        WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != '' $sWhere
                                    ) AS tbl
                                    ORDER BY hora");
        if(count($rows)>0){
            $p = $rows[0];
            $u = $rows[count($rows)-1];
            $response["primera_caja"] = $p->hora;
            $response["ultima_caja"] = $u->hora;
            $response["fecha_primera"] = $p->fecha;
            $response["fecha_ultima"] = $u->fecha;
            $response["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}}',  '{$p->hora}') AS dif")->dif;
        }else{
            $response["primera_caja"] = "";
            $response["ultima_caja"] = "";
            $response["fecha_primera"] = "";
            $response["fecha_ultima"] = "";
        }
        return $response;
    }

    public function graficasBarras(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $response->marcas = $this->db->queryAll("SELECT marca , sec, minimo, maximo
                                                FROM (
                                                    SELECT marca, 1 AS sec, minimo_{$postdata->unidad} AS minimo, maximo_{$postdata->unidad} AS maximo FROM produccion_cajas INNER JOIN produccion_tipo_caja ON nombre = marca WHERE fecha = '{$postdata->fecha_inicial}' GROUP BY marca
                                                ) AS tbl
                                                GROUP BY marca");

        $response->graficas = [];
        $response->pasteles = [];
        foreach($response->marcas as $marca){
            # BARRAS
            $response->graficas[$marca->marca] = (object)[
                "legends" => [],
                "series" => [
                    "Cantidad" => [
                        "name" => "Cantidad",
                        "connectNulls" => true,
                        "type" => "bar",
                        "itemStyle" => [
                            "normal" => [
                                "barBorderRadius" => 0,
                                "barBorderWidth" => 6
                            ]
                        ],
                        "label" => [
                            "normal" => [
                                "show" => true,
                                "position" => "inside"
                            ]
                        ],
                        "data" => [],
                    ]
                ]
            ];
            $pesos = $this->db->queryAllSpecial("SELECT peso AS id, cantidad AS label
                                                FROM(
                                                    -- SELECT caja AS peso, COUNT(1) AS cantidad FROM produccion_cajas WHERE fecha = '{$postdata->fecha_inicial}' AND marca = '{$marca->marca}' AND marca != 'PINALINDA BAG' GROUP BY caja
                                                    -- UNION ALL
                                                    SELECT grupo AS peso, SUM(cantidad) AS cantidad
                                                    FROM (
                                                        SELECT {$postdata->unidad} AS peso, COUNT(1) AS cantidad, 
                                                            IF(FLOOR({$postdata->unidad})+0.5 <= {$postdata->unidad}, 
                                                                CONCAT(
                                                                    FLOOR({$postdata->unidad})+0.5,
                                                                    '-',
                                                                    FLOOR({$postdata->unidad})+1
                                                                ), 
                                                                CONCAT(
                                                                    FLOOR({$postdata->unidad}),
                                                                    '-',
                                                                    FLOOR({$postdata->unidad})+0.5
                                                                )
                                                            ) AS grupo
                                                        FROM produccion_cajas
                                                        WHERE fecha = '{$postdata->fecha_inicial}' AND marca = '{$marca->marca}'
                                                        GROUP BY {$postdata->unidad}
                                                    ) AS tbl
                                                    GROUP BY grupo
                                                ) AS tbl");
            
            foreach($pesos as $peso => $cantidad){
                $response->graficas[$marca->marca]->legends[] = $peso;
                $response->graficas[$marca->marca]->series["Cantidad"]["umbral"] = ["max" => $marca->maximo, "min" => $marca->minimo];
                $response->graficas[$marca->marca]->series["Cantidad"]["data"][] = $cantidad;
            }
            
            # PASTELES
            $response->pasteles[$marca->marca] = [];
            $cajas1840 = $this->db->queryAllSpecial("SELECT nombre AS id, nombre AS label FROM produccion_tipo_caja WHERE maximo_{$postdata->unidad} IS NOT NULL AND minimo_{$postdata->unidad} IS NOT NULL");

            if(in_array($marca->marca, $cajas1840)){
                $sql = "SELECT value, label
                        FROM (
                            SELECT '{$marca->minimo}-{$marca->maximo}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} >= {$marca->minimo} AND {$postdata->unidad} <= {$marca->maximo}
                            UNION ALL
                            SELECT '> {$marca->maximo}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} > {$marca->maximo}
                            UNION ALL
                            SELECT '< {$marca->minimo}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} < {$marca->minimo}
                        ) AS tbl
                        WHERE value > 0";
                $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
            }else{
                $peso_prom = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM (
                    SELECT AVG({$postdata->unidad}) AS peso FROM produccion_cajas WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}'
                ) AS tbl");
                $sql = "SELECT value, label
                        FROM (
                            SELECT '{$peso_prom}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} = {$peso_prom}
                            UNION ALL
                            SELECT '> {$peso_prom}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} > {$peso_prom}
                            UNION ALL
                            SELECT '< {$peso_prom}' AS label, COUNT(1) AS value
                            FROM produccion_cajas
                            WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}' AND {$postdata->unidad} < {$peso_prom}
                        ) AS tbl";
                $response->pasteles[$marca->marca] = $this->db->queryAll($sql);
            }
        }
        return $response;
    }

    public function tablasDiferecias(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $cajas1840 = $this->db->queryAll("SELECT nombre AS marca, requerimiento_{$postdata->unidad} FROM produccion_tipo_caja WHERE status = 'Activo'");
        
        foreach($cajas1840 as $marca){
            $sql = "SELECT marca, ROUND(SUM(IF({$postdata->unidad} > {$postdata->requerimiento}, {$postdata->unidad} - {$postdata->requerimiento}, 0)), 2) AS kg_diff
                    FROM produccion_cajas
                    WHERE marca = '{$marca->marca}' AND fecha = '{$postdata->fecha_inicial}'";
            $row = $this->db->queryRow($sql);
            $row->cajas = round($row->kg_diff / $postdata->requerimiento, 2);
            $row->dolares = round($row->cajas * 6.2, 2);
            if($row->kg_diff > 0) $response->tablas[] = $row;
        }

        return $response;
    }

    public function historicoExcedente(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response->data = [];
        /*$response->umbrales = $this->db->queryRow("SELECT ROUND(AVG(kg_diff), 2) AS exce, ROUND(AVG(kg_diff) / 18.4, 2) AS cajas, ROUND(AVG(kg_diff) / 18.4 / 6.2, 2) AS dolares
            FROM (
                SELECT ROUND(SUM(IF({$postdata->unidad} > 18.4, {$postdata->unidad} - 18.4, 0)), 2) AS kg_diff FROM produccion_cajas WHERE YEAR = {$postdata->year} AND marca IN('COBANA 101', 'COBANA MADERA', 'COBANA REWE') GROUP BY marca, semana
            ) AS tbl");*/
        $marcas = $this->db->queryAllSpecial("SELECT marca AS id, marca AS label FROM produccion_cajas WHERE YEAR = {$postdata->year} AND {$postdata->unidad} > (SELECT requerimiento_{$postdata->unidad} FROM produccion_tipo_caja WHERE nombre = marca) AND marca IN(SELECT nombre FROM produccion_tipo_caja WHERE status = 'Activo') GROUP BY marca");
        $semanas = $this->db->queryAllSpecial("SELECT semana AS id, semana AS label FROM(
                SELECT semana
                FROM produccion_cajas
                WHERE YEAR = {$postdata->year} AND {$postdata->unidad} > (SELECT requerimiento_{$postdata->unidad} FROM produccion_tipo_caja WHERE nombre = marca) AND marca IN (SELECT nombre FROM produccion_tipo_caja WHERE status = 'Activo')
                GROUP BY semana
            ) AS tbl
            GROUP BY semana");
        $response->semanas = $semanas;

        $max_exce = []; $max_cajas = []; $max_dolares = [];
        $total = new stdClass;
        foreach($marcas as $marca){
            $row = new stdClass;
            $row->marca = $marca;
            $row->requerimiento = $this->db->queryOne("SELECT requerimiento_{$postdata->unidad} FROM produccion_tipo_caja WHERE nombre = '{$row->marca}'");
            foreach($semanas as $semana){
                $val = $this->db->queryOne("SELECT ROUND(SUM(IF({$postdata->unidad} > {$row->requerimiento}, {$postdata->unidad} - {$row->requerimiento}, 0)), 2) FROM produccion_cajas WHERE YEAR = {$postdata->year} AND marca = '{$marca}' AND semana = {$semana}");
                $row->{"sem_exce_{$semana}"} = $val;
                $total->{"sem_exce_{$semana}"} += $val;
                $row->{"sem_cajas_{$semana}"} = round($val / $row->requerimiento, 2);
                $total->{"sem_cajas_{$semana}"} += round($val / $row->requerimiento, 2);
                $row->{"sem_dolares_{$semana}"} = round($row->{"sem_cajas_{$semana}"} * 6.2, 2);
                $total->{"sem_dolares_{$semana}"} += round($row->{"sem_cajas_{$semana}"} * 6.2, 2);

                if($val > 0) if(!isset($row->{"min_exce"}) || ($row->{"min_exce"} > $val)) $row->{"min_exce"} = $val;
                if($val > 0) if(!isset($row->{"max_exce"}) || ($row->{"max_exce"} < $val)) $row->{"max_exce"} = $val;
                $row->{"sum_exce"} += $val;
                $total->{"sum_exce"} += $val;

                if($val > 0) if((!isset($row->{"min_cajas"}) && $val > 0) || ($row->{"min_cajas"} > $row->{"sem_cajas_{$semana}"})) $row->{"min_cajas"} = $row->{"sem_cajas_{$semana}"};
                if($val > 0) if((!isset($row->{"max_cajas"}) && $val > 0) || ($row->{"max_cajas"} < $row->{"sem_cajas_{$semana}"})) $row->{"max_cajas"} = $row->{"sem_cajas_{$semana}"};
                $row->{"sum_cajas"} += $row->{"sem_cajas_{$semana}"};
                $total->{"sum_cajas"} += $row->{"sem_cajas_{$semana}"};

                if($val > 0) if(!isset($row->{"min_dolares"}) || ($row->{"min_dolares"} > $row->{"sem_dolares_{$semana}"})) $row->{"min_dolares"} = $row->{"sem_dolares_{$semana}"};
                if($val > 0) if(!isset($row->{"max_dolares"}) || ($row->{"max_dolares"} < $row->{"sem_dolares_{$semana}"})) $row->{"max_dolares"} = $row->{"sem_dolares_{$semana}"};
                $row->{"sum_dolares"} += $row->{"sem_dolares_{$semana}"};
                $total->{"sum_dolares"} += $row->{"sem_dolares_{$semana}"};

                if($val > 0) $row->{"count"} += 1;
            }
            $row->{"avg_exce"} = round($row->{"sum_exce"} / $row->{"count"}, 2);
            $row->{"avg_cajas"} = round($row->{"sum_cajas"} / $row->{"count"}, 2);
            $row->{"avg_dolares"} = round($row->{"sum_dolares"} / $row->{"count"}, 2);

            $response->data[] = $row;
        }
        $total->marca = 'TOTAL';
        foreach($total as $key => $value){
            if(strpos($key, "sem_exce") !== false){
                if($value > 0) if(!isset($total->{"max_exce"}) || $total->{"max_exce"} < $value) $total->{"max_exce"} = $value;
                if($value > 0) if(!isset($total->{"min_exce"}) || $total->{"min_exce"} > $value) $total->{"min_exce"} = $value;
            }
            if(strpos($key, "sem_cajas") !== false){
                if($value > 0) if(!isset($total->{"max_cajas"}) || $total->{"max_cajas"} < $value) $total->{"max_cajas"} = $value;
                if($value > 0) if(!isset($total->{"min_cajas"}) || $total->{"min_cajas"} > $value) $total->{"min_cajas"} = $value;
            }
            if(strpos($key, "sem_dolares") !== false){
                if($value > 0) if(!isset($total->{"max_dolares"}) || $total->{"max_dolares"} < $value) $total->{"max_dolares"} = $value;
                if($value > 0) if(!isset($total->{"min_dolares"}) || $total->{"min_dolares"} > $value) $total->{"min_dolares"} = $value;
            }
        }
        $total->{"avg_exce"} = round($total->{"sum_exce"} / count($semanas), 2);
        $total->{"avg_cajas"} = round($total->{"sum_cajas"} / count($semanas), 2);
        $total->{"avg_dolares"} = round($total->{"sum_dolares"} / count($semanas), 2);
        $response->data[] = $total;
        return $response;
    }

    private function grafica_z($data = [], $group_y = []){
        $options = [];
        $options["tooltip"] = [
            "trigger" => 'axis',
            "axisPointer" => [
                "type" => 'cross',
                "crossStyle" => [
                    "color" => '#999'
                ]
            ]
        ];
        $options["toolbox"] = [
            "feature" => [
                "dataView" => [
                    "show" => true,
                    "readOnly" => false
                ],
                "magicType" => [
                    "show" => true,
                    "type" => ['line', 'bar']
                ],
                "restore" => [
                    "show" => true
                ],
                "saveAsImage" => [
                    "show" => true
                ]
            ]
        ];
        $options["legend"]["data"] = [];
        $options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["xAxis"] = [
            [
                "type" => 'category',
                "data" => [],
                "axisPointer" => [
                    "type" => 'shadow'
                ]
            ]
        ];
        /*
            [
                type => 'value',
                name => {String},
                min => 0,
                max => 200,
                interval => 5,
                axisLabel => [
                    formatter => {value} KG
                ]
            ]
        */
        $options["yAxis"] = [];
        /*
            [
                name => {String},
                type => 'line',
                data => [
                    {double}, {double}, {double}
                ]
            ]
        */
        $options["series"] = [];

        $maxs = [];
        $mins = [];
        $prepare_data = [];
        $_x = [];
        $_names = [];
        $_namess = [];
        foreach($data as $d){
            $d = (object) $d;
            if(!isset($maxs[$d->index_y])) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;
            if($d->value > $maxs[$d->index_y]) if($d->value > 0)
                $maxs[$d->index_y] = $d->value;

            if(!isset($mins[$d->index_y])) if($d->value > 0)
                $mins[$d->index_y] = $d->value;
            if($d->value < $mins[$d->index_y]) if($d->value > 0)
                $mins[$d->index_y] = $d->value;

            if(!in_array($d->label_x, $_x)){
                $_x[] = $d->label_x;
            }
            if(!in_array($d->name, $_namess)){
                $_namess[] = $d->name;
                 
                $n = ["name" => $d->name, "group" => $d->index_y];
                if(isset($d->line)){
                    $n["line"] = $d->line;
                }
                $_names[] = $n;
            }
            $prepare_data[$d->label_x][$d->name] = $d->value;
        }

        foreach($group_y as $key => $col){
            $col = (object) $col;
            $options["yAxis"][] = [
                'type' => 'value',
                'name' => $col->name,
                'min' => 'dataMin',
                'axisLabel' => [
                    'formatter' => "{value} $col->format"
                ]
            ];
        }

        foreach($_x as $row){
            $options["xAxis"][0]["data"][] = $row;
        }

        foreach($_names as $name){
            $name = (object) $name;

            if(!in_array($name->name, $options["legend"]["data"]))
                $options["legend"]["data"][] = $name->name;

            $serie = [
                "name" => $name->name,
                "type" => 'line',
                "connectNulls" => true,
                "data" => []
            ];
            if($name->group > 0)
                $serie["yAxisIndex"] = $name->group;

            if(isset($name->line)){
                $serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
            }

            foreach($_x as $row){
                $val = 0;
                if(isset($prepare_data[$row][$name->name]))
                    $val = $prepare_data[$row][$name->name];

                if($val > 0)
                    $serie["data"][] = $val;
                else
                    $serie["data"][] = null;
            }
            $options["series"][] = $serie;
        }

        return $options;
    }
}
