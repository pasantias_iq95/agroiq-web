<?php defined('PHRAPI') or die("Direct access not allowed!");

class Asistencia {
    private $db;
    private $config;
    private $token;
    private $Service;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function index(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;

        $response->trabajadores = $this->db->queryAll("SELECT * FROM personal WHERE status = 1 ORDER BY nombre");

        if(isset($postdata->id) && $postdata->id > 0){
            $response->data = $this->db->queryRow("SELECT asistencia_detalle.*,  personal.cedula
                FROM asistencia_detalle  
                INNER JOIN personal ON personal.id = id_personal
                WHERE asistencia_detalle.id = '{$postdata->id}'");
            $response->data->lotes = $this->db->queryAllSpecial("SELECT lote as id, lote as label FROM asistencia_detalle_lotes WHERE id_asistencia_detalle = '{$postdata->id}'");
        }
        return $response;
    }

    public function save(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "id" => getValueFrom($postdata, "id", ""),
            "fecha" => getValueFrom($postdata, "fecha", ""),
            "hora" => getValueFrom($postdata, "hora", ""),
            "administrador" => getValueFrom($postdata, "administrador", ""),
            "id_personal" => getValueFrom($postdata, "id_personal", ""),
            "personal" => getValueFrom($postdata, "personal", ""),
            "hectareas" => getValueFrom($postdata, "hectareas", ""),
            "finca" => getValueFrom($postdata, "finca", ""),
            "hectareas" => getValueFrom($postdata, "hectareas", ""),
            "lotes" => getValueFrom($postdata, "lotes", ""),
            "observaciones" => getValueFrom($postdata, "observaciones", ""),
        ];

        if($data->id > 0){
            $sql = "UPDATE asistencia_detalle
                    SET
                        fecha = '{$data->fecha}',
                        hora = '{$data->hora}',
                        administrador = '{$data->administrador}',
                        finca = '{$data->finca}',
                        id_personal = '{$data->id_personal}',
                        personal = (SELECT nombre FROM personal WHERE id = '{$data->id_personal}'),
                        cargo = (SELECT cargo FROM personal WHERE id = '{$data->id_personal}'),
                        perfil = (SELECT perfil FROM personal WHERE id = '{$data->id_personal}'),
                        hectareas = '{$data->hectareas}',
                        observaciones = '{$data->observaciones}'
                    WHERE id = {$data->id}";
            $this->db->query($sql);
            $this->db->query("DELETE FROM asistencia_detalle_lotes WHERE id_asistencia_detalle = '{$data->id}'");
            foreach(explode(",", $data->lotes) as $lote){
                $this->db->query("INSERT INTO asistencia_detalle_lotes SET lote = '{$lote}', id_asistencia_detalle = '{$data->id}'");
            }
        }else{
            $sql = "INSERT INTO asistencia_detalle
                    SET
                        fecha = '{$data->fecha}',
                        hora = '{$data->hora}',
                        administrador = '{$data->administrador}',
                        finca = '{$data->finca}',
                        id_personal = '{$data->id_personal}',
                        personal = (SELECT nombre FROM personal WHERE id = '{$data->id_personal}'),
                        cargo = (SELECT cargo FROM personal WHERE id = '{$data->id_personal}'),
                        perfil = (SELECT perfil FROM personal WHERE id = '{$data->id_personal}'),
                        hectareas = '{$data->hectareas}',
                        observaciones = '{$data->observaciones}'";
            $this->db->query($sql);
            foreach(explode(",", $data->lotes) as $lote){
                $this->db->query("INSERT INTO asistencia_detalle_lotes SET lote = '{$lote}', id_asistencia_detalle = '{$data->id}'");
            }
        }
        return true;
    }
}