<?php defined('PHRAPI') or die("Direct access not allowed!");

class Bonif {
	public $name;
	private $db;
	private $config;
	private $companies;
	private $string;
    private $Graficas;

	public function __construct(){
        header("Content-type: application/json; charset=UTF-8");
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
		$this->companies = [2,7];
		$this->string = "Total Peso";

        $Factory = new Factory();
        $this->Graficas = $Factory->Graficas;
	}

    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = (object)[
            "fecha_inicial" => getValueFrom($postdata, 'fecha_inicial', ''),
            "fecha_final" => getValueFrom($postdata, 'fecha_final', ''),
            "variable" => getValueFrom($postdata, 'variable', 'Daños'),
            "labor" => getValueFrom($postdata, 'labor', 'COSECHA'),
            "lote" => getValueFrom($postdata, 'lote', ''),
            "finca" => getValueFrom($postdata, 'idFinca', ""),
            "semana" => getValueFrom($postdata, 'semana', ""),
            "order" => getValueFrom($postdata, 'order', "semana"),
            "umbral" => getValueFrom($postdata, 'umbral', 10),
            "year" => getValueFrom($postdata, 'year', 0),
        ];
        return $response;
    }

    public function filters(){
        $response = new stdClass;
        $filters = $this->params();

        $response->fincas = $this->db->queryAllSpecial("SELECT finca AS id, finca AS label FROM merma WHERE year = {$filters->year} GROUP BY finca");
        $response->semanas = $this->db->queryAll("SELECT semana FROM merma WHERE year = {$filters->year} $sWhere GROUP BY semana");
        $response->umbrals = $this->db->queryRow("SELECT tipo AS id, umbral AS label FROM bonificacion_conts WHERE mode2 = '{$filters->variable}' AND UPPER(tipo) = '{$filters->labor}'")->label;
        return $response;
    }

    public function saveUmbral(){
        $response = new stdClass;
        $filters = $this->params();
        $response->status = 400;
        
        if($this->db->query("UPDATE bonificacion_conts SET umbral = $filters->umbral WHERE tipo = '{$filters->labor}' AND mode2 = '{$filters->variable}'")){
            $response->status = 200;
            $response->umbral = $filters->umbral;
        }else{
            $response->umbral = $this->db->queryOne("SELECT umbral FROM bonificacion_conts WHERE tipo = '{$filters->labor}' AND mode2 = '{$filters->variable}'");
        }
        return $response;
    }

	public function getHistoricoSemanal(){
        $response = new stdClass;

        /*if($filters->variable == 'Peso'){
            // Peso Prom
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = YEAR(CURRENT_DATE) AND type = '{$filters->labor}' AND semana = tbl.semana AND bloque = tbl.bloque AND campo LIKE 'Total Peso%' AND cantidad > 0 $sWhere) 
                        / (SELECT SUM(racimos_procesados) FROM merma WHERE year = YEAR(CURRENT_DATE) AND semana = tbl.semana AND bloque = tbl.bloque $sWhere)";
        }else if($filters->variable == 'Merma'){
            // % Merma
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = YEAR(CURRENT_DATE) AND type = '{$filters->labor}' AND semana = tbl.semana AND bloque = tbl.bloque AND campo LIKE 'Total Peso%' AND cantidad > 0 $sWhere)
                        / (SELECT SUM(peso_neto) 
                            FROM merma 
                            WHERE year = YEAR(CURRENT_DATE) AND semana = tbl.semana AND bloque = tbl.bloque $sWhere)
                        * 100";
        }else if($filters->variable == 'Dedos'){
            // Dedos Prom
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = YEAR(CURRENT_DATE) AND type = '{$filters->labor}' AND semana = tbl.semana AND bloque = tbl.bloque AND campo LIKE 'Total Peso%' AND cantidad > 0 $sWhere) /
                        (SELECT SUM(racimos_procesados) FROM merma WHERE year = YEAR(CURRENT_DATE) AND semana = tbl.semana AND bloque = tbl.bloque $sWhere) * 5";
        }

        $sql = "SELECT bloque, SUM(cantidad) AS cantidad
                FROM merma main
                INNER JOIN merma_detalle ON id_merma = main.id
                WHERE year = YEAR(CURRENT_DATE) AND type = '{$filters->labor}' AND cantidad > 0 $sWhere
                GROUP BY bloque
                HAVING cantidad > 0
                ORDER BY bloque";
        $bloques = $this->db->queryAll($sql);

        foreach($bloques as $row){
            $sql = "SELECT ROUND({$calculo}, 2) AS label, semana AS id
                    FROM(
                        SELECT '{$row->bloque}' AS bloque, semana
                        FROM merma main
                        INNER JOIN merma_detalle ON id_merma = main.id
                        WHERE year = YEAR(CURRENT_DATE) 
                            AND bloque = $row->bloque $sWhere
                            AND type = '{$filters->labor}'
                            AND cantidad > 0
                        GROUP BY semana
                    ) AS tbl";
            $data = [
                "lote" => $row->bloque,
                "detalle" => $this->db->queryAllSpecial($sql)
            ];
            $response->data[] = $data;

            foreach($data["detalle"] as $sem => $val){
                $d_c = [
                    "legend" => 'L'.$row->bloque,
                    "label" => $sem,
                    "selected" => 1,
                    "value" => $val
                ];
                $data_chart[] = $d_c;
            }
        }*/

        $filters = $this->params();
        if($filters->labor != "COSECHA"){
            $response = $this->getSemanalLote();
        }else{
            $response = $this->getSemanalPalanca();
        }
        $response->chart = $this->chartInit($response->data_chart, "vertical", '', "line");
        return $response;
    }

    private function getSemanalLote(){
        $response = new stdClass;

        $calculo = "";
        $response->data = [];
        $data_chart = [];

        $filters = $this->params();
        if($filters->finca != ""){
            $sWhere .= " AND finca = '{$filters->finca}'";
        }

        if($filters->finca == ""){
            if($filters->variable == 'Peso'){
                // Peso Prom
                $calculo = "SUM(peso_merma) / (SELECT SUM(racimos_procesados) FROM merma_resumen_finca_lote_semana WHERE semana = tbl.semana AND lote = tbl.lote)";
            }else if($filters->variable == '% Merma'){
                // % Merma
                $calculo = "SUM(peso_merma)
                            / (SELECT SUM(peso_neto) 
                                FROM merma 
                                WHERE year = {$filters->year} AND semana = tbl.semana AND bloque = tbl.bloque $sWhere)
                            * 100";
            }else if($filters->variable == 'Daños'){
                // Dedos Prom
                $calculo = "SUM(peso_merma) / (SELECT SUM(racimos_procesados) FROM merma_resumen_finca_lote_semana WHERE semana = tbl.semana AND lote = tbl.lote) * 5";
            }
        }else{
            $calculo = "SUM(valor)";
        }
        
        $semanas = $this->db->queryAll("SELECT semana
                                    FROM merma_resumen_finca_lote_semana_labor
                                    WHERE anio = {$filters->year} AND labor = '{$filters->labor}' AND valor > 0 $sWhere
                                    GROUP BY semana
                                    ORDER BY semana");
                                    
        foreach($semanas as $row){
            $detalle = "SELECT lote AS id, ROUND({$calculo}, 2) AS label
                        FROM merma_resumen_finca_lote_semana_labor tbl
                        WHERE labor = '{$filters->labor}'
                            AND semana = $row->semana
                            AND valor > 0
                            AND tipo = '{$filters->variable}' $sWhere
                        GROUP BY lote";
            $data = [
                "semana" => $row->semana,
                "detalle" => $this->db->queryAllSpecial($detalle)
            ];
            $response->data_semana[] = $data;
        }

        /*UMBRAL GRAFICA*/
        $umbral = $this->db->queryRow("SELECT tipo AS id, umbral AS label FROM bonificacion_conts WHERE mode2 = '{$filters->variable}' AND UPPER(tipo) = '{$filters->labor}'")->label;
        if(!$umbral > 0)
            $umbral = 10;
        $position_lotes = [];
        foreach($semanas as $row){
            $d_c = [
                "legend" => 'Umbral',
                "label" => $row->semana,
                "selected" => 1,
                "value" => $umbral,
                "position" => 0
            ]; 
            $data_chart[] = $d_c;

            $fullLotes = $this->db->queryAll("SELECT lote FROM merma_resumen_finca_lote_semana_labor WHERE anio = {$filters->year} AND labor = '{$filters->labor}' GROUP BY lote");
            foreach($fullLotes as $l){
                $val = $this->db->queryRow("SELECT valor
                                        FROM merma_resumen_finca_lote_semana_labor
                                        WHERE anio = {$filters->year} AND labor = '{$filters->labor}' AND semana = $row->semana AND valor > 0 $sWhere AND lote = '{$l->lote}'")->valor;
                if(!in_array($l->lote, $position_lotes))
                    $position_lotes[] = $l->lote;

                $d_c = [
                    "legend" => 'L'.$l->lote,
                    "label" => $row->semana,
                    "selected" => 1,
                    "value" => $val,
                    "position" => array_search($l->lote, $position_lotes) + 1
                ];
                $data_chart[] = $d_c;
            }
        }

        /*TABLA PARA IMPRIMIR*/
        $lotes = $this->db->queryAll("SELECT lote
                                    FROM merma_resumen_finca_lote_semana_labor
                                    WHERE anio = {$filters->year} AND labor = '{$filters->labor}' AND valor > 0 $sWhere
                                    GROUP BY lote
                                    ORDER BY lote");
        foreach($lotes as $row){
            $detalle = "SELECT semana AS id, ROUND({$calculo}, 2) AS label
                        FROM merma_resumen_finca_lote_semana_labor tbl
                        WHERE labor = '{$filters->labor}'
                            AND lote = '$row->lote'
                            AND valor > 0
                            AND tipo = '{$filters->variable}' $sWhere
                        GROUP BY semana";
            $data = [
                "lote" => $row->lote,
                "detalle" => $this->db->queryAllSpecial($detalle),
                "defectos" => $this->getDanos([
                    "year" => $filters->year,
                    "finca" => $filters->finca,
                    "variable" => $filters->variable,
                    "labor" => $filters->labor,
                    "lote" => $row->lote
                ])->data
            ];

            foreach($data["detalle"] as $semana => $val){
                if(!isset($response->totales[$row->lote]["max"]) || $response->totales[$row->lote]["max"] < $val)
                    $response->totales[$row->lote]["max"] = $val;
                if(!isset($response->totales[$row->lote]["min"]) || $response->totales[$row->lote]["min"] > $val)
                    $response->totales[$row->lote]["min"] = $val;

                $response->totales[$row->lote]["avg"] = $response->totales[$row->lote]["avg"] + 1;
                $response->totales[$row->lote]["sum"] += $val;
            }
            $response->data[] = $data;
        }
        
        foreach($response->totales as $tot){
            $tot["avg"] = ($tot["sum"] / $tot["avg"]);
        }

        $response->data_chart = $data_chart;
        return $response;
    }

    private function getSemanalPalanca(){
        $response = new stdClass;

        $calculo = "";
        $response->data = [];
        $data_chart = [];

        $filters = $this->params();
        if($filters->finca != ""){
            $sWhere .= " AND finca = '{$filters->finca}'";
        }

        if($filters->finca == ""){
            if($filters->variable == 'Peso'){
                // Peso Prom
                $calculo = "SUM(peso_merma) / (SELECT SUM(racimos_procesados) FROM merma_resumen_finca_lote_semana WHERE semana = tbl.semana AND lote = tbl.lote)";
            }else if($filters->variable == '% Merma'){
                // % Merma
                $calculo = "SUM(peso_merma)
                            / (SELECT SUM(peso_neto) 
                                FROM merma 
                                WHERE year = {$filters->year} AND semana = tbl.semana AND bloque = tbl.bloque $sWhere)
                            * 100";
            }else if($filters->variable == 'Daños'){
                // Dedos Prom
                $calculo = "SUM(peso_merma) / (SELECT SUM(racimos_procesados) FROM merma_resumen_finca_lote_semana WHERE semana = tbl.semana AND lote = tbl.lote) * 5";
            }
        }else{
            $calculo = "SUM(valor)";
        }
        
        $semanas = $this->db->queryAll("SELECT semana
                                    FROM merma_resumen_finca_lote_semana_labor
                                    WHERE anio = {$filters->year} AND labor = '{$filters->labor}' AND valor > 0 $sWhere
                                    GROUP BY semana
                                    ORDER BY semana");
        foreach($semanas as $row){
            $detalle = "SELECT lote AS id, ROUND({$calculo}, 2) AS label
                        FROM merma_resumen_finca_lote_semana_labor tbl
                        WHERE labor = '{$filters->labor}'
                            AND semana = $row->semana
                            AND valor > 0
                            AND tipo = '{$filters->variable}' $sWhere
                        GROUP BY lote";
            $data = [
                "semana" => $row->semana,
                "detalle" => $this->db->queryAllSpecial($detalle)
            ];
            $response->data_semana[] = $data;
        }

        /*UMBRAL GRAFICA*/
        $umbral = $this->db->queryRow("SELECT tipo AS id, umbral AS label FROM bonificacion_conts WHERE mode2 = '{$filters->variable}' AND UPPER(tipo) = '{$filters->labor}'")->label;
        if(!$umbral > 0)
            $umbral = 10;
        foreach($semanas as $row){
            $d_c = [
                "legend" => 'Umbral',
                "label" => $row->semana,
                "selected" => 1,
                "value" => $umbral
            ]; 
            $data_chart[] = $d_c;
        }

        /*TABLA PARA IMPRIMIR*/
        $lotes = $this->db->queryAll("SELECT lote
                                    FROM merma_resumen_finca_lote_semana_labor
                                    WHERE anio = {$filters->year} AND labor = '{$filters->labor}' AND valor > 0 $sWhere
                                    GROUP BY lote
                                    ORDER BY lote");
        foreach($lotes as $row){
            $detalle = "SELECT semana AS id, ROUND({$calculo}, 2) AS label
                        FROM merma_resumen_finca_lote_semana_labor tbl
                        WHERE labor = '{$filters->labor}'
                            AND lote = '$row->lote'
                            AND valor > 0
                            AND tipo = '{$filters->variable}' $sWhere
                        GROUP BY semana";
            $data = [
                "lote" => $row->lote,
                "detalle" => $this->db->queryAllSpecial($detalle),
                "defectos" => $this->getDanos([
                    "year" => $filters->year,
                    "finca" => $filters->finca,
                    "variable" => $filters->variable,
                    "labor" => $filters->labor,
                    "lote" => $row->lote
                ])->data
            ];

            foreach($data["detalle"] as $semana => $val){
                if(!isset($response->totales[$row->lote]["max"]) || $response->totales[$row->lote]["max"] < $val)
                    $response->totales[$row->lote]["max"] = $val;
                if(!isset($response->totales[$row->lote]["min"]) || $response->totales[$row->lote]["min"] > $val)
                    $response->totales[$row->lote]["min"] = $val;

                $response->totales[$row->lote]["avg"] = $response->totales[$row->lote]["avg"] + 1;
                $response->totales[$row->lote]["sum"] += $val;

                $d_c = [
                    "legend" => 'L'.$row->lote,
                    "label" => $semana,
                    "selected" => 1,
                    "value" => $val
                ];
                $data_chart[] = $d_c;
            }
            $response->data[] = $data;
        }
        
        foreach($response->totales as $tot){
            $tot["avg"] = ($tot["sum"] / $tot["avg"]);
        }

        $response->data_chart = $data_chart;
        return $response;
    }

    public function getDanos($params = []){
        $response = new stdClass;
        $filters = count($params) > 0 ? (object) $params : $this->params();

        if($filters->finca != ""){
            $sWhere .= " AND finca = '{$filters->finca}'";
        }

        $daños = "";
        if($filters->finca == "" || $filters->finca != ''){
            if($filters->variable == 'Peso'){
                // Peso Prom
                $daños = "SUM(peso_merma) / (SELECT SUM(racimos_procesados) FROM merma_resumen_finca_lote_semana WHERE semana = tbl.semana AND lote = tbl.lote $sWhere)";
            }else if($filters->variable == '% Merma'){
                // % Merma
                $daños = "SUM(peso_merma)
                            / (SELECT SUM(peso_neto) 
                                FROM merma 
                                WHERE year = {$filters->year} AND semana = tbl.semana AND bloque = tbl.lote AND labor = tbl.labor AND daño = tbl.daño AND finca = tbl.finca $sWhere)
                            * 100";
            }else if($filters->variable == 'Daños'){
                // Dedos Prom
                $daños = "SUM(peso_merma) / (SELECT SUM(racimos_procesados) FROM merma_resumen_finca_lote_semana WHERE semana = tbl.semana AND lote = tbl.lote $sWhere) * 5";
            }            
        }else{
            $daños = "valor";
        }

        $where = ($filters->semana > 0 ? "AND semana = {$filters->semana}" : "AND lote = '{$filters->lote}'");
        $campo = ($filters->semana > 0) ? 'lote' : 'semana';

        $sql = "SELECT daño AS campo
                FROM merma_resumen_finca_lote_semana_labor_daño
                WHERE anio = {$filters->year} AND labor = '{$filters->labor}' AND valor > 0 AND daño != 'Cantidad Dedos con Defectos' $sWhere $where
                GROUP BY daño";
                
        $defectos = $this->db->queryAll($sql);
        
        foreach($defectos as $row){
            $sql = "SELECT ROUND({$daños}, 2) AS label, {$campo} AS id
                    FROM merma_resumen_finca_lote_semana_labor_daño tbl
                    WHERE anio = {$filters->year} 
                        AND labor = '{$filters->labor}'
                        AND daño = '{$row->campo}'
                        AND tipo = '{$filters->variable}'
                        AND valor > 0 $sWhere $where
                    GROUP BY {$campo}";
                    #D($sql);
            $response->data[] = [
                "defecto" => str_replace("Peso ", "", $row->campo),
                "danos" => $this->db->queryAllSpecial($sql)
            ];
        }
        return $response;
    }

    public function getDiario(){
        $response = new stdClass;
        $filters = $this->params();
        if($filters->labor != "COSECHA"){
            $response = $this->getDiarioModeLote();
        }else{
            $response = $this->getDiarioModePalanca();
        }
        return $response;
    }

    private function getDiarioModeLote(){
        $response = new stdClass;
        $filters = $this->params();
        //lotes por dia
        $calculo = $this->getCalculo($filters->year, $filters->variable, $filters->labor, "", $filters->finca);
        if($filters->finca != ""){
            $sWhere .= " AND finca = '{$filters->finca}'";
        }
        $sql = "SELECT bloque AS lote,
                    IFNULL((SELECT nombre FROM personal WHERE id = (SELECT idPersonal FROM lotelabor_personal WHERE idLote = @idLote AND idLabor = @idLabor)),'POR CONFIRMAR') AS enfundador                    
                FROM merma
                WHERE semana = {$filters->semana} AND year = {$filters->year} $sWhere
                GROUP BY bloque";
                
        $lotes = $this->db->queryAll($sql);
        $sql = "SELECT DAY(date_fecha) AS dia,
                        CASE DAYOFWEEK(date_fecha)
                            WHEN 1 THEN 'DOM'
                            WHEN 2 THEN 'LUN'
                            WHEN 3 THEN 'MAR'
                            WHEN 4 THEN 'MIE'
                            WHEN 5 THEN 'JUE'
                            WHEN 6 THEN 'VIE'
                            WHEN 7 THEN 'SAB'
                        END AS nombre,
                        CONCAT(CASE DAYOFWEEK(date_fecha)
                            WHEN 1 THEN 'DOM'
                            WHEN 2 THEN 'LUN'
                            WHEN 3 THEN 'MAR'
                            WHEN 4 THEN 'MIE'
                            WHEN 5 THEN 'JUE'
                            WHEN 6 THEN 'VIE'
                            WHEN 7 THEN 'SAB'
                        END, ', ', DAY(date_fecha), ' ', 
                        CASE MONTH(date_fecha)
                            WHEN 1 THEN 'ENE'
                            WHEN 2 THEN 'FEB'
                            WHEN 3 THEN 'MAR'
                            WHEN 4 THEN 'ABR'
                            WHEN 5 THEN 'MAY'
                            WHEN 6 THEN 'JUN'
                            WHEN 7 THEN 'JUL'
                            WHEN 8 THEN 'AGO'
                            WHEN 9 THEN 'SEP'
                            WHEN 10 THEN 'OCT'
                            WHEN 11 THEN 'NOV'
                            WHEN 12 THEN 'DIC'
                        END) AS fullname,
                        date_fecha AS fecha

                FROM merma
                WHERE semana = {$filters->semana} AND year = {$filters->year}
                GROUP BY DAY(date_fecha)
                ORDER BY date_fecha";
                
        $dias = $this->db->queryAll($sql);
        $response->dias = $dias;

        foreach($lotes as $lote){
            $lote->detalle = [];
            foreach($dias as $dia){
                /* promedop de cada dia */
                $lote->detalle[$dia->nombre] = $this->db->queryOne("SELECT ROUND({$calculo}, 2) AS value
                                                                     FROM (
                                                                        SELECT '{$lote->lote}' AS bloque, '{$dia->fecha}' AS date_fecha
                                                                     ) AS tbl");
                $lote->defectos = $this->getNivelDefectos($filters->year, $lote->lote, $filters->labor, $filters->semana, $dias, $filters->variable);
            }
            /*Prom semana de cada lote*/
            $calculo_lote = str_replace("date_fecha = tbl.date_fecha", "semana = '{$filters->semana}'", $calculo);
            $lote->prom_lote = $this->db->queryRow("SELECT ROUND({$calculo_lote}, 2) AS value
                                                    FROM (
                                                    SELECT '{$lote->lote}' AS bloque
                                                    ) AS tbl")->value;
        }

        foreach($dias as $dia){
            /* Promedio de cada dia */
            $calculo_dia = str_replace("AND bloque = tbl.bloque", "", $calculo);
            $response->totales[$dia->nombre] = $this->db->queryOne("SELECT ROUND({$calculo_dia}, 2) AS value
                                                    FROM (
                                                    SELECT '{$dia->fecha}' AS date_fecha
                                                    ) AS tbl");
        }
        /* Promedio semana de todos los lotes */
        $calculo_lote = str_replace("AND bloque = tbl.bloque", "", $calculo);
        $calculo_lote = str_replace("date_fecha = tbl.date_fecha", "semana = '{$filters->semana}'", $calculo_lote);
        $response->totales["prom_lote"] = $this->db->queryRow("SELECT ROUND({$calculo_lote}, 2) AS value
                                                                FROM (
                                                                SELECT '{$lote->lote}' AS bloque
                                                                ) AS tbl")->value;

        $response->data = $lotes;
        return $response;
    }

    private function getDiarioModePalanca(){
        $response = new stdClass;
        $filters = $this->params();
        //palanca por dia
        $calculo = str_replace("AND bloque = tbl.bloque","AND palanca = tbl.palanca", $this->getCalculo($filters->year, $filters->variable, $filters->labor));
        $sql = "SELECT palanca as lote,
                    IFNULL((SELECT nombre FROM personal WHERE id = (SELECT idPersonal FROM lotelabor_personal WHERE idLote = @idLote AND idLabor = @idLabor)),'POR CONFIRMAR') AS enfundador                    
                FROM merma
                WHERE semana = {$filters->semana} AND year = {$filters->year}
                GROUP BY palanca";
        $palancas = $this->db->queryAll($sql);
        $sql = "SELECT DAY(date_fecha) AS dia,
                        CASE DAYOFWEEK(date_fecha)
                            WHEN 1 THEN 'DOM'
                            WHEN 2 THEN 'LUN'
                            WHEN 3 THEN 'MAR'
                            WHEN 4 THEN 'MIE'
                            WHEN 5 THEN 'JUE'
                            WHEN 6 THEN 'VIE'
                            WHEN 7 THEN 'SAB'
                        END AS nombre,
                        CONCAT(CASE DAYOFWEEK(date_fecha)
                            WHEN 1 THEN 'DOM'
                            WHEN 2 THEN 'LUN'
                            WHEN 3 THEN 'MAR'
                            WHEN 4 THEN 'MIE'
                            WHEN 5 THEN 'JUE'
                            WHEN 6 THEN 'VIE'
                            WHEN 7 THEN 'SAB'
                        END, ', ', DAY(date_fecha), ' ', 
                        CASE MONTH(date_fecha)
                            WHEN 1 THEN 'ENE'
                            WHEN 2 THEN 'FEB'
                            WHEN 3 THEN 'MAR'
                            WHEN 4 THEN 'ABR'
                            WHEN 5 THEN 'MAY'
                            WHEN 6 THEN 'JUN'
                            WHEN 7 THEN 'JUL'
                            WHEN 8 THEN 'AGO'
                            WHEN 9 THEN 'SEP'
                            WHEN 10 THEN 'OCT'
                            WHEN 11 THEN 'NOV'
                            WHEN 12 THEN 'DIC'
                        END) AS fullname,
                        date_fecha AS fecha
                FROM merma
                WHERE semana = {$filters->semana} AND year = {$filters->year}
                GROUP BY DAY(date_fecha)
                ORDER BY date_fecha";
        $dias = $this->db->queryAll($sql);
        $response->dias = $dias;

        foreach($palancas as $palanca){
            $palanca->detalle = [];
            foreach($dias as $dia){
                /* promedop de cada dia */
                $palanca->detalle[$dia->nombre] = $this->db->queryRow("SELECT ROUND({$calculo}, 2) AS value
                                                                     FROM (
                                                                        SELECT palanca, '{$dia->fecha}' AS date_fecha
                                                                        FROM merma
	                                                                    WHERE palanca = '{$palanca->lote}' 
                                                                        LIMIT 1
                                                                     ) AS tbl")->value;
                $palanca->defectos = $this->getNivelDefectosPalanca($filters->year, $palanca->lote, $filters->labor, $filters->semana, $dias, $filters->variable);
            }
            /*Prom semana de cada lote*/
            $calculo_lote = str_replace("date_fecha = tbl.date_fecha", "semana = '{$filters->semana}'", $calculo);
            $palanca->prom_lote = $this->db->queryOne("SELECT ROUND({$calculo_lote}, 2) AS value
                                                    FROM (
                                                        SELECT palanca
                                                        FROM merma
                                                        WHERE palanca = '{$palanca->lote}' 
                                                        LIMIT 1
                                                    ) AS tbl");
        }

        foreach($dias as $dia){
            /* Promedio de cada dia */
            $calculo_dia = str_replace("AND palanca = tbl.palanca", "", $calculo);
            $response->totales[$dia->nombre] = $this->db->queryOne("SELECT ROUND({$calculo_dia}, 2) AS value
                                                    FROM (
                                                    SELECT '{$dia->fecha}' AS date_fecha
                                                    ) AS tbl");
        }
        /* Promedio semana de todos los lotes */
        $calculo_lote = str_replace("AND palanca = tbl.palanca", "", $calculo);
        $calculo_lote = str_replace("date_fecha = tbl.date_fecha", "semana = '{$filters->semana}'", $calculo_lote);
        $response->totales["prom_lote"] = $this->db->queryOne("SELECT ROUND({$calculo_lote}, 2) AS value
                                                                FROM (
                                                                SELECT '{$palanca->lote}' AS palanca
                                                                ) AS tbl");

        $response->data = $palancas;
        return $response;
    }

    private function getNivelDefectos($year, $lote, $labor, $semana, $dias = [], $variable, $finca){
        $response = new stdClass;
        if($finca != ""){ 
            $sWhere .= " AND finca = '{$finca}'";
        }
        if($variable == 'Peso'){
            // Peso Prom
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = {$year} AND type = '{$labor}' AND date_fecha = tbl.date_fecha AND bloque = tbl.bloque AND campo = tbl.campo AND cantidad > 0 $sWhere) 
                        / (SELECT SUM(racimos_procesados) FROM merma WHERE date_fecha = tbl.date_fecha AND bloque = tbl.bloque $sWhere)";
        }else if($variable == '% Merma'){
            // % Merma
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = {$year} AND type = '{$labor}' AND date_fecha = tbl.date_fecha AND bloque = tbl.bloque AND campo LIKE 'Total Peso%' AND cantidad > 0 $sem)
                        / (SELECT SUM(peso_neto) 
                            FROM merma 
                            WHERE year = {$year} AND date_fecha = tbl.date_fecha AND bloque = tbl.bloque $sWhere)
                        * 100";
        }else if($variable == 'Daños'){
            // Dedos Prom
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = {$year} AND type = '{$labor}' AND date_fecha = tbl.date_fecha AND bloque = tbl.bloque AND campo = tbl.campo AND cantidad > 0 $sWhere) /
                        (SELECT SUM(racimos_procesados) FROM merma WHERE year = {$year} AND date_fecha = tbl.date_fecha AND bloque = tbl.bloque $sWhere) * 5";
        }

        $response = $this->db->queryAll("SELECT campo AS defecto
                    FROM merma
                    INNER JOIN merma_detalle ON id_merma = merma.id
                    WHERE TYPE = '{$labor}' AND semana = {$semana} AND flag = 1 AND cantidad > 0 AND bloque = '{$lote}'
                    GROUP BY campo");
        foreach($response as $row){
            foreach($dias as $dia){
                $row->detalle[$dia->nombre] = $this->db->queryRow("SELECT ROUND({$calculo}, 2) AS value
                                                                FROM (
                                                                SELECT '{$lote}' AS bloque, '{$dia->fecha}' AS date_fecha, campo
                                                                FROM merma_detalle
                                                                WHERE campo = '{$row->defecto}'
                                                                LIMIT 1
                                                                ) AS tbl")->value;
            }
        }
        return $response;
    }

    private function getNivelDefectosPalanca($year, $palanca, $labor, $semana, $dias = [], $variable){
        $response = new stdClass;
        if($variable == 'Peso'){
            // Peso Prom
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = {$year} AND type = '{$labor}' AND date_fecha = tbl.date_fecha AND palanca = tbl.palanca AND campo = tbl.campo AND cantidad > 0) 
                        / (SELECT SUM(racimos_procesados) FROM merma WHERE date_fecha = tbl.date_fecha AND palanca = tbl.palanca)";
        }else if($variable == '% Merma'){
            // % Merma
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = {$year} AND type = '{$labor}' AND date_fecha = tbl.date_fecha AND palanca = tbl.palanca AND campo LIKE 'Total Peso%' AND cantidad > 0)
                        / (SELECT SUM(peso_neto) 
                            FROM merma 
                            WHERE year = {$year} AND date_fecha = tbl.date_fecha AND palanca = tbl.palanca)
                        * 100";
        }else if($variable == 'Daños'){
            // Dedos Prom
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = {$year} AND type = '{$labor}' AND date_fecha = tbl.date_fecha AND palanca = tbl.palanca AND campo = tbl.campo AND cantidad > 0) /
                        (SELECT SUM(racimos_procesados) FROM merma WHERE year = {$year} AND date_fecha = tbl.date_fecha AND palanca = tbl.palanca) * 5";
        }

        $response = $this->db->queryAll("SELECT campo AS defecto
                    FROM merma
                    INNER JOIN merma_detalle ON id_merma = merma.id
                    WHERE TYPE = '{$labor}' AND semana = {$semana} AND flag = 1 AND cantidad > 0 AND palanca = '{$palanca}'
                    GROUP BY campo");
        foreach($response as $row){
            foreach($dias as $dia){
                $row->detalle[$dia->nombre] = $this->db->queryRow("SELECT ROUND({$calculo}, 2) AS value
                                                                FROM (
                                                                SELECT palanca, '{$dia->fecha}' AS date_fecha, campo
                                                                FROM merma
                                                                INNER JOIN merma_detalle ON id_merma = merma.id
                                                                WHERE campo = '{$row->defecto}' AND palanca = '{$palanca}'
                                                                LIMIT 1
                                                                ) AS tbl")->value;
            }
        }
        return $response;
    }

    private function getCalculo($year, $variable, $labor, $palanca = "", $finca = ""){
        $sWhere = "";
        if($palanca != "" && $palanca > 0){
            $sWhere .= " AND palanca = '{$palanca}'";
        }
        if($finca != ""){
            $sWhere .= " AND finca = '{$finca}'";
        }

        if($variable == 'Peso'){
            // Peso Prom
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = {$year} AND type = '{$labor}' AND date_fecha = tbl.date_fecha AND bloque = tbl.bloque AND campo LIKE 'Total Peso%' AND cantidad > 0 $sWhere) 
                        / (SELECT SUM(racimos_procesados) FROM merma WHERE date_fecha = tbl.date_fecha AND bloque = tbl.bloque $sWhere)";
        }else if($variable == '% Merma'){
            // % Merma
            $calculo = "(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = {$year} AND type = '{$labor}' AND date_fecha = tbl.date_fecha AND bloque = tbl.bloque AND campo LIKE 'Total Peso%' AND cantidad > 0 $sWhere)
                        / (SELECT SUM(peso_neto) 
                            FROM merma 
                            WHERE year = {$year} AND date_fecha = tbl.date_fecha AND bloque = tbl.bloque $sWhere)
                        * 100";
        }else if($variable == 'Daños'){
            // Dedos Prom
            $calculo = "(SELECT AVG(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE year = {$year} AND type = '{$labor}' AND date_fecha = tbl.date_fecha AND bloque = tbl.bloque AND campo LIKE 'Total Peso%' AND cantidad > 0 $sWhere)";
        }
        return $calculo;
    }

    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						if(is_numeric($value->label)){
							$value->label = (double)$value->label;
						}
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
                        if($value->legend == "Umbral"){
                            $response->chart["series"][$position]["z"] = 1;
                            $response->chart["series"][$position]["zlevel"] = 1;
                        }
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
			}
		}
		return $response->chart;
	}
}
