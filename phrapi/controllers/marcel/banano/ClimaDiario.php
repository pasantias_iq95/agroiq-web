<?php
class ClimaDiario {
	
	private $db;
	private $session;
	
	public function __construct() {
        $this->db = DB::getInstance('sigat');
        $this->session = Session::getInstance();
    }

    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $filters = (object)[
            "fecha_inicial" => getValueFrom($postdata, 'fecha_inicial', ''),
            "fecha_final" => getValueFrom($postdata, 'fecha_final', ''),
            "hora_inicio" => getValueFrom($postdata, 'hora_inicio', ''),
            "hora_fin" => getValueFrom($postdata, 'hora_fin', ''),
            "luz" => getValueFrom($postdata, 'luz', 400)

        ];
        return $filters;
    }

    public function last(){
        $response = new stdClass;

        $response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM datos_clima WHERE id_hacienda = 14");
        $response->hora = $this->db->queryOne("SELECT MAX(hora) FROM datos_clima WHERE fecha = '{$response->fecha}' AND id_hacienda = 14");

        return $response;
    }

	public function tags(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->hora_inicio != ''){
            $sWhere .= " AND hora >= '{$filters->hora_inicio}'";
        }
        if($filters->hora_fin != ''){
            $sWhere .= " AND hora <= '{$filters->hora_fin}'";
        }

        $response->tags = new stdClass;
        $response->tags->temp_maxima = [
            "value" => $this->db->queryOne("SELECT MAX(temp_maxima) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
            "acumulado" => $this->db->queryOne("SELECT ROUND(MAX(temp_maxima), 2)
                                                FROM datos_clima
                                                WHERE id_hacienda = 14 AND YEAR(fecha) = YEAR('{$filters->fecha_inicial}')")
        ];
        $response->tags->temp_minima = [
            "value" => $this->db->queryOne("SELECT MIN(temp_minima) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
            "acumulado" => $this->db->queryOne("SELECT ROUND(MIN(temp_minima), 2)
                                                FROM datos_clima
                                                WHERE id_hacienda = 14 AND YEAR(fecha) = YEAR('{$filters->fecha_inicial}')")
        ];
        $response->tags->lluvia = [
            "value" => $this->db->queryOne("SELECT ROUND(SUM(lluvia), 2)
                                            FROM datos_clima
                                            WHERE id_hacienda = 14 $sWhere"),
            "acumulado" => $this->db->queryOne("SELECT ROUND(SUM(lluvia), 2)
                                                FROM datos_clima
                                                WHERE id_hacienda = 14 AND YEAR(fecha) = YEAR('{$filters->fecha_inicial}')")
        ];
        return $response;
    }

    public function detalle(){
        $response = new stdClass;

        $filters = $this->params();
        $has = (int) $this->db->queryOne("SELECT COUNT(1) FROM datos_clima WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND id_hacienda = 14");
        if($has == 0){
            return $response;
        }

        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->hora_inicio != ''){
            $sWhere .= " AND hora >= '{$filters->hora_inicio}'";
        }
        if($filters->hora_fin != ''){
            $sWhere .= " AND hora <= '{$filters->hora_fin}'";
        }

        $response->data = [
            [
                "detalle" => "TEMPERATURA MÍNIMA",
                "acumulado" => "",
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(temp_minima), 2) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(temp_minima) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(temp_minima) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
            ],
            [
                "detalle" => "TEMPERATURA MAXIMA",
                "acumulado" => "",
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(temp_maxima), 2) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(temp_maxima) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(temp_maxima) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
            ],
            [
                "detalle" => "LLUVIA (MM)",
                "acumulado" => $this->db->queryOne("SELECT SUM(lluvia) FROM datos_clima WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND id_hacienda = 14"),
                "avg" => $this->db->queryOne("SELECT AVG(lluvia) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(lluvia) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(lluvia) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
            ],
            [
                "detalle" => "HUMEDAD RELATIVA",
                "acumulado" => "",
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(humedad), 2) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(humedad) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(humedad) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
            ],
            [
                "detalle" => "RADIACIÓN SOLAR",
                "acumulado" => $this->db->queryOne("SELECT SUM(rad_solar) FROM datos_clima WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND id_hacienda = 14"),
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(rad_solar), 2) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "max" => $this->db->queryOne("SELECT MAX(rad_solar) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "min" => $this->db->queryOne("SELECT MIN(rad_solar) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
            ],
            [
                "detalle" => "HORAS LUZ",
                "acumulado" => $this->db->queryOne("SELECT ROUND(SUM(horas_luz), 2) FROM datos_clima WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND id_hacienda = 14"),
                "avg" => $this->db->queryOne("SELECT ROUND(AVG(horas_luz), 2) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "max" => $this->db->queryOne("SELECT ROUND(MAX(horas_luz), 2) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
                "min" => $this->db->queryOne("SELECT ROUND(MIN(horas_luz), 2) FROM datos_clima WHERE id_hacienda = 14 $sWhere"),
            ]
        ];

        return $response;
    }

    public function horasLuz(){
        $response = new stdClass;

        $filters = $this->params();
        $sql = "SELECT label, legend, cant AS 'value'
                FROM(
                    SELECT DATE_FORMAT(CONCAT(CURRENT_DATE,' ',hora_t), '%h %p') AS label,
                        (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE id_hacienda = estaciones.id_hacienda $sWhere) AS legend,
                        ROUND((SELECT AVG(rad_solar) FROM datos_clima WHERE HOUR(hora) = estaciones.hora AND fecha BETWEEN '$filters->fecha_inicial' AND '$filters->fecha_final' AND id_hacienda = estaciones.id_hacienda), 2) AS cant
                    FROM (
                        SELECT estaciones.estacion_id, hora, id_hacienda, hora_t
                        FROM (
                            SELECT HOUR(hora) AS hora, hora AS hora_t
                            FROM datos_clima
                            WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND id_hacienda = 14
                            GROUP BY HOUR(hora)
                        ) AS horas
                        JOIN (
                            SELECT estacion_id
                            FROM datos_clima c
                            INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                            WHERE 1=1 AND id_hacienda = 14
                            GROUP BY estacion_id
                        ) AS estaciones
                        INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                        GROUP BY hora, estaciones.estacion_id
                    ) AS estaciones
                    GROUP BY hora, estacion_id
                ) AS estaciones";
        $response->data = $this->generateSeries($sql);

        return $response;
    }

    public function graficasViento(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->hora_inicio != ''){
            $sWhere .= " AND hora >= '{$filters->hora_inicio}'";
        }
        if($filters->hora_fin != ''){
            $sWhere .= " AND hora <= '{$filters->hora_fin}'";
        }
        
        $sql = "SELECT ROUND(AVG(velocidad_viento), 1) AS velocidad_viento
                FROM datos_clima
                WHERE velocidad_viento IS NOT NULL AND id_hacienda = 14 $sWhere";
        $response->velocidad_viento = $this->db->queryOne($sql);

        $sql = "SELECT direccion_viento AS id, COUNT(1) AS label
                FROM sigat.datos_clima
                WHERE id_hacienda = 14 AND direccion_viento IS NOT NULL $sWhere
                GROUP BY direccion_viento";
        $response->direccion_viento = $this->db->queryAllSpecial($sql);

        return $response;
    }

    private function generateSeries($sql){
		$res = $this->db->queryAll($sql);

		$response->data = [];
        $response->legend = [];
        
		$series = new stdClass;
		$markLine = new stdClass;
        $labels = [];
        
        $min = null;
        $max = null;
        $avg = null;
        $total = 0;
        $count = 0;

		foreach ($res as $key => $value) {
            if(!in_array($value->label, $response->legend)){
                $response->legend[] = $value->label;
            }
		}

        foreach ($res as $key => $value) {
            $value = (object)$value;
            $value->position = array_search($value->legend, $response->legend);
            $value->promedio = (float)$value->value;

            if(($min == null || $min > $value->value) && $value->value != null) $min = $value->value;
            if(($max == null || $max < $value->value) && $value->value != null) $max = $value->value;
            if($value->value){
                $total += (float) $value->value;
                $count++;
            }
			
			if(!isset($response->data[$value->legend]->itemStyle)){
				$series = new stdClass;
				$series->name = $value->legend;
				$series->type = "line";
				$series->connectNulls = true;
				$series->data[] = $value->value;
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				$response->data[$value->legend] = $series;
			}else{
				$response->data[$value->legend]->data[] = $value->value;
			}
		}

        $response->min = (float) $min;
        $response->max = (float) $max;
        $response->avg = ROUND($total / $count, 2);
        $response->total = $total;

        return $response;
	}
}

?>