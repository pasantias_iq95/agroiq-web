<?php defined('PHRAPI') or die("Direct access not allowed!");

class Perchas {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function last(){
        $response = new stdClass;
        $response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM lancofruit_perchas");
        return $response;
    }

    public function tags(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = [];
        if($postdata->cliente != ''){
            $sWhere .= " AND cliente = '{$postdata->cliente}'";
        }
        if($postdata->local != ''){
            $sWhere .= " AND local = '{$postdata->local}'";
        }

        $sql = "SELECT ROUND(AVG(
                                (
                                    IFNULL(temp_bodega_1, 0) + 
                                    IFNULL(temp_bodega_2, 0) + 
                                    IFNULL(temp_bodega_3, 0) + 
                                    IFNULL(temp_bodega_4, 0) + 
                                    IFNULL(temp_bodega_5, 0))/ 
                                (
                                    IF(temp_bodega_1 IS NOT NULL AND temp_bodega_1 > 0, 1, 0) + 
                                    IF(temp_bodega_2 IS NOT NULL AND temp_bodega_2 > 0, 1, 0) + 
                                    IF(temp_bodega_3 IS NOT NULL AND temp_bodega_3 > 0, 1, 0) + 
                                    IF(temp_bodega_4 IS NOT NULL AND temp_bodega_4 > 0, 1, 0) + 
                                    IF(temp_bodega_5 IS NOT NULL AND temp_bodega_5 > 0, 1, 0)
                                )
                            ), 2) 
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere";
        $response[] = [
            "tittle" => "Temp Bodega",
            "subtittle" => "",
            "valor" => (float) $this->db->queryOne($sql),
            "promedio" => (double) 0,
            "cssClass" => "red-thunderbird"
        ];

        $sql = "SELECT ROUND(AVG(
                                (
                                    IFNULL(temp_percha_1_1, 0) + 
                                    IFNULL(temp_percha_1_2, 0) + 
                                    IFNULL(temp_percha_1_3, 0) + 
                                    IFNULL(temp_percha_1_4, 0) + 
                                    IFNULL(temp_percha_1_5, 0)
                                )/ 
                                (
                                    IF(temp_percha_1_1 IS NOT NULL AND temp_percha_1_1 > 0, 1, 0) + 
                                    IF(temp_percha_1_2 IS NOT NULL AND temp_percha_1_2 > 0, 1, 0) + 
                                    IF(temp_percha_1_3 IS NOT NULL AND temp_percha_1_3 > 0, 1, 0) + 
                                    IF(temp_percha_1_4 IS NOT NULL AND temp_percha_1_4 > 0, 1, 0) + 
                                    IF(temp_percha_1_5 IS NOT NULL AND temp_percha_1_5 > 0, 1, 0)
                                )
                            ), 2) 
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere";
        $response[] = [
            "tittle" => "Temp Percha 1",
            "subtittle" => "",
            "valor" => (float) $this->db->queryOne($sql),
            "promedio" => (double) 0,
            "cssClass" => "red-thunderbird"
        ];

        $sql = "SELECT ROUND(AVG(
                                (
                                    IFNULL(temp_percha_2_1, 0) + 
                                    IFNULL(temp_percha_2_2, 0) + 
                                    IFNULL(temp_percha_2_3, 0) + 
                                    IFNULL(temp_percha_2_4, 0) + 
                                    IFNULL(temp_percha_2_5, 0)
                                )/ 
                                (
                                    IF(temp_percha_2_1 IS NOT NULL AND temp_percha_2_1 > 0, 1, 0) + 
                                    IF(temp_percha_2_2 IS NOT NULL AND temp_percha_2_2 > 0, 1, 0) + 
                                    IF(temp_percha_2_3 IS NOT NULL AND temp_percha_2_3 > 0, 1, 0) + 
                                    IF(temp_percha_2_4 IS NOT NULL AND temp_percha_2_4 > 0, 1, 0) + 
                                    IF(temp_percha_2_5 IS NOT NULL AND temp_percha_2_5 > 0, 1, 0))
                            ), 2) 
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere";
        $response[] = [
            "tittle" => "Temp Percha 2",
            "subtittle" => "",
            "valor" => (float) $this->db->queryOne($sql),
            "promedio" => (double) 0,
            "cssClass" => "red-thunderbird"
        ];
        return $response;
    }

    public function data(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->cliente != ''){
            $sWhere .= " AND cliente = '{$postdata->cliente}'";
        }
        if($postdata->local != ''){
            $sWhere .= " AND local = '{$postdata->local}'";
        }

        $sql = "SELECT tbl.*, 
                    ROUND(SUM(porc_fruta_lancofruit) / total_porc * 100, 2) AS porc_fruta_lancofruit,
                    ROUND(SUM(porc_fruta_terceros) / total_porc * 100, 2) AS porc_fruta_terceros,
                    ROUND(SUM(porc_percha_vacia) / total_porc * 100, 2) AS porc_percha_vacia
                FROM lancofruit_perchas
                JOIN (
                    SELECT 
                        SUM(gavetas) AS gavetas, 
                        SUM(kilos_totales) AS kilos_totales, 
                        SUM(kilos_destruidos) AS kilos_destruidos,
                        SUM(porc_fruta_lancofruit + porc_fruta_terceros + porc_percha_vacia) AS total_porc,

                        SUM(total_perchas_frutas_verduras) AS total_perchas_frutas_verduras,
                        SUM(total_perchas_solo_banano) AS total_perchas_solo_banano,

                        SUM(grado_1_cantidad_cluster) AS grado_1,
                        SUM(grado_2_cantidad_cluster) AS grado_2,
                        SUM(grado_3_cantidad_cluster) AS grado_3,
                        SUM(grado_4_cantidad_cluster) AS grado_4,
                        SUM(grado_5_cantidad_cluster) AS grado_5,
                        SUM(grado_6_cantidad_cluster) AS grado_6,
                        SUM(
                            IFNULL(grado_1_cantidad_cluster, 0) +
                            IFNULL(grado_2_cantidad_cluster, 0) +
                            IFNULL(grado_3_cantidad_cluster, 0) +
                            IFNULL(grado_4_cantidad_cluster, 0) +
                            IFNULL(grado_5_cantidad_cluster, 0) +
                            IFNULL(grado_6_cantidad_cluster, 0)
                        ) AS total_grados
                    FROM `lancofruit_perchas`
                    WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                ) AS tbl
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere";
        $response->data = $this->db->queryRow($sql);

        $bodega = $this->db->queryRow("SELECT 
                MAX(bodega) max_bodega, AVG(bodega) prom_bodega, MIN(bodega) min_bodega
            FROM(
                SELECT IF(temp_bodega_1 > 0, temp_bodega_1, NULL) bodega
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_bodega_2 > 0, temp_bodega_2, NULL) bodega
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_bodega_3 > 0, temp_bodega_3, NULL) bodega
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_bodega_4 > 0, temp_bodega_4, NULL) bodega
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_bodega_5 > 0, temp_bodega_5, NULL) bodega
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
            ) tbl
            WHERE bodega > 0");
        $response->data->prom_bodega = $bodega->prom_bodega;
        $response->data->max_bodega = $bodega->max_bodega;
        $response->data->min_bodega = $bodega->min_bodega;

        $percha1 = $this->db->queryRow("SELECT 
                MAX(percha1) max_percha_1, AVG(percha1) prom_percha_1, MIN(percha1) min_percha_1
            FROM(
                SELECT IF(temp_percha_1_1 > 0, temp_percha_1_1, NULL) percha1
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_percha_1_2 > 0, temp_percha_1_2, NULL) percha1
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_percha_1_3 > 0, temp_percha_1_3, NULL) percha1
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_percha_1_4 > 0, temp_percha_1_4, NULL) percha1
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_percha_1_5 > 0, temp_percha_1_5, NULL) percha1
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
            ) tbl
            WHERE percha1 > 0");
        $response->data->prom_percha_1 = $percha1->prom_percha_1;
        $response->data->max_percha_1 = $percha1->max_percha_1;
        $response->data->min_percha_1 = $percha1->min_percha_1;

        $percha2 = $this->db->queryRow("SELECT 
                MAX(percha2) max_percha_2, AVG(percha2) prom_percha_2, MIN(percha2) min_percha_2
            FROM(
                SELECT IF(temp_percha_2_1 > 0, temp_percha_2_1, NULL) percha2
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_percha_2_2 > 0, temp_percha_2_2, NULL) percha2
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_percha_2_3 > 0, temp_percha_2_3, NULL) percha2
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_percha_2_4 > 0, temp_percha_2_4, NULL) percha2
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
                UNION ALL
                SELECT IF(temp_percha_2_5 > 0, temp_percha_2_5, NULL) percha2
                FROM lancofruit_perchas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere
            ) tbl
            WHERE percha2 > 0");
        $response->data->prom_percha_2 = $percha2->prom_percha_2;
        $response->data->max_percha_2 = $percha2->max_percha_2;
        $response->data->min_percha_2 = $percha2->min_percha_2;

        $response->tags = $this->tags();
        $response->locales = $this->getLocales();
        $response->clientes = $this->getClientes();
        $response->categorias = $this->pastelesCategoriasDefectos();
        return $response;
    }

    private function pastelesCategoriasDefectos(){
        $response = new stdClass;
        $categorias = $this->getCategorias();

        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->cliente != ''){
            $sWhere .= " AND cliente = '{$postdata->cliente}'";
        }
        if($postdata->local != ''){
            $sWhere .= " AND local = '{$postdata->local}'";
        }

        $response->main = [];
        $response->defectos = [];
        $response->categorias = $categorias;
        foreach($categorias as $cat){
            $response->main[] = [
                "label" => $cat,
                "value" => $this->db->queryOne("SELECT SUM(cantidad) 
                    FROM lancofruit_perchas percha
                    INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
                    WHERE categoria = '{$cat}' $sWhere")
            ];
            $response->defectos[$cat] = $this->graficaDefectos($cat);
            if(!$response->defectos[$cat]){
                $response->defectos[$cat] = ["0.00"];
            }
        }

        return $response;
    }

    private function graficaDefectos($categoria){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->cliente != ''){
            $sWhere .= " AND cliente = '{$postdata->cliente}'";
        }
        if($postdata->local != ''){
            $sWhere .= " AND local = '{$postdata->local}'";
        }

        $sql = "SELECT defecto AS label, SUM(cantidad) AS 'value'
                FROM lancofruit_perchas percha
                INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id
                WHERE categoria = '{$categoria}' $sWhere
                GROUP BY defecto";
        $res = $this->db->queryAll($sql);
        if(!$res){
            $res[] = ["label" => "NO SE ENCONTRARON RESULTADOS", "value" => "0.00"];
        }

        return $res;
    }

    private function getCategorias(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->fecha_inicial != ''){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->cliente != ''){
            $sWhere .= " AND cliente = '{$postdata->cliente}'";
        }
        if($postdata->local != ''){
            $sWhere .= " AND local = '{$postdata->local}'";
        }

        $sql = "SELECT categoria 
                FROM lancofruit_perchas percha
                INNER JOIN lancofruit_perchas_defectos ON id_percha = percha.id 
                WHERE 1=1 $sWhere
                GROUP BY categoria";
        $response = $this->db->queryAllOne($sql);
        if(!$response){
            $response = ["NO SE ENCONTRARON RESULTADOS"];
        }
        return $response;
    }

    private function getClientes(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if($this->session->logged == 49){
            $sWhere = " AND cliente = 'TIA'";
        }
  
        $response = $this->db->queryAllOne("SELECT UPPER(cliente) FROM lancofruit_perchas WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' $sWhere GROUP BY cliente ORDER BY cliente");
        return $response;
    }

    private function getLocales(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = $this->db->queryAll("SELECT UPPER(cliente) AS cliente, UPPER(local) AS local FROM lancofruit_perchas WHERE cliente = '{$postdata->cliente}' AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' GROUP BY local ORDER BY local");
        return $response;
    }
}
