<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionCajas {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function last(){
		$response = new stdClass;
		$response->last = $this->db->queryRow("SELECT MAX(fecha) as fecha 
			FROM (
				SELECT MAX(fecha) as fecha 
				FROM produccion_gavetas 
				UNION ALL 
				SELECT MAX(fecha) 
				FROM produccion_cajas) AS tbl");
		return $response;
	}

    public function cuadreCajas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sql = "SELECT *, (SELECT SUM(valor) FROM produccion_cajas_real WHERE marca = tbl.marca AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}') AS suma_real
                FROM(
                SELECT 'CAJA' AS tipo, produccion_cajas.marca, COUNT(1) AS balanza
                FROM produccion_cajas 
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                GROUP BY produccion_cajas.marca) AS tbl";
        $response->data = $this->db->queryAll($sql);
        $sql = "SELECT *, (SELECT SUM(valor) FROM produccion_cajas_real WHERE marca = tbl.marca AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}') AS suma_real
                FROM(
                SELECT 'GAVETA' AS tipo, marca, COUNT(1) AS balanza
                FROM produccion_gavetas
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                GROUP BY marca
                UNION ALL
                SELECT 'GAVETA' AS tipo, marca, 0 AS balanza
                FROM produccion_cajas_real
                WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                    AND marca NOT IN(SELECT marca FROM produccion_gavetas WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' GROUP BY marca)
                    AND marca IN ('MI COMISARIATO', 'TIA')
                GROUP BY marca
                ) AS tbl";
        $data_gavetas = $this->db->queryAll($sql);
        $response->data = array_merge($response->data, $data_gavetas);

        foreach($response->data as $row){
            $row->detalle = $this->db->queryAll("SELECT guia, SUM(valor) AS valor FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND marca = '{$row->marca}' GROUP BY guia");
            if($row->balanza > 0 && $row->suma_real > 0)
                $row->porcentaje = $row->balanza / $row->suma_real * 100;
        }

        $response->guias = $this->db->queryAll("SELECT SUM(valor) AS cajas, guia FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' GROUP BY guia");
        foreach($response->guias as $row){
            $row->detalle = $this->db->queryAll("SELECT marca, SUM(valor) AS valor FROM produccion_cajas_real WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND guia = '{$row->guia}' GROUP BY marca");
        }
        return $response;
    }

    public function guardarCuadrar(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if(isset($postdata->fecha) && $postdata->fecha != ""){
            foreach($postdata->marca as $marca => $value){
                if($value > 0){
                    $exits = count($this->db->queryAll("SELECT * FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' AND marca = '{$marca}' AND guia = '{$postdata->guia}'")) > 0;
                    if($exits){
                        $this->db->query("UPDATE produccion_cajas_real SET valor = '{$value}' WHERE marca = '{$marca}' AND fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}'");
                        $response->message = "Se actualizo con éxito";
                    }else{
                        $this->db->query("INSERT INTO produccion_cajas_real SET valor = '{$value}', marca = '{$marca}', fecha = '{$postdata->fecha}', guia = '{$postdata->guia}', codigo_productor = '{$postdata->productor}', codigo_magap = '{$postdata->magap}', sello_seguridad = '{$postdata->sello_seguridad}'");
                        $response->message = "Se inserto con éxito";
                    }
                }
            }
        }
        return $response;
    }

    public function procesar(){
        $response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));

        $marcas = $this->db->queryAll("SELECT marca, SUM(valor) AS cantidad FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' GROUP BY marca");
        foreach($marcas as $mr){
            $bd[$mr->marca] = $this->db->queryRow("SELECT SUM(cantidad) AS cantidad 
                                                    FROM(
                                                        SELECT COUNT(1) AS cantidad 
                                                        FROM produccion_cajas 
                                                        WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}'
                                                        UNION ALL
                                                        SELECT COUNT(1) AS cantidad
                                                        FROM produccion_gavetas
                                                        WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}'
                                                    ) AS tbl")->cantidad;
            if($bd[$mr->marca] < $mr->cantidad)
                $this->db->query("UPDATE produccion_cajas_real SET status = 'PROCESADO' WHERE fecha = '{$postdata->fecha}' AND marca = '{$mr->marca}'");
        }
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $reg){
                if($reg->marca == 'MI COMISARIATO'){
                    D("DELETE FROM produccion_gavetas WHERE id = $reg->id");
                    $this->db->query("DELETE FROM produccion_gavetas WHERE id = $reg->id");
                }else
                    $this->db->query("DELETE FROM produccion_cajas WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }

	public function filters(){
		$response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
		if(isset($postdata->year) && $postdata->year != ""){
			$sYear = " AND year = '{$postdata->year}'";
		}

		$response->years = $this->db->queryAllSpecial("SELECT year as id, year as label FROM(
			SELECT year FROM produccion_cajas GROUP BY year 
			UNION ALL
			SELECT year FROM produccion_gavetas GROUP BY year
			) AS tbl
			WHERE year > 0
			GROUP BY year");
		
		$sql = "SELECT semana as id, semana as label FROM(
			SELECT semana FROM produccion_cajas  WHERE 1=1 $sYear GROUP BY semana 
			UNION ALL
			SELECT semana FROM produccion_gavetas WHERE 1=1 $sYear GROUP BY semana
			) AS tbl
			GROUP BY semana";
		$response->semanas = $this->db->queryAll($sql);
		return $response;
	}

	public function registros(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
			'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
			'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
			'fecha_final' => getValueFrom($postdata, 'fecha_final', '')
		];

		$response = new stdClass;
		$response->data = $this->db->queryAll("SELECT * FROM (
			SELECT id, fecha, 'MI COMISARIATO' AS marca, peso, 'GAVETA' AS tipo, hora
			FROM produccion_gavetas
			WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'
			UNION ALL
			SELECT id, fecha, marca, caja AS peso, 'CAJA' AS tipo, hora
			FROM produccion_cajas
			WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}') AS tbl
			ORDER BY fecha DESC, hora DESC");
		$response->numPages = $this->db->queryRow("SELECT CEIL(COUNT(1)/10) as num
			FROM (
			SELECT id
			FROM produccion_gavetas
			WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'
			UNION ALL
			SELECT id
			FROM produccion_cajas
			WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}') AS tbl")->num;
		
		return $response;	
	}

	public function resumenMarca(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
			'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
			'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
			'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'semana' => (int) getValueFrom($postdata, 'semana', 0),
		];
        /*if($filters->semana > 0){
            $sWhere = " AND semana = {$filters->semana}";
        }*/

		$response = new stdClass;
		$sql = "SELECT marca, SUM(cantidad) AS cantidad, SUM(total_kg) AS total_kg, CONV AS 'conv', tipo, promedio, maximo, minimo, desviacion
                FROM(
                SELECT
                    `cajas`.`marca` AS `marca`,
                    IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS `cantidad`,
                    ROUND(SUM(cajas.`caja`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.caja), 0), 2) AS total_kg,
                    IF(conv.id IS NOT NULL, 
                        ROUND(((SUM(cajas.`caja`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.caja), 0)) / 0.4536) / 40.5, 0),
                        COUNT(1)) AS conv,
                    'CAJA' AS tipo,
                    ROUND(AVG(cajas.`caja`), 2) AS promedio,
                    MAX(cajas.`caja`) AS maximo,
                    MIN(cajas.`caja`) AS minimo,
                    ROUND(STD(cajas.caja), 2) AS desviacion
                FROM (`produccion_cajas` `cajas`
                LEFT JOIN `produccion_cajas_real` `cajas_real`
                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                        AND status = 'PROCESADO'))
                LEFT JOIN produccion_cajas_convertir conv ON cajas.marca = conv.marca
                WHERE cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND (`cajas`.`marca` <> '') $sWhere
                GROUP BY `cajas`.`marca`, cajas_real.id
				UNION ALL
                SELECT
                    `cajas`.`marca` AS `marca`,
                    IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS `cantidad`,
                    ROUND(SUM(cajas.`peso`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.peso), 0), 2) AS total_kg,
                    IF(conv.id IS NOT NULL, 
                        ROUND(((SUM(cajas.`peso`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.peso), 0)) / 0.4536) / 40.5, 0),
                        COUNT(1)) AS conv,
                    'GAVETA' AS tipo,
                    ROUND(AVG(cajas.`peso`), 2) AS promedio,
                    MAX(cajas.`peso`) AS maximo,
                    MIN(cajas.`peso`) AS minimo,
                    ROUND(STD(cajas.peso), 2) AS desviacion
                FROM (`produccion_gavetas` `cajas`
                LEFT JOIN `produccion_cajas_real` `cajas_real`
                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                        AND status = 'PROCESADO'))
                LEFT JOIN produccion_cajas_convertir conv ON cajas.marca = conv.marca
                WHERE cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND (`cajas`.`marca` <> '') $sWhere
                GROUP BY `cajas`.`marca`, cajas_real.id
                UNION ALL
                (
                    SELECT tbl.marca, cantidad, (cantidad * promedio) AS total_kg,
                        IF(conver.id IS NOT NULL, 
                                ROUND(((cantidad * promedio) / 0.4536) / 40.5, 0),
                                COUNT(1)) AS conv,
                        tipo, promedio, promedio AS maximo, promedio AS minimo, promedio AS desviacion
                    FROM (
                        SELECT
                            `cajas_real`.`marca` AS `marca`,
                            cajas_real.`valor` AS `cantidad`,
                            (SELECT AVG(peso) FROM produccion_gavetas WHERE cajas_real.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != '') AS promedio,
                            'GAVETA' AS tipo
                        FROM produccion_cajas_real cajas_real
                        WHERE cajas_real.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != ''  AND STATUS = 'PROCESADO'
                            AND marca NOT IN (SELECT marca FROM produccion_gavetas WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}')
                            AND marca IN ('TIA', 'MI COMISARIATO')
                    ) AS tbl
                    LEFT JOIN produccion_cajas_convertir conver ON tbl.marca = conver.marca
                    HAVING cantidad > 0
                )
                ) AS tbl
                GROUP BY marca";                
		$response->data = $this->db->queryAll($sql);
        
        /* LOAD TAGS */
        $response->tags = $this->tags($filters);
        $response->tags["cajas40"] = $this->sumOfValue($response->data, 'conv');

		return $response;
	}

    private function sumOfValue($data, $prop){
        $sum = 0;
        foreach($data as $row)
            if(is_object($row))
                $sum += (double) $row->{$prop};
            else
                $sum += (double) $row[$prop];
        return $sum;
    }

	public function historicoCajasSemanal(){
		$response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
		if(isset($postdata->year) && $postdata->year != ""){
			$sYear = " AND year = '{$postdata->year}'";
		}

		$data_chart = $this->db->queryAll("SELECT label_x, sum(value) as value, name, index_y
            FROM(
                SELECT semana AS label_x, IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS value, 'CANTIDAD' AS name, '0' AS index_y
                FROM produccion_cajas cajas
                LEFT JOIN `produccion_cajas_real` `cajas_real`
                        ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                            AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`)
                            AND STATUS = 'PROCESADO'))
                WHERE 1=1 $sYear
                GROUP BY semana
                UNION ALL
                SELECT semana AS label_x, IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS value, 'CANTIDAD' AS name, '0' AS index_y
                FROM produccion_gavetas cajas
                LEFT JOIN `produccion_cajas_real` `cajas_real`
                        ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                            AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`)
                            AND STATUS = 'PROCESADO'))
                WHERE 1=1 $sYear
                GROUP BY semana
			) AS tbl
            GROUP BY label_x
			ORDER BY label_x");

		$groups = [
			[
				"name" => '',
				"type" => 'line',
				'format' => ''
			]
		];
		$response->chart = $this->grafica_z($data_chart, $groups);
		return $response;
	}

	public function getMarcas(){
		$response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
		if(isset($postdata->year) && $postdata->year != ""){
			$sYear = " AND year = '{$postdata->year}'";
		}

		$response->data = $this->db->queryAllSpecial("SELECT marca as id, marca as label FROM(
			SELECT marca
			FROM produccion_cajas
			WHERE 1=1 $sYear
			GROUP BY marca
		) AS tbl");
		return $response;
	}

    public function getGuias(){
        $response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
        
        if($postdata->guia != "" && $postdata->fecha != ""){
            $response->marcas = $this->db->queryAll("SELECT marca, valor FROM produccion_cajas_real WHERE fecha = '{$postdata->fecha}' AND guia = '{$postdata->guia}'");
        }
        return $response;
    }

    public function tags($filters){
        $response = [];
        /*if($filters->semana > 0){
            $sWhere = " AND semana = {$filters->semana}";
        }*/
        $rows = $this->db->queryAll("SELECT hora, fecha
                                    FROM(
                                        SELECT hora, fecha
                                        FROM produccion_cajas 
                                        WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != '' $sWhere
                                        UNION ALL
                                        SELECT hora, fecha
                                        FROM produccion_gavetas 
                                        WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' AND marca != '' $sWhere
                                    ) AS tbl
                                    ORDER BY hora");
        if(count($rows)>0){
            $p = $rows[0];
            $u = $rows[count($rows)-1];
            $response["primera_caja"] = $p->hora;
            $response["ultima_caja"] = $u->hora;
            $response["fecha_primera"] = $p->fecha;
            $response["fecha_ultima"] = $u->fecha;
            $response["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}}',  '{$p->hora}') AS dif")->dif;
        }else{
            $response["primera_caja"] = "";
            $response["ultima_caja"] = "";
            $response["fecha_primera"] = "";
            $response["fecha_ultima"] = "";
        }
        return $response;
    }

	private function grafica_z($data = [], $group_y = []){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
		$options["legend"]["left"] = "center";
		$options["xAxis"] = [
			[
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
		}

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
		}

		foreach($_names as $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => 'line',
				"connectNulls" => true,
				"data" => []
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
	}
}
