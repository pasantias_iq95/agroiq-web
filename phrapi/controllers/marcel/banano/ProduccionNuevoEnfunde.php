<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionNuevoEnfunde {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function getColorCinta(){
        $response = new stdClass;
        $filters = $this->getParams();
        $response->cinta = $this->db->queryRow("SELECT c.color, c.class FROM semanas_colores s INNER JOIN produccion_colores c ON s.color = c.color WHERE semana = {$filters->semana} AND year = {$filters->year}");
        return $response;
    }
    
    private function getParams(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "fecha" => getValueFrom($postdata, 'fecha', ''),
            "enfundador" => (int) getValueFrom($postdata, 'enfundador', 0),
            "lote" => getValueFrom($postdata, 'lote', 0),
            "saldo_inicial" => getValueFrom($postdata, 'saldo_inicial', 0),
            "entregadas" => (int) getValueFrom($postdata, 'entregadas', 0),
            "usadas" => (int) getValueFrom($postdata, 'usadas', 0),
            "year" => (int) getValueFrom($postdata, 'year', 0),
            "semana" => (int) getValueFrom($postdata, 'semana', 0)
        ];
        return $data;
    }

	public function index(){
        $response = new stdClass;
        $filters = $this->getParams();
        
        $response->saldo_inicial = $this->getSaldoInicial($filters);
        $response->enfundadores = $this->db->queryAll("SELECT id, nombre FROM cat_enfundadores WHERE status = 'Activo' ORDER BY nombre");
        $response->lotes = $this->db->queryAll("SELECT nombre AS lote FROM lotes");
        $response->cinta = $this->db->queryRow("SELECT color, (SELECT class FROM produccion_colores WHERE color = semanas_colores.color AND status = 'Activo') AS class
                                                FROM semanas_colores 
                                                WHERE year = YEAR('{$filters->fecha}') AND semana = getWeek('{$filters->fecha}')");
        return $response;
    }

    public function saldo_inicial(){
        $response = new stdClass;
        $filters = $this->getParams();
        $response->saldo_inicial = $this->getSaldoInicial($filters);
        return $response;
    }

    public function getSaldoInicial($filters){
        if($filters->enfundador > 0){
            $response = $this->db->queryRow("SELECT SUM(IFNULL(entregadas,0)) - SUM(IFNULL(usadas,0)) AS saldo
                                            FROM produccion_enfunde_tthh
                                            WHERE fecha <= '{$filters->fecha}' 
                                                AND id_enfundador = $filters->enfundador")->saldo;
        }else{
            $response = 0;
        }
        return $response;
    }

    public function save(){
        $response = new stdClass;
        $response->status = 400;
        $filters = $this->getParams();

        if($filters->enfundador > 0 && $filters->lote != "" && (($filters->entregadas < 0 || $filters->entregadas > 0) || $filters->usadas > 0)){
            $cinta = $this->db->queryOne("SELECT color FROM semanas_colores WHERE year = YEAR('{$filters->fecha}') AND semana = getWeek('{$filters->fecha}')");            
            $sql = "INSERT INTO produccion_enfunde_tthh SET 
                        id_finca = (SELECT idFinca FROM lotes WHERE nombre = '{$filters->lote}'),
                        id_enfundador = {$filters->enfundador}, 
                        fecha = '{$filters->fecha}', 
                        semana = getWeek('{$filters->fecha}'), 
                        cinta = '{$cinta}', 
                        usadas = {$filters->usadas}, 
                        entregadas = {$filters->entregadas}, 
                        lote = '{$filters->lote}', 
                        edad = getEdadCinta(getWeek('{$filters->fecha}'), 
                        '{$cinta}', 
                        YEAR('{$filters->fecha}'))";
            if($this->db->query($sql)){
                $response->status = 200;
            }
        }else{
            $response->data = $filters;
        }
        return $response;
    }

    private function generateSaldoInicial($filters){
        $count = $this->db->queryOne("SELECT COUNT(1) FROM produccion_enfunde_saldo_inicial_tthh WHERE fecha = '{$filters->fecha}' AND id_enfundador = $filters->enfundador");
        if($count == 0){
            $saldo_inicial = $this->db->queryOne("SELECT saldo FROM `produccion_enfunde_saldo_inicial_tthh` WHERE fecha = (SELECT MAX(fecha) FROM produccion_enfunde_tthh WHERE id_enfundador = $filters->enfundador) AND id_enfundador = $filters->enfundador");
            if(!$saldo_inicial) $saldo_inicial = 0;
            $usadas = $this->db->queryOne("SELECT SUM(usadas) FROM produccion_enfunde_tthh WHERE fecha = (SELECT MAX(fecha) FROM produccion_enfunde_tthh WHERE id_enfundador = $filters->enfundador) AND id_enfundador = $filters->enfundador");
            if(!$usadas) $usadas = 0;
            $entregadas = $this->db->queryOne("SELECT SUM(entregadas) FROM produccion_enfunde_tthh WHERE fecha = (SELECT MAX(fecha) FROM produccion_enfunde_tthh WHERE id_enfundador = $filters->enfundador) AND id_enfundador =  $filters->enfundador");
            if(!$entregadas) $entregadas = 0;
            $this->db->query("INSERT INTO produccion_enfunde_saldo_inicial_tthh SET fecha = '{$filters->fecha}', id_enfundador = $filters->enfundador, saldo = $saldo_inicial - $usadas + $entregadas");
        }
    }
}
