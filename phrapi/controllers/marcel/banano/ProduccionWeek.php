<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionWeek {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();		
        $this->sigat = DB::getInstance("sigat");
        $this->db = DB::getInstance('marcel');
	}

    public function getLastWeek(){
        $response = new stdClass;

        $filters = $this->params();
        if($filters->year > 0){
            $response->week = $this->db->queryOne("SELECT MAX(semana) FROM produccion_historica WHERE year = {$filters->year}");
            $response->firts_date = $this->db->queryOne("SELECT STR_TO_DATE('{$filters->year}{$response->week} Sunday', '%X%V %W')");
            $response->second_date = $this->db->queryOne("SELECT STR_TO_DATE('{$filters->year}{$response->week} Saturday', '%X%V %W')");
        }

        return $response;
    }

    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
			'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
			'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
			'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'semana' => (int) getValueFrom($postdata, 'semana', 0),
            'year' => (int) getValueFrom($postdata, 'year', 0),
            'var1' => getValueFrom($postdata, 'var1', ''),
            'var2' => getValueFrom($postdata, 'var2', ''),
            'type1' => getValueFrom($postdata, 'type1', ''),
            'type2' => getValueFrom($postdata, 'type2', ''),
            'id_finca' => getValueFrom($postdata, 'id_finca', 1),
            'tipo_tabla' => getValueFrom($postdata, 'tipo_tabla', 'cantidad'),
        ];
        return $filters;
    }
    
    public function tags(){
        $filters = $this->params();
        $response = new stdClass;
        return ["data" => $this->db->queryRow("SELECT * FROM produccion_resumen_tags WHERE anio = {$filters->year}")];
    }

    public function graficaEdadPromedio(){
        $response = new stdClass;
        $filters = $this->params();
        $year = $filters->year;

        $response->chart = new stdClass;
        $sql = "SELECT semana 
                FROM produccion_historica 
                WHERE year = $year
                GROUP BY semana 
                ORDER BY semana";
        $response->chart->legend = $this->db->queryAllOne($sql);

        $sql = "SELECT ROUND(AVG(edad), 2) 
                FROM produccion_historica 
                WHERE year = $year";
        $response->chart->umbral = $this->db->queryOne($sql);

        $sql = "SELECT lote, semana, ROUND(AVG(edad), 2) as edad 
                FROM produccion_historica 
                WHERE year = $year
                GROUP BY lote, semana 
                ORDER BY semana";
        $data_series = $this->db->queryAll($sql);
        $semanas = [];
        foreach($data_series as $row){
            if(!isset($response->chart->data[$row->lote])){
                $response->chart->data[$row->lote] = [
                    "connectNulls" => true,
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => "0",
                            "barBorderWidth" => "6",
                            "label" => [
                                "show" => false,
                                "position" => "insideTop"
                            ]
                        ]
                    ],
                    "type" => "line",
                    "name" => $row->lote,
                    "data" => []
                ];
            }
            $response->chart->data[$row->lote]["data"][] = $row->edad;
        }
        return $response;
    }

    private function getResumenCajas($filters){
        $filters = (object) $filters;

        $sWhere = "";
        if(isset($filters->fecha_inicial) && isset($filters->fecha_final))
        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }

        if(isset($filters->semana) && isset($filters->anio))
        if($filters->semana > 0 && $filters->anio > 0){
            $sWhere .= " AND cajas.year = $filters->anio AND cajas.semana <= $filters->semana";
        }

		$sql = "SELECT marca, SUM(cantidad) AS cantidad, SUM(total_kg) AS total_kg, CONV AS 'conv', tipo, promedio, maximo, minimo, desviacion
                FROM(
                SELECT
                    `cajas`.`marca` AS `marca`,
                    IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS `cantidad`,
                    ROUND(SUM(cajas.`caja`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.caja), 0), 2) AS total_kg,
                    IF(conv.id IS NOT NULL, 
                        ROUND(((SUM(cajas.`caja`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.caja), 0)) / 0.4536) / 40.5, 0),
                        COUNT(1)) AS conv,
                    'CAJA' AS tipo,
                    ROUND(AVG(cajas.`caja`), 2) AS promedio,
                    MAX(cajas.`caja`) AS maximo,
                    MIN(cajas.`caja`) AS minimo,
                    ROUND(STD(cajas.caja), 2) AS desviacion
                FROM (`produccion_cajas` `cajas`
                LEFT JOIN `produccion_cajas_real` `cajas_real`
                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                        AND status = 'PROCESADO'))
                LEFT JOIN produccion_cajas_convertir conv ON cajas.marca = conv.marca
                WHERE (`cajas`.`marca` <> '') $sWhere
                GROUP BY `cajas`.`marca`, cajas_real.id
				UNION ALL
                SELECT
                    `cajas`.`marca` AS `marca`,
                    IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS `cantidad`,
                    ROUND(SUM(cajas.`peso`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.peso), 0), 2) AS total_kg,
                    IF(conv.id IS NOT NULL, 
                        ROUND(((SUM(cajas.`peso`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.peso), 0)) / 0.4536) / 40.5, 0),
                        COUNT(1)) AS conv,
                    'GAVETA' AS tipo,
                    ROUND(AVG(cajas.`peso`), 2) AS promedio,
                    MAX(cajas.`peso`) AS maximo,
                    MIN(cajas.`peso`) AS minimo,
                    ROUND(STD(cajas.peso), 2) AS desviacion
                FROM (`produccion_gavetas` `cajas`
                LEFT JOIN `produccion_cajas_real` `cajas_real`
                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                        AND status = 'PROCESADO'))
                LEFT JOIN produccion_cajas_convertir conv ON cajas.marca = conv.marca
                WHERE (`cajas`.`marca` <> '') $sWhere
                GROUP BY `cajas`.`marca`, cajas_real.id
                ) AS tbl
                GROUP BY marca";                
		return $this->db->queryAll($sql);
    }

    public function reporteProduccion(){
        $response = new stdClass;
        $filters = $this->params();

        $sql = "SHOW COLUMNS
                FROM `produccion_resumen_tabla`
                WHERE FIELD LIKE 'sem_%'";
        $columns = $this->db->queryAll($sql);

        $semanas = [];
        $semanas_fields = "";
        foreach($columns as $row){
            $hasData = $this->db->queryOne("SELECT COUNT(1) FROM produccion_resumen_tabla WHERE $row->Field > 0 AND anio = $filters->year AND id_finca = {$filters->id_finca}");
            if($hasData){
                $semanas[] = str_replace("sem_", "", $row->Field);
                $semanas_fields .= ", {$row->Field}";
            }
        }

        $sql = "SELECT promedio AS 'avg', alias AS campo, maximo AS 'max', minimo AS 'min', suma AS 'sum' {$semanas_fields}
                FROM produccion_resumen_tabla
                WHERE anio = $filters->year AND id_finca = {$filters->id_finca} AND status = 'Activo' AND tipo = '{$filters->tipo_tabla}'
                ORDER BY orden";
        $table = $this->db->queryAll($sql);

        $response->data = $table;
        $response->semanas = $semanas;
        return $response;
    }

    /* GET VALUES */
    private function getCajasRealesSemana($year, $semana){
        $sql = "SELECT SUM(IF(blz > guia, blz, guia)) AS cantidad
                FROM (
                    SELECT semana, fecha, marca, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'GUIA', cantidad, 0)) AS guia
                    FROM (
                        SELECT semana, fecha, marca, COUNT(1) AS cantidad, 'BALANZA' AS origen
                        FROM produccion_cajas
                        WHERE YEAR = {$year} AND semana = {$semana}
                        GROUP BY marca, fecha
                        UNION ALL
                        SELECT semana, fecha, marca, COUNT(1) AS cantidad, 'BALANZA' AS origen
                        FROM produccion_gavetas
                        WHERE YEAR = {$year} AND semana = {$semana}
                        GROUP BY marca, fecha
                        -- cuadre de cajas
                        UNION ALL
                        SELECT getWeek(fecha), fecha, marca, SUM(valor) AS cantidad, 'GUIA' AS origen
                        FROM produccion_cajas_real
                        WHERE getYear(fecha) = {$year} AND getWeek(fecha) = {$semana} AND STATUS = 'PROCESADO'
                        GROUP BY fecha, marca
                    ) AS tbl
                    GROUP BY fecha, marca
                ) AS tbl";
        return $this->db->queryOne($sql);
    }
    private function getCajasConvertidasRealesSemana($year, $semana){
        $sql = "SELECT SUM(cajas_reales)
                FROM (
                    SELECT marca, ROUND(SUM(ROUND(IF(blz > guia, blz, guia) * IF(convertir = 0, 1, peso_prom / 0.4536 / 40.5))), 0) AS cajas_reales
                    FROM (
                        SELECT tbl.marca, tbl.fecha, blz, SUM(IFNULL(valor,0)) AS guia, peso_prom, IF(con.id IS NULL, 0, 1) AS convertir
                        FROM (
                            SELECT cajas.marca, fecha, COUNT(1) AS blz, AVG(caja) AS peso_prom
                            FROM produccion_cajas cajas
                            WHERE YEAR = {$year} AND semana = {$semana}
                            GROUP BY cajas.marca, fecha
                        ) AS tbl
                        LEFT JOIN produccion_cajas_real guia ON tbl.fecha = guia.fecha AND tbl.marca = guia.marca AND STATUS = 'PROCESADO'
                        LEFT JOIN produccion_cajas_convertir con ON con.marca = tbl.marca
                        GROUP BY tbl.fecha, tbl.marca
                    ) AS tbl
                    GROUP BY marca
                    UNION ALL
                    SELECT marca, ROUND(SUM(ROUND(IF(blz > guia, blz, guia) * IF(convertir = 0, 1, peso_prom / 0.4536 / 40.5))), 0) AS cajas_reales
                    FROM (
                        SELECT tbl.marca, tbl.fecha, blz, SUM(IFNULL(valor,0)) AS guia, peso_prom, IF(con.id IS NULL, 0, 1) AS convertir
                        FROM (
                            SELECT cajas.marca, fecha, COUNT(1) AS blz, AVG(peso) AS peso_prom
                            FROM produccion_gavetas cajas
                            WHERE YEAR = {$year} AND semana = {$semana}
                            GROUP BY cajas.marca, fecha
                        ) AS tbl
                        LEFT JOIN produccion_cajas_real guia ON tbl.fecha = guia.fecha AND tbl.marca = guia.marca AND STATUS = 'PROCESADO'
                        LEFT JOIN produccion_cajas_convertir con ON con.marca = tbl.marca
                        GROUP BY tbl.fecha, tbl.marca
                    ) AS tbl
                    GROUP BY marca
                ) AS tbl";

        return $this->db->queryOne($sql);
    }
    // inicio && fin : 'yyyy-mm-dd'
    private function getCajasConvertidasRealesInicioFin($inicio, $fin){
        $sql = "SELECT SUM(cajas_reales)
                FROM (
                    SELECT marca, ROUND(SUM(IF(blz > guia, blz, guia) * IF(convertir = 0, 1, peso_prom / 0.4536 / 40.5)), 0) AS cajas_reales
                    FROM (
                        SELECT tbl.marca, tbl.fecha, blz, SUM(IFNULL(valor,0)) AS guia, peso_prom, IF(con.id IS NULL, 0, 1) AS convertir
                        FROM (
                            SELECT cajas.marca, fecha, COUNT(1) AS blz, AVG(caja) AS peso_prom
                            FROM produccion_cajas cajas
                            WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
                            GROUP BY cajas.marca, fecha
                        ) AS tbl
                        LEFT JOIN produccion_cajas_real guia ON tbl.fecha = guia.fecha AND tbl.marca = guia.marca AND STATUS = 'PROCESADO'
                        LEFT JOIN produccion_cajas_convertir con ON con.marca = tbl.marca
                        GROUP BY tbl.fecha, tbl.marca
                    ) AS tbl
                    GROUP BY marca
                    UNION ALL
                    SELECT marca, ROUND(SUM(IF(blz > guia, blz, guia) * IF(convertir = 0, 1, peso_prom / 0.4536 / 40.5)), 0) AS cajas_reales
                    FROM (
                        SELECT tbl.marca, tbl.fecha, blz, SUM(IFNULL(valor,0)) AS guia, peso_prom, IF(con.id IS NULL, 0, 1) AS convertir
                        FROM (
                            SELECT cajas.marca, fecha, COUNT(1) AS blz, AVG(peso) AS peso_prom
                            FROM produccion_gavetas cajas
                            WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
                            GROUP BY cajas.marca, fecha
                        ) AS tbl
                        LEFT JOIN produccion_cajas_real guia ON tbl.fecha = guia.fecha AND tbl.marca = guia.marca AND STATUS = 'PROCESADO'
                        LEFT JOIN produccion_cajas_convertir con ON con.marca = tbl.marca
                        GROUP BY tbl.fecha, tbl.marca
                    ) AS tbl
                    GROUP BY marca
                ) AS tbl";
        return $this->db->queryOne($sql);
    }
    // inicio && fin : 'yyyy-mm-dd'
    private function getCajasRatioCortadoInicioFin($inicio = '', $fin = ''){
        $response = new stdClass;

        $exist_ratio_preprocesado = "SELECT IF(MAX(fecha) <= $fin, 1, 0)+IF(MIN(fecha) >= $inicio, 1, 0) FROM produccion_cajas_ratios_preprocesados";
        $exist_ratio_preprocesado = $this->db->queryOne($exist_ratio_preprocesado) == 2;

        if($exist_ratio_preprocesado){
            $sql = "SELECT ROUND(AVG(ratio_cortado), 2) AS ratio
                    FROM produccion_cajas_ratios_preprocesados
                    WHERE getYear(fecha) = {$year} AND fecha BETWEEN '{$inicio}' AND '{$fin}'";
            $val = $this->db->queryOne($sql);
        }else{
            $racimos = $this->getRacimosCortadosInicioFin($inicio, $fin);
            $cajas_conv = $this->getCajasConvertidasRealesInicioFin($inicio, $fin);
            $val = round(($cajas_conv / $racimos), 2);
        }
        return $val;
    }
    private function getCajasRatioCortadoSemana($year, $semana, $racimos = 0, $cajas_conv = 0){
        $response = new stdClass;

        $sql = "SELECT ROUND(AVG(ratio_cortado), 2) AS ratio
                FROM produccion_cajas_ratios_preprocesados
                WHERE getYear(fecha) = {$year} AND getWeek(fecha) = {$semana}";
        $val = $this->db->queryOne($sql);
        if(!$val){
            if(!$racimos > 0) $racimos = $this->getRacimosCortados($year, $semana);
            if(!$cajas_conv > 0) $cajas_conv = $this->getCajasConvertidasRealesSemana($year, $semana);
            $val = round(($cajas_conv / $racimos), 2);
        }
        return $val;
    }
    private function getCajasRatioProcesadoSemana($year, $semana, $racimos = 0, $cajas_conv = 0){
        $response = new stdClass;

        $sql = "SELECT ROUND(AVG(ratio_procesado), 2) AS ratio
                FROM produccion_cajas_ratios_preprocesados
                WHERE getYear(fecha) = {$year} AND getWeek(fecha) = {$semana}";
        $val = $this->db->queryOne($sql);
        if(!$val){
            if(!$racimos > 0) $racimos = $this->getRacimosProcesados($year, $semana);
            if(!$cajas_conv > 0) $cajas_conv = $this->getCajasConvertidasRealesSemana($year, $semana);
            $val = round(($cajas_conv / $racimos), 2);
        }
        return $val;
    }
    // inicio && fin : 'yyyy-mm-dd'
    private function getRacimosCortadosInicioFin($inicio, $fin){
        $racimos_cortados = "SELECT SUM(IF(blz > form, blz, form)) AS 'value'
            FROM (
                SELECT fecha, lote, cinta, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form
                FROM (
                    SELECT fecha, lote, cinta, COUNT(1) AS cantidad, 'BALANZA' AS origen, tipo
                    FROM produccion_historica
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}'
                    GROUP BY fecha, lote, cinta, tipo
                    UNION ALL
                    SELECT fecha, lote, cinta, SUM(cantidad) AS cantidad, 'FORMULARIO' AS origen, 'PROC' AS tipo
                    FROM(
                        SELECT r.fecha AS fecha, r.lote, r.color AS cinta, SUM(r.cantidad) AS cantidad
                        FROM racimos_cosechados_by_color r
                        INNER JOIN produccion_racimos_cuadrado c ON c.fecha = r.fecha AND c.lote = r.lote AND c.cinta = r.color
                        WHERE r.fecha BETWEEN '{$inicio}' AND '{$fin}' AND c.status = 'PROC'
                        GROUP BY r.fecha, r.lote, r.color
                        UNION ALL
                        SELECT DATE(r.fecha), r.lote, d.color_cinta, COUNT(1) * -1 AS cantidad
                        FROM racimos_cosechados r
                        INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                        INNER JOIN produccion_racimos_cuadrado c ON c.fecha = DATE(r.fecha) AND c.lote = r.lote AND c.cinta = d.color_cinta 
                        WHERE r.fecha BETWEEN '{$inicio}' AND '{$fin}' AND c.status = 'PROC'
                        GROUP BY DATE(r.fecha), r.lote, d.color_cinta
                    ) AS tbl
                    GROUP BY fecha, lote, cinta
                    UNION ALL
                    SELECT DATE(r.fecha), r.lote, d.color_cinta, COUNT(1) AS cantidad, 'FORMULARIO' AS origen, 'RECUSADO' AS tipo
                    FROM racimos_cosechados r
                    INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                    INNER JOIN produccion_racimos_cuadrado c ON c.fecha = DATE(r.fecha) AND c.lote = r.lote AND c.cinta = d.color_cinta 
                    WHERE r.fecha BETWEEN '{$inicio}' AND '{$fin}' AND c.status = 'PROC'
                    GROUP BY DATE(r.fecha), r.lote, d.color_cinta
                ) AS tbl
                GROUP BY fecha, lote, cinta
            ) AS tbl";

        return $this->db->queryOne($racimos_cortados);
    }
    // inicio && fin : 'yyyy-mm-dd'
    private function getRacimosProcesadosInicioFin($inicio, $fin){
        $racimos_cortados = "SELECT SUM(IF(blz > form, blz, form)) AS 'value'
            FROM (
                SELECT fecha, lote, cinta, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form
                FROM (
                    SELECT fecha, lote, cinta, COUNT(1) AS cantidad, 'BALANZA' AS origen
                    FROM produccion_historica
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND tipo = 'PROC'
                    GROUP BY fecha, lote, cinta
                    UNION ALL
                    SELECT fecha, lote, cinta, SUM(cantidad) AS cantidad, 'FORMULARIO' AS origen
                    FROM(
                        SELECT r.fecha AS fecha, r.lote, r.color AS cinta, SUM(r.cantidad) AS cantidad
                        FROM racimos_cosechados_by_color r
                        INNER JOIN produccion_racimos_cuadrado c ON c.fecha = r.fecha AND c.lote = r.lote AND c.cinta = r.color
                        WHERE r.fecha BETWEEN '{$inicio}' AND '{$fin}' AND c.status = 'PROC'
                        GROUP BY r.fecha, r.lote, r.color
                        UNION ALL
                        SELECT DATE(r.fecha), r.lote, d.color_cinta, COUNT(1) * -1 AS cantidad
                        FROM racimos_cosechados r
                        INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                        INNER JOIN produccion_racimos_cuadrado c ON c.fecha = DATE(r.fecha) AND c.lote = r.lote AND c.cinta = d.color_cinta 
                        WHERE r.fecha BETWEEN '{$inicio}' AND '{$fin}' AND c.status = 'PROC'
                        GROUP BY DATE(r.fecha), r.lote, d.color_cinta
                    ) AS tbl
                    GROUP BY fecha, lote, cinta
                ) AS tbl
                GROUP BY fecha, lote, cinta
            ) AS tbl";
        return $this->db->queryOne($racimos_cortados);
    }
    private function getRacimosCortados($year, $semana){
        $racimos_cortados = "SELECT SUM(IF(blz > form, blz, form)) AS 'value'
            FROM (
                SELECT fecha, lote, cinta, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form
                FROM (
                    SELECT fecha, lote, cinta, COUNT(1) AS cantidad, 'BALANZA' AS origen, tipo
                    FROM produccion_historica
                    WHERE YEAR = {$year} AND semana = {$semana}
                    GROUP BY fecha, lote, cinta, tipo
                    UNION ALL
                    SELECT fecha, lote, cinta, SUM(cantidad) AS cantidad, 'FORMULARIO' AS origen, 'PROC' AS tipo
                    FROM(
                        SELECT r.fecha AS fecha, r.lote, r.color AS cinta, SUM(r.cantidad) AS cantidad
                        FROM racimos_cosechados_by_color r
                        INNER JOIN produccion_racimos_cuadrado c ON c.fecha = r.fecha AND c.lote = r.lote AND c.cinta = r.color
                        WHERE year = {$year} AND semana = {$semana} AND c.status = 'PROC'
                        GROUP BY r.fecha, r.lote, r.color
                        UNION ALL
                        SELECT DATE(r.fecha), r.lote, d.color_cinta, COUNT(1) * -1 AS cantidad
                        FROM racimos_cosechados r
                        INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                        INNER JOIN produccion_racimos_cuadrado c ON c.fecha = DATE(r.fecha) AND c.lote = r.lote AND c.cinta = d.color_cinta 
                        WHERE year = {$year} AND semana = {$semana} AND c.status = 'PROC'
                        GROUP BY DATE(r.fecha), r.lote, d.color_cinta
                    ) AS tbl
                    GROUP BY fecha, lote, cinta
                    UNION ALL
                    SELECT DATE(r.fecha), r.lote, d.color_cinta, COUNT(1) AS cantidad, 'FORMULARIO' AS origen, 'RECUSADO' AS tipo
                    FROM racimos_cosechados r
                    INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                    INNER JOIN produccion_racimos_cuadrado c ON c.fecha = DATE(r.fecha) AND c.lote = r.lote AND c.cinta = d.color_cinta 
                    WHERE year = {$year} AND semana = {$semana} AND c.status = 'PROC'
                    GROUP BY DATE(r.fecha), r.lote, d.color_cinta
                ) AS tbl
                GROUP BY fecha, lote, cinta
            ) AS tbl";

        return $this->db->queryOne($racimos_cortados);
    }
    private function getRacimosProcesados($year, $semana){
        $racimos_cortados = "SELECT SUM(IF(blz > form, blz, form)) AS 'value'
            FROM (
                SELECT fecha, lote, cinta, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form
                FROM (
                    SELECT fecha, lote, cinta, COUNT(1) AS cantidad, 'BALANZA' AS origen
                    FROM produccion_historica
                    WHERE year = {$year} AND semana = {$semana} AND tipo = 'PROC'
                    GROUP BY fecha, lote, cinta
                    UNION ALL
                    SELECT fecha, lote, cinta, SUM(cantidad) AS cantidad, 'FORMULARIO' AS origen
                    FROM(
                        SELECT r.fecha AS fecha, r.lote, r.color AS cinta, SUM(r.cantidad) AS cantidad
                        FROM racimos_cosechados_by_color r
                        INNER JOIN produccion_racimos_cuadrado c ON c.fecha = r.fecha AND c.lote = r.lote AND c.cinta = r.color
                        WHERE year = {$year} AND semana = {$semana} AND c.status = 'PROC'
                        GROUP BY r.fecha, r.lote, r.color
                        UNION ALL
                        SELECT DATE(r.fecha), r.lote, d.color_cinta, COUNT(1) * -1 AS cantidad
                        FROM racimos_cosechados r
                        INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                        INNER JOIN produccion_racimos_cuadrado c ON c.fecha = DATE(r.fecha) AND c.lote = r.lote AND c.cinta = d.color_cinta 
                        WHERE year = {$year} AND semana = {$semana} AND c.status = 'PROC'
                        GROUP BY DATE(r.fecha), r.lote, d.color_cinta
                    ) AS tbl
                    GROUP BY fecha, lote, cinta
                ) AS tbl
                GROUP BY fecha, lote, cinta
            ) AS tbl";
        return $this->db->queryOne($racimos_cortados);
    }
    private function getPesoRacimosCortados($year, $semana){
        $racimos_cortados = $this->db->queryAll("SELECT fecha, lote, cinta, edad, SUM(IF(blz > form, blz, form)) AS cantidad, tipo
            FROM (
                SELECT fecha, lote, cinta, edad, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form, tipo
                FROM (
                    SELECT fecha, lote, cinta, edad, COUNT(1) AS cantidad, tipo, 'BALANZA' AS origen
                    FROM produccion_historica
                    WHERE YEAR = {$year} AND semana = {$semana}
                    GROUP BY fecha, lote, cinta, tipo
                    -- racimos procesados
                    UNION ALL
                    SELECT r.fecha, r.lote, r.color, edad, SUM(cantidad) AS cantidad, 'PROC' AS tipo, 'FORMULARIO' AS origen
                    FROM `racimos_cosechados_by_color` r
                    LEFT JOIN produccion_racimos_cuadrado cuad ON cuad.fecha = r.fecha AND cuad.lote = r.lote AND cuad.cinta = r.color
                    WHERE year = {$year} AND semana = {$semana} AND cuad.status = 'PROC'
                    GROUP BY r.fecha, r.lote, color
                    UNION ALL
                    SELECT DATE(r.fecha), r.lote, d.color_cinta, getEdadCinta($semana, d.color_cinta, $year), COUNT(1) * -1, 'PROC' AS tipo, 'FORMULARIO' AS origen
                    FROM racimos_cosechados r
                    INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                    LEFT JOIN produccion_racimos_cuadrado cuad ON cuad.fecha = DATE(r.fecha) AND cuad.lote = r.lote AND cuad.cinta = d.color_cinta
                    WHERE year = {$year} AND semana = {$semana} AND cuad.status = 'PROC'
                    GROUP BY DATE(r.fecha), d.color_cinta, r.lote
                ) AS tbl
                GROUP BY fecha, lote, cinta, tipo
            ) AS tbl
            GROUP BY fecha, lote, cinta, tipo
            UNION ALL
            SELECT DATE(r.fecha), r.lote, d.color_cinta, getEdadCinta($semana, d.color_cinta, $year), COUNT(1), 'RECUSADO' AS tipo
            FROM racimos_cosechados r
            INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
            LEFT JOIN produccion_racimos_cuadrado cuad ON cuad.fecha = DATE(r.fecha) AND cuad.lote = r.lote AND cuad.cinta = d.color_cinta
            WHERE year = {$year} AND semana = {$semana} AND cuad.status = 'PROC'
            GROUP BY DATE(r.fecha), d.color_cinta, r.lote");
        
        //if($this->db->queryOne("SELECT {$semana}") == 50) F($racimos_cortados);

        $racimos_cortados_balanza = $this->db->queryAll("SELECT fecha, lote, cinta, edad, COUNT(1) AS cantidad, SUM(peso) AS peso_racimos, ROUND(AVG(peso), 2) AS peso_prom, tipo
            FROM produccion_historica
            WHERE YEAR = {$year} AND semana = {$semana}
            GROUP BY fecha, lote, cinta, tipo");

        $peso_racimos = 0;
        foreach($racimos_cortados as $racimo){
            $exist = false;
            $racimo_blz = new stdClass;

            foreach($racimos_cortados_balanza as $res){
                if($racimo->fecha == $res->fecha && $racimo->cinta == $res->cinta && $racimo->lote == $res->lote && $racimo->tipo == $res->tipo){
                    $exist = true;
                    $racimo_blz = $res;
                    break;
                }
            }

            if($exist){
                if($racimo->cantidad > $racimo_blz->cantidad){
                    /*if($this->db->queryOne("SELECT {$semana}") == 50){ 
                        D("FECHA = $racimo->fecha, CINTA = {$racimo->cinta}, LOTE = $racimo->lote, PESO = $racimo_blz->peso_racimos + (($racimo->cantidad - $racimo_blz->cantidad) * $racimo_blz->peso_prom)");
                    }*/
                    $peso_racimos += $racimo_blz->peso_racimos + (($racimo->cantidad - $racimo_blz->cantidad) * $racimo_blz->peso_prom);
                }else{
                    /*if($this->db->queryOne("SELECT {$semana}") == 50) {
                        D("FECHA = $racimo->fecha, CINTA = {$racimo->cinta}, LOTE = $racimo->lote, PESO = $racimo_blz->peso_racimos");
                    }*/
                    $peso_racimos += $racimo_blz->peso_racimos;
                }
            }else{
                if($racimo->tipo == 'PROC'){
                    $ultima_cinta = $this->db->queryRow("SELECT year, semana FROM produccion_historica WHERE cinta = '{$racimo->cinta}' AND edad = '{$racimo->edad}' AND tipo = 'PROC' AND IF((year = {$year} AND semana <= {$semana}) OR year < {$year}, 1, 0) = 1 AND lote = '{$racimo->lote}' GROUP BY year, semana ORDER BY year DESC, semana DESC LIMIT 1");
                    $peso_prom = $this->db->queryOne("SELECT AVG(peso) FROM produccion_historica WHERE cinta = '{$racimo->cinta}' AND year = '{$ultima_cinta->year}' AND semana = '{$ultima_cinta->semana}' AND tipo = 'PROC' AND lote = '{$racimo->lote}'");
                }else{
                    $ultima_cinta = $this->db->queryRow("SELECT year, semana FROM produccion_historica WHERE cinta = '{$racimo->cinta}' AND edad = '{$racimo->edad}' AND tipo = 'RECUSADO' AND IF((year = {$year} AND semana <= {$semana}) OR year < {$year}, 1, 0) = 1 AND lote = '{$racimo->lote}' GROUP BY year, semana ORDER BY year DESC, semana DESC LIMIT 1");
                    $peso_prom = $this->db->queryOne("SELECT AVG(peso) FROM produccion_historica WHERE cinta = '{$racimo->cinta}' AND year = '{$ultima_cinta->year}' AND semana = '{$ultima_cinta->semana}' AND tipo = 'RECUSADO' AND lote = '{$racimo->lote}'");
                }
                $peso_racimos += round($racimo->cantidad * $peso_prom, 2);
            }
        }
        return $peso_racimos;
    }
    private function getPesoRacimosProcesados($year, $semana){
        $racimos_cortados = $this->db->queryAll("SELECT fecha, lote, cinta, edad, SUM(IF(blz > form, blz, form)) AS cantidad, tipo
            FROM (
                SELECT fecha, lote, cinta, edad, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form, tipo
                FROM (
                    SELECT fecha, lote, cinta, edad, COUNT(1) AS cantidad, tipo, 'BALANZA' AS origen
                    FROM produccion_historica
                    WHERE YEAR = {$year} AND semana = {$semana} AND tipo = 'PROC'
                    GROUP BY fecha, lote, cinta
                    -- racimos procesados
                    UNION ALL
                    SELECT r.fecha, r.lote, r.color, r.edad, SUM(r.cantidad) AS cantidad, 'PROC' AS tipo, 'FORMULARIO' AS origen
                    FROM `racimos_cosechados_by_color` r
                    LEFT JOIN produccion_racimos_cuadrado cuad ON cuad.fecha = r.fecha AND cuad.lote = r.lote AND cuad.cinta = r.color
                    WHERE year = {$year} AND semana = {$semana} AND cuad.status = 'PROC'
                    GROUP BY r.fecha, r.lote, r.color
                    UNION ALL
                    SELECT DATE(r.fecha), r.lote, d.color_cinta, getEdadCinta($semana, d.color_cinta, $year), COUNT(1) * -1, 'PROC' AS tipo, 'FORMULARIO' AS origen
                    FROM racimos_cosechados r
                    INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                    LEFT JOIN produccion_racimos_cuadrado cuad ON cuad.fecha = DATE(r.fecha) AND cuad.lote = r.lote AND cuad.cinta = d.color_cinta
                    WHERE year = {$year} AND semana = {$semana} AND cuad.status = 'PROC'
                    GROUP BY DATE(r.fecha), d.color_cinta, r.lote
                ) AS tbl
                GROUP BY fecha, lote, cinta, tipo
            ) AS tbl
            GROUP BY fecha, lote, cinta, tipo");
            
        $racimos_cortados_balanza = $this->db->queryAll("SELECT fecha, lote, cinta, edad, COUNT(1) AS cantidad, SUM(peso) AS peso_racimos, ROUND(AVG(peso), 2) AS peso_prom, tipo
            FROM produccion_historica
            WHERE YEAR = {$year} AND semana = {$semana} AND tipo = 'PROC'
            GROUP BY fecha, lote, cinta");
        
        foreach($racimos_cortados as $racimo){
            $exist = false;
            $racimo_blz = new stdClass;

            foreach($racimos_cortados_balanza as $res){
                if($racimo->fecha == $res->fecha && $racimo->cinta == $res->cinta && $racimo->lote == $res->lote){
                    $exist = true;
                    $racimo_blz = $res;
                    break;
                }
            }

            if($exist){
                if($racimo->cantidad > $racimo_blz->cantidad){
                    $peso_racimos += $racimo_blz->peso_racimos + (($racimo->cantidad - $racimo_blz->cantidad) * $racimo_blz->peso_prom);
                }else{
                    $peso_racimos += $racimo_blz->peso_racimos;
                }
            }else{
                $ultima_cinta = $this->db->queryRow("SELECT year, semana FROM produccion_historica WHERE cinta = '{$racimo->cinta}' AND edad = '{$racimo->edad}' AND tipo = 'PROC' AND IF((year = {$year} AND semana <= {$semana}) OR year < {$year}, 1, 0) = 1 AND lote = '{$racimo->lote}' GROUP BY year, semana ORDER BY year DESC, semana DESC LIMIT 1");
                $peso_prom = $this->db->queryOne("SELECT AVG(peso) FROM produccion_historica WHERE cinta = '{$racimo->cinta}' AND year = '{$ultima_cinta->year}' AND semana = '{$ultima_cinta->semana}' AND tipo = 'PROC' AND lote = '{$racimo->lote}'");
                $peso_racimos += round($racimo->cantidad * $peso_prom, 2);
            }
        }
        return $peso_racimos;
    }
    private function getPesoCajas($year, $semana){
        $cajas = $this->db->queryAll("SELECT fecha, marca, SUM(IF(blz > form, blz, form)) AS cantidad, tipo_caja
            FROM (
                SELECT fecha, marca, cantidad, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form, tipo_caja
                FROM (
                    SELECT fecha, marca, COUNT(1) AS cantidad, 'BALANZA' AS origen, 'CAJA' AS tipo_caja
                    FROM produccion_cajas
                    WHERE year = {$year} AND semana = {$semana}
                    GROUP BY fecha, marca
                    UNION ALL
                    SELECT fecha, marca, COUNT(1) AS cantidad, 'BALANZA' AS origen, 'GAVETA' AS tipo_caja
                    FROM produccion_gavetas
                    WHERE year = {$year} AND semana = {$semana}
                    GROUP BY fecha, marca
                    -- cuadre de cajas
                    UNION ALL
                    SELECT fecha, marca, SUM(valor) AS cantidad, 'FORMULARIO' AS origen, '' AS tipo_caja
                    FROM produccion_cajas_real
                    WHERE getYear(fecha) = {$year} AND getWeek(fecha) = {$semana} AND status = 'PROCESADO'
                    GROUP BY fecha, marca
                ) AS tbl
                GROUP BY fecha, marca
            ) AS tbl
            GROUP BY fecha, marca");
        
        $cajas_balanza = $this->db->queryAll("SELECT fecha, marca, cantidad, peso_cajas, peso_prom
            FROM (
                SELECT fecha, marca, COUNT(1) AS cantidad, SUM(caja) AS peso_cajas, AVG(caja) AS peso_prom
                FROM produccion_cajas
                WHERE year = {$year} AND semana = {$semana}
                GROUP BY fecha, marca
                UNION ALL
                SELECT fecha, marca, COUNT(1) AS cantidad, SUM(peso) AS peso_cajas, AVG(peso) AS peso_prom
                FROM produccion_gavetas
                WHERE year = {$year} AND semana = {$semana}
                GROUP BY fecha, marca
            ) AS tbl
            GROUP BY fecha, marca");
        
        $peso_cajas = 0;
        foreach($cajas as $caja){
            $exist = false;
            $caja_blz = new stdClass;

            foreach($cajas_balanza as $res){
                if($caja->fecha == $res->fecha && $caja->marca == $res->marca){
                    $exist = true;
                    $caja_blz = $res;
                    break;
                }
            }

            if($exist){
                if($caja->cantidad > $caja_blz->cantidad){
                    #D("FECHA = {$caja->fecha}, MARCA = {$caja->marca}, PESO = $caja_blz->peso_cajas + (($caja->cantidad - $caja_blz->cantidad) * $caja_blz->peso_prom)");
                    $peso_cajas += $caja_blz->peso_cajas + (($caja->cantidad - $caja_blz->cantidad) * $caja_blz->peso_prom);
                }else{
                    #D("FECHA = {$caja->fecha}, MARCA = {$caja->marca}, PESO = {$peso_cajas}");
                    $peso_cajas += $caja_blz->peso_cajas;
                }
            }else{
                if($caja->tipo_caja == 'CAJA'){
                    $ultima_caja = $this->db->queryRow("SELECT year, semana FROM produccion_cajas WHERE IF((year = {$year} AND semana <= {$semana}) OR year < {$year}, 1, 0) = 1 GROUP BY year, semana ORDER BY year DESC, semana DESC LIMIT 1");
                    $peso_prom = $this->db->queryOne("SELECT AVG(caja) FROM produccion_cajas WHERE marca = '{$caja->marca}' AND year = '{$ultima_caja->year}' AND semana = '{$ultima_caja->semana}'");
                }else{
                    $ultima_caja = $this->db->queryRow("SELECT year, semana FROM produccion_gavetas WHERE IF((year = {$year} AND semana <= {$semana}) OR year < {$year}, 1, 0) = 1 GROUP BY year, semana ORDER BY year DESC, semana DESC LIMIT 1");
                    $peso_prom = $this->db->queryOne("SELECT AVG(peso) FROM produccion_gavetas WHERE marca = '{$caja->marca}' AND year = '{$ultima_caja->year}' AND semana = '{$ultima_caja->semana}'");
                }
                $peso_cajas += round($caja->cantidad * $peso_prom, 2);
            }
        }
        return $peso_cajas;
    }
    private function getMermaCortadaSemana($year, $semana){
        $peso_cajas = $this->getPesoCajas($year, $semana);
        if($peso_cajas == 0) return '';
        $peso_racimos = $this->getPesoRacimosCortados($year, $semana);
        $val = round(($peso_racimos - $peso_cajas) / $peso_racimos * 100, 2);
        return $val;
    }
    private function gerMermaProcesadaSemana($year, $semana){
        $peso_cajas = $this->getPesoCajas($year, $semana);
        if($peso_cajas == 0) return '';
        $peso_racimos = $this->getPesoRacimosProcesados($year, $semana);
        $val = round(($peso_racimos - $peso_cajas) / $peso_racimos * 100, 2);
        #D("SEMANA {$semana}, RACIMOS = {$peso_racimos}, CAJAS = {$peso_cajas}");
        return $val;
    }
    private function getMermaNeta($year, $semana){
        $sql = "SELECT 
                    (SELECT SUM(cantidad) 
                    FROM merma 
                    INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                    WHERE getYear(fecha) = {$year}
                        AND getWeek(fecha) = {$semana}
                        AND porcentaje_merma > 0 
                        AND campo = 'Total Peso Merma (lb)' 
                        $sWhere) / SUM(peso_neto) * 100 AS val
                FROM(
                    SELECT SUM(peso_neto) AS peso_neto, getWeek(fecha) AS semana
                    FROM merma
                    WHERE getYear(fecha) = {$year} AND porcentaje_merma > 0 AND getWeek(fecha) = {$semana}
                    GROUP BY getWeek(fecha)
                ) AS tbl";
        return round($this->db->queryOne($sql), 2);
    }
    /* GET VALUES */



	public function reporteProduccionViejo(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }

        $response->data = $this->db->queryAll("SELECT getYear(CURRENT_DATE) AS anio, 
                STR_TO_DATE(CONCAT(getYear(CURRENT_DATE), semana,' Monday'), '%X%V %W') AS start,
                STR_TO_DATE(CONCAT(getYear(CURRENT_DATE), semana,' Saturday'), '%X%V %W') AS end,
                semana,
                (SELECT COUNT(1) FROM produccion_historica WHERE year = getYear(CURRENT_DATE) AND semana = tbl.semana AND tipo = 'PROC' {$sWhere}) AS racimos_procesados,
                (SELECT COUNT(1) FROM produccion_historica WHERE year = getYear(CURRENT_DATE) AND semana = tbl.semana AND tipo = 'RECUSADO' {$sWhere}) AS racimos_recusados,
                (SELECT SUM(peso) FROM produccion_historica WHERE year = getYear(CURRENT_DATE) AND semana = tbl.semana {$sWhere}) AS peso_racimos_cortados,
                (SELECT SUM(peso) FROM produccion_historica WHERE year = getYear(CURRENT_DATE) AND semana = tbl.semana AND tipo = 'PROC' {$sWhere}) AS peso_racimos_procesados,
                ROUND((SELECT AVG(peso) FROM produccion_historica WHERE year = getYear(CURRENT_DATE) AND semana = tbl.semana AND tipo = 'PROC' {$sWhere}), 2) AS peso_prom_racimos,
                (SELECT COUNT(1) FROM produccion_cajas WHERE getYear(fecha) = getYear(CURRENT_DATE) AND semana = tbl.semana )+(SELECT COUNT(1) FROM produccion_gavetas WHERE YEAR(fecha) = YEAR(CURRENT_DATE) AND semana = tbl.semana {$sWhere}) AS cajas,
                (SELECT 
                    IF(conver.id IS NOT NULL, 
                        ROUND(((SUM(cajas.`caja`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.caja), 0)) / 0.4536) / 40.5, 0),
                        COUNT(1))
                    FROM produccion_cajas cajas
                    LEFT JOIN `produccion_cajas_real` `cajas_real`
                                ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                                    AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                                    AND STATUS = 'PROC')
                    LEFT JOIN produccion_cajas_convertir conver ON cajas.marca = conver.marca
                    WHERE getYear(cajas.fecha) = getYear(CURRENT_DATE) AND cajas.marca != '' AND semana = tbl.semana {$sWhere})
                +
                (SELECT 
                    IF(conver.id IS NOT NULL, 
                        ROUND(((SUM(cajas.`peso`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.peso), 0)) / 0.4536) / 40.5, 0),
                        COUNT(1))
                    FROM produccion_gavetas cajas
                    LEFT JOIN `produccion_cajas_real` `cajas_real`
                                ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                                    AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                                    AND STATUS = 'PROC')
                    LEFT JOIN produccion_cajas_convertir conver ON cajas.marca = conver.marca
                    WHERE getYear(cajas.fecha) = getYear(CURRENT_DATE) AND cajas.marca != '' AND semana = tbl.semana {$sWhere}) AS 'conv'
            FROM (
                SELECT semana 
                FROM (
                    SELECT semana
                    FROM produccion_historica
                    WHERE semana >= 0 AND getYear(fecha) = getYear(CURRENT_DATE)  {$sWhere}
                    GROUP BY semana
                    UNION ALL
                    SELECT getWeek(fecha)
                    FROM merma
                    WHERE getWeek(fecha) >= 0 AND getYear(fecha) = getYear(CURRENT_DATE) {$sWhere}
                    GROUP BY getWeek(fecha)
                    UNION ALL
                    SELECT semana
                    FROM produccion_cajas
                    WHERE semana >= 0 AND getYear(fecha) = getYear(CURRENT_DATE) {$sWhere}
                    GROUP BY semana
                ) AS tbl
                GROUP BY semana
                ORDER BY semana
            ) AS tbl
            HAVING racimos_procesados > 0 OR racimos_recusados > 0 OR cajas > 0");
        foreach($response->data as $row){
            $row->racimos_procesados = (int) $row->racimos_procesados;
            $row->racimos_recusados = (int) $row->racimos_recusados;

            $cajas_conv = $this->getResumenCajas(["fecha_inicial" => $row->start, "fecha_final" => $row->end, "finca" => $filters->finca]);
            foreach($cajas_conv as $r){
                $row->cajas_conv += $r->conv;
                $row->peso_cajas += $r->total_kg;
            }

            $merma_neta = "SELECT 
                                (SELECT SUM(cantidad) 
                                FROM merma 
                                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                WHERE getYear(fecha) = getYear(CURRENT_DATE) 
                                    AND getWeek(fecha) = tbl.semana
                                    AND porcentaje_merma > 0 
                                    AND campo = 'Total Peso Merma (lb)' 
                                    $sWhere) / SUM(peso_neto) * 100 AS val
                            FROM(
                                SELECT SUM(peso_neto) AS peso_neto, getWeek(fecha) AS semana
                                FROM merma
                                WHERE getYear(fecha) = getYear(CURRENT_DATE) AND porcentaje_merma > 0 AND getWeek(fecha) = $row->semana {$sWhere}
                                GROUP BY getWeek(fecha)
                            ) AS tbl";
            $row->merma_neta = round($this->db->queryOne($merma_neta), 2);
            $row->detalle = $this->reporteProduccionLote($row->semana, $row->start, $row->end);
        }

        return $response;
    }

    public function reporteProduccionLote($semana, $start_of_week, $end_of_week){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }

        $response->data = $this->db->queryAll("SELECT getYear(CURRENT_DATE) AS anio, lote,
                (SELECT COUNT(1) FROM produccion_historica WHERE getYear(fecha) = getYear(CURRENT_DATE) AND semana = {$semana} AND lote = tbl.lote AND tipo = 'PROC' {$sWhere}) AS racimos_procesados,
                (SELECT COUNT(1) FROM produccion_historica WHERE getYear(fecha) = getYear(CURRENT_DATE) AND semana = {$semana} AND lote = tbl.lote AND tipo = 'RECUSADO' {$sWhere}) AS racimos_recusados,
                (SELECT SUM(peso) FROM produccion_historica WHERE getYear(fecha) = getYear(CURRENT_DATE) AND semana = {$semana} AND lote = tbl.lote {$sWhere}) AS peso_racimos_cortados,
                (SELECT SUM(peso) FROM produccion_historica WHERE getYear(fecha) = getYear(CURRENT_DATE) AND semana = {$semana} AND lote = tbl.lote AND tipo = 'PROC' {$sWhere}) AS peso_racimos_procesados,
                ROUND((SELECT AVG(peso) FROM produccion_historica WHERE getYear(fecha) = getYear(CURRENT_DATE) AND semana = {$semana} AND lote = tbl.lote AND tipo = 'PROC' {$sWhere}), 2) AS peso_prom_racimos
            FROM (
                SELECT lote 
                FROM (
                    SELECT lote
                    FROM produccion_historica
                    WHERE lote != '' AND getYear(fecha) = getYear(CURRENT_DATE) AND semana = {$semana} {$sWhere}
                    GROUP BY lote
                    UNION ALL
                    SELECT bloque
                    FROM merma
                    WHERE bloque != '' AND getYear(fecha) = getYear(CURRENT_DATE) AND getWeek(fecha) = {$semana} {$sWhere}
                    GROUP BY bloque
                ) AS tbl
                GROUP BY lote
                ORDER BY lote
            ) AS tbl");
        foreach($response->data as $row){
            $row->racimos_procesados = (int) $row->racimos_procesados;
            $row->racimos_recusados = (int) $row->racimos_recusados;

            $merma_neta = "SELECT 
                        (SELECT SUM(cantidad) 
                        FROM merma 
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE getWeek(fecha) = $semana AND bloque = tbl.bloque
                            AND porcentaje_merma > 0 
                            AND campo = 'Total Peso Merma (lb)' 
                            $sWhere) / SUM(peso_neto) * 100 AS val
                    FROM(
                        SELECT SUM(peso_neto) AS peso_neto, bloque
                        FROM merma
                        WHERE porcentaje_merma > 0 AND getWeek(fecha) = $semana AND bloque = '$row->lote' {$sWhere}
                        GROUP BY bloque
                    ) AS tbl";
            $row->merma_neta = round($this->db->queryOne($merma_neta), 2);
        }
        return $response->data;
    }

    public function graficaVariables(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }

        $variables = [
            "Peso" => "SELECT semana AS label_x, ROUND(AVG(peso), 2) AS value, 0 AS index_y, 'Peso' AS name
                        FROM produccion_historica
                        WHERE YEAR = {$filters->year}
                        GROUP BY semana
                        ORDER BY semana",
            "Manos" => "SELECT semana AS label_x, ROUND(AVG(manos), 2) AS value, 0 AS index_y, 'Manos' AS name
                        FROM produccion_historica
                        WHERE YEAR = {$filters->year}
                        GROUP BY semana
                        ORDER BY semana",
            "Calibre" => "SELECT semana AS label_x, ROUND(AVG(calibre), 2) AS value, 0 AS index_y, 'Calibre' AS name
                            FROM produccion_historica
                            WHERE YEAR = {$filters->year} AND calibre > 0
                            GROUP BY semana
                            ORDER BY semana",
            "Edad" => "SELECT semana AS label_x, ROUND(AVG(edad), 2) AS value, 0 AS index_y, 'Edad' AS name
                        FROM produccion_historica
                        WHERE YEAR = {$filters->year} AND edad > 0
                        GROUP BY semana
                        ORDER BY semana",
            "Merma Neta" => "SELECT semana AS label_x,
                                ROUND((SELECT SUM(cantidad) 
                                    FROM merma 
                                    INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                    WHERE getWeek(fecha) = tbl.semana
                                        AND porcentaje_merma > 0 
                                        AND campo = 'Total Peso Merma (lb)' 
                                        AND getYear(fecha) = {$filters->year}
                                        $sWhere) / SUM(peso_neto) * 100, 2) AS value,
                                0 AS index_y, 'Merma Neta' AS name
                            FROM(
                                SELECT SUM(peso_neto) AS peso_neto, getWeek(fecha) AS semana
                                FROM merma
                                WHERE porcentaje_merma > 0 {$sWhere} AND getYear(fecha) = {$filters->year}
                                GROUP BY getWeek(fecha)
                            ) AS tbl
                            GROUP BY semana",
            "Temp Min." => "SELECT semana AS label_x, temp_minima AS value, 0 AS index_y, 'Temp Min.' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Horas Luz (100)" => "SELECT semana AS label_x, horas_luz_100 AS value, 0 AS index_y, 'Horas Luz (100)' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Horas Luz (150)" => "SELECT semana AS label_x, horas_luz_150 AS value, 0 AS index_y, 'Horas Luz (150)' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Horas Luz (200)" => "SELECT semana AS label_x, horas_luz_200 AS value, 0 AS index_y, 'Horas Luz (200)' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Horas Luz (400)" => "SELECT semana AS label_x, horas_luz_400 AS value, 0 AS index_y, 'Horas Luz (400)' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Racimos Procesados" => "SELECT getWeek(fecha) AS label_x, SUM(IF(blz > form, blz, form)) AS 'value', 0 AS index_y, 'Racimos Procesados' AS 'name'
                                    FROM (
                                        SELECT fecha, lote, cinta, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form
                                        FROM (
                                            SELECT fecha, lote, cinta, COUNT(1) AS cantidad, 'BALANZA' AS origen
                                            FROM produccion_historica
                                            WHERE YEAR = {$filters->year} AND tipo = 'PROC'
                                            GROUP BY fecha, lote, cinta
                                            UNION ALL
                                            SELECT fecha, lote, cinta, SUM(cantidad) AS cantidad, 'FORMULARIO' AS origen
                                            FROM(
                                                SELECT r.fecha AS fecha, r.lote, r.color AS cinta, SUM(r.cantidad) AS cantidad
                                                FROM racimos_cosechados_by_color r
                                                INNER JOIN produccion_racimos_cuadrado c ON c.fecha = r.fecha AND c.lote = r.lote AND c.cinta = r.color
                                                WHERE getYear(r.fecha) = {$filters->year} AND c.status = 'PROC'
                                                GROUP BY r.fecha, r.lote, r.color
                                                UNION ALL
                                                SELECT DATE(r.fecha), r.lote, d.color_cinta, COUNT(1) * -1 AS cantidad
                                                FROM racimos_cosechados r
                                                INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                                                INNER JOIN produccion_racimos_cuadrado c ON c.fecha = DATE(r.fecha) AND c.lote = r.lote AND c.cinta = d.color_cinta 
                                                WHERE getYear(r.fecha) = {$filters->year} AND c.status = 'PROC'
                                                GROUP BY DATE(r.fecha), r.lote, d.color_cinta
                                            ) AS tbl
                                            GROUP BY fecha, lote, cinta
                                        ) AS tbl
                                        GROUP BY fecha, lote, cinta
                                    ) AS tbl
                                    GROUP BY getWeek(fecha)
                                    ORDER BY fecha",
            "Racimos Cosechados" => "SELECT getWeek(fecha) AS label_x, SUM(IF(blz > form, blz, form)) AS 'value', 0 AS index_y, 'Racimos Cosechados' AS 'name'
                                    FROM (
                                        SELECT fecha, lote, cinta, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form
                                        FROM (
                                            SELECT fecha, lote, cinta, COUNT(1) AS cantidad, 'BALANZA' AS origen, tipo
                                            FROM produccion_historica
                                            WHERE YEAR = {$filters->year}
                                            GROUP BY fecha, lote, cinta, tipo
                                            UNION ALL
                                            SELECT fecha, lote, cinta, SUM(cantidad) AS cantidad, 'FORMULARIO' AS origen, 'PROC' AS tipo
                                            FROM(
                                                SELECT r.fecha AS fecha, r.lote, r.color AS cinta, SUM(r.cantidad) AS cantidad
                                                FROM racimos_cosechados_by_color r
                                                INNER JOIN produccion_racimos_cuadrado c ON c.fecha = r.fecha AND c.lote = r.lote AND c.cinta = r.color
                                                WHERE getYear(r.fecha) = {$filters->year} AND c.status = 'PROC'
                                                GROUP BY r.fecha, r.lote, r.color
                                                UNION ALL
                                                SELECT DATE(r.fecha), r.lote, d.color_cinta, COUNT(1) * -1 AS cantidad
                                                FROM racimos_cosechados r
                                                INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                                                INNER JOIN produccion_racimos_cuadrado c ON c.fecha = DATE(r.fecha) AND c.lote = r.lote AND c.cinta = d.color_cinta 
                                                WHERE getYear(r.fecha) = {$filters->year} AND c.status = 'PROC'
                                                GROUP BY DATE(r.fecha), r.lote, d.color_cinta
                                            ) AS tbl
                                            GROUP BY fecha, lote, cinta
                                            UNION ALL
                                            SELECT DATE(r.fecha), r.lote, d.color_cinta, COUNT(1) AS cantidad, 'FORMULARIO' AS origen, 'RECUSADO' AS tipo
                                            FROM racimos_cosechados r
                                            INNER JOIN racimos_cosechados_detalle d ON r.id = id_racimos_cosechados
                                            INNER JOIN produccion_racimos_cuadrado c ON c.fecha = DATE(r.fecha) AND c.lote = r.lote AND c.cinta = d.color_cinta 
                                            WHERE getYear(r.fecha) = {$filters->year} AND c.status = 'PROC'
                                            GROUP BY DATE(r.fecha), r.lote, d.color_cinta
                                        ) AS tbl
                                        GROUP BY fecha, lote, cinta
                                    ) AS tbl
                                    GROUP BY getWeek(fecha)",
            "Cajas" => "SELECT getWeek(fecha) AS label_x, SUM(IF(blz > guia, blz, guia)) AS 'value', 0 AS index_y, 'Cajas' AS 'name'
                        FROM (
                            SELECT fecha, marca, SUM(IF(origen  = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen  = 'GUIA', cantidad, 0)) AS guia
                            FROM (
                                SELECT fecha, marca, COUNT(1) AS cantidad, 'BALANZA' AS origen
                                FROM produccion_cajas
                                WHERE YEAR = {$filters->year}
                                GROUP BY fecha, marca
                                UNION ALL
                                SELECT fecha, marca, COUNT(1) AS cantidad, 'BALANZA' AS origen
                                FROM produccion_gavetas
                                WHERE YEAR = {$filters->year}
                                GROUP BY fecha, marca
                                UNION ALL
                                SELECT fecha, marca, SUM(valor) AS cantidad, 'GUIA' AS origen
                                FROM produccion_cajas_real
                                WHERE getYear(fecha) = {$filters->year} AND STATUS = 'PROCESADO'
                                GROUP BY fecha, marca
                            ) AS tbl
                            GROUP BY fecha, marca
                        ) AS tbl
                        GROUP BY getWeek(fecha)
                        ORDER BY fecha",
            "Temp Max." => "SELECT semana AS label_x, temp_maxima AS value, 0 AS index_y, 'Temp Max.' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Lluvia" => "SELECT semana AS label_x, lluvia AS value, 0 AS index_y, 'Lluvia' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Humedad" => "SELECT semana AS label_x, humedad AS value, 0 AS index_y, 'Humedad' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Rad. Solar" => "SELECT semana AS label_x, rad_solar AS value, 0 AS index_y, 'Rad. Solar' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Tallo" => "SELECT getWeek(fecha) AS label_x, 
                            ROUND(SUM(tallo) / SUM(racimo) * 100, 2) AS value,
                            0 AS index_y, 'Tallo' AS name
                        FROM merma main
                        WHERE getYear(fecha) = {$filters->year} $sWhere
                        GROUP BY getWeek(fecha)",
            "Cosecha" => "SELECT getWeek(fecha) AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE getWeek(fecha) =  getWeek(merma.fecha)
                                                AND campo LIKE 'Total Peso Merma (lb)' AND TYPE = 'RESULTADOS'
                                                AND getYear(fecha) = {$filters->year}
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'COSECHA' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso%' AND TYPE = 'COSECHA' AND getYear(fecha) = {$filters->year}
                                $sWhere
                            GROUP BY getWeek(fecha)",
            "Enfunde" => "SELECT getWeek(fecha) AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE getWeek(fecha) =  getWeek(merma.fecha)
                                                AND campo LIKE 'Total Peso Merma (lb)' AND TYPE = 'RESULTADOS'
                                                AND getYear(fecha) = {$filters->year}
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'ENFUNDE' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso%' AND TYPE = 'ENFUNDE' AND getYear(fecha) = {$filters->year}
                                $sWhere
                            GROUP BY getWeek(fecha)",
            "Campo" => "SELECT getWeek(fecha) AS label_x,
                                ROUND(
                                    (SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma m
                                        INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                        WHERE getWeek(fecha) =  getWeek(merma.fecha)
                                            AND campo LIKE 'Total Peso Merma (lb)' AND TYPE = 'RESULTADOS'
                                            AND getYear(fecha) = {$filters->year}
                                            $sWhere) * 100), 2) AS value,
                                0 AS index_y, 'CAMPO' AS name
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE campo LIKE 'Total Peso%' AND TYPE = 'CAMPO' AND getYear(fecha) = {$filters->year}
                            $sWhere
                        GROUP BY getWeek(fecha)",
            "Animales" => "SELECT getWeek(fecha) AS label_x,
                                ROUND(
                                    (SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma m
                                        INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                        WHERE getWeek(fecha) =  getWeek(merma.fecha)
                                            AND campo LIKE 'Total Peso Merma (lb)' AND TYPE = 'RESULTADOS'
                                            AND getYear(fecha) = {$filters->year}
                                            $sWhere) * 100), 2) AS value,
                                0 AS index_y, 'ANIMALES' AS name
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE campo LIKE 'Total Peso%' AND TYPE = 'ANIMALES' AND getYear(fecha) = {$filters->year}
                            $sWhere
                        GROUP BY getWeek(fecha)",
            "Hongos" => "SELECT getWeek(fecha) AS label_x,
                                ROUND(
                                    (SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma m
                                        INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                        WHERE getWeek(fecha) =  getWeek(merma.fecha)
                                            AND campo LIKE 'Total Peso Merma (lb)' AND TYPE = 'RESULTADOS'
                                            AND getYear(fecha) = {$filters->year}
                                            $sWhere) * 100), 2) AS value,
                                0 AS index_y, 'HONGOS' AS name
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE campo LIKE 'Total Peso%' AND TYPE = 'HONGOS' AND getYear(fecha) = {$filters->year}
                            $sWhere
                        GROUP BY getWeek(fecha)",
            "Empacadora" => "SELECT getWeek(fecha) AS label_x,
                                ROUND(
                                    (SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma m
                                        INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                        WHERE getWeek(fecha) =  getWeek(merma.fecha)
                                            AND campo LIKE 'Total Peso Merma (lb)' AND TYPE = 'RESULTADOS'
                                            AND getYear(fecha) = {$filters->year}
                                            $sWhere) * 100), 2) AS value,
                                0 AS index_y, 'EMPACADORA' AS name
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE campo LIKE 'Total Peso%' AND TYPE = 'EMPACADORA' AND getYear(fecha) = {$filters->year}
                            $sWhere
                        GROUP BY getWeek(fecha)",
            "Fisiologicos" => "SELECT getWeek(fecha) AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE getWeek(fecha) =  getWeek(merma.fecha)
                                                AND campo LIKE 'Total Peso Merma (lb)' AND TYPE = 'RESULTADOS'
                                                AND getYear(fecha) = {$filters->year}
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'FISIOLOGICOS' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso%' AND TYPE = 'FISIOLOGICOS' AND getYear(fecha) = {$filters->year}
                                $sWhere
                            GROUP BY getWeek(fecha)",
            "Ratio Cortado" => "SELECT semana AS label_x, ROUND((conv_cajas + conv_gavetas) / (SELECT COUNT(1) FROM produccion_historica WHERE semana = tbl.semana AND year = {$filters->year}), 2) AS value, 0 AS index_y, 'Ratio Cortado' AS name 
                                FROM (
                                    SELECT semana, 
                                        (SELECT IF(conver.id IS NOT NULL, 
                                            ROUND(((SUM(cajas.`caja`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.caja), 0)) / 0.4536) / 40.5, 0),
                                            COUNT(1))
                                        FROM produccion_cajas cajas
                                        LEFT JOIN `produccion_cajas_real` `cajas_real`
                                                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                                                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                                                        AND STATUS = 'PROCESADO')
                                        LEFT JOIN produccion_cajas_convertir conver ON cajas.marca = conver.marca
                                        WHERE cajas.year = {$filters->year} AND cajas.semana = produccion_cajas.semana) AS conv_cajas,
                                        (SELECT IF(conver.id IS NOT NULL, 
                                            ROUND(((SUM(cajas.`peso`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.peso), 0)) / 0.4536) / 40.5, 0),
                                            COUNT(1))
                                        FROM produccion_gavetas cajas
                                        LEFT JOIN `produccion_cajas_real` `cajas_real`
                                                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                                                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                                                        AND STATUS = 'PROCESADO')
                                        LEFT JOIN produccion_cajas_convertir conver ON cajas.marca = conver.marca
                                        WHERE cajas.year = {$filters->year} AND cajas.semana = produccion_cajas.semana) AS conv_gavetas
                                    FROM produccion_cajas 
                                    GROUP BY semana
                                ) AS tbl
                                WHERE semana IS NOT NULL
                                HAVING value > 0",
            "Ratio Procesado" => "SELECT semana AS label_x, ROUND((conv_cajas + conv_gavetas) / (SELECT COUNT(1) FROM produccion_historica WHERE semana = tbl.semana AND year = {$filters->year} AND tipo = 'PROC'), 2) AS value, 0 AS index_y, 'Ratio Procesado' AS name 
                                FROM (
                                    SELECT semana, 
                                        (SELECT IF(conver.id IS NOT NULL, 
                                            ROUND(((SUM(cajas.`caja`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.caja), 0)) / 0.4536) / 40.5, 0),
                                            COUNT(1))
                                        FROM produccion_cajas cajas
                                        LEFT JOIN `produccion_cajas_real` `cajas_real`
                                                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                                                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                                                        AND STATUS = 'PROCESADO')
                                        LEFT JOIN produccion_cajas_convertir conver ON cajas.marca = conver.marca
                                        WHERE cajas.year = {$filters->year} AND cajas.semana = produccion_cajas.semana) AS conv_cajas,
                                        (SELECT IF(conver.id IS NOT NULL, 
                                            ROUND(((SUM(cajas.`peso`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.peso), 0)) / 0.4536) / 40.5, 0),
                                            COUNT(1))
                                        FROM produccion_gavetas cajas
                                        LEFT JOIN `produccion_cajas_real` `cajas_real`
                                                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                                                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                                                        AND STATUS = 'PROCESADO')
                                        LEFT JOIN produccion_cajas_convertir conver ON cajas.marca = conver.marca
                                        WHERE cajas.year = {$filters->year} AND cajas.semana = produccion_cajas.semana) AS conv_gavetas
                                    FROM produccion_cajas 
                                    GROUP BY semana
                                ) AS tbl
                                WHERE semana IS NOT NULL
                                HAVING value > 0",
            "HVLE (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(hoja_mas_vieja_de_estrias), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' HVLE (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 14
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "Q<5 (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' Q<5 (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 14
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "H3 (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' H3 (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 14
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "H4 (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' H4 (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 14
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "H5 (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' H5 (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 14
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "HT (0S) " => "SELECT label_x, ROUND(AVG(value), 2) value, index_y, 'HT (0S)' AS 'name'
                            FROM (
                                SELECT semana AS label_x,
                                    ROUND(AVG(hojas_totales), 2) AS 'value',
                                    0 AS index_y, 
                                    CONCAT(foco, ' HT (0S)') AS 'name'
                                FROM muestras_haciendas 
                                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                                WHERE semana >= 0 AND anio = {$filters->year}
                                    AND muestras_haciendas.id_hacienda = 14
                                    AND muestras_haciendas.id_usuario = 1
                                    AND muestras_haciendas.tipo_semana = 0
                                GROUP BY foco, semana
                                ORDER BY semana
                            ) tbl
                            GROUP BY label_x",
            "Q<5 (0S)" => "SELECT semana AS label_x,
                    ROUND(AVG(hojas_mas_vieja_libre), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' Q<5 (0S)') AS 'name'
                FROM muestras_haciendas 
                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas.id_hacienda = 14
                    AND muestras_haciendas.id_usuario = 1
                    AND muestras_haciendas.tipo_semana = 0
                GROUP BY foco, semana
                ORDER BY semana",
            "HVLE (0S)" => "SELECT semana AS label_x,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' HVLE (0S)') AS 'name'
                FROM muestras_haciendas 
                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas.id_hacienda = 14
                    AND muestras_haciendas.id_usuario = 1
                    AND muestras_haciendas.tipo_semana = 0
                GROUP BY foco, semana
                ORDER BY semana",
            "HT (11S)" => "SELECT label_x, ROUND(AVG(value), 2) value, index_y, 'HT (11S)' as 'name'
                            FROM (
                                SELECT semana AS label_x,
                                    ROUND(AVG(hojas_totales), 2) AS 'value',
                                    0 AS index_y, 
                                    CONCAT(foco, ' HT (11S)') AS 'name'
                                FROM muestras_haciendas 
                                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                                WHERE semana >= 0 AND anio = {$filters->year}
                                    AND muestras_haciendas.id_hacienda = 14
                                    AND muestras_haciendas.id_usuario = 1
                                    AND muestras_haciendas.tipo_semana = 11
                                GROUP BY foco, semana
                                ORDER BY semana
                            ) tbl
                            GROUP BY label_x",
            "Q<5 (11S)" => "SELECT semana AS label_x,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' Q<5 (11S)') AS 'name'
                FROM muestras_haciendas 
                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas.id_hacienda = 14
                    AND muestras_haciendas.id_usuario = 1
                    AND muestras_haciendas.tipo_semana = 11
                GROUP BY foco, semana
                ORDER BY semana",
            "FOLIAR" => "",
        ];
        
        $data_chart = [];
        $clima = ["Temp Min.", "Horas Luz (400)", "Horas Luz (100)", "Horas Luz (150)", "Horas Luz (200)", "Temp Max.", "Lluvia", "Humedad", "Rad. Solar"];
        $sigat  = ["HVLE (3M)","Q<5 (3M)","H3 (3M)","H4 (3M)","H5 (3M)","HT (0S)","Q<5 (0S)","HVLE (0S)","HT (11S)","Q<5 (11S)","FOLIAR"];
        if($filters->var1 == 'FOLIAR'){
            $data_chart = $this->getEmisionFoliar(14, $filters->year, 0);
        }else{
            if(in_array($filters->var1, $clima) || in_array($filters->var1, $sigat)){
                $data_chart = $this->sigat->queryAll($variables[$filters->var1]);
            }else{
                $data_chart = $this->db->queryAll($variables[$filters->var1]);
            }
        }

        if($filters->var2 == 'FOLIAR'){
            $data_chart = array_merge($data_chart, $this->getEmisionFoliar(14, $filters->year, 1));
        }else{
            $sql = $variables[$filters->var2];
            $sql = str_replace("0 AS index_y", "1 AS index_y", $sql);
            if(in_array($filters->var2, $clima) || in_array($filters->var2, $sigat)){
                $data_chart = array_merge($data_chart, $this->sigat->queryAll($sql));
            }else{
                $data_chart = array_merge($data_chart, $this->db->queryAll($sql));
            }
        }

        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => $filters->var1,
				"type" => 'line',
				'format' => ''
			],
			[
				"name" => $filters->var2,
				"type" => 'line',
				'format' => ''
			]
        ];

        /*$selected = [
            "Edad" => false,
            "Calibre" => false,
            "Peso" => false,
            "Merma Neta" => false,
            "Racimos Procesados" => false,
            "Racimos Cosechados" => true,
            "Cajas" => false,
            'Temp Min.' => true,
            'Horas Luz (400)' => false
        ];*/
        $selected = [];
        $types = [$filters->type1, $filters->type2];
        
        $response->data = $this->grafica_z($data_chart, $groups, $selected, $types);

        return $response;
    }

    private function getEmisionFoliar($finca, $year, $index){
        $sql_get_weeks = "SELECT getWeek(fecha) AS semana, getYear(fecha) AS anio , foco 
            FROM foliar 
			WHERE id_hacienda = $finca AND id_usuario = 1 AND getYear(fecha) = getYear({$year})
			GROUP BY foco, getWeek(fecha) 
			ORDER BY foco, getYear(fecha), getWeek(fecha)";
		$weeks = $this->sigat->queryAll($sql_get_weeks, true);
		$count_foco=0;
		$foco = ['foco' => ''];
		$tabla = array();
		foreach ($weeks as $key => $value) {
			$semana = $weeks[$key]->semana;
			$anio = $weeks[$key]->anio;
			if($foco['foco'] != $weeks[$key]->foco){
				$foco = ['foco' => $weeks[$key]->foco];
				$count_foco=0;
			}


			if($semana == 1){
				$pre_semana = 52;
				$pre_anio = $anio-1;
			}else{
				$pre_semana = $semana-1;
				$pre_anio = $anio;
			}


			if($count_foco > 0){
				$sql = "
					SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM
					(SELECT 
					IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
					IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
					IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
					IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
					IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
					IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
					IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
					IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
					IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
					IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
					actual.foco
					FROM (SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."') AS actual
					JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]->semana." AND YEAR(fecha) = $pre_anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."') AS pasada
					JOIN 
						(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
						FROM
							(SELECT *
							FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
							JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]->semana." AND YEAR(fecha) = $pre_anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
					JOIN 
						(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM
							(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
							FROM
								(SELECT * FROM foliar WHERE getWeek(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."') AS actual
								JOIN (SELECT * FROM foliar WHERE getWeek(fecha) = ".$weeks[$key-1]->semana." AND YEAR(fecha) = $pre_anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."') AS pasada)
							AS tbl_suma)
						AS tbl_division
					";
				$result = $this->sigat->queryAll($sql);
                if($result[0]->foco != ""){
                    $data[$result[0]->foco][] = array($semana,round(((double)$result[0]->valor),2));
                    $tabla[$result[0]->foco][$semana] = (double)round(((double)$result[0]->valor),2);
                }
            }
			$count_foco++;
		}
		$response = (object)[];
		$response->table = (object)$tabla;
        
        $data_chart = [];
        $count = 0;
        foreach($data as $key => $value){
            foreach($value as $val){
                if($val[1] > 0){
                    $data_chart[] = ["value" => $val[1], "label_x" => $val[0], "index_y" => $index, "name" => "{$key} FOLIAR"];
                }else{
                    $data_chart[] = ["value" => NULL, "label_x" => $val[0], "index_y" => $index, "name" => "{$key} FOLIAR"];
                }
            }
            $count++;
        }
        //$response->data = $this->chartInit($data_chart, "vertical", "", "line");
		return $data_chart;
    }

	public function produccionActual(){
		$data = $this->db->queryAll("SELECT *, WEEK(fecha) as semana, CEIL(WEEK(fecha)/4) as periodo FROM produccion WHERE year >= 2017");
		$data_procesada = [];
		foreach($data as $reg){
			if($reg->cosechados_blanco > 0){
				$data_procesada[$reg->semana]["blancas"]["sum_edades"] += $this->calcularEdad("BLANCA", (int) $reg->year, $reg->periodo, (int) $reg->semana);
				$data_procesada[$reg->semana]["blancas"]["prom_edades"][] = $this->calcularEdad("BLANCA", (int) $reg->year, $reg->periodo, (int) $reg->semana);
			}
			if($reg->cosechados_negro > 0){
				$data_procesada[$reg->semana]["negras"]["sum_edades"] += $this->calcularEdad("NEGRA", $reg->year, $reg->periodo, $reg->semana);
				$data_procesada[$reg->semana]["negras"]["prom_edades"][] = $this->calcularEdad("NEGRA", $reg->year, $reg->periodo, $reg->semana);
			}
			if($reg->cosechados_azul > 0){
				$data_procesada[$reg->semana]["azules"]["sum_edades"] += $this->calcularEdad("AZUL", $reg->year, $reg->periodo, $reg->semana);
				$data_procesada[$reg->semana]["azules"]["prom_edades"][] = $this->calcularEdad("AZUL", $reg->year, $reg->periodo, $reg->semana);
			}
		}
		foreach($data_procesada as $semana => $colores){
			foreach($colores as $color => $value){
				$data_sem[$semana][$color] = ($colores[$color]["sum_edades"] / count($colores[$color]["prom_edades"]));
			}
			#$data_sem[$semana]["blancas"] = ($blancas["blancas"]["sum_edades"] / count($blancas["blancas"]["prom_edades"]));
		}
		return $data_sem;
	}

	/*private function calcularEdad($color, $anio, $periodo, $semana){
		$secuencia = [
			"2016" => [
				10 => [ 37 => "CAFE", 38 => "AMARILLA", 39 => "VERDE", 40 => "AZUL"],
				11 => [ 41 => "BLANCA", 42 => "NEGRA", 43 => "LILA", 44 => "ROJA" ], 
				12 => [ 45 => "CAFE", 46 => "AMARILLA", 47 => "VERDE", 48 => "AZUL" ],
				13 => [ 49 => "BLANCA", 50 => "NEGRA", 51 => "LILA", 52 => "ROJA" ]
			],
			"2017" => [
				1 => [ 1 => "ROJA-CAFE", 2 => "CAFE", 3 => "NEGRA", 4 => "VERDE" ],
				2 => [ 5 => "AZUL", 6 => "BLANCA", 7 => "AMARILLA", 8 => "LILA", ],
				3 => [ 9 => "ROJA", 10 => "CAFE", 11 => "NEGRA", 12 => "VERDE" ],
				4 => [ 13 => "AZUL", 14 => "BLANCA", 15 => "AMARILLA", 16 => "LILA" ],
				5 => [ 17 => "ROJA", 18 => "CAFE", 19 => "NEGRA", 20 => "VERDE" ],
				6 => [ 21 => "AZUL", 22 => "BLANCA", 23 => "AMARILLA", 24 => "LILA" ],
				7 => [ 25 => "ROJA", 26 => "CAFE", 27 => "NEGRA", 28 => "VERDE" ],
				8 => [ 29 => "AZUL", 30 => "BLANCA", 31 => "AMARILLA", 32 => "LILA" ],
				9 => [ 33 => "ROJA", 34 => "CAFE", 35 => "NEGRA", 36 => "VERDE" ],
				10 => [ 37 => "AZUL", 38 => "BLANCA", 39 => "AMARILLA", 40 => "LILA" ],
				11 => [ 41 => "ROJA", 42 => "CAFE", 43 => "NEGRA", 44 => "VERDE" ],
				12 => [ 45 => "AZUL", 46 => "BLANCA", 47 => "AMARILLA", 48 => "LILA" ],
				13 => [ 49 => "ROJA", 50 => "CAFE", 51 => "NEGRA", 52 => "VERDE" ]
			]
		];
		$edad = 0;
		$encontro = false;
		$semana_edad = 0;
		$pr = 0;

		#$this->DD($semana);
		
		if($anio == 2017 && $semana < 18){
			if(in_array($semana, [14,15,16,17]))
				$pr = 13;
			if(in_array($semana, [10,11,12,13]))
				$pr = 12;
			if(in_array($semana, [6,8,9,10,3]))
				$pr = 11;
			if(in_array($semana, [5,7,4,2]))
				$pr = 10;

			foreach($secuencia["2016"][$pr] as $sec_periodo){
				if(!$encontro)
				foreach($sec_periodo as $sem => $col){					
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
						break;
					}
				}
			}
			$edad = $semana_edad;
		}else if($semana >= 18){
			foreach($secuencia["2017"][$periodo-4] as $sec_periodo){
				foreach($sec_periodo as $sem => $col){
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
					}
				}
			}
			$edad = $semana - $semana_edad;
		}else{
			foreach($secuencia["2017"][$periodo+13-5] as $sec_periodo){
				foreach($sec_periodo as $sem => $col){
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
					}
				}
			}
			$edad = $semana + (13 - $semana_edad);
		}

		return $edad;
	}*/

	private function DD($string){
		echo "<pre>";
		D($string);
		echo "</pre>";
	}

	/*private function getLotes($semana = 0){
		$response = new stdClass;
		$response->data = [];
		if($semana > 0){
			$sYear = " AND year = YEAR(CURRENT_DATE)";
			$sSemana = "p.semana";
			
			$sql = "SELECT $sSemana AS semana , lote,
					total_cosechados , 
					total_recusados,
					total_procesada,
					'false' AS expanded,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote) / cosechados) AS ratio_cortado,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote) / procesado) AS ratio_procesado,
					(SELECT COUNT(1) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS muestrados,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS largo_dedos,
					(SELECT AVG(peso) FROM produccion_peso WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear} AND lote = p.lote) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_calibracion WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS calibracion,
					(SELECT AVG(manos) FROM produccion_manos WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS manos
				FROM(
					SELECT $sSemana, lote, 
						SUM(total_cosechados) AS total_cosechados,
						SUM(total_recusados) AS total_recusados,
						SUM(total_cosechados) - SUM(total_recusados) AS total_procesada,
						SUM(total_cosechados) as cosechados,
						SUM(total_cosechados-total_recusados) AS procesado
					FROM produccion AS p
					WHERE 1 = 1 AND $sSemana = {$semana} {$sYear}
					GROUP BY lote
				) AS p";
			
			$data = $this->db->queryAll($sql);
			foreach ($data as $key => $value) {
				$response->data[$value->lote]["campo"] = $value->lote;
				$response->data[$value->lote]["muestrados"] = (double)$value->muestrados;
				$response->data[$value->lote]["peso"] = (double)$value->peso;
				$response->data[$value->lote]["manos"] = (double)$value->manos;
				$response->data[$value->lote]["largo_dedos"] = (double)$value->largo_dedos;
				$response->data[$value->lote]["calibracion"] = (double)$value->calibracion;
				$response->data[$value->lote]["total_cosechados"] = (double)$value->total_cosechados;
				$response->data[$value->lote]["total_recusados"] = (double)$value->total_recusados;
				$response->data[$value->lote]["total_procesada"] = (double)$value->total_procesada;
				$response->data[$value->lote]["total_procesada"] = (double)$value->total_procesada;
				$response->data[$value->lote]["ratio_procesado"] = (double)$value->ratio_procesado;
				$response->data[$value->lote]["ratio_cortado"] = (double)$value->ratio_cortado;
			}
		}

		return $response->data;
	}

	private function getPromedioEdadWeek(){
		$sql = "SELECT WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY WEEK(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->semana]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->semana]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$response->total[$value->semana]["sum"] += $value->cosechados_blanco * $response->data[$value->semana]["blanco"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_blanco;
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->semana]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->semana]["negra"]["cosechadas"] = $value->cosechados_negro;
				$response->total[$value->semana]["sum"] += $value->cosechados_negro * $response->data[$value->semana]["negra"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_negro;
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->semana]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->semana]["lila"]["cosechadas"] = $value->cosechados_lila;
				$response->total[$value->semana]["sum"] += $value->cosechados_lila * $response->data[$value->semana]["lila"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_lila;
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->semana]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->semana]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$response->total[$value->semana]["sum"] += $value->cosechados_rojo * $response->data[$value->semana]["roja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_rojo;
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->semana]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->semana]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$response->total[$value->semana]["sum"] += $value->cosechados_cafe * $response->data[$value->semana]["cafe"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_cafe;
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->semana]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->semana]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$response->total[$value->semana]["sum"] += $value->cosechados_amarillo * $response->data[$value->semana]["amarilla"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_amarillo;
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->semana]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->semana]["verde"]["cosechadas"] = $value->cosechados_verde;
				$response->total[$value->semana]["sum"] += $value->cosechados_verde * $response->data[$value->semana]["verde"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_verde;
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->semana]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->semana]["azul"]["cosechadas"] = $value->cosechados_azul;
				$response->total[$value->semana]["sum"] += $value->cosechados_azul * $response->data[$value->semana]["azul"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_azul;
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->semana]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->semana]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$response->total[$value->semana]["sum"] += $value->cosechados_naranja * $response->data[$value->semana]["naranja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_naranja;
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->semana]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->semana]["gris"]["cosechadas"] = $value->cosechados_gris;
				$response->total[$value->semana]["sum"] += $value->cosechados_gris * $response->data[$value->semana]["gris"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_gris;
			}
			$response->edad_promedio[$value->semana] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Edad Promedio",
				"value" => (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]),
			];

			$response->edad_prom[$value->semana] = (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]);
		}

		$response->edad = array_sum($response->edad_prom) / count($response->edad_prom);
		$minMax = [
			"min" => 9,
			"max" => 15,
		];
		$response->edad_promedio = $this->chartInit($response->edad_promedio , "vertical" , "Edad Promedio" , "line" , $minMax);
		return $response;
	}

	private function getPromedioEdad(){
		$sql = "SELECT DATE(fecha) AS fecha,WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY DATE(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		$edad_promedio = [];
		$sum_edadxcosecha = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->fecha]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->fecha]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["blanco"]["edad"] * $response->data[$value->fecha]["blanco"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["blanco"]["cosechadas"];
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->fecha]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->fecha]["negra"]["cosechadas"] = $value->cosechados_negro;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["negra"]["edad"] * $response->data[$value->fecha]["negra"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["negra"]["cosechadas"];
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->fecha]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->fecha]["lila"]["cosechadas"] = $value->cosechados_lila;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["lila"]["edad"] * $response->data[$value->fecha]["lila"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["lila"]["cosechadas"];
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->fecha]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->fecha]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["roja"]["edad"] * $response->data[$value->fecha]["roja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["roja"]["cosechadas"];
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->fecha]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->fecha]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["cafe"]["edad"] * $response->data[$value->fecha]["cafe"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["cafe"]["cosechadas"];
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->fecha]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->fecha]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["amarilla"]["edad"] * $response->data[$value->fecha]["amarilla"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["amarilla"]["cosechadas"];
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->fecha]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->fecha]["verde"]["cosechadas"] = $value->cosechados_verde;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["verde"]["edad"] * $response->data[$value->fecha]["verde"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["verde"]["cosechadas"];
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->fecha]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->fecha]["azul"]["cosechadas"] = $value->cosechados_azul;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["azul"]["edad"] * $response->data[$value->fecha]["azul"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["azul"]["cosechadas"];
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->fecha]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->fecha]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["naranja"]["edad"] * $response->data[$value->fecha]["naranja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["naranja"]["cosechadas"];
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->fecha]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->fecha]["gris"]["cosechadas"] = $value->cosechados_gris;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["gris"]["edad"] * $response->data[$value->fecha]["gris"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["gris"]["cosechadas"];
			}

			$response->edad_promedio[$value->semana][$value->fecha] = $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
			$response->data[$value->semana] += $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
		}

		return $response;
	}

	private function getEdad($color = "" , $semana = 1){
		$edad = 0;
		$sql = "SELECT
				({$semana} - (
				SELECT semana FROM semanas_colores
				WHERE YEAR(CURRENT_DATE) = year AND color = UPPER('{$color}') AND semana < ({$semana} - 8) ORDER BY semana DESC LIMIT 1
				)) + 1 AS edad";
		$edad = (int)$this->db->queryOne($sql);
		return $edad;
	}

	private function generateDataCosecha(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;
		$filters = (object)[
			"type" => getValueFrom($postdata , "type" , "cosechados" , FILTER_SANITIZE_STRING),
		];
		$sYearCampo = "YEAR(fecha)";
		$sSemana = "WEEK(p.fecha)";
		if($this->session->id_company == 2 || $this->session->id_company == 3){
			$sSemana = "semana";
			$sYearCampo = "year";
		}

		$type = [
			"cosechados" => "SUM(total_cosechados)",
			"recusados" => "SUM(total_recusados)",
			"procesada" => "(SUM(total_cosechados) - SUM(total_recusados))",
		];
		$title = "Cosechas";
		if($filters->type == "cajas"){
			$title = "Cajas";
			$sql_historico = "SELECT year AS legend,semana AS label,SUM(caja) AS value,IF(year > 2013 ,true,false) AS selected
								FROM produccion_cajas AS p
								GROUP BY year,semana";
		}else{
			$title = "Cosechas";
			$sql_historico = "SELECT {$sSemana} AS label,{$sYearCampo} AS legend,IF(year > 2013 ,true,false) AS selected,
				{$type[$filters->type]} AS value 
				FROM produccion AS p
				GROUP BY {$sYearCampo} , {$sSemana}";
		}
		
		$data = $this->db->queryAll($sql_historico);

		$minMax = [
			// "min" => 50,
			// "max" => 100,
		];
		$response->historico = $this->chartInit($data,"vertical",$title,"line",$minMax);
		return $response;
	}

	private function generateDataPeso(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(peso) AS peso 
						FROM (
							SELECT semana ,AVG(peso) as peso 
							FROM (
								(SELECT lote,semana , AVG(peso) AS peso 
									FROM produccion_peso
									WHERE year = YEAR(CURRENT_DATE)
									GROUP BY year,lote,semana)
								) AS p
							GROUP BY semana
						) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(peso),2) AS value,IF(year > 2013 ,true,false) AS selected 
							FROM (
								SELECT year , semana ,lote, AVG(peso) AS peso 
								FROM produccion_peso
								GROUP BY year,lote,semana
							) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 50,
			"max" => 100,
		];
		$response->historico = $this->chartInit($data,"vertical","Peso","line",$minMax);

		return $response;
	}

	private function generateDataManos(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(manos) AS manos 
						FROM (
							SELECT semana ,AVG(manos) as manos 
							FROM (
								(SELECT lote,semana , AVG(manos) AS manos 
								FROM produccion_manos
								WHERE year = YEAR(CURRENT_DATE)
								GROUP BY year,lote,semana)
							) AS p
							GROUP BY semana
						) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(manos),2) AS value,IF(year > 2013 ,true,false) AS selected FROM (
							SELECT year , semana ,lote,AVG(manos) AS manos FROM produccion_manos
							GROUP BY year,lote,semana) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 5,
			"max" => 10,
		];
		$response->historico = $this->chartInit($data,"vertical","Manos","line",$minMax);

		return $response;
	}

	private function generateDataCalibracion(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(calibracion) AS calibracion FROM (
						SELECT semana ,AVG(calibracion) as calibracion FROM (
						(SELECT lote,semana , AVG(calibracion) AS calibracion FROM produccion_calibracion
						WHERE year = YEAR(CURRENT_DATE)
						GROUP BY year,lote,semana)) AS p
						GROUP BY semana) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(calibracion),2) AS value,IF(year > 2013 ,true,false) AS selected FROM (
							SELECT year , semana ,lote,AVG(calibracion) AS calibracion FROM produccion_calibracion
							GROUP BY year,lote,semana) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 38,
			"max" => 46,
		];
		$response->historico = $this->chartInit($data,"vertical","Calibración","line",$minMax);

		return $response;
	}*/

	private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
			}
		}

		return $response->chart;
	}
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
	private function clear($String){
		$String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	    $String = str_replace(array('í','ì','î','ï'),"i",$String);
	    $String = str_replace(array('é','è','ê','ë'),"e",$String);
	    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
	    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	    $String = str_replace("ç","c",$String);
	    $String = str_replace("Ç","C",$String);
	    $String = str_replace("ñ","n",$String);
	    $String = str_replace("Ñ","N",$String);
	    $String = str_replace("Ý","Y",$String);
	    $String = str_replace("ý","y",$String);
	    $String = str_replace(".","",$String);
	    $String = str_replace("&aacute;","a",$String);
	    $String = str_replace("&Aacute;","A",$String);
	    $String = str_replace("&eacute;","e",$String);
	    $String = str_replace("&Eacute;","E",$String);
	    $String = str_replace("&iacute;","i",$String);
	    $String = str_replace("&Iacute;","I",$String);
	    $String = str_replace("&oacute;","o",$String);
	    $String = str_replace("&Oacute;","O",$String);
	    $String = str_replace("&uacute;","u",$String);
	    $String = str_replace("&Uacute;","U",$String);
	    return $String;
    }
    
    private function grafica_z($data = [], $group_y = [], $selected = null, $types = []){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
        ];
        $options["dataZoom"] = [
            [
                "show" => true,
                "realtime" => true
            ],
            [
                "type" => 'inside',
                "realtime" => true
            ]
        ];
		$options["legend"]["data"] = [];
		$options["legend"]["top"] = "0%";
        $options["legend"]["left"] = "center";
        $options["legend"]["selected"] = $selected;
		$options["xAxis"] = [
			[
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
        }

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				//'max' => ($key == 1) ? $maxs[$key] + ($maxs[$key] - $mins[$key]) * .05 : null,
                //'min' => ($key == 1) ? $mins[$key] - ($maxs[$key] - $mins[$key]) * .05 : null,
                'max' => null,
                'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
        }

		foreach($_names as $i => $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => isset($types[$i]) ? $types[$i] : 'line',
				"connectNulls" => true,
				"data" => []
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
	}
	
}
