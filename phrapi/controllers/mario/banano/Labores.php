<?php defined('PHRAPI') or die("Direct access not allowed!");

class Labores {
	public $name;
	private $db;
	private $agent_user;

	public function __construct(){
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}
    private function ProccessParams(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$response = (object)[
			"tarea" => getValueFrom($postdata , "tarea" , "" , FILTER_SANITIZE_STRING),
		];
		return $response;
    }

    public function data(){
        $response = new stdClass;
        $sql = "SELECT id,nombre, '0.00' AS porcentaje FROM tipo_labor WHERE status = 1";
        $response->tipo_labores = $this->db->queryAll($sql);

        foreach($response->tipo_labores as $tipo){
            $sql = "SELECT nombre, '0.00' AS porcentaje FROM labores WHERE status = 1 AND id_tipo_labor = '{$tipo->id}'";
            $tipo->labor = $this->db->queryAll($sql);
        }
        return $response;
    }


}