<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionRacimos {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function racimosEdad(){
        $response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
		];

        $sWhere = "";
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
		}

		$sql = "SELECT cinta AS cable, edad AS edad, class,
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND cinta = produccion.cinta  {$sWhere}) AS recusados, 
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta AND calibre > 0 {$sWhere}) AS muestreados, 
 					(SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM (
                        SELECT cinta , produccion.edad, class
                        FROM produccion_racimos produccion
                        LEFT JOIN produccion_colores colores ON cinta = color
                        WHERE 1 = 1  {$sWhere}
                        GROUP BY cinta
                    ) AS tbl
                    GROUP BY cinta
                ) AS produccion";
		$response->data = $this->db->queryAll($sql);
		foreach($response->data as $i => $val){
			$val->expanded = false;
            $val->cosechados = $val->procesados + $val->recusados;
			$val->lotes = $this->db->queryAll("SELECT lote, 
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere}) AS recusados,
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados, 
                    (SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM(
                        SELECT lote
                        FROM produccion_racimos
                        WHERE 1=1 AND cinta = '{$val->cable}' {$sWhere}
                        GROUP BY lote
                    ) AS tbl
                    GROUP BY lote
                ) AS produccion");
			foreach($val->lotes as $key => $value){
				$value->expanded = false;
				$value->cosechados = $value->procesados + $value->recusados;

				$value->cuadrillas = $this->db->queryAll("SELECT cuadrilla, 
						(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS recusados,
						(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre > 0 {$sWhere}) AS muestreados, 
						(SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS peso,
						(SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
						(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
						(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
					FROM(
					SELECT cuadrilla
					FROM produccion_racimos
					WHERE 1=1 AND cinta = '{$val->cable}' AND lote = '{$value->lote}' {$sWhere}
					GROUP BY cuadrilla) AS produccion");
				foreach($value->cuadrillas as $row){
					$row->cosechados = $row->procesados + $row->recusados;
				}
			}
		}
		return $response;
	}

	public function lastDay(){
		$response = new stdClass;
		$response->last = $this->db->queryRow("SELECT MAX(fecha) AS fecha FROM produccion_racimos");
		$response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha = '{$response->last->fecha}' GROUP BY id_finca");
		return $response;
    }

    public function tags(){
        $response = new stdClass;
        $response->tags = [];
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

        $sWhere = " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        $response->tags["procesados"] = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' $sWhere");
        $response->tags["recusados"] = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' $sWhere");
        $response->tags["cosechados"] = $response->tags["procesados"] + $response->tags["recusados"];
        $response->tags["kg_proc"] = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos WHERE tipo = 'PROC' $sWhere");
        $response->tags["kg_recu"] = $this->db->queryOne("SELECT SUM(peso) FROM produccion_racimos WHERE tipo = 'RECU' $sWhere");
        $response->tags["kg_prom"] = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_racimos WHERE tipo = 'PROC' $sWhere");
        //$response->tags["calibre"] = $this->db->queryOne("SELECT ROUND(AVG(calibre)) FROM produccion_racimos WHERE 1=1 $sWhere");
        $response->tags["calibre_segunda"] = $this->db->queryOne("SELECT ROUND(AVG(calibre)) FROM produccion_racimos WHERE 1=1 $sWhere");
        $response->tags["calibre_ultima"] = $this->db->queryOne("SELECT ROUND(AVG(calibre_ultima)) FROM produccion_racimos WHERE 1=1 $sWhere");
        $response->tags["manos"] = $this->db->queryOne("SELECT ROUND(AVG(manos), 2) FROM produccion_racimos WHERE 1=1 $sWhere");
        $response->tags["edad"] = $this->db->queryOne("SELECT ROUND(AVG(edad), 2) FROM produccion_racimos WHERE edad > 0 $sWhere");
        $response->tags["muestreo"] = $this->db->queryOne("SELECT ROUND(COUNT(1)/{$response->tags["procesados"]}*100, 2) FROM produccion_racimos WHERE manos IS NOT NULL AND manos > 0 AND tipo = 'PROC' $sWhere");
        $response->tags["muestreo_calibre"] = $this->db->queryOne("SELECT ROUND(COUNT(1)/{$response->tags["procesados"]}*100, 2) FROM produccion_racimos WHERE calibre_segunda IS NOT NULL AND calibre_segunda > 0 AND tipo = 'PROC' $sWhere");

        $times = $this->db->queryAll("SELECT fecha, hora FROM produccion_racimos WHERE 1=1 AND hora != '' $sWhere ORDER BY hora");
        $p = $times[0];
        $u = $times[count($times)-1];
        $response->tags["ultima_fecha"] = $u->fecha;
        $response->tags["ultima_hora"] = $u->hora;
        $response->tags["primera_fecha"] = $p->fecha;
        $response->tags["primera_hora"] = $p->hora;
        $response->tags["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}', '{$p->hora}') AS dif")->dif;

        return $response;
    }
    
    public function resumen(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

        $sWhere = " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }


        $sql = "SELECT racimos.edad, cinta, class
                FROM produccion_racimos racimos
                LEFT JOIN produccion_colores colores ON color = cinta
                WHERE 1=1 {$sWhere}
                GROUP BY edad
                ORDER BY edad";
        $edades = $this->db->queryAll($sql);

        $sql = "SELECT lote, 
                        SUM(IF(tipo = 'PROC', 1, 0)) AS procesados, 
                        SUM(IF(tipo = 'RECU', 1, 0)) AS recusados, 
                        ROUND(AVG(calibre), 2) AS calibre,
                        ROUND(AVG(IF(tipo = 'PROC', calibre, NULL)), 2) AS calibre_segunda,
                        ROUND(AVG(IF(tipo = 'PROC', calibre_ultima, NULL)), 2) AS calibre_ultima,
                        ROUND(AVG(IF(tipo = 'PROC', manos , NULL)), 2) AS manos,
                        ROUND(AVG(IF(tipo = 'PROC', dedos, NULL)), 2) AS dedos,
                        ROUND(AVG(peso), 2) AS peso
                FROM produccion_racimos
                WHERE 1=1 $sWhere
                GROUP BY lote
                ORDER BY lote+0";
        $data = $this->db->queryAll($sql);
        foreach($data as $row){
            foreach($edades as $edad){
                if($edad->edad > 0)
                    $row->{"edad_{$edad->edad}"} = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos WHERE lote = '{$row->lote}' AND edad = {$edad->edad} $sWhere");
                else
                    $row->{"edad_S/C"} = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos WHERE lote = '{$row->lote}' AND (edad = 0 OR edad IS NULL) $sWhere");
            }
        }

        $response->edades = $edades;
        $response->data = $data;
        return $response;
    }

	public function historico(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }
        
        $sWhere = "";
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }
        
		$response->data = $this->db->queryAll("SELECT historica.id, fecha, lote, causa, peso, manos, cinta, dedos, colores.class, historica.edad, cuadrilla, hora, tipo, calibre AS calibre_segunda, NULL AS calibre_ultima
			FROM produccion_racimos historica
			LEFT JOIN produccion_colores colores ON colores.color = cinta
			WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' $sWhere");
		return $response;
    }

    public function pesoRacimo(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "grupo" => getValueFrom($postdata , "grupo" , "" , FILTER_SANITIZE_STRING),
            "lote" => getValueFrom($postdata , "lote_adicional" , 0 , FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->lote > 0){
            $sWhere .= " AND lote = {$filters->lote}";
        }

        // GRAFICA DIA

        $sql = "SELECT
                    CONCAT('[', (grupo*{$filters->grupo})-{$filters->grupo}+0.1, ', ', grupo*{$filters->grupo}, ']') label_x,
                    value,
                    0 AS index_y,
                    'PESO PROM POR RAC' AS name
                FROM (
                    SELECT 
                        COUNT(1) value,
                        FLOOR(value/{$filters->grupo}) grupo
                    FROM (
                        SELECT 
                            peso AS 'value'
                        FROM produccion_racimos
                        WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND peso > 0 {$sWhere}
                        ORDER BY peso
                    ) tbl
                    GROUP BY grupo
                    ORDER BY grupo
                ) tbl";
        $data_chart = $this->db->queryAll($sql);
        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->dia = $this->grafica_z($data_chart, $groups, null, ['bar'], "PESO");

        // PESO RACIMOS VS MANOS POR RACIMO

        $sql = "SELECT 
                    manos AS index_1,
                    peso AS index_0
                FROM produccion_racimos
                WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND peso > 0 AND manos > 0 {$sWhere}
                ORDER BY manos";
        $data_chart = $this->db->queryAll($sql);
        $response->vs_manos = $this->grafica_scatter($data_chart, "MANOS", "PESO");

        return $response;
    }

    public function pesoCalibre(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "grupo" => getValueFrom($postdata , "grupo" , "" , FILTER_SANITIZE_STRING),
            "lote" => getValueFrom($postdata , "lote_adicional" , 0 , FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->lote > 0){
            $sWhere .= " AND lote = {$filters->lote}";
        }

        $year = $this->db->queryOne("SELECT getYear('{$filters->fecha}')");

        $sql = "SELECT 
                    label_x,
                    value,
                    0 AS index_y, 
                    name
                FROM (
                    SELECT 
                        semana AS label_x,
                        ROUND(AVG(peso/calibre_segunda), 2) AS 'value',
                        'PESO PROM CALIB. 2DA' AS 'name'
                    FROM produccion_racimos
                    WHERE year = {$year} AND id_finca = {$filters->finca} AND calibre_segunda > 0 AND peso > 0 {$sWhere}
                    GROUP BY semana
                    UNION ALL
                    SELECT 
                        semana AS label_x,
                        ROUND(AVG(peso/calibre_ultima), 2) AS 'value',
                        'PESO PROM CALIB. ULT' AS 'name'
                    FROM produccion_racimos
                    WHERE year = {$year} AND id_finca = {$filters->finca} AND calibre_ultima > 0 AND peso > 0 {$sWhere}
                    GROUP BY semana
                ) tbl
                ORDER BY label_x";
        $data_chart = $this->db->queryAll($sql);
        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => 'PESO PROM',
				"type" => 'line',
				'format' => ''
			]
        ];
        $response->data = $this->grafica_z($data_chart, $groups, ((object) []), ['line'], "SEM");

        // GRAFICA DIA

        $sql = "SELECT *
                FROM (
                    SELECT
                        CONCAT('[', (grupo*{$filters->grupo})-{$filters->grupo}, ', ', grupo*{$filters->grupo}, ']') label_x,
                        COUNT(1) AS value,
                        0 AS index_y,
                        'PESO PROM POR CALIB. 2DA' AS name
                    FROM (
                        SELECT 
                            value,
                            CEIL(value/{$filters->grupo}) grupo
                        FROM (
                            SELECT 
                                ROUND(peso/calibre_segunda, 2) AS 'value'
                            FROM produccion_racimos
                            WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND calibre_segunda > 0 AND peso > 0 {$sWhere}
                        ) tbl
                        ORDER BY grupo
                    ) tbl
                    GROUP BY grupo
                    UNION ALL
                    SELECT
                        CONCAT('[', (grupo*{$filters->grupo})-{$filters->grupo}, ', ', grupo*{$filters->grupo}, ']') label_x,
                        COUNT(1) AS value,
                        0 AS index_y,
                        'PESO PROM POR CALIB. ULT' AS name
                    FROM (
                        SELECT 
                            value,
                            CEIL(value/{$filters->grupo}) grupo
                        FROM (
                            SELECT 
                                ROUND(peso/calibre_ultima, 2) AS 'value'
                            FROM produccion_racimos
                            WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND calibre_ultima > 0 AND peso > 0 {$sWhere}
                        ) tbl
                        ORDER BY grupo
                    ) tbl
                    GROUP BY grupo
                ) tbl";
        $data_chart = $this->db->queryAll($sql);
        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->dia = $this->grafica_z($data_chart, $groups, ((object) []), ['bar', 'bar'], "° CALIB");

        return $response;
    }

    public function pesoMano(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "grupo" => getValueFrom($postdata , "grupo" , 1 , FILTER_SANITIZE_STRING),
            "lote" => getValueFrom($postdata , "lote_adicional" , 0 , FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->lote > 0){
            $sWhere .= " AND lote = {$filters->lote}";
        }

        $year = $this->db->queryOne("SELECT getYear('{$filters->fecha}')");

        // GRAFICA DE TENDENCIA

        $sql = "SELECT 
                    label_x,
                    value,
                    0 AS index_y, 
                    'PESO PROM MANOS.' AS 'name'
                FROM (
                    SELECT 
                        semana AS label_x,
                        ROUND(AVG(peso/manos), 2) AS 'value'
                    FROM produccion_racimos
                    WHERE year = {$year} AND id_finca = {$filters->finca} AND manos > 0 AND peso > 0 {$sWhere}
                    GROUP BY semana
                    ORDER BY semana
                ) tbl
                ORDER BY label_x";
        $data_chart = $this->db->queryAll($sql);
        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => 'PESO PROM',
				"type" => 'line',
				'format' => ''
			]
        ];
        $response->data = $this->grafica_z($data_chart, $groups, ((object)[]), ['line'], "SEM");

        // GRAFICA DEL DIA

        $sql = "SELECT
                    CONCAT('[', (grupo*{$filters->grupo})-{$filters->grupo}+0.1, ', ', grupo*{$filters->grupo}, ']') label_x,
                    COUNT(1) AS value,
                    0 AS index_y,
                    'PESO PROM POR MANO' AS name
                FROM (
                    SELECT 
                        value,
                        FLOOR(value/{$filters->grupo}) grupo
                    FROM (
                        SELECT 
                            ROUND(peso/manos, 2) AS 'value'
                        FROM produccion_racimos
                        WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND manos > 0 AND peso > 0 {$sWhere}
                    ) tbl
                    ORDER BY grupo
                ) tbl
                GROUP BY grupo";
        $data_chart = $this->db->queryAll($sql);

        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->dia = $this->grafica_z($data_chart, $groups, null, ['bar'], "MANOS");

        return $response;
    }

    public function cantidadRacimoCalibreManos(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "grupo" => getValueFrom($postdata , "grupo" , 1 , FILTER_SANITIZE_STRING),
            "lote" => getValueFrom($postdata , "lote_adicional" , 0 , FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->lote > 0){
            $sWhere .= " AND lote = {$filters->lote}";
        }

        // GRAFICA DEL DIA
        $sql = "SELECT
                    label_x,
                    value,
                    0 AS index_y,
                    'MANOS' AS name
                FROM (
                    SELECT manos AS label_x, COUNT(1) AS value
                    FROM produccion_racimos
                    WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND manos > 0 $sWhere
                    GROUP BY manos
                    ORDER BY manos
                ) tbl";
        $data_chart = $this->db->queryAll($sql);
        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->manos = $this->grafica_z($data_chart, $groups, null, ['bar'], "PESO");

        // GRAFICA DEL DIA
        $sql = "SELECT
                    label_x,
                    value,
                    0 AS index_y,
                    'CALIB. 2DA' AS name
                FROM (
                    SELECT ROUND(calibre_segunda, 0) AS label_x, COUNT(1) AS value
                    FROM produccion_racimos
                    WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND calibre_segunda > 0 $sWhere
                    GROUP BY ROUND(calibre_segunda, 0)
                    ORDER BY ROUND(calibre_segunda, 0)
                ) tbl";
        $data_chart = $this->db->queryAll($sql);
        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->calibre_segunda = $this->grafica_z($data_chart, $groups, null, ['bar'], "PESO");

        // GRAFICA DEL DIA
        $sql = "SELECT
                    label_x,
                    value,
                    0 AS index_y,
                    'CALIB. ULT' AS name
                FROM (
                    SELECT ROUND(calibre_ultima, 0) AS label_x, COUNT(1) AS value
                    FROM produccion_racimos
                    WHERE fecha = '{$filters->fecha}' AND id_finca = {$filters->finca} AND calibre_ultima > 0 $sWhere
                    GROUP BY ROUND(calibre_ultima, 0)
                    ORDER BY ROUND(calibre_ultima, 0)
                ) tbl";
        $data_chart = $this->db->queryAll($sql);
        $groups = [
			[
				"name" => 'RAC',
				"type" => 'bar',
				'format' => ''
			]
        ];
        $response->calibre_ultima = $this->grafica_z($data_chart, $groups, null, ['bar'], "PESO");

        return $response;
    }
    
    public function defectos(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

        $sWhere = "";
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        $sql = "SELECT causa, COUNT(1) AS cantidad
                FROM produccion_racimos
                WHERE tipo = 'RECU' AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' {$sWhere}
                GROUP BY causa";
        $data = $this->db->queryAll($sql);

        $total_recusados = 0;
        foreach($data as $row){
            $total_recusados += $row->cantidad;
        }
        foreach($data as $row){
            $row->porcentaje = round($row->cantidad / $total_recusados * 100, 2);
        }
        
        $response->data = $data;
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $reg){
                $this->db->query("DELETE FROM produccion_racimos WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }
    
    public function editar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id)){
            if($postdata->lote != "" && $postdata->cuadrilla != "" && $postdata->tipo != ""){
                $this->db->query("UPDATE produccion_racimos SET
                                        peso = $postdata->peso,
                                        edad = $postdata->edad,
                                        cinta = getCintaFromEdad($postdata->edad, getWeek(fecha), YEAR(fecha)),
                                        cuadrilla = '{$postdata->cuadrilla}',
                                        lote = '{$postdata->lote}',
                                        tipo = '{$postdata->tipo}',
                                        causa = '{$postdata->causa}',
                                        manos = '{$postdata->manos}',
                                        calibre = '{$postdata->calibre}',
                                        dedos = '{$postdata->dedos}'
                                    WHERE id = {$postdata->id}");
            }
            return $this->db->queryRow("SELECT h.id, fecha, lote, causa, peso, manos, calibre, cinta, dedos, h.edad, cuadrilla, hora, tipo, class
                                        FROM produccion_racimos h
                                        INNER JOIN produccion_colores c ON h.cinta = c.color
                                        WHERE h.id = {$postdata->id}");
        }
        return false;
    }

    public function analizisRecusados(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $sWhere = "";
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        if($postdata->var_recusado == 'cant'){
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM produccion_racimos
                    WHERE tipo = 'RECU' AND year = YEAR('{$postdata->fecha_inicial}') $sWhere
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa
                UNION ALL
                SELECT
                    'TOTAL' AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM produccion_racimos
                    WHERE tipo = 'RECU' AND year = YEAR('{$postdata->fecha_inicial}') $sWhere
                    GROUP BY semana
                ) AS tbl";
            $response->data = $this->db->queryAll($sql);
        }else{
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, ROUND(COUNT(1)/(SELECT COUNT(1) FROM produccion_racimos WHERE semana = main.semana AND YEAR = YEAR('{$postdata->fecha_inicial}'))*100, 2) AS cantidad
                    FROM produccion_racimos main
                    WHERE tipo = 'RECU' AND YEAR = YEAR('{$postdata->fecha_inicial}') $sWhere
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa";
            $totales = $this->db->queryRow("SELECT tbl.*, 'TOTAL' AS dano, SUM(SUM) AS 'sum', SUM(MIN) AS 'min', SUM(MAX) AS 'max', SUM(prom) AS 'prom'
                FROM (
                    SELECT
                        'TOTAL' AS dano,
                        '' AS 'sum',
                        '' AS 'min',
                        '' AS 'max',
                        '' AS 'prom',
                        SUM(IF(semana = 0, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 1)*100 AS 'sem_0',
                        SUM(IF(semana = 1, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 1)*100 AS 'sem_1',
                        SUM(IF(semana = 2, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 2)*100 AS 'sem_2',
                        SUM(IF(semana = 3, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 3)*100 AS 'sem_3',
                        SUM(IF(semana = 4, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 4)*100 AS 'sem_4',
                        SUM(IF(semana = 5, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 5)*100 AS 'sem_5',
                        SUM(IF(semana = 6, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 6)*100 AS 'sem_6',
                        SUM(IF(semana = 7, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 7)*100 AS 'sem_7',
                        SUM(IF(semana = 8, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 8)*100 AS 'sem_8',
                        SUM(IF(semana = 9, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 9)*100 AS 'sem_9',
                        SUM(IF(semana = 10, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 10)*100 AS 'sem_10',
                        SUM(IF(semana = 11, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 11)*100 AS 'sem_11',
                        SUM(IF(semana = 12, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 12)*100 AS 'sem_12',
                        SUM(IF(semana = 13, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 13)*100 AS 'sem_13',
                        SUM(IF(semana = 14, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 14)*100 AS 'sem_14',
                        SUM(IF(semana = 15, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 15)*100 AS 'sem_15',
                        SUM(IF(semana = 16, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 16)*100 AS 'sem_16',
                        SUM(IF(semana = 17, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 17)*100 AS 'sem_17',
                        SUM(IF(semana = 18, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 18)*100 AS 'sem_18',
                        SUM(IF(semana = 19, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 19)*100 AS 'sem_19',
                        SUM(IF(semana = 20, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 20)*100 AS 'sem_20',
                        SUM(IF(semana = 21, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 21)*100 AS 'sem_21',
                        SUM(IF(semana = 22, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 22)*100 AS 'sem_22',
                        SUM(IF(semana = 23, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 23)*100 AS 'sem_23',
                        SUM(IF(semana = 24, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 24)*100 AS 'sem_24',
                        SUM(IF(semana = 25, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 25)*100 AS 'sem_25',
                        SUM(IF(semana = 26, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 26)*100 AS 'sem_26',
                        SUM(IF(semana = 27, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 27)*100 AS 'sem_27',
                        SUM(IF(semana = 28, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 28)*100 AS 'sem_28',
                        SUM(IF(semana = 29, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 29)*100 AS 'sem_29',
                        SUM(IF(semana = 30, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 30)*100 AS 'sem_30',
                        SUM(IF(semana = 31, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 31)*100 AS 'sem_31',
                        SUM(IF(semana = 32, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 32)*100 AS 'sem_32',
                        SUM(IF(semana = 33, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 33)*100 AS 'sem_33',
                        SUM(IF(semana = 34, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 34)*100 AS 'sem_34',
                        SUM(IF(semana = 35, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 35)*100 AS 'sem_35',
                        SUM(IF(semana = 36, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 36)*100 AS 'sem_36',
                        SUM(IF(semana = 37, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 37)*100 AS 'sem_37',
                        SUM(IF(semana = 38, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 38)*100 AS 'sem_38',
                        SUM(IF(semana = 39, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 39)*100 AS 'sem_39',
                        SUM(IF(semana = 40, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 40)*100 AS 'sem_40',
                        SUM(IF(semana = 41, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 41)*100 AS 'sem_41',
                        SUM(IF(semana = 42, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 42)*100 AS 'sem_42',
                        SUM(IF(semana = 43, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 43)*100 AS 'sem_43',
                        SUM(IF(semana = 44, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 44)*100 AS 'sem_44',
                        SUM(IF(semana = 45, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 45)*100 AS 'sem_45',
                        SUM(IF(semana = 46, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 46)*100 AS 'sem_46',
                        SUM(IF(semana = 47, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 47)*100 AS 'sem_47',
                        SUM(IF(semana = 48, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 48)*100 AS 'sem_48',
                        SUM(IF(semana = 49, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 49)*100 AS 'sem_49',
                        SUM(IF(semana = 50, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 50)*100 AS 'sem_50',
                        SUM(IF(semana = 51, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 51)*100 AS 'sem_51',
                        SUM(IF(semana = 52, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 52)*100 AS 'sem_52',
                        SUM(IF(semana = 53, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 53)*100 AS 'sem_53'
                    FROM (
                        SELECT semana, causa, COUNT(1) AS cantidad
                        FROM produccion_racimos
                        WHERE tipo = 'RECU' AND YEAR = YEAR('{$postdata->fecha_inicial}') $sWhere
                        GROUP BY causa, semana
                    ) AS tbl
                ) AS tbl
                GROUP BY dano");
            $row_totales_gropales = $this->db->queryRow("SELECT SUM(recusados)/SUM(cortados)*100 AS 'sum', MIN(cantidad) AS 'min', MAX(cantidad) AS 'max', AVG(cantidad) AS 'prom'
                FROM (
                    SELECT semana, SUM(recusados) AS recusados, SUM(cortados) AS cortados, IF(recusados > 0, recusados/cortados*100, NULL) AS cantidad
                    FROM (
                        SELECT semana, COUNT(1) AS cortados, SUM(IF(tipo = 'RECU', 1, 0)) AS recusados
                        FROM produccion_racimos
                        WHERE YEAR = YEAR('{$postdata->fecha_inicial}') $sWhere
                        GROUP BY semana
                    ) AS tbl
                    GROUP BY semana
                ) AS tbl");

            $totales->prom = $row_totales_gropales->prom;
            $totales->sum = $row_totales_gropales->sum;
            $totales->min = $row_totales_gropales->min;
            $totales->max = $row_totales_gropales->max;
            $response->data = $this->db->queryAll($sql);
            $response->data[] = $totales;
        }
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM produccion_racimos WHERE tipo = 'RECU' AND YEAR = YEAR('{$postdata->fecha_inicial}') $sWhere GROUP BY semana");
        return $response;
    }

    private function grafica_z($data = [], $group_y = [], $selected = [], $types = [], $xAxisName = ""){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["legend"]["selected"] = $selected != null ? $selected : true;
		$options["xAxis"] = [
			[
                "name" => $xAxisName,
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
        }

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				//'max' => ($key == 1) ? $maxs[$key] + ($maxs[$key] - $mins[$key]) * .05 : null,
                //'min' => ($key == 1) ? $mins[$key] - ($maxs[$key] - $mins[$key]) * .05 : null,
                'max' => null,
                'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
        }

		foreach($_names as $i => $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => isset($types[$i]) ? $types[$i] : 'line',
				"connectNulls" => true,
                "data" => [],
                "itemStyle" => [
                    "normal" => [
                        "barBorderRadius" => "0",
                        "barBorderWidth" => "6",
                        "label" => [
                            "show" => true
                        ]
                    ]
                ]
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
    }
    
    private function grafica_scatter($data = [], $yAxisName = "", $xAxisName = ""){
        $response = new stdClass;
        $response->yAxis = [
            "name" => $yAxisName,
            "min" => 'dataMin'
        ];
        $response->xAxis = [
            "name" => $xAxisName
        ];

        $_data = [];
        foreach($data as $r){
            $_data[] = [$r->index_0, $r->index_1];
        }

        $response->series = [
            [
                "data" => $_data,
                "type" => 'scatter'
            ]
        ];

        return $response;
    }
}
