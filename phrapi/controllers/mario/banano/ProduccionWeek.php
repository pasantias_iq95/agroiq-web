<?php defined('PHRAPI') or die("Direct access not allowed!");
class ProduccionWeek {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// D($this->session);
        
        $this->db = DB::getInstance($this->session->agent_user);
        $this->sigat = DB::getInstance("sigat");
    }
    
    public function last(){
        $response = new stdClass;
        $response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM (
            SELECT MAX(fecha) AS fecha
            FROM produccion_historica
            UNION ALL
            SELECT MAX(fecha) AS fecha
            FROM produccion_cajas
            UNION ALL
            SELECT MAX(fecha) AS fecha
            FROM produccion_gavetas
        ) AS tbl");
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM (
            SELECT id_finca, finca
            FROM produccion_historica
            WHERE fecha = '{$response->fecha}'
            UNION ALL
            SELECT id_finca, finca
            FROM produccion_cajas
            WHERE fecha = '{$response->fecha}'
            UNION ALL
            SELECT id_finca, finca
            FROM produccion_gavetas
            WHERE fecha = '{$response->fecha}'
        ) AS tbl");
        return $response;
    }


	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$response->peso = $this->generateDataPeso($filters);
		$response->manos = $this->generateDataManos($filters);
		$response->calibracion = $this->generateDataCalibracion($filters);
		$response->racimos = $this->generateDataCosecha($filters);
		return $response;
    }

    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			'actualPage' => (int) getValueFrom($postdata, 'actualPage', 0),
			'pagination' => (int) getValueFrom($postdata, 'pagination', 10),
			'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
			'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'semana' => (int) getValueFrom($postdata, 'semana', 0),
            'finca' => (int) getValueFrom($postdata, 'finca', ''),
            'var1' => getValueFrom($postdata, 'var1', ''),
            'var2' => getValueFrom($postdata, 'var2', ''),
            'type1' => getValueFrom($postdata, 'type1', ''),
            'type2' => getValueFrom($postdata, 'type2', ''),
        ];
        return $filters;
    }
    
    public function tags(){
        $response = new stdClass;
        $response->data = [];
        $filters = $this->params();

        $resumen_cajas = $this->getResumenCajas($filters);

        if($filters->fecha_inicial != "" && $filters->fecha_final != ""){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' ";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }


        $sum_kg_cajas = 0;
        $sum_conv_cajas = 0;
        foreach($resumen_cajas as $row){
            $sum_kg_cajas += $row->total_kg;
            $sum_conv_cajas += $row->conv;
        }

        // PESO PROM CAJAS
        $response->data["peso_prom_cajas"]["value"] = round($sum_kg_cajas / $sum_conv_cajas, 2);
        // CALIBRACION PROM
        $response->data["calibracion_prom"]["value"] = round($this->db->queryRow("SELECT AVG(calibracion) as suma FROM produccion_historica WHERE tipo = 'PROCESADO' AND calibracion IS NOT NULL AND calibracion > 0 AND cinta != 'n/a' {$sWhere}")->suma, 2);
        // PESO PROM RACIMOS
        $response->data["peso_prom_racimos"]["value"] = round($this->db->queryOne("SELECT AVG(peso) as suma FROM produccion_historica WHERE tipo = 'PROCESADO' {$sWhere}"), 2);
        // EDAD PROM RACIMOS
        $response->data["edad_prom_racimos"]["value"] = round($this->db->queryOne("SELECT ROUND(AVG(edad), 2) AS edad FROM produccion_historica WHERE tipo = 'PROCESADO' {$sWhere}"), 2);
        // RATIO CORTADO
        $response->data["ratio_cortado"]["value"] = round($sum_conv_cajas / $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE 1=1 {$sWhere}"), 2);
        // RATIO PROCESADO
        $response->data["ratio_procesado"]["value"] = round($sum_conv_cajas / $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROCESADO' {$sWhere}"), 2);
        // % MERMA COSECHADA
        $sum_kg_racimos_cosechados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_historica WHERE 1=1 {$sWhere}");
        $response->data["porc_merma_cosechada"]["value"] = round(($sum_kg_racimos_cosechados - $sum_kg_cajas) / $sum_kg_racimos_cosechados * 100, 2);
        // % MERMA PROCESADA
        $sum_kg_racimos_procesado = $this->db->queryOne("SELECT SUM(peso) FROM produccion_historica WHERE tipo = 'PROCESADO' {$sWhere}");
        $response->data["porc_merma_procesado"]["value"] = round(($sum_kg_racimos_procesado - $sum_kg_cajas) / $sum_kg_racimos_procesado * 100, 2);
        // ENFUNDE / HA
        $response->data["enfunde_ha"]["value"] = round($this->db->queryOne("SELECT SUM(usadas) FROM produccion_enfunde WHERE 1=1 {$sWhere}") / $this->db->queryOne("SELECT hectareas FROM fincas WHERE id = 1") / $this->db->queryOne("SELECT COUNT(1) FROM (SELECT semana FROM produccion_enfunde WHERE 1=1 {$sWhere} GROUP BY semana) AS tbl"), 2);
        // % MERMA NETA
        //$response->data["porc_merma_neta"]["value"] = round((($sum_kg_racimos_procesado * .90) - $sum_kg_cajas) / ($sum_kg_racimos_procesado  * .90) * 100, 2);
        $sql = "SELECT 
                    (SELECT SUM(cantidad) 
                    FROM merma 
                    INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                    WHERE semana = tbl.semana
                        AND porcentaje_merma > 0 
                        AND campo = 'Total Peso Merma' 
                        $sWhere) / SUM(peso_neto) * 100 AS val
                FROM(
                    SELECT SUM(peso_neto) AS peso_neto, semana
                    FROM merma
                    WHERE porcentaje_merma > 0 {$sWhere}
                    GROUP BY semana
                ) AS tbl";
        $response->data["porc_merma_neta"]["value"] = round($this->db->queryOne($sql), 2);
        // MERMA KG
        $response->data["merma_kg"]["value"] = round($sum_kg_racimos_cosechados - $sum_kg_cajas, 2);
        // MERMA CAJAS
        $response->data["merma_cajas"]["value"] = round($response->data["merma_kg"]["value"] / $response->data["peso_prom_cajas"]["value"], 2);
        // MERMA DOLARES
        $response->data["merma_dolares"]["value"] = round($response->data["merma_cajas"]["value"] * 6.2, 2);

        $response->data["kg_racimos_cosechado"]["value"] = round($sum_kg_racimos_cosechados, 2);
        $response->data["kg_sum_cajas"]["value"] = round($sum_kg_cajas, 2);

        $response->data["cajas_conv"]["value"] = $sum_conv_cajas;
        $response->data["racimos_cosechados"] = $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE 1=1 {$sWhere}");
        return $response;
    }

    public function graficaEdadPromedio(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }

        $response->chart = new stdClass;
        $response->chart->legend = $this->db->queryAllOne("SELECT semana FROM racimo_web WHERE anio = YEAR('{$filters->fecha_inicial}') {$sWhere} GROUP BY semana ORDER BY semana");
        $response->chart->umbral = $this->db->queryOne("SELECT ROUND(AVG(edad), 2) FROM racimo_web WHERE anio = YEAR('{$filters->fecha_inicial}') {$sWhere}");

        $data_series = $this->db->queryAll("
			SELECT lote, semana, (SELECT ROUND(AVG(edad), 2) FROM racimo_web WHERE semana = tbl.semana AND id_lote = tbl.id_lote AND anio = YEAR('$filters->fecha_inicial') AND edad > 0) AS edad 
            FROM (
                SELECT lote, id_lote, semana
                FROM (
                    SELECT lotes.nombre as lote, id_lote
					FROM racimo_web
					INNER JOIN lotes ON id_lote = lotes.id
                    WHERE anio = YEAR('{$filters->fecha_inicial}') {$sWhere}
                    GROUP BY id_lote
                ) AS lotes
                JOIN (
                    SELECT semana 
                    FROM racimo_web
                    WHERE anio = YEAR('$filters->fecha_inicial') {$sWhere}
                    GROUP BY semana
                ) AS semanas 
            ) AS tbl
            ORDER BY semana");
        $semanas = [];
        foreach($data_series as $row){
            if(!isset($response->chart->data[$row->lote])){
                $response->chart->data[$row->lote] = [
                    "connectNulls" => true,
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => "0",
                            "barBorderWidth" => "6",
                            "label" => [
                                "show" => true,
                                "position" => "insideTop"
                            ]
                        ]
                    ],
                    "type" => "line",
                    "name" => $row->lote,
                    "data" => []
                ];
            }
            $response->chart->data[$row->lote]["data"][] = $row->edad;
        }
        return $response;
    }

    public function resumenAcumulados(){
        $response = new stdClass;
        $filters = $this->params();

        /*if($filters->fecha_inicial != "" && $filters->fecha_final != ""){
            $sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' ";
        }*/
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        $resumen_cajas = $this->getResumenCajas((object)[ "fecha_inicial" => date('Y', strtotime($filters->fecha_inicial))."-01-01", "fecha_final" => date('Y', strtotime($filters->fecha_inicial))."-12-31" ]);
        $sum_kg_cajas = 0;
        $sum_conv_cajas = 0;
        foreach($resumen_cajas as $row){
            $sum_kg_cajas += $row->total_kg;
            $sum_conv_cajas += $row->conv;
        }

        $resumen_cajas_anio = $this->getResumenCajas((object)[ "fecha_inicial" => date('Y', strtotime($filters->fecha_inicial))."-01-01", "fecha_final" => date('Y', strtotime($filters->fecha_inicial))."-12-31" ]);
        $sum_kg_cajas_anio = 0;
        $sum_conv_cajas_anio = 0;
        foreach($resumen_cajas_anio as $row){
            $sum_kg_cajas_anio += $row->total_kg;
            $sum_conv_cajas_anio += $row->conv;
        }

        $ha = (float) $this->db->queryOne("SELECT hectareas FROM fincas WHERE id = 1");

        $racimos_procesados = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROCESADO' {$sWhere}"); 
        $racimos_recusados = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' {$sWhere}"); 
        $semanas_rac = (int) $this->db->queryOne("SELECT COUNT(1) FROM (SELECT semana FROM produccion_historica GROUP BY semana) AS tbl");
        
        $racimos_procesados_anio = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'PROCESADO' AND YEAR(fecha) = YEAR(CURRENT_DATE)");
        $racimos_recusados_anio = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE tipo = 'RECUSADO' AND YEAR(fecha) = YEAR(CURRENT_DATE)");

        $response->resumen->racimos = [
            "cosechados" => [
                "acumulado" => $racimos_recusados + $racimos_procesados,
                "ha_anio" => round(($racimos_procesados_anio + $racimos_recusados_anio) / $ha, 2),
                "ha_semana" => round(($racimos_procesados_anio + $racimos_recusados_anio) / $ha / $semanas_rac, 2),
            ],
            "recusados" => [
                "acumulado" => $racimos_recusados,
                "ha_anio" => round(($racimos_recusados_anio) / $ha, 2),
                "ha_semana" => round(($racimos_recusados_anio) / $ha / $semanas_rac, 4)
            ],
            "procesados" => [
                "acumulado" => $racimos_procesados,
                "ha_anio" => round(($racimos_procesados_anio) / $ha, 2),
                "ha_semana" => round(($racimos_procesados_anio) / $ha / $semanas_rac, 2)
            ]
        ];

        // CAJAS
        $semanas_cajas = $this->db->queryOne("SELECT COUNT(1) FROM (SELECT semana FROM produccion_cajas GROUP BY semana) AS tbl");
        $response->resumen->cajas = $this->db->queryAllIntoSpecial("SELECT marca AS id, COUNT(1) AS acumulado
            FROM produccion_cajas 
            WHERE marca != '' {$sWhere} 
            GROUP BY marca 
            UNION ALL
            SELECT marca AS id, COUNT(1) AS acumulado
            FROM produccion_gavetas
            WHERE 1=1 {$sWhere}
            GROUP BY marca");
        
        foreach($response->resumen->cajas as $row){
            if($row->id != "MI COMISARIATO")
                $row->ha_anio = round($this->db->queryOne("SELECT COUNT(1) FROM produccion_cajas WHERE marca = '{$row->id}' AND YEAR(fecha) = YEAR('{$filters->fecha_inicial}')") / $ha, 2);
            else
                $row->ha_anio = round($this->db->queryOne("SELECT COUNT(1) FROM produccion_gavetas WHERE marca = '{$row->id}' AND YEAR(fecha) = YEAR('{$filters->fecha_inicial}')") / $ha, 2);
            $row->ha_semana = $row->ha_anio / $semanas_cajas;
        }
        $response->resumen->total["TOTAL CONVERTIDAS"] = [
            "id" => "TOTAL CONVERTIDAS",
            "acumulado" => $sum_conv_cajas,
            "ha_anio" => round($sum_conv_cajas_anio / $ha, 2),
            "ha_semana" => round($sum_conv_cajas_anio / $ha / $semanas_cajas, 2)
        ];

        return $response;
    }

    private function getResumenCajas($filters){
        $filters = (object) $filters;
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }
        if($filters->fecha_inicial != ""){
            $sWhere .= " AND cajas.fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        
		$sql = "SELECT marca, SUM(cantidad) AS cantidad, SUM(total_kg) AS total_kg, CONV AS 'conv', tipo, promedio, maximo, minimo, desviacion
                FROM(
                SELECT
                    `cajas`.`marca` AS `marca`,
                    IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS `cantidad`,
                    ROUND(SUM(cajas.`caja`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.caja), 0), 2) AS total_kg,
                    IF(conv.id IS NOT NULL, 
                        ROUND(((SUM(cajas.`caja`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.caja), 0)) / 0.4536) / 40.5, 0),
                        COUNT(1)) AS conv,
                    'CAJA' AS tipo,
                    ROUND(AVG(cajas.`caja`), 2) AS promedio,
                    MAX(cajas.`caja`) AS maximo,
                    MIN(cajas.`caja`) AS minimo,
                    ROUND(STD(cajas.caja), 2) AS desviacion
                FROM (`produccion_cajas` `cajas`
                LEFT JOIN `produccion_cajas_real` `cajas_real`
                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                        AND status = 'PROCESADO'))
                LEFT JOIN produccion_cajas_convertir conv ON cajas.marca = conv.marca
                WHERE (`cajas`.`marca` <> '') $sWhere
                GROUP BY `cajas`.`marca`, cajas_real.id
				UNION ALL
                SELECT
                    `cajas`.`marca` AS `marca`,
                    IF((`cajas_real`.`id` IS NOT NULL),`cajas_real`.`valor`, COUNT(1)) AS `cantidad`,
                    ROUND(SUM(cajas.`peso`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.peso), 0), 2) AS total_kg,
                    IF(conv.id IS NOT NULL, 
                        ROUND(((SUM(cajas.`peso`) + IF(cajas_real.id IS NOT NULL, (cajas_real.valor - COUNT(1)) * AVG(cajas.peso), 0)) / 0.4536) / 40.5, 0),
                        COUNT(1)) AS conv,
                    'GAVETA' AS tipo,
                    ROUND(AVG(cajas.`peso`), 2) AS promedio,
                    MAX(cajas.`peso`) AS maximo,
                    MIN(cajas.`peso`) AS minimo,
                    ROUND(STD(cajas.peso), 2) AS desviacion
                FROM (`produccion_gavetas` `cajas`
                LEFT JOIN `produccion_cajas_real` `cajas_real`
                    ON (((`cajas_real`.`fecha` = `cajas`.`fecha`)
                        AND (CONVERT(`cajas_real`.`marca` USING utf8) = `cajas`.`marca`))
                        AND status = 'PROCESADO'))
                LEFT JOIN produccion_cajas_convertir conv ON cajas.marca = conv.marca
                WHERE (`cajas`.`marca` <> '') $sWhere
                GROUP BY `cajas`.`marca`, cajas_real.id
                ) AS tbl
                GROUP BY marca";

		return $this->db->queryAll($sql);
    }

	public function reporteProduccion(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }

        $response->data = $this->db->queryAll("SELECT YEAR(CURRENT_DATE) AS anio, 
                STR_TO_DATE(CONCAT(YEAR(CURRENT_DATE), semana,' Monday'), '%X%V %W') AS start,
                STR_TO_DATE(CONCAT(YEAR(CURRENT_DATE), semana,' Saturday'), '%X%V %W') AS end,
                semana,
                (SELECT COUNT(1) FROM produccion_historica WHERE semana = tbl.semana AND tipo = 'PROCESADO' {$sWhere}) AS racimos_procesados,
                (SELECT COUNT(1) FROM produccion_historica WHERE semana = tbl.semana AND tipo = 'RECUSADO' {$sWhere}) AS racimos_recusados,
                (SELECT SUM(peso) FROM produccion_historica WHERE semana = tbl.semana {$sWhere}) AS peso_racimos_cortados,
                (SELECT SUM(peso) FROM produccion_historica WHERE semana = tbl.semana AND tipo = 'PROCESADO' {$sWhere}) AS peso_racimos_procesados,
                ROUND((SELECT AVG(peso) FROM produccion_historica WHERE semana = tbl.semana AND tipo = 'PROCESADO' {$sWhere}), 2) AS peso_prom_racimos,
                (SELECT COUNT(1) FROM produccion_cajas WHERE semana = tbl.semana {$sWhere})+(SELECT COUNT(1) FROM produccion_gavetas WHERE semana = tbl.semana {$sWhere}) AS cajas,
                (SELECT ROUND(SUM(conv), 0) FROM produccion_cajas WHERE semana = tbl.semana {$sWhere}) AS conv
            FROM (
                SELECT semana 
                FROM (
                    SELECT semana
                    FROM produccion_historica
                    WHERE semana > 0 AND YEAR = YEAR(CURRENT_DATE) {$sWhere}
                    GROUP BY semana
                    UNION ALL
                    SELECT semana
                    FROM merma
                    WHERE semana > 0 AND YEAR = YEAR(CURRENT_DATE) {$sWhere}
                    GROUP BY semana
                    UNION ALL
                    SELECT semana
                    FROM produccion_cajas
                    WHERE semana > 0 AND YEAR = YEAR(CURRENT_DATE) {$sWhere}
                    GROUP BY semana
                ) AS tbl
                GROUP BY semana
                ORDER BY semana
            ) AS tbl");
        foreach($response->data as $row){
            $row->racimos_procesados = (int) $row->racimos_procesados;
            $row->racimos_recusados = (int) $row->racimos_recusados;

            $cajas_conv = $this->getResumenCajas(["fecha_inicial" => $row->start, "fecha_final" => $row->end, "finca" => $filters->finca]);
            foreach($cajas_conv as $r){
                $row->cajas_conv += $r->conv;
                $row->peso_cajas += $r->total_kg;
            }

            $merma_neta = "SELECT 
                                (SELECT SUM(cantidad) 
                                FROM merma 
                                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                WHERE semana = tbl.semana
                                    AND porcentaje_merma > 0 
                                    AND campo = 'Total Peso Merma' 
                                    $sWhere) / SUM(peso_neto) * 100 AS val
                            FROM(
                                SELECT SUM(peso_neto) AS peso_neto, semana
                                FROM merma
                                WHERE porcentaje_merma > 0 AND semana = $row->semana {$sWhere}
                                GROUP BY semana
                            ) AS tbl";
            $row->merma_neta = round($this->db->queryOne($merma_neta), 2);
            $row->detalle = $this->reporteProduccionLote($row->semana, $row->start, $row->end);
        }

        return $response;
    }

    public function graficaVariables(){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }

        $variables = [
            "Peso" => "SELECT semana AS label_x, ROUND(AVG(peso), 2) AS value, 0 AS index_y, 'Peso' AS name
                        FROM produccion_historica
                        WHERE YEAR = YEAR(CURRENT_DATE)
                        GROUP BY semana
                        ORDER BY semana",
            "Manos" => "SELECT semana AS label_x, ROUND(AVG(manos), 2) AS value, 0 AS index_y, 'Manos' AS name
                        FROM produccion_historica
                        WHERE YEAR = YEAR(CURRENT_DATE)
                        GROUP BY semana
                        ORDER BY semana",
            "Calibre" => "SELECT semana AS label_x, ROUND(AVG(calibracion), 2) AS value, 0 AS index_y, 'Calibre' AS name
                            FROM produccion_historica
                            WHERE YEAR = YEAR(CURRENT_DATE) AND calibracion > 0
                            GROUP BY semana
                            ORDER BY semana",
            "Edad" => "SELECT semana AS label_x, ROUND(AVG(edad), 2) AS value, 0 AS index_y, 'Edad' AS name
                        FROM produccion_historica
                        WHERE YEAR = YEAR(CURRENT_DATE) AND edad > 0
                        GROUP BY semana
                        ORDER BY semana",
            "Merma Neta" => "SELECT semana AS label_x,
                                ROUND((SELECT SUM(cantidad) 
                                    FROM merma 
                                    INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                    WHERE semana = tbl.semana
                                        AND porcentaje_merma > 0 
                                        AND campo = 'Total Peso Merma' 
                                        AND year = YEAR(CURRENT_DATE)
                                        $sWhere) / SUM(peso_neto) * 100, 2) AS value,
                                0 AS index_y, 'Merma Neta' AS name
                            FROM(
                                SELECT SUM(peso_neto) AS peso_neto, semana
                                FROM merma
                                WHERE porcentaje_merma > 0 {$sWhere} AND year = YEAR(CURRENT_DATE)
                                GROUP BY semana
                            ) AS tbl
                            GROUP BY semana",
            "Temp Min." => "SELECT semana AS label_x, temp_minima AS value, 0 AS index_y, 'Temp Min.' AS name FROM datos_clima_resumen WHERE id_hacienda = 78 AND anio = YEAR(CURRENT_DATE) ORDER BY semana",
            "Horas Luz (100)" => "SELECT semana AS label_x, horas_luz_100 AS value, 0 AS index_y, 'Horas Luz (100)' AS name FROM datos_clima_resumen WHERE id_hacienda = 78 AND anio = YEAR(CURRENT_DATE) ORDER BY semana",
            "Horas Luz (150)" => "SELECT semana AS label_x, horas_luz_150 AS value, 0 AS index_y, 'Horas Luz (150)' AS name FROM datos_clima_resumen WHERE id_hacienda = 78 AND anio = YEAR(CURRENT_DATE) ORDER BY semana",
            "Horas Luz (200)" => "SELECT semana AS label_x, horas_luz_200 AS value, 0 AS index_y, 'Horas Luz (200)' AS name FROM datos_clima_resumen WHERE id_hacienda = 78 AND anio = YEAR(CURRENT_DATE) ORDER BY semana",
            "Horas Luz (400)" => "SELECT semana AS label_x, horas_luz_400 AS value, 0 AS index_y, 'Horas Luz (400)' AS name FROM datos_clima_resumen WHERE id_hacienda = 78 AND anio = YEAR(CURRENT_DATE) ORDER BY semana",
            "Racimos Procesados" => "SELECT semana AS label_x, COUNT(1) AS value, 0 AS index_y, 'Racimos Procesados' AS name
                                    FROM produccion_historica
                                    WHERE YEAR = YEAR(CURRENT_DATE) AND tipo = 'PROCESADO'
                                    GROUP BY semana
                                    ORDER BY semana",
            "Racimos Cosechados" => "SELECT semana AS label_x, COUNT(1) AS value, 1 AS index_y, 'Racimos Cosechados' AS name
                                    FROM produccion_historica
                                    WHERE YEAR = YEAR(CURRENT_DATE)
                                    GROUP BY semana
                                    ORDER BY semana",
            "Cajas" => "SELECT semana AS label_x, SUM(value) AS value, 0 AS index_y, 'Cajas' AS name
                                    FROM (
                                        SELECT semana, COUNT(1) AS value
                                        FROM produccion_cajas
                                        WHERE YEAR = YEAR(CURRENT_DATE)
                                        GROUP BY semana
                                        UNION ALL
                                        SELECT semana, COUNT(1) AS value
                                        FROM produccion_gavetas
                                        WHERE YEAR = YEAR(CURRENT_DATE)
                                        GROUP BY semana
                                    ) AS tbl
                                    GROUP BY semana
                                    ORDER BY semana",
            "Temp Max." => "SELECT semana AS label_x, temp_maxima AS value, 0 AS index_y, 'Temp Max.' AS name FROM datos_clima_resumen WHERE id_hacienda = 78 AND anio = YEAR(CURRENT_DATE) ORDER BY semana",
            "Lluvia" => "SELECT semana AS label_x, lluvia AS value, 0 AS index_y, 'Lluvia' AS name FROM datos_clima_resumen WHERE id_hacienda = 78 AND anio = YEAR(CURRENT_DATE) ORDER BY semana",
            "Humedad" => "SELECT semana AS label_x, humedad AS value, 0 AS index_y, 'Humedad' AS name FROM datos_clima_resumen WHERE id_hacienda = 78 AND anio = YEAR(CURRENT_DATE) ORDER BY semana",
            "Rad. Solar" => "SELECT semana AS label_x, rad_solar AS value, 0 AS index_y, 'Rad. Solar' AS name FROM datos_clima_resumen WHERE id_hacienda = 78 AND anio = YEAR(CURRENT_DATE) ORDER BY semana",
            "Tallo" => "SELECT semana AS label_x, 
                            ROUND(SUM(tallo) / SUM(racimo) * 100, 2) AS value,
                            0 AS index_y, 'Tallo' AS name
                        FROM merma main
                        WHERE year = YEAR(CURRENT_DATE) $sWhere
                        GROUP BY semana",
            "Cosecha" => "SELECT semana AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE semana =  merma.semana
                                                AND campo LIKE 'Total Peso Merma' AND TYPE = 'RESULTADOS'
                                                AND year = YEAR(CURRENT_DATE)
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'COSECHA' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso Merma%' AND TYPE = 'COSECHA' AND year = YEAR(CURRENT_DATE)
                                $sWhere
                            GROUP BY semana",
            "Enfunde" => "SELECT semana AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE semana =  merma.semana
                                                AND campo LIKE 'Total Peso Merma' AND TYPE = 'RESULTADOS'
                                                AND year = YEAR(CURRENT_DATE)
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'ENFUNDE' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso Merma%' AND TYPE = 'ENFUNDE' AND year = YEAR(CURRENT_DATE)
                                $sWhere
                            GROUP BY semana",
            "ADM" => "SELECT semana AS label_x,
                                ROUND(
                                    (SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma m
                                        INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                        WHERE semana =  merma.semana
                                            AND campo LIKE 'Total Peso Merma' AND TYPE = 'RESULTADOS'
                                            AND year = YEAR(CURRENT_DATE)
                                            $sWhere) * 100), 2) AS value,
                                0 AS index_y, 'ADM' AS name
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE campo LIKE 'Total Peso Merma%' AND TYPE = 'ADM' AND year = YEAR(CURRENT_DATE)
                            $sWhere
                        GROUP BY semana",
            "Natural" => "SELECT semana AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE semana =  merma.semana
                                                AND campo LIKE 'Total Peso Merma' AND TYPE = 'RESULTADOS'
                                                AND year = YEAR(CURRENT_DATE)
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'NATURAL' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso Merma%' AND TYPE = 'NATURAL' AND year = YEAR(CURRENT_DATE)
                                $sWhere
                            GROUP BY semana",
            "Proceso" => "SELECT semana AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE semana =  merma.semana
                                                AND campo LIKE 'Total Peso Merma' AND TYPE = 'RESULTADOS'
                                                AND year = YEAR(CURRENT_DATE)
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'PROCESO' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso Merma%' AND TYPE = 'PROCESO' AND year = YEAR(CURRENT_DATE)
                                $sWhere
                            GROUP BY semana",
            "Ratio Cortado" => "SELECT semana AS label_x, ROUND(conv / (SELECT COUNT(1) FROM produccion_historica WHERE semana = tbl.semana), 2) AS value, 0 AS index_y, 'Ratio Cortado' AS name 
                                FROM (
                                    SELECT semana, SUM(conv) AS conv
                                    FROM produccion_cajas 
                                    GROUP BY semana
                                ) AS tbl",
            "Ratio Procesado" => "SELECT semana AS label_x, ROUND(conv / (SELECT COUNT(1) FROM produccion_historica WHERE semana = tbl.semana AND tipo = 'PROCESADO'), 2) AS value, 0 AS index_y, 'Ratio Procesado' AS name 
                                FROM (
                                    SELECT semana, SUM(conv) AS conv
                                    FROM produccion_cajas 
                                    GROUP BY semana
                                ) AS tbl"
        ];
        
        $data_chart = [];
        $clima = ["Temp Min.", "Horas Luz (400)", "Horas Luz (100)", "Horas Luz (150)", "Horas Luz (200)", "Temp Max.", "Lluvia", "Humedad", "Rad. Solar"];
        if(in_array($filters->var1, $clima)){
            $data_chart = $this->sigat->queryAll($variables[$filters->var1]);
        }else{
            $data_chart = $this->db->queryAll($variables[$filters->var1]);
        }

        $sql = $variables[$filters->var2];
        $sql = str_replace("0 AS index_y", "1 AS index_y", $sql);
        if(in_array($filters->var2, $clima)){
            $data_chart = array_merge($data_chart, $this->sigat->queryAll($sql));
        }else{
            $data_chart = array_merge($data_chart, $this->db->queryAll($sql));
        }

        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => '',
				"type" => 'line',
				'format' => ''
			],
			[
				"name" => '',
				"type" => 'line',
				'format' => ''
			]
        ];

        /*$selected = [
            "Edad" => false,
            "Calibre" => false,
            "Peso" => false,
            "Merma Neta" => false,
            "Racimos Procesados" => false,
            "Racimos Cosechados" => true,
            "Cajas" => false,
            'Temp Min.' => true,
            'Horas Luz (400)' => false
        ];*/
        $selected = [];
        $types = [$filters->type1, $filters->type2];
        
        $response->data = $this->grafica_z($data_chart, $groups, $selected, $types);

        return $response;
    }

    public function reporteProduccionLote($semana, $start_of_week, $end_of_week){
        $response = new stdClass;
        $filters = $this->params();

        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }

        $response->data = $this->db->queryAll("SELECT YEAR(CURRENT_DATE) AS anio, lote,
                (SELECT COUNT(1) FROM produccion_historica WHERE semana = {$semana} AND lote = tbl.lote AND tipo = 'PROCESADO' {$sWhere}) AS racimos_procesados,
                (SELECT COUNT(1) FROM produccion_historica WHERE semana = {$semana} AND lote = tbl.lote AND tipo = 'RECUSADO' {$sWhere}) AS racimos_recusados,
                (SELECT SUM(peso) FROM produccion_historica WHERE semana = {$semana} AND lote = tbl.lote {$sWhere}) AS peso_racimos_cortados,
                (SELECT SUM(peso) FROM produccion_historica WHERE semana = {$semana} AND lote = tbl.lote AND tipo = 'PROCESADO' {$sWhere}) AS peso_racimos_procesados,
                ROUND((SELECT AVG(peso) FROM produccion_historica WHERE semana = {$semana} AND lote = tbl.lote AND tipo = 'PROCESADO' {$sWhere}), 2) AS peso_prom_racimos
            FROM (
                SELECT lote 
                FROM (
                    SELECT lote
                    FROM produccion_historica
                    WHERE lote != '' AND YEAR = YEAR(CURRENT_DATE) AND semana = {$semana} {$sWhere}
                    GROUP BY lote
                    UNION ALL
                    SELECT bloque
                    FROM merma
                    WHERE bloque != '' AND YEAR = YEAR(CURRENT_DATE) AND semana = {$semana} {$sWhere}
                    GROUP BY bloque
                ) AS tbl
                GROUP BY lote
                ORDER BY lote
            ) AS tbl");
        foreach($response->data as $row){
            $row->racimos_procesados = (int) $row->racimos_procesados;
            $row->racimos_recusados = (int) $row->racimos_recusados;

            $merma_neta = "SELECT 
                        (SELECT SUM(cantidad) 
                        FROM merma 
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE semana = $semana AND bloque = tbl.bloque
                            AND porcentaje_merma > 0 
                            AND campo = 'Total Peso Merma' 
                            $sWhere) / SUM(peso_neto) * 100 AS val
                    FROM(
                        SELECT SUM(peso_neto) AS peso_neto, bloque
                        FROM merma
                        WHERE porcentaje_merma > 0 AND semana = $semana AND bloque = '$row->lote' {$sWhere}
                        GROUP BY bloque
                    ) AS tbl";
            $row->merma_neta = round($this->db->queryOne($merma_neta), 2);
        }
        return $response->data;
    }

    /* VIEJO MODULO */

	public function produccionActual(){
		$data = $this->db->queryAll("SELECT *, WEEK(fecha) as semana, CEIL(WEEK(fecha)/4) as periodo FROM produccion WHERE year >= 2017");
		$data_procesada = [];
		foreach($data as $reg){
			if($reg->cosechados_blanco > 0){
				$data_procesada[$reg->semana]["blancas"]["sum_edades"] += $this->calcularEdad("BLANCA", (int) $reg->year, $reg->periodo, (int) $reg->semana);
				$data_procesada[$reg->semana]["blancas"]["prom_edades"][] = $this->calcularEdad("BLANCA", (int) $reg->year, $reg->periodo, (int) $reg->semana);
			}
			if($reg->cosechados_negro > 0){
				$data_procesada[$reg->semana]["negras"]["sum_edades"] += $this->calcularEdad("NEGRA", $reg->year, $reg->periodo, $reg->semana);
				$data_procesada[$reg->semana]["negras"]["prom_edades"][] = $this->calcularEdad("NEGRA", $reg->year, $reg->periodo, $reg->semana);
			}
			if($reg->cosechados_azul > 0){
				$data_procesada[$reg->semana]["azules"]["sum_edades"] += $this->calcularEdad("AZUL", $reg->year, $reg->periodo, $reg->semana);
				$data_procesada[$reg->semana]["azules"]["prom_edades"][] = $this->calcularEdad("AZUL", $reg->year, $reg->periodo, $reg->semana);
			}
		}
		foreach($data_procesada as $semana => $colores){
			foreach($colores as $color => $value){
				$data_sem[$semana][$color] = ($colores[$color]["sum_edades"] / count($colores[$color]["prom_edades"]));
			}
			#$data_sem[$semana]["blancas"] = ($blancas["blancas"]["sum_edades"] / count($blancas["blancas"]["prom_edades"]));
		}
		return $data_sem;
	}

	private function calcularEdad($color, $anio, $periodo, $semana){
		$secuencia = [
			"2016" => [
				10 => [ 37 => "CAFE", 38 => "AMARILLA", 39 => "VERDE", 40 => "AZUL"],
				11 => [ 41 => "BLANCA", 42 => "NEGRA", 43 => "LILA", 44 => "ROJA" ], 
				12 => [ 45 => "CAFE", 46 => "AMARILLA", 47 => "VERDE", 48 => "AZUL" ],
				13 => [ 49 => "BLANCA", 50 => "NEGRA", 51 => "LILA", 52 => "ROJA" ]
			],
			"2017" => [
				1 => [ 1 => "ROJA-CAFE", 2 => "CAFE", 3 => "NEGRA", 4 => "VERDE" ],
				2 => [ 5 => "AZUL", 6 => "BLANCA", 7 => "AMARILLA", 8 => "LILA", ],
				3 => [ 9 => "ROJA", 10 => "CAFE", 11 => "NEGRA", 12 => "VERDE" ],
				4 => [ 13 => "AZUL", 14 => "BLANCA", 15 => "AMARILLA", 16 => "LILA" ],
				5 => [ 17 => "ROJA", 18 => "CAFE", 19 => "NEGRA", 20 => "VERDE" ],
				6 => [ 21 => "AZUL", 22 => "BLANCA", 23 => "AMARILLA", 24 => "LILA" ],
				7 => [ 25 => "ROJA", 26 => "CAFE", 27 => "NEGRA", 28 => "VERDE" ],
				8 => [ 29 => "AZUL", 30 => "BLANCA", 31 => "AMARILLA", 32 => "LILA" ],
				9 => [ 33 => "ROJA", 34 => "CAFE", 35 => "NEGRA", 36 => "VERDE" ],
				10 => [ 37 => "AZUL", 38 => "BLANCA", 39 => "AMARILLA", 40 => "LILA" ],
				11 => [ 41 => "ROJA", 42 => "CAFE", 43 => "NEGRA", 44 => "VERDE" ],
				12 => [ 45 => "AZUL", 46 => "BLANCA", 47 => "AMARILLA", 48 => "LILA" ],
				13 => [ 49 => "ROJA", 50 => "CAFE", 51 => "NEGRA", 52 => "VERDE" ]
			]
		];
		$edad = 0;
		$encontro = false;
		$semana_edad = 0;
		$pr = 0;

		#$this->DD($semana);
		
		if($anio == 2017 && $semana < 18){
			if(in_array($semana, [14,15,16,17]))
				$pr = 13;
			if(in_array($semana, [10,11,12,13]))
				$pr = 12;
			if(in_array($semana, [6,8,9,10,3]))
				$pr = 11;
			if(in_array($semana, [5,7,4,2]))
				$pr = 10;

			foreach($secuencia["2016"][$pr] as $sec_periodo){
				if(!$encontro)
				foreach($sec_periodo as $sem => $col){					
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
						break;
					}
				}
			}
			$edad = $semana_edad;
		}else if($semana >= 18){
			foreach($secuencia["2017"][$periodo-4] as $sec_periodo){
				foreach($sec_periodo as $sem => $col){
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
					}
				}
			}
			$edad = $semana - $semana_edad;
		}else{
			foreach($secuencia["2017"][$periodo+13-5] as $sec_periodo){
				foreach($sec_periodo as $sem => $col){
					if($col == $color){
						$encontro = true;
						$semana_edad = $sem;
					}
				}
			}
			$edad = $semana + (13 - $semana_edad);
		}

		return $edad;
	}

	private function DD($string){
		echo "<pre>";
		D($string);
		echo "</pre>";
	}

	private function getLotes($semana = 0){
		$response = new stdClass;
		$response->data = [];
		if($semana > 0){
			$sYear = " AND year = YEAR(CURRENT_DATE)";
			$sSemana = "p.semana";
			
			$sql = "SELECT $sSemana AS semana , lote,
					total_cosechados , 
					total_recusados,
					total_procesada,
					'false' AS expanded,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote) / cosechados) AS ratio_cortado,
					((SELECT SUM(caja) AS caja FROM produccion_cajas WHERE year = YEAR(CURRENT_DATE) AND semana = p.semana AND lote = p.lote) / procesado) AS ratio_procesado,
					(SELECT COUNT(1) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS muestrados,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = {$semana}  AND lote = p.lote {$sYear}) AS largo_dedos,
					(SELECT AVG(peso) FROM produccion_peso WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS peso,
					(SELECT AVG(largo_dedos) FROM produccion_muestra WHERE WEEK(fecha) = $sSemana {$sYear} AND lote = p.lote) AS largo_dedos,
					(SELECT AVG(calibracion) FROM produccion_calibracion WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS calibracion,
					(SELECT AVG(manos) FROM produccion_manos WHERE semana = $sSemana {$sYear} AND lote = p.lote) AS manos
				FROM(
					SELECT $sSemana, lote, 
						SUM(total_cosechados) AS total_cosechados,
						SUM(total_recusados) AS total_recusados,
						SUM(total_cosechados) - SUM(total_recusados) AS total_procesada,
						SUM(total_cosechados) as cosechados,
						SUM(total_cosechados-total_recusados) AS procesado
					FROM produccion AS p
					WHERE 1 = 1 AND $sSemana = {$semana} {$sYear}
					GROUP BY lote
				) AS p";
			
			$data = $this->db->queryAll($sql);
			foreach ($data as $key => $value) {
				$response->data[$value->lote]["campo"] = $value->lote;
				$response->data[$value->lote]["muestrados"] = (double)$value->muestrados;
				$response->data[$value->lote]["peso"] = (double)$value->peso;
				$response->data[$value->lote]["manos"] = (double)$value->manos;
				$response->data[$value->lote]["largo_dedos"] = (double)$value->largo_dedos;
				$response->data[$value->lote]["calibracion"] = (double)$value->calibracion;
				$response->data[$value->lote]["total_cosechados"] = (double)$value->total_cosechados;
				$response->data[$value->lote]["total_recusados"] = (double)$value->total_recusados;
				$response->data[$value->lote]["total_procesada"] = (double)$value->total_procesada;
				$response->data[$value->lote]["total_procesada"] = (double)$value->total_procesada;
				$response->data[$value->lote]["ratio_procesado"] = (double)$value->ratio_procesado;
				$response->data[$value->lote]["ratio_cortado"] = (double)$value->ratio_cortado;
			}
		}

		return $response->data;
	}

	private function getPromedioEdadWeek(){
		$sql = "SELECT WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY WEEK(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->semana]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->semana]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$response->total[$value->semana]["sum"] += $value->cosechados_blanco * $response->data[$value->semana]["blanco"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_blanco;
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->semana]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->semana]["negra"]["cosechadas"] = $value->cosechados_negro;
				$response->total[$value->semana]["sum"] += $value->cosechados_negro * $response->data[$value->semana]["negra"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_negro;
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->semana]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->semana]["lila"]["cosechadas"] = $value->cosechados_lila;
				$response->total[$value->semana]["sum"] += $value->cosechados_lila * $response->data[$value->semana]["lila"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_lila;
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->semana]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->semana]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$response->total[$value->semana]["sum"] += $value->cosechados_rojo * $response->data[$value->semana]["roja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_rojo;
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->semana]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->semana]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$response->total[$value->semana]["sum"] += $value->cosechados_cafe * $response->data[$value->semana]["cafe"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_cafe;
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->semana]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->semana]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$response->total[$value->semana]["sum"] += $value->cosechados_amarillo * $response->data[$value->semana]["amarilla"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_amarillo;
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->semana]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->semana]["verde"]["cosechadas"] = $value->cosechados_verde;
				$response->total[$value->semana]["sum"] += $value->cosechados_verde * $response->data[$value->semana]["verde"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_verde;
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->semana]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->semana]["azul"]["cosechadas"] = $value->cosechados_azul;
				$response->total[$value->semana]["sum"] += $value->cosechados_azul * $response->data[$value->semana]["azul"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_azul;
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->semana]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->semana]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$response->total[$value->semana]["sum"] += $value->cosechados_naranja * $response->data[$value->semana]["naranja"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_naranja;
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->semana]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->semana]["gris"]["cosechadas"] = $value->cosechados_gris;
				$response->total[$value->semana]["sum"] += $value->cosechados_gris * $response->data[$value->semana]["gris"]["edad"];
				$response->total[$value->semana]["sum_cosecha"] += $value->cosechados_gris;
			}
			$response->edad_promedio[$value->semana] = [
				"label" => $value->semana,
				"position" => 0,
				"legend" => "Edad Promedio",
				"value" => (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]),
			];

			$response->edad_prom[$value->semana] = (double)($response->total[$value->semana]["sum"] / $response->total[$value->semana]["sum_cosecha"]);
		}

		$response->edad = array_sum($response->edad_prom) / count($response->edad_prom);
		$minMax = [
			"min" => 9,
			"max" => 15,
		];
		$response->edad_promedio = $this->chartInit($response->edad_promedio , "vertical" , "Edad Promedio" , "line" , $minMax);
		return $response;
	}

	private function getPromedioEdad(){
		$sql = "SELECT DATE(fecha) AS fecha,WEEK(m.fecha) AS semana,SUM(cosechados_blanco) AS cosechados_blanco , SUM(cosechados_negro) AS cosechados_negro , 
				SUM(cosechados_lila) AS cosechados_lila ,SUM(cosechados_rojo) AS cosechados_rojo , SUM(cosechados_cafe) AS cosechados_cafe , 
				SUM(cosechados_amarillo) AS cosechados_amarillo , SUM(cosechados_verde) AS cosechados_verde , 
				SUM(cosechados_azul) AS cosechados_azul,SUM(cosechados_naranja) AS cosechados_naranja,SUM(cosechados_gris) AS cosechados_gris
				FROM produccion AS m
				GROUP BY DATE(fecha)";
		$data = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->data = [];
		$edad_promedio = [];
		$sum_edadxcosecha = [];
		foreach ($data as $key => $value) {
			$value->cosechados_blanco = (int)$value->cosechados_blanco;
			$value->cosechados_negro = (int)$value->cosechados_negro;
			$value->cosechados_lila = (int)$value->cosechados_lila;
			$value->cosechados_rojo = (int)$value->cosechados_rojo;
			$value->cosechados_cafe = (int)$value->cosechados_cafe;
			$value->cosechados_amarillo = (int)$value->cosechados_amarillo;
			$value->cosechados_verde = (int)$value->cosechados_verde;
			$value->cosechados_azul = (int)$value->cosechados_azul;
			$value->cosechados_naranja = (int)$value->cosechados_naranja;
			$value->cosechados_gris = (int)$value->cosechados_gris;
			if($value->cosechados_blanco > 0){
				$response->data[$value->fecha]["blanco"]["edad"] = $this->getEdad("blanca" , $value->semana);
				$response->data[$value->fecha]["blanco"]["cosechadas"] = $value->cosechados_blanco;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["blanco"]["edad"] * $response->data[$value->fecha]["blanco"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["blanco"]["cosechadas"];
			}
			if($value->cosechados_negro > 0){
				$response->data[$value->fecha]["negra"]["edad"] = $this->getEdad("negra" , $value->semana);
				$response->data[$value->fecha]["negra"]["cosechadas"] = $value->cosechados_negro;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["negra"]["edad"] * $response->data[$value->fecha]["negra"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["negra"]["cosechadas"];
			}
			if($value->cosechados_lila > 0){
				$response->data[$value->fecha]["lila"]["edad"] = $this->getEdad("lila" , $value->semana);
				$response->data[$value->fecha]["lila"]["cosechadas"] = $value->cosechados_lila;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["lila"]["edad"] * $response->data[$value->fecha]["lila"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["lila"]["cosechadas"];
			}
			if($value->cosechados_rojo > 0){
				$response->data[$value->fecha]["roja"]["edad"] = $this->getEdad("roja" , $value->semana);
				$response->data[$value->fecha]["roja"]["cosechadas"] = $value->cosechados_rojo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["roja"]["edad"] * $response->data[$value->fecha]["roja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["roja"]["cosechadas"];
			}
			if($value->cosechados_cafe > 0){
				$response->data[$value->fecha]["cafe"]["edad"] = $this->getEdad("cafe" , $value->semana);
				$response->data[$value->fecha]["cafe"]["cosechadas"] = $value->cosechados_cafe;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["cafe"]["edad"] * $response->data[$value->fecha]["cafe"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["cafe"]["cosechadas"];
			}
			if($value->cosechados_amarillo > 0){
				$response->data[$value->fecha]["amarilla"]["edad"] = $this->getEdad("amarilla" , $value->semana);
				$response->data[$value->fecha]["amarilla"]["cosechadas"] = $value->cosechados_amarillo;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["amarilla"]["edad"] * $response->data[$value->fecha]["amarilla"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["amarilla"]["cosechadas"];
			}
			if($value->cosechados_verde > 0){
				$response->data[$value->fecha]["verde"]["edad"] = $this->getEdad("verde" , $value->semana);
				$response->data[$value->fecha]["verde"]["cosechadas"] = $value->cosechados_verde;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["verde"]["edad"] * $response->data[$value->fecha]["verde"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["verde"]["cosechadas"];
			}
			if($value->cosechados_azul > 0){
				$response->data[$value->fecha]["azul"]["edad"] = $this->getEdad("azul" , $value->semana);
				$response->data[$value->fecha]["azul"]["cosechadas"] = $value->cosechados_azul;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["azul"]["edad"] * $response->data[$value->fecha]["azul"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["azul"]["cosechadas"];
			}
			if($value->cosechados_naranja > 0){
				$response->data[$value->fecha]["naranja"]["edad"] = $this->getEdad("naranja" , $value->semana);
				$response->data[$value->fecha]["naranja"]["cosechadas"] = $value->cosechados_naranja;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["naranja"]["edad"] * $response->data[$value->fecha]["naranja"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["naranja"]["cosechadas"];
			}
			if($value->cosechados_gris > 0){
				$response->data[$value->fecha]["gris"]["edad"] = $this->getEdad("gris" , $value->semana);
				$response->data[$value->fecha]["gris"]["cosechadas"] = $value->cosechados_gris;
				$sum_edadxcosecha[$value->fecha]["sum"] += $response->data[$value->fecha]["gris"]["edad"] * $response->data[$value->fecha]["gris"]["cosechadas"];
				$sum_edadxcosecha[$value->fecha]["sum_cosecha"] += $response->data[$value->fecha]["gris"]["cosechadas"];
			}

			$response->edad_promedio[$value->semana][$value->fecha] = $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
			$response->data[$value->semana] += $sum_edadxcosecha[$value->fecha]["sum"] / $sum_edadxcosecha[$value->fecha]["sum_cosecha"];
		}

		return $response;
	}

	private function getEdad($color = "" , $semana = 1){
		$edad = 0;
		$sql = "SELECT
				({$semana} - (
				SELECT semana FROM semanas_colores
				WHERE YEAR(CURRENT_DATE) = year AND color = UPPER('{$color}') AND semana < ({$semana} - 8) ORDER BY semana DESC LIMIT 1
				)) + 1 AS edad";
		$edad = (int)$this->db->queryOne($sql);
		return $edad;
	}

	private function generateDataCosecha(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;
		$filters = (object)[
			"type" => getValueFrom($postdata , "type" , "cosechados" , FILTER_SANITIZE_STRING),
		];
		$sYearCampo = "YEAR(fecha)";
		$sSemana = "WEEK(p.fecha)";
		if($this->session->id_company == 2 || $this->session->id_company == 3){
			$sSemana = "semana";
			$sYearCampo = "year";
		}

		$type = [
			"cosechados" => "SUM(total_cosechados)",
			"recusados" => "SUM(total_recusados)",
			"procesada" => "(SUM(total_cosechados) - SUM(total_recusados))",
		];
		$title = "Cosechas";
		if($filters->type == "cajas"){
			$title = "Cajas";
			$sql_historico = "SELECT year AS legend,semana AS label,SUM(caja) AS value,IF(year > 2013 ,true,false) AS selected
								FROM produccion_cajas AS p
								GROUP BY year,semana";
		}else{
			$title = "Cosechas";
			$sql_historico = "SELECT {$sSemana} AS label,{$sYearCampo} AS legend,IF(year > 2013 ,true,false) AS selected,
				{$type[$filters->type]} AS value 
				FROM produccion AS p
				GROUP BY {$sYearCampo} , {$sSemana}";
		}
		
		$data = $this->db->queryAll($sql_historico);

		$minMax = [
			// "min" => 50,
			// "max" => 100,
		];
		$response->historico = $this->chartInit($data,"vertical",$title,"line",$minMax);
		return $response;
	}

	private function generateDataPeso(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(peso) AS peso 
						FROM (
							SELECT semana ,AVG(peso) as peso 
							FROM (
								(SELECT lote,semana , AVG(peso) AS peso 
									FROM produccion_historica
									WHERE year = YEAR(CURRENT_DATE)
									GROUP BY year,lote,semana)
								) AS p
							GROUP BY semana
						) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(peso),2) AS value,IF(year > 2013 ,true,false) AS selected 
							FROM (
								SELECT year , semana ,lote, AVG(peso) AS peso 
								FROM produccion_historica
								GROUP BY year,lote,semana
							) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 50,
			"max" => 100,
		];
		$response->historico = $this->chartInit($data,"vertical","Peso","line",$minMax);

		return $response;
	}

	private function generateDataManos(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(manos) AS manos 
						FROM (
							SELECT semana ,AVG(manos) as manos 
							FROM (
								(SELECT lote,semana , AVG(manos) AS manos 
								FROM produccion_historica
								WHERE year = YEAR(CURRENT_DATE)
								GROUP BY year,lote,semana)
							) AS p
							GROUP BY semana
						) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(manos),2) AS value,IF(year > 2013 ,true,false) AS selected FROM (
							SELECT year , semana ,lote,AVG(manos) AS manos FROM produccion_manos
							GROUP BY year,lote,semana) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 5,
			"max" => 10,
		];
		$response->historico = $this->chartInit($data,"vertical","Manos","line",$minMax);

		return $response;
	}

	private function generateDataCalibracion(){
		$response = new stdClass;
		$sql_promedio = "SELECT AVG(calibracion) AS calibracion 
                        FROM (
						    SELECT semana ,AVG(calibracion) as calibracion 
                            FROM (
						        (SELECT lote,semana , AVG(calibracion) AS calibracion 
                                FROM produccion_historica
						        WHERE year = YEAR(CURRENT_DATE)
						        GROUP BY year,lote,semana
                                )
                            ) AS p
						GROUP BY semana) AS p1";
		$response->promedio = (double)$this->db->queryOne($sql_promedio);

		$sql_historico = "SELECT year AS legend , semana AS label , ROUND(AVG(calibracion),2) AS value,IF(year > 2013 ,true,false) AS selected 
                            FROM (
							    SELECT year , semana ,lote,AVG(calibracion) AS calibracion 
                                FROM produccion_historica
							    GROUP BY year,lote,semana
                            ) AS details
							GROUP BY year,semana";
		$data = $this->db->queryAll($sql_historico);
		// $data = [];
		$minMax = [
			"min" => 38,
			"max" => 46,
		];
		$response->historico = $this->chartInit($data,"vertical","Calibración","line",$minMax);

		return $response;
	}

	private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if($type == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}else{
					$response->chart["legend"]["data"][] = $value->label;
					// if($count == 0){
						$response->chart["series"][$count] = [
							"name" => $value->label,
							"type" => $type,
							"data" => [(int)$value->value],
							"label" => [
								"normal" => ["show" => true , "position" => "top"],
								"emphasis" => ["show" => true , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					// }
				}
				$count++;
			}
		}

		return $response->chart;
	}
	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['45%', '40%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = $this->session->sloganCompany;
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["magicType"]["show"] = false;
			$response->pie["toolbox"]["feature"]["magicType"]["type"] = ['pie'];
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["x"] = "center";
			$response->pie["legend"]["y"] = "bottom";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];
			$colors = [];
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->label = ucwords(strtolower($value->label));
				$response->pie["legend"]["data"][] = $value->label;
				if($version === 3){
					if(isset($value->color)){
							$colors = [
								"normal" => ["color" => $value->color],
							];
					}
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							],
							"itemStyle" => $colors
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value];
					$response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value,
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
							"emphasis" => [ "label" => ["formatter" => "{b} \n {c} \n ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
	private function clear($String){
		$String = str_replace(array('á','à','â','ã','ª','ä'),"a",$String);
	    $String = str_replace(array('Á','À','Â','Ã','Ä'),"A",$String);
	    $String = str_replace(array('Í','Ì','Î','Ï'),"I",$String);
	    $String = str_replace(array('í','ì','î','ï'),"i",$String);
	    $String = str_replace(array('é','è','ê','ë'),"e",$String);
	    $String = str_replace(array('É','È','Ê','Ë'),"E",$String);
	    $String = str_replace(array('ó','ò','ô','õ','ö','º'),"o",$String);
	    $String = str_replace(array('Ó','Ò','Ô','Õ','Ö'),"O",$String);
	    $String = str_replace(array('ú','ù','û','ü'),"u",$String);
	    $String = str_replace(array('Ú','Ù','Û','Ü'),"U",$String);
	    $String = str_replace(array('[','^','´','`','¨','~',']'),"",$String);
	    $String = str_replace("ç","c",$String);
	    $String = str_replace("Ç","C",$String);
	    $String = str_replace("ñ","n",$String);
	    $String = str_replace("Ñ","N",$String);
	    $String = str_replace("Ý","Y",$String);
	    $String = str_replace("ý","y",$String);
	    $String = str_replace(".","",$String);
	    $String = str_replace("&aacute;","a",$String);
	    $String = str_replace("&Aacute;","A",$String);
	    $String = str_replace("&eacute;","e",$String);
	    $String = str_replace("&Eacute;","E",$String);
	    $String = str_replace("&iacute;","i",$String);
	    $String = str_replace("&Iacute;","I",$String);
	    $String = str_replace("&oacute;","o",$String);
	    $String = str_replace("&Oacute;","O",$String);
	    $String = str_replace("&uacute;","u",$String);
	    $String = str_replace("&Uacute;","U",$String);
	    return $String;
    }
    
    private function grafica_z($data = [], $group_y = [], $selected = null, $types = []){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["legend"]["selected"] = $selected;
		$options["xAxis"] = [
			[
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
        }

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				//'max' => ($key == 1) ? $maxs[$key] + ($maxs[$key] - $mins[$key]) * .05 : null,
                //'min' => ($key == 1) ? $mins[$key] - ($maxs[$key] - $mins[$key]) * .05 : null,
                'max' => null,
                'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
        }

		foreach($_names as $i => $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => isset($types[$i]) ? $types[$i] : 'line',
				"connectNulls" => true,
				"data" => []
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
	}
	
}
