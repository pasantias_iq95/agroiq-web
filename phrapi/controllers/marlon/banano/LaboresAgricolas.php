<?php defined('PHRAPI') or die("Direct access not allowed!");

class LaboresAgricolas {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->postdata = (object)json_decode(file_get_contents("php://input"));
    }

    public function last(){
        $response = new stdClass;
        $response->anios = $this->db->queryAllOne("SELECT anio FROM muestras_anual GROUP BY anio");
        return $response;
    }

    public function tendencia(){
        $response = new stdClass;

        $sql_hacienda_week = "SELECT *
            FROM (
                SELECT ROUND(AVG(promedio), 2) AS value, 'PROMEDIO GENERAL' AS serie, semana AS legend
                FROM muestras_anual 
                WHERE YEAR(fecha) = {$this->postdata->anio}
                GROUP BY semana
                UNION ALL
                SELECT value, fincas.`nombre` AS serie, semanas.semana AS legend
                FROM (
                    SELECT semana
                    FROM muestras_anual
                    WHERE YEAR(fecha) = {$this->postdata->anio}
                    GROUP BY semana
                ) AS semanas
                JOIN (
                    SELECT id, nombre
                    FROM fincas
                ) AS fincas
                LEFT JOIN (
                    SELECT idFinca, semana, ROUND(AVG(promedio), 2) AS value
                    FROM muestras_anual
                    WHERE YEAR(fecha) = {$this->postdata->anio}
                    GROUP BY idFinca, semana
                ) AS valores ON semanas.semana = valores.semana AND idFinca = fincas.`id`
            ) AS valores
            ORDER BY legend";
        $data_por_semana = $this->db->queryAll($sql_hacienda_week);
        $response->week = (object)[
            "legends" => [],
            "series" => [
                "PROMEDIO GENERAL" => [
                    "name" => "PROMEDIO GENERAL",
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => [],
                ]
            ]
        ];
        foreach($data_por_semana as $row){
            if(!in_array($row->legend, $response->week->legends)){
                $response->week->legends[] = $row->legend;
            }
            if(!in_array($row->serie, array_keys($response->week->series))){
                $response->week->series[$row->serie] = [
                    "name" => $row->serie,
                    "connectNulls" => true,
                    "type" => "line",
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => 0,
                            "barBorderWidth" => 6
                        ]
                    ],
                    "data" => []
                ];
            }
            $response->week->series[$row->serie]["data"][] = $row->value;
        }

        return $response;
    }

    public function tablaHistorica(){
        $response = new stdClass;

        $response->data = [];
        $response->umbrals = $this->session->umbrals;

        $fincas = $this->db->queryAll("SELECT idFinca, fincas.nombre AS finca FROM muestras_anual_resumen_finca INNER JOIN fincas ON idFinca = fincas.id WHERE anio = {$this->postdata->anio} GROUP BY idFinca");
        $semanas = $this->db->queryAll("SELECT semana FROM muestras_anual WHERE anio = {$this->postdata->anio} AND semana > 0 GROUP BY semana");

        foreach($fincas as $fila1){
            $idFinca = $fila1->idFinca;

            foreach($semanas as $s){
                $semana = $s->semana;
                $fila1->{"SEM {$semana}"} = $this->db->queryOne("SELECT promedio FROM muestras_anual_resumen_finca WHERE anio = {$this->postdata->anio} AND semana = {$semana} AND idFinca = {$idFinca}");
            }

            #segundo nivel
            $lotes = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote FROM muestras_anual_resumen_lote INNER JOIN lotes ON idLote = lotes.id WHERE anio = {$this->postdata->anio} AND muestras_anual_resumen_lote.idFinca = {$idFinca} AND promedio > 0 GROUP BY idLote");
            foreach($lotes as $fila2){
                $idLote = $fila2->idLote;

                foreach($semanas as $s2){
                    $semana2 = $s2->semana;
                    $fila2->{"SEM {$semana2}"} = $this->db->queryOne("SELECT promedio FROM muestras_anual_resumen_lote WHERE anio = {$this->postdata->anio} AND semana = {$semana2} AND idFinca = {$idFinca} AND idLote = {$idLote}");
                }

                #tercer nivel
                $labores = $this->db->queryAll("SELECT idLabor, labores.nombre AS labor FROM muestras_anual_resumen_labor INNER JOIN labores ON idLabor = labores.id WHERE anio = {$this->postdata->anio} AND idFinca = {$idFinca} AND idLote = {$idLote} AND promedio > 0 GROUP BY idLabor");
                foreach($labores as $fila3){
                    $idLabor = $fila3->idLabor;

                    foreach($semanas as $s3){
                        $semana3 = $s3->semana;
                        $fila3->{"SEM {$semana3}"} = $this->db->queryOne("SELECT promedio FROM muestras_anual_resumen_labor WHERE anio = {$this->postdata->anio} AND semana = {$semana3} AND idFinca = {$idFinca} AND idLote = {$idLote} AND idLabor = {$idLabor}");
                    }

                    #cuarto nivel
                    $causas = $this->db->queryAll("SELECT causa FROM muestras_anual_resumen_causas WHERE anio = {$this->postdata->anio} AND idFinca = {$idFinca} AND idLote = {$idLote} AND idLabor = {$idLabor} AND num_danos > 0 GROUP BY causa");
                    foreach($causas as $fila4){
                        foreach($semanas as $s4){
                            $semana4 = $s4->semana;
                            $fila4->{"SEM {$semana4}"} = $this->db->queryOne("SELECT num_danos FROM muestras_anual_resumen_causas FORCE INDEX(anio_semana_finca_lote_labor_causa) WHERE anio = {$this->postdata->anio} AND semana = {$semana4} AND idFinca = {$idFinca} AND idLote = {$idLote} AND idLabor = {$idLabor} AND causa = '{$fila4->causa}'");
                        }
                    }
                    $fila3->detalle = $causas;
                }
                $fila2->detalle = $labores;
            }
            $fila1->detalle = $lotes;
        }
        $response->semanas = $semanas;
        $response->data = $fincas;
        return $response;
    }

    public function graficaLabores(){
        $response = new stdClass;

        $response->main = $this->db->queryAll("SELECT idLabor, labores.nombre AS name, ROUND(AVG(promedio), 2) AS value FROM muestras_anual INNER JOIN labores ON idLabor = labores.id WHERE anio = {$this->postdata->anio} AND promedio > 0 GROUP BY idLabor");
        foreach($response->main as $row){
            $causas = $this->db->queryAll("SELECT causa AS name, SUM(num_danos) AS value FROM muestras_anual_resumen_causas WHERE anio = {$this->postdata->anio} AND idLabor = {$row->idLabor} AND num_danos > 0 GROUP BY causa");
            foreach($causas as $causa){
                $response->labores[$row->name] = $causas;
            }
        }
        return $response;
    }

    public function getFotos(){
        $response = new stdClass;
        $response->fotos = $this->db->queryAll("SELECT semana FROM muestras_anual anual INNER JOIN muestras_imagenes ON idMuestra = anual.id INNER JOIN labores ON idLabor = labores.id WHERE anio = {$this->postdata->anio} GROUP BY semana");
        foreach($response->fotos as $semana){
            $semana->fincas = $this->db->queryAll("SELECT idFinca, fincas.nombre AS finca FROM muestras_anual anual INNER JOIN muestras_imagenes ON idMuestra = anual.id INNER JOIN fincas ON idFinca = fincas.id WHERE anio = {$this->postdata->anio} AND semana = {$semana->semana} GROUP BY idFinca");
            foreach($semana->fincas as $finca){
                $finca->lotes = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote FROM muestras_anual anual INNER JOIN muestras_imagenes ON idMuestra = anual.id INNER JOIN lotes ON idLote = lotes.id WHERE anual.idFinca = $finca->idFinca AND anio = {$this->postdata->anio} AND semana = {$semana->semana} GROUP BY idLote");
                foreach($finca->lotes as $lote){
                    $lote->labores = $this->db->queryAll("SELECT idLabor, labores.nombre AS labor FROM muestras_anual anual INNER JOIN muestras_imagenes ON idMuestra = anual.id INNER JOIN labores ON idLabor = labores.id WHERE idFinca = {$finca->idFinca} AND idLote = {$lote->idLote} AND anio = {$this->postdata->anio} AND semana = {$semana->semana} GROUP BY idLabor");
                    foreach($lote->labores as $labor){
                        $labor->imagenes = $this->db->queryAll("SELECT imagen AS url, anual.fecha, anual.promedio FROM muestras_anual anual INNER JOIN muestras_imagenes ON idMuestra = anual.id INNER JOIN labores ON idLabor = labores.id WHERE idFinca = {$finca->idFinca} AND idLote = {$lote->idLote} AND idLabor = {$labor->idLabor} AND semana = {$semana->semana} AND anio = {$this->postdata->anio}");
                    }
                }
            }
        }
        return $response;
    }
}