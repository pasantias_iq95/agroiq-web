<?php defined('PHRAPI') or die("Direct access not allowed!");

class Merma {
	public $name;
	private $db;
	private $MermaTypePeso;
	private $config;
	private $search;
	private $cajas;
	private $usd;
	private $factor;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// F($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->search = "Total Merma";
        $this->cajas = "18.86";
        $this->usd = "6.16";
        $this->factor = 1;
        if($this->session->id_company == 2){
        	$this->search = "Total Daños";
        }
        if($this->session->id_company == 7){
        	$this->search = "Total Merma";
        	$this->cajas = "18.86";
        	$this->usd = "6.16";
        	$this->factor = 5;
        }

        $this->MermaTypePeso = "Kg";
    }
    
    public function last(){
        $response = new stdClass;
        $response->fecha = $this->db->queryOne("SELECT MAX(date_fecha) FROM merma WHERE YEAR(date_fecha) = YEAR(CURRENT_DATE)");
        return $response;
    }

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}

		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}

		if(isset($postdata->idFincaDia)){
			$id_fincaDia = $postdata->idFincaDia;
		}

		if(isset($postdata->statusLbKg) && $postdata->statusLbKg == 1){
			$this->MermaTypePeso = "Kg";
		}else{
			$this->MermaTypePeso = "Lb";
		}

		if(isset($postdata->year)){
			$year = $postdata->year;
		}else{
			$year = 2016;
		}

		$response->id_company = (int)$this->session->id_company;
		$response->tags = $this->tags($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		$response->tabla_lote_merma = $this->tabla_lote_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		// Cargamos la data una sola ves 
		$danos = $this->tabla_danos_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		// Cargamos la data del historico una sola ves 
		$historico = $this->grafica_historico($palanca , $id_finca , $year);
		// Cargamos la data de tendencia una sola ves 
		$tendencia = $this->tendencia($palanca , $id_finca , $postdata);
		// Cargamos la data Grafica Dia
		$dia = $this->grafica_dia($palanca , $id_finca , $id_fincaDia , $postdata);
		// Asignamos la data 'Tabla'
		$response->tabla_danos_merma = $danos->data;
		// Asignamos la data 'Tabla' Daños
		$response->tabla_danos_merma_danhos_merma = $danos->danhos_merma;
		// Asignamos la data 'Grafica de daños'
		$response->danos = $danos->grafica;
		// Asignamos la data 'Grafica del detalle de daños'
		$response->danos_detalle = $danos->grafica_detalle;
		// Asignamos la data 'Grafica del historico'
		$response->historico = $historico->data;
		// Asignamos la data 'Grafica del dia'
		$response->dia = $dia->data;
		// Agregamos el titulo de la 'Grafica del dia'
		$response->dia_title = $dia->title;
		// Agregamos el promedio del historico
		$response->historico_avg = $historico->avg;
		$response->historico_legends = $historico->legend;

		// Asignamos la data 'Grafica del historico' y Legend
		$response->tendencia = $tendencia->series;
		$response->tendencia_legends = $tendencia->legend;

		$response->palanca = ["" => "Todos"] + $this->getPalanca($postdata);
		$response->fincas = [];
		$response->factor = (double)$this->factor;
		if($this->session->id_company == 7 || $this->session->id_company == 6){
			$response->fincas = ["" => "Todas"] + $this->getFincas($postdata);
		}

		return $response;
	}

	// public function historico(){
	// 	$postdata = (object)json_decode(file_get_contents("php://input"));
	// 	$palanca = "";
	// 	if(isset($postdata->palanca)){
	// 		$palanca = $postdata->palanca;
	// 	}

	// 	if(isset($postdata->idFinca)){
	// 		$id_finca = $postdata->idFinca;
	// 	}

	// 	$historico = $this->grafica_historico($palanca , $id_finca , $year);

	// 	$response->historico = $historico->data;

	// 	return $response;
	// }

	private function getPalanca($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";
		if(isset($filters->idFinca) && $filters->idFinca != ""){
			// $sWhere .= " AND id_finca = {$filters->idFinca}";
			if($filters->idFinca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$filters->idFinca}";
			}
		}
		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT palanca AS id , palanca AS label  FROM merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND palanca != '' $sWhere {$typeMerma}
				GROUP BY TRIM(palanca)";
		// D($sql);
		$response->data = [];
		$response->data = $this->db->queryAllSpecial($sql);
		return $response->data;
	}	

	private function getFincas($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		
		$response = new stdClass;
		$sql = "SELECT id_finca AS id , finca AS label  FROM merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND finca != ''
				GROUP BY id_finca";
		$response->data = [];
		if($this->session->id_company == 7){
			$response->data = ["9999999" => "San José"] + $this->db->queryAllSpecial($sql);
		}else{
			$response->data = $this->db->queryAllSpecial($sql);
		}
		return $response->data;
	}

	private function tags($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$sql = "SELECT IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS merma  FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
		$rp1 = $this->db->queryRow($sql);

		$peso_neto = "peso_neto";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$this->cajas = "18.86";
			}else{
				$cantidad = "(cantidad)";
				$this->cajas = "41.50";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 7){
			$peso_neto = "(racimo - tallo)";
			$cantidad = "(cantidad)";
			$cajas = "(SUM(total_peso_merma-tallo) / $this->cajas) AS cajas";
		}elseif($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$peso_neto = "((racimo - tallo) / 2.2 )";
				$cantidad = "(cantidad / 2.2)";
			}else{
				$peso_neto = "(racimo - tallo)";
				$cantidad = "(cantidad)";
			}
		}else{
			$cantidad = "(cantidad)";
		}

		$total_merma = 0;
		$data = "";

		$TagsCaja = (double)$this->db->queryOne("SELECT {$cajas} FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' {$sWhere}");


        $sql = "SELECT AVG(porcentaje_merma) AS merma_neta ,AVG(porcentaje_merma_procesada) AS merma_procesada 
                FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
        $rp1 = $this->db->queryRow($sql);

        $sql2 = "SELECT AVG((SELECT (cantidad)
                FROM merma_detalle
                WHERE merma_detalle.id_merma = merma.id
                AND campo = '% MERMA CORTADA')) AS merma_cortada,
                AVG((SELECT (cantidad)
                FROM merma_detalle
                WHERE merma_detalle.id_merma = merma.id
                AND campo = '% TALLO')) AS tallo
                FROM merma
                WHERE date_fecha BETWEEN '$inicio' AND '$fin' $sWhere";
        $rp2 = $this->db->queryRow($sql2);

        /*$sql ="SELECT LOWER(type) AS id,ROUND(AVG(Total),2) AS label 
                FROM (
                    SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , {$peso_neto} , (({$cantidad}) / {$peso_neto}) * 100 as Total , porcentaje_merma  FROM merma
                    INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                    WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND campo like '%Total Peso%' {$sWhere}
                    AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  
                ) as detalle
                GROUP BY type";
        $data = $this->db->queryAllSpecial($sql);*/

        $sql = "SELECT LOWER(type) AS type, ROUND(merma, 2) AS peso, IFNULL(ROUND(merma / (SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND campo = 'Total peso Merma Neta') * 100, 2), 0) AS porc
                FROM (
                    SELECT type, SUM(cantidad) AS merma
                    FROM merma
                    INNER JOIN merma_detalle ON merma.id = id_merma
                    WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND campo LIKE 'Total Peso%' {$sWhere}
                        AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  
                    GROUP BY TYPE
                ) AS tbl";
        $tt = $this->db->queryAll($sql);

        $data = new stdClass;
        $peso = new stdClass;
        foreach($tt as $row){
            $data->{$row->type} = $row->porc;
            $peso->{$row->type} = ($row->peso > 0) ? $row->peso : '';
        }

        $tags = [];
        $tags = array(
            'merma' 		  => $rp1->merma_neta,
            'merma_procesada' => $rp1->merma_procesada,
            'merma_cortada'	  => $rp2->merma_cortada,
            'tallo'			  => $rp2->tallo,
            'animales'        => ($data->animales != null) ? $data->animales : 0,
            'hongos' 		  => ($data->hongos != null) ? $data->hongos : 0,
            'insectos'        => ($data->insectos != null) ? $data->insectos : 0,
            'empacadora'      => ($data->empacadora != null) ? $data->empacadora : 0,
            'cosecha' 		  => ($data->cosecha) ? $data->cosecha : 0,
            'lotero' 		  => ($data->{'lotero aereo'} != null) ? $data->{'lotero aereo'} : 0,
            'terrestre'       => ($data->{'lotero terrestre'} != null) ? $data->{'lotero terrestre'} : 0,
            'fisiologicos' 	  => ($data->fisiologicos) ? $data->fisiologicos : 0,
            'cajas' 	  	  => $TagsCaja,
            'usd' 	  	  	  => ($TagsCaja * $this->usd) ,
            'pesos'           => $peso
        );
    

		return $tags;
	}

	private function tabla_lote_merma($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$data = [];
		$peso_total = "SUM(total_peso_merma) AS peso_total";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "AVG(total_peso_merma / 2.2)";
				$this->cajas = "18.86";
			}else{
				$total_peso_merma = "AVG(total_peso_merma)";
				$this->cajas = "41.50";
			}

			$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
			$racimos_lote = "racimos_procesados";
			$sql = "SELECT id , bloque AS lote ,{$total_peso_merma} AS total_peso_merma , AVG(total_defectos) AS total_defectos , SUM(total_defectos) AS total_defectos_sum , IF(1=1/*date_fecha > DATE('2016-09-13')*/, AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS merma  ,SUM($racimos_lote) AS racimos_lote , AVG($racimos_lote) AS racimos_lote_avg , {$peso_total} , {$cajas}
                    FROM merma main
                    WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere 
                    GROUP BY bloque";
			#D($sql);
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
			$total_peso_merma = "total_peso_merma";
			$racimos_lote = "id";

			if($this->session->id_company == 7){
				$peso_total = "SUM((total_peso_merma-tallo)) AS peso_total";
				$total_peso_merma = "(total_peso_merma-tallo) / racimos_procesados";
				$racimos_lote = "racimos_procesados";
				$cajas = "(SUM(total_peso_merma - tallo)  / $this->cajas) AS cajas";
			}

			if($this->session->id_company == 4 || $this->session->id_company == 9 || $this->session->id_company == 6){
				$racimos_lote = "racimos_procesados";

				if($this->MermaTypePeso == "Kg"){
					$peso_total = "SUM((peso_neto / 2.2 )) AS peso_total";
					$total_peso_merma = "(peso_neto / 2.2 ) / racimos_procesados";
					$peso_neto = "((racimo - tallo) / 2.2 )";
				}else{
					$total_peso_merma = "peso_neto / racimos_procesados";
					$peso_neto = "(peso_neto)";
					$peso_total = "SUM(peso_neto) AS peso_total";
				}
			}

			if($this->session->id_company == 3){
				if($this->MermaTypePeso == "Kg"){
					$total_peso_merma = "((total_peso_merma-tallo) / 2.2 ) / racimos_procesados";
					$peso_neto = "((racimo - tallo) / 2.2 )";
				}else{
					$total_peso_merma = "(total_peso_merma-tallo) / racimos_procesados";
					$peso_neto = "(racimo - tallo)";
					// $cantidad = "(cantidad)";
                }
                
                
			}

			$typeMerma = "";
			if($this->session->id_company == 6){
				$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
			}

			$sql = "SELECT id , 
						bloque AS lote ,
						(SELECT AVG({$total_peso_merma}) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS total_peso_merma , 
						(SELECT AVG(total_defectos) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS total_defectos , 
						(SELECT SUM(total_defectos) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS total_defectos_sum ,
						(SELECT AVG(porcentaje_merma) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS merma,
						(SELECT {$cajas} FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS cajas,  
						(SELECT SUM({$racimos_lote}) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS racimos_lote , 
						(SELECT AVG({$racimos_lote}) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS racimos_lote_avg , 
						(SELECT {$peso_total} FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS peso_total
			FROM(
			SELECT id, bloque
				FROM merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma} 
				GROUP BY bloque) AS bloques";
		}
        $res = $this->db->queryAll($sql);
		$danhos = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->usd = (double)($value->cajas * $this->usd);
			if(is_numeric($value->lote)){
				$value->lote = (double)$value->lote;
			}else{
				$value->lote = $value->lote;
			}
			$danhos = $this->getDanhos($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
			$value->expanded = false;
			$value->detalle = $this->tabla_lote_merma_detalle($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
			$value->detalle_danhos = $danhos->danhos;
            $value->detalle_danhos_sum = $danhos->danhos_suma;
            
            if($this->session->id_company == 2){
                $value->total_peso_merma = 0;
                foreach($value->detalle as $row){
                    $value->total_peso_merma += $row->danhos_peso;
                    $value->dedos_prom_marcel += $row->dedos_prom_marcel;
                }
            }
		}
		//D($res);
		return $res;
	}

	private function getDanhos($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		if($this->session->id_company == 7){
			$typeMerma = " AND campo NOT LIKE '%Total Peso%'";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label , SUM(cantidad) AS label2 FROM (
				SELECT merma_detalle.id,merma.id AS id_m , fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE bloque = '{$bloque}' AND date_fecha BETWEEN '$inicio' AND '$fin' AND campo like '%".$this->search."%'  {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
				GROUP BY type";
		// D($sql);
		$res = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->danhos = [];
		$response->danhos_suma = [];
		foreach ($res as $key => $value) {
			$response->danhos[$value->id] = (double)$value->label;
			$response->danhos_suma[$value->id] = (double)$value->label2;
		}

		return $response;
	}

	private function getDanhosPesos(){
		if($this->session->id_company == 7){
			$sql = "";
		}
		return array();
	}

	private function tabla_lote_merma_detalle($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "", $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$data = [];
		$peso_neto = "peso_neto";
		$racimos_procesados = "racimos_procesados";
		$cantidad_racimos = "(cantidad)";
		$racimos_lote = "cantidad";
		$peso_total = "(cantidad)";
		$cajas = "((cantidad) / $this->cajas)";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$cantidad_racimos = "(cantidad / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$cantidad_racimos = "(cantidad)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$racimos_lote = "racimos_procesados";

		}elseif($this->session->id_company == 7){
			$cajas = "((cantidad) / $this->cajas)";
			$cantidad = "(cantidad)";
			$cantidad_racimos = "(cantidad/racimos_procesados)";
			$racimos_procesados = "racimos_procesados";
			$racimos_lote = "racimos_procesados";
			$peso_total = "(cantidad)";
		}elseif($this->session->id_company == 4  || $this->session->id_company == 9){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$peso_total = "(cantidad / 2.2)";
				$cantidad_racimos = "($cantidad / racimos_procesados)";
				$peso_neto = "((racimo - tallo) / 2.2)";
			}else{
				$peso_neto = "(racimo - tallo)";
				$cantidad = "(cantidad)";
				$cantidad_racimos = "(cantidad / racimos_procesados)";
			}
			$racimos_procesados = "racimos_procesados";
		}elseif($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$cantidad_racimos = "($cantidad / racimos_procesados)";
				$peso_neto = "((racimo - tallo) / 2.2)";
			}else{
				$peso_neto = "(racimo - tallo)";
				$cantidad = "(cantidad)";
				$cantidad_racimos = "(cantidad / racimos_procesados)";
			}
			$racimos_procesados = "racimos_procesados";
		}else{
				$cantidad = "(cantidad)";
				$cantidad_racimos = "(cantidad)";
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$peso_total = "(cantidad / 2.2)";
				$cantidad_racimos = "($cantidad / racimos_procesados)";
				$peso_neto = "((racimo - tallo) / 2.2)";
			}else{
				$peso_neto = "(racimo - tallo)";
				$cantidad = "(cantidad)";
				$cantidad_racimos = "(cantidad / racimos_procesados)";
			}
		}

		/*$sql = "SELECT type , bloque , AVG((cantidad)) AS danhos_peso, AVG(Total) AS cantidad , AVG(porcentaje_merma) , SUM(racimos_lote) AS racimos_lote , 
				AVG(racimos_lote) AS racimos_lote_avg , SUM(total_peso) AS peso_total , SUM(cajas) AS cajas
				FROM (
				SELECT fecha  , bloque, merma_detalle.id_merma , {$racimos_lote} , $cajas , 
				{$racimos_procesados} , type ,campo ,{$cantidad_racimos} AS cantidad , {$peso_neto} , ({$cantidad} / {$peso_neto}) * 100 as Total , porcentaje_merma ,
				{$peso_total}
				FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE bloque = '{$bloque}' AND date_fecha BETWEEN '$inicio' AND '$fin' AND campo like '%Total Peso%' {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
				GROUP BY type";*/

		$sql = "SELECT type , bloque , 
					SUM(cantidad) AS danhos_peso, 
					SUM(total) AS cantidad , 
					AVG(porcentaje_merma) , 
					racimos_lote , 
					AVG(racimos_lote) AS racimos_lote_avg , 
					SUM(total_peso) AS peso_total , 
					SUM(cajas) AS cajas
				FROM (
					SELECT 
						fecha, 
						bloque, 
						merma_detalle.id_merma , 
						SUM({$racimos_lote}) AS racimos_lote, 
						SUM($cajas) AS cajas, 
						{$racimos_procesados} , 
						type ,
						campo , 
						SUM({$cantidad_racimos}) AS cantidad , 
						{$peso_neto} , 
						SUM(({$cantidad} / {$peso_neto}) * 100) as total , 
						porcentaje_merma ,
						SUM({$peso_total}) AS total_peso
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE bloque = '{$bloque}' AND date_fecha BETWEEN '$inicio' AND '$fin' AND "
						.(($this->session->id_company == 3) ? "flag  = IF(TYPE = 'LOTERO AEREO', 3, 1)" : "flag = 1")." {$typeMerma}
					AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere 
					GROUP BY type, campo
					HAVING cantidad > 0) as detalle
				GROUP BY type";

		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			#calcular porcentaje de merma marun y marlon
			if($this->session->id_company == 7 || $this->session->id_company == 4 || $this->session->id_company == 6 || $this->session->id_company == 2){
				$peso_neto = ($this->session->id_company == 7 || $this->session->id_company == 2) ? "peso_neto" : "racimo";

				$sql = "SELECT ROUND(AVG((porcentaje_peso_merma/{$peso_neto}*100)),2) AS porcentaje_merma
						FROM (
							SELECT bloque, merma.id, TYPE, porcentaje_merma, {$peso_neto}, SUM(cantidad) AS porcentaje_peso_merma
							FROM merma
							INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
							WHERE bloque = '{$bloque}' AND date_fecha BETWEEN '$inicio' AND '$fin' AND flag = 1 AND type = '{$value->type}' {$sWhere}
							AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') 
							GROUP BY merma.id, TYPE
						) AS tbl
						GROUP BY TYPE";
				#D($sql);
				$value->cantidad = $this->db->queryRow($sql)->porcentaje_merma;
			}

			$value->expanded = false;
			$value->usd = (double)($value->cajas * $this->usd);
            $value->details = $this->tabla_lote_merma_detalle_details($inicio, $fin , $value->type, $palanca, $id_finca , $filters , $bloque, $value->danhos_peso, $value->racimos_lote, $value->cantidad, $cajas);
            foreach($value->details as $row){
                $value->dedos_prom_marcel += $row->dedos_prom_marcel;
            }
		}
		return $res;
	}

	private function tabla_lote_merma_detalle_details($inicio, $fin , $type , $palanca = "", $id_finca = "" , $filters = [] , $bloque, $_danhos_peso, 
		$racimos_procesados, $_cantidad){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$cajas = "(SUM(cantidad) / $this->cajas) AS cajas";
		$peso_total = "SUM(cantidad) AS total_peso";

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$racimo = "racimo";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "racimo";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 4){
			$cantidad = "(cantidad/racimos_procesados)";
			$racimo = "(racimo)";

			if($this->MermaTypePeso == "Kg"){
				$peso_total = "SUM(cantidad / 2.2) AS total_peso";
			}
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad/racimos_procesados)";
			$racimo = "(racimo)";
		}elseif($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$racimo = "(racimo)";
				$cantidad = "(cantidad)";
			}
		}else{
				$racimo = "racimo";
				$cantidad = "(cantidad)";
		}


		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
			$cantidad = "(cantidad/racimos_procesados)";
			$racimo = "(racimo)";

			if($this->MermaTypePeso == "Kg"){
				$peso_total = "SUM(cantidad / 2.2) AS total_peso";
			}
		}

		$condition = "AND flag = 1";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}
		}

		$response = new stdClass;
		$subQuery = "(SELECT SUM(racimo) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin')";
		/*$sql = "SELECT campo AS type, campo,
				(AVG(cantidad) / {$racimos_procesados}) AS danhos_peso, 
				(AVG(cantidad)/{$_danhos_peso}*".round($_cantidad,2).")/{$racimos_procesados} AS cantidad,
				SUM(cajas) AS cajas,
				(SUM(cajas)*{$this->usd}) AS usd,
				total_peso
				FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,REPLACE(campo,'Peso ' , '') AS campo , $peso_total, $cajas, SUM{$cantidad} AS cantidad , {$racimo} AS racimo FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' AND bloque = '{$bloque}' {$condition} {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere
				GROUP BY campo
				HAVING cantidad > 0) as detalle
				GROUP BY type ,campo";*/
		$sql = "SELECT campo AS type, campo,
				AVG(cantidad) AS danhos_peso, 
				AVG(total) AS cantidad,
				SUM(cajas) AS cajas,
				(SUM(cajas)*{$this->usd}) AS usd,
				total_peso
				FROM (
					SELECT fecha  , bloque,merma_detalle.id_merma , type ,REPLACE(campo,'Peso ' , '') AS campo , $peso_total, $cajas, SUM({$cantidad}) AS cantidad , {$racimo}  AS racimo, SUM(((cantidad) / peso_neto) * 100) AS total
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' AND bloque = '{$bloque}' {$condition} {$typeMerma}
					AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere
					GROUP BY campo
					HAVING cantidad > 0) as detalle
				GROUP BY campo";
		#D($sql);
		$response->data = $this->db->queryAll($sql);

		#calcular porcentaje de merma marun y marlon
		if($this->session->id_company == 7 || $this->session->id_company == 4 || $this->session->id_company == 6 || $this->session->id_company == 2){
			$peso_neto = ($this->session->id_company == 7) ? "peso_neto" : "racimo";

			foreach($response->data as $value){
				$sql = "SELECT ROUND(AVG((porcentaje_peso_merma/{$peso_neto}*100)),2) AS porcentaje_merma
						FROM (
							SELECT bloque, merma.id, TYPE, porcentaje_merma, {$peso_neto}, SUM(cantidad) AS porcentaje_peso_merma, campo
							FROM merma
							INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
							WHERE bloque = '{$bloque}' AND date_fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' {$condition} {$typeMerma}
							AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') AND REPLACE(campo,'Peso ' , '') = '{$value->campo}' $sWhere
							GROUP BY merma.id, TYPE, campo
						) AS tbl
						GROUP BY TYPE, campo";
				#D($sql);
                $value->cantidad = $this->db->queryRow($sql)->porcentaje_merma;
                if($this->session->id_company == 2){
                    $value->dedos_prom_marcel = $this->db->queryOne("SELECT SUM(cantidad) / SUM((SELECT COUNT(1) FROM merma_detalle WHERE id_merma = m.id_merma AND cantidad > 0 AND flag = 3)) AS dedos_prom
                        FROM merma
                        INNER JOIN merma_detalle m ON merma.id = id_merma
                        WHERE fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' AND campo = '{$value->campo}' AND flag = 2 AND cantidad > 0");
                }
            }
		}
		// $response->danhos_merma = $this->getDanhosMermaDetalle($inicio, $fin , $type , $palanca , $id_finca , $filters);
		return $response->data;
	}

	private function grafica_historico($palanca = "", $id_finca = "" , $year = "YEAR(CURRENT_DATE)" ){
		$sWhere = "";
		$sWhere_PromFincas = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$sWhere .= " AND YEAR(fecha) = {$year}";
		$Umbral = 2;

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}

		$sql = "SELECT TRIM(finca) AS finca ,id_finca, WEEK(fecha) AS fecha, IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere
				GROUP BY id_finca , WEEK(fecha)
				ORDER BY id_finca , WEEK(fecha)";


		// $typeMerma = "";
		// if($this->session->id_company == 6){
		// 	$Umbral = 10;
		// 	$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";

		// 	$sql = "SELECT TRIM(tipo_merma) AS finca ,tipo_merma AS id_finca, WEEK(fecha) AS fecha, IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
		// 		FROM merma
		// 		WHERE porcentaje_merma > 0 $sWhere
		// 		GROUP BY tipo_merma , WEEK(fecha)
		// 		ORDER BY tipo_merma , WEEK(fecha)";
		// }

		if($this->session->id_company == 7 || $this->session->id_company == 6){
			$Umbral = 10;
			if((int)$id_finca > 1 && $palanca != ""){
				$sWhere_PromFincas = " AND palanca = '{$palanca}'";
			}elseif((int)$id_finca == 1){
				$sWhere_PromFincas = $sWhere;
			}
			$sWhere_PromFincas .= " AND YEAR(fecha) = {$year}";
			// D($sWhere_PromFincas);
			$sql = "(SELECT TRIM(finca) AS finca ,id_finca, WEEK(fecha) AS fecha, IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere
				GROUP BY id_finca , WEEK(fecha)
				ORDER BY id_finca , WEEK(fecha))
				UNION ALL
				(SELECT 'San José' AS finca, '9999999' AS id_finca, fecha , AVG(porcentaje_merma) ,total_peso_merma FROM (
				SELECT TRIM(finca) AS finca ,id_finca, WEEK(fecha) AS fecha, IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
								FROM merma
								WHERE porcentaje_merma > 0 AND finca LIKE '%San%' $sWhere_PromFincas
								GROUP BY id_finca , WEEK(fecha)
								ORDER BY id_finca , WEEK(fecha)) as detalle
				GROUP BY (fecha)
				ORDER BY (fecha))";
			// D($sql);
		}

		if($this->session->id_company == 4 || $this->session->id_company == 9){
			$Umbral = 5;
		}

		$response = new stdClass;
		// D($sql);
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->porcentaje_merma = (float)$value->porcentaje_merma;
			$sum += $value->porcentaje_merma;
			$response->legend[(int)$value->fecha] = (int)$value->fecha;
			if($finca != $value->id_finca){
				$series = new stdClass;
				$finca = $value->id_finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->data = [];

				if($this->session->id_company == 7)
					$series->data[$value->fecha] = round($value->porcentaje_merma ,2);
				else
					$series->data[] = round($value->porcentaje_merma ,2);

				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series->markLine = new stdClass;
					$series->markLine->data = [];
					$markLine->name = "Umbral";
					$markLine->value = $Umbral;
					$markLine->xAxis = -1;
					$markLine->yAxis = $Umbral;
					$series->markLine->data[0][] = $markLine;
					
					$markLine = new stdClass;
					$markLine->xAxis = 52;
					$markLine->yAxis = $Umbral;

					$series->markLine->data[0][] = $markLine;
					$flag_count++;
				}
				
				$response->data[$value->id_finca] = $series;
			}else{
				if($finca == $value->id_finca){
					if($this->session->id_company == 7)
						$response->data[$value->id_finca]->data[$value->fecha] = round($value->porcentaje_merma ,2);
					else
						$response->data[$value->id_finca]->data[] = round($value->porcentaje_merma ,2);
				}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}

	private function historico_general($palanca = "" , $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$Umbral = 10;
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT TRIM(finca) AS finca ,id_finca, WEEK(fecha) AS fecha, IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere {$typeMerma}
				GROUP BY WEEK(fecha)";
		$res = $this->db->queryAll($sql);
	}

	private function grafica_dia($palanca = "", $id_finca = ""  , $id_fincaDia = "" , $filters = []){
		$sWhere = "";
		$fincaDia = 0;
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		if($id_fincaDia != ""){
			$fincaDia = $id_fincaDia;
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}


		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT id , id_finca,TIME_FORMAT(TIME(fecha) , '%H:%i') AS time ,date_fecha as fecha ,IF(date_fecha > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma,
				CONCAT_WS(' ','Última Actualización : ' , date_fecha) as title FROM merma
				WHERE 1 = 1 $sWhere {$typeMerma}
				GROUP BY id
				ORDER BY fecha desc";
		// D($sql);
		$res = $this->db->queryAll($sql);
		$count = 1;
		$fecha = "";
		$flag = 0;
		$response->data = new stdClass;
		$response->data->data = [];
		$response->data->label = [];
		$response->data->series = [];
		$response->title = "";
		$label = "";
		foreach ($res as $key => $value) {
			$value = (object)$value;
			if($value->fecha != $fecha){
				$fecha = $value->fecha;
				$flag++;
				if($flag == 2){
					break;
				}
			}

			if($fincaDia > 0){
				if($value->id_finca == $fincaDia){
					$response->title = $value->title;
					$value->porcentaje_merma = (float)$value->porcentaje_merma;
					if($value->time == '00:00'){
						$label = "Hora ".$count;
					}else{
						$label = $value->time;
					}
					$response->data->label[$value->fecha." ".$label] = $label;
					$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
					$response->data->data[$label] = $value;
					$count++;
				}
			}else{
				$response->title = $value->title;
				$value->porcentaje_merma = (float)$value->porcentaje_merma;
				if($value->time == '00:00'){
					$label = "Hora ".$count;
				}else{
					$label = $value->time;
				}
				$response->data->label[$value->fecha." ".$label] = $label;
				$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
				$response->data->data[$label] = $value;
				$count++;
			}
		}
		return $response; 
	}

	private function tabla_danos_merma($inicio, $fin , $palanca , $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$response = new stdClass;
		$racimo = "racimo";
		$peso_total = "SUM(total_peso_merma) AS peso_total";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		$racimos_lote = "racimos_procesados";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
				$this->cajas = "18.86";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "(racimo)";
				$this->cajas = "41.5";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad)";
			$racimo = "(racimo)";
			$peso_total = "SUM(total_peso_merma - tallo) AS peso_total";
			$cajas = "(SUM(total_peso_merma - tallo) / $this->cajas) AS cajas";
			$racimos_lote = "racimos_procesados";
		}elseif($this->session->id_company == 4){
			$peso_total = "SUM(peso_neto) AS peso_total";

			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "(racimo)";
			}
		}elseif($this->session->id_company == 3){
			$peso_total = "SUM(peso_neto) AS peso_total";

			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$racimo = "(racimo)";
				$cantidad = "(cantidad)";
			}
		}else{
				$cantidad = "(cantidad)";
				$racimo = "(racimo)";
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";

			$peso_total = "SUM(peso_neto) AS peso_total";

			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "(racimo)";
			}
		}

		$subQuery = "(SELECT SUM(racimo) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin')";
		$sql ="SELECT type ,AVG(cantidad) AS danhos_peso ,AVG(total_defectos) AS total_defectos,  SUM(total_defectos) AS total_defectos_sum , $peso_total,
			        SUM($racimos_lote) AS racimos_lote , AVG($racimos_lote) AS racimos_lote_avg,
                    (SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON merma.id = id_merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND campo LIKE 'Total Peso%' AND type = detalle.type) AS cantidad,
                    {$cajas}
			        FROM (
			SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , peso_neto, total_defectos , total_peso_merma , tallo , {$racimos_lote}
			FROM merma
			INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
			WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND campo like '%Total Peso%' {$typeMerma}
			AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere 
			HAVING cantidad > 0) as detalle
			GROUP BY type";

		$response->data = $this->db->queryAll($sql);
		$response->grafica = $this->pie($response->data , '50%' ,"Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		
		$response->grafica_detalle = [];
		$detalle = "";
		foreach ($response->data as $key => $value) {
			$value = (object)$value;
			$value->usd = (double)($value->cajas * $this->usd);
			$detalle = $this->tabla_danos_merma_detalle($inicio, $fin , $value->type, $palanca, $id_finca , $filters);
			$value->detalle = $detalle->data;
			$value->detalle_danhos = $detalle->danhos_merma;
			$value->detalle_danhos_sum = $detalle->danhos_merma_sum;
			$response->grafica_detalle[$value->type] = $detalle->grafica;
		}
		$response->danhos_merma = $this->getDanhosMerma($inicio, $fin , $palanca , $id_finca , $filters);
		return $response;
	}

	private function getDanhosMerma($inicio ,$fin  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$sWhereConsulta = "AND campo like '%".$this->search."%'";
		if($this->session->id_company == 4 || $this->session->id_company == 9){
			$sWhereConsulta = "AND flag = 1";
		}

		if($this->session->id_company == 7){
			$sWhereConsulta = "AND campo like '%".$this->search."%' AND campo NOT LIKE '%Total Peso%'";
		}

		if($this->session->id_company == 3){
			$sWhereConsulta = "AND campo like '%Total Peso%'";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label FROM (
				SELECT merma_detalle.id,merma.id AS id_m , fecha  ,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' {$sWhereConsulta} {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
				GROUP BY type";
		//D($sql);
		$res = $this->db->queryAllSpecial($sql);

		return $res;
	}

	private function tabla_danos_merma_detalle($inicio, $fin , $type , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$peso_total = "SUM(total_peso_merma) AS peso_total";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		$usdCampo =  "((SUM(total_peso_merma) / $this->cajas) * $this->usd) AS usd";
		$racimos_lote = "racimos_procesados";
		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$racimo = "racimo";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
				$this->cajas = "18.86";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "racimo";
				$this->cajas = "41.50";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad)";
			$racimo = "(racimo)";
			$peso_total = "SUM(total_peso_merma - tallo) AS peso_total";
			$cajas = "(SUM(total_peso_merma - tallo) / $this->cajas) AS cajas";
			$usdCampo =  "((SUM(total_peso_merma - tallo) / $this->cajas) * $this->usd) AS usd";
			$racimos_lote = "racimos_procesados";
		}elseif($this->session->id_company == 4){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$racimo = "(racimo)";
				$cantidad = "(cantidad)";
			}
		}elseif($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$racimo = "(racimo)";
				$cantidad = "(cantidad)";
			}
		}else{
				$racimo = "racimo";
				$cantidad = "(cantidad)";
		}


		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$condition = "AND flag = 1";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}
		}

		$response = new stdClass;
		$subQuery = "(SELECT SUM(racimo) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin')";
		$sql = "SELECT campo AS type, campo , AVG(cantidad) AS danhos_peso, (SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON merma.id = id_merma WHERE campo = detalle.campo AND date_fecha BETWEEN '$inicio' AND '$fin') AS cantidad  , $cajas , 
				$peso_total , SUM($racimos_lote) AS racimos_lote , AVG($racimos_lote) AS racimos_lote_avg , $usdCampo
				FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,REPLACE(campo,'Peso ' , '') AS campo , {$cantidad} AS cantidad , {$racimo} AS racimo ,
				total_peso_merma , tallo , $racimos_lote
				FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' {$condition} {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere
				HAVING cantidad > 0) as detalle
				GROUP BY type ,campo";
		// D($sql);
		$response->data = $this->db->queryAll($sql);
		// foreach ($response->data as $key => $value) {
		// 	$value = (object)$value;
		// }
		$danhos = $this->getDanhosMermaDetalle($inicio, $fin , $type , $palanca , $id_finca , $filters);
		$response->danhos_merma = $danhos->danhos;
		$response->danhos_merma_sum = $danhos->danhos_suma;
		$response->grafica = $this->pie($response->data , '50%' ,"Detalle de Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		return $response;
	}

	private function getDanhosMermaDetalle($inicio ,$fin , $type  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$condition = "AND flag = 2";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}elseif($type == "COSECHA"){
				$condition = "AND (flag = 1 OR flag = 2)";
			}
		}

		$sql = "SELECT campo AS id ,AVG(cantidad) AS label , SUM(cantidad) AS label2 FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND type = '{$type}' {$condition} {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere) as detalle
				GROUP BY type ,campo";
		#D($sql);
		$res = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->danhos = [];
		$response->danhos_suma = [];
		foreach ($res as $key => $value) {
			$response->danhos[$value->id] = (double)$value->label;
			$response->danhos_suma[$value->id] = (double)$value->label2;
		}

		return $response;
	}

	private function tendencia($palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		if(isset($filters->yearTendencia) && $filters->yearTendencia != ""){
			$sWhere .= " AND YEAR (fecha) = '{$filters->yearTendencia}' ";
		}else{
			$sWhere .= " AND YEAR(fecha) = 2017 ";
		}

		$Umbral = 50;
        $semanas = $this->db->queryAllSpecial("SELECT semana as id, semana as label FROM merma WHERE 1=1 $sWhere GROUP BY semana");
        $total_semanas = $this->db->queryAllSpecial("SELECT semana AS id, SUM(cantidad) AS label
                                                    FROM merma
                                                    INNER JOIN merma_detalle ON id_merma = merma.`id`
                                                    WHERE TYPE = 'RESULTADOS' AND campo = 'Total peso Merma Neta' $sWhere
                                                    GROUP BY semana");
        $data = $this->db->queryAll("SELECT TYPE AS labor, semana, SUM(cantidad) AS cantidad
                                                    FROM merma
                                                    INNER JOIN merma_detalle ON id_merma = merma.`id`
                                                    WHERE TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
                                                        AND flag = 1 $sWhere
                                                    GROUP BY TYPE, semana");
        $types = [];
        
		$response = new stdClass;
		$series = new stdClass;
		$flag_count = 0;
		$labor = "";

		$response->series = [];
		$cantidad = 0;
		foreach ($data as $key => $value) {
            if(!in_array($value->semana, $response->legend))
                $response->legend[] = (int) $value->semana;
                
			$cantidad = round(($value->cantidad / $total_semanas[$value->semana]) *100 , 2);

			if(!in_array($value->labor, $types)){
                $types[] = $value->labor;

				$series = new stdClass;
				$series->name = $value->labor;
				$series->type = "line";
				$series->data = [];
				$series->data[] = $cantidad;
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series->markLine = new stdClass;
					$series->markLine->data = [];
					$markLine->name = "Umbral";
					$markLine->value = $Umbral;
					$markLine->xAxis = -1;
					$markLine->yAxis = $Umbral;
					$series->markLine->data[0][] = $markLine;
					
					$markLine = new stdClass;
					$markLine->xAxis = 52;
					$markLine->yAxis = $Umbral;

					$series->markLine->data[0][] = $markLine;
					$flag_count++;
				}	

				$response->series[$value->labor] = $series;
			}else{
				$response->series[$value->labor]->data[] = $cantidad;
			}
		}
		return $response;
	}

	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "DAÑOS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['60%', '60%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = "Merma";
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["orient"] = "vertical";
			$response->pie["legend"]["x"] = "left";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];

			foreach ($data as $key => $value) {
				$value = (object)$value;
				$response->pie["legend"]["data"][] = $value->type;
				if($version === 3){
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)$value->cantidad , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							]
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2)];
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2),
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}