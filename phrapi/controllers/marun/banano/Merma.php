<?php defined('PHRAPI') or die("Direct access not allowed!");

class Merma {
	public $name;
	private $db;
	private $MermaTypePeso;
	private $config;
	private $search;
	private $cajas;
	private $usd;
	private $factor;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->search = "Total Merma";
        $this->cajas = "18.86";
        $this->usd = "6.16";
        $this->factor = 1;
        if($this->session->id_company == 2){
        	$this->search = "Total Daños";
        }
        if($this->session->id_company == 7){
        	$this->search = "Total Merma";
        	$this->cajas = "18.86";
        	$this->usd = "6.16";
        	$this->factor = 5;
        }

        $this->MermaTypePeso = "Kg";
    }
    
    public function weeksHistories(){
        $response = new stdClass;
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"type" => ["ENFUNDE" , "CAMPO" , "COSECHA" , "ANIMALES" , "HONGOS" , "EMPACADORA" , "FISIOLOGICOS"],
			"mode" => getValueFrom($postdata , "mode" , "Peso" , FILTER_SANITIZE_STRING),
			"idFinca" => getValueFrom($postdata , "idFinca" , 1 , FILTER_SANITIZE_PHRAPI_INT),
			"year" => getValueFrom($postdata , "year" , 2017 , FILTER_SANITIZE_PHRAPI_INT),
			"palanca" => getValueFrom($postdata , "palanca" , "" , FILTER_SANITIZE_STRING),
		];

		if($this->MermaTypePeso == "Kg"){
			$cantidad = "(cantidad / 2.2)";
		}else{
			$cantidad = "(cantidad)";
		}

		$peso_neto = "peso_neto";
        if($filters->mode == "Total Peso"){

			$modeCampo = "SUM(cantidad)";

		}elseif($filters->mode == "Prom peso-viaje"){

			$modeCampo = "SUM(cantidad) AS cantidad";

		}elseif($filters->mode == "Prom peso-racimo"){

			$modeCampo = "SUM(cantidad / racimos_procesados) AS cantidad";

		}elseif($filters->mode == "Merma"){

			$modeCampo = "ROUND(SUM(Total),2)";

		}elseif($filters->mode == "Total Danos"){

			$filters->type = ["ENFUNDE" , "COSECHA"];
			$modeCampo = "SUM(cantidad) AS cantidad";
			$this->string = "Total Daños";

		}elseif($filters->mode == "Prom dano-viaje"){

			$filters->type = ["ENFUNDE" , "COSECHA"];
			$modeCampo = "SUM(cantidad) AS cantidad";
			$this->string = "Total Daños";

		}elseif($filters->mode == "Prom dano-racimo"){

			$filters->type = ["ENFUNDE" , "COSECHA"];
			$modeCampo = "SUM(cantidad / racimos_procesados) AS cantidad";
			$this->string = "Total Daños";
		}

		$sql_config = "SELECT umbral , bono , descuento FROM bonificacion_conts WHERE tipo = '{$filters->type}' AND mode = '{$filters->mode}'";
		$_config = $this->db->queryRow($sql_config);
		$umbral = (double)$_config->umbral;
		$valor_descuento = (double)$_config->descuento;
		$bono = (double)$_config->bono;

        $sql = "SELECT semanas.semana AS label , bloque AS legend ,
					IF(bloque != 127 ,true,false) AS selected,
					0 AS cantidad 
					FROM (SELECT getWeek(fecha) AS semana FROM merma WHERE getWeek(fecha) > 0 AND YEAR(fecha) = {$filters->year} GROUP BY getWeek(fecha) ORDER BY getWeek(fecha)) AS semanas , merma AS m
					WHERE YEAR(fecha) = YEAR(CURRENT_DATE) {$sFinca}
					AND bloque != 127 AND bloque != 0
					GROUP BY bloque , semanas.semana";
        $variable = $this->db->queryAll($sql);

        // 24/06/2017 - TAG: NUEVA FORMULA
        $response->historico_tabla = [];
        $valor = 0;
        $sumCategories = [];
        $countCategories = [];
        // 24/06/2017 - TAG: NUEVA FORMULA
		$weeks = (array)$this->db->queryAllSpecial("SELECT getWeek(fecha) AS id , getWeek(fecha) AS label FROM merma WHERE YEAR(fecha) = {$filters->year} GROUP BY getWeek(fecha)");
		foreach ($filters->type as $llave => $category) {
	        foreach ($variable as $key => $value) {
				$response->historico_tabla[$category]["expanded"] = false;
				$response->historico_tabla[$category]["category"] = $category;
				$sql ="SELECT $modeCampo FROM (
							SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , ({$cantidad}) AS cantidad , {$peso_neto} , (({$cantidad}) / {$peso_neto}) * 100 as Total , porcentaje_merma ,
							racimos_procesados
							FROM merma
							INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
							WHERE  getWeek(fecha) = {$value->label} AND YEAR(fecha) = {$filters->year} AND type = '{$category}'  AND campo like '%$this->string%' AND bloque = '{$value->legend}'  {$sFinca}
							AND bloque != 127 
							AND bloque != 0 
							AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')  ) as detalle";
				// D($sql);
				$valor = (double)$this->db->queryOne($sql);

				// D($valor);
				$response->historico_tabla[$category]["lotes"][$value->legend]["lote"] = $value->legend;
				$response->historico_tabla[$category]["lotes"][$value->legend]["expanded"] = false;
				$response->historico_tabla[$category]["lotes"][$value->legend]["cantidad"][$value->label] = $valor;
				if($this->session->id_company == 2){
					if($filters->type == "COSECHA"){
						$response->historico_tabla[$category]["lotes"][$value->legend]["umbral"][$value->label] = (double)$umbral;
					}else{
						$response->historico_tabla[$category]["lotes"][$value->legend]["umbral"][$value->label] = $this->db->queryRow("SELECT IFNULL(umbral, 0) as umbral FROM bonificacion_config WHERE lote = '{$value->legend}' AND semana <= '{$value->label}' AND umbral > 0 ORDER BY semana DESC LIMIT 1")->umbral;
					}
				}
				// if($this->session->id_company == 7){
				//     $response->historico_tabla[$value->legend][$valor]["umbral"][$value->label] = (double)$umbral;
				// }
				if($valor > 0){
					$count[$value->legend]++;
					$sum[$value->legend] += $valor;

					// 26/06/2017 - TAG: HISTORICO SEMANAL
					$countCategories[$category][$value->label]++;
					$sumCategories[$category][$value->label] += $valor;
					// 26/06/2017 - TAG: HISTORICO SEMANAL

					$response->historico_tabla[$category]["lotes"][$value->legend]["total"] = ROUND(($sum[$value->legend] / $count[$value->legend]) , 2);
					if($this->session->id_company == 2){
						if($filters->type == "COSECHA"){
							$response->historico_tabla[$category]["lotes"][$value->legend]["umbral"][$value->label] = (double)$umbral;
						}else{
							$response->historico_tabla[$category]["lotes"][$value->legend]["umbral_total"] = $this->db->queryRow("SELECT IFNULL(umbral, 0) as umbral FROM bonificacion_config WHERE lote = '{$value->legend}' AND umbral > 0 ORDER BY semana DESC LIMIT 1")->umbral;
						}
					}
					if($this->session->id_company == 7){
						$response->historico_tabla[$category]["lotes"][$value->legend]["umbral_total"] = (double)$umbral;
					}
					// $response->historico_tabla[$value->legend]["total"] = ROUND(($sum[$value->legend] / $count[$value->legend]) , 2);
				}
				$response->totales_historico[$category]["cantidad"][$value->label]  += $valor;
				// $response->historico_tabla[$category]["totales"][$value->label]  = round(($sum[$value->legend] / $count[$value->legend]) , 2);
				$response->historico_tabla[$category]["totales"][$value->label]  += $valor;
				// D($sumCategories[$category][$value->label]);
				// D($countCategories[$category][$value->label]);
				if($filters->mode == "Total Peso" || $filters->mode == "Total Danos"){
					$response->historico_tabla[$category]["totales"][$value->label]  = $sumCategories[$category][$value->label];
				}else{
					$response->historico_tabla[$category]["totales"][$value->label]  = $sumCategories[$category][$value->label] / $countCategories[$category][$value->label];
				}
				if($this->session->id_company == 2){
					$response->totales_historico[$category]["umbral"][$value->label] = $this->db->queryRow("SELECT IFNULL(umbral, 0) as umbral FROM bonificacion_config WHERE lote = '{$value->legend}' AND semana <= '{$value->label}' AND umbral > 0 ORDER BY semana DESC LIMIT 1")->umbral;
					// $response->historico_tabla[$category]["totales"][$value->label] = $response->totales_historico[$category]["umbral"][$value->label];
				}
				if($this->session->id_company == 7){
					$response->totales_historico[$category]["umbral"][$value->label] = (double)$umbral;
				}
				$response->totales_historico[$category]["total"]  += $valor;
				// $response->historico_tabla[$category]["totales"]  += $valor;
			}
        }
		// 24/06/2017 - TAG: NUEVA FORMULA
		foreach ($filters->type as $llave => $category) {
			foreach ($response->historico_tabla[$category]["lotes"] as $key => $value) {
				$response->historico_tabla[$category]["lotes"][$key]["details"] = $this->getDefectos($key , 0 , $category , $filters->idFinca  , $filters->mode , "weeks" , $filters ,$response->historico_tabla[$category]["lotes"] , 'weeks');
			}
		}
		// 24/06/2017 - TAG: NUEVA FORMULA



		$semanas_historico = (array)$this->db->queryAllSpecial("SELECT getWeek(fecha) AS id , getWeek(fecha) AS label FROM merma WHERE YEAR(fecha) = {$filters->year} GROUP BY getWeek(fecha)");
		
		foreach ($semanas_historico as $key => $value) {
			$response->semanas_historico[] = (double)$value;
		}

        return $response;
    }

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}

		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}

		if(isset($postdata->idFincaDia)){
			$id_fincaDia = $postdata->idFincaDia;
		}

		if(isset($postdata->statusLbKg) && $postdata->statusLbKg == 1){
			$this->MermaTypePeso = "Kg";
		}else{
			$this->MermaTypePeso = "Lb";
		}

		if(isset($postdata->year)){
			$year = $postdata->year;
		}else{
			$year = 2016;
		}
        $response = new stdClass();
		$response->id_company = (int)$this->session->id_company;
		$response->tags = $this->tags($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		$response->tabla_lote_merma = $this->tabla_lote_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);

		// Cargamos la data una sola ves 
		$danos = $this->tabla_danos_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);

		// Cargamos la data del historico una sola ves 
		$historico = $this->grafica_historico($palanca , $id_finca , $year);

		// Cargamos la data de tendencia una sola ves 
		$tendencia = $this->tendencia($palanca , $id_finca , $postdata);
		// Cargamos la data Grafica Dia
		$dia = $this->grafica_dia($palanca , $id_finca , $id_fincaDia , $postdata);
		// Asignamos la data 'Tabla'
		#$response->tabla_danos_merma = [];
		$response->tabla_danos_merma = $danos->data;
		// Asignamos la data 'Tabla' Daños
		//$response->tabla_danos_merma_danhos_merma = [];
		$response->tabla_danos_merma_danhos_merma = $danos->danhos_merma;
		// Asignamos la data 'Grafica de daños'
		$response->danos = $danos->grafica;
		// Asignamos la data 'Grafica del detalle de daños'
		$response->danos_detalle = $danos->grafica_detalle;
		// Asignamos la data 'Grafica del historico'
		$response->historico = $historico->data;
		// Asignamos la data 'Grafica del dia'
		$response->dia = $dia->data;
		// Agregamos el titulo de la 'Grafica del dia'
		$response->dia_title = $dia->title;
		// Agregamos el promedio del historico
		$response->historico_avg = $historico->avg;
		$response->historico_legends = $historico->legend;

		// Asignamos la data 'Grafica del historico' y Legend
		$response->tendencia = $tendencia->series;
		$response->tendencia_legends = $tendencia->legend;

		$response->palanca = ["" => "Todos"] + $this->getPalanca($postdata);
		$response->factor = (double)$this->factor;
        $response->fincas = ["" => "Todas"] + $this->getFincas($postdata);
        $response->years = $this->db->queryAllOne("SELECT YEAR(fecha) AS y FROM merma GROUP BY YEAR(fecha) HAVING y > 0 AND y > 2016");

		return $response;
	}
	
	public function tallo(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;

		$sWhere = "";
		if($postdata->idFinca != ""){
			if($postdata->idFinca == "9999999"){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$postdata->idFinca}";
			}
		}

		$sql = "SELECT *, 0 AS 'sum', 0 AS 'count'
				FROM(
					SELECT
						semanas.semana,
						lotes.lote,
						tbl.cantidad
					FROM (
						SELECT semana
						FROM merma
						LEFT JOIN lotes ON lotes.nombre = bloque
						WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0' $sWhere
						GROUP BY semana
					) semanas
					JOIN (
						SELECT bloque AS lote
						FROM merma
						LEFT JOIN lotes ON lotes.nombre = bloque
						WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0' $sWhere
						GROUP BY lote
					) lotes
					LEFT JOIN (
						SELECT bloque AS lote, semana, ROUND(AVG(tallo/racimo*100), 2) cantidad
						FROM merma 
						LEFT JOIN lotes ON lotes.nombre = bloque
						WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0' $sWhere AND tallo > 0
						GROUP BY semana, bloque
					) tbl ON tbl.semana = semanas.semana AND tbl.lote = lotes.lote

					UNION ALL
					SELECT semana, 'MARUN' AS lote, ROUND(AVG(cantidad), 2) AS cantidad
					FROM (
						SELECT
							semanas.semana,
							lotes.lote,
							tbl.cantidad
						FROM (
							SELECT semana
							FROM merma
							WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY semana
						) semanas
						JOIN (
							SELECT bloque AS lote
							FROM merma
							WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY lote
						) lotes
						LEFT JOIN (
							SELECT bloque AS lote, semana, ROUND(AVG(tallo/racimo*100), 2) cantidad
							FROM merma 
							WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0' AND tallo > 0
							GROUP BY semana, bloque
						) tbl ON tbl.semana = semanas.semana AND tbl.lote = lotes.lote
						ORDER BY lotes.lote, semanas.semana
					) tbl
					GROUP BY semana
				) tbl
				ORDER BY lote+0, semana";

		$response->data = $this->db->queryAll($sql);

		foreach($response->data as $row){
			if($row->value > 0){
				$row->sum += $row->value;
				$row->count += 1;
				if(!isset($row->max) || $row->max < $row->value) $row->max = $row->value;
				if(!isset($row->min) || $row->min > $row->value) $row->min = $row->value;
			}
		}
		
		$data_chart = new stdClass;
		$data_table = [];
		$data_table_lotes = [];
		$data_chart->data = [];
		$data_chart->legend = [];
		$umbral = ["count" => 0, "sum" => 0];
		foreach($response->data as $row){
			if(!isset($data_chart->data[$row->lote])){
				$data_chart->data[$row->lote] = [
					"connectNulls" => true,
					"data" => [],
					"name" => $row->lote,
					"type" => 'line',
					'itemStyle' => [
						"normal" => [
							"barBorderRadius" => "0",
							"barBorderWidth" => "6",
							"label" => [
								"show" => false,
								"position" => "insideTop"
							]
						]
					]
				];
			}

			if($row->lote == 'MARUN'){
				$umbral["count"] += 1;
				$umbral["sum"] += $row->cantidad;
			}
			$data_chart->data[$row->lote]["data"][] = $row->cantidad;
			if(!in_array($row->semana, $data_chart->legend)){
				$data_chart->legend[] = $row->semana;
			}

			if($row->lote != 'MARUN'){
				if(!in_array($row->lote, $data_table_lotes)){
					$data_table_lotes[] = $row->lote;
					$data_table[] = ["lote" => $row->lote];
				}
				$indexLote = array_search($row->lote, $data_table_lotes);
				$data_table[$indexLote]["sem_{$row->semana}"] = $row->cantidad;

				if($row->cantidad > 0){
					$data_table[$indexLote]["sum"] += $row->cantidad;
					$data_table[$indexLote]["count"] += 1;
					$data_table[$indexLote]["avg"] = round($data_table[$indexLote]["sum"] / $data_table[$indexLote]["count"], 2);
					if(!isset($data_table[$indexLote]["max"]) || $data_table[$indexLote]["max"] < $row->cantidad) $data_table[$indexLote]["max"] = $row->cantidad;
					if(!isset($data_table[$indexLote]["min"]) || $data_table[$indexLote]["min"] > $row->cantidad) $data_table[$indexLote]["min"] = $row->cantidad;
				}
			}
		}
		foreach($response->data as $row){
			if($row->lote == 'MARUN'){
				if(!in_array($row->lote, $data_table_lotes)){
					$data_table_lotes[] = $row->lote;
					$data_table[] = ["lote" => $row->lote];
				}
				$indexLote = array_search($row->lote, $data_table_lotes);
				$data_table[$indexLote]["sem_{$row->semana}"] = $row->cantidad;

				if($row->cantidad > 0){
					$data_table[$indexLote]["sum"] += $row->cantidad;
					$data_table[$indexLote]["count"] += 1;
					$data_table[$indexLote]["avg"] = round($data_table[$indexLote]["sum"] / $data_table[$indexLote]["count"], 2);
					if(!isset($data_table[$indexLote]["max"]) || $data_table[$indexLote]["max"] < $row->cantidad) $data_table[$indexLote]["max"] = $row->cantidad;
					if(!isset($data_table[$indexLote]["min"]) || $data_table[$indexLote]["min"] > $row->cantidad) $data_table[$indexLote]["min"] = $row->cantidad;
				}
			}
		}
		$response->umbral = round($umbral["sum"]/$umbral["count"], 2);
		$response->chart = $data_chart;
		$response->datatable = $data_table;
		return $response;
	}
    
    # BEGIN TABLA VARIABLES
    public function tablaVariables(){
        $response = new stdClass;
        $filters = $postdata = (object)json_decode(file_get_contents("php://input"));

        $response->tablaCatergorias = $this->tablaVariablesCategorias($filters);
        $response->tablaDanos = $this->tablaVariablesDanos($filters);
        return $response;
    }

    private function tablaVariablesCategorias($filters = []){
        $response = new stdClass;
        $filters = (object) $filters;

        $sFecha = "";
        $sWhere = "";
        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sFecha .= " AND date_fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->finca != ''){
            $sWhere .= " AND id_finca = '{$filters->finca}'";
        }

        $categorias = $this->db->queryAllOne("SELECT TYPE FROM merma INNER JOIN merma_detalle ON merma.id = id_merma WHERE TYPE NOT IN ('RESULTADOS') $sFecha $sWhere GROUP BY TYPE ORDER BY TYPE");
        $lotes = $this->db->queryAllOne("SELECT bloque FROM merma WHERE 1=1 $sFecha $sWhere GROUP BY bloque ORDER BY bloque");

        $response->data = [];
        foreach($lotes as $lote){
            $row = new stdClass;
            $row->lote = $lote;

            foreach($categorias as $categoria){
                if($filters->variable == 'PESO PROM RACIMO'){
                    $row->{"cat_{$categoria}"} = $this->getValuePesoPromRacimo($filters->fecha_inicial, $filters->fecha_final, $filters->finca, $lote, $categoria);
                }else if($filters->variable == '% MERMA'){
                    $row->{"cat_{$categoria}"} = $this->getValueMerma($filters->fecha_inicial, $filters->fecha_final, $filters->finca, $lote, $categoria);
                }else if($filters->variable == 'DEDOS PROM RACIMO'){
                    $row->{"cat_{$categoria}"} = $this->getValueDedosProm($filters->fecha_inicial, $filters->fecha_final, $filters->finca, $lote, $categoria);
                }
                if($row->{"cat_{$categoria}"} > 0)
                    $row->total += $row->{"cat_{$categoria}"};
            }
            $response->data[] = $row;
        }

        #BEGIN ROW PROMEDIO
        $sum_categorias = [];
        $promedio = new stdClass;
        $promedio->lote = 'PROM';
        foreach($categorias as $categoria){
            foreach($response->data as $row){
                if($row->{"cat_{$categoria}"} > 0){
                    $sum_categorias[$categoria]["sum"] += $row->{"cat_{$categoria}"};
                    $sum_categorias[$categoria]["count"] += 1;
                }
            }
        }
        foreach($response->data as $row){
            if($row->{"total"} > 0){
                $sum_categorias["total"]["sum"] += $row->{"total"};
                $sum_categorias["total"]["count"] += 1;
            }
        }
        $promedio->total = round($sum_categorias["total"]["sum"] / $sum_categorias["total"]["count"], 2);
        foreach($categorias as $categoria){
            $promedio->{"cat_{$categoria}"} = $sum_categorias[$categoria]["count"] > 0 ? round($sum_categorias[$categoria]["sum"] / $sum_categorias[$categoria]["count"], 2) : 0;
        }
        $response->data[] = $promedio;
        #END ROW PROMEDIO

        $response->categorias = $categorias;
        $response->lotes = $lotes;
        return $response;
    }

    private function tablaVariablesDanos($filters = []){
        $response = new stdClass;
        $filters = (object) $filters;

        $sFecha = "";
        $sWhere = "";
        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sFecha .= " AND date_fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->finca != ''){
            $sWhere .= " AND id_finca = '{$filters->finca}'";
        }
        if($filters->categoria == ''){
            $filters->categoria = $this->db->queryOne("SELECT TYPE FROM merma INNER JOIN merma_detalle WHERE cantidad > 0 AND campo LIKE 'Total Peso%' AND TYPE != 'RESULTADOS' $sFecha $sWhere LIMIT 1");
            $response->categoria = $filters->categoria;
        }

        $defectos = $this->db->queryAllOne("SELECT campo FROM merma INNER JOIN merma_detalle ON merma.id = id_merma WHERE flag = 1 AND TYPE = '{$filters->categoria}' $sFecha $sWhere GROUP BY campo ORDER BY campo");
        $lotes = $this->db->queryAllOne("SELECT bloque FROM merma INNER JOIN merma_detalle ON merma.id = id_merma WHERE TYPE = '{$filters->categoria}' AND cantidad > 0 AND campo LIKE 'Total Peso%' $sFecha $sWhere GROUP BY bloque ORDER BY bloque");

        $response->data = [];
        foreach($lotes as $lote){
            $row = new stdClass;
            $row->lote = $lote;

            foreach($defectos as $defecto){
                if($filters->variable == 'PESO PROM RACIMO'){
                    $row->{"def_{$defecto}"} = $this->getValuePesoPromRacimo($filters->fecha_inicial, $filters->fecha_final, $filters->finca, $lote, $filters->categoria, $defecto);
                }else if($filters->variable == '% MERMA'){
                    $row->{"def_{$defecto}"} = $this->getValueMerma($filters->fecha_inicial, $filters->fecha_final, $filters->finca, $lote, $filters->categoria, $defecto);
                }else if($filters->variable == 'DEDOS PROM RACIMO'){
                    $row->{"def_{$defecto}"} = $this->getValueDedosProm($filters->fecha_inicial, $filters->fecha_final, $filters->finca, $lote, $filters->categoria, $defecto);
                }
                if($row->{"def_{$defecto}"} > 0)
                    $row->total += $row->{"def_{$defecto}"};
            }
            $response->data[] = $row;
        }

        #BEGIN ROW PROMEDIO
        $sum_defectos = [];
        $promedio = new stdClass;
        $promedio->lote = 'PROM';
        foreach($defectos as $defecto){
            foreach($response->data as $row){
                if($row->{"def_{$defecto}"} > 0){
                    $sum_defectos[$defecto]["sum"] += $row->{"def_{$defecto}"};
                    $sum_defectos[$defecto]["count"] += 1;
                }
            }
        }
        foreach($response->data as $row){
            if($row->{"total"} > 0){
                $sum_defectos["total"]["sum"] += $row->{"total"};
                $sum_defectos["total"]["count"] += 1;
            }
        }
        $promedio->total = $sum_defectos["total"]["count"] > 0 ? round($sum_defectos["total"]["sum"] / $sum_defectos["total"]["count"], 2) : 0;
        foreach($defectos as $defecto){
            $promedio->{"def_{$defecto}"} = round($sum_defectos[$defecto]["sum"] / $sum_defectos[$defecto]["count"], 2);
        }
        $response->data[] = $promedio;
        #END ROW PROMEDIO

        $response->defectos = $defectos;
        $response->lotes = $lotes;
        return $response;
    }

    private function getValuePesoPromRacimo($inicio, $fin, $finca = 0, $lote, $categoria, $defecto = ""){
        $sWhere = "";
        if($finca > 0){
            $sWhere .= " AND id_finca = '{$finca}'";
        }
        if($defecto != ''){
            $sql = "SELECT ROUND(SUM(IFNULL(cantidad, 0)) / SUM(racimos_procesados), 2)
                    FROM merma 
                    LEFT JOIN merma_detalle ON merma.id = id_merma  AND TYPE = '{$categoria}' AND campo = '{$defecto}'
                    WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND bloque = '{$lote}' $sWhere";
        }else{
            $sql = "SELECT ROUND(SUM(IFNULL(cantidad, 0)) / SUM(racimos_procesados), 2)
                    FROM merma 
                    LEFT JOIN merma_detalle ON merma.id = id_merma  AND TYPE = '{$categoria}' AND campo LIKE 'Total Peso%'
                    WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND bloque = '{$lote}' $sWhere";
        }
        return $this->db->queryOne($sql);
    }

    private function getValueDedosProm($inicio, $fin, $finca = 0, $lote, $categoria, $defecto = ""){
        if($finca > 0){
            $sWhere .= " AND id_finca = '{$finca}'";
        }
        if($defecto != ''){
            $sql = "SELECT ROUND(SUM(IFNULL(cantidad, 0)) / SUM(racimos_procesados) * 5, 2)
                    FROM merma 
                    LEFT JOIN merma_detalle ON merma.id = id_merma  AND TYPE = '{$categoria}' AND campo = '{$defecto}'
                    WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND bloque = '{$lote}' $sWhere";
        }else{
            $sql = "SELECT ROUND(SUM(IFNULL(cantidad, 0)) / SUM(racimos_procesados) * 5, 2)
                    FROM merma 
                    LEFT JOIN merma_detalle ON merma.id = id_merma  AND TYPE = '{$categoria}' AND campo LIKE 'Total Peso%'
                    WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND bloque = '{$lote}' $sWhere";
        }
        return $this->db->queryOne($sql);
    }

    private function getValueMerma($inicio, $fin, $finca = 0, $lote, $categoria, $defecto = ""){
        if($finca > 0){
            $sWhere .= " AND id_finca = '{$finca}'";
        }
        if($defecto != ''){
            $sql = "SELECT ROUND(SUM(IFNULL(cantidad, 0)) / SUM(peso_neto) * 100, 2)
                    FROM merma 
                    LEFT JOIN merma_detalle ON merma.id = id_merma  AND TYPE = '{$categoria}' AND campo = '{$defecto}'
                    WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND bloque = '{$lote}' $sWhere";
        }else{
            $sql = "SELECT ROUND(SUM(IFNULL(cantidad, 0)) / SUM(peso_neto) * 100, 2)
                    FROM merma 
                    LEFT JOIN merma_detalle ON merma.id = id_merma  AND TYPE = '{$categoria}' AND campo LIKE 'Total Peso%'
                    WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' AND bloque = '{$lote}' $sWhere";
        }
        return $this->db->queryOne($sql);
    }

    #END TABLA VARIABLES

	// public function historico(){
	// 	$postdata = (object)json_decode(file_get_contents("php://input"));
	// 	$palanca = "";
	// 	if(isset($postdata->palanca)){
	// 		$palanca = $postdata->palanca;
	// 	}

	// 	if(isset($postdata->idFinca)){
	// 		$id_finca = $postdata->idFinca;
	// 	}

	// 	$historico = $this->grafica_historico($palanca , $id_finca , $year);

	// 	$response->historico = $historico->data;

	// 	return $response;
    // }
    
    public function last(){
        //ultima fecha con registros
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if(isset($postdata->fecha_inicial) && isset($postdata->fecha_final)){
            if(!empty($postdata->fecha_inicial) && !empty($postdata->fecha_final)){
                $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";

                if($postdata->idFinca > 0){
                    $e = $this->db->queryOne("SELECT EXISTS(SELECT * FROM merma WHERE date_fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' AND id_finca = $postdata->idFinca)");
                    if(!$e){
                        $response->finca = $this->db->queryOne("SELECT id_finca FROM merma WHERE date_fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}' LIMIT 1");
                    }
                }
            }
        }
        if(!isset($postdata->fecha_inicial) && !isset($postdata->fecha_final)){
            $reg = $this->db->queryRow("SELECT finca, DATE(fecha) AS fecha, id_finca FROM merma WHERE 1=1 {$sWhere} ORDER BY fecha DESC, id_finca LIMIT 1");
            $response->fecha = $reg->fecha;
			$response->finca = $reg->id_finca;
			
			$response->days = $this->db->queryAllOne("SELECT date_fecha FROM merma WHERE fecha != '0000-00-00' GROUP BY date_fecha");
        }
        return $response;
    }

	private function getPalanca($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";
		if(isset($filters->idFinca) && $filters->idFinca != ""){
			// $sWhere .= " AND id_finca = {$filters->idFinca}";
			if($filters->idFinca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$filters->idFinca}";
			}
		}
		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT palanca AS id , palanca AS label  FROM merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND palanca != '' $sWhere {$typeMerma}
				GROUP BY TRIM(palanca)";
		// D($sql);
		$response->data = [];
		$response->data = $this->db->queryAllSpecial($sql);
		return $response->data;
	}	

	private function getFincas($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		
		$response = new stdClass;
		$sql = "SELECT id_finca AS id , finca AS label  FROM merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND finca != ''
                GROUP BY id_finca";
        $response->data = $this->db->queryAllSpecial($sql);

        $san_jose = false;
        foreach($response->data as $id_finca => $finca){
            if($id_finca == 2 || $id_finca == 3) $san_jose = true;
        }
        if($san_jose){
            $response->data = ["9999999" => "San José"] + $response->data;
        }
        
		return $response->data;
	}

	private function tags($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$sql = "SELECT (SELECT SUM(cantidad) 
                        FROM merma 
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                            AND porcentaje_merma > 0 
                            AND campo = 'Total peso Merma Neta (Kg)'
                            $sWhere) / SUM(peso_neto) * 100 AS merma
				FROM merma 
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
                
		$rp1 = $this->db->queryRow($sql);

		$peso_neto = "(racimo - tallo)";
		$cantidad = "(cantidad)";
		$cajas = "(SUM(total_peso_merma-tallo) / $this->cajas) AS cajas";

		$TagsCaja = (double)$this->db->queryOne("SELECT {$cajas} FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' {$sWhere}");


		$sql = "SELECT 
					(SELECT SUM(cantidad) 
                        FROM merma 
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                            AND porcentaje_merma > 0 
                            AND campo = 'Total peso Merma Neta (Kg)'
                            $sWhere) / SUM(peso_neto) * 100 AS merma_neta,
					AVG(porcentaje_merma_procesada) AS merma_procesada 
				FROM merma 
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
            
		$rp1 = $this->db->queryRow($sql);

		$sql2 = "SELECT AVG((SELECT (cantidad)
				FROM merma_detalle
				WHERE merma_detalle.id_merma = merma.id
				AND campo = '% MERMA CORTADA')) AS merma_cortada,
				AVG((SELECT (cantidad)
				FROM merma_detalle
				WHERE merma_detalle.id_merma = merma.id
				AND campo = '% TALLO')) AS tallo
				FROM merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' $sWhere";
		$rp2 = $this->db->queryRow($sql2);

		$sql ="SELECT LOWER(type) AS id, 
                        ROUND(
                            (SUM(cantidad) / 
                            (SELECT SUM(cantidad)
                                FROM merma
                                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                                    AND campo LIKE 'Total peso Merma Neta (Kg)'
                                    $sWhere) * 100), 2) AS label
                FROM merma
                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                    AND campo LIKE 'Total peso%'
                    AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
                    $sWhere
                GROUP BY TYPE";

        $data_categorias = $this->db->queryAllSpecial($sql);
        $data = [];
        $sum_peso = 0;
        foreach($data_categorias as $cat => $val){
            $data[$cat]["porc"] = $val;
            $data[$cat]["peso"] = $this->db->queryRow("SELECT ROUND(SUM(cantidad), 2) as peso 
                                                        FROM merma
                                                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                                        WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                                                            AND campo LIKE 'Total peso%'
                                                            AND TYPE = UPPER('{$cat}')
                                                            $sWhere")->peso;
            $sum_peso += $data[$cat]["peso"];
        }


		$tags = [];
		$tags = array(
			'merma' 		  => ["porc" => $rp1->merma_neta, "peso" => $sum_peso ],
			'merma_procesada' => $rp1->merma_procesada,
			'merma_cortada'	  => $rp2->merma_cortada,
			'tallo'			  => $rp2->tallo,
			'empaque' 		  => ["porc" => $data["empaque"]["porc"], "peso" => $data["empaque"]["peso"]],
			'administracion'  => ["porc" => $data["administracion"]["porc"], "peso" => $data["administracion"]["peso"]],
			'cosecha' 		  => ["porc" => $data["cosecha"]["porc"], "peso" => $data["cosecha"]["peso"]],
			'deshoje' 		  => ["porc" => $data["deshoje"]["porc"], "peso" => $data["deshoje"]["peso"]],
			'lotero' 		  => ["porc" => $data['lotero aereo']["porc"], "peso" => $data["lotero aereo"]["peso"]],
			'fisiologicos' 	  => ["porc" => $data["fisiologicos"]["porc"], "peso" => $data["fisiologicos"]["peso"]],
            'apuntalador' 	  => ["porc" => $data["apuntalador"]["porc"], "peso" => $data["apuntalador"]["peso"]],
			'cajas' 	  	  => $TagsCaja,
			'usd' 	  	  	  => ($TagsCaja * $this->usd) ,
		);

		return $tags;
	}

	private function tabla_lote_merma($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$data = [];
		$peso_total = "SUM(total_peso_merma) AS peso_total";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		if($this->session->id_company == 2){

		}else{
			$total_peso_merma = "total_peso_merma";
			$racimos_lote = "id";

			if($this->session->id_company == 7){
				$peso_total = "SUM((total_peso_merma-tallo)) AS peso_total";
				$total_peso_merma = "(total_peso_merma-tallo) / racimos_procesados";
				$racimos_lote = "racimos_procesados";
				$cajas = "(SUM(total_peso_merma - tallo)  / $this->cajas) AS cajas";
			}
			$typeMerma = "";

			$sql = "SELECT id , 
						bloque AS lote ,
						(SELECT AVG((total_peso_merma-tallo) / racimos_procesados) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = bloques.bloque $sWhere {$typeMerma}) AS total_peso_merma , 
						(SELECT AVG(total_defectos) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = bloques.bloque $sWhere {$typeMerma}) AS total_defectos , 
						(SELECT SUM(total_defectos) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = bloques.bloque $sWhere {$typeMerma}) AS total_defectos_sum ,
						(SELECT SUM(peso_neto) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = bloques.bloque $sWhere {$typeMerma}) AS peso_neto,
						(SELECT {$cajas} FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS cajas,  
						(SELECT SUM({$racimos_lote}) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS racimos_lote , 
						(SELECT AVG({$racimos_lote}) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS racimos_lote_avg , 
						(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE bloque = bloques.bloque 
                                AND date_fecha BETWEEN '$inicio' AND '$fin'
                                AND campo = 'Total Peso Merma Neta (Kg)' $sWhere {$typeMerma}) AS peso_total
			FROM(
			SELECT id, bloque
				FROM merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma} 
				GROUP BY bloque) AS bloques";
		}
		$res = $this->db->queryAll($sql);
		$danhos = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
            $value->merma = $value->peso_total / $value->peso_neto * 100;
			$value->usd = (double)($value->cajas * $this->usd);
			$value->expanded = false;
			$value->detalle = [];
            //$value->detalle = $this->tabla_lote_merma_detalle($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
			$danhos = $this->getDanhos($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
			$value->detalle_danhos = $danhos->danhos;
			$value->detalle_danhos_sum = $danhos->danhos_suma;
		}
		//D($res);
		return $res;
	}

	private function getDanhos($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		if($this->session->id_company == 7){
			$typeMerma = " AND campo NOT LIKE '%Total Peso%'";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label , SUM(cantidad) AS label2 FROM (
				SELECT merma_detalle.id,merma.id AS id_m , fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE bloque = '{$bloque}' AND DATE(fecha) BETWEEN '$inicio' AND '$fin' AND campo like '%".$this->search."%'  {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere ) as detalle
				GROUP BY type";
		// D($sql);
		$res = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->danhos = [];
		$response->danhos_suma = [];
		foreach ($res as $key => $value) {
			$response->danhos[$value->id] = (double)$value->label;
			$response->danhos_suma[$value->id] = (double)$value->label2;
		}

		return $response;
	}

	private function getDanhosPesos(){
		if($this->session->id_company == 7){
			$sql = "";
		}
		return array();
	}

	public function getSecondLevel(){
		$postdata = (array)json_decode(file_get_contents("php://input"));

		$data = (object)[
			"id_finca" => getValueFrom($postdata , "idFinca" , 0 ),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , 0 ),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , 0 ),
			"bloque" => getValueFrom($postdata , "bloque" , 0 ),
			"palanca" => getValueFrom($postdata , "palanca" , 0 )
		];

		return $this->tabla_lote_merma_detalle($data->fecha_inicial , $data->fecha_final , $data->bloque , $data->palanca , $data->id_finca);
	}

	private function tabla_lote_merma_detalle($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "", $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$data = [];

        $cajas = "(SUM(total_peso_merma - tallo)  / $this->cajas) AS cajas";

		$sql = "SELECT TYPE AS 'type', '{$bloque}' AS bloque,
						(SELECT AVG(total_defectos) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' AND TYPE = types.TYPE $sWhere ) AS total_defectos , 
						(SELECT SUM(total_defectos) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' AND TYPE = types.TYPE $sWhere ) AS total_defectos_sum ,
						(SELECT SUM(peso_neto) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' $sWhere ) AS peso_neto,
						(SELECT {$cajas} FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE bloque = '{$bloque}' AND TYPE = types.TYPE AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere ) AS cajas,  
						(SELECT SUM(racimos_procesados) FROM merma WHERE bloque = '{$bloque}' AND TYPE = types.TYPE AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere ) AS racimos_lote , 
						(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE bloque = '{$bloque}' AND TYPE = types.TYPE
                                AND date_fecha BETWEEN '$inicio' AND '$fin'
                                AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
                                AND campo LIKE 'Total Peso%' $sWhere) AS peso_total
                FROM(
                SELECT TYPE
                FROM merma
                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                WHERE bloque = '{$bloque}' 
                    AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere
                    AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') 
                    AND campo LIKE 'Total Peso%'
                GROUP BY TYPE) AS types";
		
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value->expanded = false;
            $value->cantidad = $value->peso_total / $value->peso_neto * 100;
			$value->usd = (double)($value->cajas * $this->usd);
			$value->details = [];
			// $value->details = $this->tabla_lote_merma_detalle_details($inicio, $fin , $value->type, $palanca, $id_finca , $filters , $bloque, $value->danhos_peso, $value->racimos_lote, $value->cantidad, $cajas);
		}
		return $res;
	}

	public function getThirdLevel(){
		$postdata = (array)json_decode(file_get_contents("php://input"));

		$data = (object)[
			"id_finca" => getValueFrom($postdata , "idFinca" , 0 ),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , 0 ),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , 0 ),
			"bloque" => getValueFrom($postdata , "bloque" , 0 ),
			"type" => getValueFrom($postdata , "type" , 0 ),
			"danhos_peso" => getValueFrom($postdata , "danhos_peso" , 0 ),
			"racimos_lote" => getValueFrom($postdata , "racimos_lote" , 0 ),
			"cantidad" => getValueFrom($postdata , "cantidad" , 0 ),
			"palanca" => getValueFrom($postdata , "palanca" , 0 )
		];

		return $this->tabla_lote_merma_detalle_details($data->fecha_inicial , $data->fecha_final , $data->type , $data->palanca , $data->id_finca , [] ,$data->bloque ,$data->danhos_peso ,$data->racimos_lote ,$data->cantidad ,"" );
	}

	private function tabla_lote_merma_detalle_details($inicio, $fin , $type , $palanca = "", $id_finca = "" , $filters = [] , $bloque, $_danhos_peso, 
		$racimos_procesados, $_cantidad){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$cajas = "(SUM(cantidad) / $this->cajas) AS cajas";
		$peso_total = "SUM(cantidad) AS total_peso";

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$racimo = "racimo";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "racimo";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 4){
			$cantidad = "(cantidad/racimos_procesados)";
			$racimo = "(racimo)";

			if($this->MermaTypePeso == "Kg"){
				$peso_total = "SUM(cantidad / 2.2) AS total_peso";
			}
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad/racimos_procesados)";
			$racimo = "(racimo)";
		}elseif($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$racimo = "(racimo)";
				$cantidad = "(cantidad)";
			}
		}else{
				$racimo = "racimo";
				$cantidad = "(cantidad)";
		}


		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
			$cantidad = "(cantidad/racimos_procesados)";
			$racimo = "(racimo)";

			if($this->MermaTypePeso == "Kg"){
				$peso_total = "SUM(cantidad / 2.2) AS total_peso";
			}
		}

		$condition = "AND flag = 1";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}
		}

		$response = new stdClass;
		$sql = "SELECT REPLACE(campo,'Peso ' , '') AS campo,
						(SELECT AVG(total_defectos) FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo $sWhere ) AS total_defectos , 
						(SELECT SUM(total_defectos) FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo $sWhere ) AS total_defectos_sum ,
						(SELECT SUM(peso_neto) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' $sWhere ) AS peso_neto,
						(SELECT {$cajas} FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere ) AS cajas,  
						(SELECT SUM(racimos_procesados) FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere ) AS racimos_lote , 
						(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo
                                AND date_fecha BETWEEN '$inicio' AND '$fin'
                                AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
                                $sWhere) AS peso_total
                FROM(
                SELECT campo
                FROM merma
                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                WHERE bloque = '{$bloque}' 
                    AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere
                    AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')                     
                    AND type = '{$type}'
                    AND cantidad > 0 AND flag = 1
                GROUP BY campo) AS campos";
		
		$response->data = $this->db->queryAll($sql);
        foreach($response->data as $row){
            $row->cantidad = $row->peso_total / $row->peso_neto * 100;
        }
		// $response->danhos_merma = $this->getDanhosMermaDetalle($inicio, $fin , $type , $palanca , $id_finca , $filters);
		return $response->data;
	}

	private function grafica_historico($palanca = "", $id_finca = "" , $year = "YEAR(CURRENT_DATE)" ){
		$sWhere = "";
		$sWhere_PromFincas = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$sWhere .= " AND YEAR(fecha) = {$year}";
		$Umbral = 2;

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}

		$sql = "SELECT TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere
				GROUP BY id_finca , getWeek(fecha)
				ORDER BY id_finca , getWeek(fecha)";
		
        if((int)$id_finca > 1 && $palanca != ""){
            $sWhere_PromFincas = " AND palanca = '{$palanca}'";
        }elseif((int)$id_finca == 1){
            $sWhere_PromFincas = $sWhere;
        }
        $sWhere_PromFincas .= " AND YEAR(fecha) = {$year}";
		$sql = "SELECT *
				FROM (
					SELECT finca, id_finca, semana AS fecha, SUM(total_peso_merma-tallo)/SUM(peso_neto)*100 AS porcentaje_merma, 0 AS total_peso_merma
					FROM merma
					WHERE porcentaje_merma > 0 $sWhere
					GROUP BY year, semana
					UNION ALL
					SELECT 'San José' AS finca, '9999999' AS id_finca, fecha , AVG(porcentaje_merma) ,total_peso_merma FROM (
					SELECT TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
									FROM merma
									WHERE porcentaje_merma > 0 AND finca LIKE '%San%' $sWhere_PromFincas
									GROUP BY id_finca , getWeek(fecha)
									ORDER BY id_finca , getWeek(fecha)) as detalle
					GROUP BY (fecha)
					ORDER BY (fecha)
				) tbl
				ORDER BY fecha, id_finca";

		$sql = "SELECT 
					semanas.semana AS fecha,
					fincas.id_finca,
					fincas.finca,
					datos.valor AS porcentaje_merma
				FROM (
					SELECT semana
					FROM merma
					WHERE 1=1 $sWhere
					GROUP BY semana
				) semanas
				JOIN (
					SELECT id_finca, finca
					FROM merma
					WHERE 1=1 $sWhere
					GROUP BY id_finca, finca
				) fincas
				LEFT JOIN (
					SELECT semana, id_finca, SUM(total_peso_merma-tallo)/SUM(peso_neto)*100 valor
					FROM merma
					WHERE 1=1 $sWhere
					GROUP BY semana, id_finca
				) datos ON datos.semana = semanas.semana AND datos.id_finca = fincas.id_finca
				ORDER BY fincas.id_finca, semanas.semana";

		$Umbral = 5;
		if($year == 2019) $Umbral = 3;

		$response = new stdClass;
		// D($sql);
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->porcentaje_merma = (float)$value->porcentaje_merma;
			$sum += $value->porcentaje_merma;
			$response->legend[(int)$value->fecha] = (int)$value->fecha;
			if($finca != $value->id_finca){
				$series = new stdClass;
				$finca = $value->id_finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->data = [];
				$series->data[$value->fecha] = round($value->porcentaje_merma ,2);
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => false , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series->markLine = new stdClass;
					$series->markLine->data = [[ "name" => "Umbral", "label" => 'Umbral', "value" => $Umbral, "xAxis" => -1, "yAxis" => $Umbral ]];
					$flag_count++;
				}
				
				$response->data[$value->id_finca] = $series;
			}else{
				if($finca == $value->id_finca){
					// D($response->data[$value->id_finca]);
					$response->data[$value->id_finca]->data[$value->fecha] = round($value->porcentaje_merma ,2);
				}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}

	private function historico_general($palanca = "" , $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}

		$typeMerma = "";
		$response = new stdClass;
		$sql = "SELECT TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere {$typeMerma}
				GROUP BY getWeek(fecha)";
		$res = $this->db->queryAll($sql);
	}

	private function grafica_dia($palanca = "", $id_finca = ""  , $id_fincaDia = "" , $filters = []){
		$sWhere = "";
		$fincaDia = 0;
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
        }

        if($filters->fecha_inicial != '' && $filters->fecha_final != ''){
            $sWhere .= " AND date_fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
		if($id_fincaDia != ""){
			$fincaDia = $id_fincaDia;
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}


        $typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT id , id_finca,TIME_FORMAT(TIME(fecha) , '%H:%i') AS time ,DATE(fecha) as fecha ,IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma,
				CONCAT_WS(' ','Última Actualización : ' , DATE(fecha)) as title 
                FROM merma
				WHERE 1 = 1 $sWhere {$typeMerma}
				GROUP BY id
				ORDER BY fecha desc";
		// D($sql);
		$res = $this->db->queryAll($sql);
		$count = 1;
		$fecha = "";
		$flag = 0;
		$response->data = new stdClass;
		$response->data->data = [];
		$response->data->label = [];
		$response->data->series = [];
		$response->title = "";
		$label = "";
		foreach ($res as $key => $value) {
			$value = (object)$value;
			if($value->fecha != $fecha){
				$fecha = $value->fecha;
				$flag++;
				if($flag == 2){
					break;
				}
			}

			if($fincaDia > 0){
				if($value->id_finca == $fincaDia){
					$response->title = $value->title;
					$value->porcentaje_merma = (float)$value->porcentaje_merma;
					if($value->time == '00:00'){
						$label = "Hora ".$count;
					}else{
						$label = $value->time;
					}
					$response->data->label[$value->fecha." ".$label] = $label;
					$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
					$response->data->data[$label] = $value;
					$count++;
				}
			}else{
				$response->title = $value->title;
				$value->porcentaje_merma = (float)$value->porcentaje_merma;
				if($value->time == '00:00'){
					$label = "Hora ".$count;
				}else{
					$label = $value->time;
				}
				$response->data->label[$value->fecha." ".$label] = $label;
				$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
				$response->data->data[$label] = $value;
				$count++;
			}
		}
		return $response; 
	}

	private function tabla_danos_merma($inicio, $fin , $palanca , $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$response = new stdClass;
		$cantidad = "(cantidad)";
		$racimo = "(racimo)";
		$peso_total = "SUM(total_peso_merma - tallo) AS peso_total";
		$cajas = "(SUM(total_peso_merma - tallo) / $this->cajas) AS cajas";
		$racimos_lote = "racimos_procesados";

		$typeMerma = "";
		
		$subQuery = "(SELECT SUM(racimo) FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin')";
		$sql ="SELECT type,
						AVG(cantidad) AS danhos_peso,
						AVG(total_defectos) AS total_defectos, 
						SUM(total_defectos) AS total_defectos_sum , 
						$peso_total,
						SUM($racimos_lote) AS racimos_lote , 
						AVG($racimos_lote) AS racimos_lote_avg,
						ROUND((ROUND(SUM(cantidad),2) / ROUND({$subQuery},2)) * 100,2) AS cantidad, 
                        {$cajas}
				FROM (
					SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , peso_neto, total_defectos , total_peso_merma , tallo , {$racimos_lote}
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND campo like '%Total Peso%' {$typeMerma}
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere 
					HAVING cantidad > 0
				) as detalle
				GROUP BY type";
		
		$response->data = $this->db->queryAll($sql);
        

        $data_chart = $this->db->queryAll("SELECT type, ROUND(SUM(cantidad), 1) AS cantidad
                                                FROM merma
                                                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                                WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                                                    AND campo LIKE 'Total peso%'
                                                    AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
                                                    $sWhere
                                                GROUP BY TYPE
                                                HAVING cantidad > 0");
		$response->grafica = $this->pie($data_chart , '50%' ,"Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		$response->grafica_detalle = [];
		$detalle = "";
		foreach ($response->data as $key => $value) {
			$value = (object)$value;
			$value->usd = (double)($value->cajas * $this->usd);
			$value->detalle = [];
		}

        foreach($data_chart as $value){
            $detalle = $this->tabla_danos_merma_detalle($inicio, $fin , $value->type, $palanca, $id_finca , $filters, $value->cantidad);
            $response->grafica_detalle[$value->type] = $detalle->grafica;
        }

		$response->danhos_merma = [];
		$response->danhos_merma = $this->getDanhosMerma($inicio, $fin , $palanca , $id_finca , $filters);
		return $response;
	}

	private function getDanhosMerma($inicio ,$fin  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$sWhereConsulta = "AND campo like '%".$this->search."%'";
		if($this->session->id_company == 4 || $this->session->id_company == 9){
			$sWhereConsulta = "AND flag = 1";
		}

		if($this->session->id_company == 7){
			$sWhereConsulta = "AND campo like '%".$this->search."%' AND campo NOT LIKE '%Total Peso%'";
		}

		if($this->session->id_company == 3){
			$sWhereConsulta = "AND campo like '%Total Peso%'";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label 
				FROM (
					SELECT merma_detalle.id,merma.id AS id_m , fecha  ,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' {$sWhereConsulta} {$typeMerma}
					AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere 
				) as detalle
				GROUP BY type";
		//D($sql);
		$res = $this->db->queryAllSpecial($sql);

		return $res;
	}

	private function tabla_danos_merma_detalle($inicio, $fin , $type , $palanca = "", $id_finca = "" , $filters = [], $value_total = 0){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$response = new stdClass;
		$sql = "SELECT campo AS type, ROUND((SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma
                                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                        WHERE date_fecha BETWEEN '$inicio' AND '$fin'
                                            AND campo LIKE 'Total Peso%'
                                            AND TYPE = '{$type}' $sWhere) * 100) * (ROUND($value_total, 2) / 100), 2) AS cantidad
                FROM merma
                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                WHERE date_fecha BETWEEN '$inicio' AND '$fin'
                    AND TYPE = '{$type}'
                    AND flag = 1
                    AND cantidad > 0 $sWhere
                GROUP BY campo";
		
		$response->data = $this->db->queryAll($sql);
		$response->grafica = $this->pie($response->data , '50%' ,"Detalle de Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		return $response;
	}

	private function getDanhosMermaDetalle($inicio ,$fin , $type  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$condition = "AND flag = 2";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}elseif($type == "COSECHA"){
				$condition = "AND (flag = 1 OR flag = 2)";
			}
		}

		$sql = "SELECT campo AS id ,AVG(cantidad) AS label , SUM(cantidad) AS label2 FROM (
				SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND type = '{$type}' {$condition} {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS') $sWhere) as detalle
				GROUP BY type ,campo";
		#D($sql);
		$res = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->danhos = [];
		$response->danhos_suma = [];
		foreach ($res as $key => $value) {
			$response->danhos[$value->id] = (double)$value->label;
			$response->danhos_suma[$value->id] = (double)$value->label2;
		}

		return $response;
	}

	public function tendenciaSemanal(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}
		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}
		if(isset($postdata->yearTendencia)){
			$year = $postdata->yearTendencia;
		}else{
			$year = date('Y');
        }

        $tendencia = $this->tendencia($palanca , $id_finca , $postdata);
        $response->tendencia = $tendencia->series;
		$response->tendencia_legends = $tendencia->legend;
        return $response;
    }

	private function tendencia($palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		if(isset($filters->yearTendencia) && $filters->yearTendencia != ""){
			$sWhere .= " AND YEAR (fecha) = '{$filters->yearTendencia}' ";
		}else{
			$sWhere .= " AND YEAR(fecha) = 2017 ";
		}

		$Umbral = 50;
        $semanas = $this->db->queryAllSpecial("SELECT semana as id, semana as label FROM merma WHERE 1=1 $sWhere GROUP BY semana");
        $total_semanas = $this->db->queryAllSpecial("SELECT semana AS id, SUM(cantidad) AS label
                                                    FROM merma
                                                    INNER JOIN merma_detalle ON id_merma = merma.`id`
                                                    WHERE TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS')
                                                        AND campo LIKE 'Total Peso%' $sWhere
													GROUP BY semana");
		$sql = "SELECT semana, TYPE AS labor, 
				(
					SELECT SUM(cantidad) 
					FROM merma 
					INNER JOIN merma_detalle ON id_merma = merma.`id` 
					WHERE semana = semanas.semana 
						AND TYPE = categorias.type 
						AND flag = 1 
						AND cantidad > 0
						$sWhere
				) cantidad
			FROM (
				SELECT semana
				FROM merma
				INNER JOIN merma_detalle ON id_merma = merma.`id`
				WHERE flag = 1 AND TYPE != 'RESULTADOS' AND type != 'FIRMA ELECTRONICA' $sWhere
				GROUP BY semana
			) AS semanas
			JOIN (
				SELECT TYPE
				FROM merma
				INNER JOIN merma_detalle ON id_merma = merma.`id`
				WHERE flag = 1 AND TYPE != 'RESULTADOS' AND type != 'FIRMA ELECTRONICA' $sWhere
				GROUP BY TYPE
			) AS categorias
			ORDER BY semanas.semana";
		
        $data = $this->db->queryAll($sql);
        $types = [];
		$response = new stdClass;
		$series = new stdClass;
		$flag_count = 0;
		$labor = "";

		$response->series = [];
		$response->legend = [];
		$cantidad = 0;
		foreach ($data as $key => $value) {
            if(!in_array($value->semana, $response->legend))
                $response->legend[] = (int) $value->semana;
                
			$cantidad = round(($value->cantidad / $total_semanas[$value->semana]) * 100 , 2);

			if(!in_array($value->labor, $types)){
                $types[] = $value->labor;

				$series = new stdClass;
				$series->name = $value->labor;
				$series->type = "line";
				$series->data[] = $cantidad;
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => false , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series = new stdClass;
					$series->markLine = new stdClass;
					$series->markLine->data = [];
					$markLine = new stdClass;
					$markLine->name = "Umbral";
					$markLine->value = $Umbral;
					$markLine->xAxis = -1;
					$markLine->yAxis = $Umbral;
					$series->markLine->data[0][] = $markLine;
					
					$markLine = new stdClass;
					$markLine->xAxis = 52;
					$markLine->yAxis = $Umbral;

					$series->markLine->data[0][] = $markLine;
					$flag_count++;
				}	

				$response->series[$value->labor] = $series;
			}else{
				$response->series[$value->labor]->data[] = $cantidad;
			}
		}

		return $response;
	}

	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "DAÑOS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['60%', '60%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = "Merma";
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["orient"] = "vertical";
			$response->pie["legend"]["x"] = "left";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];

			foreach ($data as $key => $value) {
				$value = (object)$value;
				$response->pie["legend"]["data"][] = $value->type;
				if($version === 3){
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)$value->cantidad , 
							"label" => [
								"normal" => ["show" => false , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							]
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2)];
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2),
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}