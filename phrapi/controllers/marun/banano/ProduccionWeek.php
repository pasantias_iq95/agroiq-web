<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionWeek {
	public $name;
	private $db;

	public function __construct(){
		$this->session = Session::getInstance();
        $this->sigat = DB::getInstance("sigat");
        $this->piq = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

    public function getLastWeek(){
        $response = new stdClass;

        $filters = $this->params();
        $response->fincas = $this->db->queryAll("SELECT id, nombre FROM fincas WHERE status_produccion = 'Activo'");

        return $response;
    }

    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			'fecha_inicial' => getValueFrom($postdata, 'fecha_inicial', ''),
			'fecha_final' => getValueFrom($postdata, 'fecha_final', ''),
            'semana' => (int) getValueFrom($postdata, 'semana', 0),
            'year' => (int) getValueFrom($postdata, 'year', 0),
            'hectareas' => (float) getValueFrom($postdata, 'hectareas', 0),
            'var1' => getValueFrom($postdata, 'var1', ''),
            'var2' => getValueFrom($postdata, 'var2', ''),
            'type1' => getValueFrom($postdata, 'type1', ''),
            'type2' => getValueFrom($postdata, 'type2', ''),
            'sector' => getValueFrom($postdata, 'sector', ''),
            'idFinca' => getValueFrom($postdata, 'idFinca', 0, FILTER_SANITIZE_PHRAPI_INT),
        ];
        return $filters;
    }
    
    public function tags(){
        $filters = $this->params();
        $response = $this->db->queryRow("SELECT * FROM produccion_resumen_tags WHERE anio = $filters->year AND id_finca = $filters->idFinca");
        $response->unidad_cajas = $this->piq->queryOne("SELECT unidad_peso_prom_caja FROM companies_tags_resumen_general WHERE id_company = {$this->session->id_company}");
        $response->unidad_racimo = $this->piq->queryOne("SELECT unidad_peso_prom_racimo FROM companies_tags_resumen_general WHERE id_company = {$this->session->id_company}");
        return $response;
    }

    public function changeHectareas(){
        $filters = $this->params();
        $this->db->queryOne("UPDATE fincas SET hectareas = {$filters->hectareas} WHERE id = 1");
        return $filters->hectareas;
    }

    public function graficaEdadPromedio(){
        $response = new stdClass;
        $filters = $this->params();

        $sWhere = "";
        if($filters->sector != ''){
            $lotes = $this->db->queryOne("SELECT GROUP_CONCAT(CONCAT(\"'\",nombre,\"'\") SEPARATOR ',') FROM lotes WHERE sector = '{$filters->sector}'");
            $sWhere .= " AND lote IN ($lotes)";
        }

        $response->chart = new stdClass;
        $response->chart->legend = $this->db->queryAllOne("SELECT semana FROM racimo_web WHERE anio = '{$filters->year}' {$sWhere} GROUP BY semana ORDER BY semana");
        $response->chart->umbral = $this->db->queryOne("SELECT ROUND(AVG(edad), 2) FROM racimo_web WHERE anio = '{$filters->year}' {$sWhere}");
        $data_series = $this->db->queryAll("SELECT lote, semana, ROUND(AVG(edad), 2) as edad FROM racimo_web WHERE anio = $filters->year {$sWhere} GROUP BY lote, semana ORDER BY semana");
        $semanas = [];
        foreach($data_series as $row){
            if(!isset($response->chart->data[$row->lote])){
                $response->chart->data[$row->lote] = [
                    "connectNulls" => true,
                    "itemStyle" => [
                        "normal" => [
                            "barBorderRadius" => "0",
                            "barBorderWidth" => "6",
                            "label" => [
                                "show" => false,
                                "position" => "insideTop"
                            ]
                        ]
                    ],
                    "type" => "line",
                    "name" => $row->lote,
                    "data" => []
                ];
            }
            $response->chart->data[$row->lote]["data"][] = $row->edad;
        }
        return $response;
    }

    public function reporteProduccion(){
        $response = new stdClass;
        $filters = $this->params();

        $sql = "SHOW COLUMNS
                FROM `produccion_resumen_tabla`
                WHERE FIELD LIKE 'sem_%'";
        $columns = $this->db->queryAll($sql);

        $semanas = [];
        $semanas_fields = "";
        foreach($columns as $row){
            $hasData = $this->db->queryOne("SELECT COUNT(1) FROM produccion_resumen_tabla WHERE $row->Field > 0 AND anio = $filters->year");
            if($hasData){
                $semanas[] = str_replace("sem_", "", $row->Field);
                $semanas_fields .= ", {$row->Field}";
            }
        }

        $sql = "SELECT promedio AS 'avg', alias AS campo, maximo AS 'max', minimo AS 'min', suma AS 'sum' {$semanas_fields}
                FROM produccion_resumen_tabla
                WHERE anio = $filters->year 
                    AND id_finca = $filters->idFinca
                    AND status = 'Activo'
                ORDER BY orden";
        $table = $this->db->queryAll($sql);
        $response->data = $table;
        $response->semanas = $semanas;
        
        return $response;
    }

    public function graficaVariables(){
        $response = new stdClass;
        $filters = $this->params();
        
        $sWhere = "";
        if(isset($filters->finca) && $filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca ";
        }

        $variables = [
            "Peso" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Peso' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'PESO RAC PROM (KG)' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Manos" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Manos' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'MANOS PROM' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Calibre" => "SELECT semana AS label_x, ROUND(AVG(calibre_segunda), 2) AS value, 0 AS index_y, 'Calibre' AS name
                        FROM racimo_web
                        WHERE anio = {$filters->year} AND calibre_segunda > 0 $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Calibre Segunda" => "SELECT semana AS label_x, ROUND(AVG(calibre_segunda), 2) AS value, 0 AS index_y, 'Calibre Segunda' AS name
                        FROM racimo_web
                        WHERE anio = {$filters->year} AND calibre_segunda > 0 $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Calibre Ultima" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Calibre Ultima' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'CALIB ULTIMA PROM' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Edad" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Edad' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'EDAD PROM' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Cajas/Ha" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Cajas/Ha' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'CAJAS/Ha' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Merma Neta" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Merma Neta' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = '% MERMA NETA' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Temp Min." => "SELECT semana AS label_x, temp_minima AS value, 0 AS index_y, 'Temp Min.' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Horas Luz (100)" => "SELECT semana AS label_x, horas_luz_100 AS value, 0 AS index_y, 'Horas Luz (100)' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Horas Luz (150)" => "SELECT semana AS label_x, horas_luz_150 AS value, 0 AS index_y, 'Horas Luz (150)' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Horas Luz (200)" => "SELECT semana AS label_x, horas_luz_200 AS value, 0 AS index_y, 'Horas Luz (200)' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Horas Luz (400)" => "SELECT semana AS label_x, horas_luz_400 AS value, 0 AS index_y, 'Horas Luz (400)' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Racimos Procesados" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Racimos Procesados' AS name
                                    FROM produccion_resumen_semana
                                    WHERE anio = {$filters->year} AND variable = 'RACIMOS PROCESADOS' $sWhere
                                    GROUP BY semana
                                    ORDER BY semana",
            "Racimos Cosechados" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Racimos Cosechados' AS name
                                    FROM produccion_resumen_semana
                                    WHERE anio = {$filters->year} AND variable = 'RACIMOS CORTADOS' $sWhere
                                    GROUP BY semana
                                    ORDER BY semana",
            "Cajas" => "SELECT semana AS label_x, COUNT(1) AS 'value', 0 AS index_y, 'Cajas' AS 'name'
                        FROM produccion_cajas
                        WHERE year = {$filters->year}
                        GROUP BY semana
                        ORDER BY semana",
            "Temp Max." => "SELECT semana AS label_x, temp_maxima AS value, 0 AS index_y, 'Temp Max.' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Lluvia" => "SELECT semana AS label_x, lluvia AS value, 0 AS index_y, 'Lluvia' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Humedad" => "SELECT semana AS label_x, humedad AS value, 0 AS index_y, 'Humedad' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Rad. Solar" => "SELECT semana AS label_x, rad_solar AS value, 0 AS index_y, 'Rad. Solar' AS name FROM datos_clima_resumen WHERE id_hacienda = 14 AND anio = {$filters->year} ORDER BY semana",
            "Apuntalador" => "SELECT WEEK(fecha) AS label_x,
                                ROUND(
                                    (SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma m
                                        INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                        WHERE WEEK(fecha) =  WEEK(merma.fecha)
                                            AND campo LIKE 'Total Peso Merma (Kg)' AND TYPE = 'RESULTADOS'
                                            AND YEAR(fecha) = {$filters->year}
                                            $sWhere) * 100), 2) AS value,
                                0 AS index_y, 'APUNTALADOR' AS name
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE campo LIKE 'Total Peso%' AND TYPE = 'APUNTALADOR' AND YEAR(fecha) = {$filters->year}
                            $sWhere
                        GROUP BY WEEK(fecha)",
            "Cosecha" => "SELECT WEEK(fecha) AS label_x,
                                ROUND(
                                    (SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma m
                                        INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                        WHERE WEEK(fecha) =  WEEK(merma.fecha)
                                            AND campo LIKE 'Total Peso Merma (Kg)' AND TYPE = 'RESULTADOS'
                                            AND YEAR(fecha) = {$filters->year}
                                            $sWhere) * 100), 2) AS value,
                                0 AS index_y, 'COSECHA' AS name
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE campo LIKE 'Total Peso%' AND TYPE = 'COSECHA' AND YEAR(fecha) = {$filters->year}
                            $sWhere
                        GROUP BY WEEK(fecha)",
            "Lotero Aereo" => "SELECT WEEK(fecha) AS label_x,
                                ROUND(
                                    (SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma m
                                        INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                        WHERE WEEK(fecha) =  WEEK(merma.fecha)
                                            AND campo LIKE 'Total Peso Merma (Kg)' AND TYPE = 'RESULTADOS'
                                            AND YEAR(fecha) = {$filters->year}
                                            $sWhere) * 100), 2) AS value,
                                0 AS index_y, 'LOTERO AEREO' AS name
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE campo LIKE 'Total Peso%' AND TYPE = 'LOTERO AEREO' AND YEAR(fecha) = {$filters->year}
                            $sWhere
                        GROUP BY WEEK(fecha)",
            "Empaque" => "SELECT WEEK(fecha) AS label_x,
                                ROUND(
                                    (SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma m
                                        INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                        WHERE WEEK(fecha) =  WEEK(merma.fecha)
                                            AND campo LIKE 'Total Peso Merma (Kg)' AND TYPE = 'RESULTADOS'
                                            AND YEAR(fecha) = {$filters->year}
                                            $sWhere) * 100), 2) AS value,
                                0 AS index_y, 'Empaque' AS name
                        FROM merma
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE campo LIKE 'Total Peso%' AND TYPE = 'EMPAQUE' AND YEAR(fecha) = {$filters->year}
                            $sWhere
                        GROUP BY WEEK(fecha)",
            "Fisiologicos" => "SELECT WEEK(fecha) AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE WEEK(fecha) =  WEEK(merma.fecha)
                                                AND campo LIKE 'Total Peso Merma (Kg)' AND TYPE = 'RESULTADOS'
                                                AND YEAR(fecha) = {$filters->year}
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'FISIOLOGICOS' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso%' AND TYPE = 'FISIOLOGICOS' AND YEAR(fecha) = {$filters->year}
                                $sWhere
                            GROUP BY WEEK(fecha)",
            "Deshoje" => "SELECT WEEK(fecha) AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE WEEK(fecha) =  WEEK(merma.fecha)
                                                AND campo LIKE 'Total Peso Merma (Kg)' AND TYPE = 'RESULTADOS'
                                                AND YEAR(fecha) = {$filters->year}
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'DESHOJE' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso%' AND TYPE = 'DESHOJE' AND YEAR(fecha) = {$filters->year}
                                $sWhere
                            GROUP BY WEEK(fecha)",
            "Administracion" => "SELECT WEEK(fecha) AS label_x,
                                    ROUND(
                                        (SUM(cantidad) / 
                                        (SELECT SUM(cantidad)
                                            FROM merma m
                                            INNER JOIN merma_detalle ON m.id = merma_detalle.id_merma
                                            WHERE WEEK(fecha) =  WEEK(merma.fecha)
                                                AND campo LIKE 'Total Peso Merma (Kg)' AND TYPE = 'RESULTADOS'
                                                AND YEAR(fecha) = {$filters->year}
                                                $sWhere) * 100), 2) AS value,
                                    0 AS index_y, 'ADMINISTRACION' AS name
                            FROM merma
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE campo LIKE 'Total Peso%' AND TYPE = 'ADMINISTRACION' AND YEAR(fecha) = {$filters->year}
                                $sWhere
                            GROUP BY WEEK(fecha)",
            "Ratio Cortado" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Ratio Cortado' AS name
                                FROM produccion_resumen_semana
                                WHERE anio = {$filters->year} AND variable = 'RATIO CORTADO' $sWhere
                                GROUP BY semana
                                ORDER BY semana",
            "Ratio Procesado" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Ratio Procesado' AS name
                                FROM produccion_resumen_semana
                                WHERE anio = {$filters->year} AND variable = 'RATIO PROCESADO' $sWhere
                                GROUP BY semana
                                ORDER BY semana",
            "HVLE (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(hoja_mas_vieja_de_estrias), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' HVLE (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 16
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "Q<5 (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' Q<5 (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 16
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "H3 (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(IF(total_hoja_3 = 100 OR total_hoja_3 = 1, 1, 0)), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' H3 (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 16
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "H4 (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(IF(total_hoja_4 = 100 OR total_hoja_4 = 1, 1, 0)), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' H4 (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 16
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "H5 (3M)" => "SELECT semana AS label_x,
                    ROUND(AVG(IF(total_hoja_5 = 100 OR total_hoja_5 = 1, 1, 0)), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' H5 (3M)') AS 'name'
                FROM muestras_haciendas_3M 
                INNER JOIN muestras_hacienda_detalle_3M ON muestras_haciendas_3M.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas_3M.id_hacienda = 16
                    AND muestras_haciendas_3M.id_usuario = 1
                GROUP BY foco, semana
                ORDER BY semana",
            "HT (0S) " => "SELECT label_x, ROUND(AVG(value), 2) value, index_y, 'HT (0S)' AS 'name'
                FROM (
                    SELECT semana AS label_x,
                        ROUND(AVG(hojas_totales), 2) AS 'value',
                        0 AS index_y, 
                        CONCAT(foco, ' HT (0S)') AS 'name'
                    FROM muestras_haciendas 
                    INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                    WHERE semana >= 0 AND anio = {$filters->year}
                        AND muestras_haciendas.id_hacienda = 16
                        AND muestras_haciendas.id_usuario = 1
                        AND muestras_haciendas.tipo_semana = 0
                    GROUP BY foco, semana
                    ORDER BY semana
                ) tbl
                GROUP BY label_x",
            "Q<5 (0S)" => "SELECT semana AS label_x,
                    ROUND(AVG(hojas_mas_vieja_libre), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' Q<5 (0S)') AS 'name'
                FROM muestras_haciendas 
                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas.id_hacienda = 16
                    AND muestras_haciendas.id_usuario = 1
                    AND muestras_haciendas.tipo_semana = 0
                GROUP BY foco, semana
                ORDER BY semana",
            "HVLE (0S)" => "SELECT semana AS label_x,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' HVLE (0S)') AS 'name'
                FROM muestras_haciendas 
                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas.id_hacienda = 16
                    AND muestras_haciendas.id_usuario = 1
                    AND muestras_haciendas.tipo_semana = 0
                GROUP BY foco, semana
                ORDER BY semana",
            "HT (11S)" => "SELECT label_x, ROUND(AVG(value), 2) value, index_y, 'HT (11S)' AS 'name'
                FROM (
                    SELECT semana AS label_x,
                        ROUND(AVG(hojas_totales), 2) AS 'value',
                        0 AS index_y, 
                        CONCAT(foco, ' HT (11S)') AS 'name'
                    FROM muestras_haciendas 
                    INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                    WHERE semana >= 0 AND anio = {$filters->year}
                        AND muestras_haciendas.id_hacienda = 16
                        AND muestras_haciendas.id_usuario = 1
                        AND muestras_haciendas.tipo_semana = 11
                    GROUP BY foco, semana
                    ORDER BY semana
                ) tbl
                GROUP BY label_x",
            "Q<5 (11S)" => "SELECT semana AS label_x,
                    ROUND(AVG(hoja_mas_vieja_libre_quema_menor), 2) AS 'value',
                    0 AS index_y, 
                    CONCAT(foco, ' Q<5 (11S)') AS 'name'
                FROM muestras_haciendas 
                INNER JOIN muestras_hacienda_detalle ON muestras_haciendas.id = id_Mhacienda
                WHERE semana >= 0 AND anio = {$filters->year}
                    AND muestras_haciendas.id_hacienda = 16
                    AND muestras_haciendas.id_usuario = 1
                    AND muestras_haciendas.tipo_semana = 11
                GROUP BY foco, semana
                ORDER BY semana",
            "FOLIAR" => "",
            "% Tallo" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, '% Tallo' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = '% TALLO' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "% Recusados" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, '% Recusados' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = '% RECUSADOS' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "% Merma Cortada" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, '% Merma Cortada' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'MERMA CORTADA' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "% Merma Procesada" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, '% Merma Procesada' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'MERMA PROCESADA' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "% Recobro" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, '% Recobro' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = '% RECOBRO' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Ratooning" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Ratooning' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'RATOONING' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Enfunde/Ha" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Enfunde/Ha' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'ENFUNDE/HA' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Racimos Cosechados/Ha" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Racimos Cosechados/Ha' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'RACIMOS CORTADOS/HA' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Racimos Procesados/Ha" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Racimos Procesados/Ha' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'RACIMOS PROCESADOS/HA' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
            "Racimos Recusados/Ha" => "SELECT semana AS label_x, ROUND(valor, 2) AS value, 0 AS index_y, 'Racimos Recusados/Ha' AS name
                        FROM produccion_resumen_semana
                        WHERE anio = {$filters->year} AND variable = 'RACIMOS RECUSADOS/HA' $sWhere
                        GROUP BY semana
                        ORDER BY semana",
        ];
        
        $data_chart = [];
        $clima = ["Temp Min.", "Horas Luz (400)", "Horas Luz (100)", "Horas Luz (150)", "Horas Luz (200)", "Temp Max.", "Lluvia", "Humedad", "Rad. Solar"];
        $sigat  = ["HVLE (3M) (GUARUMAL SH)","HVLE (3M) (GUARUMAL PA)","Q<5 (3M) (GUARUMAL SH)","Q<5 (3M) (GUARUMAL PA)","H3 (3M) (GUARUMAL SH)","H3 (3M) (GUARUMAL PA)","H4 (3M) (GUARUMAL SH)","H4 (3M) (GUARUMAL PA)","H5 (3M) (GUARUMAL SH)","H5 (3M) (GUARUMAL PA)","HT (0S) (GUARUMAL SH)","HT (0S) (GUARUMAL PA)","Q<5 (0S) (GUARUMAL SH)","Q<5 (0S) (GUARUMAL PA)","HVLE (0S) (GUARUMAL SH)","HVLE (0S) (GUARUMAL PA)","HT (11S) (GUARUMAL SH)","HT (11S) (GUARUMAL PA)","Q<5 (11S) (GUARUMAL SH)","Q<5 (11S) (GUARUMAL PA)","FOLIAR (GUARUMAL SH)","FOLIAR (GUARUMAL PA)", "HVLE (3M)","Q<5 (3M)","H3 (3M)","H4 (3M)","H5 (3M)","HT (0S)","Q<5 (0S)","HVLE (0S)","HT (11S)","Q<5 (11S)","FOLIAR"];
        if(in_array($filters->var1, ["FOLIAR (GUARUMAL SH)", "FOLIAR (GUARUMAL PA)"])){
            if($filters->var1 == "FOLIAR (GUARUMAL SH)") $data_chart = $this->getEmisionFoliar($filters->year, 40, 0);
            if($filters->var1 == "FOLIAR (GUARUMAL PA)") $data_chart = $this->getEmisionFoliar($filters->year, 41, 0);
        }else{
            if(in_array($filters->var1, $clima) || in_array($filters->var1, $sigat)){
                $data_chart = $this->sigat->queryAll($variables[$filters->var1]);
            }else{
                $data_chart = $this->db->queryAll($variables[$filters->var1]);
            }
        }

        if(in_array($filters->var2, ["FOLIAR (GUARUMAL SH)", "FOLIAR (GUARUMAL PA)"])){
            if($filters->var2 == "FOLIAR (GUARUMAL SH)") $data_chart = array_merge($data_chart, $this->getEmisionFoliar($filters->year, 40, 1));
            if($filters->var2 == "FOLIAR (GUARUMAL PA)") $data_chart = array_merge($data_chart, $this->getEmisionFoliar($filters->year, 41, 1));
        }else{
            $sql = $variables[$filters->var2];
            $sql = str_replace("0 AS index_y", "1 AS index_y", $sql);
            if(in_array($filters->var2, $clima) || in_array($filters->var2, $sigat)){
                $data_chart = array_merge($data_chart, $this->sigat->queryAll($sql));
            }else{
                $data_chart = array_merge($data_chart, $this->db->queryAll($sql));
            }
        }

        $semanas = array();
        foreach ($data_chart as $key => $row)
        {
            $row = (object) $row;
            $semanas[$key] = (int) $row->label_x;
        }
        array_multisort($semanas, SORT_ASC, $data_chart);

        $groups = [
			[
				"name" => $filters->var1,
				"type" => 'line',
				'format' => ''
			],
			[
				"name" => $filters->var2,
				"type" => 'line',
				'format' => ''
			]
        ];

        /*$selected = [
            "Edad" => false,
            "Calibre" => false,
            "Peso" => false,
            "Merma Neta" => false,
            "Racimos Procesados" => false,
            "Racimos Cosechados" => true,
            "Cajas" => false,
            'Temp Min.' => true,
            'Horas Luz (400)' => false
        ];*/
        $selected = [];
        $types = [$filters->type1, $filters->type2];
        
        $response->data = $this->grafica_z($data_chart, $groups, $selected, $types);

        return $response;
    }

    private function getEmisionFoliar($year, $finca, $index){
        $sql_get_weeks = "SELECT WEEK(fecha) AS semana, YEAR(fecha) AS anio , foco 
            FROM foliar 
			WHERE id_hacienda = $finca AND id_usuario  = 1  AND YEAR(fecha) = {$year}
			GROUP BY foco , WEEK(fecha) 
			ORDER BY foco ,YEAR(fecha), WEEK(fecha)";
		$weeks = $this->sigat->queryAll($sql_get_weeks, true);
		$count_foco=0;
		$foco = ['foco' => ''];
		$tabla = array();
		foreach ($weeks as $key => $value) {
			$semana = $weeks[$key]->semana;
			$anio = $weeks[$key]->anio;
			if($foco['foco'] != $weeks[$key]->foco){
				$foco = ['foco' => $weeks[$key]->foco];
				$count_foco=0;
			}


			if($semana == 1){
				$pre_semana = 52;
				$pre_anio = $anio-1;
			}else{
				$pre_semana = $semana-1;
				$pre_anio = $anio;
			}


			if($count_foco > 0){
				$sql = "
					SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10)/division AS valor, foco
					FROM
					(SELECT 
					IF(actual.emision1 > pasada.emision1,(actual.emision1-pasada.emision1)/dif_dias*7,0) AS emision1,
					IF(actual.emision2 > pasada.emision2,(actual.emision2-pasada.emision2)/dif_dias*7,0) AS emision2,
					IF(actual.emision3 > pasada.emision3,(actual.emision3-pasada.emision3)/dif_dias*7,0) AS emision3,
					IF(actual.emision4 > pasada.emision4,(actual.emision4-pasada.emision4)/dif_dias*7,0) AS emision4,
					IF(actual.emision5 > pasada.emision5,(actual.emision5-pasada.emision5)/dif_dias*7,0) AS emision5,
					IF(actual.emision6 > pasada.emision6,(actual.emision6-pasada.emision6)/dif_dias*7,0) AS emision6,
					IF(actual.emision7 > pasada.emision7,(actual.emision7-pasada.emision7)/dif_dias*7,0) AS emision7,
					IF(actual.emision8 > pasada.emision8,(actual.emision8-pasada.emision8)/dif_dias*7,0) AS emision8,
					IF(actual.emision9 > pasada.emision9,(actual.emision9-pasada.emision9)/dif_dias*7,0) AS emision9,
					IF(actual.emision10 > pasada.emision10,(actual.emision10-pasada.emision10)/dif_dias*7,0) AS emision10,
					actual.foco
					FROM (SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."') AS actual
					JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]->semana." AND YEAR(fecha) = $pre_anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."') AS pasada
					JOIN 
						(SELECT DATEDIFF(dia_actual,dia_pasado) AS dif_dias
						FROM
							(SELECT *
							FROM (SELECT MIN(fecha) AS dia_actual FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS actual
							JOIN (SELECT MIN(fecha) AS dia_pasado FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]->semana." AND YEAR(fecha) = $pre_anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."' GROUP BY id_hacienda) AS pasada) AS tbl_dif) tbl_dif_d) AS tabla
					JOIN 
						(SELECT (emision1+emision2+emision3+emision4+emision5+emision6+emision7+emision8+emision9+emision10) AS division 
						FROM
							(SELECT IF(actual.emision1 > pasada.emision1,1,0) AS emision1,IF(actual.emision2 > pasada.emision2,1,0) AS emision2,IF(actual.emision3 > pasada.emision3,1,0) AS emision3,IF(actual.emision4 > pasada.emision4,1,0) AS emision4,IF(actual.emision5 > pasada.emision5,1,0) AS emision5,IF(actual.emision6 > pasada.emision6,1,0) AS emision6,IF(actual.emision7 > pasada.emision7,1,0) AS emision7,IF(actual.emision8 > pasada.emision8,1,0) AS emision8,IF(actual.emision9 > pasada.emision9,1,0) AS emision9,IF(actual.emision10 > pasada.emision10,1,0) AS emision10
							FROM
								(SELECT * FROM foliar WHERE WEEK(fecha) = $semana AND YEAR(fecha) = $anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."') AS actual
								JOIN (SELECT * FROM foliar WHERE WEEK(fecha) = ".$weeks[$key-1]->semana." AND YEAR(fecha) = $pre_anio AND id_hacienda = $finca AND id_usuario  = 1  AND foco = '".$foco["foco"]."') AS pasada)
							AS tbl_suma)
						AS tbl_division
					";
				$result = $this->sigat->queryAll($sql);
                if($result[0]->foco != ""){
                    $data[$result[0]->foco][] = array($semana,round(((double)$result[0]->valor),2));
                    $tabla[$result[0]->foco][$semana] = (double)round(((double)$result[0]->valor),2);
                }
            }
			$count_foco++;
		}
		$response = (object)[];
		$response->table = (object)$tabla;
        
        $data_chart = [];
        $count = 0;
        foreach($data as $key => $value){
            foreach($value as $val){
                if($val[1] > 0){
                    $data_chart[] = ["value" => $val[1], "label_x" => $val[0], "index_y" => $index, "name" => "{$key} FOLIAR (GUARUMAL ".($finca == 40 ? 'SH' : 'PA').")"];
                }else{
                    $data_chart[] = ["value" => NULL, "label_x" => $val[0], "index_y" => $index, "name" => "{$key} FOLIAR (GUARUMAL ".($finca == 40 ? 'SH' : 'PA').")"];
                }
            }
            $count++;
        }
		return $data_chart;
    }

	private function DD($string){
		echo "<pre>";
		D($string);
		echo "</pre>";
	}
    
    private function grafica_z($data = [], $group_y = [], $selected = null, $types = []){
		$options = [];
		$options["tooltip"] = [
			"trigger" => 'axis',
			"axisPointer" => [
				"type" => 'cross',
				"crossStyle" => [
					"color" => '#999'
				]
			]
		];
		$options["toolbox"] = [
			"feature" => [
				"dataView" => [
					"show" => true,
					"readOnly" => false
				],
				"magicType" => [
					"show" => true,
					"type" => ['line', 'bar']
				],
				"restore" => [
					"show" => true
				],
				"saveAsImage" => [
					"show" => true
				]
			]
		];
		$options["legend"]["data"] = [];
		$options["legend"]["bottom"] = "0%";
        $options["legend"]["left"] = "center";
        $options["legend"]["selected"] = $selected;
		$options["xAxis"] = [
			[
				"type" => 'category',
				"data" => [],
				"axisPointer" => [
					"type" => 'shadow'
				]
			]
		];
		/*
			[
				type => 'value',
				name => {String},
				min => 0,
				max => 200,
				interval => 5,
				axisLabel => [
					formatter => {value} KG
				]
			]
		*/
		$options["yAxis"] = [];
		/*
			[
				name => {String},
				type => 'line',
				data => [
					{double}, {double}, {double}
				]
			]
		*/
		$options["series"] = [];

		$maxs = [];
		$mins = [];
		$prepare_data = [];
		$_x = [];
		$_names = [];
		$_namess = [];
		foreach($data as $d){
			$d = (object) $d;
			if(!isset($maxs[$d->index_y])) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;
			if($d->value > $maxs[$d->index_y]) if($d->value > 0)
				$maxs[$d->index_y] = $d->value;

			if(!isset($mins[$d->index_y])) if($d->value > 0)
				$mins[$d->index_y] = $d->value;
			if($d->value < $mins[$d->index_y]) if($d->value > 0)
				$mins[$d->index_y] = $d->value;

			if(!in_array($d->label_x, $_x)){
				$_x[] = $d->label_x;
			}
			if(!in_array($d->name, $_namess)){
				$_namess[] = $d->name;
				 
				$n = ["name" => $d->name, "group" => $d->index_y];
				if(isset($d->line)){
					$n["line"] = $d->line;
				}
				$_names[] = $n;
			}
			$prepare_data[$d->label_x][$d->name] = $d->value;
        }

		foreach($group_y as $key => $col){
			$col = (object) $col;
			$options["yAxis"][] = [
				'type' => 'value',
				'name' => $col->name,
				//'max' => ($key == 1) ? $maxs[$key] + ($maxs[$key] - $mins[$key]) * .05 : null,
                //'min' => ($key == 1) ? $mins[$key] - ($maxs[$key] - $mins[$key]) * .05 : null,
                'max' => null,
                'min' => 'dataMin',
				'axisLabel' => [
					'formatter' => "{value} $col->format"
				]
			];
		}

		foreach($_x as $row){
			$options["xAxis"][0]["data"][] = $row;
        }

		foreach($_names as $i => $name){
			$name = (object) $name;

			if(!in_array($name->name, $options["legend"]["data"]))
				$options["legend"]["data"][] = $name->name;

			$serie = [
				"name" => $name->name,
				"type" => isset($types[$i]) ? $types[$i] : 'line',
				"connectNulls" => true,
				"data" => []
			];
			if($name->group > 0)
				$serie["yAxisIndex"] = $name->group;

			if(isset($name->line)){
				$serie["itemStyle"]["normal"]["lineStyle"]["width"] = 5;
			}

			foreach($_x as $row){
				$val = 0;
				if(isset($prepare_data[$row][$name->name]))
					$val = $prepare_data[$row][$name->name];

				if($val > 0)
					$serie["data"][] = $val;
				else
					$serie["data"][] = null;
			}
			$options["series"][] = $serie;
		}

		return $options;
	}
	
}
