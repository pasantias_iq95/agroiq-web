<?php defined('PHRAPI') or die("Direct access not allowed!");

class CacaoAnalisis {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance('cacao_'.$this->session->agent_user);
    }

    public function last(){
        $response = new stdClass;        
        $row = $this->db->queryRow("SELECT * FROM cacao_analisis ORDER BY id DESC LIMIT 1");        
        $response->row = $row;
        $response->row->imagenes = explode("|", $response->row->fotos);
        $response->data = $this->rowToData($row);        
        return $response;
    }
    
    public function filters(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->fecha != ''){
            $sFecha = " AND fecha = '{$postdata->fecha}'";
        }
        if($postdata->responsable != ''){
            $sResponsable = " AND responsable = '{$postdata->responsable}'";
        }
        if($postdata->finca != ''){
            $sFinca = " AND finca = '{$postdata->finca}'";
        }
        if($postdata->codigo != ''){
            #$sCodigo = " AND codigo = '{$postdata->codigo}'";
        }

        $response->fechas = $this->db->queryAllOne("SELECT fecha FROM cacao_analisis WHERE 1=1 $sResponsable $sFinca $sCodigo GROUP BY fecha");
        $response->responsables = $this->db->queryAllOne("SELECT responsable FROM cacao_analisis WHERE 1=1 $sFecha $sFinca $sCodigo GROUP BY responsable");
        $response->fincas = $this->db->queryAllOne("SELECT finca FROM cacao_analisis WHERE 1=1 $sFecha $sResponsable $sCodigo GROUP BY finca");
        $response->codigos = $this->db->queryAllOne("SELECT codigo FROM cacao_analisis WHERE 1=1 $sFecha $sResponsable $sFinca GROUP BY codigo");
        return $response;
    }

	public function analisis(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $sWhere = "";
        if($postdata->fecha != ''){
            $sWhere .= " AND fecha = '{$postdata->fecha}'";
        }
        if($postdata->codigo != ''){
            $sWhere .= " AND codigo = '{$postdata->codigo}'";
        }
        if($postdata->finca != ''){
            $sWhere .= " AND finca = '{$postdata->finca}'";
        }
        if($postdata->responsable != ''){
            $sWhere .= " AND responsable = '{$postdata->responsable}'";
        }

        $row = $this->db->queryRow("SELECT * FROM cacao_analisis WHERE 1=1 $sWhere");
        $response->imagenes = explode("|", $row->fotos);
        $response->row = $row;
        $response->data = $this->rowToData($row);
        return $response;
    }

    private function rowToData($row){
        $response = new stdClass;
        $response->data = [
            [
                "name" => "Aroma a Cacao",
                "value" => $row->aroma_cacao,
                "max" => 5
            ],
            [
                "name" => "Sabor a Cacao",
                "value" => $row->sabor_cacao,
                "max" => 5
            ],
            [
                "name" => "Floral (dulce, ácidos)",
                "value" => $row->floral_dulce_acidos,
                "max" => 5
            ],
            [
                "name" => "Floral (nueces, almendras)",
                "value" => $row->floral_nueces_almendras,
                "max" => 5
            ],
            [
                "name" => "Astringencia",
                "value" => $row->astringencia,
                "max" => 5
            ],
            [
                "name" => "Acidez",
                "value" => $row->acidez,
                "max" => 5
            ],
            [
                "name" => "Amargor",
                "value" => $row->amargor,
                "max" => 5
            ]
        ];
        return $response->data;
    }
}
