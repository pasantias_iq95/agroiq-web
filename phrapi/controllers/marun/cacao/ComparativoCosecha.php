<?php defined('PHRAPI') or die("Direct access not allowed!");

class ComparativoCosecha {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance('cacao_'.$this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $sql = "SELECT TIME(fecha) hora, REPLACE(sector, 'C', '') AS sector, lote, codigo, SUM(peso) peso, COUNT(1) mallas
                FROM balanza_agrosoft_cacao
                WHERE DATE(fecha) = '{$postdata->fecha}'
                GROUP BY sector, lote, codigo
                ORDER BY codigo";
        $response->balanza = $this->db->queryAll($sql);

        $sql = "SELECT TIME(hora) hora, sector, lote, codigo, total_peso, mallas_entregadas mallas
                FROM reporte_cosecha
                WHERE fecha = '{$postdata->fecha}'
                ORDER BY codigo";
        $response->formularios = $this->db->queryAll($sql);

        return $response;
    }
}
