<?php defined('PHRAPI') or die("Direct access not allowed!");

class ConsolidadoCosecha {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance('cacao_'.$this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $formula = [
            "KG" => '',
            "LB" => '/ 0.4536',
            "QQ" => '/ 0.4536 / 100'
        ];

        $sql = "SELECT 
                    a.fecha,
                    IF(a.codigo IN (500, 501, 502, 503, 504),
                        'N/A',
                        (
                            SELECT p.cedula 
                            FROM cacao_asignacion_codigos_mallas c 
                            INNER JOIN personal p ON cosechador = p.nombre 
                            WHERE fecha = DATE(a.fecha) 
                                AND c.codigo = a.codigo
                            LIMIT 1
                        )
                    ) AS cedula,
                    IF(a.codigo IN (500, 501, 502, 503, 504),
                        (
                            SELECT cosechador
                            FROM codigos_fijos
                            WHERE codigo = a.codigo
                        ),
                        (
                            SELECT cosechador 
                            FROM cacao_asignacion_codigos_mallas 
                            WHERE fecha = DATE(a.fecha) 
                                AND codigo = a.codigo 
                            LIMIT 1
                        )
                    ) AS cosechador,
                    a.lote,
                    a.sector,
                    a.codigo,
                    IF(a.codigo IN (500, 501, 502, 503, 504),
                        'N/A',
                        IFNULL((SELECT ROUND(cantidad, 0) FROM cacao_asignacion_codigos_mallas c WHERE fecha = DATE(a.fecha) AND codigo = a.codigo LIMIT 1), 0)
                    ) AS entregadas,
                    (SELECT COUNT(1) FROM `balanza_agrosoft_cacao` WHERE fecha = a.fecha AND codigo = a.codigo) AS recibidas,
                    (SELECT ROUND(SUM(peso) {$formula[$postdata->unidad]}, 2) FROM `balanza_agrosoft_cacao` WHERE fecha = a.fecha AND codigo = a.codigo) AS peso
                FROM (
                    SELECT *
                    FROM (
                        SELECT fecha, lote, sector, codigo
                        FROM asignacion_area a
                        WHERE a.fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                        GROUP BY a.fecha, a.lote, a.sector, a.codigo
                        UNION ALL
                        SELECT DATE(fecha), '' lote, '' sector, codigo
                        FROM balanza_agrosoft_cacao
                        WHERE fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'
                        GROUP BY DATE(fecha), codigo
                    ) a
                    GROUP BY fecha, codigo
                ) a";
        $response->data = $this->db->queryAll($sql);

        $total = new stdClass;
        $total->fecha = '';
        $total->cosechador = '';
        $total->cedula = 'TOTAL';
        $total->sector = '';
        $total->lote = '';
        $total->codigo = '';
        foreach($response->data as $row){
            $row->saldo = $row->cedula != 'N/A' ? ((float) $row->entregadas) - ((float) $row->recibidas) : 'N/A';

            $total->recibidas = ((float) $total->recibidas) + ((float) $row->recibidas);
            $total->entregadas = ((float) $total->entregadas) + ((float) $row->entregadas);
            $total->peso = ((float) $total->peso) + ((float) $row->peso);
            $total->saldo = ($row->saldo > 0) ? ((float) $total->saldo) + ((float) $row->saldo) : 0;
        }
        $response->data[] = $total;

        return $response;
    }

    public function save(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->id > 0 && $postdata->campo != '' && $postdata->valor != ''){
            $this->db->query("UPDATE reporte_cosecha SET {$postdata->campo} = '{$postdata->valor}' WHERE id = {$postdata->id}");
            $response->status = 200;
        }else{
            $response->status = 400;
        }
        return $response;
    }
}
