<?php defined('PHRAPI') or die("Direct access not allowed!");

class ReporteCosecha extends ReactGrafica {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance('cacao_'.$this->session->agent_user);
    }

    public function index(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if(isset($postdata->sector) && $postdata->sector != ''){
            $sWhere .= " AND REPLACE(sector, 'C', '') = '{$postdata->sector}'";
        }

        $response->sectores = $this->db->queryAllOne("SELECT REPLACE(sector, 'C', '') FROM reporteCosechaBalanza WHERE year = YEAR(CURRENT_DATE) AND sector != '' GROUP BY REPLACE(sector, 'C', '') ORDER BY sector");
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM reporteCosechaBalanza WHERE year = YEAR(CURRENT_DATE) $sWhere GROUP BY semana ORDER BY semana");
        $formula = [
            "KG" => '',
            "LB" => '/ 0.4536',
            "QQ" => '/ 0.4536 / 100'
        ];

        $sql = "SELECT lote, -1 AS 'min', -1 AS 'max', 0 AS 'sum', 0 AS 'count'
                FROM reporteCosechaBalanza
                WHERE year = YEAR(CURRENT_DATE) AND lote != '' $sWhere
                GROUP BY lote
                ORDER BY CAST(lote AS DECIMAL)";
        $response->data = $this->db->queryAll($sql);
        $total = (object)["lote" => "SAN JOSE", "sum" => 0, "count" => 0, "max" => -1, "min" => -1];
        foreach($response->data as $row){
            foreach($response->semanas as $sem){
                $row->{"sem_$sem"} = (float) $this->db->queryOne("SELECT ROUND(SUM(peso {$formula[$postdata->unidad]}), 2) FROM reporteCosechaBalanza WHERE year = YEAR(CURRENT_DATE) AND semana = $sem AND lote = '{$row->lote}'");

                // MAX, MIN, AVG, SUM
                if($row->{"sem_$sem"} > 0){
                    if($row->max == -1 || $row->max < $row->{"sem_$sem"}) $row->max = $row->{"sem_$sem"};
                    if($row->min == -1 || $row->min > $row->{"sem_$sem"}) $row->min = $row->{"sem_$sem"};
                    $row->sum += $row->{"sem_$sem"};
                    $row->count++;
                    $row->avg = round($row->sum/$row->count, 2);

                    // TOTAL
                    $total->{"sem_$sem"} += $row->{"sem_$sem"};
                    if($total->max == -1 || $total->max < $total->{"sem_$sem"}) $total->max = $total->{"sem_$sem"};
                    if($total->min == -1 || $total->min > $total->{"sem_$sem"}) $total->min = $total->{"sem_$sem"};
                    $total->sum += $row->{"sem_$sem"};
                }
            }
        }
        $total->avg = round($total->sum / count($response->semanas), 2);
        $response->data[] = $total;

        // NUMBER FORMAT
        foreach($response->data as $row){
            if($row->sum > 0) $row->sum = number_format($row->sum, 2);
            if($row->avg > 0) $row->avg = number_format($row->avg, 2);
            if($row->min > 0) $row->min = number_format($row->min, 2);
            if($row->max > 0) $row->max = number_format($row->max, 2);
            foreach($response->semanas as $sem){
                if($row->{"sem_$sem"} > 0) $row->{"sem_$sem"} = number_format($row->{"sem_$sem"}, 2);
                else $row->{"sem_$sem"} = '';
            }
        }

        return $response;
    }

    public function chart(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if(isset($postdata->sector) && $postdata->sector != ''){
            $sWhere .= " AND sector = '{$postdata->sector}'";
        }

        $formula = [
            "KG" => '',
            "LB" => '/ 0.4536',
            "QQ" => '/ 0.4536 / 100'
        ];
        $sql = "SELECT 
                        semanas.semana AS legend, 
                        lotes.lote AS label,
                        peso AS value,
                        TRUE AS selected
                    FROM (
                        SELECT semana
                        FROM reporteCosechaBalanza
                        WHERE year = YEAR(CURRENT_DATE) $sWhere
                        GROUP BY semana
                    ) semanas
                    JOIN (
                        SELECT lote
                        FROM reporteCosechaBalanza
                        WHERE year = YEAR(CURRENT_DATE) $sWhere
                        GROUP BY lote
                        UNION ALL
                        SELECT 'SAN JOSE' AS lote
                    ) lotes
                    LEFT JOIN (
                        SELECT lote, semana, ROUND(SUM(peso {$formula[$postdata->unidad]}), 2) as peso
                        FROM reporteCosechaBalanza
                        WHERE year = YEAR(CURRENT_DATE) $sWhere
                        GROUP BY lote, semana
                        UNION ALL
                        SELECT 'SAN JOSE' lote, semana, ROUND(AVG(peso {$formula[$postdata->unidad]}), 2) as peso
                        FROM reporteCosechaBalanza
                        WHERE year = YEAR(CURRENT_DATE) $sWhere
                        GROUP BY semana
                    ) tbl ON semanas.semana = tbl.semana AND lotes.lote = tbl.lote
                    ORDER BY lotes.lote, semanas.semana";
        $data = $this->db->queryAll($sql);
        $data_chart = new stdClass;
		$data_chart->data = [];
		$data_chart->legend = [];

		foreach($data as $i => $row){
			if(!isset($data_chart->data[$row->label])){
				$data_chart->data[$row->label] = [
					"connectNulls" => true,
					"data" => [],
					"name" => $row->label,
					"type" => 'line',
					'itemStyle' => [
						"normal" => [
							"barBorderRadius" => "0",
							"barBorderWidth" => "6",
							"label" => [
								"show" => false,
								"position" => "insideTop"
							]
						]
					]
                ];
            }

            $data_chart->data[$row->label]["data"][] = $row->value;
			if(!in_array($row->legend, $data_chart->legend)){
				$data_chart->legend[] = $row->legend;
			}
        }
        $response->data = $data_chart;

        $sql = "SELECT ROUND(AVG(peso), 2)
                FROM (
                    SELECT AVG(peso {$formula[$postdata->unidad]}) AS peso
                    FROM reporteCosechaBalanza
                    WHERE year = YEAR(CURRENT_DATE) $sWhere
                    GROUP BY semana
                ) tbl";
        $response->data->umbral = (float) $this->db->queryOne($sql);
        return $response;
    }

    public function save(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->id > 0 && $postdata->campo != '' && $postdata->valor != ''){
            $this->db->query("UPDATE reporte_cosecha SET {$postdata->campo} = '{$postdata->valor}' WHERE id = {$postdata->id}");
            $response->status = 200;
        }else{
            $response->status = 400;
        }
        return $response;
    }
}
