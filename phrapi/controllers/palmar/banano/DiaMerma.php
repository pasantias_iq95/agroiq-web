<?php defined('PHRAPI') or die("Direct access not allowed!");

class DiaMerma {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function last(){
		$response = new stdClass;

		$response->days = $this->db->queryAllOne("SELECT DISTINCT date_fecha FROM merma");
        $response->categorias = $this->db->queryAllOne("SELECT var FROM (
                SELECT 'COSECHA' AS var, COUNT(1) AS cant
                FROM merma
                WHERE date_fecha = CURRENT_DATE AND porcentaje_cosecha > 0
                UNION ALL
                SELECT 'ENFUNDE' AS var, COUNT(1)
                FROM merma
                WHERE date_fecha = CURRENT_DATE AND porcentaje_enfunde > 0
            ) AS tbl
            WHERE cant > 0");
        return $response;
	}
	
	public function index(){
		$response = new stdClass;
		$response->viajes = [];

		$merma = $this->merma();
		$response->merma = $merma->data;
		for($x = 1; $x <= $merma->viajes; $x++) $response->viajes[] = $x;

		$response->defectos = []; #$this->defectos();
		$response->pesos = $this->pesos();
		return $response;
	}

	private function merma(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$response->data = $this->db->queryAll("SELECT palanca FROM merma WHERE date_fecha = '{$postdata->fecha_inicial}' GROUP BY palanca");
		$response->viajes = 0;
		foreach($response->data as $palanca){
			$sql = "SELECT 
						m.id, 
						TIME(fecha) AS hora, 
						ROUND(cantidad/peso_neto*100, 2) AS merma,
						cantidad,
						peso_neto
					FROM merma m
					INNER JOIN merma_detalle d ON id_merma = m.id
					WHERE date_fecha = '{$postdata->fecha_inicial}'
						AND palanca = '{$palanca->palanca}'
						AND UPPER(campo) = 'TOTAL PESO MERMA DE {$postdata->categoria}'
					ORDER BY fecha";
			$palanca->viajes = $this->db->queryAll($sql);
			if($response->viajes < count($palanca->viajes)) $response->viajes = count($palanca->viajes);

			$palanca->defectos = $this->db->queryAll("SELECT campo, SUM(cantidad) cantidad FROM merma m INNER JOIN merma_detalle d ON id_merma = m.id WHERE date_fecha = '{$postdata->fecha_inicial}' AND palanca = '{$palanca->palanca}' AND type = '{$postdata->categoria}' AND flag = 1 GROUP BY campo ORDER BY campo");
			foreach($palanca->defectos as $d){
				$sql = "SELECT ROUND(SUM(cantidad)/peso_neto*100, 2) cant
						FROM merma m
						LEFT JOIN merma_detalle d ON m.id = id_merma AND campo = '{$d->campo}' AND flag = 1
						WHERE date_fecha = '{$postdata->fecha_inicial}' AND palanca = '{$palanca->palanca}'
						GROUP BY m.id
						ORDER BY fecha";
				$d->viajes = $this->db->queryAll($sql);
			}
		}

		return $response;
	}

	private function defectos(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$response = $this->db->queryAll("SELECT palanca FROM merma WHERE date_fecha = '{$postdata->fecha_inicial}' GROUP BY palanca");
		foreach($response as $palanca){
			$sql = "SELECT 
						m.id, 
						TIME(fecha) AS hora, 
						cantidad AS defectos
					FROM merma m
					INNER JOIN merma_detalle d ON id_merma = m.id
					WHERE date_fecha = '{$postdata->fecha_inicial}'
						AND palanca = '{$palanca->palanca}'
						AND UPPER(campo) = 'TOTAL MERMA DE {$postdata->categoria} (# DAÑOS)'
					ORDER BY fecha";
			$palanca->viajes = $this->db->queryAll($sql);
			if($response->viajes < count($palanca->viajes)) $response->viajes = count($palanca->viajes);

			$palanca->defectos = $this->db->queryAll("SELECT campo, SUM(cantidad) cantidad FROM merma m INNER JOIN merma_detalle d ON id_merma = m.id WHERE date_fecha = '{$postdata->fecha_inicial}' AND palanca = '{$palanca->palanca}' AND type = '{$postdata->categoria}' AND flag = 1 GROUP BY campo ORDER BY campo");
			foreach($palanca->defectos as $d){
				$sql = "SELECT ROUND(SUM(cantidad)/peso_neto*100, 2) cant
						FROM merma m
						LEFT JOIN merma_detalle d ON m.id = id_merma AND campo = '{$d->campo}' AND flag = 2
						WHERE date_fecha = '{$postdata->fecha_inicial}' AND palanca = '{$palanca->palanca}'
						GROUP BY m.id
						ORDER BY fecha";
				$d->viajes = $this->db->queryAll($sql);
			}
		}

		return $response;
	}

	private function pesos(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$response = $this->db->queryAll("SELECT palanca FROM merma WHERE date_fecha = '{$postdata->fecha_inicial}' GROUP BY palanca");
		foreach($response as $palanca){
			$sql = "SELECT 
						m.id, 
						TIME(fecha) AS hora, 
						cantidad AS peso
					FROM merma m
					INNER JOIN merma_detalle d ON id_merma = m.id
					WHERE date_fecha = '{$postdata->fecha_inicial}'
						AND palanca = '{$palanca->palanca}'
						AND UPPER(campo) = 'TOTAL PESO MERMA DE {$postdata->categoria}'
					ORDER BY fecha";
			$palanca->viajes = $this->db->queryAll($sql);
			if($response->viajes < count($palanca->viajes)) $response->viajes = count($palanca->viajes);

			$palanca->defectos = $this->db->queryAll("SELECT campo, SUM(cantidad) cantidad FROM merma m INNER JOIN merma_detalle d ON id_merma = m.id WHERE date_fecha = '{$postdata->fecha_inicial}' AND palanca = '{$palanca->palanca}' AND type = '{$postdata->categoria}' AND flag = 1 GROUP BY campo ORDER BY campo");
			foreach($palanca->defectos as $d){
				$sql = "SELECT ROUND(SUM(cantidad), 2) cant
						FROM merma m
						LEFT JOIN merma_detalle d ON m.id = id_merma AND campo = '{$d->campo}' AND flag = 1
						WHERE date_fecha = '{$postdata->fecha_inicial}' AND palanca = '{$palanca->palanca}'
						GROUP BY m.id
						ORDER BY fecha";
				$d->viajes = $this->db->queryAll($sql);
			}
		}

		return $response;
	}
}