<?php defined('PHRAPI') or die("Direct access not allowed!");

class Merma {
	public $name;
	private $db;
	private $MermaTypePeso;
	private $config;
	private $search;
	private $cajas;
	private $usd;
	private $factor;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
		// F($this->session);
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->search = "Total Merma";
        $this->cajas = "18.86";
        $this->usd = "6.16";
        $this->factor = 1;
        if($this->session->id_company == 2){
        	$this->search = "Total Daños";
        }
        if($this->session->id_company == 7){
        	$this->search = "Total Merma";
        	$this->cajas = "18.86";
        	$this->usd = "6.16";
        	$this->factor = 5;
        }

        $this->MermaTypePeso = "Kg";
	}

	# BEGIN TABLA VARIABLES
    public function tablaVariables(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;

		$cliente = 'AGRICOLA SEBASTIAN';
		if($this->session->id_company == 11){
			$cliente = 'SUMIFRU';
		}

		$sql = "SELECT *
				FROM(
					SELECT
						semanas.semana,
						lotes.lote,
						tbl.cantidad
					FROM (
						SELECT semana
						FROM merma
						WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
						GROUP BY semana
					) semanas
					JOIN (
						SELECT bloque AS lote
						FROM merma
						WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
						GROUP BY lote
						ORDER BY lote+0
					) lotes
					LEFT JOIN (
						SELECT bloque AS lote, semana, ROUND(AVG(tallo/peso_racimos*100), 2) cantidad
						FROM merma 
						WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
						GROUP BY semana, bloque
					) tbl ON tbl.semana = semanas.semana AND tbl.lote = lotes.lote
					UNION ALL
					SELECT semana, '{$cliente}' AS lote, ROUND(AVG(cantidad), 2) AS cantidad
					FROM (
						SELECT
							semanas.semana,
							lotes.lote,
							tbl.cantidad
						FROM (
							SELECT semana
							FROM merma
							WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY semana
						) semanas
						JOIN (
							SELECT bloque AS lote
							FROM merma
							WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY lote
						) lotes
						LEFT JOIN (
							SELECT bloque AS lote, semana, ROUND(AVG(tallo/peso_racimos*100), 2) cantidad
							FROM merma 
							WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY semana, bloque
						) tbl ON tbl.semana = semanas.semana AND tbl.lote = lotes.lote
						ORDER BY lotes.lote, semanas.semana
					) tbl
					GROUP BY semana
				) tbl
				ORDER BY lote+0, semana";

		$response->data = $this->db->queryAll($sql);
		
		$data_chart = new stdClass;
		$data_table = [];
		$data_chart->data = [];
		$data_chart->legend = [];
		$umbral = ["count" => 0, "sum" => 0];
		foreach($response->data as $row){
			if(!isset($data_chart->data[$row->lote])){
				$data_chart->data[$row->lote] = [
					"connectNulls" => true,
					"data" => [],
					"name" => $row->lote,
					"type" => 'line',
					'itemStyle' => [
						"normal" => [
							"barBorderRadius" => "0",
							"barBorderWidth" => "6",
							"label" => [
								"show" => false,
								"position" => "insideTop"
							]
						]
					]
				];
			}

			if($row->lote == $cliente){
				$umbral["count"] += 1;
				$umbral["sum"] += $row->cantidad;
			}
			$data_chart->data[$row->lote]["data"][] = $row->cantidad;
			if(!in_array($row->semana, $data_chart->legend)){
				$data_chart->legend[] = $row->semana;
			}

			if($row->lote != $cliente){
				if(!in_array($row->lote, $data_table_lotes)){
					$data_table_lotes[] = $row->lote;
					$data_table[] = ["lote" => $row->lote];
				}
				$indexLote = array_search($row->lote, $data_table_lotes);
				$data_table[$indexLote]["sem_{$row->semana}"] = $row->cantidad;

				if($row->cantidad > 0){
					$data_table[$indexLote]["sum"] += $row->cantidad;
					$data_table[$indexLote]["count"] += 1;
					$data_table[$indexLote]["avg"] = round($data_table[$indexLote]["sum"] / $data_table[$indexLote]["count"], 2);
					if(!isset($data_table[$indexLote]["max"]) || $data_table[$indexLote]["max"] < $row->cantidad) $data_table[$indexLote]["max"] = $row->cantidad;
					if(!isset($data_table[$indexLote]["min"]) || $data_table[$indexLote]["min"] > $row->cantidad) $data_table[$indexLote]["min"] = $row->cantidad;
				}
			}
		}

		foreach($response->data as $row){
			if($row->lote == $cliente){
				if(!in_array($row->lote, $data_table_lotes)){
					$data_table_lotes[] = $row->lote;
					$data_table[] = ["lote" => $row->lote];
				}
				$indexLote = array_search($row->lote, $data_table_lotes);
				$data_table[$indexLote]["sem_{$row->semana}"] = $row->cantidad;

				if($row->cantidad > 0){
					$data_table[$indexLote]["sum"] += $row->cantidad;
					$data_table[$indexLote]["count"] += 1;
					$data_table[$indexLote]["avg"] = round($data_table[$indexLote]["sum"] / $data_table[$indexLote]["count"], 2);
					if(!isset($data_table[$indexLote]["max"]) || $data_table[$indexLote]["max"] < $row->cantidad) $data_table[$indexLote]["max"] = $row->cantidad;
					if(!isset($data_table[$indexLote]["min"]) || $data_table[$indexLote]["min"] > $row->cantidad) $data_table[$indexLote]["min"] = $row->cantidad;
				}
			}
		}
		$response->umbral = round($umbral["sum"]/$umbral["count"], 2);
		$response->chart = $data_chart;
		$response->datatable = $data_table;
		return $response;
	}

	#END TABLA VARIABLES
	
	public function tallo(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$response = new stdClass;

		$sWhere = "";
		if($postdata->idFinca != ""){
			if($postdata->idFinca == "9999999"){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$postdata->idFinca}";
			}
		}

		$sql = "SELECT *, 0 AS 'sum', 0 AS 'count'
				FROM(
					SELECT
						semanas.semana,
						lotes.lote,
						tbl.cantidad
					FROM (
						SELECT semana
						FROM merma
						LEFT JOIN lotes ON lotes.nombre = bloque
						WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0' $sWhere
						GROUP BY semana
					) semanas
					JOIN (
						SELECT bloque AS lote
						FROM merma
						LEFT JOIN lotes ON lotes.nombre = bloque
						WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0' $sWhere
						GROUP BY lote
					) lotes
					LEFT JOIN (
						SELECT bloque AS lote, semana, ROUND(AVG(tallo/racimo*100), 2) cantidad
						FROM merma 
						LEFT JOIN lotes ON lotes.nombre = bloque
						WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0' $sWhere
						GROUP BY semana, bloque
						ORDER BY bloque+0
					) tbl ON tbl.semana = semanas.semana AND tbl.lote = lotes.lote

					UNION ALL
					SELECT semana, 'AGRICOLA SEBASTIAN' AS lote, ROUND(AVG(cantidad), 2) AS cantidad
					FROM (
						SELECT
							semanas.semana,
							lotes.lote,
							tbl.cantidad
						FROM (
							SELECT semana
							FROM merma
							WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY semana
						) semanas
						JOIN (
							SELECT bloque AS lote
							FROM merma
							WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY lote
						) lotes
						LEFT JOIN (
							SELECT bloque AS lote, semana, ROUND(AVG(tallo/racimo*100), 2) cantidad
							FROM merma 
							WHERE YEAR = $postdata->year AND porcentaje_merma > 0 AND bloque != '0'
							GROUP BY semana, bloque
						) tbl ON tbl.semana = semanas.semana AND tbl.lote = lotes.lote
						ORDER BY lotes.lote, semanas.semana
					) tbl
					GROUP BY semana
				) tbl";

		$response->data = $this->db->queryAll($sql);

		foreach($response->data as $row){
			if($row->value > 0){
				$row->sum += $row->value;
				$row->count += 1;
				if(!isset($row->max) || $row->max < $row->value) $row->max = $row->value;
				if(!isset($row->min) || $row->min > $row->value) $row->min = $row->value;
			}
		}
		
		$data_chart = new stdClass;
		$data_table = [];
		$data_table_lotes = [];
		$data_chart->data = [];
		$data_chart->legend = [];
		$umbral = ["count" => 0, "sum" => 0];
		foreach($response->data as $row){
			if(!isset($data_chart->data[$row->lote])){
				$data_chart->data[$row->lote] = [
					"connectNulls" => true,
					"data" => [],
					"name" => $row->lote,
					"type" => 'line',
					'itemStyle' => [
						"normal" => [
							"barBorderRadius" => "0",
							"barBorderWidth" => "6",
							"label" => [
								"show" => false,
								"position" => "insideTop"
							]
						]
					]
				];
			}

			if($row->lote == 'AGRICOLA SEBASTIAN'){
				$umbral["count"] += 1;
				$umbral["sum"] += $row->cantidad;
			}
			$data_chart->data[$row->lote]["data"][] = $row->cantidad;
			if(!in_array($row->semana, $data_chart->legend)){
				$data_chart->legend[] = $row->semana;
			}

			if($row->lote != 'AGRICOLA SEBASTIAN'){
				if(!in_array($row->lote, $data_table_lotes)){
					$data_table_lotes[] = $row->lote;
					$data_table[] = ["lote" => $row->lote];
				}
				$indexLote = array_search($row->lote, $data_table_lotes);
				$data_table[$indexLote]["sem_{$row->semana}"] = $row->cantidad;

				if($row->cantidad > 0){
					$data_table[$indexLote]["sum"] += $row->cantidad;
					$data_table[$indexLote]["count"] += 1;
					$data_table[$indexLote]["avg"] = round($data_table[$indexLote]["sum"] / $data_table[$indexLote]["count"], 2);
					if(!isset($data_table[$indexLote]["max"]) || $data_table[$indexLote]["max"] < $row->cantidad) $data_table[$indexLote]["max"] = $row->cantidad;
					if(!isset($data_table[$indexLote]["min"]) || $data_table[$indexLote]["min"] > $row->cantidad) $data_table[$indexLote]["min"] = $row->cantidad;
				}
			}
		}
		foreach($response->data as $row){
			if($row->lote == 'AGRICOLA SEBASTIAN'){
				if(!in_array($row->lote, $data_table_lotes)){
					$data_table_lotes[] = $row->lote;
					$data_table[] = ["lote" => $row->lote];
				}
				$indexLote = array_search($row->lote, $data_table_lotes);
				$data_table[$indexLote]["sem_{$row->semana}"] = $row->cantidad;

				if($row->cantidad > 0){
					$data_table[$indexLote]["sum"] += $row->cantidad;
					$data_table[$indexLote]["count"] += 1;
					$data_table[$indexLote]["avg"] = round($data_table[$indexLote]["sum"] / $data_table[$indexLote]["count"], 2);
					if(!isset($data_table[$indexLote]["max"]) || $data_table[$indexLote]["max"] < $row->cantidad) $data_table[$indexLote]["max"] = $row->cantidad;
					if(!isset($data_table[$indexLote]["min"]) || $data_table[$indexLote]["min"] > $row->cantidad) $data_table[$indexLote]["min"] = $row->cantidad;
				}
			}
		}
		$response->umbral = round($umbral["sum"]/$umbral["count"], 2);
		$response->chart = $data_chart;
		$response->datatable = $data_table;
		return $response;
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}

		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}

		if(isset($postdata->idFincaDia)){
			$id_fincaDia = $postdata->idFincaDia;
		}

		if(isset($postdata->statusLbKg) && $postdata->statusLbKg == 1){
			$this->MermaTypePeso = "Kg";
		}else{
			$this->MermaTypePeso = "Lb";
		}

		if(isset($postdata->year)){
			$year = $postdata->year;
		}else{
			$year = 2016;
		}
        $response = new stdClass();
		$response->id_company = (int)$this->session->id_company;
		$response->tags = $this->tags($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);
		$response->tabla_lote_merma = $this->tabla_lote_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);

		// Cargamos la data una sola ves 
		$danos = $this->tabla_danos_merma($postdata->fecha_inicial, $postdata->fecha_final , $palanca , $id_finca , $postdata);

		// Cargamos la data del historico una sola ves 
		$historico = $this->grafica_historico($palanca , $id_finca , $year);

		// Cargamos la data de tendencia una sola ves 
		$tendencia = $this->tendencia($palanca , $id_finca , $postdata);
		// Cargamos la data Grafica Dia
		$dia = $this->grafica_dia($palanca , $id_finca , $id_fincaDia , $postdata);
		// Asignamos la data 'Tabla'
		#$response->tabla_danos_merma = [];
		$response->tabla_danos_merma = $danos->data;
		// Asignamos la data 'Tabla' Daños
		//$response->tabla_danos_merma_danhos_merma = [];
		$response->tabla_danos_merma_danhos_merma = $danos->danhos_merma;
		// Asignamos la data 'Grafica de daños'
		$response->danos = $danos->grafica;
		// Asignamos la data 'Grafica del detalle de daños'
		$response->danos_detalle = $danos->grafica_detalle;
		// Asignamos la data 'Grafica del historico'
		$response->historico = $historico->data;
		// Asignamos la data 'Grafica del dia'
		$response->dia = $dia->data;
		// Agregamos el titulo de la 'Grafica del dia'
		$response->dia_title = $dia->title;
		// Agregamos el promedio del historico
		$response->historico_avg = $historico->avg;
		$response->historico_legends = $historico->legend;

		// Asignamos la data 'Grafica del historico' y Legend
		$response->tendencia = $tendencia->series;
		$response->tendencia_legends = $tendencia->legend;

		$response->palanca = ["" => "Todos"] + $this->getPalanca($postdata);
		$response->factor = (double)$this->factor;
		$response->fincas = ["" => "Todas"] + $this->getFincas($postdata);

		return $response;
	}
	
	public function tendenciaSemanal(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
		$palanca = "";
		if(isset($postdata->palanca)){
			$palanca = $postdata->palanca;
		}
		if(isset($postdata->idFinca)){
			$id_finca = $postdata->idFinca;
		}
		if(isset($postdata->yearTendencia)){
			$year = $postdata->yearTendencia;
		}else{
			$year = date('Y');
        }

        $tendencia = $this->tendencia($palanca , $id_finca , $postdata);
        $response->tendencia = $tendencia->series;
		$response->tendencia_legends = $tendencia->legend;
        return $response;
    }
    
    public function tableDedosPromedio(){
        $response = new stdClass;

        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response->semanas = $this->db->queryAllSpecial("SELECT semana AS id, semana AS label FROM merma WHERE YEAR('{$postdata->fecha_inicial}') GROUP BY semana ORDER BY semana");
        $lotes = $this->db->queryAll("SELECT bloque AS lote FROM merma INNER JOIN merma_detalle ON id_merma = merma.id AND campo = 'Dedos Gemelos' WHERE year = YEAR('{$postdata->fecha_inicial}') AND bloque != '' AND cantidad > 0 GROUP BY bloque");
        $response->umbral = $this->db->queryOne("SELECT ROUND(AVG(dedos_gemelos / racimos_procesados), 2) AS val
            FROM (
                SELECT racimos_procesados, ((SELECT cantidad FROM merma_detalle WHERE id_merma = merma.id AND campo = 'Dedos Gemelos')) AS dedos_gemelos
                FROM merma
                WHERE YEAR =  YEAR('{$postdata->fecha_inicial}')
                HAVING dedos_gemelos > 0
            ) AS tbl");
        $response->data = [];
        /* LOTES */
        foreach($lotes as $lote){
            $row = new stdClass;
            $row->key = $lote->lote;
            $row->lote = $lote->lote;

            $max = null;
            $min = null;
            $sum = 0;
            $count = 0;

            $max_e = []; //maximos dedos
            $min_e = []; //minimos dedos
            $sum_e = []; //suma dedos
            $count_e = [];

            /* DEDOS POR SEMANA POR LOTE */
            foreach($response->semanas as $sem){
                $val = $this->db->queryOne("SELECT ROUND(AVG(dedos_gemelos / racimos_procesados), 2) AS val
                FROM (
                    SELECT racimos_procesados, ((SELECT cantidad FROM merma_detalle WHERE id_merma = merma.id AND campo = 'Dedos Gemelos')) AS dedos_gemelos
                    FROM merma
                    WHERE YEAR =  YEAR('{$postdata->fecha_inicial}') AND bloque = '{$lote->lote}' AND semana = $sem
                    HAVING dedos_gemelos > 0
                ) AS tbl");

                $row->{"sem_{$sem}"} = $val;
                $total->{"sem_{$sem}"} += $val;

                if($max == null || $max < $val && $val > 0) $max = $val;
                if($min == null || $min > $val && $val > 0) $min = $val;
                if($val > 0) $count++;
                $sum += $val;
            }

            $row->max = $max;
            $row->min = $min;
            $row->avg = round($sum / $count, 2);
            $response->data[] = $row;
        }

        return $response;
    }

    public function tablaCierreEnfunde(){
        $response = new stdClass;
        $response->lote = $this->tablaCierreEnfundePorLote();
        $response->variables = $this->tablaCierreEnfundePorVariable();
        return $response;
    }

    public function tablaCierreEnfundePorVariable(){
        $response = new stdClass;
        $filters = $postdata = (object)json_decode(file_get_contents("php://input"));

        $campos = $this->db->queryAllOne("SELECT campo FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE year = YEAR('{$filters->fecha_inicial}') AND type = 'CIERRE ENFUNDE' AND campo IN ('Cirugía', 'Corbatas (3)', 'Deschive', 'Protectores') AND cantidad > 0 GROUP BY campo");
        $semanas = $this->db->queryAllOne("SELECT semana FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE year = YEAR('{$filters->fecha_inicial}') AND type = 'CIERRE ENFUNDE' AND campo IN ('Cirugía', 'Corbatas (3)', 'Deschive', 'Protectores') AND cantidad > 0 GROUP BY semana");

        $response->data = [];
        foreach($campos as $campo){
            $row = new stdClass;
            $row->lote = $campo;

            foreach($semanas as $semana){
                $row->{"sem_{$semana}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE year = YEAR('{$filters->fecha_inicial}') AND semana = '{$semana}' AND type = 'CIERRE ENFUNDE' AND campo = '{$campo}' AND cantidad > 0");
                
                if($row->{"sem_{$semana}"} > 0){
                    if(!isset($row->{"max"}) || $row->{"max"} < $row->{"sem_{$semana}"}) $row->{"max"} = $row->{"sem_{$semana}"};
                    if(!isset($row->{"min"}) || $row->{"min"} > $row->{"sem_{$semana}"}) $row->{"min"} = $row->{"sem_{$semana}"};
                    $row->{"count"} += 1;
                    $row->{"sum"} += $row->{"sem_{$semana}"};
                    $row->{"avg"} = round($row->{"sum"} / $row->{"count"}, 2);
                }
            }

            $row->children = $this->getChildrenLotes($campo, $semanas, "YEAR('{$filters->fecha_inicial}')");
            $response->data[] = $row;
        }

        $response->semanas = $semanas;
        return $response;
    }

    private function getChildrenLotes($var, $semanas, $year){
        $response = [];
        $filters = $postdata = (object)json_decode(file_get_contents("php://input"));

        $lotes = $this->db->queryAllOne("SELECT bloque FROM merma INNER JOIN merma_detalle ON merma.id = id_merma WHERE year = {$year} AND type = 'CIERRE ENFUNDE' AND campo = '{$var}' AND cantidad > 0 GROUP BY bloque");
        foreach($lotes as $lote){
            $row = new stdClass;
            $row->lote = $lote;

            foreach($semanas as $semana){
                $row->{"sem_{$semana}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON merma.id = id_merma WHERE year = {$year} AND semana = {$semana} AND bloque = '{$lote}' AND type = 'CIERRE ENFUNDE' AND campo = '{$var}'");

                if($row->{"sem_{$semana}"} > 0){
                    if(!isset($row->{"max"}) || $row->{"max"} < $row->{"sem_{$semana}"}) $row->{"max"} = $row->{"sem_{$semana}"};
                    if(!isset($row->{"min"}) || $row->{"min"} < $row->{"sem_{$semana}"}) $row->{"min"} = $row->{"sem_{$semana}"};
                    $row->{"count"} += 1;
                    $row->{"sum"} += $row->{"sem_{$semana}"};
                    $row->{"avg"} = round($row->{"sum"} / $row->{"count"}, 2);
                }
            }
            $response[] = $row;
        }

        return $response;
    }

    public function tablaCierreEnfundePorLote(){
        $response = new stdClass;
        $filters = $postdata = (object)json_decode(file_get_contents("php://input"));

        $lotes = $this->db->queryAllOne("SELECT bloque FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE year = YEAR('{$filters->fecha_inicial}') AND type = 'CIERRE ENFUNDE' AND campo IN ('Cirugía', 'Corbatas (3)', 'Deschive', 'Protectores') AND cantidad > 0 GROUP BY bloque");
        $semanas = $this->db->queryAllOne("SELECT semana FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE year = YEAR('{$filters->fecha_inicial}') AND type = 'CIERRE ENFUNDE' AND campo IN ('Cirugía', 'Corbatas (3)', 'Deschive', 'Protectores') AND cantidad > 0 GROUP BY semana");

        $response->data = [];
        foreach($lotes as $lote){
            $row = new stdClass;
            $row->lote = $lote;

            foreach($semanas as $semana){
                $row->{"sem_{$semana}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE year = YEAR('{$filters->fecha_inicial}') AND semana = '{$semana}' AND bloque = '{$lote}' AND type = 'CIERRE ENFUNDE' AND campo IN ('Cirugía', 'Corbatas (3)', 'Deschive', 'Protectores')");
                
                if($row->{"sem_{$semana}"} > 0){
                    if(!isset($row->{"max"}) || $row->{"max"} < $row->{"sem_{$semana}"}) $row->{"max"} = $row->{"sem_{$semana}"};
                    if(!isset($row->{"min"}) || $row->{"min"} > $row->{"sem_{$semana}"}) $row->{"min"} = $row->{"sem_{$semana}"};
                    $row->{"count"} += 1;
                    $row->{"sum"} += $row->{"sem_{$semana}"};
                    $row->{"avg"} = round($row->{"sum"} / $row->{"count"}, 2);
                }
            }

            $row->children = $this->getChildrenVariables($lote, $semanas, "YEAR('{$filters->fecha_inicial}')");
            $response->data[] = $row;
        }

        $response->semanas = $semanas;
        return $response;
    }

    private function getChildrenVariables($lote, $semanas, $year){
        $response = [];
        $filters = $postdata = (object)json_decode(file_get_contents("php://input"));

        $variables = $this->db->queryAllOne("SELECT campo FROM merma INNER JOIN merma_detalle ON merma.id = id_merma WHERE year = {$year} AND bloque = '{$lote}' AND type = 'CIERRE ENFUNDE' AND campo IN ('Cirugía', 'Corbatas (3)', 'Deschive', 'Protectores') AND cantidad > 0 GROUP BY campo");
        foreach($variables as $var){
            $row = new stdClass;
            $row->lote = $var;

            foreach($semanas as $semana){
                $row->{"sem_{$semana}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON merma.id = id_merma WHERE year = {$year} AND semana = {$semana} AND bloque = '{$lote}' AND type = 'CIERRE ENFUNDE' AND campo = '{$var}'");

                if($row->{"sem_{$semana}"} > 0){
                    if(!isset($row->{"max"}) || $row->{"max"} < $row->{"sem_{$semana}"}) $row->{"max"} = $row->{"sem_{$semana}"};
                    if(!isset($row->{"min"}) || $row->{"min"} < $row->{"sem_{$semana}"}) $row->{"min"} = $row->{"sem_{$semana}"};
                    $row->{"count"} += 1;
                    $row->{"sum"} += $row->{"sem_{$semana}"};
                    $row->{"avg"} = round($row->{"sum"} / $row->{"count"}, 2);
                }
            }
            $response[] = $row;
        }

        return $response;
    }
    
    public function last(){
		$days = $this->db->queryAllOne("SELECT date_fecha FROM merma GROUP BY date_fecha");
        //ultima fecha con registros
		$reg = $this->db->queryRow("SELECT finca, DATE(fecha) AS fecha, id_finca FROM merma ORDER BY fecha DESC, id_finca LIMIT 1");
		$years = $this->db->queryAllOne("SELECT year FROM merma WHERE year > 0 GROUP BY year ORDER BY year");
        return ["fecha" => $reg->fecha, "finca" => $reg->id_finca, "years" => $years, "days" => $days];
    }

	private function getPalanca($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		$sWhere = "";
		if(isset($filters->idFinca) && $filters->idFinca != ""){
			// $sWhere .= " AND id_finca = {$filters->idFinca}";
			if($filters->idFinca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$filters->idFinca}";
			}
		}
		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT palanca AS id , palanca AS label  FROM merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND palanca != '' $sWhere {$typeMerma}
				GROUP BY TRIM(palanca)";
		// D($sql);
		$response->data = [];
		$response->data = $this->db->queryAllSpecial($sql);
		return $response->data;
	}	

	private function getFincas($filters = []){
		
		$inicio = $filters->fecha_inicial;
		$fin = $filters->fecha_final;
		
		$response = new stdClass;
		$sql = "SELECT id_finca AS id , finca AS label  FROM merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND finca != ''
                GROUP BY id_finca";
        $response->data = $this->db->queryAllSpecial($sql);
		return $response->data;
	}

	private function tags($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$sql = "SELECT (SELECT SUM(cantidad) 
                        FROM merma 
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                            AND porcentaje_merma > 0 
                            AND campo = 'Total peso Merma Neta (Lb)'
                            $sWhere) / SUM(peso_neto) * 100 AS merma
				FROM merma 
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
                
		$rp1 = $this->db->queryRow($sql);

		$peso_neto = "(racimo - tallo)";
		$cantidad = "(cantidad)";
		$cajas = "(SUM(total_peso_merma-tallo) / $this->cajas) AS cajas";

		$TagsCaja = (double)$this->db->queryOne("SELECT {$cajas} FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' {$sWhere}");


		$sql = "SELECT 
					(SELECT SUM(cantidad) 
                        FROM merma 
                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                        WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                            AND porcentaje_merma > 0 
                            AND campo = 'Total peso Merma Neta (Lb)'
                            $sWhere) / SUM(peso_neto) * 100 AS merma_neta,
					AVG(porcentaje_merma_procesada) AS merma_procesada 
				FROM merma 
                WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND porcentaje_merma > 0 $sWhere";
            
		$rp1 = $this->db->queryRow($sql);

		$sql2 = "SELECT AVG((SELECT (cantidad)
				FROM merma_detalle
				WHERE merma_detalle.id_merma = merma.id
				AND campo = '% MERMA CORTADA')) AS merma_cortada,
				AVG((tallo/racimo)*100) AS tallo
				FROM merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' $sWhere";
		$rp2 = $this->db->queryRow($sql2);

		$sql ="SELECT LOWER(type) AS id, 
                        ROUND(
                            (SUM(cantidad) / 
                            (SELECT SUM(cantidad)
                                FROM merma
                                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                                    AND campo LIKE 'Total peso Merma Neta (Lb)'
                                    $sWhere) * 100), 2) AS label
                FROM merma
                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                    AND campo LIKE 'Total Peso Merma%'
                    AND TYPE NOT IN ('RESULTADOS', 'PESO RACIMOS', 'PESO TALLOS')
                    $sWhere
                GROUP BY TYPE";

        $data_categorias = $this->db->queryAllSpecial($sql);
        $data = [];
        $sum_peso = 0;
        foreach($data_categorias as $cat => $val){
            $data[$cat]["porc"] = $val;
            $data[$cat]["peso"] = $this->db->queryRow("SELECT ROUND(SUM(cantidad), 2) as peso 
                                                        FROM merma
                                                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                                        WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                                                            AND campo LIKE 'Total Peso Merma%'
                                                            AND TYPE = UPPER('{$cat}')
                                                            $sWhere")->peso;
            $sum_peso += $data[$cat]["peso"];
        }


		$tags = [];
		$tags = array(
			'merma' 		  => ["porc" => $rp1->merma_neta, "peso" => $sum_peso ],
			'tallo'			  => $rp2->tallo,
			'campo' 	      => ["porc" => $data["campo"]["porc"], "peso" => $data["campo"]["peso"]],
            'cosecha' 		  => ["porc" => $data["cosecha"]["porc"], "peso" => $data["cosecha"]["peso"]],
            'proceso' 		  => ["porc" => $data["proceso"]["porc"], "peso" => $data["proceso"]["peso"]],
            'fisiologicos' 	  => ["porc" => $data["fisiologicos"]["porc"], "peso" => $data["fisiologicos"]["peso"]],
		);

		return $tags;
	}

	private function tabla_lote_merma($inicio, $fin , $palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$data = [];
		$peso_total = "SUM(total_peso_merma) AS peso_total";
		$cajas = "(SUM(total_peso_merma) / $this->cajas) AS cajas";
		if($this->session->id_company == 2){

		}else{
			$total_peso_merma = "total_peso_merma";
			$racimos_lote = "id";

			if($this->session->id_company == 7){
				$peso_total = "SUM((total_peso_merma-tallo)) AS peso_total";
				$total_peso_merma = "(total_peso_merma-tallo) / racimos_procesados";
				$racimos_lote = "racimos_procesados";
				$cajas = "(SUM(total_peso_merma - tallo)  / $this->cajas) AS cajas";
			}
			$typeMerma = "";

			$sql = "SELECT id , 
						bloque AS lote ,
						(SELECT AVG((total_peso_merma-tallo) / racimos_procesados) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = bloques.bloque $sWhere {$typeMerma}) AS total_peso_merma , 
						(SELECT AVG(total_defectos) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = bloques.bloque $sWhere {$typeMerma}) AS total_defectos , 
						(SELECT SUM(total_defectos) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = bloques.bloque $sWhere {$typeMerma}) AS total_defectos_sum ,
						(SELECT SUM(peso_neto) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = bloques.bloque $sWhere {$typeMerma}) AS peso_neto,
						(SELECT {$cajas} FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS cajas,  
						(SELECT SUM(racimos_procesados) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS racimos_lote , 
						(SELECT AVG(racimos_procesados) FROM merma WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma}) AS racimos_lote_avg , 
						(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE bloque = bloques.bloque 
                                AND date_fecha BETWEEN '$inicio' AND '$fin'
                                AND campo = 'Total peso Merma Neta (Lb)' $sWhere {$typeMerma}) AS peso_total,
                        (SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE bloque = bloques.bloque AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma} AND campo = 'Total Defectos') AS total_danos
			FROM(
			SELECT id, bloque
				FROM merma
				WHERE date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma} 
                GROUP BY bloque) AS bloques";
		}
		$res = $this->db->queryAll($sql);
		$danhos = [];
		foreach ($res as $key => $value) {
			$value = (object)$value;
            $value->merma = $value->peso_total / $value->peso_neto * 100;
			$value->usd = (double)($value->cajas * $this->usd);
			$value->expanded = false;
			$value->detalle = [];
            //$value->detalle = $this->tabla_lote_merma_detalle($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
			$danhos = $this->getDanhos($inicio, $fin , $value->lote , $palanca , $id_finca , $filters);
			$value->detalle_danhos = $danhos->danhos;
			$value->detalle_danhos_sum = $danhos->danhos_suma;
		}
		//D($res);
		return $res;
	}

	private function getDanhos($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		if($this->session->id_company == 7){
			$typeMerma = " AND campo NOT LIKE '%Total Peso Merma%'";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label , SUM(cantidad) AS label2 FROM (
				SELECT merma_detalle.id,merma.id AS id_m , fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE bloque = '{$bloque}' AND DATE(fecha) BETWEEN '$inicio' AND '$fin' AND campo like '%".$this->search."%'  {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS', 'PESO TALLOS') $sWhere ) as detalle
				GROUP BY type";
		// D($sql);
		$res = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->danhos = [];
		$response->danhos_suma = [];
		foreach ($res as $key => $value) {
			$response->danhos[$value->id] = (double)$value->label;
			$response->danhos_suma[$value->id] = (double)$value->label2;
		}

		return $response;
	}

	private function getDanhosPesos(){
		if($this->session->id_company == 7){
			$sql = "";
		}
		return array();
	}

	public function getSecondLevel(){
		$postdata = (array)json_decode(file_get_contents("php://input"));

		$data = (object)[
			"id_finca" => getValueFrom($postdata , "idFinca" , 0 ),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , 0 ),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , 0 ),
			"bloque" => getValueFrom($postdata , "bloque" , 0 ),
			"palanca" => getValueFrom($postdata , "palanca" , 0 )
		];

		return $this->tabla_lote_merma_detalle($data->fecha_inicial , $data->fecha_final , $data->bloque , $data->palanca , $data->id_finca);
	}

	private function tabla_lote_merma_detalle($inicio ,$fin ,$bloque , $palanca = "", $id_finca = "", $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$data = [];

        $cajas = "(SUM(total_peso_merma - tallo)  / $this->cajas) AS cajas";

		$sql = "SELECT TYPE AS 'type', '{$bloque}' AS bloque,
						(SELECT AVG(total_defectos) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' AND TYPE = types.TYPE $sWhere ) AS total_defectos , 
						(SELECT SUM(total_defectos) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' AND TYPE = types.TYPE $sWhere ) AS total_defectos_sum ,
						(SELECT SUM(peso_neto) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' $sWhere ) AS peso_neto,
						(SELECT {$cajas} FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE bloque = '{$bloque}' AND TYPE = types.TYPE AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere ) AS cajas,  
						(SELECT SUM(racimos_procesados) FROM merma WHERE bloque = '{$bloque}' AND TYPE = types.TYPE AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere ) AS racimos_lote , 
						(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE bloque = '{$bloque}' AND TYPE = types.TYPE
                                AND date_fecha BETWEEN '$inicio' AND '$fin'
                                AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS', 'PESO TALLOS')
                                AND campo LIKE 'Total Peso Merma%' $sWhere) AS peso_total,
                        (SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE bloque = '{$bloque}' AND TYPE = types.TYPE AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma} AND campo LIKE 'Total%' AND flag = 2) AS total_danos
                FROM(
					SELECT TYPE
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE bloque = '{$bloque}' 
						AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere
						AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS', 'PESO TALLOS')
						AND campo LIKE 'Total Peso Merma%'
					GROUP BY TYPE
				) AS types
                HAVING peso_total > 0";
		
		$res = $this->db->queryAll($sql);
		foreach ($res as $key => $value) {
			$value->expanded = false;
            $value->cantidad = $value->peso_total / $value->peso_neto * 100;
			$value->usd = (double)($value->cajas * $this->usd);
			$value->details = [];
			// $value->details = $this->tabla_lote_merma_detalle_details($inicio, $fin , $value->type, $palanca, $id_finca , $filters , $bloque, $value->danhos_peso, $value->racimos_lote, $value->cantidad, $cajas);
		}
		return $res;
	}

	public function getThirdLevel(){
		$postdata = (array)json_decode(file_get_contents("php://input"));

		$data = (object)[
			"id_finca" => getValueFrom($postdata , "idFinca" , 0 ),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , 0 ),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , 0 ),
			"bloque" => getValueFrom($postdata , "bloque" , 0 ),
			"type" => getValueFrom($postdata , "type" , 0 ),
			"danhos_peso" => getValueFrom($postdata , "danhos_peso" , 0 ),
			"racimos_lote" => getValueFrom($postdata , "racimos_lote" , 0 ),
			"cantidad" => getValueFrom($postdata , "cantidad" , 0 ),
			"palanca" => getValueFrom($postdata , "palanca" , 0 )
		];

		return $this->tabla_lote_merma_detalle_details($data->fecha_inicial , $data->fecha_final , $data->type , $data->palanca , $data->id_finca , [] ,$data->bloque ,$data->danhos_peso ,$data->racimos_lote ,$data->cantidad ,"" );
	}

	private function tabla_lote_merma_detalle_details($inicio, $fin , $type , $palanca = "", $id_finca = "" , $filters = [] , $bloque, $_danhos_peso, 
		$racimos_procesados, $_cantidad){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		$cajas = "(SUM(cantidad) / $this->cajas) AS cajas";
		$peso_total = "SUM(cantidad) AS total_peso";

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			$racimo = "racimo";
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$cantidad = "(cantidad)";
				$racimo = "racimo";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}elseif($this->session->id_company == 4){
			$cantidad = "(cantidad/racimos_procesados)";
			$racimo = "(racimo)";

			if($this->MermaTypePeso == "Kg"){
				$peso_total = "SUM(cantidad / 2.2) AS total_peso";
			}
		}elseif($this->session->id_company == 7){
			$cantidad = "(cantidad/racimos_procesados)";
			$racimo = "(racimo)";
		}elseif($this->session->id_company == 3){
			if($this->MermaTypePeso == "Kg"){
				$cantidad = "(cantidad / 2.2)";
				$racimo = "(racimo / 2.2)";
			}else{
				$racimo = "(racimo)";
				$cantidad = "(cantidad)";
			}
		}else{
				$racimo = "racimo";
				$cantidad = "(cantidad)";
		}

		$condition = "AND flag = 1";
		$response = new stdClass;
		$sql = "SELECT REPLACE(campo,'Peso ' , '') AS campo,
						(SELECT AVG(total_defectos) FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo $sWhere ) AS total_defectos , 
						(SELECT SUM(total_defectos) FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo $sWhere ) AS total_defectos_sum ,
						(SELECT SUM(peso_neto) FROM merma WHERE date_fecha BETWEEN '$inicio' AND '$fin' AND bloque = '{$bloque}' $sWhere ) AS peso_neto,
						(SELECT {$cajas} FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere ) AS cajas,  
						(SELECT SUM(racimos_procesados) FROM merma INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma WHERE bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere ) AS racimos_lote , 
						(SELECT SUM(cantidad)
                            FROM merma 
                            INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                            WHERE bloque = '{$bloque}' AND TYPE = '{$type}' AND campo = campos.campo
                                AND date_fecha BETWEEN '$inicio' AND '$fin'
                                AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS', 'PESO TALLOS')
                                $sWhere) AS peso_total,
                        (SELECT SUM(cantidad) FROM merma INNER JOIN merma_detalle ON id_merma = merma.id WHERE bloque = '{$bloque}' AND TYPE = '{$type}' AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere {$typeMerma} AND campo = CONCAT(campos.campo, ' (# Daños)') AND flag = 2) AS total_danos
                FROM(
                SELECT campo
                FROM merma
                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                WHERE bloque = '{$bloque}' 
                    AND date_fecha BETWEEN '$inicio' AND '$fin' $sWhere
                    AND TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS', 'PESO TALLOS')
                    AND type = '{$type}'
                    AND cantidad > 0 AND flag = 1
                    AND campo NOT LIKE 'Total%'
                GROUP BY campo) AS campos";
		
		$response->data = $this->db->queryAll($sql);
        foreach($response->data as $row){
            $row->cantidad = $row->peso_total / $row->peso_neto * 100;
        }
		// $response->danhos_merma = $this->getDanhosMermaDetalle($inicio, $fin , $type , $palanca , $id_finca , $filters);
		return $response->data;
	}

	private function grafica_historico($palanca = "", $id_finca = "" , $year = "YEAR(CURRENT_DATE)" ){
		$sWhere = "";
		$sWhere_PromFincas = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
        }
        
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$sWhere .= " AND YEAR(fecha) = {$year}";
		$Umbral = 10;

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}

		$sql = "SELECT TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere
				GROUP BY id_finca , getWeek(fecha)
				ORDER BY id_finca , getWeek(fecha)";
		
        if((int)$id_finca > 1 && $palanca != ""){
            $sWhere_PromFincas = " AND palanca = '{$palanca}'";
        }elseif((int)$id_finca == 1){
            $sWhere_PromFincas = $sWhere;
        }
        $sWhere_PromFincas .= " AND YEAR(fecha) = {$year}";
        // D($sWhere_PromFincas);
        $sql = "(SELECT TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
            FROM merma
            WHERE porcentaje_merma > 0 AND id_finca > 0 $sWhere
            GROUP BY id_finca , getWeek(fecha)
            ORDER BY id_finca , getWeek(fecha))";  


		$Umbral = 10;

		$response = new stdClass;
		// D($sql);
		$res = $this->db->queryAll($sql);
		$response->data = [];
		$response->legend = [];
		$response->avg = 0;
		$count = 0;
		$sum = 0;
		$series = new stdClass;
		$finca = "";
		$flag_count = 0;
		$markLine = new stdClass;
		foreach ($res as $key => $value) {
			$value = (object)$value;
			$value->porcentaje_merma = (float)$value->porcentaje_merma;
			$sum += $value->porcentaje_merma;
			$response->legend[(int)$value->fecha] = (int)$value->fecha;
			if($finca != $value->id_finca){
				$series = new stdClass;
				$finca = $value->id_finca;
				$series->name = $value->finca;
				$series->type = "line";
				$series->data = [];
				$series->data[$value->fecha] = round($value->porcentaje_merma ,2);
				$series->itemStyle = (object)[
					"normal" => [
						// "color" => '#007537',
						// "barBorderColor" => '#007537',
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series->markLine->data = [[ "name" => "Umbral", "label" => 'Umbral', "value" => 10, "xAxis" => -1, "yAxis" => 10 ]];
					$flag_count++;
				}
				
				$response->data[$value->id_finca] = $series;
			}else{
				if($finca == $value->id_finca){
					// D($response->data[$value->id_finca]);
					$response->data[$value->id_finca]->data[$value->fecha] = round($value->porcentaje_merma ,2);
				}
			}
			$count++;
		}

		$response->avg = ($sum / $count);

		return $response; 
	}

	private function historico_general($palanca = "" , $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}

		$typeMerma = "";
		$response = new stdClass;
		$sql = "SELECT TRIM(finca) AS finca ,id_finca, getWeek(fecha) AS fecha, IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma
				FROM merma
				WHERE porcentaje_merma > 0 $sWhere {$typeMerma}
				GROUP BY getWeek(fecha)";
		$res = $this->db->queryAll($sql);
	}

	private function grafica_dia($palanca = "", $id_finca = ""  , $id_fincaDia = "" , $filters = []){
		$sWhere = "";
		$fincaDia = 0;
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		if($id_fincaDia != ""){
			$fincaDia = $id_fincaDia;
		}

		if($this->session->id_company == 2){
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
			if($this->MermaTypePeso == "Kg"){
				$total_peso_merma = "(total_peso_merma / 2.2)";
			}else{
				$total_peso_merma = "(total_peso_merma)";
			}
			/*----------  CAMBIO DE PESO A LIBRA MARCEL  ----------*/
		}else{
				$total_peso_merma = "(total_peso_merma)";
		}


		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$response = new stdClass;
		$sql = "SELECT id , id_finca,TIME_FORMAT(TIME(fecha) , '%H:%i') AS time ,DATE(fecha) as fecha ,IF(DATE(fecha) > DATE('2016-09-13'), AVG(porcentaje_merma) , AVG((porcentaje_merma))) AS porcentaje_merma, AVG({$total_peso_merma}) AS total_peso_merma,
				CONCAT_WS(' ','Última Actualización : ' , DATE(fecha)) as title FROM merma
				WHERE 1 = 1 $sWhere {$typeMerma}
				GROUP BY id
				ORDER BY fecha desc";
		// D($sql);
		$res = $this->db->queryAll($sql);
		$count = 1;
		$fecha = "";
		$flag = 0;
		$response->data = new stdClass;
		$response->data->data = [];
		$response->data->label = [];
		$response->data->series = [];
		$response->title = "";
		$label = "";
		foreach ($res as $key => $value) {
			$value = (object)$value;
			if($value->fecha != $fecha){
				$fecha = $value->fecha;
				$flag++;
				if($flag == 2){
					break;
				}
			}

			if($fincaDia > 0){
				if($value->id_finca == $fincaDia){
					$response->title = $value->title;
					$value->porcentaje_merma = (float)$value->porcentaje_merma;
					if($value->time == '00:00'){
						$label = "Hora ".$count;
					}else{
						$label = $value->time;
					}
					$response->data->label[$value->fecha." ".$label] = $label;
					$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
					$response->data->data[$label] = $value;
					$count++;
				}
			}else{
				$response->title = $value->title;
				$value->porcentaje_merma = (float)$value->porcentaje_merma;
				if($value->time == '00:00'){
					$label = "Hora ".$count;
				}else{
					$label = $value->time;
				}
				$response->data->label[$value->fecha." ".$label] = $label;
				$response->data->series[$value->fecha." ".$label] = $value->porcentaje_merma;
				$response->data->data[$label] = $value;
				$count++;
			}
		}
		return $response; 
	}

	private function tabla_danos_merma($inicio, $fin , $palanca , $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$response = new stdClass;
		$cantidad = "(cantidad)";
		$racimo = "(racimo)";
		$peso_total = "SUM(total_peso_merma - tallo) AS peso_total";
		$cajas = "(SUM(total_peso_merma - tallo) / $this->cajas) AS cajas";
		$racimos_lote = "racimos_procesados";

		$typeMerma = "";
		
		$subQuery = "(SELECT SUM(racimo) FROM merma WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin')";
		$sql ="SELECT type,
						AVG(cantidad) AS danhos_peso,
						AVG(total_defectos) AS total_defectos, 
						SUM(total_defectos) AS total_defectos_sum , 
						$peso_total,
						SUM($racimos_lote) AS racimos_lote , 
						AVG($racimos_lote) AS racimos_lote_avg,
						ROUND((ROUND(SUM(cantidad),2) / ROUND({$subQuery},2)) * 100,2) AS cantidad, 
                        {$cajas}
				FROM (
					SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , {$cantidad} AS cantidad , peso_neto, total_defectos , total_peso_merma , tallo , {$racimos_lote}
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' AND campo like '%Total Peso Merma%' {$typeMerma}
						AND type NOT IN ('RESULTADOS', 'PESO RACIMOS', 'PESO TALLOS') $sWhere 
					HAVING cantidad > 0
				) as detalle
				GROUP BY type";
		
        $response->data = $this->db->queryAll($sql);
        

        $data_chart = $this->db->queryAll("SELECT type, ROUND(SUM(cantidad), 1) AS cantidad
                                                FROM merma
                                                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                                WHERE date_fecha BETWEEN '$inicio' AND '$fin' 
                                                    AND campo LIKE 'Total Peso Merma%'
                                                    AND TYPE NOT IN ('RESULTADOS', 'PESO RACIMOS', 'PESO TALLOS')
                                                    $sWhere
                                                GROUP BY TYPE
                                                HAVING cantidad > 0");
		$response->grafica = $this->pie($data_chart , '50%' ,"Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		$response->grafica_detalle = [];
		$detalle = "";
		foreach ($response->data as $key => $value) {
			$value = (object)$value;
			$value->usd = (double)($value->cajas * $this->usd);
			$value->detalle = [];
		}

        foreach($data_chart as $value){
            $detalle = $this->tabla_danos_merma_detalle($inicio, $fin , $value->type, $palanca, $id_finca , $filters, $value->cantidad);
            $response->grafica_detalle[$value->type] = $detalle->grafica;
        }

		$response->danhos_merma = [];
//		$response->danhos_merma = $this->getDanhosMerma($inicio, $fin , $palanca , $id_finca , $filters);
		return $response;
	}

	private function getDanhosMerma($inicio ,$fin  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$sWhereConsulta = "AND campo like '%".$this->search."%'";
		if($this->session->id_company == 4 || $this->session->id_company == 9){
			$sWhereConsulta = "AND flag = 1";
		}

		if($this->session->id_company == 7){
			$sWhereConsulta = "AND campo like '%".$this->search."%' AND campo NOT LIKE '%Total Peso Merma%'";
		}

		if($this->session->id_company == 3){
			$sWhereConsulta = "AND campo like '%Total Peso Merma%'";
		}

		$sql = "SELECT type AS id ,AVG(cantidad) AS label FROM (
				SELECT merma_detalle.id,merma.id AS id_m , fecha  ,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma  FROM merma
				INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
				WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' {$sWhereConsulta} {$typeMerma}
				AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS', 'PESO TALLOS') $sWhere ) as detalle
				GROUP BY type";
		//D($sql);
		$res = $this->db->queryAllSpecial($sql);

		return $res;
	}

	private function tabla_danos_merma_detalle($inicio, $fin , $type , $palanca = "", $id_finca = "" , $filters = [], $value_total = 0){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$response = new stdClass;
		$sql = "SELECT campo AS type, ROUND((SUM(cantidad) / 
                                    (SELECT SUM(cantidad)
                                        FROM merma
                                        INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                                        WHERE date_fecha BETWEEN '$inicio' AND '$fin'
                                            AND campo LIKE 'Total Peso Merma%'
                                            AND TYPE = '{$type}' AND flag = 0 $sWhere) * 100) * (ROUND($value_total, 2) / 100), 2) AS cantidad
                FROM merma
                INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
                WHERE date_fecha BETWEEN '$inicio' AND '$fin'
                    AND TYPE = '{$type}'
                    AND flag = 1
                    AND cantidad > 0 $sWhere
                GROUP BY campo";
		
		$response->data = $this->db->queryAll($sql);
		$response->grafica = $this->pie($response->data , '50%' ,"Detalle de Daños"  , "area" , "normal" , false , ['50%', '50%'] , 2);
		return $response;
	}

	private function getDanhosMermaDetalle($inicio ,$fin , $type  , $palanca = "", $id_finca = "" , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}

		$typeMerma = "";
		if($this->session->id_company == 6){
			$typeMerma = " AND tipo_merma = '{$filters->idMerma}'";
		}

		$condition = "AND flag = 2";
		if($this->session->id_company == 3){
			if($type == "LOTERO AEREO"){
				$condition = "AND flag = 3";
			}elseif($type == "COSECHA"){
				$condition = "AND (flag = 1 OR flag = 2)";
			}
		}

		$sql = "SELECT campo AS id ,AVG(cantidad) AS label , SUM(cantidad) AS label2 
				FROM (
					SELECT fecha  , bloque,merma_detalle.id_merma , type ,campo , cantidad , peso_neto , (cantidad / peso_neto) * 100 as Total , porcentaje_merma 
					FROM merma
					INNER JOIN merma_detalle ON merma.id = merma_detalle.id_merma
					WHERE DATE(fecha) BETWEEN '$inicio' AND '$fin' 
						AND type = '{$type}' {$condition} {$typeMerma}
						AND type NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS', 'PESO TALLOS') $sWher
				) as detalle
				GROUP BY type ,campo";
		#D($sql);
		$res = $this->db->queryAll($sql);
		$response = new stdClass;
		$response->danhos = [];
		$response->danhos_suma = [];
		foreach ($res as $key => $value) {
			$response->danhos[$value->id] = (double)$value->label;
			$response->danhos_suma[$value->id] = (double)$value->label2;
		}

		return $response;
	}

	private function tendencia($palanca , $id_finca , $filters = []){
		$sWhere = "";
		if($palanca != ""){
			$sWhere = " AND palanca = '{$palanca}'";
		}
		if($id_finca != ""){
			if($id_finca == "9999999" && $this->session->id_company == 7){
				$sWhere .= " AND id_finca IN (2,3)";
			}else{
				$sWhere .= " AND id_finca = {$id_finca}";
			}
		}
		if(isset($filters->yearTendencia) && $filters->yearTendencia != ""){
			$sWhere .= " AND YEAR (fecha) = '{$filters->yearTendencia}' ";
		}else{
			$sWhere .= " AND YEAR(fecha) = 2017 ";
		}

		$Umbral = 50;
        $semanas = $this->db->queryAllSpecial("SELECT semana as id, semana as label FROM merma WHERE 1=1 $sWhere GROUP BY semana ORDER BY semana");
        $total_semanas = $this->db->queryAllSpecial("SELECT semana AS id, SUM(cantidad) AS label
                                                    FROM merma
                                                    INNER JOIN merma_detalle ON id_merma = merma.`id`
                                                    WHERE TYPE NOT IN ('RESULTADOS','FIRMA ELECTRONICA','OTROS' ,'RECUSADOS', 'PESO RACIMOS', 'PESO TALLOS')
                                                        AND campo LIKE 'Total Peso Merma%' $sWhere
                                                    GROUP BY semana
                                                    ORDER BY semana");
        $data = $this->db->queryAll("SELECT semana, TYPE AS labor, 
										(
											SELECT SUM(cantidad) 
											FROM merma 
											INNER JOIN merma_detalle ON id_merma = merma.`id` 
											WHERE semana = semanas.semana 
												AND TYPE = categorias.type 
												AND flag = 1 
												AND cantidad > 0
												$sWhere
										) cantidad
									FROM (
										SELECT semana
										FROM merma
										INNER JOIN merma_detalle ON id_merma = merma.`id`
										WHERE flag = 1 AND TYPE NOT IN ('RESULTADOS', 'PESO TALLOS', 'PESO RACIMOS') $sWhere
										GROUP BY semana
									) AS semanas
									JOIN (
										SELECT TYPE
										FROM merma
										INNER JOIN merma_detalle ON id_merma = merma.`id`
										WHERE flag = 1 AND TYPE NOT IN ('RESULTADOS', 'PESO TALLOS', 'PESO RACIMOS') $sWhere
										GROUP BY TYPE
									) AS categorias
									ORDER BY semanas.semana");
        $types = [];
        
		$response = new stdClass;
		$series = new stdClass;
		$flag_count = 0;
		$labor = "";

		$response->series = [];
		$cantidad = 0;
		foreach ($data as $key => $value) {
            if(!in_array($value->semana, $response->legend))
                $response->legend[] = (int) $value->semana;
                
			$cantidad = round(($value->cantidad / $total_semanas[$value->semana])*100 , 2);

			if(!in_array($value->labor, $types)){
                $types[] = $value->labor;

				$series = new stdClass;
				$series->name = $value->labor;
				$series->type = "line";
				$series->data = [];
				$series->data[] = $cantidad;
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				if($flag_count == 0){
					$series->markLine = new stdClass;
					$series->markLine->data = [];
					$markLine->name = "Umbral";
					$markLine->value = $Umbral;
					$markLine->xAxis = -1;
					$markLine->yAxis = $Umbral;
					$series->markLine->data[0][] = $markLine;
					
					$markLine = new stdClass;
					$markLine->xAxis = 52;
					$markLine->yAxis = $Umbral;

					$series->markLine->data[0][] = $markLine;
					$flag_count++;
				}	

				$response->series[$value->labor] = $series;
			}else{
				$response->series[$value->labor]->data[] = $cantidad;
			}
		}

		return $response;
	}

	private function pie($data = [] , $radius = ['50%', '70%'] , $name = "DAÑOS" , $roseType = "" , $type = "normal" , $legend = true , $position = ['60%', '60%'] , $version = 3){
		$response = new stdClass;
		$response->pie = [];
		if(count($data) > 0){
			$response->pie["title"]["show"] = true;
			$response->pie["title"]["text"] = $name;
			$response->pie["title"]["left"] = "center";
			// $response->pie["title"]["left"] = "right";
			$response->pie["title"]["subtext"] = "Merma";
			$response->pie["tooltip"]["trigger"] = "item";
			$response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
			$response->pie["toolbox"]["show"] = true;
			$response->pie["toolbox"]["feature"]["mark"]["show"] = true;
			$response->pie["toolbox"]["feature"]["restore"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
			$response->pie["legend"]["show"] = $legend;
			$response->pie["legend"]["orient"] = "vertical";
			$response->pie["legend"]["x"] = "left";
			$response->pie["calculable"] = true;
			$response->pie["legend"]["data"] = [];
			$response->pie["series"]["name"] = $name;
			$response->pie["series"]["type"] = "pie";
			$response->pie["series"]["center"] = $position;
			$response->pie["series"]["radius"] = $radius;
			if($version === 3){
				$response->pie["series"]["selectedMode"] = true;
				$response->pie["series"]["label"]["normal"]["show"] = true;
				$response->pie["series"]["label"]["emphasis"]["show"] = true;
				if($type != "normal"){
					$response->pie["series"]["roseType"] = $roseType;
					$response->pie["series"]["avoidLabelOverlap"] = "pie";
				}
				// $response->pie["series"]["label"]["normal"]["position"] = "center";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
				// $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
				$response->pie["series"]["labelLine"]["normal"]["show"] = true;
			}
			$response->pie["series"]["data"] = [];

			foreach ($data as $key => $value) {
				$value = (object)$value;
				$response->pie["legend"]["data"][] = $value->type;
				if($version === 3){
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)$value->cantidad , 
							"label" => [
								"normal" => ["show" => true , "position" => "outside" , "formatter" => "{b} \n {c} ({d}%)"],
								"emphasis" => ["show" => true , "position" => "outside"],
							]
					];
				}else{
					// $response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2)];
					$response->pie["series"]["data"][] = ["name" => $value->type  , "value" => (float)round($value->cantidad,2),
						"itemStyle" => [
							"normal" => [ "label" => ["formatter" => "{b} \n {c} ({d}%)"] ],
						]
					];
				}
			}
		}

		return $response->pie;
	}
}