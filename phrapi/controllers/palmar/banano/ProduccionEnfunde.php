<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionEnfunde extends ReactGrafica {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function getParams(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "semana" => getValueFrom($postdata, 'semana', ''),
            "year" => getValueFrom($postdata, 'year', date('y'))
        ];
        return $data;
    }

    public function last(){
        $response = new stdClass;
        $response->semana = $this->db->queryOne("SELECT getWeek(MAX(fecha)) AS sem FROM produccion_enfunde");
        return $response;
    }

	public function index(){
        $response = $this->tablePrincipal();
        return $response;
    }

    public function tags(){
        $response = new stdClass;
        $response->tags = new stdClass;
        $filters = $this->getParams();

        $response->tags->acumulado = $this->db->queryOne("SELECT SUM(usadas) FROM produccion_enfunde WHERE years = $filters->year");
        $response->tags->enfunde_sem = $this->db->queryOne("SELECT ROUND(AVG(usadas), 2) FROM (
                SELECT AVG(usadas) AS usadas
                FROM (
                    SELECT SUM(usadas) AS usadas, lote
                    FROM produccion_enfunde
                    WHERE years = $filters->year
                    GROUP BY semana, lote
                ) AS tbl
                GROUP BY lote
            ) AS tbl");
        $response->tags->enfunde_ha = round($this->db->queryOne("SELECT
            (SELECT SUM(usadas)
            FROM produccion_enfunde
            WHERE years = $filters->year AND semana = $filters->semana)
            / (
                SELECT SUM(hectareas)
                FROM (
                    SELECT lote
                    FROM produccion_enfunde
                    WHERE years = $filters->year AND semana = $filters->semana
                    GROUP BY lote
                ) tbl
                INNER JOIN lotes ON nombre = lote
            )"), 2);
        //$response->tags->enfunde_ha = $this->db->queryOne("SELECT ROUND(AVG(enfunde), 2) FROM(SELECT ROUND(SUM(usadas)/hectareas,2) AS enfunde FROM produccion_enfunde INNER JOIN lotes ON lotes.nombre = produccion_enfunde.lote WHERE years = $filters->year AND semana = $filters->semana GROUP BY lote) AS tbl");;
        return $response;
    }

    public function lotesSemanal(){
        $response = new stdClass;
        $filters = $this->getParams();
        if($filters->year == ""){
            $filters->year = 'YEAR(CURRENT_DATE)';
        }

        $response->semanas = $this->db->queryAllSpecial("SELECT semana AS id, semana AS label FROM produccion_enfunde WHERE years = $filters->year GROUP BY semana ORDER BY semana");
        $lotes = $this->db->queryAll("SELECT lote FROM produccion_enfunde WHERE years = $filters->year AND lote != '' GROUP BY lote");
        $response->data = [];
        /* LOTES */
        foreach($lotes as $lote){
            $row = new stdClass;
            $row->key = $lote->lote;
            $row->lote = $lote->lote;

            $max = null; $max_ha = null;
            $min = null; $min_ha = null;
            $sum = 0; $sum_ha = 0; $sum_t = 0; 
            $count = 0; $count_ha = 0; $count_t = 0;

            $max_e = []; $max_e_ha = []; //maximos enfundes por enfundador
            $min_e = []; $min_e_ha = []; //minimos enfundes por enfundador
            $sum_e = []; $sum_e_ha = []; //suma enfunde por enfundador
            $count_e = []; $count_e = [];

            /* ENFUNDADORES QUE TRABAJARON EN ESE LOTE */
            $childs = $this->db->queryAll("SELECT e.id AS 'key', e.nombre AS lote FROM produccion_enfunde INNER JOIN cat_enfundadores e ON id_enfundador = e.id WHERE years = $filters->year AND lote = $lote->lote GROUP BY e.id");

            /* FUNDAS USADAS POR SEMANA POR LOTE */
            foreach($response->semanas as $sem){
                $val = $this->db->queryOne("SELECT ROUND(SUM(usadas), 0) FROM produccion_enfunde WHERE years = $filters->year AND semana = $sem AND lote = $lote->lote");
                if(in_array($lote->lote, ["2015", "2017"])){
                    $area_lote = 1;
                }else{
                    $area_lote = (float) $this->db->queryOne("SELECT hectareas FROM lotes WHERE nombre = '{$lote->lote}'");
                }

                $val_ha = $this->db->queryOne("SELECT ROUND(SUM(usadas)/{$area_lote}, 2) FROM produccion_enfunde WHERE years = $filters->year AND semana = $sem AND lote = $lote->lote");

                $row->{"sem_{$sem}"} = $val;
                if($val > 0){
                    $total->{"sem_{$sem}_sum"} += $val;
                    $total->{"sem_{$sem}_count"} += 1;
                    $total->{"sem_{$sem}"} = round($total->{"sem_{$sem}_sum"}/$total->{"sem_{$sem}_count"}, 2);
                }

                $row->{"sem_ha_{$sem}"} = $val_ha;
                if($val_ha > 0){
                    $total_ha->{"sem_ha_{$sem}_sum"} += $val_ha;
                    $total_ha->{"sem_ha_{$sem}_count"} += 1;
                    $total_ha->{"sem_ha_{$sem}"} = round($total_ha->{"sem_ha_{$sem}_sum"}/$total_ha->{"sem_ha_{$sem}_count"}, 2);
                }

                if($max == null || $max < $val && $val > 0) $max = $val;
                if($min == null || $min > $val && $val > 0) $min = $val;
                if($val > 0) $count++;
                $sum += $val;

                if($max_ha == null || $max_ha < $val_ha && $val_ha > 0) $max_ha = $val_ha;
                if($min_ha == null || $min_ha > $val_ha && $val_ha > 0) $min_ha = $val_ha;
                if($val_ha > 0) $count_ha++;
                $sum_ha += $val_ha;
                
                /* FUNDAS USADAS POR SEMANA POR ENFUNDADOR */
                foreach($childs as $child){
                    $val_e = $this->db->queryOne("SELECT ROUND(SUM(usadas), 0) FROM produccion_enfunde WHERE years = $filters->year AND semana = $sem AND lote = $lote->lote AND id_enfundador = $child->key");
                    $val_e_ha = $this->db->queryOne("SELECT ROUND(SUM(usadas)/{$area_lote}, 2) FROM produccion_enfunde WHERE years = $filters->year AND semana = $sem AND lote = $lote->lote AND id_enfundador = $child->key");                    

                    $child->{"sem_{$sem}"} = $val_e;
                    $child->{"sem_ha_{$sem}"} = $val_e;

                    if((!isset($max_e[$child->key]) && $val_e > 0) || $max_e[$child->key] < $val_e && $val_e > 0) $max_e[$child->key] = $val_e;
                    if((!isset($min_e[$child->key]) && $val_e > 0) || $min_e[$child->key] > $val_e && $val_e > 0) $min_e[$child->key] = $val_e;
                    if($val_e > 0) $count_e[$child->key]++;
                    $sum_e[$child->key] += $val_e;

                    if((!isset($max_e_ha[$child->key]) && $val_e_ha > 0) || $max_e_ha[$child->key] < $val_e_ha && $val_e_ha > 0) $max_e_ha[$child->key] = $val_e_ha;
                    if((!isset($min_e_ha[$child->key]) && $val_e_ha > 0) || $min_e_ha[$child->key] > $val_e_ha && $val_e_ha > 0) $min_e_ha[$child->key] = $val_e_ha;
                    if($val_e_ha > 0) $count_e_ha[$child->key]++;
                    $sum_e_ha[$child->key] += $val_e_ha;
                }
            }

            /* FILAS EXPANDIBLES (MAX, MIN, AVG) */
            foreach($childs as $child){
                $child->max = $max_e[$child->key];
                $child->min = $min_e[$child->key];
                $child->avg = round($sum_e[$child->key] / $count_e[$child->key], 2);

                $child->max_ha = $max_e_ha[$child->key];
                $child->min_ha = $min_e_ha[$child->key];
                $child->avg_ha = round($sum_e_ha[$child->key] / $count_e_ha[$child->key], 2);
            }
            $row->children = $childs;

            $row->max = $max;
            $row->min = $min;
            $row->avg = round($sum / $count, 2);

            $row->max_ha = $max_ha;
            $row->min_ha = $min_ha;
            $row->avg_ha = round($sum_ha / $count_ha, 2);
            $response->data[] = $row;
        }

        $max = null; $max_ha = null;
        $min = null; $min_ha = null;
        $sum = 0; $sum_ha = 0;
        $count = 0; $count_ha = 0;
        foreach($total as $key => $val){
            if(strpos($key,"_sum") === false && strpos($key,"_count") === false){
                if($max == null || $max < $val && $val > 0) $max = $val;
                if($min == null || $min > $val && $val > 0) $min = $val;
                if($val > 0) $count++;
                $sum += $val;
            }
        }
        foreach($total_ha as $key => $val_ha){
            if(strpos($key,"_sum") === false && strpos($key,"_count") === false){
                $total->{$key} = $val_ha;
                if($max_ha == null || $max_ha < $val_ha && $val_ha > 0) $max_ha = $val_ha;
                if($min_ha == null || $min_ha > $val_ha && $val_ha > 0) $min_ha = $val_ha;
                if($val_ha > 0) $count_ha++;
                $sum_ha += $val_ha;
            }
        }
        $total->lote = "TOTAL";
        $total->min = $min;
        $total->max = $max;
        $total->avg = round($sum / $count, 2);

        $total->min_ha = $min_ha;
        $total->max_ha = $max_ha;
        $total->avg_ha = round($sum_ha / $count_ha, 2);

        $response->data[] = $total;

        return $response;
    }

    public function enfundadoresSemanal(){
        $response = new stdClass;
        $filters = $this->getParams();
        if($filters->year == ""){
            $filters->year = 'YEAR(CURRENT_DATE)';
        }

        $response->semanas = $this->db->queryAllSpecial("SELECT semana AS id, semana AS label FROM produccion_enfunde WHERE years = $filters->year GROUP BY semana ORDER BY semana");
        $enfundadores = $this->db->queryAll("SELECT e.id, e.nombre AS enfundador FROM produccion_enfunde INNER JOIN cat_enfundadores e ON id_enfundador = e.id WHERE years = $filters->year GROUP BY e.id");
        $response->data = [];
        foreach($enfundadores as $enf){
            $row = new stdClass;
            $row->enfundador = $enf->enfundador;

            $max = null; $max_ha = null;
            $min = null; $min_ha = null;
            $sum = 0; $sum_ha = 0;
            $count = 0; $count_ha = 0;
            foreach($response->semanas as $sem){
                $val = $this->db->queryOne("SELECT ROUND(SUM(usadas), 0) FROM produccion_enfunde WHERE years = $filters->year AND semana = $sem AND id_enfundador = $enf->id");
                $val_ha = $this->db->queryOne("SELECT ROUND(SUM(usadas/IFNULL(hectareas, 1)), 2) FROM produccion_enfunde INNER JOIN lotes ON lotes.nombre = lote WHERE years = $filters->year AND semana = $sem AND id_enfundador = $enf->id");
                $row->{"sem_{$sem}"} = $val;
                $total->{"sem_{$sem}"} += $val;

                $row->{"sem_ha_{$sem}"} = $val_ha;
                $total_ha->{"sem_ha_{$sem}"} += $val_ha;

                if($max == null || $max < $val && $val > 0) $max = $val;
                if($min == null || $min > $val && $val > 0) $min = $val;
                if($val > 0) $count++;
                $sum += $val;

                if($max_ha == null || $max_ha < $val_ha && $val_ha > 0) $max_ha = $val_ha;
                if($min_ha == null || $min_ha > $val_ha && $val_ha > 0) $min_ha = $val_ha;
                if($val_ha > 0) $count_ha++;
                $sum_ha += $val_ha;
            }

            $row->max = $max;
            $row->min = $min;
            $row->avg = round($sum / $count, 2);
            $row->max_ha = $max_ha;
            $row->min_ha = $min_ha;
            $row->avg_ha = round($sum_ha / $count_ha, 2);
            $response->data[] = $row;
        }

        $max = null; $max_ha = null;
        $min = null; $min_ha = null;
        $sum = 0; $sum_ha = 0;
        $count = 0; $count_ha = 0;
        foreach($total as $val){
            if($max == null || $max < $val && $val > 0) $max = $val;
            if($min == null || $min > $val && $val > 0) $min = $val;
            if($val > 0) $count++;
            $sum += $val;
        }
        foreach($total_ha as $key => $val_ha){
            $total->{$key} = $val_ha;
            if($max_ha == null || $max_ha < $val_ha && $val_ha > 0) $max_ha = $val_ha;
            if($min_ha == null || $min_ha > $val_ha && $val_ha > 0) $min_ha = $val_ha;
            if($val_ha > 0) $count_ha++;
            $sum_ha += $val_ha;
        }
        $total->enfundador = "TOTAL";
        $total->min = $min;
        $total->max = $max;
        $total->avg = round($sum / $count, 2);
        $total->min_ha = $min_ha;
        $total->max_ha = $max_ha;
        $total->avg_ha = round($sum_ha / $count_ha, 2);
        $response->data[] = $total;

        return $response;
    }

    public function graficaSemanal(){
        $response = new stdClass;
        $filters = $this->getParams();

        if($filters->year == ""){
            $filters->year = 'YEAR(CURRENT_DATE)';
        }

        $sql = "SELECT semanas.semana AS label , 
                        anios.anio AS legend,
                        IF(valores.valor = 0, NULL, valores.valor) AS value,
                        TRUE AS selected
                FROM (
                    SELECT years as anio
                    FROM produccion_enfunde
                    GROUP BY years
                ) as anios
                JOIN (
                    SELECT semana
                    FROM produccion_enfunde
                    GROUP BY semana
                ) as semanas
                LEFT JOIN (
                    SELECT years, semana, SUM(usadas) as valor
                    FROM produccion_enfunde
                    GROUP BY years, semana
                ) as valores ON valores.years = anios.anio AND valores.semana = semanas.semana";
        
        $sql_ha = "SELECT semana AS label , 
                        years AS legend,
                        ROUND(AVG(usadas), 2) AS value,
                        TRUE AS selected
                    FROM (
                        SELECT years, semana, lote, SUM(usadas)/hectareas AS usadas
                        FROM produccion_enfunde
                        INNER JOIN lotes ON lotes.nombre = lote
                        GROUP BY years, semana, lote
                    ) AS tbl
                    GROUP BY years, semana";
        
        // DB, SQL, TYPE
        $response->data = $this->generateSeries($this->db, $sql, "line", null, true);
        $response->data_ha = $this->generateSeries($this->db, $sql_ha, "line", null, true);

        /*$sql_temp_min = "SELECT 
                            semana AS label,
                            CONCAT('TEMP.M ', anio) AS legend,
                            temp_minima AS value,
                            IF(anio = YEAR(CURRENT_DATE), TRUE, FALSE) AS selected,
                            1 AS yAxisIndex
                        FROM `datos_clima_resumen`
                        WHERE id_hacienda = 14
                        GROUP BY anio";
        $data_min = $this->generateSeries($this->db_sigat, $sql_temp_min, "min", null, true);
        foreach($data_min->data as $name => $values){
            $response->data->data[$name] = $values;
            $response->data_ha->data[$name] = $values;
        }*/

        // GET UMBRAL
        $response->umbral = $this->db->queryOne("SELECT ROUND(AVG(usadas), 2) FROM (SELECT SUM(usadas) AS usadas FROM produccion_enfunde WHERE years = YEAR(CURRENT_DATE) GROUP BY semana) AS tbl");
        $response->umbral_ha = $this->db->queryOne("SELECT ROUND(AVG(usadas_semana), 2) FROM (SELECT AVG(usadas) AS usadas_semana FROM (SELECT SUM(usadas)/hectareas AS usadas, semana FROM produccion_enfunde INNER JOIN lotes ON lotes.nombre = produccion_enfunde.lote WHERE years = YEAR(CURRENT_DATE) GROUP BY semana, lote) AS tbl GROUP BY semana) AS tbl");

        return $response;
    }

    public function graficaPorLote(){
        $response = new stdClass;
        $filters = $this->getParams();
        if($filters->year == ""){
            $filters->year = 'YEAR(CURRENT_DATE)';
        }

        $sql = "SELECT semana AS label , 
                        lote AS legend,
                        SUM(usadas) AS value,
                        true AS selected
                FROM produccion_enfunde
                WHERE lote != 0 AND years = {$filters->year}
                GROUP BY lote, semana";

        $sql_ha = "SELECT semana AS label , 
                    lote AS legend,
                    ROUND(SUM(usadas)/hectareas,2) AS value,
                    true AS selected
                FROM produccion_enfunde
                INNER JOIN lotes ON lotes.nombre = lote
                WHERE lote != 0 AND years = {$filters->year}
                GROUP BY lote, semana";
        
        // DB, SQL, TYPE
        $response->data = $this->generateSeries($this->db, $sql, "line");
        $response->data_ha = $this->generateSeries($this->db, $sql_ha, "line");

        // GET UMBRAL
        $response->umbral = $this->db->queryOne("SELECT ROUND(AVG(usadas), 2) FROM(SELECT AVG(usadas) AS usadas FROM (SELECT SUM(usadas) AS usadas, semana, lote FROM produccion_enfunde WHERE years = YEAR(CURRENT_DATE) GROUP BY lote, semana) AS tbl WHERE lote != '0' GROUP BY semana) AS tbl");
        $response->umbral_ha = $this->db->queryOne("SELECT ROUND(AVG(usadas), 2) FROM(SELECT AVG(usadas) AS usadas FROM (SELECT SUM(usadas)/hectareas AS usadas, semana, lote FROM produccion_enfunde INNER JOIN lotes ON lotes.nombre = lote WHERE years = YEAR(CURRENT_DATE) GROUP BY lote, semana) AS tbl WHERE lote != '0' GROUP BY semana) AS tbl");

        return $response;
    }

    public function enfundadoresLote(){
        $response = new stdClass;
        $filters = $this->getParams();
        if($filters->year == ""){
            $filters->year = 'YEAR(CURRENT_DATE)';
        }
        if($filters->semana == ""){
            $filters->semana = 'getWeek(CURRENT_DATE)';
        }

        $sum = 0;
        $sum_ha = 0;
        $count = 0;

        $response->lotes = $this->db->queryAll("SELECT lote FROM produccion_enfunde WHERE semana = $filters->semana AND years = $filters->year GROUP BY lote");
        $response->data = $this->db->queryAll("SELECT e.id, e.nombre AS enfundador FROM produccion_enfunde INNER JOIN cat_enfundadores e ON id_enfundador = e.id WHERE semana = $filters->semana AND years = $filters->year GROUP BY id_enfundador ORDER BY e.nombre");
        foreach($response->data as $row){
            foreach($response->lotes as $lote){
                $row->{$lote->lote} = $this->db->queryOne("SELECT SUM(usadas) FROM produccion_enfunde WHERE lote = $lote->lote AND semana = $filters->semana AND years = $filters->year AND id_enfundador = $row->id");
                $row->{$lote->lote."_ha"} = $this->db->queryOne("SELECT ROUND(SUM(usadas)/hectareas,2) FROM produccion_enfunde INNER JOIN lotes ON lotes.nombre = lote WHERE lote = $lote->lote AND semana = $filters->semana AND years = $filters->year AND id_enfundador = $row->id");
                $row->total += $row->{$lote->lote};
                $row->total_ha += $row->{$lote->lote."_ha"};
            }
        }
        return $response;
    }

    private function tablePrincipal(){
        $response = new stdClass;
        $filters = $this->getParams();

        if($filters->year == ""){
            $filters->year = 'YEAR(CURRENT_DATE)';
        }
        if($filters->semana == ""){
            $filters->semana = 'getWeek(CURRENT_DATE)';
        }
        $response->dias = $this->db->queryAll("SELECT CASE DAYOFWEEK(fecha)
                    WHEN 1 THEN 'DOM'
                    WHEN 2 THEN 'LUN'
                    WHEN 3 THEN 'MAR'
                    WHEN 4 THEN 'MIE'
                    WHEN 5 THEN 'JUE'
                    WHEN 6 THEN 'VIE'
                    WHEN 7 THEN 'SAB'
                END AS nombre,
                fecha
            FROM produccion_enfunde
            WHERE semana = $filters->semana AND years = $filters->year
            GROUP BY DAYOFWEEK(fecha)");
        $response->anios = $this->db->queryAll("SELECT years AS anio FROM produccion_enfunde GROUP BY years ORDER BY years DESC");
        $response->semanas = $this->db->queryAll("SELECT *, (SELECT class FROM produccion_colores WHERE color = tbl.cinta) AS class
            FROM(
                SELECT semana AS sem, 
                            (SELECT color FROM semanas_colores WHERE year = years AND semana = p.semana) AS cinta
                FROM produccion_enfunde p
                LEFT JOIN cat_enfundadores cat_e ON id_enfundador = cat_e.id 
                WHERE years = $filters->year
                GROUP BY semana
            ) AS tbl");

        $response->enfundadores = $this->db->queryAll("SELECT cat_e.id, cat_e.nombre FROM produccion_enfunde INNER JOIN cat_enfundadores cat_e ON id_enfundador = cat_e.id WHERE semana = $filters->semana AND years = $filters->year GROUP BY id_enfundador");
        foreach($response->enfundadores as $row){
            foreach($response->dias as $dia){
                $row->inicial[$dia->nombre] = $this->db->queryOne("SELECT saldo FROM produccion_enfunde_saldo_inicial WHERE fecha = '{$dia->fecha}' AND id_enfundador = $row->id");
                $row->{$dia->nombre} = $this->db->queryOne("SELECT SUM(usadas) FROM produccion_enfunde WHERE fecha = '{$dia->fecha}' AND id_enfundador = $row->id");
                $row->entregadas[$dia->nombre] = $this->db->queryOne("SELECT SUM(entregadas) FROM produccion_enfunde WHERE fecha = '{$dia->fecha}' AND id_enfundador = $row->id");
                $row->final[$dia->nombre] = $row->inicial[$dia->nombre] - $row->{$dia->nombre} + $row->entregadas[$dia->nombre];
                $row->total += $row->{$dia->nombre};
            }
        }
        return $response;
    }

    private function enfundadorSemanal(){
        $response = [];
        if($filters->year == ""){
            $filters->year = 'YEAR(CURRENT_DATE)';
        }
        $sql = "SELECT c.id, c.nombre AS enfundador, SUM(usadas) AS usadas
                FROM produccion_enfunde
                INNER JOIN cat_enfundadores c ON id_enfundador = c.id
                WHERE years = $filters->year
                GROUP BY c.id
                ORDER BY c.nombre";
        $response = $this->db->queryAll($sql);
        foreach($response as $row){
            $row->detalle = $this->db->queryAllSpecial("SELECT semana AS id, SUM(usadas) AS label
                FROM produccion_enfunde
                WHERE id_enfundador = $row->id AND years = $filters->year
                GROUP BY semana");
        }
        return $response;
    }
}
