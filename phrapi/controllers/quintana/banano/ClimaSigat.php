<?php
class ClimaSigat {
	
	private $db;
	private $session;
	
	public function __construct() {
        $this->db = DB::getInstance('sigat');
        $this->session = Session::getInstance();
    }

	public function index(){
        $response = new stdClass;
        $filters = $this->getFilters();

        $response->years = $this->db->queryAllOne("SELECT anio FROM datos_clima_resumen WHERE estacion_id = 'hdadomenica' GROUP BY anio");
        $response->gerentes = $this->db->queryAll("SELECT id, nombre AS label FROM cat_gerentes WHERE status = 1");

        $sWhere .= " AND estacion_id = 'hdadomenica'";

        // DATA GRAFICAS

        $sql = "SELECT semana AS label,
                    (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                    ROUND((SELECT temp_minima FROM datos_clima_resumen WHERE semana = estaciones.semana AND anio = $filters->year AND id_hacienda = estaciones.id_hacienda), 2) AS 'value'
                FROM(
                    SELECT estaciones.estacion_id, semana, id_hacienda
                    FROM (
                        SELECT semana
                        FROM datos_clima_resumen
                        WHERE anio = $filters->year AND estacion_id = 'hdadomenica'
                        GROUP BY semana
                    ) AS semanas
                    JOIN (
                        SELECT estacion_id
                        FROM datos_clima_resumen c
                        INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                        WHERE 1=1 $sWhere
                        GROUP BY estacion_id
                    ) AS estaciones
                    INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                    GROUP BY semana, estaciones.estacion_id
                ) AS estaciones
                GROUP BY semana, estacion_id";
                
        $response->grafica_temp_min = $this->generateSeries($sql);

        $sql = "SELECT semana AS label,
                    (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                    ROUND((SELECT temp_maxima FROM datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND id_hacienda = estaciones.id_hacienda), 2) AS 'value'
                FROM(
                    SELECT estaciones.estacion_id, semana, id_hacienda
                    FROM (
                        SELECT semana
                        FROM datos_clima_resumen
                        WHERE anio = $filters->year AND estacion_id = 'hdadomenica'
                        GROUP BY semana
                    ) AS semanas
                    JOIN (
                        SELECT estacion_id
                        FROM datos_clima_resumen c
                        INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                        WHERE 1=1 $sWhere
                        GROUP BY estacion_id
                    ) AS estaciones
                    INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                    GROUP BY semana, estaciones.estacion_id
                ) AS estaciones
                GROUP BY semana, estacion_id";
        $response->grafica_temp_max = $this->generateSeries($sql);

        $sql = "SELECT semana AS label,
                    (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                    IFNULL(ROUND((SELECT lluvia FROM datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND id_hacienda = estaciones.id_hacienda), 2), 0) AS 'value'
                FROM(
                    SELECT estaciones.estacion_id, semana, id_hacienda
                    FROM (
                        SELECT semana
                        FROM datos_clima_resumen
                        WHERE anio = $filters->year AND estacion_id = 'hdadomenica'
                        GROUP BY semana
                    ) AS semanas
                    JOIN (
                        SELECT estacion_id
                        FROM datos_clima_resumen c
                        INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                        WHERE 1=1 $sWhere
                        GROUP BY estacion_id
                    ) AS estaciones
                    INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                    GROUP BY semana, estaciones.estacion_id
                ) AS estaciones
                GROUP BY semana, estacion_id";
        $response->grafica_lluvia = $this->generateSeries($sql);

        $sql = "SELECT semana AS label,
                    (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                    ROUND((SELECT humedad FROM datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND id_hacienda = estaciones.id_hacienda), 2) AS 'value'
                FROM(
                    SELECT estaciones.estacion_id, semana, id_hacienda
                    FROM (
                        SELECT semana
                        FROM datos_clima_resumen
                        WHERE anio = $filters->year AND estacion_id = 'hdadomenica'
                        GROUP BY semana
                    ) AS semanas
                    JOIN (
                        SELECT estacion_id
                        FROM datos_clima_resumen c
                        INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                        WHERE 1=1 $sWhere
                        GROUP BY estacion_id
                    ) AS estaciones
                    INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                    GROUP BY semana, estaciones.estacion_id
                ) AS estaciones
                GROUP BY semana, estacion_id";
        $response->grafica_humedad = $this->generateSeries($sql);

        $sql = "SELECT label, legend, ROUND(suma_rad / dias_semana, 2) as 'value'
                FROM(
                    SELECT semana AS label,
                        (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                        (SELECT rad_solar from datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND id_hacienda = estaciones.id_hacienda) as suma_rad,
                        (SELECT dias_semana from datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND estacion_id = estaciones.estacion_id AND id_hacienda = estaciones.id_hacienda) as dias_semana
                    from (
                        SELECT estaciones.estacion_id, semana, id_hacienda
                        FROM (
                            SELECT semana
                            FROM datos_clima_resumen
                            WHERE anio = {$filters->year} AND estacion_id = 'hdadomenica'
                            GROUP BY semana
                        ) AS semanas
                        JOIN (
                            SELECT estacion_id
                            FROM datos_clima_resumen c
                            INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                            WHERE 1=1 $sWhere
                            GROUP BY estacion_id
                        ) AS estaciones
                        INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                        GROUP BY semana, estaciones.estacion_id
                    ) AS estaciones
                    GROUP BY semana, estacion_id
                ) AS estaciones";
        $response->grafica_rad_solar = $this->generateSeries($sql);

        $sql = "SELECT label, legend, horas_semana as 'value'
                FROM(
                    SELECT semana AS label,
                        (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                        (SELECT horas_luz_100 from datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND rad_solar > 100 AND id_hacienda = estaciones.id_hacienda) as horas_semana,
                        (SELECT dias_semana from datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND estacion_id = estaciones.estacion_id AND id_hacienda = estaciones.id_hacienda) as dias_semana
                    from (
                        SELECT estaciones.estacion_id, semana, id_hacienda
                        FROM (
                            SELECT semana
                            FROM datos_clima_resumen
                            WHERE anio = {$filters->year} AND estacion_id = 'hdadomenica'
                            GROUP BY semana
                        ) AS semanas
                        JOIN (
                            SELECT estacion_id
                            FROM datos_clima_resumen c
                            INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                            WHERE 1=1  $sWhere
                            GROUP BY estacion_id
                        ) AS estaciones
                        INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                        GROUP BY semana, estaciones.estacion_id
                    ) AS estaciones
                    GROUP BY semana, estacion_id
                ) AS estaciones";
        $response->grafica_horas_luz[100] = $this->generateSeries($sql);

        $sql = "SELECT label, legend, horas_semana as 'value'
                FROM(
                    SELECT semana AS label,
                        (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                        (SELECT horas_luz_150 from datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND rad_solar > 150 AND id_hacienda = estaciones.id_hacienda) as horas_semana
                    from (
                        SELECT estaciones.estacion_id, semana, id_hacienda
                        FROM (
                            SELECT semana
                            FROM datos_clima_resumen
                            WHERE anio = {$filters->year} AND estacion_id = 'hdadomenica'
                            GROUP BY semana
                        ) AS semanas
                        JOIN (
                            SELECT estacion_id
                            FROM datos_clima_resumen c
                            INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                            WHERE 1=1  $sWhere
                            GROUP BY estacion_id
                        ) AS estaciones
                        INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                        GROUP BY semana, estaciones.estacion_id
                    ) AS estaciones
                    GROUP BY semana, estacion_id
                ) AS estaciones";
        $response->grafica_horas_luz[150] = $this->generateSeries($sql);

        $sql = "SELECT label, legend, horas_semana as 'value'
                FROM(
                    SELECT semana AS label,
                        (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                        (SELECT horas_luz_200 from datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND rad_solar > 200 AND id_hacienda = estaciones.id_hacienda) as horas_semana
                    from (
                        SELECT estaciones.estacion_id, semana, id_hacienda
                        FROM (
                            SELECT semana
                            FROM datos_clima_resumen
                            WHERE anio = {$filters->year} AND estacion_id = 'hdadomenica'
                            GROUP BY semana
                        ) AS semanas
                        JOIN (
                            SELECT estacion_id
                            FROM datos_clima_resumen c
                            INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                            WHERE 1=1  $sWhere
                            GROUP BY estacion_id
                        ) AS estaciones
                        INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                        GROUP BY semana, estaciones.estacion_id
                    ) AS estaciones
                    GROUP BY semana, estacion_id
                ) AS estaciones";
        $response->grafica_horas_luz[200] = $this->generateSeries($sql);

        $sql = "SELECT label, legend, horas_semana as 'value'
                FROM(
                    SELECT semana AS label,
                        (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                        (SELECT horas_luz_400 from datos_clima_resumen WHERE anio = $filters->year AND semana = estaciones.semana AND rad_solar > 400 AND id_hacienda = estaciones.id_hacienda) as horas_semana
                    from (
                        SELECT estaciones.estacion_id, semana, id_hacienda
                        FROM (
                            SELECT semana
                            FROM datos_clima_resumen
                            WHERE anio = {$filters->year} AND estacion_id = 'hdadomenica'
                            GROUP BY semana
                        ) AS semanas
                        JOIN (
                            SELECT estacion_id
                            FROM datos_clima_resumen c
                            INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                            WHERE 1=1  $sWhere
                            GROUP BY estacion_id
                        ) AS estaciones
                        INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                        GROUP BY semana, estaciones.estacion_id
                    ) AS estaciones
                    GROUP BY semana, estacion_id
                ) AS estaciones";
        $response->grafica_horas_luz[400] = $this->generateSeries($sql);

        // DATA TABLAS

        $response->tablas = [];
        foreach($response->grafica_temp_min->data as $estacion => $row){
            $response->tablas[$estacion] = new stdClass;

            $response->tablas[$estacion]->temp_min = [ "detalle" => "TEMP MIN" ];
            $response->tablas[$estacion]->temp_max = [ "detalle" => "TEMP MAX" ];
            $response->tablas[$estacion]->lluvia = [ "detalle" => "LLUVIA" ];
            $response->tablas[$estacion]->humedad = [ "detalle" => "HUMEDAD" ];
            $response->tablas[$estacion]->rad_solar = [ "detalle" => "RAD SOLAR" ];
            $response->tablas[$estacion]->horas_luz_100 = [ "detalle" => "HORAS LUZ 100 (W/M2)" ];
            $response->tablas[$estacion]->horas_luz_150 = [ "detalle" => "HORAS LUZ 150 (W/M2)" ];
            $response->tablas[$estacion]->horas_luz_200 = [ "detalle" => "HORAS LUZ 200 (W/M2)" ];
            $response->tablas[$estacion]->horas_luz_400 = [ "detalle" => "HORAS LUZ 400 (W/M2)" ];

            // SET VALUES

            foreach($response->grafica_temp_min->legend as $index => $semana){
                if(!in_array($semana, $response->semanas))
                    $response->semanas[] = $semana;

                $response->tablas[$estacion]->temp_min["sem_{$semana}"] = ($response->grafica_temp_min->data[$estacion]->data[$index] > 0) ? ((float) $response->grafica_temp_min->data[$estacion]->data[$index]) : '';
                $response->tablas[$estacion]->temp_max["sem_{$semana}"] = ($response->grafica_temp_max->data[$estacion]->data[$index] > 0) ? ((float) $response->grafica_temp_max->data[$estacion]->data[$index]) : '';
                $response->tablas[$estacion]->lluvia["sem_{$semana}"] = (float) $response->grafica_lluvia->data[$estacion]->data[$index];
                $response->tablas[$estacion]->humedad["sem_{$semana}"] = ($response->grafica_humedad->data[$estacion]->data[$index] > 0) ? ((float)$response->grafica_humedad->data[$estacion]->data[$index]) : '';
                $response->tablas[$estacion]->rad_solar["sem_{$semana}"] = ($response->grafica_rad_solar->data[$estacion]->data[$index] > 0) ? ((float)$response->grafica_rad_solar->data[$estacion]->data[$index]) : '';
                $response->tablas[$estacion]->horas_luz_100["sem_{$semana}"] = (float) $response->grafica_horas_luz[100]->data[$estacion]->data[$index];
                $response->tablas[$estacion]->horas_luz_150["sem_{$semana}"] = (float) $response->grafica_horas_luz[150]->data[$estacion]->data[$index];
                $response->tablas[$estacion]->horas_luz_200["sem_{$semana}"] = (float) $response->grafica_horas_luz[200]->data[$estacion]->data[$index];
                $response->tablas[$estacion]->horas_luz_400["sem_{$semana}"] = (float) $response->grafica_horas_luz[400]->data[$estacion]->data[$index];
            }

            // MAX, MIN, AVG , TOTAL

            $response->tablas[$estacion]->temp_min["min"] = $this->getMin($response->grafica_temp_min->data[$estacion]->data);
            $response->tablas[$estacion]->temp_min["max"] = $this->getMax($response->grafica_temp_min->data[$estacion]->data);
            $response->tablas[$estacion]->temp_min["avg"] = $this->getAvg($response->grafica_temp_min->data[$estacion]->data);
            $response->tablas[$estacion]->temp_min["total"] = "";

            $response->tablas[$estacion]->temp_max["min"] = $this->getMin($response->grafica_temp_max->data[$estacion]->data);
            $response->tablas[$estacion]->temp_max["max"] = $this->getMax($response->grafica_temp_max->data[$estacion]->data);
            $response->tablas[$estacion]->temp_max["avg"] = $this->getAvg($response->grafica_temp_max->data[$estacion]->data);
            $response->tablas[$estacion]->temp_max["total"] = "";

            $response->tablas[$estacion]->lluvia["min"] = $this->getMin($response->grafica_lluvia->data[$estacion]->data, true);
            $response->tablas[$estacion]->lluvia["max"] = $this->getMax($response->grafica_lluvia->data[$estacion]->data, true);
            $response->tablas[$estacion]->lluvia["avg"] = $this->getAvg($response->grafica_lluvia->data[$estacion]->data, true);
            $response->tablas[$estacion]->lluvia["total"] = $this->getSum($response->grafica_lluvia->data[$estacion]->data, true);

            $response->tablas[$estacion]->rad_solar["min"] = $this->getMin($response->grafica_rad_solar->data[$estacion]->data);
            $response->tablas[$estacion]->rad_solar["max"] = $this->getMax($response->grafica_rad_solar->data[$estacion]->data);
            $response->tablas[$estacion]->rad_solar["avg"] = $this->getAvg($response->grafica_rad_solar->data[$estacion]->data);
            $response->tablas[$estacion]->rad_solar["total"] = "";

            $response->tablas[$estacion]->humedad["min"] = $this->getMin($response->grafica_humedad->data[$estacion]->data);
            $response->tablas[$estacion]->humedad["max"] = $this->getMax($response->grafica_humedad->data[$estacion]->data);
            $response->tablas[$estacion]->humedad["avg"] = $this->getAvg($response->grafica_humedad->data[$estacion]->data);
            $response->tablas[$estacion]->humedad["total"] = "";

            $response->tablas[$estacion]->horas_luz_100["min"] = $this->getMin($response->grafica_horas_luz[100]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_100["max"] = $this->getMax($response->grafica_horas_luz[100]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_100["avg"] = $this->getAvg($response->grafica_horas_luz[100]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_100["total"] = $this->getSum($response->grafica_horas_luz[100]->data[$estacion]->data);

            $response->tablas[$estacion]->horas_luz_150["min"] = $this->getMin($response->grafica_horas_luz[150]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_150["max"] = $this->getMax($response->grafica_horas_luz[150]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_150["avg"] = $this->getAvg($response->grafica_horas_luz[150]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_150["total"] = $this->getSum($response->grafica_horas_luz[150]->data[$estacion]->data);

            $response->tablas[$estacion]->horas_luz_200["min"] = $this->getMin($response->grafica_horas_luz[200]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_200["max"] = $this->getMax($response->grafica_horas_luz[200]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_200["avg"] = $this->getAvg($response->grafica_horas_luz[200]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_200["total"] = $this->getSum($response->grafica_horas_luz[200]->data[$estacion]->data);

            $response->tablas[$estacion]->horas_luz_400["min"] = $this->getMin($response->grafica_horas_luz[400]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_400["max"] = $this->getMax($response->grafica_horas_luz[400]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_400["avg"] = $this->getAvg($response->grafica_horas_luz[400]->data[$estacion]->data);
            $response->tablas[$estacion]->horas_luz_400["total"] = $this->getSum($response->grafica_horas_luz[400]->data[$estacion]->data);
        }
        return $response;
    }

    public function graficaHorasDia(){
        $response = new stdClass;
        $filters = $this->getFilters();

        $sWhere .= " AND estacion_id = 'hdadomenica'";

        $sql = "SELECT label, legend, cant AS 'value'
                FROM(
                    SELECT DATE_FORMAT(CONCAT(CURRENT_DATE,' ',hora_t), '%h %p') AS label,
                        (SELECT f.nombre FROM estaciones_haciendas INNER JOIN cat_haciendas f ON id_hacienda = f.id WHERE estacion_id = estaciones.estacion_id $sWhere) AS legend,
                        ROUND((SELECT AVG(rad_solar) FROM datos_clima WHERE HOUR(hora) = estaciones.hora AND fecha BETWEEN '$filters->fecha_inicial' AND '$filters->fecha_final' AND estacion_id = estaciones.estacion_id AND id_hacienda = estaciones.id_hacienda), 2) AS cant
                    FROM (
                        SELECT estaciones.estacion_id, hora, id_hacienda, hora_t
                        FROM (
                            SELECT HOUR(hora) AS hora, hora AS hora_t
                            FROM datos_clima
                            WHERE YEAR(fecha) = {$filters->year} AND estacion_id = 'hdadomenica'
                            GROUP BY HOUR(hora)
                        ) AS horas
                        JOIN (
                            SELECT estacion_id
                            FROM datos_clima c
                            INNER JOIN cat_haciendas f ON f.id = c.id_hacienda AND alias IS NOT NULL
                            WHERE 1=1 $sWhere
                            GROUP BY estacion_id
                        ) AS estaciones
                        INNER JOIN estaciones_haciendas e ON estaciones.estacion_id = e.estacion_id
                        GROUP BY hora, estaciones.estacion_id
                    ) AS estaciones
                    GROUP BY hora, estacion_id
                ) AS estaciones";
        $response->grafica_horas_dia = $this->generateSeries($sql);

        return $response;
    }
    
    private function getMin($data, $cero = false){
        $min = null;
        foreach($data as $val){
            if($min == null) {
                if(!$cero && $val > 0){
                    $min = $val;
                }else if($cero){
                    $min = $val;
                }
            }
            else{
                if(!$cero && $val > 0 && $val < $min){
                    $min = $val;
                }else if($cero && $val < $min){
                    $min = $val;
                }
            }
        }
        return (float) $min;
    }

    private function getMax($data, $cero = false){
        $max = null;
        foreach($data as $val){
            if($max == null) {
                if(!$cero && $val > 0){
                    $max = $val;
                }else if($cero){
                    $max = $val;
                }
            }
            else{
                if(!$cero && $val > 0 && $val > $max){
                    $max = $val;
                }else if($cero && $val > $max){
                    $max = $val;
                }
            }
        }
        return (float) $max;
    }

    private function getAvg($data, $cero = false){
        $total = 0;
        $count = 0;
        foreach($data as $val){
            if(!$cero && $val > 0){
                $total += $val;
                $count++;
            }else if($cero){
                $total += $val;
                $count++;
            }
        }
        return round($total / $count, 2);
    }

    private function getSum($data, $cero = false){
        $total = 0;
        foreach($data as $val){
            if(!$cero && $val > 0){
                $total += $val;
            }else if($cero){
                $total += $val;
            }
        }
        return round($total, 2);
    }

	private function getFilters(){
        $data = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        
        return $data;

        return $response;
    }

	private function generateSeries($sql){
		$res = $this->db->queryAll($sql);

		$response->data = [];
        $response->legend = [];
        
		$series = new stdClass;
		$markLine = new stdClass;
        $labels = [];
        
        $min = null;
        $max = null;
        $avg = null;
        $total = 0;
        $count = 0;

		foreach ($res as $key => $value) {
            if(!in_array($value->label, $response->legend)){
                $response->legend[] = $value->label;
            }
		}

        foreach ($res as $key => $value) {
            $value = (object)$value;
            $value->position = array_search($value->legend, $response->legend);
            $value->promedio = (float)$value->value;

            if(($min == null || $min > $value->value) && $value->value != null) $min = $value->value;
            if(($max == null || $max < $value->value) && $value->value != null) $max = $value->value;
            if($value->value){
                $total += (float) $value->value;
                $count++;
            }
			
			if(!isset($response->data[$value->legend]->itemStyle)){
				$series = new stdClass;
				$series->name = $value->legend;
				$series->type = "line";
				$series->connectNulls = true;
				$series->data[] = $value->value;
				$series->itemStyle = (object)[
					"normal" => [
						"barBorderWidth" => 6,
						"barBorderRadius" => 0,
						"label" => [ "show" => true , "position" => "insideTop"]
					]
				];
				$response->data[$value->legend] = $series;
			}else{
				$response->data[$value->legend]->data[] = $value->value;
			}
		}

        $response->min = (float) $min;
        $response->max = (float) $max;
        $response->avg = ROUND($total / $count, 2);
        $response->total = $total;

        return $response;
	}

    private function chartInit($data = [] , $mode = "vertical" , $name = "" , $type = "line" , $minMax = []){
		$response = new stdClass;
		$response->chart = [];
		if(count($data) > 0){
			$response->chart["title"]["show"] = true;
			$response->chart["title"]["text"] = $name;
			$response->chart["title"]["subtext"] = $this->session->sloganCompany;
			$response->chart["tooltip"]["trigger"] = "item";
			$response->chart["tooltip"]["axisPointer"]["type"] = "shadow";
			$response->chart["toolbox"]["show"] = true;
			$response->chart["toolbox"]["feature"]["mark"]["show"] = true;
			$response->chart["toolbox"]["feature"]["restore"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["show"] = true;
			$response->chart["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
			$response->chart["toolbox"]["feature"]["saveAsImage"]["show"] = true;
			$response->chart["legend"]["data"] = [];
			$response->chart["legend"]["left"] = "center";
			$response->chart["legend"]["orient"] = "vertical";
			$response->chart["legend"]["bottom"] = "1%";
			$response->chart["grid"]["left"] = "3%";
			$response->chart["grid"]["right"] = "4%";
			$response->chart["grid"]["bottom"] = "15%";
			$response->chart["grid"]["containLabel"] = true;
			if($mode == "vertical"){
				$response->chart["xAxis"] = ["type" => "category" , "data" => [""] , "axisLabel" => ["rotate" => 60] , "margin" => 2];
				$response->chart["yAxis"] = ["type" => "value"];
				if(isset($minMax["min"])){
					$response->chart["yAxis"] = ["type" => "value" , "min" => $minMax["min"], "max" => $minMax["max"]];
				}
			}else if($mode == "horizontal"){
				$response->chart["yAxis"] = ["type" => "category" , "data" => [""]];
				$response->chart["xAxis"] = ["type" => "value"];
			}
			$response->chart["series"] = [];
			$count = 0;
			$position = -1;
			$colors = [];
			if($type == "line"){
				$response->chart["xAxis"]["data"] = [];
			}
			foreach ($data as $key => $value) {
				$value = (object)$value;
				$value->legend = ucwords(strtolower($value->legend));
				$value->label = ucwords(strtolower($value->label));
				if(isset($value->position)){
					$position = $value->position;
				}
				if(isset($value->color)){
					$colors = [
						"normal" => ["color" => $value->color],
					];
				}

				if("line" == "line"){
					if(!in_array($value->label, $response->chart["xAxis"]["data"])){
						$response->chart["xAxis"]["data"][] = $value->label;
					}
					if(!in_array($value->legend, $response->chart["legend"]["data"])){
						if(!isset($value->position)){
							$position++;
						}
						$response->chart["legend"]["data"][] = $value->legend;
						$response->chart["legend"]["selected"][$value->legend] = ($value->selected == 0) ? false : true;
						$response->chart["series"][$position] = [
							"name" => $value->legend,
							"type" => $type,
							"data" => [],
							"label" => [
								"normal" => ["show" => false , "position" => "top"],
								"emphasis" => ["show" => false , "position" => "top"],
							],
							"itemStyle" => $colors
						];
					}

					$response->chart["series"][$position]["data"][] = ROUND($value->value,2);

				}
			}
		}

		return $response->chart;
	}
}

?>