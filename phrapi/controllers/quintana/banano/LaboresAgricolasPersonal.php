<?php defined('PHRAPI') or die("Direct access not allowed!");

class LaboresAgricolasPersonal {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
        $this->num_plantas_per_json = 10;
    }

    public function index(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if($postdata->finca > 0){
            $sWhere .= " AND muestras.idFinca = $postdata->finca";
        }

        $response->umbrals = $this->session->umbrals;
        
        $response->periodos = $this->db->queryAll("SELECT periodo FROM muestras WHERE anio = YEAR(CURRENT_DATE) AND periodo > 0 $sWhere GROUP BY periodo");
        $sql = "SELECT idEmpleado, empleado as zona
                FROM muestras
                WHERE anio = YEAR(CURRENT_DATE) $sWhere
                GROUP BY idEmpleado";
        $response->data = $this->db->queryAll($sql);
        foreach($response->data as $empleado){
            #segundo nivel
            $lotes = $this->db->queryAll("SELECT idLote, lotes.nombre AS lote, CONCAT('LOTE ', lotes.nombre) AS zona, 'LOTE' as tipo FROM muestras INNER JOIN lotes ON idLote = lotes.id WHERE anio = YEAR(CURRENT_DATE) AND idEmpleado = $empleado->idEmpleado $sWhere GROUP BY idLote");
            foreach($lotes as $fila2){
                $idLote = $fila2->idLote;

                #tercer nivel (labor)
                $labores = $this->db->queryAll("SELECT idLabor, l.nombre AS labor, l.nombre AS zona, 'LABOR' AS tipo
                    FROM muestras
                    INNER JOIN labores l ON l.id = idLabor AND status = 1
                    WHERE anio = YEAR(CURRENT_DATE) AND idLote = {$idLote} AND idEmpleado = $empleado->idEmpleado $sWhere
                    GROUP BY idLabor
                    ORDER BY l.nombre
                ");
                foreach($labores as $fila3){
                    $idLabor = $fila3->idLabor;
                    $ponderacion_labor = $this->db->queryOne("SELECT SUM(valor) FROM labores_causas WHERE idLabor = $idLabor");

                    foreach($response->periodos as $s3){
                        $periodo3 = $s3->periodo;
                        $suma_ponderacion = $this->db->queryOne("SELECT SUM(valor)
                            FROM muestras
                            WHERE anio = YEAR(CURRENT_DATE) 
                                AND periodo = {$periodo3} 
                                AND idLote = {$idLote} 
                                AND idLabor = {$idLabor} 
                                AND idEmpleado = $empleado->idEmpleado
                                $sWhere
                        ");
                        $fila3->{"PERIODO {$periodo3}"} = round(100 - ($suma_ponderacion / ($ponderacion_labor * $this->num_plantas_per_json) * 100), 2);
                    }

                    #cuarto nivel (causa)
                    /* (MALO)
                        PORCENTAJE DE CAUSAS MALAS
                    */
                    $causas = $this->db->queryAll("SELECT idLaborCausa, laborCausaDesc AS causa, laborCausaDesc AS zona, 'CAUSA' AS tipo
                        FROM muestras 
                        WHERE anio = YEAR(CURRENT_DATE) 
                            AND idLote = {$idLote} 
                            AND idLabor = {$idLabor}
                            AND idEmpleado = $empleado->idEmpleado
                            $sWhere
                        GROUP BY laborCausaDesc");
                    foreach($causas as $fila4){
                        foreach($response->periodos as $s4){
                            $periodo4 = $s4->periodo;
                            $suma_ponderacion = $this->db->queryOne("SELECT SUM(valor)
                                FROM muestras
                                WHERE anio = YEAR(CURRENT_DATE) 
                                    AND periodo = {$periodo4}
                                    AND idLote = {$idLote}
                                    AND idLabor = {$idLabor} 
                                    AND laborCausaDesc = '{$fila4->causa}'
                                    AND idEmpleado = $empleado->idEmpleado
                                    $sWhere");
                            $fila4->{"PERIODO {$periodo4}"} = round($suma_ponderacion / ($ponderacion_labor * $this->num_plantas_per_json) * 100, 2);
                        }
                    }
                    $fila3->children = $causas;
                }
                $fila2->children = $labores;

                /*
                    PROMEDIO PONDERADO A LAS LABORES PARA CALCULAR EL VALOR POR LOTE
                */
                foreach($response->periodos as $s2){
                    $periodo2 = $s2->periodo;
                    $sum = 0;
                    $count = 0;
                    foreach($fila2->children as $labor){
                        $sum += $labor->{"PERIODO {$periodo2}"};
                        $count++;
                    }
                    $fila2->{"PERIODO {$periodo2}"} = round($sum / $count, 2);
                }
            }
            $empleado->children = $lotes;

            /*
                PROMEDIO PONDERADO A LOS LOTES PARA CALCULAR EL VALOR POR PERSONA
            */
            foreach($response->periodos as $ss){
                $periodo = $ss->periodo;
                $sum = 0;
                $count = 0;
                foreach($empleado->children as $finca){
                    $sum += $finca->{"PERIODO {$periodo}"};
                    $count++;
                }
                $empleado->{"PERIODO {$periodo}"} = round($sum / $count, 2);
            }
        }

        return $response;
    }
}