<?php defined('PHRAPI') or die("Direct access not allowed!");

class Produccion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->maindb = DB::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
	}

	public function lastDay(){
		$response = new stdClass;
		$response->last = $this->db->queryRow("SELECT MAX(fecha) AS fecha FROM produccion_racimos");
		$response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha = '{$response->last->fecha}' GROUP BY id_finca");
		return $response;
	}

	public function historico(){
		$postdata = (object)json_decode(file_get_contents("php://input"));

		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }
        
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }
        
		$response->data = $this->db->queryAll("SELECT historica.id, fecha, lote, causa, peso, IF(tipo = 'RECU', 0 , manos) AS manos, IF(tipo = 'RECU', 0, calibre) AS calibre, cinta, IF(tipo = 'RECU', 0, dedos) AS dedos, colores.class, historica.edad, cuadrilla, hora, tipo
			FROM produccion_racimos historica
			LEFT JOIN produccion_colores colores ON colores.color = cinta
			WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' $sWhere");
		return $response;
	}

	public function index(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
            "sector" => getValueFrom($postdata , "sector" , "" , FILTER_SANITIZE_STRING),
        ];

        $response = new stdClass;
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($response->fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($response->fincas)[0];
        }

		$sWhere = "";
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->sector != ""){
			$sWhere .= " AND sector = '{$filters->sector}'";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

		$response->totales = (object)[
			"total_cosechados" => 0,
			"total_recusados" => 0,
			"total_procesada" => 0,
			"muestreados" => 0,
			"porcen" => 100,
			"peso" => 0,
			"manos" => 0,
			"calibracion" => 0,
			"dedos" => 0
		];

		$sql = "SELECT lote, 
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND lote = produccion.lote  {$sWhere}) AS total_recusados,
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote  {$sWhere}) AS total_procesados,
                    ROUND((SELECT AVG(edad) FROM produccion_racimos WHERE lote = produccion.lote {$sWhere}), 2) AS edad,
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados,
					(SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote  {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM(
                        SELECT lote
                        FROM produccion_racimos
                        WHERE 1=1 {$sWhere}
                        GROUP BY lote
                    ) AS tbl
                    GROUP BY lote
                ) AS produccion";

		$response->data = $this->db->queryAll($sql);

		foreach ($response->data as $key => $value) {
			$value->expanded = false;
			$value->total_cosechados = $value->total_procesados + $value->total_recusados;

			$value->cables = $this->db->queryAll("SELECT cinta AS cable, edad, class,
                                (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere}) AS total_recusados, 
                                (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere}) AS total_procesados, 
								(SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere}) AS peso,
								(SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
								(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
								(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
							FROM(
                                SELECT *
                                FROM (
                                    SELECT cinta , produccion.edad, class
                                    FROM produccion_racimos produccion
                                    LEFT JOIN produccion_colores colores ON cinta = color
                                    WHERE 1 = 1 AND lote = '{$value->lote}'  {$sWhere}
                                    GROUP BY cinta
                                ) AS tbl
                                GROUP BY cinta
                            ) AS produccion");
			foreach ($value->cables as $llave => $valor) {
				$valor->expanded = false;
				$valor->total_cosechados = $valor->total_procesados + $valor->total_recusados;

				$valor->cuadrillas = $this->db->queryAll("SELECT cuadrilla,                        
                        (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS total_recusados, 
                        (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND calibre > 0 AND cuadrilla = produccion.cuadrilla {$sWhere}) AS muestreados, 
                        (SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS peso,
                        (SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
                        (SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
                        (SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = '{$value->lote}' AND cinta = '{$valor->cable}' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
                    FROM(
                        SELECT cuadrilla
                        FROM produccion_racimos historica
                        LEFT JOIN produccion_colores colores ON cinta = color
                        WHERE 1 = 1 {$sWhere} AND lote = '{$value->lote}' AND cinta = '{$valor->cable}'
                        GROUP BY cuadrilla
                    ) AS produccion");
				foreach($valor->cuadrillas as $row){
					$row->total_cosechados = $row->procesados + $row->total_recusados;
				}
			}
		}

        $sql_original = "SELECT SUM(cant) AS cant
            FROM(
                SELECT lote, cinta, COUNT(1) AS cant 
                FROM produccion_racimos
                WHERE tipo = 'PROC' $sWhere
                GROUP BY lote, cinta
            ) AS tbl";

        $response->totales->total_procesada = $this->db->queryOne($sql_original);

        $sql_original = "SELECT SUM(cant) AS cant
        FROM(
            SELECT lote, cinta, COUNT(1) AS cant 
            FROM produccion_racimos 
            WHERE 1=1 $sWhere
            GROUP BY lote, cinta
        ) AS tbl";

        $response->totales->total_cosechados = $this->db->queryOne($sql_original);
        $response->totales->muestreados = $response->totales->total_procesada + $response->totales->total_cosechados;

        $response->id_company = $this->session->id_company;

        $sql_original = "SELECT SUM(cant) AS cant
        FROM(
            SELECT lote, cinta, COUNT(1) AS cant 
            FROM produccion_racimos 
            WHERE tipo = 'RECU' $sWhere
            GROUP BY lote, cinta
        ) AS tbl";

        $response->totales->recusados = $this->db->queryOne($sql_original);
		$response->totales->peso = $this->db->queryRow("SELECT AVG(peso) as suma FROM produccion_racimos WHERE tipo = 'PROC' {$sWhere}")->suma;
		$response->totales->manos = $this->db->queryRow("SELECT AVG(manos) as suma FROM produccion_racimos WHERE tipo = 'PROC' AND manos IS NOT NULL AND manos > 0 AND cinta != 'n/a' {$sWhere}")->suma;
		$response->totales->calibracion = $this->db->queryRow("SELECT AVG(calibre) as suma FROM produccion_racimos WHERE tipo = 'PROC' AND calibre IS NOT NULL AND calibre > 0 AND cinta != 'n/a' {$sWhere}")->suma;
		$response->totales->dedos = $this->db->queryRow("SELECT AVG(dedos) as suma FROM produccion_racimos WHERE tipo = 'PROC' AND dedos IS NOT NULL AND dedos > 0 AND cinta != 'n/a' {$sWhere}")->suma;
        $response->totales->porcen = ($response->totales->muestreados / $response->totales->total_cosechados) * 100;
        $response->totales->edad = $this->db->queryRow("SELECT ROUND(AVG(edad), 2) AS edad FROM produccion_racimos WHERE tipo = 'PROC' {$sWhere}")->edad;

        #$suma_peso_proc = (float) $this->db->queryOne("SELECT SUM(IF(blz > form, blz, form))-SUM(blz) FROM produccion_racimos_cuadrado WHERE status = 'PROC' $sWhere");
        $suma_peso_proc = (float) $response->totales->peso;
        $response->tags["kg_proc"] = $this->db->queryOne("SELECT SUM(peso)+($suma_peso_proc) FROM produccion_racimos WHERE tipo = 'PROC' $sWhere");

        #$suma_peso_proc = (float) $this->db->queryOne("SELECT SUM(IF(blz > form, blz, form))-SUM(blz) FROM produccion_racimos_cuadrado WHERE status = 'RECU' $sWhere");
        $suma_peso_proc = (float) $response->totales->peso;
        $response->tags["kg_recu"] = $this->db->queryOne("SELECT SUM(peso)+($suma_peso_proc) FROM produccion_racimos WHERE tipo = 'RECU' $sWhere");

        $times = $this->db->queryAll("SELECT fecha, hora FROM produccion_racimos WHERE 1=1 AND hora != '' $sWhere ORDER BY hora");
        $p = $times[0];
        $u = $times[count($times)-1];
        $response->tags["ultima_fecha"] = $u->fecha;
        $response->tags["ultima_hora"] = $u->hora;
        $response->tags["primera_fecha"] = $p->fecha;
        $response->tags["primera_hora"] = $p->hora;
        $response->tags["diferencia"] = $this->db->queryRow("SELECT TIMEDIFF('{$u->hora}', '{$p->hora}') AS dif")->dif;
		return $response;
	}

	public function racimosPalanca(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];
        
        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

		$sql = "SELECT cuadrilla,
					(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND cuadrilla = produccion.cuadrilla  {$sWhere}) AS recusados , 
					(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla  {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND calibre > 0 {$sWhere}) AS muestreados, 
                    ROUND((SELECT AVG(edad) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND edad > 0 {$sWhere}), 2) AS edad,
					(SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla  {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = produccion.cuadrilla AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
				SELECT cuadrilla
				FROM produccion_racimos produccion
				WHERE 1 = 1  {$sWhere}
				GROUP BY cuadrilla) AS produccion";
		$response->palancas = $this->db->queryAll($sql);
		foreach($response->palancas as $val){
			$val->cosechados = $val->procesados + $val->recusados;

			$val->lotes = $this->db->queryAll("SELECT lote,
					(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote {$sWhere}) AS recusados , 
					(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados, 
                    ROUND((SELECT AVG(edad) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = produccion.lote AND edad > 0 {$sWhere}), 2) AS edad, 
					(SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}'AND lote = produccion.lote  {$sWhere}) AS peso,
					(SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = produccion.lote AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = produccion.lote AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = produccion.lote AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
				SELECT lote
				FROM produccion_racimos produccion
				WHERE cuadrilla = '{$val->cuadrilla}' {$sWhere}
				GROUP BY lote) AS produccion");
			foreach($val->lotes as $value){
				$value->cosechados = $value->procesados + $value->recusados;
                
                $sql = "SELECT cinta AS cable, produccion.edad, class,
                            (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere}) AS recusados , 
                            (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta {$sWhere}) AS procesados, 
                            (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND calibre > 0 {$sWhere}) AS muestreados, 
                            (SELECT AVG(edad) FROM produccion_racimos WHERE cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND edad > 0 {$sWhere}) AS edad,
                            (SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' AND cinta = produccion.cinta  {$sWhere}) AS peso,
                            (SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
                            (SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
                            (SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND cuadrilla = '{$val->cuadrilla}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cinta = produccion.cinta AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
                        FROM(
                        SELECT cinta , produccion.edad, class
                        FROM produccion_racimos produccion
                        LEFT JOIN produccion_colores colores ON cinta = color
                        WHERE cuadrilla = '{$val->cuadrilla}' AND lote = '{$value->lote}' {$sWhere}
                        GROUP BY cinta) AS produccion";
                $value->cables = $this->db->queryAll($sql);
                    
				foreach($value->cables as $row){
					$row->cosechados = $row->procesados + $row->recusados;
				}
			}
		}
		return $response;
	}

	public function racimosEdad(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
            "sector" => getValueFrom($postdata , "sector" , "" , FILTER_SANITIZE_STRING),
        ];
        
        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }
        $sWhere = "";
		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->sector != ''){
            $sWhere .= " AND sector = '{$filters->sector}'";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

		$sql = "SELECT cinta AS cable, edad AS edad, class,
					(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND cinta = produccion.cinta  {$sWhere}) AS recusados, 
    				(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere}) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta AND calibre_segunda > 0 {$sWhere}) AS muestreados, 
                    (SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta  {$sWhere}) AS peso,
					(SELECT AVG(calibre_segunda) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre_segunda IS NOT NULL AND calibre_segunda > 0 {$sWhere}) AS calibracion,
                    (SELECT AVG(calibre_ultima) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND calibre_ultima IS NOT NULL AND calibre_ultima > 0 {$sWhere}) AS calibre_ultima,
					(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = produccion.cinta AND cinta != 'n/a' AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM (
                        SELECT cinta , produccion.edad, class
                        FROM produccion_racimos produccion
                        LEFT JOIN produccion_colores colores ON cinta = color
                        WHERE 1 = 1  {$sWhere}
                        GROUP BY cinta
                    ) AS tbl
                    GROUP BY cinta
                ) AS produccion";
                
		$response->data = $this->db->queryAll($sql);
		foreach($response->data as $i => $val){
			$val->expanded = false;
            $val->cosechados = $val->procesados + $val->recusados;
			$val->lotes = $this->db->queryAll("SELECT lote, 
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = '{$val->cable}' AND lote = produccion.lote AND status = 'RECU'),
					    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere})) AS recusados,
                    IFNULL((SELECT SUM(IF(blz > form, blz, form)) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = '{$val->cable}' AND lote = produccion.lote AND status = 'PROC'),
					    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere})) AS procesados, 
                    (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote AND calibre > 0 {$sWhere}) AS muestreados, 
                    IFNULL((SELECT IF(SUM(blz) = 0, (SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' {$sWhere}), NULL) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' AND cinta = '{$val->cable}' AND lote = produccion.lote),
					    (SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = produccion.lote {$sWhere})) AS peso,
					(SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
					(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
					(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = produccion.lote AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
				FROM(
                    SELECT *
                    FROM(
                        SELECT lote
                        FROM produccion_racimos
                        WHERE 1=1 AND cinta = '{$val->cable}' {$sWhere}
                        GROUP BY lote
                    ) AS tbl
                    GROUP BY lote
                ) AS produccion");
			foreach($val->lotes as $key => $value){
				$value->expanded = false;
				$value->cosechados = $value->procesados + $value->recusados;

				$value->cuadrillas = $this->db->queryAll("SELECT cuadrilla, 
						(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'RECU' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS recusados,
						(SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS procesados, 
                        (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre > 0 {$sWhere}) AS muestreados, 
						(SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla {$sWhere}) AS peso,
						(SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND calibre IS NOT NULL AND calibre > 0 {$sWhere}) AS calibracion,
						(SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND manos IS NOT NULL AND manos > 0 {$sWhere}) AS manos,
						(SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND cinta = '{$val->cable}' AND cinta != 'n/a' AND lote = '{$value->lote}' AND cuadrilla = produccion.cuadrilla AND dedos IS NOT NULL AND dedos > 0 {$sWhere}) AS dedos
					FROM(
					SELECT cuadrilla
					FROM produccion_racimos
					WHERE 1=1 AND cinta = '{$val->cable}' AND lote = '{$value->lote}' {$sWhere}
					GROUP BY cuadrilla) AS produccion");
				foreach($value->cuadrillas as $row){
					$row->cosechados = $row->procesados + $row->recusados;
				}
			}
		}
		return $response;
	}

	public function promediosLotes(){
		$postdata = (object)json_decode(file_get_contents("php://input"));
		$filters = (object)[
			"finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
			"fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
			"fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];
        
        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

		if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
			$sWhere .= " AND fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

		$response->data = $this->db->queryAll("SELECT lote, 
				ROUND((SELECT AVG(edad) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND edad > 0), 2) AS edad, 
                #TAG - cuadrar
                IFNULL((SELECT SUM(IF(blz > form, blz, form))
                        FROM produccion_racimos_cuadrado
                        WHERE fecha = '{$filters->fecha_inicial}' AND lote = produccion.lote), (SELECT COUNT(1) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere})) AS racimos, 
				ROUND((SELECT AVG(peso) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND peso > 0), 2) AS peso, 
				ROUND((SELECT AVG(calibre) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND calibre > 0 AND cinta != 'n/a'), 2) AS calibre, 
				ROUND((SELECT AVG(manos) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND manos > 0 AND cinta != 'n/a'), 2) AS manos, 
				ROUND((SELECT AVG(dedos) FROM produccion_racimos WHERE tipo = 'PROC' AND lote = produccion.lote {$sWhere} AND dedos > 0 AND cinta != 'n/a'), 2) AS dedos
			FROM(
				SELECT lote
				FROM produccion_racimos h
				WHERE tipo = 'PROC' {$sWhere}
				GROUP BY lote) AS produccion");
		return $response;
    }
    
    public function racimosFolmularios(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        $response = new stdClass;
        $fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_racimos WHERE fecha BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}' GROUP BY id_finca");
        
        if(!isset($fincas[$filters->finca]) && $filters->finca != ''){
            $filters->finca = array_keys($fincas)[0];
        }

        if($filters->fecha_final != "" && $filters->fecha_inicial != ""){
            $sWhere .= " AND DATE(fecha) BETWEEN '{$filters->fecha_inicial}' AND '{$filters->fecha_final}'";
        }
        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }
        
        /* BEGIN TABLA POR LOTE */
        $response->racimos_formularios = $this->db->queryAll("SELECT id_finca, (SELECT nombre FROM fincas WHERE id = id_finca) AS finca, lote, 
                                                    SUM(racimos_cosechados) AS cosechados, 
                                                    (SELECT COUNT(1) 
                                                        FROM racimos_cosechados
                                                        INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = racimos_cosechados_detalle.`id_racimos_cosechados`
                                                        WHERE lote = tbl.lote {$sWhere}) AS recusados
                                        FROM racimos_cosechados tbl
                                        WHERE 1=1 $sWhere AND lote != ''
                                        GROUP BY id_finca, lote");
        
        $suma_edades = 0;
        $cantidad = 0;
        $nombres_cintas = ["roja" => "ROJA", "lila" => "LILA", "negra" => "NEGRO", "verde" => "VERDE", "azul" => "AZUL", "amarilla" => "AMARILLO", "blanca" => "BLANCO", "cafe" => "CAFE"];
        foreach($response->racimos_formularios as $row){
            $colores = $this->db->queryRow("SELECT SUM(roja) AS roja, SUM(lila) AS lila, SUM(negra) AS negra, SUM(verde) AS verde, SUM(azul) AS azul, SUM(amarilla) AS amarilla, SUM(blanca) AS blanca, SUM(cafe) AS cafe
                                            FROM racimos_cosechados 
                                            WHERE lote = '{$row->lote}' AND id_finca = $row->id_finca {$sWhere}");
            $colores = (array) $colores;
            $in = [];
            $d_suma_edades = 0;
            $d_cantidad = 0;
            foreach($colores as $col => $val){
                if($val > 0){
                    $cinta = $nombres_cintas[$col];
                    $in[] = $cinta;
                    $detalle = $this->db->queryRow("SELECT '{$cinta}' AS cinta, SUM($col) AS cosechados,
                                                                (SELECT COUNT(1) 
                                                                        FROM racimos_cosechados
                                                                        INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = racimos_cosechados_detalle.`id_racimos_cosechados`
                                                                        WHERE lote = tbl.lote AND id_finca = tbl.id_finca AND UPPER(TRIM(color_cinta)) = '{$col}' {$sWhere}) AS recusados,
                                                                IFNULL((SELECT edad FROM produccion_racimos WHERE cinta = '{$cinta}' AND fecha = tbl.fecha LIMIT 1), getEdadCinta(getWeek(fecha), '{$cinta}', YEAR(fecha))) AS edad
                                                    FROM racimos_cosechados tbl
                                                    WHERE lote = '{$row->lote}' AND id_finca = {$row->id_finca} {$sWhere}");
                    $class = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = '{$cinta}'");
                    $detalle->class = $class;
                    $detalle->procesados = $detalle->cosechados - $detalle->recusados;

                    $suma_edades += ($detalle->edad * $detalle->procesados);
                    $cantidad += $detalle->procesados;

                    $d_suma_edades += ($detalle->edad * $detalle->procesados);
                    $d_cantidad += $detalle->procesados;

                    $detalle->detalle = $this->db->queryAll("SELECT palanca, SUM($col) AS cosechados,
                                                                            (SELECT COUNT(1) 
                                                                                FROM racimos_cosechados
                                                                                INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = racimos_cosechados_detalle.`id_racimos_cosechados`
                                                                                WHERE lote = tbl.lote AND id_finca = tbl.id_finca AND color_cinta = '{$col}' AND palanca = tbl.palanca {$sWhere}) AS recusados
                                                                FROM racimos_cosechados tbl
                                                                WHERE lote = '{$row->lote}' AND id_finca = {$row->id_finca} {$sWhere}
                                                                GROUP BY  palanca
                                                                HAVING cosechados > 0");
                    foreach($detalle->detalle as $p){
                        $p->procesados = $p->cosechados - $p->recusados;
                    }

                    $row->detalle[] = $detalle;
                }
            }
            $in = "'".implode("','", $in)."'";

            $row->edad = $d_suma_edades / $d_cantidad;
            $row->procesados = round($row->cosechados - $row->recusados, 2);
        }
        $response->prom_edad = $suma_edades / $cantidad;
        /* END TABLE POR LOTE */

        /* BEGIN TABLA POR PALANCA */
        $response->table_palanca = $this->db->queryAll("SELECT palanca as cuadrilla, (SELECT nombre FROM fincas WHERE id = id_finca) AS finca,
                    SUM(roja + lila + negra + verde + azul + blanca + amarilla + cafe) AS proc,
                    SUM((SELECT COUNT(1) FROM racimos_cosechados_detalle WHERE id_racimos_cosechados = racimos_cosechados.id)) AS recu
            FROM racimos_cosechados
            WHERE 1=1 {$sWhere}
            GROUP BY id_finca, palanca");
        foreach($response->table_palanca as $row){
            $row->proc = $row->proc - $row->recu;
            $row->cose = $row->proc + $row->recu;
        }
        /* END TABLA POR PALANCA */

        /* BEGIN TABLA POR EDAD */
        $edades = $this->db->queryAll("SELECT id_finca, (SELECT nombre FROM fincas WHERE id = id_finca) AS finca, YEAR(fecha) AS year, getWeek(fecha) AS sem, SUM(roja) AS roja, SUM(lila) AS lila, SUM(negra) AS negra, SUM(verde) AS verde, SUM(azul) AS azul, SUM(amarilla) AS amarilla, SUM(blanca) AS blanca, SUM(cafe) AS cafe
            FROM racimos_cosechados his
            WHERE 1=1 {$sWhere}
            GROUP BY id_finca, getWeek(fecha)");
        foreach($edades as $row){
            foreach($row as $col => $val){
                if($col != 'sem' && $col != 'year' && $col != 'id_finca' && $col != 'finca')
                if($val > 0){
                    $edad = $this->db->queryRow("SELECT getEdadCinta($row->sem, '{$nombres_cintas[$col]}', $row->year) AS edad,
                                                        (SELECT class FROM produccion_colores WHERE '$nombres_cintas[$col]' = color) AS class");
                    $item = [
                        "finca" => $row->finca,
                        "col" => $col,
                        "cinta" => $nombres_cintas[$col],
                        "edad" => $edad->edad,
                        "class" => $edad->class,
                        "proc" => $val,
                        "recu" => $this->db->queryOne("SELECT COUNT(1) FROM racimos_cosechados INNER JOIN racimos_cosechados_detalle ON racimos_cosechados.id = id_racimos_cosechados WHERE id_finca = $row->id_finca AND color_cinta = '{$nombres_cintas[$col]}' {$sWhere}")
                    ];
                    $item["proc"] -= $item["recu"];
                    $item["cose"] = $item["proc"] + $item["recu"];
                    $response->table_edades[] = $item;
                }
            }
        }
        /* END TABLA POR EDAD */
        return $response;
    }

    public function eliminar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(count($postdata->ids) > 0){
            foreach($postdata->ids as $reg){
                $this->db->query("DELETE FROM produccion_racimos WHERE id = $reg->id");
            }
            return true;
        }
        return false;
    }
    
    public function editar(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if(isset($postdata->id)){
            if($postdata->peso > 0 && $postdata->lote != "" && $postdata->cuadrilla != "" && $postdata->edad > 0 && $postdata->tipo != ""){
                /*$this->db->query("INSERT INTO update_produccion_racimos(id, fecha, hora, id_finca, finca, idv, racimo, num_racimo, grupo_racimo, peso, lote, cinta, edad, cuadrilla, manos, calibre, dedos, tipo, nivel, causa, timestamp, semana, id_balanza)
                                    SELECT id, fecha, hora, id_finca, finca, idv, racimo, num_racimo, grupo_racimo, peso, lote, cinta, edad, cuadrilla, manos, calibre, dedos, tipo, nivel, causa, timestamp, semana, id_balanza
                                    FROM produccion_racimos
                                    WHERE id = {$postdata->id}");*/
                $this->db->query("UPDATE produccion_racimos SET
                                        peso = $postdata->peso,
                                        edad = $postdata->edad,
                                        cinta = getCintaFromEdad($postdata->edad, getWeek(fecha), YEAR(fecha)),
                                        cuadrilla = '{$postdata->cuadrilla}',
                                        lote = '{$postdata->lote}',
                                        tipo = '{$postdata->tipo}',
                                        causa = '{$postdata->causa}',
                                        manos = '{$postdata->manos}',
                                        calibre = '{$postdata->calibre}',
                                        dedos = '{$postdata->dedos}'
                                    WHERE id = {$postdata->id}");
            }
            return $this->db->queryRow("SELECT h.id, fecha, lote, causa, peso, manos, calibre, cinta, dedos, h.edad, cuadrilla, hora, tipo, class
                                        FROM produccion_racimos h
                                        INNER JOIN produccion_colores c ON h.cinta = c.color
                                        WHERE h.id = {$postdata->id}");
        }
        return false;
    }

    public function cuadreRacimos(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        $peso_prom = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_racimos WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC' $sWhere");

        $response->cuadrado = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}'");
        $sql = "SELECT lote, edad, cinta, class, blz,
                    (SELECT SUM(cantidad) FROM racimos_cosechados_by_color WHERE lote = tbl.lote AND color = cinta AND fecha = '{$filters->fecha_inicial}' $sWhere) -
                        (SELECT COUNT(1) FROM racimos_cosechados r INNER JOIN racimos_cosechados_detalle ON r.id = id_racimos_cosechados WHERE DATE(fecha) = '{$filters->fecha_inicial}' AND lote = tbl.lote AND color_cinta = tbl.cinta $sWhere) AS form,
                    '{$peso_prom}' AS peso_prom
                FROM(
                    SELECT lote, edad, cinta, class, SUM(blz) AS blz
                    FROM (
                        SELECT lote, h.edad, cinta, c.class, COUNT(1) AS blz
                        FROM produccion_racimos h
                        LEFT JOIN produccion_colores c ON c.color = cinta
                        WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'PROC' $sWhere
                        GROUP BY lote, h.edad
                        UNION ALL
                        SELECT lote, h.edad, h.color AS cinta, c.class, 0 AS blz
                        FROM racimos_cosechados_by_color h
                        LEFT JOIN produccion_colores c ON c.color = h.color
                        WHERE fecha = '{$filters->fecha_inicial}' $sWhere
                        GROUP BY lote, h.edad
                    ) AS tbl
                    GROUP BY lote, edad
                ) AS tbl";

        $response->data = $this->db->queryAll($sql);

        $sql = "SELECT lote, edad, cinta, class, blz,
                    (SELECT COUNT(1) FROM racimos_cosechados r INNER JOIN racimos_cosechados_detalle ON r.id = id_racimos_cosechados WHERE lote = tbl.lote AND UPPER(color_cinta) = cinta AND DATE(fecha) = '{$filters->fecha_inicial}' AND status = 'RECU' $sWhere) AS form,
                    '{$peso_prom}' AS peso_prom
                FROM(
                    SELECT lote, h.edad, cinta, c.class, COUNT(1) AS blz
                    FROM produccion_racimos h
                    LEFT JOIN produccion_colores c ON c.color = cinta
                    WHERE fecha = '{$filters->fecha_inicial}' AND tipo = 'RECU' $sWhere
                    GROUP BY lote, h.edad
                    UNION ALL
                    SELECT lote, getEdadCinta(getWeek(fecha), d.`color_cinta`, YEAR(fecha)) AS edad, d.`color_cinta`, class, 0 as blz
                    FROM racimos_cosechados r
                    INNER JOIN racimos_cosechados_detalle d ON id_racimos_cosechados = r.`id`
                    LEFT JOIN produccion_colores c ON c.color = d.`color_cinta`
                    WHERE DATE(fecha) = '{$filters->fecha_inicial}' $sWhere
                    GROUP BY lote, d.color_cinta
                ) AS tbl
                GROUP BY lote, edad, cinta";
        $response->data_recu = $this->db->queryAll($sql);

        $response->faltante_recusados = [];
        foreach($response->data_recu as $row){
            if($row->form > $row->blz){
                $sql = "SELECT *, 
                            (SELECT COUNT(1) FROM produccion_racimos WHERE causa = tbl.causa AND tipo = 'RECU' AND fecha = '{$filters->fecha_inicial}' AND lote = tbl.lote AND cinta = color_cinta $sWhere) AS cantidad_blz,
                            '{$peso_prom}' AS peso_prom
                        FROM(
                            SELECT lote, d.`color_cinta`, causa, COUNT(1) AS cantidad
                            FROM racimos_cosechados r
                            INNER JOIN racimos_cosechados_detalle d ON r.id = d.id_racimos_cosechados
                            INNER JOIN produccion_colores c ON c.color = d.`color_cinta`
                            WHERE DATE(fecha) = '{$filters->fecha_inicial}' AND lote = '$row->lote' $sWhere
                            GROUP BY lote, d.`color_cinta`, causa
                        ) AS tbl
                        HAVING cantidad > cantidad_blz";
                $response->faltante_recusados = array_merge($response->faltante_recusados, $this->db->queryAll($sql));
            }
        }
    
        return $response;
    }

    public function cuadrarRacimos(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        if($filters->finca != ""){
            $sWhere .= " AND id_finca = $filters->finca";
        }

        $response->status = 400;
        $dia_procesado = $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_cuadrado WHERE fecha = '{$filters->fecha_inicial}' $sWhere");
        if($dia_procesado == 0){
            $response->status = 200;

            $data = $this->cuadreRacimos();
            $procesados = $data->data;
            $recusados = $data->data_recu;
            foreach($procesados as $row){
                $this->db->query("INSERT INTO produccion_racimos_cuadrado SET fecha = '{$filters->fecha_inicial}', lote = '{$row->lote}', cinta = '{$row->cinta}', blz = '$row->blz', form = '$row->form', id_finca = $filters->finca");
            }
            foreach($recusados as $row){
                $this->db->query("INSERT INTO produccion_racimos_cuadrado SET fecha = '{$filters->fecha_inicial}', lote = '{$row->lote}', cinta = '{$row->cinta}', blz = '$row->blz', form = '$row->form', status = 'RECU', id_finca = $filters->finca");
            }
        }

        return $response;
    }

    public function analizisRecusados(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->var_recusado == 'cant'){
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM produccion_racimos
                    WHERE tipo = 'RECU' AND year = YEAR('{$postdata->fecha_inicial}')
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa
                UNION ALL
                SELECT
                    'TOTAL' AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, COUNT(1) AS cantidad
                    FROM produccion_racimos
                    WHERE tipo = 'RECU' AND year = YEAR('{$postdata->fecha_inicial}')
                    GROUP BY semana
                ) AS tbl";
            $response->data = $this->db->queryAll($sql);
        }else{
            $sql = "SELECT
                    causa AS dano,
                    SUM(cantidad) AS 'sum',
                    MIN(cantidad) AS 'min',
                    MAX(cantidad) AS 'max',
                    ROUND(AVG(cantidad), 2) AS 'prom',
                    SUM(IF(semana = 0, cantidad, 0)) AS 'sem_0',
                    SUM(IF(semana = 1, cantidad, 0)) AS 'sem_1',
                    SUM(IF(semana = 2, cantidad, 0)) AS 'sem_2',
                    SUM(IF(semana = 3, cantidad, 0)) AS 'sem_3',
                    SUM(IF(semana = 4, cantidad, 0)) AS 'sem_4',
                    SUM(IF(semana = 5, cantidad, 0)) AS 'sem_5',
                    SUM(IF(semana = 6, cantidad, 0)) AS 'sem_6',
                    SUM(IF(semana = 7, cantidad, 0)) AS 'sem_7',
                    SUM(IF(semana = 8, cantidad, 0)) AS 'sem_8',
                    SUM(IF(semana = 9, cantidad, 0)) AS 'sem_9',
                    SUM(IF(semana = 10, cantidad, 0)) AS 'sem_10',
                    SUM(IF(semana = 11, cantidad, 0)) AS 'sem_11',
                    SUM(IF(semana = 12, cantidad, 0)) AS 'sem_12',
                    SUM(IF(semana = 13, cantidad, 0)) AS 'sem_13',
                    SUM(IF(semana = 14, cantidad, 0)) AS 'sem_14',
                    SUM(IF(semana = 15, cantidad, 0)) AS 'sem_15',
                    SUM(IF(semana = 16, cantidad, 0)) AS 'sem_16',
                    SUM(IF(semana = 17, cantidad, 0)) AS 'sem_17',
                    SUM(IF(semana = 18, cantidad, 0)) AS 'sem_18',
                    SUM(IF(semana = 19, cantidad, 0)) AS 'sem_19',
                    SUM(IF(semana = 20, cantidad, 0)) AS 'sem_20',
                    SUM(IF(semana = 21, cantidad, 0)) AS 'sem_21',
                    SUM(IF(semana = 22, cantidad, 0)) AS 'sem_22',
                    SUM(IF(semana = 23, cantidad, 0)) AS 'sem_23',
                    SUM(IF(semana = 24, cantidad, 0)) AS 'sem_24',
                    SUM(IF(semana = 25, cantidad, 0)) AS 'sem_25',
                    SUM(IF(semana = 26, cantidad, 0)) AS 'sem_26',
                    SUM(IF(semana = 27, cantidad, 0)) AS 'sem_27',
                    SUM(IF(semana = 28, cantidad, 0)) AS 'sem_28',
                    SUM(IF(semana = 29, cantidad, 0)) AS 'sem_29',
                    SUM(IF(semana = 30, cantidad, 0)) AS 'sem_30',
                    SUM(IF(semana = 31, cantidad, 0)) AS 'sem_31',
                    SUM(IF(semana = 32, cantidad, 0)) AS 'sem_32',
                    SUM(IF(semana = 33, cantidad, 0)) AS 'sem_33',
                    SUM(IF(semana = 34, cantidad, 0)) AS 'sem_34',
                    SUM(IF(semana = 35, cantidad, 0)) AS 'sem_35',
                    SUM(IF(semana = 36, cantidad, 0)) AS 'sem_36',
                    SUM(IF(semana = 37, cantidad, 0)) AS 'sem_37',
                    SUM(IF(semana = 38, cantidad, 0)) AS 'sem_38',
                    SUM(IF(semana = 39, cantidad, 0)) AS 'sem_39',
                    SUM(IF(semana = 40, cantidad, 0)) AS 'sem_40',
                    SUM(IF(semana = 41, cantidad, 0)) AS 'sem_41',
                    SUM(IF(semana = 42, cantidad, 0)) AS 'sem_42',
                    SUM(IF(semana = 43, cantidad, 0)) AS 'sem_43',
                    SUM(IF(semana = 44, cantidad, 0)) AS 'sem_44',
                    SUM(IF(semana = 45, cantidad, 0)) AS 'sem_45',
                    SUM(IF(semana = 46, cantidad, 0)) AS 'sem_46',
                    SUM(IF(semana = 47, cantidad, 0)) AS 'sem_47',
                    SUM(IF(semana = 48, cantidad, 0)) AS 'sem_48',
                    SUM(IF(semana = 49, cantidad, 0)) AS 'sem_49',
                    SUM(IF(semana = 50, cantidad, 0)) AS 'sem_50',
                    SUM(IF(semana = 51, cantidad, 0)) AS 'sem_51',
                    SUM(IF(semana = 52, cantidad, 0)) AS 'sem_52',
                    SUM(IF(semana = 53, cantidad, 0)) AS 'sem_53'
                FROM (
                    SELECT semana, causa, ROUND(COUNT(1)/(SELECT COUNT(1) FROM produccion_racimos WHERE semana = main.semana AND YEAR = YEAR('{$postdata->fecha_inicial}'))*100, 2) AS cantidad
                    FROM produccion_racimos main
                    WHERE tipo = 'RECU' AND YEAR = YEAR('{$postdata->fecha_inicial}')
                    GROUP BY semana
                ) AS tbl
                GROUP BY causa";
            $totales = $this->db->queryRow("SELECT tbl.*, 'TOTAL' AS dano, SUM(SUM) AS 'sum', SUM(MIN) AS 'min', SUM(MAX) AS 'max', SUM(prom) AS 'prom'
                FROM (
                    SELECT
                        'TOTAL' AS dano,
                        '' AS 'sum',
                        '' AS 'min',
                        '' AS 'max',
                        '' AS 'prom',
                        SUM(IF(semana = 0, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 1)*100 AS 'sem_0',
                        SUM(IF(semana = 1, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 1)*100 AS 'sem_1',
                        SUM(IF(semana = 2, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 2)*100 AS 'sem_2',
                        SUM(IF(semana = 3, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 3)*100 AS 'sem_3',
                        SUM(IF(semana = 4, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 4)*100 AS 'sem_4',
                        SUM(IF(semana = 5, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 5)*100 AS 'sem_5',
                        SUM(IF(semana = 6, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 6)*100 AS 'sem_6',
                        SUM(IF(semana = 7, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 7)*100 AS 'sem_7',
                        SUM(IF(semana = 8, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 8)*100 AS 'sem_8',
                        SUM(IF(semana = 9, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 9)*100 AS 'sem_9',
                        SUM(IF(semana = 10, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 10)*100 AS 'sem_10',
                        SUM(IF(semana = 11, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 11)*100 AS 'sem_11',
                        SUM(IF(semana = 12, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 12)*100 AS 'sem_12',
                        SUM(IF(semana = 13, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 13)*100 AS 'sem_13',
                        SUM(IF(semana = 14, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 14)*100 AS 'sem_14',
                        SUM(IF(semana = 15, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 15)*100 AS 'sem_15',
                        SUM(IF(semana = 16, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 16)*100 AS 'sem_16',
                        SUM(IF(semana = 17, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 17)*100 AS 'sem_17',
                        SUM(IF(semana = 18, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 18)*100 AS 'sem_18',
                        SUM(IF(semana = 19, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 19)*100 AS 'sem_19',
                        SUM(IF(semana = 20, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 20)*100 AS 'sem_20',
                        SUM(IF(semana = 21, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 21)*100 AS 'sem_21',
                        SUM(IF(semana = 22, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 22)*100 AS 'sem_22',
                        SUM(IF(semana = 23, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 23)*100 AS 'sem_23',
                        SUM(IF(semana = 24, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 24)*100 AS 'sem_24',
                        SUM(IF(semana = 25, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 25)*100 AS 'sem_25',
                        SUM(IF(semana = 26, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 26)*100 AS 'sem_26',
                        SUM(IF(semana = 27, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 27)*100 AS 'sem_27',
                        SUM(IF(semana = 28, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 28)*100 AS 'sem_28',
                        SUM(IF(semana = 29, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 29)*100 AS 'sem_29',
                        SUM(IF(semana = 30, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 30)*100 AS 'sem_30',
                        SUM(IF(semana = 31, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 31)*100 AS 'sem_31',
                        SUM(IF(semana = 32, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 32)*100 AS 'sem_32',
                        SUM(IF(semana = 33, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 33)*100 AS 'sem_33',
                        SUM(IF(semana = 34, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 34)*100 AS 'sem_34',
                        SUM(IF(semana = 35, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 35)*100 AS 'sem_35',
                        SUM(IF(semana = 36, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 36)*100 AS 'sem_36',
                        SUM(IF(semana = 37, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 37)*100 AS 'sem_37',
                        SUM(IF(semana = 38, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 38)*100 AS 'sem_38',
                        SUM(IF(semana = 39, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 39)*100 AS 'sem_39',
                        SUM(IF(semana = 40, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 40)*100 AS 'sem_40',
                        SUM(IF(semana = 41, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 41)*100 AS 'sem_41',
                        SUM(IF(semana = 42, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 42)*100 AS 'sem_42',
                        SUM(IF(semana = 43, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 43)*100 AS 'sem_43',
                        SUM(IF(semana = 44, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 44)*100 AS 'sem_44',
                        SUM(IF(semana = 45, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 45)*100 AS 'sem_45',
                        SUM(IF(semana = 46, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 46)*100 AS 'sem_46',
                        SUM(IF(semana = 47, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 47)*100 AS 'sem_47',
                        SUM(IF(semana = 48, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 48)*100 AS 'sem_48',
                        SUM(IF(semana = 49, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 49)*100 AS 'sem_49',
                        SUM(IF(semana = 50, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 50)*100 AS 'sem_50',
                        SUM(IF(semana = 51, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 51)*100 AS 'sem_51',
                        SUM(IF(semana = 52, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 52)*100 AS 'sem_52',
                        SUM(IF(semana = 53, cantidad, 0))/(SELECT COUNT(1) FROM produccion_racimos WHERE YEAR = YEAR('{$postdata->fecha_inicial}') AND semana = 53)*100 AS 'sem_53'
                    FROM (
                        SELECT semana, causa, COUNT(1) AS cantidad
                        FROM produccion_racimos
                        WHERE tipo = 'RECU' AND YEAR = YEAR('{$postdata->fecha_inicial}')
                        GROUP BY causa, semana
                    ) AS tbl
                ) AS tbl
                GROUP BY dano");
            $row_totales_gropales = $this->db->queryRow("SELECT SUM(recusados)/SUM(cortados)*100 AS 'sum', MIN(cantidad) AS 'min', MAX(cantidad) AS 'max', AVG(cantidad) AS 'prom'
                FROM (
                    SELECT semana, SUM(recusados) AS recusados, SUM(cortados) AS cortados, IF(recusados > 0, recusados/cortados*100, NULL) AS cantidad
                    FROM (
                        SELECT semana, COUNT(1) AS cortados, SUM(IF(tipo = 'RECU', 1, 0)) AS recusados
                        FROM produccion_racimos
                        WHERE YEAR = YEAR('{$postdata->fecha_inicial}')
                        GROUP BY semana
                    ) AS tbl
                    GROUP BY semana
                ) AS tbl");

            $totales->prom = $row_totales_gropales->prom;
            $totales->sum = $row_totales_gropales->sum;
            $totales->min = $row_totales_gropales->min;
            $totales->max = $row_totales_gropales->max;
            $response->data = $this->db->queryAll($sql);
            $response->data[] = $totales;
        }
        $response->semanas = $this->db->queryAllOne("SELECT semana FROM produccion_racimos WHERE tipo = 'RECU' AND YEAR = YEAR('{$postdata->fecha_inicial}') GROUP BY semana");
        return $response;
    }

    public function racimosPorViaje(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            "finca" => getValueFrom($postdata , "finca" , "" , FILTER_SANITIZE_STRING),
            "fecha_inicial" => getValueFrom($postdata , "fecha_inicial" , "" , FILTER_SANITIZE_STRING),
            "fecha_final" => getValueFrom($postdata , "fecha_final" , "" , FILTER_SANITIZE_STRING),
        ];

        if($filters->finca != ''){
            $sWhere .= " AND id_finca = '{$filters->finca}'";
            $sWhereP .= " AND p.id_finca = '{$filters->finca}'";
        }

        if(strtotime($filters->fecha_inicial) > strtotime('2018-02-02')){
            $sumar = $filters->fecha_inicial == '2018-02-21' ? 41 : 0;
            $sql = "SELECT (@i := @i+1) + $sumar AS label, grupo_racimo AS id
                    FROM (
                        SELECT grupo_racimo
                        FROM produccion_racimos 
                        WHERE fecha = '{$filters->fecha_inicial}'
                        GROUP BY grupo_racimo
                        ORDER BY hora
                    ) AS tbl
                    JOIN (SELECT @i := 0) AS tbl2";
            $numeros_viajes = $this->db->queryAllSpecial($sql);

            $sql = "SELECT *, 
                        IF(hora_blz IS NULL OR hora_blz = '', hora_form, hora_blz) AS hora, 
                        lote AS newLote, 
                        cuadrilla AS newCuadrilla,
                        IF(hora_blz != '', (SELECT COUNT(DISTINCT grupo_racimo)+1 FROM produccion_racimos WHERE fecha = '{$filters->fecha_inicial}' AND hora < hora_blz $sWhere), 0) AS viajeBlz
                FROM (
                    SELECT *,  
                        (SELECT TIME(fecha) FROM racimos_cosechados WHERE id = tbl.id_formulario $sWhere) AS hora_form,
                        (SELECT MIN(hora) FROM produccion_racimos WHERE grupo_racimo = tbl.grupo_racimo AND id_finca = tbl.id_finca) AS hora_blz,
                        getNumViaje(tbl.grupo_racimo, '{$filters->fecha_inicial}') AS num_viaje_blz,
                        (SELECT COUNT(1) FROM produccion_racimos_viajes_procesados WHERE id_finca = tbl.id_finca AND grupo_racimo = tbl.grupo_racimo AND STATUS = 'Procesado') AS procesado,
                        (SELECT COUNT(1) FROM produccion_racimos WHERE grupo_racimo = tbl.grupo_racimo AND id_finca = tbl.id_finca AND fecha = '{$filters->fecha_inicial}') AS blz,
                        (SELECT racimos_cosechados FROM racimos_cosechados WHERE id = id_formulario) AS form,
                        (SELECT json FROM racimos_cosechados WHERE id = id_formulario) AS jsonForm,
                        (SELECT viaje FROM racimos_cosechados WHERE id = id_formulario) AS viajeForm
                    FROM (
                        SELECT p.id_finca, p.grupo_racimo, lote, cuadrilla, id_formulario
                        FROM produccion_racimos p
                        INNER JOIN `produccion_racimos_viajes_relacionados` r ON r.`grupo_racimo` = p.`grupo_racimo` AND p.id_finca_procesado_en = r.id_finca
                        WHERE p.`fecha` = '{$filters->fecha_inicial}' $sWhereP
                        GROUP BY p.`grupo_racimo`
                        UNION ALL
                        SELECT id_finca, NULL, lote, palanca, m.id
                        FROM racimos_cosechados m
                        WHERE DATE(fecha) = '{$filters->fecha_inicial}'
                            AND id NOT IN (SELECT id_formulario FROM `produccion_racimos_viajes_relacionados`)
                            $sWhere
                        UNION ALL
                        SELECT id_finca, grupo_racimo, lote, cuadrilla, NULL
                        FROM produccion_racimos p
                        WHERE fecha = '{$filters->fecha_inicial}'
                            AND grupo_racimo NOT IN (SELECT grupo_racimo FROM `produccion_racimos_viajes_relacionados` WHERE id_finca = p.id_finca_procesado_en)
                            $sWhere
                        GROUP BY grupo_racimo
                    ) AS tbl
                ) AS tbl
                ORDER BY hora";
                

            $response->data = $this->db->queryAll($sql);

            foreach($response->data as $index => $row){
                if($row->grupo_racimo){
                    $row->num_viaje_blz = $numeros_viajes[$row->grupo_racimo];
                    $sql2 = "SELECT p.id, cinta, p.edad, num_racimo, IF(tipo = 'RECU', 'RECU', tipo) AS tipo, class
                            FROM produccion_racimos p
                            LEFT JOIN produccion_colores c ON c.color = cinta 
                            WHERE grupo_racimo = $row->grupo_racimo AND fecha = '{$filters->fecha_inicial}' $sWhere
                            ORDER BY num_racimo";
                    $row->detalle = $this->db->queryAll($sql2);
                }
                
                if($row->jsonForm){
                    $sql2 = "SELECT r.id, r.color AS cinta, cantidad, r.edad, lote, cantidad, r.status, c.class
                            FROM racimos_cosechados_by_color r
                            LEFT JOIN produccion_colores c ON c.color = r.color
                            WHERE json = '{$row->jsonForm}'
                            ORDER BY id";
                    $formulario = $this->db->queryAll($sql2);
                    $row->formulario = [];
        
                    foreach($formulario as $form){
        
                        $recusadas = $this->db->queryOne("SELECT COUNT(1) FROM racimos_cosechados_detalle WHERE json = '{$row->jsonForm}' AND color_cinta = '{$form->cinta}'");
                        $recusadas_usadas = 0;
                        for($i = 0; $i < $form->cantidad; $i++){
                            $tipo = 'PROC';
                            if($recusadas_usadas < $recusadas){
                                $tipo = 'RECU';
                                $recusadas_usadas++;
                            }
        
                            $row->formulario[] = [
                                "id" => $form->id,
                                "cinta" => $form->cinta,
                                "edad" => $form->edad,
                                "tipo" => $tipo,
                                "class" => $form->class,
                            ];
                        }
                    }
                }
            }
        }else{
            $sql = "SELECT lote, cuadrilla, grupo_racimo, hora,
                        (SELECT COUNT(1) FROM produccion_racimos_viajes_procesados WHERE id_finca = {$filters->finca} AND grupo_racimo = tbl.grupo_racimo AND status = 'Procesado') AS procesado, 
                        (SELECT COUNT(1) FROM produccion_racimos WHERE grupo_racimo = tbl.grupo_racimo AND id_finca = {$filters->finca}) AS blz
                    FROM (
                        SELECT grupo_racimo, lote, cuadrilla, hora
                        FROM produccion_racimos
                        WHERE fecha = '{$filters->fecha_inicial}' AND id_finca = {$filters->finca}
                        GROUP BY grupo_racimo
                        ORDER BY grupo_racimo
                    ) AS tbl";
            $response->data = $this->db->queryAll($sql);

            $viajes = $this->db->queryAll("SELECT id, json, racimos_cosechados, lote, palanca, TIME(fecha) AS hora FROM `racimos_cosechados` WHERE DATE(fecha) = '{$filters->fecha_inicial}' AND id_finca = {$filters->finca} ORDER BY fecha");

            if(count($response->data) < count($viajes)){
                $d = count($viajes) - count($response->data);
                for($x = 1; $x <= $d; $x++){
                    $response->data[] = (object)[
                        "lote" => "",
                        "cuadrilla" => "",
                        "grupo_racimo" => 0,
                        "procesado" => 0,
                        "blz" => 0,
                        "form" => 0
                    ];
                }
            }
            foreach($response->data as $index => $row){
                if($viajes[$index]){
                    $row->form = $viajes[$index]->racimos_cosechados;
                    $row->jsonForm = $viajes[$index]->json;
                }

                if($row->grupo_racimo == 0){
                    $row->lote = $viajes[$index]->lote;
                    $row->cuadrilla = $viajes[$index]->palanca;
                }
                
                $sql2 = "SELECT p.id, cinta, p.edad, num_racimo, IF(tipo = 'RECU', 'RECU', tipo) AS tipo, class
                        FROM produccion_racimos p
                        LEFT JOIN produccion_colores c ON c.color = cinta 
                        WHERE grupo_racimo = $row->grupo_racimo AND id_finca = {$filters->finca}
                        ORDER BY num_racimo";
                $row->detalle = $this->db->queryAll($sql2);
    
                $sql2 = "SELECT r.id, r.color AS cinta, cantidad, r.edad, lote, cantidad, r.status, c.class
                        FROM racimos_cosechados_by_color r
                        LEFT JOIN produccion_colores c ON c.color = r.color
                        WHERE json = '{$row->jsonForm}'
                        ORDER BY id";
                $formulario = $this->db->queryAll($sql2);
                $row->formulario = [];
    
                foreach($formulario as $form){
    
                    $recusadas = $this->db->queryOne("SELECT COUNT(1) FROM racimos_cosechados_detalle WHERE json = '{$row->jsonForm}' AND color_cinta = '{$form->cinta}'");
                    $recusadas_usadas = 0;
                    for($i = 0; $i < $form->cantidad; $i++){
                        $tipo = 'PROC';
                        if($recusadas_usadas < $recusadas){
                            $tipo = 'RECU';
                            $recusadas_usadas++;
                        }
    
                        $row->formulario[] = [
                            "id" => $form->id,
                            "cinta" => $form->cinta,
                            "edad" => $form->edad,
                            "tipo" => $tipo,
                            "class" => $form->class,
                        ];
                    }
                }
            }
        }
        return $response;
    }

    public function guardarViaje(){
        $response = new stdClass;
        $response->status = 400;
        $response->message = "";

        $postdata = (object)json_decode(file_get_contents("php://input"));
        $cintas = ["VERDE", "ROJA", "AZUL", "CAFE", "BLANCO", "NEGRO", "LILA", "AMARILLO", ""];
        $cintas_color = ["VERDE" => "verde", "ROJA" => "roja", "AZUL" => "azul", "CAFE" => "cafe", "BLANCO" => "blanca", "NEGRO" => "negra", "LILA" => "lila", "AMARILLO" => "amairlla"];
        $tipos = ["RECU", "PROC"];

        /* VALIDAR */
        if(!$postdata->finca > 0){
            $response->message = "Favor de solo seleccionar una finca";
            return $response;
        }
        if(count($postdata->formulario) > 0 && count($postdata->balanza) > 0){
            foreach($postdata->balanza as $row){
                if(in_array($row->cinta, $cintas)){
                    if($row->cinta != ""){
                        if(!in_array($row->tipo, $tipos)){
                            $response->message = "Error, Tipo de racimo '{$row->tipo}' no valido";
                            break;
                        }
                    }
                }else{
                    $response->message = "Error, Color de cinta '{$row->cinta}' no valida";
                    break;
                }
            }

            foreach($postdata->formulario as $row){
                if(in_array($row->cinta, $cintas)){
                    if($row->cinta != ""){
                        if(!in_array($row->tipo, $tipos)){
                            $response->message = "Error, Tipo de racimo '{$row->tipo}' no valido";
                            break;
                        }
                    }
                }else{
                    $response->message = "Error, Color de cinta '{$row->cinta}' no valida";
                    break;
                }
            }
        }else{
            $response->messsage = "Error, No puedes eliminar todos los racimos del viaje";
        }
        /* END VALIDAR */

        /* PROCESAR */
        $procesado_en = $this->db->queryOne("SELECT id_finca_procesado_en FROM produccion_racimos WHERE id_finca = {$postdata->finca} AND fecha = '{$postdata->fecha}' LIMIT 1");
        if($postdata->grupo_racimo == 0){
            //CREAR VIAJE NUEVO
            $n_grupo_racimo = $this->db->queryOne("SELECT MAX(grupo_racimo) + 1 FROM produccion_racimos WHERE id_finca_procesado_en = $procesado_en");
            $this->db->query("INSERT INTO produccion_racimos_viajes_procesados SET grupo_racimo = $n_grupo_racimo, id_finca = $procesado_en");
            $postdata->grupo_racimo = $n_grupo_racimo;
        }
        if($response->message == ""){
            $response->status = 200;
            /* BALANZA */
            for($i = 1; $i <= 25; $i++){
                $racimo = $postdata->balanza->{"{$i}"};
                $racimo->tipo = $racimo->tipo == 'RECU' ? 'RECU' : $racimo->tipo;
                $racimo->id = (int) $this->db->queryOne("SELECT id FROM produccion_racimos WHERE grupo_racimo = {$postdata->grupo_racimo} AND num_racimo = {$racimo->num_racimo} AND fecha = '{$postdata->fecha}' AND id_finca = {$postdata->finca}");

                if($racimo->cinta != "" && $racimo->tipo != ""){
                    if($racimo->id > 0){
                        $sql = "UPDATE produccion_racimos SET cinta = '{$racimo->cinta}', edad = getEdadCinta(getWeek(fecha), '{$racimo->cinta}', YEAR(fecha)), tipo = '{$racimo->tipo}' WHERE id = $racimo->id";
                        $this->db->query($sql);
                    }else{
                        $peso_prom = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_racimos WHERE grupo_racimo = $postdata->grupo_racimo AND tipo = 'PROC'");
                        $sql = "INSERT INTO produccion_racimos SET 
                                    num_racimo = '{$racimo->num_racimo}',
                                    grupo_racimo = '{$postdata->grupo_racimo}',
                                    cinta = '{$racimo->cinta}', 
                                    tipo = '{$racimo->tipo}', 
                                    peso = '{$peso_prom}', 
                                    id_finca = $postdata->finca,
                                    finca = (SELECT nombre FROM fincas WHERE id = $postdata->finca),
                                    id_finca_procesado_en = $procesado_en,
                                    finca_procesado_en = (SELECT nombre FROM fincas WHERE id = $procesado_en),
                                    edad = getEdadCinta(getWeek('{$postdata->fecha}'), '{$racimo->cinta}', YEAR('{$postdata->fecha}')),
                                    cuadrilla = '{$postdata->cuadrilla}',
                                    lote = '{$postdata->lote}',
                                    fecha = '{$postdata->fecha}',
                                    semana = getWeek('{$postdata->fecha}'),
                                    year = YEAR('{$postdata->fecha}'),
                                    hora = CURRENT_TIME";
                        $this->db->query($sql);
                    }
                }else if($racimo->id > 0){
                    $sql = "DELETE FROM produccion_racimos WHERE id = $racimo->id";
                    $this->db->query($sql);
                }
            }
            /* END BALANZA */

            /* FORMULARIO */
            /*for($i = 1; $i <= 25; $i++){
                $racimo = $postdata->formulario->{"{$i}"};
                $racimo->tipo = $racimo->tipo == 'RECU' ? 'RECUSADO' : $racimo->tipo;

                if($racimo->cinta != "" && $racimo->tipo != ""){
                    if($racimo->id > 0){
                        $cinta_vieja = $this->db->queryOne("SELECT color FROM racimos_cosechados_by_color WHERE id = $racimo->id");
                        $tipo_viejo = $this->db->queryOne("SELECT COUNT(1) FROM racimos_cosechados_detalle WHERE json = '{$postdata->jsonForm}' AND color_cinta = '{$cinta_vieja}'") > 0 ? 'RECUSADO' : 'PROC';
                        $existNewCinta = $this->db->queryOne("SELECT COUNT(1) FROM racimos_cosechados_by_color WHERE json = '{$postdata->jsonForm}' AND color = '{$racimo->cinta}'");
                        $cantidad_antes = $this->db->queryOne("SELECT cantidad FROM racimos_cosechados_by_color WHERE id = $racimo->id");

                        if($cinta_vieja != $racimo->cinta){
                            $sql = "UPDATE racimos_cosechados SET {$cintas_color[$cinta_vieja]} = {$cintas_color[$cinta_vieja]} - 1, {$cintas_color[$racimo->cinta]} = {$cintas_color[$racimo->cinta]} + 1 WHERE json = '{$postdata->jsonForm}'";
                            $this->db->query($sql);

                            if($cantidad_antes > 1){
                                $sql = "UPDATE racimos_cosechados_by_color SET cantidad = cantidad - 1 WHERE id = $racimo->id";
                                $this->db->query($sql);
                            }else{
                                $sql = "DELETE FROM racimos_cosechados_by_color WHERE id = $racimo->id";
                                $this->db->query($sql);
                            }
                            if($existNewCinta > 0){
                                $sql = "UPDATE racimos_cosechados_by_color SET cantidad = cantidad + 1 WHERE json = '{$postdata->jsonForm}' AND color = '{$racimo->cinta}'";
                                $this->db->query($sql);
                            }else{
                                $sql = "INSERT INTO racimos_cosechados_by_color SET 
                                            cantidad = 1, 
                                            fecha = '{$postdata->fecha}', 
                                            color = '{$racimo->cinta}', 
                                            edad = getEdadCinta(getWeek('{$postdata->fecha}'), '{$racimo->cinta}', YEAR('{$postdata->fecha}')),
                                            lote = '{$postdata->lote}',
                                            palanca = '{$postdata->cuadrilla}',
                                            id_finca = {$postdata->finca},
                                            finca = (SELECT nombre FROM fincas WHERE id = {$postdata->finca}),
                                            status = 'PROC',
                                            json = '{$postdata->jsonForm}'";
                                $this->db->query($sql);
                            }
                        }

                        if($tipo_viejo != $racimo->tipo){
                            if($tipo_viejo == 'RECUSADO'){
                                $id_racimo_detalle = $this->db->queryOne("SELECT id FROM racimos_cosechados_detalle WHERE json = '{$postdata->jsonForm}' AND color_cinta = '{$cinta_vieja}' LIMIT 1");
                                $sql = "DELETE FROM racimos_cosechados_detalle WHERE id = $id_racimo_detalle";
                                $this->db->query($sql);
                            }else{
                                $id_racimo = $this->db->queryOne("SELECT id FROM racimos_cosechados WHERE json = '{$postdata->jsonForm}'");
                                $sql = "INSERT INTO racimos_cosechados_detalle SET
                                            id_racimos_cosechados = {$id_racimo},
                                            color_cinta = '{$racimo->cinta}',
                                            status = 'RECU',
                                            causa = '',
                                            json = '{$postdata->jsonForm}'";
                                $this->db->query($sql);
                            }
                        }
                    }else{
                        $sql = "UPDATE racimos_cosechados SET racimos_cosechados = racimos_cosechados + 1, {$cintas_color[$racimo->cinta]} = 1 WHERE json = '{$postdata->jsonForm}'";
                        $this->db->query($sql);

                        $sql = "INSERT INTO racimos_cosechados_by_color SET
                                    fecha = '{$postdata->fecha}',
                                    color = '{$racimo->cinta}',
                                    edad = getEdadCinta(getWeek('{$postdata->fecha}'), '{$racimo->cinta}', YEAR('{$postdata->fecha}')),
                                    lote = '{$postdata->lote}',
                                    palanca = '{$postdata->cuadrilla}',
                                    id_finca = '{$postdata->finca}',
                                    finca = (SELECT nombre FROM fincas WHERE id = {$postdata->finca}),
                                    cantidad = 1,
                                    status = 'PROC',
                                    json = '{$postdata->jsonForm}'";
                        $this->db->query($sql);

                        if($racimo->tipo == 'RECUSADO'){
                            $id_racimo = $this->db->queryOne("SELECT id FROM racimos_cosechados WHERE json = '{$postdata->jsonForm}'");
                            $sql = "INSERT INTO racimos_cosechados_detalle SET
                                        id_racimos_cosechados = {$id_racimo},
                                        color_cinta = '{$racimo->cinta}',
                                        status = 'RECU',
                                        causa = '',
                                        json = '{$postdata->jsonForm}'";
                            $this->db->query($sql);
                        }
                    }
                }else if($racimo->id > 0){
                    $cantidad = $this->db->queryOne("SELECT cantidad FROM racimos_cosechados_by_color WHERE id = $racimo->id");
                    $cinta = $this->db->queryOne("SELECT color FROM racimos_cosechados_by_color WHERE id = $racimo->id");

                    if($cantidad > 1){
                        $sql = "UPDATE racimos_cosechados_by_color SET cantidad = cantidad - 1 WHERE id = $racimo->id";
                        $this->db->query($sql);
                        $sql = "UPDATE racimos_cosechados SET racimos_cosechados = racimos_cosechados - 1, {$cintas_color[$cinta]} = {$cintas_color[$cinta]} - 1 WHERE json = '{$postdata->jsonForm}'";
                        $this->db->query($sql);
                    }else{
                        $sql = "DELETE FROM racimos_cosechados_by_color WHERE id = $racimo->id";
                        $this->db->query($sql);
                        $sql = "UPDATE racimos_cosechados SET racimos_cosechados = racimos_cosechados - 1, {$cintas_color[$cinta]} = 0 WHERE json = '{$postdata->jsonForm}'";
                        $this->db->query($sql);
                    }
                }
            }*/
            /* END FORMULARIO */
        }
        /* END PROCESAR */

        return $response;
    }

    public function procesarViaje(){
        $response = new stdClass;
        $response->status = 400;

        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->grupo_racimo > 0 && $postdata->finca > 0){
            $procesado_en = $this->db->queryOne("SELECT id_finca_procesado_en FROM produccion_racimos WHERE id_finca = {$postdata->finca} AND fecha = '{$postdata->fecha}' LIMIT 1");
            $e = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_viajes_procesados WHERE id_finca = {$procesado_en} AND grupo_racimo = {$postdata->grupo_racimo} AND status = 'Pendiente'");
            if($e == 1){
                $response->status = 200;
                $this->db->query("UPDATE produccion_racimos_viajes_procesados SET status = 'Procesado' WHERE id_finca = {$procesado_en} AND grupo_racimo = {$postdata->grupo_racimo}");
            }else{
                $response->message = "Este viaje ya esta finalizado";
            }
        }

        return $response;
    }

    public function desprocesarViaje(){
        $response = new stdClass;
        $response->status = 400;

        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->grupo_racimo > 0 && $postdata->finca > 0){
            $procesado_en = $this->db->queryOne("SELECT id_finca_procesado_en FROM produccion_racimos WHERE id_finca = {$postdata->finca} AND fecha = '{$postdata->fecha}' LIMIT 1");
            $e = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_racimos_viajes_procesados WHERE id_finca = {$procesado_en} AND grupo_racimo = {$postdata->grupo_racimo} AND status = 'Procesado'");
            if($e == 1){
                $response->status = 200;
                $this->db->query("UPDATE produccion_racimos_viajes_procesados SET status = 'Pendiente' WHERE id_finca = {$procesado_en} AND grupo_racimo = {$postdata->grupo_racimo}");
            }
        }

        return $response;
    }

    private function timeToSec($str_time){
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $str_time);
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        return $time_seconds;
    }

    private function validarViaje($form, $blz){
        $form = (object) $form;
        $blz = (object) $blz;

        return $this->timeToSec($form->hora) < $this->timeToSec($blz->hora)
            && $form->lote == $blz->lote
            && $form->palanca == $blz->cuadrilla;
    }

    public function unirBalanzaFormulario(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response->status = 400;

        if($postdata->id_formulario > 0 && $postdata->grupo_racimo > 0 && $postdata->id_finca > 0){
            $response->status = 200;
            $id_finca_procesado_en = $this->db->queryOne("SELECT id_finca_procesado_en FROM produccion_racimos WHERE id_finca = $postdata->id_finca AND grupo_racimo = {$postdata->grupo_racimo}");
            $this->db->query("INSERT INTO produccion_racimos_viajes_relacionados SET id_finca = $id_finca_procesado_en, grupo_racimo = $postdata->grupo_racimo, id_formulario = $postdata->id_formulario");
        }

        return $response;
    }

    public function crearBalazaDeFormulario(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response->status = 400;

        if($postdata->id_formulario > 0){
            $response->status = 200;

            $json = $this->db->queryOne("SELECT json FROM racimos_cosechados WHERE id = $postdata->id_formulario");
            $fecha = $this->db->queryOne("SELECT DATE(fecha) FROM racimos_cosechados WHERE id = $postdata->id_formulario");
            $id_finca = $this->db->queryOne("SELECT id_finca FROM racimos_cosechados WHERE id = $postdata->id_formulario");
            $forms = $this->db->queryAll("SELECT id, cantidad, color, lote, palanca FROM racimos_cosechados_by_color WHERE json = '{$json}'");
            $grupo_racimo = $this->db->queryOne("SELECT MAX(grupo_racimo) + 1 FROM produccion_racimos WHERE id_finca = $id_finca");
            $id_finca_procesado_en = $this->db->queryOne("SELECT id_finca_procesado_en FROM produccion_racimos WHERE fecha = '{$fecha}'");

            $n = 1;
            foreach($forms as $rw){
                $rw->cantidad = (int) $rw->cantidad;
                $recus = $this->db->queryAll("SELECT causa FROM racimos_cosechados_detalle WHERE json = '{$json}' AND color_cinta = '{$rw->color}'");
                $peso_prom = (float) $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_racimos WHERE id_finca = $id_finca AND lote = '{$rw->lote}' AND cinta = '{$rw->color}' AND fecha = '{$fecha}'");

                for($x = 1; $x <= $rw->cantidad; $x++){
                    $type = 'PROC';
                    $causa = 'NULL';
                    if($x <= count($recus)){
                        $type = 'RECU';
                        $causa = "'{$recus[$x-1]->causa}'";
                    }

                    $sql = "INSERT INTO produccion_racimos SET
                                id_finca = $id_finca,
                                finca = (SELECT nombre FROM fincas WHERE id = $id_finca),
                                id_finca_procesado_en = $id_finca_procesado_en,
                                finca_procesado_en = (SELECT nombre FROM fincas WHERE id = $id_finca_procesado_en),
                                lote = '{$rw->lote}',
                                cuadrilla = '{$rw->palanca}',
                                fecha = '{$fecha}',
                                grupo_racimo = {$grupo_racimo},
                                num_racimo = $n,
                                cinta = '{$rw->color}',
                                peso = '{$peso_prom}',
                                edad = getEdadCinta(getWeek('{$fecha}'), '{$rw->color}', getYear('{$fecha}')),
                                semana = getWeek('{$fecha}'),
                                year = getYear('{$fecha}'),
                                tipo = '{$type}',
                                causa = {$causa}";
                    $this->db->query($sql);
                    $n++;
                }
            }
            $this->db->query("INSERT INTO produccion_racimos_viajes_relacionados SET id_finca = $id_finca, grupo_racimo = $grupo_racimo, id_formulario = $postdata->id_formulario");
        }

        return $response;
    }

    public function cambiarLote(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;
        
        if($postdata->id_finca > 0 && ($postdata->grupo_racimo > 0 || $postdata->id_formulario > 0) && $postdata->lote != ''){
            $response->status = 200;
            if($postdata->grupo_racimo > 0){
                $this->db->query("UPDATE produccion_racimos SET lote = '{$postdata->lote}' WHERE id_finca = '{$postdata->id_finca}' AND grupo_racimo = '{$postdata->grupo_racimo}'");
            }
            if($postdata->id_formulario > 0){
                $this->db->query("UPDATE racimos_cosechados SET lote = '{$postdata->lote}' WHERE id = $postdata->id_formulario");
                $json = $this->db->queryOne("SELECT json FROM racimos_cosechados WHERE id = $postdata->id_formulario");
                $this->db->query("UPDATE racimos_cosechados_by_color SET lote = '{$postdata->lote}' WHERE json = '{$json}'");
            }
        }

        return $response;
    }

    public function cambiarCuadrilla(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response = new stdClass;
        $response->status = 400;
        
        if($postdata->id_finca > 0 && ($postdata->grupo_racimo > 0 || $postdata->id_formulario > 0) && $postdata->cuadrilla != ''){
            $response->status = 200;

            if($postdata->grupo_racimo > 0){
                $this->db->query("UPDATE produccion_racimos SET cuadrilla = '{$postdata->cuadrilla}' WHERE id_finca = '{$postdata->id_finca}' AND grupo_racimo = '{$postdata->grupo_racimo}'");
            }
            if($postdata->id_formulario > 0){
                $this->db->query("UPDATE racimos_cosechados SET palanca = '{$postdata->cuadrilla}' WHERE id = $postdata->id_formulario");
                $json = $this->db->queryOne("SELECT json FROM racimos_cosechados WHERE id = $postdata->id_formulario");
                $this->db->query("UPDATE racimos_cosechados_by_color SET palanca = '{$postdata->cuadrilla}' WHERE json = '{$json}'");
            }
        }

        return $response;
    }
}
