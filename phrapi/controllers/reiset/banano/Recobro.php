<?php defined('PHRAPI') or die("Direct access not allowed!");

class Recobro {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    // ENFUNDE VS RACIMOS COSECHADOS
    // ENFUNDE VS RACIMOS COSECHADOS
	public function data(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        if($postdata->lote != ''){
            $sWhere .= " AND lote = '{$postdata->lote}'";
        }
        if($postdata->sector != ''){
            $sWhere .= " AND sector = '{$postdata->sector}'";
        }

        $response->lotes = $this->db->queryAllOne("
            SELECT lote 
            FROM (
                SELECT lote
                FROM produccion_enfunde
                WHERE 1=1 {$sWhere}
                GROUP BY lote
                UNION ALL
                SELECT lote
                FROM produccion_racimos_formularios
                WHERE 1=1 {$sWhere}
                GROUP BY lote
            ) tbl 
            GROUP BY lote 
            ORDER BY lote");

        $sql_edades = "";
        $edades = $this->db->queryAllOne("SELECT edad FROM produccion_racimos_formularios WHERE semana_enfundada IS NOT NULL AND edad IS NOT NULL AND edad != 'N/A' {$sWhere} GROUP BY edad ORDER BY CAST(edad AS DECIMAL)");
        foreach($edades as $e){
            if($postdata->lote != '')
                $sql_edades .= " (SELECT COUNT(1) FROM produccion_racimos_formularios WHERE edad = {$e} {$sWhere} AND semana_enfundada = tbl.semana_enfundada AND anio_enfundado = tbl.anio_enfundado AND lote = tbl.lote) sem_{$e}, ";
            else
                $sql_edades .= " (SELECT COUNT(1) FROM produccion_racimos_formularios WHERE edad = {$e} {$sWhere} AND semana_enfundada = tbl.semana_enfundada AND anio_enfundado = tbl.anio_enfundado) sem_{$e}, ";
        }
        $response->semanas_edad = $edades;

        if($postdata->lote == '')
            $response->data = $this->getDataBySemana($sql_edades, $postdata->sector);
        else
            $response->data = $this->getDataByLote($sql_edades, $postdata->lote, $postdata->sector);
        return $response;
    }

    private function getDataBySemana($sql_edades, $sector){
        $sWhere = "";
        if($sector != ''){
            $sWhere .= " AND sector = '{$sector}'";
        }
        $sql = "SELECT
                    anio_enfundado,
                    semana_enfundada,
                    (SELECT SUM(usadas) FROM produccion_enfunde WHERE years = anio_enfundado AND semana = semana_enfundada {$sWhere}) AS enfunde,
                    {$sql_edades}
                    CONCAT(anio_enfundado, ' - ', semana_enfundada) AS sem_enf
                FROM (
                    SELECT anio_enfundado, semana_enfundada
                    FROM produccion_racimos_formularios
                    WHERE semana_enfundada IS NOT NULL AND semana_enfundada > 0 AND anio_enfundado > 0 {$sWhere}
                    GROUP BY anio_enfundado, semana_enfundada
                    UNION ALL
                    SELECT years, semana
                    FROM produccion_enfunde 
                    WHERE semana IS NOT NULL AND semana > 0 AND years > 0 {$sWhere}
                    GROUP BY years, semana
                ) tbl
                GROUP BY anio_enfundado, semana_enfundada
                ORDER BY anio_enfundado, semana_enfundada";
        $data = $this->db->queryAll($sql);
        foreach($data as $row){
            $row->total = 0;
            if($row->enfunde > 0) $row->enfunde = round($row->enfunde, 0);

            foreach($row as $col => $val){

                if($col != 'sem_enf')
                if(strpos($col, "sem_") !== false){
                    $row->total += (int) $val;

                    if(!$val > 0) $val = '';
                    if($val > 0) $val = round($val, 0);
                }
            }

            $row->saldo = $row->enfunde - $row->total;
            if($row->enfunde > 0 && $row->total > 0) $row->rec = round(($row->total / $row->enfunde) * 100, 2);

            $row->cinta = $this->db->queryOne("SELECT color FROM semanas_colores WHERE semana = $row->semana_enfundada AND year = $row->anio_enfundado");
            $row->class = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = '$row->cinta'");

            $row->porc_total = $row->rec;
            $row->no_recuperable = round(((100-$row->porc_total) / 100) * $row->enfunde, 0);
        }
        return $data;
    }

    private function getDataByLote($sql_edades, $lote, $sector){
        $sWhere = "";
        if($sector != ''){
            $sWhere .= " AND sector = '{$sector}'";
        }

        $sql = "SELECT
                    anio_enfundado,
                    semana_enfundada,
                    (SELECT SUM(usadas) FROM produccion_enfunde WHERE years = anio_enfundado AND semana = semana_enfundada AND lote = tbl.lote {$sWhere}) AS enfunde,
                    {$sql_edades}
                    CONCAT(anio_enfundado, ' - ', semana_enfundada) AS sem_enf
                FROM (
                    SELECT anio_enfundado, semana_enfundada, lote
                    FROM produccion_racimos_formularios
                    WHERE semana_enfundada IS NOT NULL AND lote = '{$lote}' AND semana_enfundada > 0 {$sWhere}
                    GROUP BY anio_enfundado, semana_enfundada
                    UNION ALL
                    SELECT years, semana, lote
                    FROM produccion_enfunde
                    WHERE semana IS NOT NULL AND lote = '{$lote}' AND semana > 0 {$sWhere}
                    GROUP BY years, semana
                ) tbl
                GROUP BY anio_enfundado, semana_enfundada
                ORDER BY anio_enfundado, semana_enfundada";
        $data = $this->db->queryAll($sql);
        foreach($data as $row){
            $row->total = 0;
            if($row->enfunde > 0) $row->enfunde = round($row->enfunde, 0);

            foreach($row as $col => $val){

                if($col != 'sem_enf')
                if(strpos($col, "sem_") !== false){
                    $row->total += (int) $val;

                    if(!$val > 0) $val = '';
                    if($val > 0) $val = round($val, 0);
                }
            }

            $row->saldo = $row->enfunde - $row->total;
            if($row->enfunde > 0 && $row->total > 0) $row->rec = round(($row->total / $row->enfunde) * 100, 2);

            $row->cinta = $this->db->queryOne("SELECT color FROM semanas_colores WHERE semana = $row->semana_enfundada AND year = $row->anio_enfundado");
            $row->class = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = '$row->cinta'");

            $row->porc_total = $row->rec;
            $row->no_recuperable = round(((100-$row->porc_total) / 100) * $row->enfunde, 0);
        }
        return $data;
    }
	/*public function data(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $response->lotes = $this->db->queryAllOne("SELECT lote FROM recobro_data_lote GROUP BY lote ORDER BY lote");

        $sWhere = "";
        if(isset($postdata->lote) && $postdata->lote != ''){
            $sWhere .= " AND lote = '{$postdata->lote}'";
        }
        if(isset($postdata->sector) && $postdata->sector != ''){
            $sWhere .= " AND sector = '{$postdata->sector}'";
        }

        $semanas_edad = $this->db->queryAllOne("SHOW COLUMNS FROM recobro_data");
        $sql_edades = "";
        foreach($semanas_edad as $column){
            if(strpos($column, 'sem_') !== false){
                $hasData = $this->db->queryOne("SELECT COUNT(1) FROM recobro_data_lote WHERE {$column} > 0 $sWhere") > 0;
                if($hasData){
                    $response->semanas_edad[] = (int) str_replace('sem_', '', $column);
                    $sql_edades .= ", IF({$column} = 0 OR {$column} IS NULL, '', {$column}) AS '{$column}'";
                }
            }
        }
        asort($response->semanas_edad);
        if(!isset($postdata->lote) || $postdata->lote == '')
            $response->data = $this->getDataBySemana($sql_edades, $sWhere);
        else
            $response->data = $this->getDataByLote($sql_edades, $postdata->lote, $sWhere);
        return $response;
    }

    private function getDataBySemana($sql_edades, $sWhere){
        return $this->db->queryAll("SELECT *, (total - enfunde) AS saldo, 
                IFNULL(ROUND((total / enfunde * 100), 2), '') AS rec,
                CONCAT(IF(semana + 13 > num_sem, anio + 1, anio), ' - ', IF(semana + 13 > num_sem, (semana + 13 - num_sem), semana + 13)) AS 's_proc',
                (SELECT COUNT(1) FROM produccion_racimos_formularios WHERE year = IF(tbl.semana + 13 > num_sem, tbl.anio + 1, tbl.anio) AND semana = IF(tbl.semana + 13 > num_sem, (tbl.semana + 13 - num_sem), tbl.semana + 13) {$sWhere}) AS 't_sem'
            FROM(
                SELECT semanas.semana, semanas.anio, semanas.sem_enf, semanas.enfunde, IFNULL(data.total, '') AS total, colores.class, (SELECT COUNT(1) FROM semanas_colores WHERE year = semanas.anio) AS num_sem $sql_edades
                FROM (
                    SELECT semana, anio, sem_enf, enfunde
                    FROM (
                        SELECT semana, years AS anio, CONCAt(years, ' - ', semana) AS sem_enf, ROUND(SUM(usadas), 0) AS enfunde
                        FROM produccion_enfunde
                        WHERE 1=1 {$sWhere}
                        GROUP BY years, semana
                        UNION ALL
                        SELECT semana, anio, CONCAt(anio, ' - ', semana) AS sem_enf, 0 AS enfunde
                        FROM recobro_data
                        WHERE 1=1 {$sWhere}
                        GROUP BY anio, semana
                    ) AS tbl
                    GROUP BY anio, semana
                ) AS semanas
                LEFT JOIN (
                    SELECT semana, year, s.color, c.class
                    FROM semanas_colores s
                    INNER JOIN produccion_colores c ON c.color = s.color
                ) AS colores ON semanas.semana = colores.semana AND semanas.anio = colores.year
                LEFT JOIN (
                    SELECT semana, anio,
                        total 
                        $sql_edades
                    FROM recobro_data
                    WHERE 1=1 {$sWhere}
                ) AS data ON data.anio = semanas.anio AND data.semana = semanas.semana
                ORDER BY semanas.anio, semanas.semana
            ) AS tbl");
    }

    private function getDataByLote($sql_edades, $lote, $sWhere){
        return $this->db->queryAll("SELECT *, (total - enfunde) AS saldo, 
                IFNULL(ROUND((total / enfunde * 100), 2), '') AS rec,
                CONCAT(IF(semana + 13 > num_sem, anio + 1, anio), ' - ', IF(semana + 13 > num_sem, (semana + 13 - num_sem), semana + 13)) AS 's_proc',
                (SELECT COUNT(1) FROM produccion_racimos_formularios WHERE year = IF(tbl.semana + 13 > num_sem, tbl.anio + 1, tbl.anio) AND semana = IF(tbl.semana + 13 > num_sem, (tbl.semana + 13 - num_sem), tbl.semana + 13) AND lote = '{$lote}' {$sWhere}) AS 't_sem'
            FROM(
                SELECT semanas.semana, semanas.anio, semanas.sem_enf, semanas.enfunde, IFNULL(data.total, '') AS total, colores.class, (SELECT COUNT(1) FROM semanas_colores WHERE year = semanas.anio) AS num_sem $sql_edades
                FROM (
                    SELECT semana, anio, sem_enf, enfunde
                    FROM (
                        SELECT semana, years AS anio, CONCAt(years, ' - ', semana) AS sem_enf, ROUND(SUM(usadas), 0) AS enfunde
                        FROM produccion_enfunde
                        WHERE lote = '{$lote}' {$sWhere}
                        GROUP BY years, semana
                        UNION ALL
                        SELECT semana, anio, CONCAt(anio, ' - ', semana) AS sem_enf, 0 AS enfunde
                        FROM recobro_data_lote
                        WHERE lote = '{$lote}' {$sWhere}
                        GROUP BY anio, semana
                    ) AS tbl
                    GROUP BY anio, semana
                ) AS semanas
                LEFT JOIN (
                    SELECT semana, year, s.color, c.class
                    FROM semanas_colores s
                    INNER JOIN produccion_colores c ON c.color = s.color
                ) AS colores ON semanas.semana = colores.semana AND semanas.anio = colores.year
                LEFT JOIN (
                    SELECT semana, anio, lote,
                        total 
                        $sql_edades
                    FROM recobro_data_lote
                    WHERE lote = '{$lote}' {$sWhere}
                ) AS data ON data.anio = semanas.anio AND data.semana = semanas.semana
                ORDER BY semanas.anio, semanas.semana
            ) AS tbl");
    }*/

    // ENFUNDE VS PRECALIBRACION
    public function enfundePrecalibracion(){
        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT *, (total - enfunde) AS saldo, 
                ROUND((total / enfunde * 100), 2) AS rec,
                CONCAT(IF(semana + 14 > num_sem, anio + 1, anio), ' - ', IF(semana + 14 > num_sem, (semana + 14 - num_sem), semana + 14)) AS 's_proc',
                0 AS 't_sem'
            FROM(
                SELECT semanas.semana, semanas.anio, semanas.sem_enf, semanas.enfunde, data.total, colores.class, (SELECT COUNT(1) FROM semanas_colores WHERE year = semanas.anio) AS num_sem $sql_edades
                FROM (
                    SELECT semana, years AS anio, CONCAt(years, ' - ', semana) AS sem_enf, ROUND(SUM(usadas), 0) AS enfunde
                    FROM produccion_enfunde
                    GROUP BY years, semana
                    ORDER BY years, semana
                ) AS semanas
                LEFT JOIN (
                    SELECT semana, year, s.color, c.class
                    FROM semanas_colores s
                    INNER JOIN produccion_colores c ON c.color = s.color
                ) AS colores ON semanas.semana = colores.semana AND semanas.anio = colores.year
                LEFT JOIN (
                    SELECT semana, anio,
                        total 
                        $sql_edades
                    FROM recobro_data 
                ) AS data 
                ON data.anio = semanas.anio AND data.semana = semanas.semana
            ) AS tbl");
        return $response;
    }

    // PRECALIBRACION VS RACIMOS COSECHADOS
    public function precalibracionRacimos(){
        $response = new stdClass;

        return $response;
    }
}
