<?php
	/**
	*  CLASS FROM GERENTES
	*/
	class Gerentes 
	{
		private $conexion;
		private $session;
		
		public function __construct()
		{
			$this->conexion = new M_Conexion();
        	$this->session = Session::getInstance();
		}

		public function index(){
			$sWhere = "";
			$sOrder = " ORDER BY sector.id";
			$DesAsc = "ASC";
			$sOrder .= " {$DesAsc}";
			$sLimit = "";
			if(isset($_POST)){

				/*----------  ORDER BY ----------*/
				
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 1){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY sector.id {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 2){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY sector.nombre {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 3){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY cat_usuarios.usuario {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 4){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY (SELECT COUNT(id_gerente) FROM cat_fincas WHERE id_gerente = sector.id)  {$DesAsc}";
				}
				if(isset($_POST['order'][0]['column']) && $_POST['order'][0]['column'] == 5){
					$DesAsc = $_POST['order'][0]['dir'];
					$sOrder = " ORDER BY sector.status {$DesAsc}";
				}
				/*----------  ORDER BY ----------*/

				if(isset($_POST['search_id']) && trim($_POST['search_id']) != ""){
					$sWhere .= " AND sector.id = ".$_POST["search_id"];
				}				
				if(isset($_POST['search_name']) && trim($_POST['search_name']) != ""){
					$sWhere .= " AND sector.nombre LIKE '%".$_POST['search_name']."%'";
				}
				if(isset($_POST['search_user']) && trim($_POST['search_user']) != ""){
					$sWhere .= " AND cat_usuarios.usuario LIKE '%".$_POST['search_user']."%'";
				}
				if(isset($_POST['search_pass']) && trim($_POST['search_pass']) != ""){
					$sWhere .= " AND (SELECT COUNT(id_gerente) FROM cat_fincas WHERE id_gerente = sector.id)  LIKE '%".$_POST['search_pass']."%'";
				}
				if(isset($_POST['order_status']) && trim($_POST['order_status']) != ""){
					$sWhere .= " AND sector.status = ".$_POST["order_status"];
				}

				/*----------  LIMIT  ----------*/
				if(isset($_POST['length']) && $_POST['length'] > 0){
					$sLimit = " LIMIT ".$_POST['start'].",".$_POST['length'];
				}
			}

			$sql = "SELECT sector.id, id_usuario, sector.nombre, status, sector.email, hectarea, usuario, pass
					,(SELECT COUNT(id_gerente) FROM cat_fincas WHERE id_gerente = sector.id) AS totalFincas
					FROM cat_gerentes AS sector 
					LEFT JOIN cat_usuarios ON cat_usuarios.id = sector.usuario_id
					WHERE sector.id_usuario = '{$this->session->logged}' $sWhere 
					GROUP BY sector.id 
					$sOrder $sLimit";
			$res = $this->conexion->link->query($sql);
			$datos = (object)[
				"customActionMessage" => "Error al consultar la informacion",
				"customActionStatus" => "Error",
				"data" => [],
				"draw" => 0,
				"recordsFiltered" => 0,
				"recordsTotal" => 0,
			];
			$count = 0;
			while($fila = $res->fetch_assoc()){
				$fila = (object)$fila;
				$count++;
				$datos->data[] = [
					$count,
					$fila->id,
					$fila->nombre,
					$fila->email,
					$fila->totalFincas,
					$fila->status,
					'<button class="btn btn-sm green btn-editable btn-outline margin-bottom"><i class="fa fa-plus"></i> Editar</button>'					
				];
			}

			$datos->recordsTotal = count($datos->data);
			$datos->customActionMessage = "Información completada con éxito";
			$datos->customActionStatus = "OK";

			return json_encode($datos);
		}

		public function create(){
			$data = json_decode($_POST['params']);
			if($data->nombre != ""){
				$sql = "INSERT INTO cat_gerentes SET 
				nombre = '{$data->nombre}' , 
				id_usuario = '{$this->session->logged}'";
				$ids = (int)$this->conexion->Consultas(1,$sql);
				if($ids > 0){
					if($data->user != "" && $data->pass != ""){
						/* CREACION DE USUARIO */
						$id_usuario = 0;
						$res = $this->conexion->link->query("SELECT * FROM cat_gerentes WHERE id = {$ids} AND usuario_id IS NOT NULL");
						if($cuenta = $res->fetch_assoc()){
							$id_usuario = $cuenta["usuario_id"];
							$this->conexion->Consultas(1, "UPDATE cat_usuarios SET email = '{$data->user}', usuario = '{$data->user}', pass = '{$data->pass}', nombre = '{$data->nombre}' WHERE id = '{$id_usuario}'");
						}else{
							$id_usuario = $this->conexion->Consultas(1, "INSERT INTO cat_usuarios SET email = '{$data->user}', usuario = '{$data->user}', pass = '{$data->pass}', nombre = '{$data->nombre}', tipo = 'CICLOS'");
						}

						/* INFO PERSONAL */
						$sql = "UPDATE cat_gerentes SET 
							usuario_id = '{$id_usuario}'
							WHERE id = {$ids} AND id_usuario = '{$this->session->logged}'";
						$this->conexion->Consultas(1,$sql);
					}
				}

				/* ASIGNACION DE FINCAS */
				if($data->fincas){
					foreach($data->fincas as $finca){
						$res = $this->conexion->link->query("SELECT * FROM cat_fincas WHERE id_finca = '{$finca}'");
						if($res->num_rows == 0){
							$sql_fincas = "UPDATE cat_fincas SET id_gerente = {$ids} WHERE id = {$finca}";
							$this->conexion->Consultas(1,$sql_fincas);
						}
					}
				}

        		return $ids;
			}
		}

		public function update(){
			$data = json_decode($_POST["params"]);
			// print_r($data);
			if((int)$data->id > 0 && $data->nombre != ""){
				$sCampo = "";
				if($data->user != "" && $data->pass != ""){
					/* CREACION DE USUARIO */
					$id_usuario = 0;
					$res = $this->conexion->link->query("SELECT * FROM cat_gerentes WHERE id = {$data->id} AND usuario_id IS NOT NULL");
					if($cuenta = $res->fetch_assoc()){
						$id_usuario = $cuenta["usuario_id"];
						$this->conexion->Consultas(1, "UPDATE cat_usuarios SET email = '{$data->user}', usuario = '{$data->user}', pass = '{$data->pass}', nombre = '{$data->nombre}' WHERE id = '{$id_usuario}'");
					}else{
						$id_usuario = $this->conexion->Consultas(1, "INSERT INTO cat_usuarios SET email = '{$data->user}', 
							usuario = '{$data->user}', pass = '{$data->pass}', nombre = '{$data->nombre}', tipo = 'CICLOS'");
					}
					$sCampo = ",email = '{$data->user}',usuario_id = '{$id_usuario}'";
				}
				
				/* INFO PERSONAL */
				$sql = "UPDATE cat_gerentes SET 
					nombre = '{$data->nombre}' 
					{$sCampo}
					WHERE id = {$data->id} AND id_usuario = '{$this->session->logged}'";
				$this->conexion->Consultas(1,$sql);


				/* ASIGNACION DE FINCAS */
				$sql_fincas = "UPDATE cat_fincas SET id_gerente = NULL WHERE id_gerente = {$data->id}";
				$this->conexion->Consultas(1,$sql_fincas);
				if($data->fincas){
					foreach($data->fincas as $finca){						
						$sql_fincas = "UPDATE cat_fincas SET id_gerente = {$data->id} WHERE id = {$finca}";
						$this->conexion->Consultas(1,$sql_fincas);
					}
				}
        		return $data->id;
			}
			return false;
		}

		public function changeStatus(){
			extract($_POST);
			if((int)$id > 0 ){
				$status = ($estado == 'activo') ? 1 : 0;
				$sql = "UPDATE cat_gerentes SET status={$status} WHERE id = {$id} AND id_usuario = '{$this->session->logged}'";
				// echo $sql;
				$this->conexion->Consultas(1,$sql);
        		return $id;
			}
		}

		public function edit(){
			$response = (object)[
				"success" => 400,
				"data" => [],
				"clientes" => [],
				"fincas" => [],
			];
			$response->fincas = $this->getFincas($_GET['id']);
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				if($id > 0){
					$sql = "SELECT * FROM cat_gerentes WHERE id_usuario = '{$this->session->logged}' AND id = {$id}";
					$res = $this->conexion->link->query($sql);
					if($res->num_rows > 0){
						$response->data = (object)$res->fetch_assoc();
					}
					$response->success = 200;
				}
			}
			return json_encode($response);
		}

		private function getClient(){
			$cliente = [];
			$sql = "SELECT id , nombre FROM cat_clientes WHERE id_usuario = '{$this->session->logged}' AND status > 0";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}

		private function getFincas($id_gerente){
			$selected = "";
			if($id_gerente){
				$selected = ", IF(id_gerente = '{$id_gerente}' , 'selected' , '') AS selected";
			}
			$cliente = [];
			$sql = "SELECT cat_fincas.id , cat_fincas.nombre  , 
					IF(id_gerente > 0 OR id_gerente != '' , 'Fincas con Gerentes' , 'Fincas sin Gerentes') AS grupo
					,id_gerente  {$selected}
					FROM cat_fincas 
					LEFT JOIN cat_gerentes ON cat_fincas.id_gerente = cat_gerentes.id
					WHERE cat_fincas.status > 0 
					ORDER BY id_gerente , nombre";
			$res = $this->conexion->link->query($sql);
			while($fila = $res->fetch_assoc()){
				$cliente[] = (object)$fila;
			}

			return $cliente;
		}
	}
?>
