<?php defined('PHRAPI') or die("Direct access not allowed!");

class Calidad2 {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->session = Session::getInstance();
		$this->db = DB::getInstance($this->session->agent_user);
		$this->postdata = (object)json_decode(file_get_contents("php://input"));
    }
    
    public function last(){
		$response = new stdClass;
		$response->days = $this->db->queryAllOne("SELECT fecha FROM calidad GROUP BY fecha");
		$response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM calidad");
        return $response;
	}

	public function variables(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		$sFinca = "";
		if($this->postdata->id_finca > 0){
			$sFinca .= " AND id_finca = {$this->postdata->id_finca}";
		}

		$response->marcas = $this->db->queryAllOne("SELECT marca FROM calidad WHERE 1=1 {$sWhere} {$sFinca} GROUP BY marca");

		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}
		$response->fincas = $this->db->queryAll("SELECT id_finca as id, finca FROM calidad WHERE 1=1 {$sWhere} GROUP BY id_finca");
		return $response;
	}
	
	public function index(){
		$response = new stdClass;
		$response->status = 200;
		$response->tags = $this->tags();
		$response->defectos = $this->defectos();
		$response->empaque = $this->empaque();
		$response->cluster = $this->cluster();
		$response->fotos = $this->fotos();
		return $response;
	}

	private function fotos(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
			$sWhere .= " AND id_finca = {$this->postdata->id_finca}";
		}
		if(isset($this->postdata->marca) && $this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT id_calidad, type, path, calidad_cluster, fecha
				FROM calidad
				INNER JOIN calidad_images ON id_calidad = calidad.id
				WHERE 1=1 {$sWhere}
				ORDER BY calidad.id";
		$response->data = $this->db->queryAll($sql);
		return $response;
	}

	private function cluster(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
			$sWhere .= " AND id_finca = {$this->postdata->id_finca}";
		}
		if(isset($this->postdata->marca) && $this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT cluster_dedos as tipo, SUM(cantidad) cantidad
				FROM calidad
				INNER JOIN calidad_dedos ON id_calidad = calidad.id
				WHERE cantidad > 0 {$sWhere}
				GROUP BY cluster_dedos";
		$response->data = $this->db->queryAll($sql);
		return $response;
	}

	private function tags(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
			$sWhere .= " AND id_finca = {$this->postdata->id_finca}";
		}
		if(isset($this->postdata->marca) && $this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$response->calidad_cluster = (float) $this->db->queryOne("SELECT ROUND(AVG(calidad_cluster), 2) FROM calidad WHERE 1=1 $sWhere");
		$response->calidad_dedos = (float) $this->db->queryOne("SELECT ROUND(AVG(calidad_dedos), 2) FROM calidad WHERE 1=1 $sWhere");
		$response->cluster_promedio = (float) $this->db->queryOne("SELECT ROUND(AVG(cantidad_cluster_caja), 2) FROM calidad WHERE 1=1 $sWhere");
		#$response->total_defectos = (float) $this->db->queryOne("SELECT ROUND(SUM(cantidad), 2) FROM calidad INNER JOIN calidad_detalle ON id_calidad = calidad.id WHERE 1=1 $sWhere");
		$response->calidad_empaque = (float) 0;
		$response->peso_prom_cluster = (float) $this->db->queryOne("SELECT ROUND(AVG(cantidad), 2) FROM calidad INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.id WHERE 1=1 $sWhere");
		$response->dedos_promedio = (float) $this->db->queryOne("SELECT ROUND(AVG(cantidad_dedos), 2) FROM calidad WHERE 1=1 $sWhere");
		return $response;
	}

	private function defectos(){
		$response = new stdClass;

		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
			$sWhere .= " AND id_finca = {$this->postdata->id_finca}";
		}
		if(isset($this->postdata->marca) && $this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		// DATA
		$sql = "SELECT *
                FROM calidad 
				WHERE 1=1 {$sWhere}
				ORDER BY id";
		$response->data = $this->db->queryAll($sql);

		foreach($response->data as $row){
			$sql = "SELECT type, campo, SUM(cantidad) cantidad
					FROM calidad
					INNER JOIN calidad_detalle ON id_calidad = calidad.id
					WHERE calidad.id = $row->id {$sWhere}
					GROUP BY type, campo";
			$defectos = $this->db->queryAll($sql);
			$_defectos = [];

			$row->total_defectos = 0;
			foreach($defectos as $d){
				$_defecto = explode("-", $d->campo);
				$_defectos[$d->type.'_'.trim($_defecto[0])] = $d->cantidad;
				$row->total_defectos += (int) $d->cantidad;
			}
			$row->defectos = $_defectos;
		}

		// CATEGORIAS
		$sql = "SELECT type
				FROM calidad
				INNER JOIN calidad_detalle ON id_calidad = calidad.id
				WHERE 1=1 {$sWhere}
				GROUP BY type";
		$response->categorias = $this->db->queryAll($sql);
		foreach($response->categorias as $categoria){
			$sql = "SELECT campo
					FROM calidad
					INNER JOIN calidad_detalle ON id_calidad = calidad.id
					WHERE type = '{$categoria->type}' {$sWhere}
					GROUP BY campo";
			$categoria->defectos = $this->db->queryAll($sql);
			foreach($categoria->defectos as $defecto){
				$_defecto = explode("-", $defecto->campo);
				$defecto->siglas = trim($_defecto[0]);
				$defecto->descripcion = trim($_defecto[1]);
			}
		}

		return $response;
	}

    public function empaque(){
        $response = new stdClass;
        $sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if(isset($this->postdata->id_finca) && $this->postdata->id_finca > 0){
			$sWhere .= " AND id_finca = {$this->postdata->id_finca}";
		}
		if(isset($this->postdata->marca) && $this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

        $sql = "SELECT detalle AS label, ROUND(SUM(cantidad), 2) AS value
                FROM calidad
                INNER JOIN calidad_empaque ON id_calidad = calidad.`id`
                WHERE cantidad > 0 $sWhere
                GROUP BY detalle";
        $response->data = $this->db->queryAll($sql);
        return $response;
    }
}