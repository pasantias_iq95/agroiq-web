<?php defined('PHRAPI') or die("Direct access not allowed!");

class CalidadComparacion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->session = Session::getInstance();
		$this->db = DB::getInstance($this->session->agent_user);
		$this->postdata = (object)json_decode(file_get_contents("php://input"));
    }
    
    public function last(){
		$response = new stdClass;
		$response->days = $this->db->queryAllOne("SELECT fecha FROM calidad GROUP BY fecha");
		$response->fecha = $this->db->queryOne("SELECT MAX(fecha) FROM calidad");
        return $response;
	}

	public function variables(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		$response->marcas = $this->db->queryAllOne("SELECT marca FROM calidad WHERE 1=1 {$sWhere} GROUP BY marca");
		return $response;
	}

	public function principal(){
		$response = new stdClass;
		$response->general = $this->general();
		$response->defectos = $this->defectos();
		$response->empaque = $this->empaque();
		$response->cluster = $this->cluster();
		return $response;
	}

	public function graficaZona(){
		$response = new stdClass;
		$response->defectos = $this->graficaDefectosZona();
		$response->cluster = $this->graficaClusterZona();
		$response->empaque = $this->graficaEmpaqueZona();

		$response->defectos_finca = $this->graficaDefectosFincas();
		$response->cluster_finca = $this->graficaClusterFincas();
		$response->empaque_finca = $this->graficaEmpaqueFincas();
		return $response;
	}

	// tabla principal
	public function cluster(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT cluster_dedos
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				WHERE 1=1 {$sWhere}
				GROUP BY cluster_dedos
				ORDER BY cluster_dedos";
		$response->tipos = $this->db->queryAllOne($sql);

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere}
				GROUP BY zona";
		$response->data = $this->db->queryAll($sql);
		foreach($response->data as $zona){
			$zona->total = 0;
			foreach($response->tipos as $tipo){
				$zona->{"tipo_{$tipo}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM calidad INNER JOIN fincas ON id_finca = fincas.id INNER JOIN calidad_dedos ON id_calidad = calidad.id AND cluster_dedos = {$tipo} WHERE zona = '{$zona->zona}' {$sWhere}");
				$zona->total += $zona->{"tipo_{$tipo}"};
			}

			$sql = "SELECT finca as zona, id_finca
					FROM calidad
					INNER JOIN fincas ON id_finca = fincas.id
					WHERE zona = '{$zona->zona}'
					GROUP BY id_finca";
			$zona->detalle = $this->db->queryAll($sql);
			foreach($zona->detalle as $finca){
				$finca->total = 0;
				foreach($response->tipos as $tipo){
					$finca->{"tipo_{$tipo}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM calidad INNER JOIN calidad_dedos ON id_calidad = calidad.id AND cluster_dedos = {$tipo} WHERE id_finca = $finca->id_finca {$sWhere}");
					$finca->total += $finca->{"tipo_{$tipo}"};
				}
			}
		}

		return $response;
	}

	public function empaque(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT detalle
				FROM calidad
				INNER JOIN calidad_empaque ON id_calidad = calidad.id
				WHERE cantidad > 0 {$sWhere}
				GROUP BY detalle";
		$response->defectos = $this->db->queryAllOne($sql);

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_empaque ON id_calidad = calidad.id
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere} 
				GROUP BY zona";
		$response->data = $this->db->queryAll($sql);
		foreach($response->data as $zona){
			$zona->total_defectos = 0;
			foreach($response->defectos as $defecto){
				$zona->{$defecto} = (int) $this->db->queryOne("SELECT SUM(cantidad) FROM calidad INNER JOIN fincas ON id_finca = fincas.id INNER JOIN calidad_empaque ON id_calidad = calidad.id AND detalle = '{$defecto}' WHERE zona = '{$zona->zona}' {$sWhere}");
				$zona->total_defectos += $zona->{$defecto};
			}

			$sql = "SELECT finca as zona, id_finca
					FROM calidad
					INNER JOIN fincas ON fincas.id = id_finca
					INNER JOIN calidad_empaque ON id_calidad = calidad.id
					WHERE zona = '{$zona->zona}'
					GROUP BY id_finca";
			$zona->detalle = $this->db->queryAll($sql);
			foreach($zona->detalle as $finca){
				$finca->total_defectos = 0;
				foreach($response->defectos as $defecto){
					$finca->{$defecto} = (int) $this->db->queryOne("SELECT SUM(cantidad) FROM calidad INNER JOIN calidad_empaque ON id_calidad = calidad.id AND detalle = '{$defecto}' WHERE id_finca = '{$finca->id_finca}' {$sWhere}");
					$finca->total_defectos += $finca->{$defecto};
				}
			}
		}

		return $response;
	}

	public function defectos(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}
		
		$sql = "SELECT type
				FROM calidad
				INNER JOIN calidad_detalle ON id_calidad = calidad.id
				WHERE 1=1 {$sWhere}
				GROUP BY type";
		$response->categorias = $this->db->queryAll($sql);
		foreach($response->categorias as $categoria){
			$sql = "SELECT campo
					FROM calidad
					INNER JOIN calidad_detalle ON id_calidad = calidad.id
					WHERE type = '{$categoria->type}' {$sWhere}
					GROUP BY campo";
			$categoria->defectos = $this->db->queryAll($sql);
			foreach($categoria->defectos as $defecto){
				$_defecto = explode("-", $defecto->campo);
				$defecto->siglas = trim($_defecto[0]);
				$defecto->descripcion = trim($_defecto[1]);
			}
		}

		$response->data = $this->db->queryAll("SELECT zona FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE 1=1 {$sWhere} GROUP BY zona");
		foreach($response->data as $zona){
			$zona->total_defectos = 0;
			foreach($response->categorias as $categoria){
				foreach($categoria->defectos as $defecto){
					$zona->{"{$categoria->type}_{$defecto->siglas}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM calidad INNER JOIN fincas ON id_finca = fincas.id INNER JOIN calidad_detalle ON id_calidad = calidad.id AND type = '{$categoria->type}' AND campo = '{$defecto->campo}' WHERE zona = '{$zona->zona}' {$sWhere}");
					$zona->total_defectos += $zona->{"{$categoria->type}_{$defecto->siglas}"};
				}
			}

			$zona->detalle = $this->db->queryAll("SELECT id_finca, finca as zona FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE zona = '{$zona->zona}' GROUP BY id_finca");
			foreach($zona->detalle as $finca){
				$finca->total_defectos = 0;
				foreach($response->categorias as $categoria){
					foreach($categoria->defectos as $defecto){
						$finca->{"{$categoria->type}_{$defecto->siglas}"} = $this->db->queryOne("SELECT SUM(cantidad) FROM calidad INNER JOIN calidad_detalle ON id_calidad = calidad.id AND type = '{$categoria->type}' AND campo = '{$defecto->campo}' WHERE id_finca = $finca->id_finca {$sWhere}");
						$finca->total_defectos += $finca->{"{$categoria->type}_{$defecto->siglas}"};
					}
				}
			}			
		}
		return $response;
	}

	public function general(){
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$zonas = $this->db->queryAll("SELECT zona FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE 1=1 {$sWhere} GROUP BY zona");
		foreach($zonas as $zona){
			$zona->calidad_cluster = (float) $this->db->queryOne("SELECT ROUND(AVG(calidad_cluster), 2) FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE zona = '{$zona->zona}' {$sWhere}");
			$zona->calidad_dedos = (float) $this->db->queryOne("SELECT ROUND(AVG(calidad_dedos), 2) FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE zona = '{$zona->zona}' {$sWhere}");
			$zona->calidad_empaque = (float) $this->db->queryOne("SELECT AVG(calidad_empaque) FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE zona = '{$zona->zona}' {$sWhere}");
			$zona->cluster_promedio = (float) $this->db->queryOne("SELECT ROUND(AVG(cantidad_cluster_caja), 2) FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE zona = '{$zona->zona}' {$sWhere}");
			$zona->dedos_promedio = (float) $this->db->queryOne("SELECT ROUND(AVG(cantidad_dedos_caja), 2) FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE zona = '{$zona->zona}' {$sWhere}");
			$zona->peso_prom_cluster = (float) $this->db->queryOne("SELECT ROUND(AVG(cantidad), 2) FROM calidad INNER JOIN fincas ON id_finca = fincas.id INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.id WHERE zona = '{$zona->zona}' $sWhere");
			$zona->muestras_total = 0;

			$zona->detalle = $this->db->queryAll("SELECT nombre as zona, id_finca FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE zona = '{$zona->zona}' {$sWhere} GROUP BY id_finca");
			foreach($zona->detalle as $finca){
				$finca->calidad_cluster = (float) $this->db->queryOne("SELECT ROUND(AVG(calidad_cluster), 2) FROM calidad WHERE id_finca = '{$finca->id_finca}' {$sWhere}");
				$finca->calidad_dedos = (float) $this->db->queryOne("SELECT ROUND(AVG(calidad_dedos), 2) FROM calidad WHERE id_finca = '{$finca->id_finca}' {$sWhere}");
				$finca->calidad_empaque = (float) $this->db->queryOne("SELECT AVG(calidad_empaque) FROM calidad INNER JOIN fincas ON id_finca = fincas.id WHERE id_finca = '{$finca->id_finca}' {$sWhere}");
				$finca->cluster_promedio = (float) $this->db->queryOne("SELECT ROUND(AVG(cantidad_cluster_caja), 2) FROM calidad WHERE id_finca = '{$finca->id_finca}' {$sWhere}");
				$finca->dedos_promedio = (float) $this->db->queryOne("SELECT ROUND(AVG(cantidad_dedos_caja), 2) FROM calidad WHERE id_finca = '{$finca->id_finca}' {$sWhere}");
				$finca->peso_prom_cluster = (float) $this->db->queryOne("SELECT ROUND(AVG(cantidad), 2) FROM calidad INNER JOIN calidad_pesos_cluster ON id_calidad = calidad.id WHERE id_finca = '{$finca->id_finca}' {$sWhere}");
				$finca->muestras = (int) $this->db->queryOne("SELECT COUNT(1) FROM calidad WHERE id_finca = {$finca->id_finca} {$sWhere}");

				$zona->muestras_total += $finca->muestras;
				$zona->muestras_count++;
			}
			$zona->muestras = round($zona->muestras_total / $zona->muestras_count, 2);
		}
		return $zonas;
	}


	// graficas zonas
	public function graficaDefectosZona(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$response->data = new stdClass;
		$sql = "SELECT type
				FROM calidad
				INNER JOIN fincas ON id_finca = fincas.id
				INNER JOIN calidad_detalle ON id_calidad = calidad.id AND type IS NOT NULL
				WHERE cantidad > 0 {$sWhere}
				GROUP BY type";
		$response->categorias = $this->db->queryAllOne($sql);
		foreach($response->categorias as $categoria){
			$response->data->{$categoria} = ["series" => []];

			$sql = "SELECT zona
					FROM calidad
					INNER JOIN fincas ON id_finca = fincas.id
					INNER JOIN calidad_detalle ON id_calidad = calidad.id
					WHERE cantidad > 0 AND type = '{$categoria}' {$sWhere}
					GROUP BY zona";
			$zonas = $this->db->queryAllOne($sql);

			$sql = "SELECT campo
					FROM calidad
					INNER JOIN fincas ON id_finca = fincas.id
					INNER JOIN calidad_detalle ON id_calidad = calidad.id
					WHERE cantidad > 0 AND type = '{$categoria}' {$sWhere}
					GROUP BY campo";
			$defectos = $this->db->queryAll($sql);

			foreach($defectos as $defecto){
				$siglas = trim(explode("-", $defecto->campo)[0]);
				$response->data->{$categoria}["series"][] = [
					"name" => $siglas,
					"type" => 'bar',
					"stack" => 'barra',
					"data" => []
				];
				
				foreach($zonas as $zona){
					$sql = "SELECT SUM(cantidad)
							FROM calidad
							INNER JOIN fincas ON id_finca = fincas.id
							INNER JOIN calidad_detalle ON id_calidad = calidad.id
							WHERE zona = '{$zona}' AND campo = '{$defecto->campo}' AND type = '{$categoria}' {$sWhere}";
					$val = (int) $this->db->queryOne($sql);
					$response->data->{$categoria}["series"][count($response->data->{$categoria}["series"])-1]["data"][] = $val;
				}
			}
			$response->data->{$categoria}["legends"] = $zonas;
		}

		return $response;
	}

	public function graficaClusterZona(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$response->data = new stdClass;

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere}
				GROUP BY zona";
		$zonas = $this->db->queryAllOne($sql);

		$sql = "SELECT cluster_dedos
				FROM calidad
				INNER JOIN fincas ON id_finca = fincas.id
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				WHERE cantidad > 0 {$sWhere}
				GROUP BY cluster_dedos";
		$defectos = $this->db->queryAll($sql);

		$response->data->series = [];
		foreach($defectos as $defecto){
			$response->data->series[] = [
				"name" => "{$defecto->cluster_dedos} Dedos",
				"type" => 'bar',
				"stack" => 'barra',
				"data" => []
			];
			
			foreach($zonas as $zona){
				$sql = "SELECT SUM(cantidad)
						FROM calidad
						INNER JOIN fincas ON id_finca = fincas.id
						INNER JOIN calidad_dedos ON calidad.id = id_calidad
						WHERE zona = '{$zona}' AND cluster_dedos = '{$defecto->cluster_dedos}' {$sWhere}";
				$val = (int) $this->db->queryOne($sql);
				$response->data->series[count($response->data->series)-1]["data"][] = $val;
			}
		}
		$response->data->legends = $zonas;

		return $response;
	}

	public function graficaEmpaqueZona(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$response->data = new stdClass;

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere}
				GROUP BY zona";
		$zonas = $this->db->queryAllOne($sql);

		$sql = "SELECT detalle
				FROM calidad
				INNER JOIN fincas ON id_finca = fincas.id
				INNER JOIN calidad_empaque ON calidad.id = id_calidad
				WHERE cantidad > 0 {$sWhere}
				GROUP BY detalle";
		$defectos = $this->db->queryAll($sql);

		$response->data->series = [];
		foreach($defectos as $defecto){
			$response->data->series[] = [
				"name" => $defecto->detalle,
				"type" => 'bar',
				"stack" => 'barra',
				"data" => []
			];
			
			foreach($zonas as $zona){
				$sql = "SELECT SUM(cantidad)
						FROM calidad
						INNER JOIN fincas ON id_finca = fincas.id
						INNER JOIN calidad_empaque ON calidad.id = id_calidad
						WHERE zona = '{$zona}' AND detalle = '{$defecto->detalle}' {$sWhere}";
				$val = (int) $this->db->queryOne($sql);
				$response->data->series[count($response->data->series)-1]["data"][] = $val;
			}
		}
		$response->data->legends = $zonas;

		return $response;
	}

	// graficas fincas
	public function graficaDefectosFincas(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere}
				GROUP BY zona";
		$response->zonas = $this->db->queryAllOne($sql);
		$response->data = new stdClass;

		foreach($response->zonas as $zona){
			$sql = "SELECT type
					FROM calidad
					INNER JOIN fincas ON id_finca = fincas.id
					INNER JOIN calidad_detalle ON id_calidad = calidad.id AND type IS NOT NULL
					WHERE cantidad > 0 AND zona = '{$zona}' {$sWhere}
					GROUP BY type";
			$response->categorias = $this->db->queryAllOne($sql);
			foreach($response->categorias as $categoria){
				if(!$response->data->{$zona}){
					$response->data->{$zona} =  new stdClass;
				}
				$response->data->{$zona}->{$categoria} = ["series" => []];

				$sql = "SELECT finca
						FROM calidad
						INNER JOIN fincas ON id_finca = fincas.id
						INNER JOIN calidad_detalle ON id_calidad = calidad.id
						WHERE cantidad > 0 AND type = '{$categoria}' AND zona = '{$zona}' {$sWhere}
						GROUP BY id_finca";
				$fincas = $this->db->queryAllOne($sql);

				$sql = "SELECT campo
						FROM calidad
						INNER JOIN fincas ON id_finca = fincas.id
						INNER JOIN calidad_detalle ON id_calidad = calidad.id
						WHERE cantidad > 0 AND type = '{$categoria}' AND zona = '{$zona}' {$sWhere}
						GROUP BY campo";
				$defectos = $this->db->queryAll($sql);

				foreach($defectos as $defecto){
					$siglas = trim(explode("-", $defecto->campo)[0]);
					$response->data->{$zona}->{$categoria}["series"][] = [
						"name" => $siglas,
						"type" => 'bar',
						"stack" => 'barra',
						"data" => []
					];
					
					foreach($fincas as $finca){
						$sql = "SELECT SUM(cantidad)
								FROM calidad
								INNER JOIN fincas ON id_finca = fincas.id
								INNER JOIN calidad_detalle ON id_calidad = calidad.id
								WHERE finca = '{$finca}' AND campo = '{$defecto->campo}' AND type = '{$categoria}' AND zona = '{$zona}' {$sWhere}";
						$val = (int) $this->db->queryOne($sql);
						$response->data->{$zona}->{$categoria}["series"][count($response->data->{$zona}->{$categoria}["series"])-1]["data"][] = $val;
					}
				}
				$response->data->{$zona}->{$categoria}["legends"] = $fincas;
			}
		}

		return $response;
	}

	public function graficaClusterFincas(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere} AND zona != ''
				GROUP BY zona";
		$response->zonas = $this->db->queryAllOne($sql);

		$response->data = new stdClass;
		foreach($response->zonas as $zona){
			$response->data->{$zona} = new stdClass;

			$sql = "SELECT finca
					FROM calidad
					INNER JOIN calidad_dedos ON calidad.id = id_calidad
					INNER JOIN fincas ON id_finca = fincas.id
					WHERE zona = '{$zona}' {$sWhere}
					GROUP BY id_finca";
			$fincas = $this->db->queryAllOne($sql);

			$sql = "SELECT cluster_dedos
					FROM calidad
					INNER JOIN fincas ON id_finca = fincas.id
					INNER JOIN calidad_dedos ON calidad.id = id_calidad
					WHERE cantidad > 0 AND zona = '{$zona}' {$sWhere}
					GROUP BY cluster_dedos";
			$defectos = $this->db->queryAll($sql);

			$response->data->{$zona}->series = [];
			foreach($defectos as $defecto){
				$response->data->{$zona}->series[] = [
					"name" => "{$defecto->cluster_dedos} Dedos",
					"type" => 'bar',
					"stack" => 'barra',
					"data" => []
				];
				
				foreach($fincas as $finca){
					$sql = "SELECT SUM(cantidad)
							FROM calidad
							INNER JOIN fincas ON id_finca = fincas.id
							INNER JOIN calidad_dedos ON calidad.id = id_calidad
							WHERE finca = '{$finca}' AND cluster_dedos = '{$defecto->cluster_dedos}' AND zona = '{$zona}' {$sWhere}";
					$val = (int) $this->db->queryOne($sql);
					$response->data->{$zona}->series[count($response->data->{$zona}->series)-1]["data"][] = $val;
				}
			}
			$response->data->{$zona}->legends = $fincas;
		}

		return $response;
	}

	public function graficaEmpaqueFincas(){
		$response = new stdClass;
		$sWhere = " AND fecha BETWEEN '{$this->postdata->fecha_inicial}' AND '{$this->postdata->fecha_final}'";
		if($this->postdata->marca != ''){
			$sWhere .= " AND marca = '{$this->postdata->marca}'";
		}

		$sql = "SELECT zona
				FROM calidad
				INNER JOIN calidad_dedos ON calidad.id = id_calidad
				INNER JOIN fincas ON id_finca = fincas.id
				WHERE 1=1 {$sWhere} AND zona != ''
				GROUP BY zona";
		$zonas = $this->db->queryAllOne($sql);

		$response->data = new stdClass;
		foreach($zonas as $zona){

			$sql = "SELECT finca
					FROM calidad
					INNER JOIN calidad_dedos ON calidad.id = id_calidad
					INNER JOIN fincas ON id_finca = fincas.id
					WHERE zona = '{$zona}' {$sWhere}
					GROUP BY id_finca";
			$fincas = $this->db->queryAllOne($sql);

			$sql = "SELECT detalle
					FROM calidad
					INNER JOIN fincas ON id_finca = fincas.id
					INNER JOIN calidad_empaque ON calidad.id = id_calidad
					WHERE zona = '{$zona}' AND cantidad > 0 {$sWhere}
					GROUP BY detalle";
			$defectos = $this->db->queryAll($sql);

			if(!$response->data->{$zona}){
				$response->data->{$zona} = new stdClass;
			}
			$response->data->{$zona}->series = [];
			foreach($defectos as $defecto){
				$response->data->{$zona}->series[] = [
					"name" => $defecto->detalle,
					"type" => 'bar',
					"stack" => 'barra',
					"data" => []
				];
				
				foreach($fincas as $finca){
					$sql = "SELECT SUM(cantidad)
							FROM calidad
							INNER JOIN fincas ON id_finca = fincas.id
							INNER JOIN calidad_empaque ON calidad.id = id_calidad
							WHERE zona = '{$zona}' AND finca = '{$finca}' AND detalle = '{$defecto->detalle}' {$sWhere}";
					$val = (int) $this->db->queryOne($sql);
					$response->data->{$zona}->series[count($response->data->{$zona}->series)-1]["data"][] = $val;
				}
			}
			$response->data->{$zona}->legends = $fincas;
		}
		$response->zonas = $zonas;

		return $response;
	}
}