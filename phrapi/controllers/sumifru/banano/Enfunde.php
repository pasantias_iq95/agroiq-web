<?php defined('PHRAPI') or die("Direct access not allowed!");

class Enfunde extends ReactGrafica {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
        $this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }
    
    public function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $data = (object)[
            "week" => getValueFrom($postdata, 'week', ''),
            "year" => getValueFrom($postdata, 'year', date('y')),
            "id_finca" => (int) getValueFrom($postdata, 'id_finca', 0, FILTER_SANITIZE_PHRAPI_INT)
        ];
        return $data;
    }

    public function last(){
        $response = new stdClass;
        $params = $this->params();

        $response->weeks = $this->db->queryAll("
            SELECT anio, semana 
            FROM (
                SELECT anio_enfundado AS anio, semana_enfundada AS semana
                FROM racimo_web
                WHERE anio_enfundado > 0 AND semana_enfundada > 0
                GROUP BY anio_enfundado, semana_enfundada
                UNION ALL
                SELECT years, semana
                FROM produccion_enfunde
                WHERE years > 0 AND semana > 0
                GROUP BY years, semana
            ) tbl
            GROUP BY anio, semana
            ORDER BY anio, semana");

        if(!$params->year > 0 && !$params->week > 0){
            $response->last_year = end($response->weeks)->anio;
            $response->last_week = end($response->weeks)->semana;
            $params->year = $response->last_year;
            $params->week = $response->last_week;
        }
        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, fincas.nombre AS label FROM racimo_web INNER JOIN fincas ON id_finca = fincas.id WHERE id_finca > 0 AND anio_enfundado = $params->year AND semana_enfundada = $params->week GROUP BY id_finca");
        return $response;
    }

	public function index(){
        $response = new stdClass;
        $params = $this->params();

        $sWhere = "";
        if($params->id_finca > 0){
            $sWhere .= " AND id_finca = $params->id_finca";
        }

        $response->color = $this->db->queryOne("SELECT color FROM semanas_colores WHERE year = {$params->year} AND semana = {$params->week}");
        $response->color = $this->db->queryRow("SELECT * FROM produccion_colores WHERE color = '{$response->color}'");

        $sql = "SELECT 
                    id_lote,
                    lote,
                    IFNULL((SELECT SUM(usadas) FROM produccion_enfunde WHERE years = {$params->year} AND semana = {$params->week} AND lote = tbl.lote $sWhere), 0) AS racimos_enfunde
                FROM (
                    SELECT lotes.id as id_lote, lotes.nombre as lote
                    FROM produccion_enfunde
                    INNER JOIN lotes ON idFinca = id_finca AND nombre = lote
                    WHERE years = {$params->year} AND semana = {$params->week} $sWhere
                    GROUP BY lotes.id
                    UNION ALL
                    SELECT id_lote, lotes.nombre
                    FROM racimo_web
                    INNER JOIN lotes ON id_lote = lotes.id
                    WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} $sWhere
                    GROUP BY id_lote
                ) tbl
                GROUP BY id_lote";
        $response->data = $this->db->queryAll($sql);
        $response->edades = $this->db->queryAll("SELECT id_cinta, IFNULL(edad, 'S/C') AS edad FROM racimo_web WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} $sWhere GROUP BY edad ORDER BY edad");

        foreach($response->data as $row){
            foreach($response->edades as $e){
                $row->{"edad_{$e->edad}"} = (int) $this->db->queryOne("SELECT COUNT(1) FROM racimo_web WHERE anio_enfundado = {$params->year} AND semana_enfundada = {$params->week} AND id_cinta = {$e->id_cinta} AND id_lote = {$row->id_lote} AND edad = '{$e->edad}' $sWhere");
                $row->cosechados += $row->{"edad_{$e->edad}"};
            }
        }

        return $response;
    }
}
