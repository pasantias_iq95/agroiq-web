<?php defined('PHRAPI') or die("Direct access not allowed!");

class ProduccionResumenCorte {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function filters(){
        $response = new stdClass;

        $filters = $this->params();

        $response->fincas = $this->db->queryAll("SELECT id_finca id, finca nombre FROM produccion_historica WHERE fecha = '{$filters->fecha}' GROUP BY id_finca");

        return $response;
    }
    
    private function params(){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $filters = (object)[
            'fecha' => getValueFrom($postdata, 'fecha_inicial', '', FILTER_SANITIZE_STRING),
            'id_finca' => getValueFrom($postdata, 'id_finca', 0, FILTER_SANITIZE_PHRAPI_INT)
        ];
        return $filters;
    }
    
    public function resumenProceso(){
        $response = new stdClass;
        $filters = $this->params();
        
        $response->data = [
            [
                "name" => "RACIMOS CORTADOS",
                "value" => $this->getRacimosCortadosInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "RACIMOS PROCESADOS",
                "value" => $this->getRacimosProcesadosInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "RACIMOS RECUSADOS",
                "value" => 0
            ],
            [
                "name" => "PESO CAJAS (KG)",
                "value" => $this->getPesoCajasKG($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "PESO RACIMOS (KG)",
                "value" => $this->getPesoRacimosKG($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "% TALLO",
                "value" => $this->getTallo($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "PESO RAC PROM (KG)",
                "value" => $this->getPesoPromRacimosKg($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "CALIB 2DA PROM",
                "value" => $this->getCalibreProm($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "CALIB ULTIMA PROM",
                "value" => $this->getCalibreUltimaProm($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "MANOS PROM",
                "value" => $this->getManosProm($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "L DEDO PROM",
                "value" => $this->getLargoDedosProm($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "RATIO CORT",
                "value" => $this->getCajasRatioCortadoInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "RATIO PROC",
                "value" => $this->getCajasRatioProcesadoInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "MERMA CORT",
                "value" => $this->getMermaCortadaInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ],
            [
                "name" => "MERMA PROC",
                "value" => $this->getMermaProcesadaInicioFin($filters->fecha, $filters->fecha, $filters->id_finca)
            ]
        ];

        // RACIMOS RECUSADOS
        $response->data[2]["value"] = $response->data[0]["value"] - $response->data[1]["value"];

        return $response;
    }

    /* GET VALUES */
    private function getRacimosProcesadosInicioFin($inicio, $fin, $id_finca){
        $sWhere = "";
        $sWhere2 = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
            $sWhere2 .= " AND r.id_finca = $id_finca";
        }

        $racimos_cortados = "SELECT SUM(IF(blz > form, blz, form)) AS 'value'
            FROM (
                SELECT fecha, lote, cinta, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form
                FROM (
                    SELECT fecha, lote, cinta, COUNT(1) AS cantidad, 'BALANZA' AS origen
                    FROM produccion_historica
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND tipo = 'PROC' {$sWhere}
                    GROUP BY fecha, lote, cinta
                ) AS tbl
                GROUP BY fecha, lote, cinta
            ) AS tbl";
        return $this->db->queryOne($racimos_cortados);
    }
    private function getCajasConvertidasRealesInicioFin($inicio, $fin, $id_finca){
        $sWhere = "";
        $sWhere2 = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
            $sWhere2 .= " AND r.id_finca = $id_finca";
        }

        $sql = "SELECT SUM(cajas_reales)
                FROM (
                    SELECT SUM(cajas) cajas_reales
                    FROM (
                        SELECT ROUND(SUM(cajas.convertidas), 0) cajas
                        FROM produccion_cajas cajas
                        WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' $sWhere
                        GROUP BY fecha
                    ) tbl
                ) AS tbl";
        return $this->db->queryOne($sql);
    }
    private function getCajasRatioCortadoInicioFin($inicio, $fin, $id_finca){
        $response = new stdClass;
        $racimos = $this->getRacimosCortadosInicioFin($inicio, $fin, $id_finca);
        $cajas_conv = $this->getCajasConvertidasRealesInicioFin($inicio, $fin, $id_finca);
        $val = round(($cajas_conv / $racimos), 2);
        return $val;
    }
    private function getCajasRatioProcesadoInicioFin($inicio, $fin, $id_finca){
        $response = new stdClass;

        $racimos = $this->getRacimosProcesadosInicioFin($inicio, $fin, $id_finca);
        $cajas_conv = $this->getCajasConvertidasRealesInicioFin($inicio, $fin, $id_finca);
        $val = round(($cajas_conv / $racimos), 2);
        
        return $val;
    }
    private function getRacimosCortadosInicioFin($inicio, $fin, $id_finca){
        $sWhere = "";
        $sWhere2 = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
            $sWhere2 .= " AND r.id_finca = $id_finca";
        }

        $racimos_cortados = "SELECT SUM(IF(blz > form, blz, form)) AS 'value'
            FROM (
                SELECT fecha, lote, cinta, SUM(IF(origen = 'BALANZA', cantidad, 0)) AS blz, SUM(IF(origen = 'FORMULARIO', cantidad, 0)) AS form
                FROM (
                    SELECT fecha, lote, cinta, COUNT(1) AS cantidad, 'BALANZA' AS origen, tipo
                    FROM produccion_historica
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' {$sWhere}
                    GROUP BY fecha, lote, cinta, tipo
                ) AS tbl
                GROUP BY fecha, lote, cinta
            ) AS tbl";

        return $this->db->queryOne($racimos_cortados);
    }
    private function getPesoCajasKG($inicio, $fin, $id_finca){
        $sWhere = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
        }

        $sql = "SELECT SUM(peso)
                FROM (
                    SELECT SUM(kg) AS peso
                    FROM produccion_cajas
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' {$sWhere}
                ) AS tbl";
        return (float) $this->db->queryOne($sql);
    }
    private function getPesoRacimosKG($inicio, $fin, $id_finca){
        $sWhere = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
        }

        $sql = "SELECT SUM(peso)
                FROM (
                    SELECT SUM(peso) AS peso
                    FROM produccion_historica
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' {$sWhere}
                ) AS tbl";
        return (float) $this->db->queryOne($sql);
    }
    private function getPesoRacimosProcesoKG($inicio, $fin, $id_finca){
        $sWhere = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
        }

        $sql = "SELECT SUM(peso)
                FROM (
                    SELECT SUM(peso) AS peso
                    FROM produccion_historica
                    WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND tipo = 'PROC' {$sWhere}
                ) AS tbl";
        return (float) $this->db->queryOne($sql);
    }
    private function getPesoPromRacimosKg($inicio, $fin, $id_finca){
        $sWhere = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
        }

        $sql = "SELECT AVG(peso) AS peso
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' {$sWhere}";
        return (float) $this->db->queryOne($sql);
    }
    private function getCalibreProm($inicio, $fin, $id_finca){
        $sWhere = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
        }

        $sql = "SELECT AVG(calibre_segunda)
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND calibre_segunda > 0 {$sWhere}";
        return (float) $this->db->queryOne($sql);
    }
    private function getCalibreUltimaProm($inicio, $fin, $id_finca){
        $sWhere = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
        }

        $sql = "SELECT AVG(calibre_ultima)
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND calibre_ultima > 0 {$sWhere}";
        return (float) $this->db->queryOne($sql);
    }
    private function getManosProm($inicio, $fin, $id_finca){
        $sWhere = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
        }

        $sql = "SELECT AVG(manos)
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND manos > 0 {$sWhere}";
        return (float) $this->db->queryOne($sql);
    }
    private function getLargoDedosProm($inicio, $fin, $id_finca){
        $sWhere = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
        }

        $sql = "SELECT AVG(dedos)
                FROM produccion_historica
                WHERE fecha BETWEEN '{$inicio}' AND '{$fin}' AND dedos > 0 {$sWhere}";
        return (float) $this->db->queryOne($sql);
    }
    private function getTallo($inicio, $fin, $id_finca){
        $sWhere = "";
        if($id_finca != ''){
            $sWhere .= " AND id_finca = $id_finca";
        }

        $sql = "SELECT ROUND(SUM(tallo) / SUM(racimo) * 100, 2)
                FROM merma main
                WHERE date_fecha BETWEEN '{$inicio}' AND '{$fin}' {$sWhere}";
        return (float) $this->db->queryOne($sql);
    }
    private function getMermaCortadaInicioFin($inicio, $fin, $id_finca){
        $peso_cajas = $this->getPesoCajasKG($inicio, $fin, $id_finca);
        if($peso_cajas == 0) return '';
        $peso_racimos = $this->getPesoRacimosKG($inicio, $fin, $id_finca);
        $val = round(($peso_racimos - $peso_cajas) / $peso_racimos * 100, 2);
        return $val;
    }
    private function getMermaProcesadaInicioFin($inicio, $fin, $id_finca){
        $peso_cajas = $this->getPesoCajasKG($inicio, $fin, $id_finca);
        if($peso_cajas == 0) return '';
        $peso_racimos = $this->getPesoRacimosProcesoKG($inicio, $fin, $id_finca);
        $val = round(($peso_racimos - $peso_cajas) / $peso_racimos * 100, 2);
        return $val;
    }
    /* GET VALUES */
}
