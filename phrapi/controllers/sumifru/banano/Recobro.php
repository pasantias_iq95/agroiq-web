<?php defined('PHRAPI') or die("Direct access not allowed!");

class Recobro {
	public $name;
	private $db;
	private $config;

	public function __construct(){
        $this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    // ENFUNDE VS RACIMOS COSECHADOS
	public function data(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));


        $sWhere = "";
        if($postdata->id_finca > 0){
            $sWhere .= " AND id_finca = '{$postdata->id_finca}'";
        }
        $response->lotes = $this->db->queryAllOne("SELECT lotes.nombre as lote FROM racimo_web INNER JOIN lotes ON id_lote = lotes.id WHERE 1=1 $sWhere GROUP BY lotes.id ORDER BY lotes.nombre");

        $sql_edades = "";
        $edades = $this->db->queryAllOne("SELECT edad FROM racimo_web WHERE semana_enfundada IS NOT NULL AND edad IS NOT NULL AND edad != 'N/A' GROUP BY edad ORDER BY CAST(edad AS DECIMAL)");
        foreach($edades as $e){
            if($postdata->lote != '')
                $sql_edades .= " (SELECT COUNT(1) FROM racimo_web WHERE edad = {$e} AND semana_enfundada = tbl.semana_enfundada AND anio_enfundado = tbl.anio_enfundado AND id_lote = tbl.id_lote $sWhere) sem_{$e}, ";
            else
                $sql_edades .= " (SELECT COUNT(1) FROM racimo_web WHERE edad = {$e} AND semana_enfundada = tbl.semana_enfundada AND anio_enfundado = tbl.anio_enfundado $sWhere) sem_{$e}, ";
        }
        $response->semanas_edad = $edades;

        if($postdata->lote == '')
            $response->data = $this->getDataBySemana($sql_edades);
        else
            $response->data = $this->getDataByLote($sql_edades, $postdata->lote);
        return $response;
    }

    private function getDataBySemana($sql_edades){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        $sWhere = "";
        if($postdata->id_finca > 0){
            $sWhere .= " AND id_finca = '{$postdata->id_finca}'";
        }

        $sql = "SELECT
                    anio_enfundado,
                    semana_enfundada,
                    (SELECT SUM(usadas) FROM produccion_enfunde WHERE years = anio_enfundado AND semana = semana_enfundada $sWhere) AS enfunde,
                    {$sql_edades}
                    CONCAT(anio_enfundado, ' - ', semana_enfundada) AS sem_enf
                FROM (
                    SELECT anio_enfundado, semana_enfundada
                    FROM racimo_web
                    WHERE semana_enfundada IS NOT NULL $sWhere
                    GROUP BY anio_enfundado, semana_enfundada
                    UNION ALL
                    SELECT years, semana
                    FROM produccion_enfunde
                    WHERE semana IS NOT NULL $sWhere
                    GROUP BY years, semana
                ) tbl
                GROUP BY anio_enfundado, semana_enfundada
                ORDER BY anio_enfundado, semana_enfundada";
        $data = $this->db->queryAll($sql);
        
        foreach($data as $row){
            $row->total = 0;
            if($row->enfunde > 0) $row->enfunde = round($row->enfunde, 0);

            foreach($row as $col => $val){

                if($col != 'sem_enf')
                if(strpos($col, "sem_") !== false){
                    $row->total += (int) $val;

                    if(!$val > 0) $val = '';
                    if($val > 0) $val = round($val, 0);
                }
            }

            $row->saldo = $row->enfunde - $row->total;
            if($row->enfunde > 0 && $row->total > 0) $row->rec = round(($row->total / $row->enfunde) * 100, 2);

            $row->cinta = $this->db->queryOne("SELECT color FROM semanas_colores WHERE semana = $row->semana_enfundada AND year = $row->anio_enfundado");
            $row->class = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = '$row->cinta'");

            $row->porc_total = $row->rec;
            $row->no_recuperable = round(((100-$row->porc_total) / 100) * $row->enfunde, 0);
        }
        return $data;
    }

    private function getDataByLote($sql_edades, $lote){
        $sql = "SELECT
                    anio_enfundado,
                    semana_enfundada,
                    (SELECT SUM(usadas) FROM produccion_enfunde INNER JOIN lotes ON lotes.nombre = lote AND id_finca = idFinca WHERE years = anio_enfundado AND semana = semana_enfundada AND lotes.id = tbl.id_lote) AS enfunde,
                    {$sql_edades}
                    CONCAT(anio_enfundado, ' - ', semana_enfundada) AS sem_enf
                FROM (
                    SELECT anio_enfundado, semana_enfundada, id_lote
                    FROM racimo_web
                    WHERE semana_enfundada IS NOT NULL AND id_lote = '{$lote}'
                    GROUP BY anio_enfundado, semana_enfundada
                    UNION ALL
                    SELECT years, semana, lotes.id as id_lote
                    FROM produccion_enfunde
                    INNER JOIN lotes ON id_finca = lotes.idFinca AND nombre = lote
                    WHERE semana IS NOT NULL AND lotes.id = '{$lote}'
                    GROUP BY years, semana
                ) tbl
                GROUP BY anio_enfundado, semana_enfundada
                ORDER BY anio_enfundado, semana_enfundada";
        $data = $this->db->queryAll($sql);
        foreach($data as $row){
            $row->total = 0;
            if($row->enfunde > 0) $row->enfunde = round($row->enfunde, 0);

            foreach($row as $col => $val){

                if($col != 'sem_enf')
                if(strpos($col, "sem_") !== false){
                    $row->total += (int) $val;

                    if(!$val > 0) $val = '';
                    if($val > 0) $val = round($val, 0);
                }
            }

            $row->saldo = $row->enfunde - $row->total;
            if($row->enfunde > 0 && $row->total > 0) $row->rec = round(($row->total / $row->enfunde) * 100, 2);

            $row->cinta = $this->db->queryOne("SELECT color FROM semanas_colores WHERE semana = $row->semana_enfundada AND year = $row->anio_enfundado");
            $row->class = $this->db->queryOne("SELECT class FROM produccion_colores WHERE color = '$row->cinta'");

            $row->porc_total = $row->rec;
            $row->no_recuperable = round(((100-$row->porc_total) / 100) * $row->enfunde, 0);
        }
        return $data;
    }

    // ENFUNDE VS PRECALIBRACION
    public function enfundePrecalibracion(){
        $response = new stdClass;
        $response->data = $this->db->queryAll("SELECT *, (total - enfunde) AS saldo, 
                ROUND((total / enfunde * 100), 2) AS rec,
                CONCAT(IF(semana + 14 > num_sem, anio + 1, anio), ' - ', IF(semana + 14 > num_sem, (semana + 14 - num_sem), semana + 14)) AS 's_proc',
                0 AS 't_sem'
            FROM(
                SELECT semanas.semana, semanas.anio, semanas.sem_enf, semanas.enfunde, data.total, colores.class, (SELECT COUNT(1) FROM semanas_colores WHERE year = semanas.anio) AS num_sem $sql_edades
                FROM (
                    SELECT semana, years AS anio, CONCAt(years, ' - ', semana) AS sem_enf, ROUND(SUM(usadas), 0) AS enfunde
                    FROM produccion_enfunde
                    GROUP BY years, semana
                    ORDER BY years, semana
                ) AS semanas
                LEFT JOIN (
                    SELECT semana, year, s.color, c.class
                    FROM semanas_colores s
                    INNER JOIN produccion_colores c ON c.color = s.color
                ) AS colores ON semanas.semana = colores.semana AND semanas.anio = colores.year
                LEFT JOIN (
                    SELECT semana, anio,
                        total 
                        $sql_edades
                    FROM recobro_data 
                ) AS data 
                ON data.anio = semanas.anio AND data.semana = semanas.semana
            ) AS tbl");
        return $response;
    }

    // PRECALIBRACION VS RACIMOS COSECHADOS
    public function precalibracionRacimos(){
        $response = new stdClass;

        return $response;
    }
}
