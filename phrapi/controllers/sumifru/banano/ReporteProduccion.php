<?php defined('PHRAPI') or die("Direct access not allowed!");

class ReporteProduccion {
	public $name;
	private $db;
	private $config;

	public function __construct(){
		$this->config = $GLOBALS['config'];
		$this->session = Session::getInstance();
        $this->db = DB::getInstance($this->session->agent_user);
    }

    public function last(){
        $response = new stdClass;

        $response->years = $this->db->queryAllOne("SELECT YEAR FROM produccion_historica WHERE edad > 0 AND peso > 0 GROUP BY year ORDER BY year");
        $lastYear = $response->years[count($response->years)-1];
        $response->last_year = $lastYear;
        $response->weeks = $this->db->queryAllOne("SELECT semana FROM produccion_historica WHERE year = {$lastYear} GROUP BY semana ORDER BY semana");
        $response->last_week = $this->db->queryOne("SELECT MAX(semana) FROM produccion_historica WHERE year = {$lastYear}");
        return $response;
    }

    public function getFincas(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $response->fincas = $this->db->queryAllSpecial("SELECT id_finca AS id, finca AS label FROM produccion_historica WHERE year = $postdata->year AND semana = $postdata->semana GROUP BY id_finca");
        return $response;
    }
    
    public function getWeeks(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));

        $response->semanas = $this->db->queryAllOne("SELECT semana FROM produccion_historica WHERE edad > 0 AND peso > 0 AND year = $postdata->year GROUP BY semana");
        return $response;
    }

	public function main(){
        $response = new stdClass;
        $postdata = (object)json_decode(file_get_contents("php://input"));
        
        $sWhere = "";
        if($postdata->fecha_inicial && $postdata->fecha_final){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->id_finca != ''){
            $sWhere .= " AND id_finca = $postdata->id_finca";
        }
        if($postdata->year != '' && $postdata->semana != ''){
            $sWhere .= " AND year = $postdata->year AND semana = $postdata->semana";
        }

        $sql = "SELECT lote, COUNT(1) AS cosechados, SUM(peso) AS peso, SUM(IF(tipo = 'RECU', 1, 0)) AS recusados, SUM(IF(tipo = 'PROC', 1, 0)) AS procesados, ROUND(AVG(peso), 2) AS peso_prom_racimo
                FROM produccion_historica
                WHERE 1=1 $sWhere
                GROUP BY lote";
        $data = $this->db->queryAll($sql);

        $peso_cajas = $this->db->queryOne("SELECT SUM(kg) FROM (
                SELECT SUM(kg) as kg FROM produccion_cajas WHERE 1=1 $sWhere
                UNION ALL
                SELECT SUM(kg) as kg FROM produccion_gavetas WHERE 1=1 $sWhere
            ) AS tbl");
        $peso_racimos_procesados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_historica WHERE 1=1 $sWhere");
        $total_merma_lb_porc = ($peso_racimos_procesados - $peso_cajas);

        $response->totales = [
            "peso_cajas" => $peso_cajas,
            "peso_racimos_procesados" => $peso_racimos_procesados,
            "merma_proc_lb" => $total_merma_lb_porc,
            "total_peso_racimos_procesados" => 0,
            "total_racimos_procesados" => 0
        ];
        $response->edades = [];
        foreach($data as $row){
            // EDADES
            $edades = $this->getEdades($row->lote);
            foreach($edades as $ed){
                if(!in_array($ed, $response->edades)) $response->edades[] = (int) $ed;
                $row->{"edad_{$ed}"} = $this->db->queryOne("SELECT 
                        COUNT(1)
                    FROM produccion_historica 
                    WHERE lote = '{$row->lote}' AND edad = '{$ed}' $sWhere");
                $response->totales["edad_{$ed}"] += $row->{"edad_{$ed}"};
            }

            $row->edad_sc = (int) $this->db->queryOne("SELECT COUNT(1) FROM produccion_historica WHERE cinta = 'S/C' AND lote = '{$row->lote}' $sWhere");
            if($row->edad_sc > 0){
                $response->cintaSC = true;
                $response->totales["edad_sc"] += $row->edad_sc;
            }
            $row->hectareas = $this->db->queryOne("SELECT IFNULL(SUM(hectareas), 1) FROM lotes WHERE nombre = '{$row->lote}'");

            // COLUMNAS DE CAJAS (CALCULOS)
            #$row->peso_prom_racimo = $this->db->queryOne("SELECT ROUND(AVG(peso), 2) FROM produccion_historica WHERE lote = '{$row->lote}' $sWhere");
            $row->peso_racimos_cosechados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_historica WHERE lote = '{$row->lote}' $sWhere");
            $row->peso_racimos_recusados = $this->db->queryOne("SELECT SUM(peso) FROM produccion_historica WHERE lote = '{$row->lote}' AND tipo = 'RECU' $sWhere");
            $row->peso_racimos_procesados = $row->peso_racimos_cosechados - $row->peso_racimos_recusados;
            $row->merma_lb_cosechada = $row->peso_racimos_cosechados - $peso_cajas;
            $row->merma_lb_procesada = ($row->peso_racimos_cosechados - $row->peso_racimos_recusados) - $peso_cajas;
            $row->porcentaje_distribucion_merma = ($row->peso_racimos_procesados / $peso_racimos_procesados);
            $row->merma_lb_proc = ($row->porcentaje_distribucion_merma * $total_merma_lb_porc);
            $row->merma_lb_cosec = ($row->merma_lb_proc + $row->peso_racimos_recusados);
            $row->convertidas = round(($row->peso_racimos_cosechados - $row->merma_lb_cosec) / 18.3708, 0);
            $row->cajas_lb = $row->peso_racimos_cosechados - $row->merma_lb_proc;
            $row->cajas_ha = round($row->convertidas / $row->hectareas, 2);
            $row->ratio_cortado = round($row->convertidas / $row->cosechados, 2);
            $row->ratio_procesado = round($row->convertidas / $row->procesados, 2);
            $row->cajas_ha_proyeccion = round($row->cajas_ha * 52, 2);

            // TOTALES
            $response->totales["total_racimos_cosechados"] += $row->cosechados;
            $response->totales["total_racimos_procesados"] += $row->procesados;
            $response->totales["total_racimos_recusados"] += $row->recusados;
            $response->totales["total_convertidas"] += $row->convertidas;
            $response->totales["total_peso_racimos_procesados"] += $row->peso_racimos_procesados;
        }

        $response->data = $data;
        sort($response->edades, SORT_NUMERIC);
        $response->cintas = $this->db->queryAllSpecial("SELECT h.edad as id, class as label FROM produccion_historica h LEFT JOIN produccion_colores ON cinta = color WHERE 1=1 $sWhere GROUP BY h.edad, cinta");
        return $response;
    }

    private function getEdades($lote){
        $postdata = (object)json_decode(file_get_contents("php://input"));
        if($postdata->fecha_inicial && $postdata->fecha_final){
            $sWhere .= " AND fecha BETWEEN '{$postdata->fecha_inicial}' AND '{$postdata->fecha_final}'";
        }
        if($postdata->year != '' && $postdata->semana != ''){
            $sWhere .= " AND year = $postdata->year AND semana = $postdata->semana";
        }
        if($postdata->id_finca != ''){
            $sWhere .= " AND id_finca = $postdata->id_finca";
        }

        $sql = "SELECT edad
                FROM produccion_historica
                WHERE lote = '{$lote}' AND edad IS NOT NULL $sWhere AND edad > 0
                GROUP BY edad
                ORDER BY edad";
        return $this->db->queryAllOne($sql);
    }
}
