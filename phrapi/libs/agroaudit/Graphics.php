<?php defined('PHRAPI') or die("Direct access not allowed!");

class Graphics {
    public $name;
    private $db;
    private $config;

    public function __construct(){
        $this->config = $GLOBALS['config'];
    }

    public function bar_special($data = [] , $mode = "vertical" , $name = "ESTRUCTURAS LEGALES"){
        $response = new stdClass;
        $response->bar = [];
        if(count($data) > 0){
            $response->bar["title"]["show"] = true;
            $response->bar["title"]["text"] = $name;
            $response->bar["title"]["subtext"] = $this->session->sloganCompany;
            $response->bar["tooltip"]["trigger"] = "item";
            $response->bar["tooltip"]["axisPointer"]["type"] = "shadow";
            $response->bar["toolbox"]["show"] = true;
            $response->bar["toolbox"]["feature"]["mark"]["show"] = true;
            $response->bar["toolbox"]["feature"]["restore"]["show"] = true;
            $response->bar["toolbox"]["feature"]["magicType"]["show"] = false;
            $response->bar["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
            $response->bar["toolbox"]["feature"]["saveAsImage"]["show"] = true;
            $response->bar["legend"]["data"] = [];
            $response->bar["legend"]["left"] = "center";
            $response->bar["legend"]["bottom"] = "1%";
            if($mode == "vertical"){
                $response->bar["grid"]["left"] = "3%";
                $response->bar["grid"]["right"] = "4%";
                $response->bar["grid"]["bottom"] = "15%";
                $response->bar["grid"]["containLabel"] = true;
                $response->bar["xAxis"] = ["type" => "category" , "data" => [""]];
                $response->bar["yAxis"] = ["type" => "value"];
            }else if($mode == "horizontal"){
                $response->bar["grid"]["left"] = "5%";
                $response->bar["grid"]["right"] = "1%";
                $response->bar["grid"]["bottom"] = "1%";
                $response->bar["grid"]["containLabel"] = true;
                $response->bar["yAxis"] = ["type" => "category" , "data" => [] , "axisLabel" => ["margin" => 15]];
                $response->bar["xAxis"] = ["type" => "value"];
            }
            $response->bar["series"] = [];
            $count = 0;
            $count_series = 0;
            $label_series = [];
            $label_s= [];
            $pais = [];
            $pais_count = [];
            $data_series = [];
            foreach ($data as $key => $value) {
                $value = (object)$value;
                if(!in_array($value->label_yaxis, $pais)){
                    // D($value->label_yaxis);
                    $pais[] = $value->label_yaxis;
                    $pais_count[$value->label_yaxis] = $count;
                    $count++;
                }
                if($mode == "horizontal"){
                    // $response->bar["legend"]["data"][] = $value->label;
                    if(!in_array($value->label_yaxis, $response->bar["yAxis"]["data"])){
                        $response->bar["yAxis"]["data"][] = $value->label_yaxis;
                    }
                    if(!in_array($value->label, $label_s)){
                        $label_s[] = $value->label;
                        $label_series[$value->label] = $count_series;
                        $count_series++;
                        $response->bar["series"][$label_series[$value->label]] = [
                            "name" => $value->label,
                            "type" => "bar",
                            "data" => [],
                            "stack" => "Respuestas",
                            "label" => [
                                "normal" => ["show" => true , "position" => "insideRight"],
                                "emphasis" => ["show" => true , "position" => "insideRight"],
                            ]
                        ];
                    }
                    $data_series[$value->label_yaxis][$value->label] = (int)$value->value;
                    // $response->bar["series"][$label_series[$value->label]]["data"][$pais_count[$value->label_yaxis]] = (int)$value->value;
                }else{
                    $response->bar["legend"]["data"][] = $value->label;
                    $response->bar["series"][$count] = [
                        "name" => $value->label,
                        "type" => "bar",
                        "data" => [(int)$value->value],
                        "label" => [
                            "normal" => ["show" => true , "position" => "top"],
                            "emphasis" => ["show" => true , "position" => "top"],
                        ]
                    ];
                }
            }
            foreach ($label_series as $key => $value) {
                foreach ($pais as $llave => $valor) {
                    // $response->bar["series"][$value]["data"][]
                    $response->bar["series"][$value]["data"][$llave] = (isset($data_series[$valor][$key])) ? $data_series[$valor][$key] : "";
                }
            }
            // D($pais);
            // D($label_series);
            $response->label_series = $label_series;
            $response->pais = $pais;
            $response->data_series = $data_series;
        }

        return $response;
    }

    public function getOptionsMap($data , $name = "PROJECTOS" , $map = "AMERICA"){
        $response = new stdClass;
        $response->map = [];
        $response->map["backgroundColor"] = "";
        $response->map["title"]["text"] = $map;
        $response->map["title"]["subtext"] = $this->session->sloganCompany;
        $response->map["title"]["left"] = "";
        $response->map["tooltip"]["trigger"] = "item";
        $response->map["tooltip"]["formatter"] = "";
        $response->map["legend"]["orient"] = "vertical";
        $response->map["legend"]["x"] = "bottom";
        $response->map["legend"]["y"] = "right";
        $response->map["legend"]["data"] = [$nombre];
        $response->map["legend"]["textStyle"]["color"] = "#f4e925";
        $response->map["geo"]["map"] = $map;
        $response->map["geo"]["label"]["emphasis"]["show"] = true;
        $response->map["geo"]["roam"] = true;
        $response->map["geo"]["itemStyle"]["normal"]["borderColor"] = "#c49f47";
        $response->map["geo"]["itemStyle"]["normal"]["areaColor"] = "#035a03";
        $response->map["geo"]["itemStyle"]["emphasis"]["areaColor"] = "#c49f47";
        $response->map["roamController"]["show"] = true;
        $response->map["roamController"]["x"] = "right";
        $response->map["roamController"]["mapTypeControl"][$map] = true;
        $response->map["series"] = [];
        $series = [];
        $response->data = new stdClass;
        $response->geoCoordMap = new stdClass;
        $response->data = [];
        $response->geoCoordMap = [];
        foreach ($data as $key => $value) {
            $value = (object)$value;
            $response->data[] = ["nombre" => $value->label , "value" => $value->value];
            $response->geoCoordMap[$value->label] = [$value->lng , $value->lat];
        }
        return $response;
    }

    public function pie($data = [] , $radius = ['50%', '70%'] , $name = "CATEGORIAS" , $roseType = "" , $type="normal"){
        $response = new stdClass;
        $response->pie = [];
        if(count($data) > 0){
            $response->pie["title"]["show"] = true;
            $response->pie["title"]["text"] = $name;
            $response->pie["title"]["left"] = "center";
            $response->pie["title"]["subtext"] = $this->session->sloganCompany;
            $response->pie["tooltip"]["trigger"] = "item";
            $response->pie["tooltip"]["formatter"] = "{a} <br/>{b}: {c} ({d}%)";
            $response->pie["toolbox"]["show"] = true;
            $response->pie["toolbox"]["feature"]["mark"]["show"] = true;
            $response->pie["toolbox"]["feature"]["restore"]["show"] = false;
            $response->pie["toolbox"]["feature"]["saveAsImage"]["show"] = true;
            $response->pie["toolbox"]["feature"]["saveAsImage"]["name"] = $name;
            $response->pie["legend"]["orient"] = "horizontal";
            $response->pie["legend"]["left"] = "center";
            $response->bar["legend"]["bottom"] = "1%";
            $response->pie["legend"]["data"] = [];
            $response->pie["series"]["name"] = $name;
            $response->pie["series"]["type"] = "pie";
            $response->pie["series"]["radius"] = $radius;
            $response->pie["series"]["center"] = ['50%', '50%'];
            if($type != "normal"){
                $response->pie["series"]["roseType"] = $roseType;
                $response->pie["series"]["avoidLabelOverlap"] = "pie";
            }
            $response->pie["series"]["label"]["normal"]["show"] = true;
            $response->pie["series"]["label"]["emphasis"]["show"] = true;
            // $response->pie["series"]["label"]["normal"]["position"] = "center";
            // $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontSize"] = "30";
            // $response->pie["series"]["label"]["emphasis"]["textStyle"]["fontWeight"] = "bold";
            $response->pie["series"]["labelLine"]["normal"]["show"] = true;
            $response->pie["series"]["data"] = [];

            foreach ($data as $key => $value) {
                $value = (object)$value;
                $response->pie["legend"]["data"][] = $value->label;
                $response->pie["series"]["data"][] = ["name" => $value->label  , "value" => (float)$value->value , 
                        "label" => [
                            "normal" => ["show" => true , "position" => "outside"],
                            "emphasis" => ["show" => true , "position" => "outside"],
                        ]
                ];
            }
        }

        return $response->pie;
    }

    public function bar($data = [] , $mode = "vertical" , $name = "ESTRUCTURAS LEGALES"){
        $response = new stdClass;
        $response->bar = [];
        if(count($data) > 0){
            $response->bar["title"]["show"] = true;
            $response->bar["title"]["text"] = $name;
            $response->bar["title"]["subtext"] = $this->session->sloganCompany;
            $response->bar["tooltip"]["trigger"] = "item";
            $response->bar["tooltip"]["axisPointer"]["type"] = "shadow";
            $response->bar["toolbox"]["show"] = true;
            $response->bar["toolbox"]["feature"]["mark"]["show"] = true;
            $response->bar["toolbox"]["feature"]["restore"]["show"] = true;
            $response->bar["toolbox"]["feature"]["magicType"]["show"] = false;
            $response->bar["toolbox"]["feature"]["magicType"]["type"] = ['bar' , 'line'];
            $response->bar["toolbox"]["feature"]["saveAsImage"]["show"] = true;
            $response->bar["legend"]["data"] = [];
            $response->bar["legend"]["left"] = "center";
            $response->bar["legend"]["bottom"] = "1%";
            if($mode == "vertical"){
                $response->bar["grid"]["left"] = "3%";
                $response->bar["grid"]["right"] = "4%";
                $response->bar["grid"]["bottom"] = "15%";
                $response->bar["grid"]["containLabel"] = true;
                $response->bar["xAxis"] = ["type" => "category" , "data" => [""]];
                $response->bar["yAxis"] = ["type" => "value"];
            }else if($mode == "horizontal"){
                $response->bar["grid"]["left"] = "5%";
                $response->bar["grid"]["right"] = "1%";
                $response->bar["grid"]["bottom"] = "1%";
                $response->bar["grid"]["containLabel"] = true;
                $response->bar["yAxis"] = ["type" => "category" , "data" => [] , "axisLabel" => ["margin" => 15]];
                $response->bar["xAxis"] = ["type" => "value"];
            }
            $response->bar["series"] = [];
            $count = 0;
            foreach ($data as $key => $value) {
                $value = (object)$value;
                if($mode == "horizontal"){
                    // $response->bar["legend"]["data"][] = $value->label;
                    $response->bar["yAxis"]["data"][] = $value->label;
                    $response->bar["series"][$count] = [
                        "name" => $value->label,
                        "type" => "bar",
                        "data" => [],
                        "stack" => "Respuestas",
                        "label" => [
                            "normal" => ["show" => true , "position" => "insideRight"],
                            "emphasis" => ["show" => true , "position" => "insideRight"],
                        ]
                    ];
                    $response->bar["series"][$count]["data"] = (array)array_pad($response->bar["series"][$count]["data"] ,count($data) , "");
                    $response->bar["series"][$count]["data"][$count] = (int)$value->value;
                }else{
                    $response->bar["legend"]["data"][] = $value->label;
                    $response->bar["series"][$count] = [
                        "name" => $value->label,
                        "type" => "bar",
                        "data" => [(int)$value->value],
                        "label" => [
                            "normal" => ["show" => true , "position" => "top"],
                            "emphasis" => ["show" => true , "position" => "top"],
                        ]
                    ];
                }
                $count++;
            }
            //     series : [
            //         {
            //             name:'Test 1',
            //             type:'bar',
            //             data:[862, 1018, 964, 1026, 1679, 1600, 1570],
            //             markLine : {
            //                 lineStyle: {
            //                     normal: {
            //                         type: 'dashed'
            //                     }
            //                 },
            //                 data : [
            //                     [{type : 'min'}, {type : 'max'}]
            //                 ]
            //             }
            //         },
        }

        return $response->bar;
    }
}