<style>
    .pointer {
        cursor : pointer;
    }
    .main-row {
        border: 2px solid #d6d6d6;
        background-color: #dfdfdf87;
    }
    .chart {
        height : 400px;
    }
    select.input-sm {
        color : black;
    }
    td,th {
        text-align : center;
    }
</style>

<?php 
    $session = Session::getInstance();
?>

<div ng-controller="controller">
    <h3 class="page-title"> 
        Reporte Comparativo
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Labores</a>
                <i class="fa fa-angle-right"></i>
             </li>
        </ul>
        <div class="page-toolbar">
            <ng-calendarapp search="search"></ng-calendarapp>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Comparación por finca y labor</span>
                    </div>
                    <div class="tools">
                        
                    </div>
                </div>
                <div class="portlet-body text-right">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" style="padding-right: 10px">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>FINCA</th>
                                            <th ng-repeat="labor in tabla_por_labor.labores">{{labor.nombre}}</th>
                                            <th>PROM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat-start="row in tabla_por_labor.tabla_por_labor" class="pointer main-row" ng-click="row.expanded = !row.expanded">
                                            <td>{{ row.finca }}</td>
                                            <td ng-repeat="labor in tabla_por_labor.labores">{{ row['labor_'+labor.idLabor] = (row.lotes | avgOfValue : 'labor_'+labor.idLabor) | number : 2 }} %</td>
                                            <td>{{ row.promedio = (row.lotes | avgOfValue : 'promedio' | number : 2) }} %</td>
                                        </tr>
                                        <tr ng-repeat-end ng-repeat="lote in row.lotes" ng-show="row.expanded">
                                            <td>{{ lote.zona }}</td>
                                            <td ng-repeat="labor in tabla_por_labor.labores">{{ lote['labor_'+labor.idLabor] | number : 2 }} %</td>
                                            <td>{{ lote.promedio = (lote.valores | avg) | number : 2 }} %</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Comparación por finca y causa</span>
                    </div>
                    <div class="actions">
                        <select class="form-control" ng-model="filters.tablaPorCausa.labor" ng-change="tablaPorCausa()">
                            <option value="{{labor.idLabor}}" ng-repeat="labor in tabla_por_labor.labores" ng-selected="labor.idLabor == filters.tablaPorCausa.labor">{{labor.nombre}}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body text-right" id="tablaPorCausa">
                    <div class="table-responsive" style="padding-right : 10">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>FINCA</th>
                                    <th ng-repeat="causa in tabla_por_causa.causas">{{causa.nombre}}</th>
                                    <th>PROM</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat-start="row in tabla_por_causa.data" class="pointer main-row" ng-click="row.expanded = !row.expanded">
                                    <td>{{ row.finca }}</td>
                                    <td ng-repeat="causa in tabla_por_causa.causas">{{ row['causa_'+causa.idLaborCausa] = (row.lotes | avgOfValue: 'causa_'+causa.idLaborCausa | number : 2) }} %</td>
                                    <td>{{ row.promedio = (row.lotes | avgOfValue : 'promedio' | number : 2) }} %</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="lote in row.lotes" ng-show="row.expanded">
                                    <td>{{ lote.zona }}</td>
                                    <td ng-repeat="causa in tabla_por_causa.causas">{{ lote['causa_'+causa.idLaborCausa] }} %</td>
                                    <td>{{ lote.promedio = (lote.valores | avg) | number : 2 }} %</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>  

    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">% Calidad por Labor - <small>{{ labor }}</small></span>
                    </div>
                    <div class="actions">
                        <!--<select class="input-sm" ng-model="filters.calidadLabor.labor" ng-change="getGraficaPorLabor(tabla_por_labor.fincas, tabla_por_labor.tabla_por_labor)">
                            <option value="">PROM</option>
                            <option value="{{labor.idLabor}}" ng-repeat="labor in tabla_por_labor.labores" ng-selected="labor.idLabor == filters.calidadLabor.labor">{{labor.nombre}}</option>
                        </select>-->
                    </div>
                </div>
                <div class="portlet-body" id="calidadLabor">
                    <div id="chart-calidad-by-labor" class="chart"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">% Calidad por Causa</span>
                    </div>
                    <div class="actions">
                        <select class="input-sm" ng-model="filters.calidadCausa.causa" ng-change="getGraficaPorCausa(tabla_por_causa.fincas, tabla_por_causa.data)">
                            <option value="">PROM</option>
                            <option value="{{ causa.idLaborCausa }}" ng-repeat="causa in tabla_por_causa.causas">{{ causa.nombre }}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body" id="calidadCausa">
                    <div id="chart-calidad-by-causa" class="chart"></div>
                </div>
            </div>
        </div>
    </div>  
</div>