<style>
    .pointer {
        cursor : pointer;
    }
    .main-row {
        border: 2px solid #d6d6d6;
        background-color: #dfdfdf87;
    }
    .chart {
        height : 400px;
    }
    select.input-sm {
        color : black;
    }

    .container {
        padding: 2px 16px;
    }

    #columns {
        column-width: 320px;
        column-gap: 15px;
        width: 90%;
        margin: 50px auto;
    }

    div#columns figure {
        background: #fefefe;
        border: 2px solid #fcfcfc;
        box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
        margin: 0 2px 15px;
        padding: 15px;
        padding-bottom: 10px;
        transition: opacity .4s ease-in-out;
        display: inline-block;
        column-break-inside: avoid;
    }

    div#columns figure img {
        width: 100%; height: auto;
        border-bottom: 1px solid #ccc;
        padding-bottom: 15px;
        margin-bottom: 5px;
    }

    div#columns figure figcaption {
        font-size: 1rem;
        color: #444;
        line-height: 1.5;
    }

    div#columns small { 
        font-size: 1rem;
        float: right; 
        text-transform: uppercase;
        color: #aaa;
    } 

    div#columns small a { 
        color: #666; 
        text-decoration: none; 
        transition: .4s color;
    }

    @media screen and (max-width: 750px) { 
        #columns { column-gap: 0px; }
        #columns figure { width: 100%; }
    }

    .highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
    .input-bar {
        height : 36px !important;
    }
    .toggler {
        background-color: #3f4d56 !important;
        color : 'white' !important;
    }
</style>

<div ng-controller="controller">
    <h3 class="page-title"> 
        Reporte Día

        <div class="theme-panel">
            <div class="toggler" style="display: block;" ng-click="openMenu()">
                <i class="fa fa-cogs"></i>
            </div>
            <div class="toggler-close" style="display: none;" ng-click="closeMenu()">
                <i class="fa fa-times"></i>
            </div>
            <div class="theme-options" style="display: none;">
                <div class="theme-option">
                    <span> Calidad Min </span>
                    <input class="layout-style-option form-control input-sm" type="number" ng-model="umbral.min" />
                </div>
                <div class="theme-option">
                    <span> Calidad Max </span>
                    <input class="layout-style-option form-control input-sm" type="number" ng-model="umbral.max" />
                </div>
                <div class="theme-option">
                    <button class="btn btn-success" ng-click="saveCalidadUmbral(); closeMenu()">
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Labores</a>
                <i class="fa fa-angle-right"></i>
             </li>
        </ul>
        <div class="page-toolbar">
            <select class="input-sm input-bar" ng-model="filters.time_mode">
                <option value="Diario">Diario</option>
                <option value="Semanal">Semanal</option>
                <option value="Periodal">Periodal</option>
            </select>
            <input ng-show="filters.time_mode == 'Diario'" id="datepicker" class="input-sm input-bar" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly value="{{ filters.fecha_inicial }}">
            <select ng-show="filters.time_mode == 'Periodal'" class="input-sm input-bar" ng-model="filters.periodo" ng-change="changePeriodo()">
                <option value="">Periodo</option>
                <option value="{{p}}" ng-repeat="p in periodos">{{p}}</option>
            </select>
            <select ng-show="filters.time_mode == 'Semanal'" class="input-sm input-bar" ng-model="filters.periodo" ng-change="changeSemana()">
                <option value="">Semana</option>
                <option value="{{p}}" ng-repeat="p in semanas">{{p}}</option>
            </select>
        </div>
    </div>

    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li class="{{ !filters.id_finca ? 'active' : '' }}">
                <a ng-click="filters.id_finca = 0; index()">TODOS</a>
            </li>
            <li ng-repeat="finca in fincas" class="{{ filters.id_finca == finca.id ? 'active' : '' }}">
                <a ng-click="filters.id_finca = finca.id; index()">{{finca.nombre}}</a>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Mapa. Geolocalización</span>
                    </div>
                    <div class="actions">
                        <button class="btn btn-primary" title="Restablecer mapa" ng-click="refrestablecerMapa()">
                            <i class="fa fa-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-2">
                            <select class="form-control" ng-model="filters.markers.periodo" ng-change="changeMap()">
                                <option value="">Periodo</option>
                                <option value="{{p}}" ng-repeat="p in markers.periodos" ng-selected="p == filters.markers.periodo">{{p}}</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" ng-model="filters.markers.lote" ng-change="changeMap()">
                                <option value="">Lote</option>
                                <option value="{{p.id_lote}}" ng-repeat="p in markers.lotes" ng-selected="p.id_lote == filters.markers.lote">{{p.nombre}}</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" ng-model="filters.markers.labor" ng-change="changeMap()">
                                <option value="">Labor</option>
                                <option value="{{p.id_labor}}" ng-repeat="p in markers.labores" ng-selected="p.id_labor == filters.markers.labor">{{p.nombre}}</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <input type="number" class="form-control" placeholder="% Cal prom mayor a:" ng-model="filters.markers.above" ng-change="changesMarkers = true" step="0.1" min="0" max="100">
                        </div>
                        <div class="col-md-2">
                            <input type="number" class="form-control" placeholder="% Cal prom menor a:" ng-model="filters.markers.bellow" ng-change="changesMarkers = true" step="0.1" min="0" max="100">
                        </div>
                        <div class="col-md-2">
                            <span 
                                ng-show="filters.markers.above || filters.markers.bellow" 
                                class="badge badge-pill badge-info"
                                title="{{ filters.markers.above ? 'Calidad promedio por encima de '+filters.markers.above : '' }} {{ filters.markers.above && filters.markers.bellow ? 'y':'' }} {{ filters.markers.bellow ? 'Calidad promedio por debajo de '+filters.markers.bellow : '' }}"
                            >
                                <i class="fa fa-info"></i>
                            </span>
                            <button class="btn btn-primary" ng-show="changesMarkers == true" ng-click="changesMarkers = false; reRenderMarkers()">Aplicar</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="map-container">
                            <div id="map" style="height: 600px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  

    <div class="row">
        <div class="col-md-7">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Tabla Resumen</span>
                    </div>
                    <div class="actions">
                        <button class="btn green-dark" style="border : 0.4px solid black;" ng-click="excel('resumen')">
                            Excel
                        </button>
                    </div>
                </div>
                <div class="portlet-body text-right">
                    <div class="table-responsive" style="padding-right : 5px">
                        <table class="table" id="resumen">
                            <thead>
                                <tr>
                                    <th>LABOR</th>
                                    <th ng-repeat="lote in lotes" class="text-right">{{lote.lote}}</th>
                                    <th class="text-right">P</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat-start="row in tabla_resumen" ng-click="row.expanded = !row.expanded" class="pointer main-row">
                                    <td>{{ row.nombre }}</td>
                                    <td ng-repeat="lote in lotes" class="{{getUmbralFont(row['lote_'+lote.idLote])}} text-right">
                                        <b>{{ row['lote_'+lote.idLote] | number : 2 }} %</b>
                                    </td>
                                    <td class="{{getUmbralFont(row.promedio)}} text-right">
                                        <b>{{ row.promedio = ((row.values | avg) || 0) | number : 2 }} %</b>
                                    </td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="subrow in row.detalle" ng-show="row.expanded">
                                    <td>{{ subrow.causa }}</td>
                                    <td ng-repeat="lote in lotes" class="text-right">
                                        {{ subrow['lote_'+lote.idLote] | number : 2 }} %
                                    </td>
                                    <td class="text-right">{{ subrow.promedio = ((subrow.values | avg) || 0) | number : 2 }} %</td>
                                </tr>
                                <tr>
                                    <th>RESULTADO FINAL</th>
                                    <th ng-repeat="lote in lotes" class="text-right">
                                        {{ tabla_resumen | sumOfValue : 'lote_'+lote.idLote | number : 2 }} %
                                    </th>
                                    <th></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Causas</span>
                    </div>
                    <div class="actions">
                        <select class="input-sm" ng-change="refreshPieCausas()" ng-model="filters.pieCausa.lote">
                            <option value="">Lote</option>
                            <option value="{{ l.idLote }}" ng-repeat="l in pieCausa.lotes" ng-selected="l.idLote == filters.pieCausa.lote">{{ l.nombre }}</option>
                        </select>
                        <select class="input-sm" ng-change="refreshPieCausas()" ng-model="filters.pieCausa.labor">
                            <option value="{{ l.id_labor }}" ng-repeat="l in pieCausa.labores" ng-selected="l.id_labor == filters.pieCausa.labor">{{ l.nombre }}</option>
                        </select>
                        <button class="btn btn-primary btn-sm" ng-click="clearPieCausa()" title="Restablecer filtros">
                            <i class="fa fa-refresh"></i>
                        </button>
                    </div>
                </div>
                <div class="portlet-body" id="pie-causas-container">
                    <div id="pie-causas" class="chart"></div>
                </div>
            </div>
        </div>
    </div> 

    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">% Calidad por Labor</span>
                    </div>
                    <div class="actions">
                        <select class="input-sm" ng-model="filters.calidadLabor.lote" ng-change="refreshCalidadLabor()">
                            <option ng-repeat="lote in calidadLabor.lotes" ng-selected="filters.calidadLabor.lote == lote.id_lote" value="{{ lote.id_lote }}">{{ lote.nombre }}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body" id="calidadLabor">
                    <div id="chart-calidad-by-labor" class="chart"></div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">% Calidad por Lote</span>
                    </div>
                    <div class="actions">
                        <select class="input-sm" ng-model="filters.calidadLote.labor" ng-change="refreshCalidadLote()">
                            <option ng-repeat="labor in calidadLote.labores" ng-selected="filters.calidadLote.labor == labor.id_labor" value="{{ labor.id_labor }}">{{ labor.nombre }}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body" id="calidadLote">
                    <div id="chart-calidad-by-lote" class="chart"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">ANALISIS DE MUESTREO (CANTIDAD DE PLANTAS)</span>
                    </div>
                    <div class="actions">
                        <button class="btn green-dark" style="border : 0.4px solid black;" ng-click="excel('lineas_plantas')">
                            Excel
                        </button>
                    </div>
                </div>
                <div class="portlet-body" id="calidadLabor">
                    <div class="table-responsive" style="padding-right : 5px">
                        <table class="table" id="lineas_plantas">
                            <thead>
                                <tr>
                                    <th>LOTE</th>
                                    <th class="text-right" ng-repeat="linea in lineas_plantas.lineas">{{linea}}</th>
                                    <th class="text-right">TOTAL</th>
                                    <th class="text-right">%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="main-row" ng-repeat="row in lineas_plantas.lotes">
                                    <td>{{row.nombre}}</td>
                                    <td class="text-right" ng-repeat="linea in lineas_plantas.lineas">{{ row['linea_'+linea] }}</td>
                                    <td class="text-right">{{ row.total = (row.values | sum) | number }}</td>
                                    <td class="text-right">{{ row.total/total.total*100 | number : 2 }}%</td>
                                </tr>
                                <tr>
                                    <th>TOTAL</th>
                                    <td class="text-right" ng-repeat="linea in lineas_plantas.lineas">{{ total[linea] = (lineas_plantas.lotes | sumOfValue : 'linea_'+linea) }}</td>
                                    <td class="text-right">{{ total.total = (lineas_plantas.lotes | sumOfValue : 'total') }}</td>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>%</th>
                                    <td class="text-right" ng-repeat="linea in lineas_plantas.lineas">{{ total[linea]/total.total*100 | number : 2 }}%</td>
                                    <td></td>
                                    <th></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Fotos</span>
                    </div>
                    <div class="actions">
                        
                    </div>
                </div>
                <div class="portlet-body" id="calidadLabor">

                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-tabs">
                            <li ng-class="{ 'active' : !filters.fotos.labor }">
                                <a ng-click="deleteFilter('labor')">LABORES</a>
                            </li>
                            <li ng-repeat="labor in (fotos.data | filter : { linea : filters.fotos.linea, lote : filters.fotos.lote } | getNotRepeat : 'labor')" class="{{ filters.fotos.labor == labor ? 'active' : '' }}">
                                <a ng-click="filters.fotos.labor = labor;">{{ labor }}</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-tabs">
                            <li ng-class="{ 'active' : !filters.fotos.lote }">
                                <a ng-click="deleteFilter('lote')">LOTES</a>
                            </li>
                            <li ng-repeat="lote in (fotos.data | filter : { linea : filters.fotos.linea, labor : filters.fotos.labor } | getNotRepeat : 'lote')" class="{{ filters.fotos.lote == lote ? 'active' : '' }}">
                                <a ng-click="filters.fotos.lote = lote;">{{ lote }}</a>
                            </li>
                        </ul>
                    </div>

                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-tabs">
                            <li ng-class="{ 'active' : !filters.fotos.linea }">
                                <a ng-click="deleteFilter('linea')">LINEA</a>
                            </li>
                            <li ng-repeat="linea in (fotos.data | filter : { lote : filters.fotos.lote, labor : filters.fotos.labor } | getNotRepeat : 'linea')" class="{{ filters.fotos.linea == linea ? 'active' : '' }}">
                                <a ng-click="filters.fotos.linea = linea;">{{ linea }}</a>
                            </li>
                        </ul>
                    </div>

                    <div ng-show="fotos.data.length > 0" class="row" id="columns">
                        <figure ng-repeat="image in fotos.data | filter : filters.fotos">
                            <a href="{{image.url}}" target="_black"><img src="{{ image.url }}" alt="Imagen no disponible"></a>
                            <figcaption>
                                <b>Labor : {{ image.labor }}</b>
                                <br>
                                Lote : {{ image.lote }}
                                <br>
                                Linea : {{ image.linea }}
                            </figcaption>
                        </figure>
                    </div>

                    <h3 ng-if="fotos.data.length == 0">NO HAY IMAGENES</h3>
                </div>
            </div>
        </div>
    </div>
</div>