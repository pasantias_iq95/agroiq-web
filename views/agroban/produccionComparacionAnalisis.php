<style>
	th,td{
		text-align: center !important;
	}
    .border-bottom-red {
        border-bottom : 2px solid red !important;
    }
    .border-bottom-green {
        border-bottom : 2px solid green !important;
    }
    .text-red {
        color : red !important;
        font-weight: bold !important;
    }
    .text-green {
        color : green !important;
        font-weight: bold !important;
    }
    .form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
    .text-left {
        text-align : left !important;
    }
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
        Benchmark
     </h3>
     <div class="page-bar"
             <li>
                 <i class="icon-home"></i>
                 <a>Benchmark</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label style="margin-top: 7px; margin-right: 10px;">
                Año : 2018
            </label>
        </div>
    </div>
	<div id="contenedor" class="div2">

        <div class="row">
            <div class="col-md-6">
                <div class="portlet box green-haze" id="porHectareas">
                    <div class="portlet-title">
                        <span class="caption">Por Hectáreas</span>
                        <div class="tools">
                            <select class="input-sm" ng-model="filters.por_hectareas" style="color: black" ng-change="getPorHectareas()">
                                <option value="HA/SEM">Ha/Sem</option>
                                <option value="HA/ANIO">Ha/Año</option>
                            </select>
                        </div>
                    </div>
					<div class="portlet-body">
						<div class="table-responsive">
                            <table class="table table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th>INDICADORES</th>
                                        <th>AGROBAN</th>
                                        <th>LOS RIOS NORTE</th>
                                        <th>EL ORO</th>
                                        <th>GUAYAS</th>
                                        <th>LOS RIOS SUR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in por_hectareas">
                                        <td class="text-left">{{ row.name }}</td>
                                        <td>{{ row.agroban }}</td>
                                        <td class="{{ getUmbral(row.agroban, row.quintana, row.name) }}">{{ row.quintana }}</td>
                                        <td class="{{ getUmbral(row.agroban, row.laniado, row.name) }}">{{ row.laniado }}</td>
                                        <td class="{{ getUmbral(row.agroban, row.marun, row.name) }}">{{ row.marun }}</td>
                                        <td class="{{ getUmbral(row.agroban, row.reiset, row.name) }}">{{ row.reiset }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
					</div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="portlet box green-haze" id="porHectareas">
                    <div class="portlet-title">
                        <span class="caption"></span>
                        <div class="tools">
                            <select class="input-sm" ng-model="filters.chart_por_hectareas" style="color: black" ng-change="chartPorHectareas()">
                                <option value="{{row.name}}" ng-repeat="row in por_hectareas" ng-selected="row.name == filters.chart_por_hectareas">{{row.name}}</option>
                            </select>
                        </div>
                    </div>
					<div class="portlet-body">
                        <div id="chart-porHeactareas" style="height:400px">
                        
                        </div>
					</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="portlet box green-haze" id="semanal">
                    <div class="portlet-title">
                        <span class="caption">Por Semana</span>
                        <div class="tools">
                            <select class="input-sm" ng-model="filters.por_semana" style="color: black" ng-change="getSemanal()">
                                <option value="">Todos</option>
                                <option value="{{sem}}" ng-repeat="sem in semanas">{{sem}}</option>
                            </select>
                        </div>
                    </div>
					<div class="portlet-body">
                        <div class="table-responsive">
                            <table class="table table-condensed table-bordered">
                                <thead>
                                    <tr>
                                        <th>INDICADORES</th>
                                        <th>AGROBAN</th>
                                        <th>LOS RIOS NORTE</th>
                                        <th>EL ORO</th>
                                        <th>GUAYAS</th>
                                        <th>LOS RIOS SUR</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in semanal">
                                        <td class="text-left">{{ row.name }}</td>
                                        <td>{{ row.agroban }}</td>
                                        <td class="{{ getUmbralFont(row.agroban, row.quintana, row.name) }}">{{ row.quintana }}</td>
                                        <td class="{{ getUmbralFont(row.agroban, row.laniado, row.name) }}">{{ row.laniado }}</td>
                                        <td class="{{ getUmbralFont(row.agroban, row.marun, row.name) }}">{{ row.marun }}</td>
                                        <td class="{{ getUmbralFont(row.agroban, row.reiset, row.name) }}">{{ row.reiset }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
					</div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="portlet box green-haze" id="porHectareas">
                    <div class="portlet-title">
                        <span class="caption"></span>
                        <div class="tools">
                            <select class="input-sm" ng-model="filters.chart_semanal" style="color: black" ng-change="chartSemanal()">
                                <option value="{{row.name}}" ng-repeat="row in semanal" ng-selected="row.name == filters.chart_semanal">{{row.name}}</option>
                            </select>
                        </div>
                    </div>
					<div class="portlet-body">
                        <div id="chart-semanal" style="height:400px">
                        
                        </div>
					</div>
                </div>
            </div>
        </div>

	</div>
</div>