
<style>
    .no-padding {
        padding : 0px !important;
    }
    .no-margin {
        margin : 0px !important;
    }
    #listado {
        min-height: 560px;
		max-height: 560px;
        overflow-y : scroll;
        overflow-x: hide;
        cursor: pointer;
    }
	#map-polygon {
		min-height: 600px;
		max-height: 600px;
		min-width: 100%;
		max-width: 100%;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	.center-th {
		text-align: center !important;
	}
    .right-th {
        text-align: right !important;
    }
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
    table td, th {
        text-align: center;
    }
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.portlet.box .dataTables_wrapper .dt-buttons{
        margin-top: 0px !important; 
        padding-bottom: 5px !important;
    }
    .fixedHeader-floating{top:60px!important;}
    .chart-container {
        height: 400px;
    }
    .animate-show-hide.ng-hide {
        opacity: 0;
    }

    .animate-show-hide.ng-hide-add,
    .animate-show-hide.ng-hide-remove {
        transition: all linear 0.5s;
    }

    .check-element {
        border: 1px solid black;
        opacity: 1;
        padding: 10px;
    }

    .buttons{
        float: right;
        padding: 5px;
    }
</style>
<div ng-controller="lancofruit">
	<h3 class="page-title"> 
          Lancofruit
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Revisión Lancofruit</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="tools">
            <div class="buttons">
                <button class="btn red" ng-show="polygonArray.length > 0" id="last-point">Borrar ultimo punto</button>
                <button class="btn green" id="new-draw">Crear nueva cerca</button>
                <button class="btn blue" id="save-draw">Guardar la cerca</button>
            </div>
         </div>
    </div>
    <div class="row" ng-init="init()">
        <div class="col-md-12">
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i> Mapa </div>
                    <div class="tools">
                        <a href="" class="collapse" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <form id="form-datos" role="form" method="post" class="form-horizontal form-row-seperated no-margin">
                                        <div>
                                            <div class="form-group no-padding" style="border-bottom: none;">
                                                <!--<label class="col-md-4 control-label" for="name">Buscar <span class="required" aria-required="true"> * </span></label>-->
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" ng-model="filters.search" id="search" placeholder="Buscar....">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <ul class="list-group" id="listado">
                                    <li class="list-group-item"  ng-repeat="polygon in polygons | filter : {name : filters.search}" ng-click="showPoly(polygon)">  {{polygon.name}} </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="panel" style="padding:8px;margin:0">
                                <label for="nombre">Nombre de la Cerca : 
                                    <input type="text" id="nombre" name="nombre" ng-model="nombre">
                                </label>
                            </div>
                            <div id="map-polygon" style="padding: 0px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>