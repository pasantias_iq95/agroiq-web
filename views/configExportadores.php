<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	/*table > th,td{
		text-align: center !important;
	}*/
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.portlet.box .dataTables_wrapper .dt-buttons{
        margin-top: 0px !important; 
        padding-bottom: 5px !important;
    }
    .fixedHeader-floating{top:60px!important;}
</style>
<div>
	<h3 class="page-title"> 
          Exportadores
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Exportadores</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <!-- <ng-calendarapp  search="search"></ng-calendarapp> -->
        </div>
    </div>
	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Exportadores</span>
		    <div class="tools">
                <div class="btn-group">
                    <a id="addData" href="editExportador" class="btn blue"> Agregar </a>
                </div>
	        </div>
		</div>
		<div class="portlet-body">
			<div class="table-container">
                <div class="table-actions-wrapper">
                </div>
				<table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer" id="datatable_ajax">
                    <thead>
                        <tr >
                            <th class="center-th" width="1%"></th>
                            <th class="center-th" width="5%"> ID</th>
                            <th class="center-th" width="10%"> Cliente </th>
                            <th class="center-th" width="10%"> Exportador </th>
                            <th class="center-th" width="10%"> Estado </th>
                            <th class="center-th" width="5%"> Acciones </th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td><input type="text" class="form-control form-filter input-sm" name="id" id="id"> </td>
                            <td><input type="text" class="form-control form-filter input-sm" name="cliente" id="cliente"> </td>
                            <td><input type="text" class="form-control form-filter input-sm" name="exportador" id="exportador"> </td>
                            <td>
                                <select name="status" class="form-control form-filter input-sm" id="status">
                                    <option value="">TODOS</option>
                                    <option value="Activo">Activo</option>
                                    <option value="Inactivo">Inactivo</option>
                                </select>
                            </td>
                            <td>
                                <div class="margin-bottom-5">
                                    <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                        <i class="fa fa-search"></i> </button>
                                </div>
                                <button class="btn btn-sm red btn-outline filter-cancel" >
                                    <i class="fa fa-times"></i> </button>
                            </td>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>
			</div>
		</div>
	</div>
</div>