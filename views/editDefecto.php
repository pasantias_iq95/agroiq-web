        

<div ng-controller="control" ng-cloak>
     <h3 class="page-title"> 
          <?= isset($_GET["id"]) ? "Editar" : "Agregar" ?> Defecto
     </h3>
     <div class="page-bar" ng-init="nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="/configCategorias">Listado de Defectos</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
    </div>
    <form id="formularioOrden"  role="form" method="post" class="form-horizontal form-row-seperated">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <?= isset($_GET["id"]) ? "Editar" : "Agregar" ?> Defecto </div>
                <div class="actions btn-set">
                    <button type="button" class="btn blue" ng-click="saveDatos()">
                        <i class="fa fa-check"></i> Guardar</button>
                </div>
            </div>
            <div class="form-body portlet-body">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Información</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Defecto :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" ng-model="data.nombre" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Categoría :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" ng-model="data.categoria">
                                        <option ng-repeat="(key, value) in categorias" value="{{key}}" ng-selected="key == data.id_categoria">{{value}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Valor :
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" ng-model="data.valor" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Siglas :
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" ng-model="data.siglas" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>