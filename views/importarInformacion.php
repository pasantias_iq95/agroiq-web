<?php
    $session = Session::getInstance();
?>
    <script src="http://hayageek.github.io/jQuery-Upload-File/4.0.10/jquery.uploadfile.min.js"></script>
    <link href="http://hayageek.github.io/jQuery-Upload-File/4.0.10/uploadfile.css" rel="stylesheet">

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row" ng-controller="importarInformacion">
        <div class="col-md-12">
            <div class="portlet box blue hide" id="subir_fotos">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-gift"></i>SUBIENDO ARCHIVOS ...
                    </div>
                    <div class="tools">
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row" ng-repeat="row in xmls">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <label for="">{{ row }}</label> 
                                <i id="no_{{row}}" class="fa fa-times font-red-thunderbird"></i>
                                <i id="si_{{row}}" class="hide fa fa-check font-green-jungle"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="tab-pane" id="tab_1">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>HACER UNA NUEVA IMPORTACION
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- class="horizontal-form" id="form" method="post" action="../excel/guardar_archivos.php" enctype="multipart/form-data" -->
                        <form>
                            <div class="form-body">
                                <h3 class="form-section">SELECCIONAR ARCHIVO XML</h3>
                                <div class="row">
                                    <div class="col-md-6">
                                        <input id="usuario" name="usuario" type="text" value="<?= $session->id ?>" class="hide"/>
                                        <input id="xmls" name="xmls" type="file" fileread="fileread" accept="text/xml" multiple/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions right">
                                <div id="editor"></div>
                                <button id="cancel" type="button" class="btn default cancel">Cancelar</button>
                                <button id="subir" type="button" ng-click="subirXmls()" class="btn blue">subir</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
