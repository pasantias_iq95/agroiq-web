<style>
    .chart {
        height : 400px;
    }
    td, th {
        text-align : center;
    }
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }

    #columns {
        column-width: 320px;
        column-gap: 15px;
        width: 90%;
        margin: 50px auto;
    }

    div#columns figure {
        background: #fefefe;
        border: 2px solid #fcfcfc;
        box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
        margin: 0 2px 15px;
        padding: 15px;
        padding-bottom: 10px;
        transition: opacity .4s ease-in-out;
        display: inline-block;
        column-break-inside: avoid;
    }

    div#columns figure img {
        width: 100%; height: auto;
        border-bottom: 1px solid #ccc;
        padding-bottom: 15px;
        margin-bottom: 5px;
    }

    div#columns figure figcaption {
        font-size: 1rem;
        color: #444;
        line-height: 1.5;
    }

    div#columns small { 
        font-size: 1rem;
        float: right; 
        text-transform: uppercase;
        color: #aaa;
    } 

    div#columns small a { 
        color: #666; 
        text-decoration: none; 
        transition: .4s color;
    }

    @media screen and (max-width: 750px) { 
        #columns { column-gap: 0px; }
        #columns figure { width: 100%; }
    }

    .calendar-table td.available {
		background-color: green !important;
    	color: white !important;
	}
	.icalendar-table td.available:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
    .center-text{
        text-align : center;
        border-radius: 3px; 
        border: 1px solid yellow;
    }.btn{
        margin-left : 2px;
    }
    .scroll-vertical {
        overflow-y: scroll
    }
    .style-select {
        height : 40px;
        color : black !important;
    }
</style>
<div ng-controller="comparativo" ng-cloak id="scope">
    <div style="width: 100%;margin-bottom: 10px;display:table;">
        <h3 class="page-title" style="display: table-cell !important;"> 
            Reporte de Comparación
        </h3>
        <!-- <div class="theme-panel">
            <div class="toggler" style="display: block;" ng-click="openMenu()">
                <i class="fa fa-cogs"></i>
            </div>
            <div class="toggler-close" style="display: none;" ng-click="closeMenu()">
                <i class="fa fa-times"></i>
            </div>
            <div class="theme-options" style="display: none;">
                <div class="theme-option">
                    <small> GAVETAS </small>
                    <br>
                    <span> RUTA 1  </span>
                    <input class="layout-style-option form-control input-sm" type="number" ng-model="umbral_cluster.min" />
                </div>
                <div class="theme-option">
                    <button class="btn btn-success" ng-click="">
                        Guardar
                    </button>
                </div>
            </div>
        </div> -->
    </div>

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Ventas</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            CLIENTE
            <select class="input-sm" ng-model="filters.cliente" ng-change="comparativo()" style="height: 40px;">
                <option value="">TODOS</option>
                <option ng-repeat="cliente in clientes | orderBy : 'cliente'" value="{{cliente}}" >{{ cliente }}</option>
            </select>
            <ng-calendarapp search="search"></ng-calendarapp>
        </div>
    </div>
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>
                <span ng-if="var == 'tabla'"> VENTAS</span>
            </div>
            <select ng-if="var != 'tabla'" ng-change="comparativo()" class="style-select" ng-model="filters.type">
                <option ng-selected="filters.type == 'VENTAS'" value="VENTAS">VENTAS</option>
                <option ng-selected="filters.type == 'GAVETAS'" value="GAVETAS">GAVETAS</option>
            </select>
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tabla_venta_dia" data-toggle="tab" ng-click="var = 'tabla'" aria-expanded="true"> TABLA </a>
                </li>
                <li class="">
                    <a href="#barras" ng-click="renderBar(); var = 'barra'" data-toggle="tab" aria-expanded="false"> BARRAS </a>
                </li>
                <li class="">
                    <a href="#pastel" ng-click="renderPie(); var = 'pastel'" data-toggle="tab" aria-expanded="false"> PASTEL </a>
                </li>
            </ul>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabla_venta_dia">
                            <div class="table-responsive table-scrollable">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:10%">RUTA</th>
                                            <th style="min-width:10%">PUNTOS</th>
                                            <th style="min-width:10%">$ VENTAS</th>
                                            <th style="min-width:10%">GAVETAS</th>
                                            <th style="min-width:10%"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat-start="row in data | filter : filtros" ng-click="row.expanded = !row.expanded">
                                            <td><b>{{ row.ruta }}</b></td>
                                            <td><b>{{ row.puntos }}</b></td>
                                            <td><b>{{row.ventas }}</b></td>
                                            <td><b>{{ row.ventas / 6 }}</b></td>
                                            <td class="text-center"><i class="fa fa-{{ row.expanded ? 'minus' : 'plus' }}"></i></td>
                                        </tr>
                                        <tr ng-repeat-end="row" ng-show="row.expanded" ng-repeat="detalle in row.detalle">
                                            <td>{{ detalle.ruta }}</td>
                                            <td>{{ detalle.puntos }}</td>
                                            <td>{{detalle.ventas }}</td>
                                            <td>{{ detalle.ventas / 6 }}</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th style="min-width:10%">TOTAL</th>
                                            <th style="min-width:10%">{{ ((data | filter : filtros) | sumOfValue : 'puntos') | number : 0 }}</th>
                                            <th style="min-width:10%">{{ ((data | filter : filtros) | sumOfValue : 'ventas') | number : 2 }}</th>
                                            <th style="min-width:10%">{{ ((data | filter : filtros) | sumOfValue : 'ventas') / 6 | number : 2 }}</th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="barras">
                            <div id="line-ventadia" class="chart"></div>
                        </div>
                        <div class="tab-pane" id="pastel">
                            <div id="pastel-ventadia" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>