<style>
    .chart {
        height : 400px;
    }
    td, th {
        text-align : center;
    }
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }

    #columns {
        column-width: 320px;
        column-gap: 15px;
        width: 90%;
        margin: 50px auto;
    }

    div#columns figure {
        background: #fefefe;
        border: 2px solid #fcfcfc;
        box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
        margin: 0 2px 15px;
        padding: 15px;
        padding-bottom: 10px;
        transition: opacity .4s ease-in-out;
        display: inline-block;
        column-break-inside: avoid;
    }

    div#columns figure img {
        width: 100%; height: auto;
        border-bottom: 1px solid #ccc;
        padding-bottom: 15px;
        margin-bottom: 5px;
    }

    div#columns figure figcaption {
        font-size: 1rem;
        color: #444;
        line-height: 1.5;
    }

    div#columns small { 
        font-size: 1rem;
        float: right; 
        text-transform: uppercase;
        color: #aaa;
    } 

    div#columns small a { 
        color: #666; 
        text-decoration: none; 
        transition: .4s color;
    }

    @media screen and (max-width: 750px) { 
        #columns { column-gap: 0px; }
        #columns figure { width: 100%; }
    }

    .calendar-table td.available {
		background-color: green !important;
    	color: white !important;
	}
	.icalendar-table td.available:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
    .center-text{
        text-align : center;
        border-radius: 3px; 
        border: 1px solid yellow;
    }.btn{
        margin-left : 2px;
    }
    .scroll-vertical {
        overflow-y: scroll
    }
</style>
<div ng-controller="tendencia" ng-cloak id="scope">
    <div style="width: 100%;margin-bottom: 10px;display:table;">
        <h3 class="page-title" style="display: table-cell !important;"> 
            Reporte de Tendencia
        </h3>
    </div>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Ventas</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            RUTA
            <select class="input-sm" ng-model="filters.cliente" ng-change="general()" style="height: 40px;">
                <option value="">TODOS</option>
                <option ng-repeat="cliente in clientes | orderBy : 'cliente'" value="{{cliente}}" >{{ cliente }}</option>
            </select>
        </div>
    </div>
    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li class="{{ filters.type == 'VENTAS' ? 'active' : '' }}">
                <a ng-click="filters.type = 'VENTAS'; general()">VENTAS</a>
            </li>
            <li class="{{ filters.type == 'GAVETAS' ? 'active' : '' }}">
                <a ng-click="filters.type = 'GAVETAS'; general()">GAVETAS</a>
            </li>
        </ul>
    </div>
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> VENTAS 
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabla_venta_dia">
                            <div class="portlet-body" id="principal">
                                <div>
                                    <div id="line-ventadia" class="chart"></div>
                                </div>
                                <div class="table table-hover table-bordered" id="tabla-general"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<script src="componentes/FilterableSortableTable.js"></script>