 <?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
<style>
    .pic {
        height : 200px;
        width : 200px;
    }
</style>
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="assets/pages/css/error.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL STYLES -->
<div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
        <h1>Mantenimiento</h1>
    </div>
    <!-- END PAGE TITLE -->
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE BREADCRUMB -->
<ul class="page-breadcrumb breadcrumb">
    <li>
        <a href="/start.php">Inicio</a>
    </li>
</ul>
<!-- END PAGE BREADCRUMB -->
<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12 page-404">
        <div class="col-md-offset-1 col-md-3">
            <img src="img/maintenance.png" class="pic">
        </div>
        <div class="col-md-5">
            <h3>Estamos en mantenimiento</h3>
            <p> Actualmente al modulo que te dirigías se encuentra en mantenimiento, favor de intentar mas tarde.
                <br/>
                <a href="/start.php"> Regresar </a>
            </p>
        </div>
    </div>
</div>