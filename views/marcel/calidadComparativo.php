<style>
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
    .right-th {
		text-align: right !important;
		padding-right: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.alginLeft {
		text-align: left !important;
		padding-top: 20px;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}
	td-hidden {
		padding: 1px;
	}
	
	.subtittleTr {
	    padding: 2px 25px;
	}

	.Titlespan {
		font-size: 21px;
		/* text-align: left; */
		float: left;
	}
    .delete  {
        background-color : #c8d8e8 !important;
    }

	@media print
		{
		body * { visibility: hidden; }
		.div2 * { visibility: visible; }
		.div2 { position: absolute; top: 40px; left: 30px; }
		}
</style>
<script src="../componentes/FilterableSortableTable.js" type="text/javascript"></script>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Calidad
     </h3>
     <div class="page-bar" ng-init="produccion.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="calidad">Calidad</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
        </div>
    </div>
	<div id="contenedor" class="div2">

        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">Importar Información </div>
                <div class="tools">
                    <button class="btn blue" ng-click="subir()">Subir</button>
                </div>
            </div>
            <div class="portlet-body">
                <h3>Excel (.xlsx)</h3>
                <input id="input-file" file="file" type="file" accept=".xlsx">
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">Calidad</div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="comparativo" style="height: 400px;">
                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">Peso</div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="peso" style="height: 400px;">
                                                
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">Cluster</div>
                        <div class="tools">
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="cluster" style="height: 400px;">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">Daños</div>
                        <div class="tools">
                            <select class="input-sm pull-right" style="color: black;" ng-model="filters.defecto" ng-change="init(true)">
                                <option value="{{ key }}" ng-repeat="(key, value) in defectos" ng-selected="key == filters.defecto">{{ value }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="daños" style="height: 400px;">
                    
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    Historico </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel-group accordion" id="accordion3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="tab_comparativo" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> Calidad </a>
                            </h4>
                        </div>
                        <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div id="table-comparativo"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="tab_peso" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> Peso </a>
                            </h4>
                        </div>
                        <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div id="table-peso"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="tab_cluster" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> Cluster </a>
                            </h4>
                        </div>
                        <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div id="table-cluster"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="tab_daños" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Daños </a>
                            </h4>
                        </div>
                        <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div id="table-daños"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>