<style>
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.anchoTable {
		width: 100px;
	}
    .delete  {
        background-color : #c8d8e8 !important;
    }

    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }
</style>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>-->
<script src="componentes/react-photo-gallery.min.js"></script>
<script src="componentes/gallery.js"></script>

<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Calidad Fotos
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="calidad">Calidad</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label>SEMANA </label>
            <select class="input-sm" ng-model="filters.desde" ng-change="getFotos()">
                <option value="{{value.semana}}" ng-repeat="(key, value) in semanas | orderObjectBy : 'semana'" ng-if="menorSemana(value.semana)" ng-selected="value.semana == filters.desde">{{value.semana}}</option>
            </select>
            <label>HASTA </label>
            <select class="input-sm" ng-model="filters.hasta" ng-change="getFotos()">
                <option value="{{value.semana}}" ng-repeat="(key, value) in semanas | orderObjectBy : 'semana'" ng-if="mayorSemana(value.semana)" ng-selected="value.semana == filters.hasta">{{value.semana}}</option>
            </select>
        </div>
    </div>


    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Fotos </div>
            <div class="tools">
                <select class="form-control" ng-model="filters.marca">
                    <option value="">TODAS</option>
                    <option value="{{value}}" ng-repeat="value in marcas">{{value}}</option>
                </select>
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">
                <div class="panel panel-default" ng-repeat="(semana, row) in fotos | orderObjectBy : 'semana'">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_{{row.semana}}_{{$index}}" aria-expanded="false"> 
                                SEMANA {{ row.semana }} 
                                <span class="pull-right" style="margin-right : 10px;"><b>{{row.filtrado.length}} Fotos<b></span>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_3_{{row.semana}}_{{$index}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="col-md-3" ng-repeat="calidad in row.filtrado = (row.data | filter : { marca: filters.marca })">
                                <div class="card">
                                    <img src="{{ calidad.url }}" alt="Imagen no disponible" style="width:100%">
                                    <div class="container">
                                        <h4><b>{{ calidad.marca }}</b></h4> 
                                        <p>Calidad : {{ calidad.calidad_cluster }} %</p> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
