<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.alginLeft {
		text-align: left !important;
		padding-top: 20px;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}
	td-hidden {
		padding: 1px;
	}
	
	.subtittleTr {
	    padding: 2px 25px;
	}

	.Titlespan {
		font-size: 21px;
		/* text-align: left; */
		float: left;
	}
	.td-small td {
		max-width : 60px;
	}

	@media print
		{
		body * { visibility: hidden; }
		.div2 * { visibility: visible; }
		.div2 { position: absolute; top: 40px; left: 30px; }
		}
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Gerencia
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Gerencia</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
			<label for="semana">
                Sem : 
            </label>
            <select name="semana" id="semana" ng-model="params.semana" style="margin: 2px;height: 36px;">
				<option value="">TODAS</option>
                <option ng-repeat="(key, value) in semanas" value="{{key}}" ng-selected="value.sem == params.semana">{{value.sem}}</option>
            </select>
			<ng-calendarapp></ng-calendarapp>
        </div>
    </div>
	<div id="contenedor" class="div2">
		<div class="row">
			<div class="col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">TOTAL PRODUCCION</span>
						<div class="tools">
						</div>
					</div>
					<div class="portlet-body" >
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
										<th class="center-th"></th>
										<th>Finca 1</th>
										<th>Finca 2</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="q in produccion_questions">
										<td>{{ q.name }}</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">PROMEDIOS</span>
						<div class="tools">
						</div>
					</div>
					<div class="portlet-body" >
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_3">
								<thead>
									<tr>
										<th class="center-th"></th>
										<th>Finca 1</th>
										<th>Finca 2</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="q in promedios_questions">
										<td>{{ q.name }}</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

</div>