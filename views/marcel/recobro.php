<style>
	td, th {
        text-align : center;
    }
    .black {
        color : black;
    }
    .no-margin {
        margin: 0px;
    }
    .form-control {
        border-radius : 3px !important;
    }
    .btn {
        border-radius : 3px !important;
    }
    .portlet {
        border-radius : 3px !important;
    }
</style>

<script src="componentes/ReactPapiroRecobro.js"></script>

<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción. Recobro
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Recobro</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar" style="display: flex;">
            <label for="semana" style="margin: auto;">
                Sem : 
            </label>
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-fit-height dropdown-toggle {{colorClass}}" data-toggle="dropdown" data-hover="dropdown" data-delay="2000" data-close-others="true" aria-expanded="false"> 
                    {{ filters.year }} - {{ filters.week }}
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu" style="max-height: 300px; overflow-y: scroll;">
                    <li ng-repeat="sem in semanas" class="{{sem.class}}" ng-click="filters.week = sem.semana; filters.year = sem.anio; init()">
                        <a href="javascript:;" class="{{sem.class}}">
                            {{ sem.anio }} -  {{ sem.semana }}
                        </a>
                    </li>
                </ul>
            </div>
         </div>
    </div>

    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li ng-repeat="(id_finca, nombre) in fincas" class="{{ filters.id_finca == id_finca ? 'active' : '' }}">
                <a ng-click="filters.id_finca = id_finca; init(); getDataTable()">{{nombre}}</a>
            </li>
        </ul>
    </div>

	<div id="contenedor" class="div2">
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">SALDO DE FUNDAS</span>
						<div class="tools">
                            <button class="btn btn-primary" data-toggle="modal" data-target="#caidos-modal" ng-disabled="lotesload">
                                Caidos
                            </button>
						</div>
					</div>
					<div class="portlet-body">
                        <div class="table-responsive" id="saldo_fundas">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th rowspan="2">LOTE</th>
                                        <th rowspan="2">RAC<br>ENF</th>
                                        <th ng-if="edades.length > 0" class="text-center sm" colspan="{{edades.length}}">RACIMOS COSECHADOS POR EDAD (SEM)</th>
                                        <th rowspan="2">TOTAL<br>COSE</th>
                                        <th rowspan="2">SALDO<br>RAC</th>
                                        <th rowspan="2">%</th>
                                        <th rowspan="2">CAIDOS</th>
                                        <th rowspan="2">% CAIDOS</th>
                                        <th rowspan="2">% RECOBRO</th>
                                        <th rowspan="2">NO RECUP</th>
                                    </tr>
                                    <tr>
                                        <th ng-repeat="e in edades | orderObjectBy : 'edad'" class="sm">{{ e.edad }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in data">
                                        <td>{{row.lote}}</td>
                                        <td class="{{ colorClass }}">
                                            {{ row.racimos_enfunde > 0 ? (row.racimos_enfunde | number: 0) : '' }}
                                        </td>
                                        <td ng-if="edades.length > 0" class="sm" ng-repeat="e in edades | orderObjectBy : 'edad'">
                                            {{ row['edad_'+e.edad] > 0 ? row['edad_'+e.edad] : '' }}
                                        </td>
                                        <td>{{row.cosechados}}</td>
                                        <td>{{row.saldo = (row.racimos_enfunde - row.cosechados)}}</td>
                                        <td>{{ row.enfunde }}%</td>
                                        <td>{{ row.caidos ? row.caidos : '' }}</td>
                                        <td>{{ row.porc_caidos ? row.porc_caidos+'%' : '' }}</td>
                                        <td class="{{ getUmbral(row.total) }}">{{row.total = ((row.porc_caidos)+(row.enfunde)) | number: 2}}%</td>
                                        <td>{{ ((100-row.total) /100 ) *row.racimos_enfunde | number: 0}}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th class="{{ colorClass }}">{{ totales.racimos_enfunde | number }}</th>
                                        <th ng-if="edades.length > 0" class="sm" ng-repeat="e in edades | orderObjectBy : 'edad'">
                                            {{ (data | sumOfValue : 'edad_'+e.edad) > 0 ? (data | sumOfValue : 'edad_'+e.edad | number) : '' }}
                                        </th>
                                        <th>{{ totales.cosechados = (data | sumOfValue : 'cosechados') | number }}</th>
                                        <th>{{ totales.saldo = (data | sumOfValue : 'saldo') | number }}</th>
                                        <th>{{ totales.recobro | number : 2 }}%</th>
                                        <th>{{ totales.caidos | number }}</th>
                                        <th>{{ totales.porc_caidos | number : 2 }}%</th>
                                        <th class="{{ getUmbral(totales.total) }}">{{ totales.total = (totales.recobro + totales.porc_caidos) | number: 2 }}%</th>
                                        <th>{{ (100-totales.total)*(totales.racimos_enfunde/100) | number: 0 }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
					</div>
				</div>
            </div>
        </div>
	
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">ENFUNDE - RACIMOS COSECHADOS</span>
						<div class="tools">
                            <select class="input-sm form-control" style="width: 80px" ng-model="filters.lote" ng-change="getDataTable()">
                                <option value="">LOTES</option>
                                <option value="{{lote}}" ng-repeat="lote in lotes" ng-selected="lote == filters.lote">{{ lote }}</option>
                            </select>
						</div>
					</div>
					<div class="portlet-body">
                        <ul class="nav nav-pills">
                            <li role="presentation" class="{{ tabActive == 'recobro' ? 'active' : '' }}">
                                <a ng-click="tabActive = 'recobro'; initReactRecobro()">Recobro</a>
                            </li>
                            <li role="presentation" class="{{ tabActive == 'papiro' ? 'active' : '' }}">
                                <a ng-click="tabActive = 'papiro'; initReactRecobro()">Papiro</a>
                            </li>
                        </ul>
                        <div id="data-table" ng-show="tabActive == 'recobro'"></div>
                        <div id="papiro" ng-show="tabActive == 'papiro'"></div>
					</div>
				</div>
            </div>
            
            <div class="col-md-12 hide">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">PRECALIBRACION - RACIMOS COSECHADOS</span>
						<div class="tools">
						</div>
					</div>
					<div class="portlet-body">
                        <div id="data-table-2"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="modal fade" id="caidos-modal" role="basic" aria-hidden="true">
        <div class="modal-dialog" style="background: white;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Agregar Caidos</h4>
                <div class="row">
                    <div class="alert alert-danger no-margin {{ newCaidos.lote != '' && newCaidos.cinta != '' && newCaidos.cantidad > 0 ? 'hide' : '' }}" role="alert">
                        Faltan campos por llenar
                    </div>
                    <div id="saved" class="alert alert-success no-margin hide" role="alert">
                        Guardado con éxito
                    </div>
                    <button 
                        style="margin-right: 10px; border-radius: 5px"
                        class="btn green-haze pull-right {{ newCaidos.lote != '' && newCaidos.cinta != '' && newCaidos.cantidad > 0 ? '' : 'hide' }}"
                        ng-click="saveCaidos()">
                        Guardar
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="label-control">Lote <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-md-12 {{ newCaidos.lote == '' ? 'has-error' : '' }}">
                                    <select class="form-control" ng-model="newCaidos.lote">
                                        <option value="">Seleccione</option>
                                        <option value="{{l}}" ng-repeat="l in lotes_caidos">{{l}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="label-control">Cinta <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-md-12 {{ newCaidos.cinta == '' ? 'has-error' : '' }}">
                                    <select class="form-control" ng-model="newCaidos.cinta">
                                        <option value="">Seleccione</option>
                                        <option value="{{color}}" class="{{clss}}" ng-repeat="(color, clss) in coloresClass">{{color}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="label-control">Semana Enfunde <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-md-12 {{ newCaidos.enfunde == '' ? 'has-error' : '' }}">
                                    <select class="form-control" ng-model="newCaidos.enfunde">
                                        <option value="">Seleccione</option>
                                        <option value="{{sem.anio}}-{{sem.semana}}" class="{{sem.class}}" ng-repeat="sem in semanas">{{ sem.anio }} - {{ sem.semana }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="label-control">Cantidad <span class="text-danger">*</span></label>
                                </div>
                                <div class="col-md-12 {{ !newCaidos.cantidad > 0 ? 'has-error' : '' }}">
                                    <input type="number" class="form-control" ng-model="newCaidos.cantidad">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>
<script src="componentes/FilterableSortableTable.js"></script>