
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.calidad.value) }}">
                                            <span class="counter_tags" data-value="{{tags.calidad.value}}">0</span>
                                            <small class="font-{{ revision(tags.calidad.value) }}">%</small>
                                        </h3>
                                        <small>CALIDAD PROMEDIO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.calidad.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.calidad.value) }}">
                                            <span class="sr-only">{{tags.calidad.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.calidad.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>-->
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.calidad_dedos.value) }}">
                                            <span class="counter_tags" data-value="{{tags.calidad_dedos.value}}">{{tags.calidad_dedos.value}}</span>
                                            <small class="font-{{ revision(tags.calidad_dedos.value) }}">%</small>
                                        </h3>
                                        <small>CALIDAD DEDOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.calidad_dedos.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.calidad_dedos.value) }}">
                                            <span class="sr-only">{{tags.calidad_dedos.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.calidad_dedos.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.calidad_cluster.value) }}">
                                            <span class="counter_tags" data-value="{{tags.calidad_cluster.value}}">{{tags.calidad_cluster.value}}</span>
                                            <small class="font-{{ revision(tags.calidad_cluster.value) }}">%</small>
                                        </h3>
                                        <small>CALIDAD CLUSTER</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.calidad_cluster.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.calidad_cluster.value) }}">
                                            <span class="sr-only">{{tags.calidad_cluster.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.calidad_cluster.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.calidad_maxima_cluster.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.calidad_maxima_cluster.value}}">{{tags.calidad_maxima_cluster.value}}</span>
                                            <small class="font-{{ revision(tags.calidad_maxima_cluster.value) }}">%</small>
                                        </h3>
                                        <small>CALIDAD MÁXIMA (CLUSTER)</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.calidad_maxima_cluster.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.calidad_maxima_cluster.value) }}">
                                            <span class="sr-only"> {{tags.calidad_maxima_cluster.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">CALIDAD MÁXIMA (DEDOS)</div>
                                        <div class="status-number">{{tags.calidad_maxima_dedos.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.calidad_minima_cluster.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.calidad_minima_cluster.value}}">{{tags.calidad_minima_cluster.value}}</span>
                                            <small class="font-{{ revision(tags.calidad_minima_cluster.value) }}">%</small>
                                        </h3>
                                        <small>CALIDAD MÍNIMA (CLUSTER)</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.calidad_minima_cluster.value}}%;" class="progress-bar progress-bar-success  {{ revision(tags.calidad_minima_cluster.value) }}">
                                            <span class="sr-only">0% grow</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">CALIDAD MÍNIMA (DEDOS)</div>
                                        <div class="status-number">{{tags.calidad_minima_dedos.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>


                    <div class="row">
					<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.desviacion_estandar.value) }}">
                                            <span class="counter_tags" data-value="{{tags.desviacion_estandar.value}}">{{tags.desviacion_estandar.value}}</span>
                                            <small class="font-{{ revision(tags.desviacion_estandar.value) }}"></small>
                                        </h3>
                                        <small>DESVIACIÓN ESTÁNDAR</small>
                                    </div>
                                    <div class="icon">
                                        <img src="assets/icons/variation.svg" width="25" width="14" alt="">
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:{{tags.desviacion_estandar.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.desviacion_estandar.value) }}">
                                            <span class="sr-only">{{tags.desviacion_estandar.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.peso.value) }}">
                                            <span class="counter_tags" data-value="{{tags.peso.value}}">{{tags.peso.value}}</span>
                                            <small class="font-{{ revision(tags.peso.value) }}">kg</small>
                                        </h3>
                                        <small>PESO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fas fa-balance-scale"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.peso.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.peso.value) }}">
                                            <span class="sr-only">{{tags.peso.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cluster.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.cluster.value}}">{{tags.cluster.value}}</span>
                                            <small class="font-{{ revision(tags.cluster.value) }}"></small>
                                        </h3>
                                        <small>#CLÚSTER </small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.cluster.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cluster.value) }}">
                                            <span class="sr-only"> {{tags.cluster.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.dedos_promedio.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.dedos_promedio.value}}">{{tags.dedos_promedio.value}}</span>
                                            <small class="font-{{ revision(tags.dedos_promedio.value) }}"></small>
                                        </h3>
                                        <small>DEDOS PROMEDIO </small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.dedos_promedio.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.dedos_promedio.value) }}">
                                            <span class="sr-only"> {{tags.dedos_promedio.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>