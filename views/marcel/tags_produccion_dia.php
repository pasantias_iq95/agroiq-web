
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-jungle">
                                            <span class="counter_tags" data-value="">{{tags.primera_hora}}</span>
                                            <small class="font-green-jungle"></small>
                                        </h3>
                                        <small>PRIMERA PALANCA DEL DIA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 0%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only"></span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">{{tags.primera_fecha}}</div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-jungle">
                                            <span class="counter_tags" data-value="">{{tags.ultima_hora}}</span>
                                            <small class="font-green-jungle"></small>
                                        </h3>
                                        <small>ULTIMA PALANCA DEL DIA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 0%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only"></span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">{{tags.ultima_fecha}}</div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-jungle">
                                            <span class="counter_tags" data-value="">{{tags.diferencia}}</span>
                                            <small class="font-green-jungle"></small>
                                        </h3>
                                        <small>HORAS DE PRODUCCIÓN</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 0%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only"></span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(totales.total_cosechados) }}">
                                            <span class="counter_tags" data-value="{{totales.total_cosechados}}">{{totales.total_cosechados}}</span>
                                            <small class="font-{{ revision(totales.total_cosechados) }}"></small>
                                        </h3>
                                        <small>COSECHADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{totales.total_cosechados}}%;" class="progress-bar progress-bar-success {{ revision(totales.total_cosechados) }}">
                                            <span class="sr-only">{{totales.total_cosechados}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{totales.total_cosechados}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(totales.total_procesada) }}">
                                            <span class="counter_tags" data-value="{{totales.total_procesada}}">{{totales.total_procesada}}</span>
                                            <small class="font-{{ revision(totales.total_procesada) }}"></small>
                                        </h3>
                                        <small>PROCESADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{totales.total_procesada}}%;" class="progress-bar progress-bar-success {{ revision(totales.recusados) }}">
                                            <span class="sr-only"> {{totales.total_procesada}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(totales.recusados) }}">
                                            <span class="counter_tags" data-value=" {{totales.recusados}}">{{totales.recusados}}</span>
                                            <small class="font-{{ revision(totales.recusados) }}"></small>
                                        </h3>
                                        <small>RECUSADOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{totales.recusados}}%;" class="progress-bar progress-bar-success {{ revision(totales.recusados) }}">
                                            <span class="sr-only"> {{totales.recusados}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{totales.recusados}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(totales.calibracion) }}">
                                            <span class="counter_tags" data-value=" {{totales.calibracion}}">{{totales.calibracion | number : 2}}</span>
                                            <small class="font-{{ revision(totales.calibracion) }}"></small>
                                        </h3>
                                        <small>CALIBRACIÓN 2DA. PROM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{totales.calibracion}}%;" class="progress-bar progress-bar-success {{ revision(totales.calibracion) }}">
                                            <span class="sr-only"> {{totales.calibracion}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">CALIBRACIÓN ULT. PROM</div>
                                        <div class="status-number">{{ totales.calibracion_ultima | number : 2 }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span class="counter_tags" data-value="{{totales.peso}}">{{ (promedios | avgOfValue:'manos') | number : 2 }}</span>
                                            <small></small>
                                        </h3>
                                        <small>MANOS PROM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{(promedios | avgOfValue:'manos') | number : 2}}%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only">{{totales.peso}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{ (promedios | avgOfValue:'manos') | number : 2 }} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision((registros | avgOfValue:'edad')) }}">
                                            <span class="counter_tags" data-value=" {{(registros | avgOfValue:'edad')}}">{{ (registros | avgOfValue:'edad') | number: 2 }}</span>
                                            <small class="font-{{ revision((registros | avgOfValue:'edad')) }}"></small>
                                        </h3>
                                        <small>EDAD PROM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{(registros | avgOfValue:'edad')}}%;" class="progress-bar progress-bar-success {{ revision((registros | avgOfValue:'edad')) }}">
                                            <span class="sr-only">{{(registros | avgOfValue:'edad')}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">{{ (registros | avgOfValue:'edad') | number : 2 }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span class="counter_tags" data-value="{{ tags.kg_proc }}">{{ tags.kg_proc | number: 2 }}</span>
                                            <small></small>
                                        </h3>
                                        <small>PESO KG PROC</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 100%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only">{{ tags.kg_proc }}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">{{ tags.kg_proc | number: 2 }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span class="counter_tags" data-value="{{ tags.kg_recu }}">{{ tags.kg_recu | number: 2 }}</span>
                                            <small></small>
                                        </h3>
                                        <small>PESO KG RECU</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: 100%;" class="progress-bar progress-bar-success">
                                            <span class="sr-only">{{ tags.kg_recu }}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{ tags.kg_recu | number: 2 }}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(totales.peso) }}">
                                            <span class="counter_tags" data-value="{{totales.peso}}">{{totales.peso | number : 2}}</span>
                                            <small class="font-{{ revision(totales.peso) }}"></small>
                                        </h3>
                                        <small>PESO KG PROM</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{totales.peso}}%;" class="progress-bar progress-bar-success {{ revision(totales.peso) }}">
                                            <span class="sr-only">{{totales.peso}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{totales.peso | number: 2}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>