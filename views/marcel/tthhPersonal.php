    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
    </style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="informe_tthh" ng-cloak>
     <h3 class="page-title"> 
          Personal
     </h3>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Personal</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
     </div>
     <?php include("./views/tags_tthh.php");?>           
     <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]" onload="loadExternal()">

     </div>  
</div>