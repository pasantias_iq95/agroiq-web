<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/
    
    
    // $report = $factory->Reportes;
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
    </style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="reportes" ng-cloak>
     <h3 class="page-title"> 
          {{tittles.getTittle()}}
          <small>[{{search.date_select}}]</small>
     </h3>
     <div class="page-bar" ng-init="wizardStep.nocache()">
         <ul class="page-breadcrumb">
            <li ng-click="changeStep(0)" ng-if="id_company > 4 && id_company != 7">
                 <i class="icon-home"></i>
                 <a>Zonas</a>
                 <i class="fa fa-angle-right"></i>
             </li>
            <li ng-click="changeStep(5 , 0 ,0 , 0 , 0 , wizardStep.params.idZonaLabor)" ng-if="id_company > 4 && id_company != 7" ng-show="id_company > 4 && wizardStep.params.idZonaLabor > 0 && id_company != 7">
                 <i class="icon-home"></i>
                 <a>Labores - Zonas</a>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li ng-click="changeStep(1 ,wizardStep.params.idZona)" ng-if="id_company > 3" ng-show="wizardStep.params.idZona > 0 && id_company > 3">
                 <i class="icon-home"></i>
                 <a>Fincas</a>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li ng-click="changeStep(6 , 0 , 0 , 0 , 0 , wizardStep.params.idZonaLabor , wizardStep.params.idFincaLabor)" ng-if="id_company > 3" ng-show="id_company > 3 && wizardStep.params.idFincaLabor > 0 && wizardStep.params.idZonaLabor > 0">
                 <i class="icon-home"></i>
                 <a>Labores - Fincas</a> 
                 <i class="fa fa-angle-right"></i>
             </li>
             <li ng-show="wizardStep.params.idFinca > 0" ng-click="changeStep(2 , wizardStep.params.idZona , wizardStep.params.idFinca)">
                 <i class="icon-home"></i>
                 <a>Lotes</a>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li ng-click="changeStep(7 , 0, 0 , 0 , 0 , wizardStep.params.idZonaLabor , wizardStep.params.idFincaLabor , wizardStep.params.idZonaLabor , wizardStep.params.idLoteLabor)" ng-show="wizardStep.params.idFincaLabor > 0 && wizardStep.params.idZonaLabor > 0 && wizardStep.params.idLoteLabor > 0">
                 <i class="icon-home"></i>
                 <a>Labores - Lotes</a>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li ng-show="wizardStep.params.idLote > 0" ng-click="changeStep(3 , wizardStep.params.idZona , wizardStep.params.idFinca , wizardStep.params.idLote)" >
                 <span><a>Labores</a></span>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li ng-show="wizardStep.params.idLote > 0 && wizardStep.params.idLabor > 0" ng-click="changeStep(4 , wizardStep.params.idZona , wizardStep.params.idFinca , wizardStep.params.idLote , wizardStep.params.idLabor)" >
                 <span><a>Causas</a></span>
             </li>
         </ul>
         <div class="page-toolbar">
            <ng-calendarapp  search="search"></ng-calendarapp>
             <div class="btn-group pull-right" ng-show="id_company == 1">
                <!-- <ng-calendarapp search="search"></ng-calendarapp> -->
                 <button ng-show="id_company == 1" type="button" class="btn btn-fit-height grey-salt dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> Exportar
                     <i class="fa fa-angle-down"></i>
                 </button>
                 <ul class="dropdown-menu pull-right" role="menu">
                     <li>
                         <a>
                             <i class="icon-bell"></i> Imprimir</a>
                     </li>
                     <li>
                         <a ng-click="demoFromHTML()">
                             <i class="icon-shield"></i> PDF</a>
                     </li>
                     <li>
                         <a href="#">
                             <i class="icon-user"></i> Excel</a>
                     </li>
                     <li>
                         <a href="#">
                             <i class="icon-bag"></i> CSV</a>
                     </li>
                     <!-- <li class="divider"> </li> -->
                 </ul>
             </div>
         </div>
     </div>
     <?php include("./views/tags_details.php");?>  
    <h4> 
        {{status_tittle | uppercase}}
    </h4>
     <div id="reportes_all" ng-include="wizardStep.templatePath[wizardStep.step]" onload="loadExternal()">

     </div>  
</div>