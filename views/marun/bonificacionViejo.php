<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.page-bar .page-toolbar{
		display: inline-flex !important;
	}
	.subContent {
		font-style: italic !important;
	}

	.no-padding{
		padding : 0px !important;
	}

	.widthTable {
		min-width : 50px;
		max-width : 50px;
	}

	.widthTable2 {
		min-width : 200px;
		max-width : 200px;
	}

	.widthNames {
		min-width : 300px;
		max-width : 300px;
	}

	.widthNamesTable {
		min-width : 500px;
		max-width : 500px;
	}
</style>MARUN
<div ng-controller="bonificacion" ng-cloak>
	<h3 class="page-title"> 
          Bonificación
     </h3>
     <div class="page-bar" ng-init="bonificacion.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Bonificación</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
		 	<label ng-if="id_company == 2 || id_company == 7" ng-show="historica.type == 'COSECHA'" for="" style="width: 53%;padding: 8px;">
                Personal : 
            </label>
            <select ng-if="id_company == 2 || id_company == 7" ng-show="historica.type == 'COSECHA'" name="palanca" id="palanca" ng-model="bonificacion.params.palanca" onchange="convertModeSelect()"  style="margin: 2px;height: 36px; width: 100%;">
				<option value="">Todos</option>
                <option ng-repeat="(key, value) in personal" value="{{ value }}">{{ value }}</option>
            </select>
		 	<label ng-if="id_company == 2 || id_company == 7" ng-show="historica.type == 'ENFUNDE' || historica.type == 'DESHOJE'" for="lote" style="width: 53%;padding: 8px;">
                Lote : 
            </label>
            <select ng-if="id_company == 2 || id_company == 7" ng-show="historica.type == 'ENFUNDE' || historica.type == 'DESHOJE'" onchange="convertModeSelect()" name="lote" id="lote" ng-model="bonificacion.params.lote" style="margin: 2px;height: 36px; width: 100%;">
				<option value="">Todos</option>
                <option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
            </select>
            <label for="" style="width: 53%;padding: 8px;">
                Tipo : 
            </label>
            <select onchange="convertModeSelect()" name="mode" id="mode" ng-model="bonificacion.params.mode" style="margin: 2px;height: 36px; width: 100%;">
                <option value="0"> Peso </option>
                <option value="1"> Merma </option>
                <option value="2"> Daños </option>
            </select>   
         	<label ng-if="showFincas > 0" for="" style="width: 53%;padding: 8px;">
                Finca : 
            </label>
            <select ng-if="showFincas > 0" onchange="cambiosFincas()" name="idFinca" id="idFinca" ng-model="bonificacion.params.idFinca" style="margin: 2px;height: 36px; width: 100%;">
                <option ng-repeat="(key, value) in fincas" value="{{key}}">{{value}}</option>
            </select>   
            <label for="" style="width: 60%;padding: 6px;">
                Labor: 
            </label>
           <select name="type" id="type" class="form-control" ng-model="historica.type" onchange="changeHistorica()" style="margin: 2px;height: 36px; width: 100%;">
           		<option value="LOTERO AEREO">LOTERO AEREO</option>
           		<option value="COSECHA">COSECHA</option>
           		<option value="DESHOJE">DESHOJE</option>
           	</select>
        </div>
    </div>
	<div class="portlet box green">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Configuracion </div>
			<div class="tools">
				<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
			</div>
		</div>
		<div class="portlet-body form">
			<form action="#" class="form-horizontal">
				<div class="form-body">
					<div class="row">
						<h3 class="form-section col-md-3">Datos de {{configuracion.title}}</h3>
						<div class="col-md-offset-7 col-md-2">
							<button type="button" ng-click="configuracion.save()" class="btn green">Guardar</button>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label col-md-3">Umbral : </label>
								<div class="col-md-9">
									<input type="number" min="0" class="form-control" id="umbral" name="umbral" ng-model="configuracion.data.umbral" placeholder="10">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label col-md-3">Bono : </label>
								<div class="col-md-9">
									<input type="number" min="0" class="form-control" id="bono" name="bono" ng-model="configuracion.data.bono" placeholder="90">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label col-md-4">Descuento : </label>
								<div class="col-md-8">
									<input type="number" min="0" class="form-control" id="descuento" name="descuento" ng-model="configuracion.data.descuento" placeholder="0">
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

	<div class="portlet box green" id="contentTable">
		<div class="portlet-title">
		    <span class="caption">Bonificación</span>
		    <div class="tools">
				<button id="saveUmbrals" class="btn btn-primary inline hide" ng-click="saveUmbrals()">Guardar</button>
				<select name="semana" id="semana" class="input-sm" style="color: black;" ng-model="bonificacion.params.semana" onchange="changeweek()">
	           		<option value="">SEMANAS</option>
	           		<option ng-repeat="(key, value) in semanas | order" ng-selected="bonificacion.params.semana == value" value="{{value}}">{{value}}</option>
	           	</select>
				<div class="btn-group">
					<button class="btn btn-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
						Exportar
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="javascript:;" ng-click="exportExcel('table_bonificacion', 'Bonificación')">Excel</a></li>
						<li><a href="javascript:;" ng-click="exportPdf('table_bonificacion')">PDF</a></li>
						<li><a href="javascript:;">Imprimir</a></li>
					</ul>
				</div>
	        </div>
		</div>
		<div class="portlet-body">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover" id="table_bonificacion">
					<thead>
			                <tr role="row" class="heading">
			                    <th class="center-th widthTable" ng-click="orderByField= 'lote';reverseSort = !reverseSort"> {{title.column_one}}  </th>
								<th ng-show="id_company == 2" class="center-th widthTable"> Hectáreas </th>
								<th ng-show="id_company == 2" class="center-th widthTable" ng-click="orderByField= 'umbral'; reverseSort = !reverseSort"> Umbral </th>
			                    <th class="center-th widthNames" ng-click="orderByField= 'enfundador';reverseSort = !reverseSort"> {{title.column_two}}  </th>
			                    <th class="center-th widthTable" ng-repeat="dia in dias" ng-bind="dia"></th>
			                    <th class="center-th widthTable" ng-click="orderByField= 'total';reverseSort = !reverseSort"> {{ (bonificacion.params.mode == 2 && id_company != 2) ? 'Total' : 'Prom' }} Sem </th> 
			                    <th class="center-th widthTable" ng-click="orderByField= 'exedente';reverseSort = !reverseSort"> Exced </th>                                                  
			                    <th class="center-th widthTable" ng-click="orderByField= 'usd';reverseSort = !reverseSort">  USD  </th>                                                  
			                </tr>
					</thead>
					<tbody>
						<tr ng-repeat-start="lote in lotes | orderObjectBy:orderByField:reverseSort" ng-click="openDetalle(lote)">
							<td class="center-td">{{lote.lote}}</td>
							<td ng-show="id_company == 2" class="center-td"><input class="input form-control hectareas" type="text" data-lote="{{ lote.lote }}" value="{{ lote.hectareas | number : 2 }}" end/></td>
							<td ng-show="id_company == 2" class="center-td"><input class="input form-control umbral" type="text" data-lote="{{ lote.lote }}" value="{{ lote.umbral || 0 | number : 2 }}" end/></td>
							<td class="left-td">{{lote.enfundador}}</td>
							<td ng-repeat="dia in campos" >
								<span class="{{tagsFlags(lote.umbral, lote.cantidad[dia])}}"></span>
								{{lote.cantidad[dia] | number : 2}}
							</td>
							<td>
								<span class="{{tagsFlags(lote.umbral, lote.total)}}"></span>
								{{lote.total | number : 2}}
							</td>
							<td>
								{{ lote.exedente | number : 2 }}
							</td>
							<td ng-show="id_company == 2">{{ (lote.total <= (lote.umbral || 0)) ? (lote.hectareas || 0) * configuracion.data.bono : lote.usd | number : 2 }}</td>
							<td ng-show="id_company == 7">{{ lote.usd | number : 2 }}</td>
						</tr>	
						<tr ng-show="lote.expanded" ng-repeat-end="" ng-repeat="defecto in lote.defectos">
							<td colspan="2" class="left-td" >{{defecto.defecto}}</td>
							<td ng-show="id_company == 2" colspan="2" class="left-td "></td>
							<td ng-repeat="dia in campos" >
								<span class="{{tagsFlags(defecto.valor[dia])}}"></span>
								{{defecto.valor[dia] | number : 2}}
							</td>
							<td class="center-td ">
								<span class="{{tagsFlags(defecto.total)}}"></span>
								{{defecto.total | number : 2}}
							</td>
							<td colspan="{{id_company == 3 ? 1 : 2}}" class="center-td "></td>
						</tr>
					</tbody>
					<tfoot>
						<tr ng-repeat="total in totales">
							<td>Total : </td>
							<td ng-show="id_company == 2"></td>
							<td ng-show="id_company == 2"></td>
							<td></td>
							<td ng-repeat="dia in campos">
								{{total.cantidad[dia]  | number : 2}}
							</td>
							<td>{{total.defectos  | number : 2}}</td>
							<td>{{total.exedente  | number : 2}}</td>
							<td>{{total.usd  | number : 2}}</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
	<div id="contentHistorico">
		<div class="portlet box green">
			<div class="portlet-title">
			    <span class="caption">Historico</span>
			    <!-- <div class="tools">
		        </div> -->
			</div>
			<div class="portlet-body">
				<div id="historico" style="height: 400px"></div>
			</div>
		</div>
		<div class="portlet box green">
			<div class="portlet-title">
			    <span class="caption">Historíco Semanal</span>
			    <div class="tools">
					<div class="btn-group">
						<button class="btn btn-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
							Exportar
						</button>
						<ul class="dropdown-menu" role="menu">
							<li><a href="javascript:;" ng-click="exportExcel('table_historico', 'Historíco Semanal')">Excel</a></li>
							<li><a href="javascript:;">PDF</a></li>
							<li><a href="javascript:;">Imprimir</a></li>
						</ul>
					</div>
		        </div>
			</div>
			<div class="portlet-body">
				<div class="table-responsive">
					<table class="table table-striped table-bordered table-hover" id="table_historico">
						<thead>
				                <tr role="row" class="heading">
				                    <th width="8.33%" class="center-th" ng-click="orderByField= 'lote';reverseSort = !reverseSort"> {{title.column_one}}  </th>
				                    <th width="8.33%" class="center-th" ng-repeat="dia in semanas_historico" ng-bind="dia"></th>
				                    <th width="8.33%" class="center-th" ng-click="orderByField= 'total';reverseSort = !reverseSort"> Total </th>                                  
				                </tr>
						</thead>
						<tbody>
							<tr ng-repeat-start="lote in historico_tabla | orderObjectBy:orderByField:reverseSort" ng-click="openDetalle(lote)">
								<td class="center-td">{{lote.lote}}</td>
								<td ng-repeat="dia in semanas_historico" class="{{ bgTagsFlags(lote.umbral[dia], lote.cantidad[dia]) }}">
									<!-- (lote.cantidad[dia] > 0) ? ( lote.cantidad[dia] | number : 2 ) : '' -->
									{{
										lote.cantidad[dia] | number : 2
									}}
								</td>
								<td class="{{ bgTagsFlags(lote.umbral_total, lote.total) }}">
									{{lote.total | number : 2}}
								</td>
							</tr>
							<tr ng-show="lote.expanded" ng-repeat-end="" ng-repeat="campo in lote.details | orderObjectBy:orderByField:reverseSort" style="font-style: italic !important;">
								<td class="center-td">{{campo.defecto}}</td>
								<td ng-repeat="dia in semanas_historico" >
									 <span ng-if="campo.valor[dia] > 0" class="{{tagsFlags(campo.valor[dia])}}"></span> 
									{{campo.valor[dia] | number : 2}}
								</td>
								<td>
									<span ng-if="campo.total > 0" class="{{tagsFlags(campo.total)}}"></span>
									{{campo.total | number : 2}}
								</td>
							</tr>	
						</tbody>
						<tfoot>
							<tr ng-repeat="total in totales_historico">
								<td>Total : </td>
								<td ng-repeat="dia in semanas_historico">
									{{total.cantidad[dia]  | number : 2}}
								</td>
								<td>{{total.defectos  | number : 2}}</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>