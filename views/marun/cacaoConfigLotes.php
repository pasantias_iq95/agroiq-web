<style>
    .chart {
        min-height : 400px;
        height: 100%;
    }
    td, th {
        text-align : center;
    }
</style>
<div ng-controller="controller">
	<h3 class="page-title"> 
        Config. Lotes
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Config. Lotes</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar" id="filters">
            
        </div>
    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                Configuración
            </div>
            <div class="tools">
                
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>HA</th>
                                    <th>LOTE</th>
                                    <th>SECTOR</th>
                                    <th>SUBLOTES</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="l in lotes">
                                    <td>{{ l.hectareas }}</td>
                                    <td>{{ l.nombre }}</td>
                                    <td>{{ l.sector }}</td>
                                    <td>{{ l.sublotes }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="grafica-arbol" class="chart">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script src="componentes/FilterableSortableTable.js"></script>