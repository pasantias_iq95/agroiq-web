<style>
    .chart {
        height : 400px;
    }
</style>
<div ng-controller="controller">
	<h3 class="page-title"> 
        Reporte Cosecha
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Reporte Cosecha</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar" id="filters">
            Sector:
            <select class="input-sm" ng-change="init()" ng-model="filters.sector">
                <option value="">Todos</option>
                <option value="{{key}}" ng-repeat="(key, name) in sectores">{{name}}</option>
            </select>
        </div>
    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                Reporte de Cosecha ({{ filters.unidad }})
            </div>
            <div class="tools">
                <select ng-model="filters.unidad" class="form-control" ng-change="init()">
                    <option value="KG">KG</option>
                    <option value="LB">LB</option>
                    <option value="QQ">QQ</option>
                </select>
            </div>
        </div>
        <div class="portlet-body">
            <div id="chart-lotes" class="chart">

            </div>
            <div id="table-data"></div>
        </div>
    </div>

</div>
<script src="componentes/FilterableSortableTable.js"></script>