<style>
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
    .nobmp-reporte-accordion{
        border: 0 !important;
        margin: 0 !important;
        padding: 0 !important;
    }
</style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="informe_tthh" ng-cloak>
     <h3 class="page-title"> 
          Personal
     </h3>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Personal</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
           
        </div>
     </div>
     <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-settings"></i>PERSONAL</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="portlet-body">
                        <div class="row" style="padding-bottom: 5px;">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <button id="sample_editable_1_new" class="btn green"> Agregar Personal
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                            <thead>
                                <tr>
                                    <th>N</th>
                                    <th>NOMBRES</th>
                                    <th>N CEDULA</th>
                                    <th>STATUS</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="detalle in tabla.detalle">
                                    <td width="5%" >{{detalle.id}}</td>
                                    <td width="18%" >
                                        {{detalle.nombre}}
                                    </td>
                                    <td width="12%" >
                                        {{detalle.cedula}}
                                    </td>
                                    <td width="12%" >
                                        <button class="btn btn-sm {{(detalle.status == 1) ? 'bg-green-jungle bg-font-green-jungle' : 'red' }} status" id="status">
                                            {{detalle.tittle_status}}
                                        </button>
                                    </td>
                                    <td width="12%" >
                                        <a href="/cacaotthhFPersonal?idPersonal={{detalle.id}}"> Editar </a>
                                    </td>
                                    <td width="12%">
                                    {{detalle.status}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>