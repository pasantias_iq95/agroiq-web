<style>
    .chart {
        height : 400px;
    }
    td, th {
        text-align : center;
    }
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }

    #columns {
        column-width: 320px;
        column-gap: 15px;
        width: 90%;
        margin: 50px auto;
    }

    div#columns figure {
        background: #fefefe;
        border: 2px solid #fcfcfc;
        box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
        margin: 0 2px 15px;
        padding: 15px;
        padding-bottom: 10px;
        transition: opacity .4s ease-in-out;
        display: inline-block;
        column-break-inside: avoid;
    }

    div#columns figure img {
        width: 100%; height: auto;
        border-bottom: 1px solid #ccc;
        padding-bottom: 15px;
        margin-bottom: 5px;
    }

    div#columns figure figcaption {
        font-size: 1rem;
        color: #444;
        line-height: 1.5;
    }

    div#columns small { 
        font-size: 1rem;
        float: right; 
        text-transform: uppercase;
        color: #aaa;
    } 

    div#columns small a { 
        color: #666; 
        text-decoration: none; 
        transition: .4s color;
    }

    @media screen and (max-width: 750px) { 
        #columns { column-gap: 0px; }
        #columns figure { width: 100%; }
    }

    .calendar-table td.available {
		background-color: green !important;
    	color: white !important;
	}
	.icalendar-table td.available:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>

<div ng-controller="controller" ng-cloak>
    <div style="width: 100%;margin-bottom: 10px;display:table;">
        <h3 class="page-title" style="display: table-cell !important;"> 
            Reporte Día
        </h3>

        <div class="theme-panel">
            <div class="toggler" style="display: block;" ng-click="openMenu()">
                <i class="fa fa-cogs"></i>
            </div>
            <div class="toggler-close" style="display: none;" ng-click="closeMenu()">
                <i class="fa fa-times"></i>
            </div>
            <div class="theme-options" style="display: none;">
                <div class="theme-option">
                    <small> % Calidad Cluster </small>
                    <br>
                    <span >Bajo </span>
                    <input class="layout-style-option form-control input-sm" type="number" ng-model="umbral_cluster.min" />
                    <span> Alto </span>
                    <input class="layout-style-option form-control input-sm" type="number" ng-model="umbral_cluster.max" />
                </div>
                <div class="theme-option">
                    <small> % Calidad Dedos </small>
                    <br>
                    <span> Bajo </span>
                    <input class="layout-style-option form-control input-sm" type="number" ng-model="umbral_dedos.min" />
                    <span> Alto </span>
                    <input class="layout-style-option form-control input-sm" type="number" ng-model="umbral_dedos.max" />
                </div>
                <div class="theme-option">
                    <small> % Calidad Empaque </small>
                    <br>
                    <span> Bajo </span>
                    <input class="layout-style-option form-control input-sm" type="number" ng-model="umbral_empaque.min" />
                    <span> Alto </span>
                    <input class="layout-style-option form-control input-sm" type="number" ng-model="umbral_empaque.max" />
                </div>
                <div class="theme-option">
                    <button class="btn btn-success" ng-click="saveCalidadUmbral(); closeMenu()">
                        Guardar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Calidad</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            Marca
            <select class="input-sm" ng-model="filters.marca" ng-change="init()" style="height: 40px;">
                <option value="">TODOS</option>
                <option value="{{marca}}" ng-repeat="marca in marcas">{{ marca }}</option>
            </select>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
    </div>
     
    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li class="{{ !filters.id_finca ? 'active' : '' }}">
                <a ng-click="filters.id_finca = 0; init()">{{ nombre_cliente }}</a>
            </li>
            <li ng-repeat="finca in fincas" class="{{ filters.id_finca == finca.id ? 'active' : '' }}">
                <a ng-click="filters.id_finca = finca.id; init()">{{finca.finca}}</a>
            </li>
        </ul>
    </div>

    <div id="tags">
        <?php include("./views/sumifru/tags_calidad.php");?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Defectos</span>
                    </div>
                </div>
                <div class="portlet-body" id="defectos">
                    <div class="table-responsive" ng-show="defectos.data.length > 0">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="4"></th>
                                    <th colspan="{{ categoria.defectos.length }}" ng-repeat="categoria in defectos.categorias">{{ categoria.type }}</th>
                                    <th colspan="3"></th>
                                    <th ng-if="config.calidad_empaque"></th>
                                </tr>
                                <tr>
                                    <th>N Caja</th>
                                    <th>Peso (LB)</th>
                                    <th># Cluster</th>
                                    <th># Dedos</th>
                                    <th ng-repeat-start="categoria in defectos.categorias" class="hide"></th>
                                    <th ng-repeat-end ng-repeat="defecto in categoria.defectos" title="{{ defecto.descripcion }}">{{ defecto.siglas }}</th>
                                    <th>Total Daños</th>
                                    <th>Cal. Dedos (%)</th>
                                    <th>Cal. Cluster (%)</th>
                                    <th ng-if="config.calidad_empaque">Cal. Empaque (%)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="calidad in defectos.data">
                                    <td>{{$index+1}}</td>
                                    <td><span ng-class="{ 'font-red-thunderbird' : calidad.peso == 0 }">{{calidad.peso | number : 2}}</span></td>
                                    <td>{{calidad.cantidad_cluster_caja}}</td>
                                    <td>{{calidad.cantidad_dedos}}</td>
                                    <td ng-repeat-start="categoria in defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="defecto in categoria.defectos">{{ calidad.defectos[categoria.type+'_'+defecto.siglas] }}</td>
                                    <td>{{ calidad.total_defectos }}</td>
                                    <td class="{{ getDedosUmbral(calidad.calidad_dedos) }}" >{{calidad.calidad_dedos | number : 2}}</td>
                                    <td class="{{ getClusterUmbral(calidad.calidad_cluster) }}">{{calidad.calidad_cluster | number : 2}}</td>
                                    <td ng-if="config.calidad_empaque" class="{{ getEmpaqueUmbral(calidad.calidad_empaque) }}">{{ calidad.calidad_empaque | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>P/T</td>
                                    <td>{{ defectos.data | avgOfValue : 'peso' | number : 2 }}</td>
                                    <td>{{ defectos.data | avgOfValue : 'cantidad_cluster_caja' | number : 2 }}</td>
                                    <td>{{ defectos.data | avgOfValue : 'cantidad_dedos' | number : 2 }}</td>
                                    <td ng-repeat-start="categoria in defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="defecto in categoria.defectos">{{ defectos.data | sumOfValueDouble : 'defectos' : (categoria.type+'_'+defecto.siglas) }}</td>
                                    <td>{{ defectos.data | sumOfValue : 'total_defectos' | number }}</td>
                                    <td class="{{ getDedosUmbral((defectos.data | avgOfValue : 'calidad_dedos')) }}">{{ defectos.data | avgOfValue : 'calidad_dedos' | number : 2 }}</td>
                                    <td class="{{ getClusterUmbral((defectos.data | avgOfValue : 'calidad_cluster')) }}">{{ defectos.data | avgOfValue : 'calidad_cluster' | number : 2 }}</td>
                                    <td ng-if="config.calidad_empaque" class="{{ getEmpaqueUmbral(tags.calidad_empaque) }}">{{ tags.calidad_empaque = (defectos.data | avgOfValue : 'calidad_empaque') | number : 2 }}</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th>{{ total_defectos.cantidad_cluster_caja = (defectos.data | sumOfValue : 'cantidad_cluster_caja') | number }}</th>
                                    <th></th>
                                    <td ng-repeat-start="categoria in defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="defecto in categoria.defectos"></td>
                                    <td></td>
                                    <td style="text-align: right !important;"></td>
                                    <td style="text-align: right !important;"></td>
                                    <td ng-if="config.calidad_empaque"></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div ng-show="defectos.data.length == 0">
                        <h4>SIN DEFECTOS</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6" ng-if="config.calidad_empaque">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Empaque</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="">
                            <a href="#empaque_tab_tabla" data-toggle="tab" aria-expanded="true"> Tabla </a>
                        </li>
                        <li class="active">
                            <a ng-click="reRenderEmpaquePie()" href="#empaque_tab_grafica" data-toggle="tab" aria-expanded="false"> Gráfica </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="empaque">
                    <div class="tab-content">
                        <div class="tab-pane" id="empaque_tab_tabla">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Detalle</th>
                                            <th>Cantidad Cluster</th>
                                            <th>%</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in empaque.data">
                                            <td>{{ row.label }}</td>
                                            <td>{{ row.value }}</td>
                                            <td>{{ row.porc = (row.value/(empaque.data | sumOfValue : 'value')*100) | number : 2 }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>{{ total_empaque.cantidad = (empaque.data | sumOfValue : 'value') | number : 2 }}</th>
                                            <th>100%</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane active" id="empaque_tab_grafica">
                            <div id="empaque-pie" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div ng-class="{ 'col-md-6' : config.calidad_emapque, 'col-md-12' : !config.calidad_empaque }">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Cluster</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="">
                            <a href="#cluster_tab_tabla" data-toggle="tab" aria-expanded="false"> Tabla </a>
                        </li>
                        <li class="active">
                            <a ng-click="reRenderClusterPie()" href="#cluster_tab_grafica" data-toggle="tab" aria-expanded="true"> Gráfica </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="cluster">
                    <div class="tab-content">
                        <div class="tab-pane active" id="cluster_tab_grafica">
                            <div id="cluster-pie" class="chart"></div>
                        </div>
                        <div class="tab-pane" id="cluster_tab_tabla">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th># Dedos / Cluster</th>
                                            <th>Cantidad Cluster</th>
                                            <th>%</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="r in cluster.data">
                                            <td>{{ r.tipo }}</td>
                                            <td>{{ r.cantidad }}</td>
                                            <td>{{ r.cantidad/(cluster.data | sumOfValue : 'cantidad')*100 | number:2 }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>{{ (cluster.data | sumOfValue : 'cantidad') }}</th>
                                            <th>100%</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Fotos</span>
                    </div>
                </div>
                <div class="portlet-body" id="fotos">
                    <!-- MUESTRAS -->
                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-tabs">
                            <li ng-class="{ 'active' : !filters.fotos.id_calidad }">
                                <a ng-click="deleteFilter('id_calidad')">TODOS</a>
                            </li>
                            <li ng-repeat="row in defectos.data" class="{{ filters.fotos.id_calidad == row.id ? 'active' : '' }}">
                                <a ng-click="filters.fotos.id_calidad = row.id;">MUESTRA {{$index+1}}</a>
                            </li>
                        </ul>
                    </div>

                    <!-- TIPOS -->
                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-tabs">
                            <li ng-class="{ 'active' : !filters.fotos.type }">
                                <a ng-click="deleteFilter('type')">TODOS</a>
                            </li>
                            <li ng-repeat="type in (fotos.data | filter : { id_calidad : filters.fotos.id_calidad } | getNotRepeat : 'type')" class="{{ filters.fotos.type == type ? 'active' : '' }}">
                                <a ng-click="filters.fotos.type = type;">{{ type }}</a>
                            </li>
                        </ul>
                    </div>

                    <div class="row" id="columns">
                        <figure ng-repeat="image in fotos.data | filter : filters.fotos">
                            <a href="{{image.path}}" target="_black"><img src="{{ image.path }}" alt="Imagen no disponible"></a>
                            <figcaption>
                                <b>Calidad : {{ image.calidad_cluster }}%</b>
                                <br>
                                Fecha : {{ image.fecha }}
                                <br>
                                Tipo : {{ image.type }}
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>