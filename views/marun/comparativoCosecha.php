<style>
    th, td {
        text-align : center;
    }
</style>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>-->
<div ng-controller="controller" ng-cloak>
	<h3 class="page-title"> 
        Comparativo - Cosecha
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Comparativo - Cosecha</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar" id="filters">
            Fecha : 
            <input class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" ng-model="filters.fecha" ng-change="load()" readonly>
        </div>
    </div>

    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                BASE DE DATOS
            </div>
            <div class="tools">

            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="5">FORMULARIO</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>{{ data.formularios | sumOfValue : 'mallas' }}</th>
                                    <th>{{ data.formularios | sumOfValue : 'peso' | number: 2 }}</th>
                                </tr>
                                <tr>
                                    <th>CODIGO</th>
                                    <th>LOTE</th>
                                    <th>SECTOR</th>
                                    <th># MALLAS</th>
                                    <th>PESO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="r in data.formularios">
                                    <td ng-dblclick="r.editing = true">
                                        <span ng-show="!r.editing">{{ r.codigo }}</span>
                                        <input type="text" ng-show="r.editing" ng-model="r.codigo" class="form-control input-sm">
                                    </td>
                                    <td>{{ r.lote }}</td>
                                    <td>{{ r.sector }}</td>
                                    <td>{{ r.mallas | number }}</td>
                                    <td>{{ r.peso }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="5">BALANZA</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>{{ data.balanza | sumOfValue : 'mallas' }}</th>
                                    <th>{{ data.balanza | sumOfValue : 'peso' | number: 2 }}</th>
                                </tr>
                                <tr>
                                    <th>CODIGO</th>
                                    <th>LOTE</th>
                                    <th>SECTOR</th>
                                    <th># MALLAS</th>
                                    <th>PESO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="r in data.balanza">
                                    <td>{{ r.codigo }}</td>
                                    <td>{{ r.lote }}</td>
                                    <td>{{ r.sector }}</td>
                                    <td>{{ r.mallas }}</td>
                                    <td>{{ r.peso | number: 2 }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>