<style>
	th, td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	table {
		cursor: pointer !important;
	}
	.anchoTable {
		width: 100px;
	}
    .delete  {
        background-color : #c8d8e8 !important;
    }
    .chart {
        height : 400px;
    }
    .red {
        background-color: red;
        color : white;
    }
    .form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
    .highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}

    .has-error {
        background-color : #f49fa7;
    }
    .selected {
        background-color : #dadada;
    }
    .min-width {
        min-width : 100px;
    }
</style>

<div id="scope" ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción
     </h3>
     <div class="page-bar" ng-init="getLastDay()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <button class="btn green-jungle" ng-disabled="cargandoHistorico" ng-click="exportarFormatoEspecial()">Excel</button>
		 	
            <input id="datepicker" class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly>
        </div>
    </div>

    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li ng-repeat="(key, value) in fincas" class="{{ produccion.params.finca == key ? 'active' : '' }}">
                <a ng-click="produccion.params.finca = key; produccion.params.id_lote_adicional = ''; produccion.nocache(); getMuestreo()">{{value}}</a>
            </li>
        </ul>
    </div>

	<?php include('./views/marun/tags_produccion_dia_demo.php') ?>

	<div class="row">
        <div class="col-md-6">
            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN POR EDAD</span>
				</div>
				<div class="portlet-body">
                    <div class="row">
                        <div class="table-responsive col-md-12" id="racimos_edad">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th colspan="2">PROMEDIO</th>
                                        <th colspan="3">RACIMOS</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th class="center-th">EDAD</th>
                                        <th class="center-th">CALIB 2DA</th>
                                        <th class="center-th">CALIB ULT</th>
                                        <th class="center-th" title="Procesados">PROC</th>
                                        <th class="center-th" title="Recusados">RECU</th>
                                        <th class="center-th" title="Cortados">CORT</th>
                                        <th class="center-th">%</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="color in tabla.edades | orderObjectBy : 'edad'" ng-show="color.procesados > 0 || color.recusados > 0 || color.edad == 'N/A'">
                                        <td class="{{ color.class }}">{{color.edad | uppercase}}</td>
                                        <td>{{color.calibracion_segunda | number:2 }}</td>
                                        <td>{{color.calibracion_ultima | number:2}}</td>
                                        <td>{{color.procesados}}</td>
                                        <td>{{color.recusados}}</td>
                                        <td>{{color.cosechados}}</td>
                                        <td>{{color.cosechados/totales.cosechados*100 | number : 2}}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>{{ tags.edad | number: 2 }}</td>
                                        <td>{{ resumen_totales.calibre_segunda | number : 2 }}</td>
                                        <td>{{ resumen_totales.calibre_ultima | number : 2 }}</td>
                                        <td>{{ tabla.edades | sumOfValue : 'procesados' | number }}</td>
                                        <td>{{ tabla.edades | sumOfValue : 'recusados' | number }}</td>
                                        <td>{{ totales.cosechados = (tabla.edades | sumOfValue : 'cosechados') | number }}</td>
                                        <td>100</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN DE RECUSADOS</span>
				</div>
				<div class="portlet-body">
                    <div class="row">
                        <div class="table-responsive" id="racimos_recusados">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>CAUSA</th>
                                        <th>CANTIDAD</th>
                                        <th>PORCENTAJE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="r in recusados">
                                        <td>{{ r.causa }}</td>
                                        <td>{{ r.cantidad }}</td>
                                        <td>{{ r.porcentaje | number: 2 }}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>RECUSADOS</th>
                                        <th>{{ recusados | sumOfValue : 'cantidad' | number }}</th>
                                        <th>100</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="col-md-6 hide">
                            <div id="grafica-defectos" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN POR LOTE</span>
				</div>
				<div class="portlet-body" >
					<div class="table-responsive" id="promedios_lotes">
						<table class="table table-striped table-bordered table-hover">
							<thead>
                                <tr>
                                    <th class="center-th"></th>
									<th class="center-th" colspan="{{edades.length}}" ng-if="edades.length > 0">EDAD</th>
                                    <th class="center-th" colspan="4">RACIMOS</th>
									<th class="center-th" colspan="4">PROMEDIO</th>
                                </tr>
								<tr>
									<th class="center-th">LOTE</th>
									<th class="center-th {{ e.class }}" ng-repeat="e in edades | orderObjectBy:'edad'">{{ e.edad }}</th>
                                    <th class="center-th">COSE</th>
									<th class="center-th">PROC</th>
                                    <th class="center-th">RECU</th>
									<th class="center-th">PESO PROM</th>
									<th class="center-th">CALIB 2DA</th>
                                    <th class="center-th">CALIB ULT</th>
									<th class="center-th">MANOS</th>
									<th class="center-th">DEDOS</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="row in resumen">
									<td>{{ row.lote }}</td>
									<td ng-repeat="e in edades | orderObjectBy:'edad'">{{ row['edad_'+e.edad] }}</td>
                                    <td>{{ row.cosechados = ((row.procesados | num) + (row.recusados | num)) | number }}</td>
									<td>{{ row.procesados | number }}</td>
                                    <td>{{ row.recusados }}</td>
									<td>{{ row.peso }}</td>
									<td>{{ (row.calibre_segunda > 0) ? row.calibre_segunda : '' }}</td>
                                    <td>{{ (row.calibre_ultima > 0) ? row.calibre_ultima : '' }}</td>
									<td>{{ (row.manos > 0) ? row.manos : '' }}</td>
									<td>{{ (row.dedos > 0) ? row.dedos : '' }}</td>
								</tr>
							</tbody>
                            <tfoot>
                                <tr>
                                    <th>TOTAL</th>
                                    <th class="center-th" ng-repeat="e in edades | orderObjectBy:'edad'">{{ resumen | sumOfValue : 'edad_'+e.edad }}</th>
                                    <th class="center-th">{{ resumen | sumOfValue : 'cosechados' | number }}</th>
                                    <th class="center-th">{{ resumen | sumOfValue : 'procesados' | number }}</th>
                                    <th class="center-th">{{ resumen | sumOfValue : 'recusados' | number }}</th>
                                    <th class="center-th">{{ resumen_totales.peso_prom | number: 2 }}</th>
                                    <th class="center-th">{{ resumen_totales.calibre_segunda | number: 2 }}</th>
                                    <th class="center-th">{{ resumen_totales.calibre_ultima | number: 2 }}</th>
                                    <th class="center-th">{{ resumen_totales.manos_prom | number: 2 }}</th>
                                    <th class="center-th">{{ resumen_totales.dedos_prom | number: 2 }}</th>
                                </tr>
                            </tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i> Racimos </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="panel-group accordion" id="accordion3">
   
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_7" aria-expanded="false"> Análisis de Defectos </a>
                        </h4>
                    </div>
                    <div id="collapse_3_7" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="col-md-3 pull-right">
                                <select class="form-control" ng-model="produccion.params.var_recusado" ng-init="produccion.params.var_recusado = 'cant';" ng-change="getAnalisisRecusados()">
                                    <option value="cant">CANTIDAD</option>
                                    <option value="porc">% </option>
                                </select>
                            </div>
                            <div id="table-defectos"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4" aria-expanded="false"> Base de Datos Racimos </a>
                        </h4>
                    </div>
                    <div id="collapse_3_4" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="btn-group pull-right" style="margin-right: 5px;">
                                <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                    Exportar <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <a href="javascript:;" ng-click="exportPrint('registros')"> Imprimir </a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" ng-click="exportExcel('registros')">Excel</a>
                                    </li>
                                </ul>
                            </div>
                            <button class="btn red-thunderbird pull-right" style="height: 30px;" ng-click="eliminar()">Eliminar</button>
                            <button class="btn green-jungle pull-right" ng-show="editing" style="height: 30px;" ng-click="guardar()">Guardar</button>
                            <br>
                            <div class="table-responsive table-scrollable">
                                <table class="table table-striped table-bordered table-hover" id="edit_registros">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable" ng-click="setOrderTable('id')"> ID </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('hora')"> Hora </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('viaje')"> Viaje </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('num_racimo')"> # Rac </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('cuadrilla')"> Cuadrilla  </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('lote')"> Lote </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('edad')"> Edad </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('peso')"> KG </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('manos')"> Manos </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('calibre_segunda')"> Calibre Segunda </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('calibre_ultima')"> Calibre Ultima </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('dedos')"> Dedos </th>
                                            <th class="center-th anchoTable" style="min-width: 100px;" ng-click="setOrderTable('tipo')"> Tipo </th>
                                            <th class="center-th anchoTable" ng-click="setOrderTable('causa')"> Causa </th>
                                        </tr>
                                        <tr>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.id" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.hora" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilterE.viaje" ng-change="deleteProp(searchFilterE, 'viaje')" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilterE.num_racimo" ng-change="deleteProp(searchFilterE, 'num_racimo')"/></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter._cuadrilla" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter._lote" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter._edad" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.peso" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.manos" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.calibre_segunda" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.calibre_ultima" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.dedos" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.tipo" /></th>
                                            <th><input type="text" class="form-control input-filter" ng-model="searchFilter.causa" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="{{ row.delete ? 'delete' : '' }}" 
                                            data-id="{{row.id}}" 
                                            data-marca="{{row.marca}}" 
                                            data-index="{{$index}}"
                                            ng-dblclick="edit(row)"
                                            ng-repeat="row in resultValue = (registros | filter: searchFilter | filter : searchFilterE : true | orderObjectBy : searchTable.orderBy : searchTable.reverse | startFrom: searchTable.startFrom | limitTo : searchTable.limit)">
                                            <td class="anchoTable">
                                                <input type="checkbox" ng-model="row.delete" />
                                                {{ row.id }}
                                            </td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">{{ row.viaje }}</td>
                                            <td class="anchoTable">{{ row.num_racimo }}</td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{row.cuadrilla}}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.cuadrilla"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.lote }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.lote"/>
                                            </td>
                                            <td class="anchoTable {{ row.class }}">
                                                <span ng-show="!row.editing">{{ row.edad }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.edad"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.peso"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.manos > 0 ? (row.manos  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.manos"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.calibre_segunda > 0 ? (row.calibre_segunda  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.calibre"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.calibre_ultima > 0 ? (row.calibre_ultima  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing" ng-model="row.calibre"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.dedos > 0 ? (row.dedos  | number : 2) : '' }}</span>
                                                <input type="text" class="form-control input-sm" ng-if="row.editing " ng-model="row.dedos"/>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing">{{ row.tipo }}</span>
                                                <select class="form-control" ng-model="row.tipo" ng-if="row.editing">
                                                    <option value="PROC">PROC</option>
                                                    <option value="RECU">RECU</option>
                                                </select>
                                            </td>
                                            <td class="anchoTable">
                                                <span ng-show="!row.editing && row.tipo == 'RECU'">{{ row.causa }}</span>
                                                <select class="form-control input-sm" ng-if="row.editing && row.tipo == 'RECU'" ng-model="row.causa">
                                                    <option value="">Seleccione</option>
                                                    <option value="{{c}}" ng-repeat="c in causas">{{c}}</option>
                                                </option>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total: {{ (resultValue | countOfValue:'id') | number }}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValue | avgOfValue:'edad') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'peso') | number : 2 }}</td>
                                            <td>{{ (resultValue | avgOfValue:'manos') | number : 2 }}</td>
                                            <td>{{ (resultValue | avgOfValue:'calibre_segunda') | number : 2 }}</td>
                                            <td>{{ (resultValue | avgOfValue:'calibre_ultima') | number : 2 }}</td>
                                            <td>{{ (resultValue | avgOfValue:'dedos') | number : 2 }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="12">
                                                <div class="col-md-5 col-sm-12">
                                                    Página {{ (searchTable.actual_page | num) || 0 }} de {{ searchTable.numPages }}
                                                </div>
                                                <div class="col-md-7 col-sm-12">
                                                    <div class="pull-left">
                                                        <a class="btn btn-sm default prev" ng-click="prev(registros)"><</a>
                                                        <input type="text" maxlength="5" class="pagination-panel-input form-control input-sm input-inline input-mini" ng-model="searchTable.actual_page" readonly/>
                                                        <a class="btn btn-sm default next" ng-click="next(registros)">></a>

                                                        <select class="input-sm pull-right" style="color: #555;" ng-model="searchTable.limit" ng-change="searchTable.changePagination()">
                                                            <option value="{{ registros.length }}">TODOS</option>
                                                            <option ng-repeat="opt in searchTable.optionsPagination" value="{{ opt > 0 ? opt : '' }}" ng-selected="opt == searchTable.limit">{{ opt > 0 ? opt : 'TODOS' }}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>

                                <table class="table table-striped table-bordered table-hover" style="display: none;" id="registros">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th class="center-th anchoTable"> ID </th>
                                            <th class="center-th anchoTable" style="min-width: 120px;" style="min-width: 70px;"> Fecha </th>
                                            <th class="center-th anchoTable"> Hora </th>
                                            <th class="center-th anchoTable"> Cuadrilla  </th>
                                            <th class="center-th anchoTable"> Lote </th>
                                            <th class="center-th anchoTable"> Edad </th>
                                            <th class="center-th anchoTable"> Cinta </th>
                                            <th class="center-th anchoTable"> KG </th>
                                            <th class="center-th anchoTable"> Manos </th>
                                            <th class="center-th anchoTable"> Calibre Segunda </th>
                                            <th class="center-th anchoTable"> Calibre Ultima </th>
                                            <th class="center-th anchoTable"> Dedos </th>
                                            <th class="center-th anchoTable" > Tipo </th>
                                            <th class="center-th anchoTable"> Causa </th>
                                        </tr>
                                    <tbody>
                                        <tr ng-repeat="row in resultValue = (registros | orderObjectBy : searchTable.orderBy : searchTable.reverse | startFrom: searchTable.startFrom | limitTo : searchTable.limit)">
                                            <td class="anchoTable">{{ row.id }}</td>
                                            <td class="anchoTable">{{ row.fecha }}</td>
                                            <td class="anchoTable">{{ row.hora }}</td>
                                            <td class="anchoTable">{{row.cuadrilla}}</td>
                                            <td class="anchoTable">{{ row.lote }}</td>
                                            <td class="anchoTable">{{ row.edad }}</td>
                                            <td class="anchoTable">{{ row.cinta }}</td>
                                            <td class="anchoTable">{{ (row.peso > 0) ? (row.peso  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.manos > 0 && row.tipo != 'RECU') ? (row.manos  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.calibre_segunda > 0 && row.tipo != 'RECU') ? (row.calibre_segunda  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.calibre_ultima > 0 && row.tipo != 'RECU') ? (row.calibre_ultima  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ (row.dedos > 0 && row.tipo != 'RECU') ? (row.dedos  | number : 2) : '' }}</td>
                                            <td class="anchoTable">{{ row.tipo == 'RECU' ? 'RECU' : row.tipo  }}</td>
                                            <td class="anchoTable">{{ row.causa }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td>Total:</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>{{ (resultValue | avgOfValue:'edad') | number : 2 }}</td>
                                            <td></td>
                                            <td>{{ (resultValue | sumOfValue:'peso') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'manos') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'calibre_segunda') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'calibre_ultima') | number : 2 }}</td>
                                            <td>{{ (resultValue | sumOfValue:'dedos') | number : 2 }}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default hide">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_5" aria-expanded="false"> Viajes </a>
                        </h4>
                    </div>
                    <div id="collapse_3_5" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Viaje</th>
                                            <th>Lote</th>
                                            <th>Palanca</th>
                                            <th># Racimos</th>
                                            <th>Referencias</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in viajes" ng-init="row.jsons = row.referencias.split(',')" class="{{ row.jsons.length > 1 ? 'red' : '' }}">
                                            <td>{{ row.viaje }}</td>
                                            <td>
                                                <span ng-if="row.jsons.length == 1">{{ row.lote }}</span>
                                                <span ng-if="row.jsons.length > 1" ng-repeat="lote in row.lote.split(',')">
                                                    {{ lote }} <br />
                                                </span>
                                            <td>
                                                <span ng-if="row.jsons.length == 1">{{ row.palanca }}</span>
                                                <span ng-if="row.jsons.length > 1" ng-repeat="palanca in row.palanca.split(',')">
                                                    {{ palanca }} <br />
                                                </span>
                                            </td>
                                            <td>{{ row.racimos }}</td>
                                            <td>
                                                <span ng-repeat="json in row.jsons">
                                                    <a href="https://s3.amazonaws.com/json-publicos/banano/marun/racimosCosechados/pdf/{{json.replace('.json','')}}.pdf" target="_blank">{{ json.replace('.json', '') }}</a> <br />
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="openMuestreo()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_6" aria-expanded="false"> Muestreo </a>
                        </h4>
                    </div>
                    <div id="collapse_3_6" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div id="grafica-muestreo" class="chart"></div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a ng-click="openGraficasAdicionales()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_10" aria-expanded="false"> Gráficas Adicionales </a>
                        </h4>
                    </div>
                    <div id="collapse_3_10" class="panel-collapse collapse" aria-expanded="true" style="">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12"> 
                                    <div class="col-md-2 col-md-offset-8">
                                        Lote:<br>
                                        <select class="form-control input-md" ng-model="produccion.params.id_lote_adicional" ng-change="openGraficasAdicionales()">
                                            <option value="">TODOS</option>
                                            <option value="{{l.id_lote}}" ng-repeat="l in resumen">{{ l.lote }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-primary" ng-click="openGraficasAdicionales()"> <i class="fa fa-refresh"></i> Refrescar Gráficas</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <fieldset>
                                    <legend>Del Día</legend>
                                    <div class="col-md-6">
                                        <div class="col-md-10">
                                            <h3>PESO RACIMOS VS MANOS POR RACIMO (KG)</h3>
                                        </div>
                                        <div class="col-md-2">
                                            
                                        </div>
                                        <div id="grafica-manos-peso" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-10">
                                            <h3>PESO POR RACIMO</h3>
                                        </div>
                                        <div class="col-md-2">
                                            Grupo <br>
                                            <input type="number" class="form-control" ng-model="grupo_racimo" step="0.1">
                                        </div>
                                        <div id="grafica-peso-racimo" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-10">
                                            <h3>PESO PROM POR MANO</h3>
                                        </div>
                                        <div class="col-md-2">
                                            Grupo <br>
                                            <input type="number" class="form-control" ng-model="grupo_mano" step="0.1">
                                        </div>
                                        <div id="grafica-peso-mano" class="chart"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="col-md-10">
                                            <h3>PESO PROM POR °CALIB</h3>
                                        </div>
                                        <div class="col-md-2">
                                            Grupo <br>
                                            <input type="number" class="form-control" ng-model="grupo_calibre" step="0.1">
                                        </div>
                                        <div id="grafica-peso-calibre" class="chart"></div>
                                    </div>
                                    
                                    <div class="col-md-12" style="margin-top : 15px;">
                                        <div class="portlet box default">
                                            <div class="portlet-title">
                                                <ul class="nav nav-tabs">
                                                    <li ng-class="{'active' : graficas_add.var == 'manos'}">
                                                        <a ng-click="graficas_add.var = 'manos'; renderCantidad()">Manos</a>
                                                    </li>
                                                    <li ng-class="{'active' : graficas_add.var == 'calibre_segunda'}">
                                                        <a ng-click="graficas_add.var = 'calibre_segunda'; renderCantidad()">Calib. 2da</a>
                                                    </li>
                                                    <li ng-class="{'active' : graficas_add.var == 'calibre_ultima'}">
                                                        <a ng-click="graficas_add.var = 'calibre_ultima'; renderCantidad()">Calib. Ult</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="grafica-cantidad-manos-calibre" class="chart"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Tendencia</legend>
                                    <div class="col-md-12">
                                        <div id="grafica-peso-mano-tendencia" class="chart"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id="grafica-peso-calibre-tendencia" class="chart"></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_8" aria-expanded="false"> Cuadre de Racimos por Viaje </a>
                        </h4>
                    </div>
                    <div id="collapse_3_8" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body" id="viajes">
                            <div class="row">
                                <div class="col-md-12 text-right" ng-show="!dia_finalizado">
                                    <button class="btn btn-warning" ng-click="desbloquearViajes()">Desbloquear <i class="fa fa-unlock-alt"></i> </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-scrollable">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">VIAJE REAL</th>
                                                    <th clasS="text-center">VIAJE<br>BLZ-FORM</th>
                                                    <th class="text-center">LOTE<br>BLZ-FORM</th>
                                                    <th class="text-center">CUADRILLA<br>BLZ-FORM</th>
                                                    <th class="text-center">HORA BLZ</th>
                                                    <th class="text-center">HORA FORM</th>
                                                    <th class="text-center">BLZ<br>RAC</th>
                                                    <th class="text-center">FORM<br>RAC</th>
                                                    <th class="text-center">%</th>
                                                    <th class="text-center"></th>
                                                    <th class="text-center">REF.</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="row in racimosViajes | orderObjectBy : '_numero'">
                                                    <td class="text-center">{{ row._numero }}</td>
                                                    <td class="text-center">
                                                        {{ (row.blz_viaje != null) ? 'B'+row.blz_viaje : '' }} 
                                                        {{ row.blz_viaje != null && row.form_viaje != null ? '-' : '' }} 
                                                        {{ (row.form_viaje != null) ? 'F'+row.form_viaje : '' }}
                                                    </td>
                                                    <td class="text-center" ng-class="{ 'has-error' : row.blz_lote != row.form_lote }">
                                                        <a class="btn btn-sm btn-info" role="button" ng-show="row.blz_lote && row.form_lote && row.blz_lote != row.form_lote" title="Cambiar" ng-click="cambiarLote(row)">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <span ng-show="(row.blz_lote && row.form_lote) && row.blz_lote != row.form_lote">
                                                            {{ row.blz_lote }} - {{ row.form_lote }}
                                                        </span>
                                                        <span ng-show="(!row.blz_lote || !row.form_lote) || row.blz_lote == row.form_lote">
                                                            {{ row.blz_lote ? row.blz_lote : row.form_lote }}
                                                        </span>
                                                    </td>
                                                    <td class="text-center" ng-class="{ 'has-error' : row.blz_palanca && row.form_palanca && row.blz_palanca != row.form_palanca }">
                                                        <a class="btn btn-sm btn-info" role="button" ng-show="row.blz_palanca && row.form_palanca && row.blz_palanca != row.form_palanca" title="Cambiar" ng-click="cambiarPalanca(row)">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <span ng-show="(row.blz_palanca && row.form_palanca) && row.blz_palanca != row.form_palanca">
                                                            {{ row.blz_palanca }} - {{ row.form_palanca }}
                                                        </span>
                                                        <span ng-show="(!row.blz_palanca || !row.form_palanca) || row.blz_palanca == row.form_palanca">
                                                            {{ row.blz_palanca ? row.blz_palanca : row.form_palanca }}
                                                        </span>
                                                    </td>
                                                    <td class="text-center">{{ row.blz_hora }}</td>
                                                    <td class="text-center">{{ row.form_hora }}</td>
                                                    <td class="text-center" ng-class="{ 'has-error' : row.blz_racimos == 0 }">
                                                        <span ng-class="{ 'bold' : row.blz_racimos != 25 }">
                                                            {{ row.blz_racimos }}
                                                        </span>
                                                    </td>
                                                    <td class="text-center" ng-class="{ 'has-error' : row.form_racimos == 0 }">
                                                        <span ng-show="row.form_racimos > 0" ng-class="{ 'bold' : row.form_racimos != 25 }">
                                                            {{ row.form_racimos }}
                                                        </span>
                                                    </td>
                                                    <td class="text-center {{ encontrarDiferenciasTodos(row) && row.form_racimos > 0 && row.procesado == 0 ? coloresClass['CELESTE'] : '' }}"
                                                        ng-class="{ 'has-error' : row.form_error }">
                                                        <span ng-show="row.form_racimos > 0 && row.blz_racimos > 0">{{ row.blz_racimos / 25 * 100 | number }} %</span>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:;" class="btn btn-default" ng-show="!row.blocked && (row.form_racimos > 0 || row.blz_racimos > 0) && produccion.params.finca > 0" ng-click="editarViaje(row, $index)">Editar</a>
                                                        <a href="javascript:;" class="btn btn-danger" ng-show="!row.blocked && !(row.form_racimos > 0 || row.blz_racimos > 0) && produccion.params.finca > 0" ng-click="borrarViaje(row, $index)"><i class="fa fa-trash"></i></a>
                                                        <i class="fa fa-lock" ng-show="row.blocked"></i>
                                                    </td>
                                                    <td>
                                                        <span ng-repeat="r in row.form_referencias.split(',')">
                                                            <a href="//s3.amazonaws.com/json-publicos/banano/marun/racimosCosechados/pdf/{{r}}.pdf" target="_blank">{{r}}</a><br>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th class="text-center">TOT</th>
                                                    <th class="text-center"></th>
                                                    <th class="text-center"></th>
                                                    <th></th>
                                                    <th class="text-center"></th>
                                                    <th class="text-center"></th>
                                                    <th class="text-center">{{ racimosViajes | countOfValue : 'blz_racimos' }}V <br> {{ racimosViajes | sumOfValue : 'blz_racimos' }} RAC</th>
                                                    <th class="text-center">{{ racimosViajes | countOfValue : 'form_racimos' }}V <br> {{ racimosViajes | sumOfValue : 'form_racimos' }} RAC</th>
                                                    <th class="text-center"></th>
                                                    <th class="text-center"></th>
                                                    <th></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="viajes-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999!important;">
        <div class="modal-dialog modal-70" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">VIAJE #{{viajeSelected.blz_viaje}}</h4>
                </div>
                <div class="modal-body">
                    <div class="table-scrollable">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="4" class="text-center">BALANZA</th>
                                    <th colspan="4" class="text-center">FORMULARIO</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th class="text-center">RAC</th>
                                    <th class="text-center">
                                        <!-- SELECCIONAR TODOS LOS RAC BLZ -->
                                        <i class="fa fa-check-square-o" ng-show="selected.form == 0 && selected.blz == 0" ng-click="selectAll('balanza')"></i>
                                        <i class="fa fa-minus-square-o" ng-show="selected.blz > 0" ng-click="diselectAll('balanza')"></i>
                                    </th>
                                    <th class="text-center">TIPO</th>
                                    <th class="text-center">CINTA</th>
                                    <th class="text-center">LOTE</th>
                                    <th class="text-center">TIPO</th>
                                    <th class="text-center">CINTA</th>
                                    <th class="text-center">LOTE</th>
                                    <th>
                                        <!-- SELECCIONAR TODOS LOS RAC FORM -->
                                        <i class="fa fa-check-square-o" ng-show="selected.form == 0 && selected.blz == 0" ng-click="selectAll('formulario')"></i>
                                        <i class="fa fa-minus-square-o" ng-show="selected.form > 0" ng-click="diselectAll('formulario')"></i>
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="i in numbers">
                                    <td>{{ i }}</td>
                                    <td>
                                        <!-- SELECT BLZ -->
                                        <input type="checkbox" 
                                            value="1" 
                                            ng-show="selected.form == 0 && viajeSelected.balanza[i].id_racimo > 0"
                                            ng-model="viajeSelected.balanza[i].selected" 
                                            ng-checked="viajeSelected.balanza[i].selected"
                                            ng-change="changeChecked('blz', viajeSelected.balanza[i].selected, viajeSelected.balanza[i].id_racimo)">
                                    </td>
                                    <td ng-class="{ 'selected' : viajeSelected.balanza[i].selected }">
                                        <!-- SELECT TIPO CINTA BLZ -->
                                        <select ng-show="config.mode_process == 1" class="form-control" ng-model="viajeSelected.balanza[i].tipo">
                                            <option value="RECU" ng-selected="viajeSelected.balanza[i].tipo == 'RECU'">RECU</option>
                                            <option value="PROC" ng-selected="viajeSelected.balanza[i].tipo == 'PROC'">PROC</option>
                                        </select>
                                        <!-- NO EDIT TIPO CINTA BLZ -->
                                        <label style="padding:10px;"  ng-show="config.mode_process != 1">{{ viajeSelected.balanza[i].tipo }}</label>
                                    </td>
                                    <td ng-class="{ 'selected' : viajeSelected.balanza[i].selected }">
                                        <!-- SELECT CINTA BLZ 
                                        <select 
                                            ng-show="config.mode_process == 1" 
                                            class="form-control select-color {{viajeSelected.balanza[i].cssClass}}" 
                                            ng-model="viajeSelected.balanza[i].cinta" 
                                            ng-change="viajeSelected.balanza[i].cssClass = coloresClass[viajeSelected.balanza[i].cinta]">

                                            <option value="S/C" class="{{coloresClass['S/C']}}">S/C</option>
                                            <option
                                                ng-repeat="(color, cls) in coloresClass"
                                                value="{{ color }}"
                                                class="{{ cls }}"
                                                ng-if="color != 'CELESTE'"
                                                ng-selected="color == viajeSelected.balanza[i].cinta">{{ color }}</option>
                                        </select>-->
                                        <!-- NO EDIT CINTA BLZ -->
                                        <label ng-show="config.mode_process != 1" style="padding:10px;"  class="{{viajeSelected.balanza[i].cssClass}}">{{ viajeSelected.balanza[i].cinta }}</label>
                                    </td>
                                    <td ng-class="{ 'selected' : viajeSelected.balanza[i].selected }">
                                        <!-- SELECT LOTE BLZ -->
                                        <select ng-show="config.mode_process == 1" class="form-control" ng-model="viajeSelected.balanza[i].id_lote">
                                            <option value="{{l.id}}" ng-repeat="l in lotes">{{ l.nombre }}</option>
                                        </select>
                                        <!-- NO EDIT LOTE BLZ -->
                                        <label ng-show="config.mode_process != 1">{{ viajeSelected.balanza[i].lote }}</label>
                                    </td>

                                    <!-- FORMULARIO -->
                                    <td ng-class="{ 'selected' : viajeSelected.formulario[i].selected }">
                                        <select ng-show="config.mode_process == 2" class="form-control min-width" ng-model="viajeSelected.formulario[i].tipo" ng-change="viajeSelected.formulario[i].editing = true">
                                            <option value="RECU" ng-selected="viajeSelected.formulario[i].tipo == 'RECU'">RECU</option>
                                            <option value="PROC" ng-selected="viajeSelected.formulario[i].tipo == 'PROC'">PROC</option>
                                        </select>
                                        <label ng-show="config.mode_process != 2">{{ viajeSelected.formulario[i].tipo }}</label>
                                    </td>
                                    <td ng-class="{ 'selected' : viajeSelected.formulario[i].selected }">
                                        <select 
                                            ng-show="config.mode_process == 2" 
                                            class="form-control select-color {{viajeSelected.formulario[i].cssClass}}" 
                                            ng-model="viajeSelected.formulario[i].cinta" 
                                            ng-change="viajeSelected.formulario[i].editing = true; viajeSelected.formulario[i].cssClass = coloresClass[viajeSelected.formulario[i].cinta];">

                                            <option value="S/C" class="{{coloresClass['S/C']}}">S/C</option>
                                            <option
                                                ng-repeat="(color, cls) in coloresClass"
                                                value="{{ color }}"
                                                class="{{ cls }}"
                                                ng-if="color != 'CELESTE'"
                                                ng-selected="color == viajeSelected.formulario[i].cinta">{{ color }}</option>
                                        </select>
                                        <!--
                                        <label style="padding:10px;" ng-show="config.mode_process != 2" class="{{viajeSelected.formulario[i].cssClass}}">{{ viajeSelected.formulario[i].cinta }}</label>
                                        -->
                                    </td>
                                    <td ng-class="{ 'selected' : viajeSelected.formulario[i].selected }">
                                        <!-- SELECT LOTE BLZ -->
                                        <select ng-show="config.mode_process == 2" class="form-control min-width" ng-model="viajeSelected.formulario[i].id_lote" ng-change="viajeSelected.formulario[i].editing = true">
                                            <option value="{{l.id}}" ng-repeat="l in lotes" ng-selected="l.id == viajeSelected.formulario[i].id_lote">{{ l.nombre }}</option>
                                        </select>
                                        <!-- NO EDIT LOTE BLZ
                                        <label ng-show="config.mode_process != 2">{{ viajeSelected.formulario[i].lote }}</label>
                                        -->
                                    </td>
                                    <td>
                                        <button class="btn btn-sm green-haze" ng-show="viajeSelected.formulario[i].editing" ng-click="saveEditRacimo(i, viajeSelected.formulario[i])">
                                            <i class="fa fa-save"></i>
                                        </button>
                                        <button class="btn btn-sm" ng-show="viajeSelected.formulario[i].editing" ng-click="backRacimo(i, viajeSelected.formulario[i])">
                                            <i class="fa fa-reply"></i>
                                        </button>
                                        <!-- SELECT FORM -->
                                        <input 
                                            type="checkbox" 
                                            value="1" 
                                            ng-show="selected.blz == 0 && viajeSelected.formulario[i].id_racimo > 0 && !viajeSelected.formulario[i].editing"
                                            ng-checked="viajeSelected.formulario[i].selected"
                                            ng-model="viajeSelected.formulario[i].selected" 
                                            ng-change="changeChecked('form', viajeSelected.formulario[i].selected, viajeSelected.formulario[i].id_racimo)">
                                    </td>
                                    <td class="{{ encontrarDiferencias(viajeSelected.balanza[i], viajeSelected.formulario[i]) ? coloresClass['ROJA'] : '' }}"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
                    <button ng-show="selected.form > 0 || selected.blz > 0" class="btn red-soft" ng-click="borrarRacimosViajes()">Borrar racimos seleccionados</button>
                    <button ng-show="selected.form > 0 || selected.blz > 0" class="btn yellow-gold" ng-click="crearRacimosViajes()">Crear viaje nuevo</button>
                    <button ng-show="selected.form > 0 || selected.blz > 0" class="btn green-haze" ng-click="moverRacimosViajes()">Mover a otro viaje</button>
                    <button ng-show="selected.form > 0 || selected.blz > 0" class="btn green-haze" ng-click="moverRacimosFinca()">Mover a otra finca</button>
                    <button ng-show="viajeSelected.procesado == 0" class="btn blue" ng-click="guardarRacimosViajes()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mover-racimos-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999!important;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        MOVER RACIMOS DEL VIAJE {{ viajeSelected._numero }}
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                Seleccione al viaje que quiere mover
                                <small>({{ selected.blz ? selected.blz : selected.form }} Racimos)</small>
                            </p>
                            <div class="col-md-6">
                                <select class="form-control" ng-model="to_id_viaje">
                                    <option value="">Seleccione</option>
                                    <option value="{{ v.id }}" ng-repeat="v in racimosViajes" ng-if="v.numero != viajeSelected.numero" ng-class="{ 'has-error' : v.form_error }" ng-disabled="v.form_error">{{ v._numero }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn default" data-dismiss="modal">Cancelar</button>
                    <button class="btn blue" ng-click="guardarMoverRacimosViajes()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="mover-finca-racimos-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999!important;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        MOVER RACIMOS DEL VIAJE {{ viajeSelected._numero }} A OTRA FINCA
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <select class="form-control" ng-model="to_id_finca" ng-change="getViajesFinca()">
                                    <option value="">Seleccione</option>
                                    <option 
                                        value="{{ f.id }}" 
                                        ng-repeat="f in fincasAvailable" 
                                        ng-hide="id == produccion.params.finca">{{f.nombre}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <p>
                                Seleccione al viaje que quiere mover
                                <small>({{ selected.blz ? selected.blz : selected.form }} Racimos)</small>
                            </p>
                            <div class="col-md-6">
                                <select class="form-control" ng-model="to_id_viaje_otra_finca">
                                    <option value="">Seleccione</option>
                                    <option value="{{ v.id }}" ng-repeat="v in racimosViajesFinca | orderObjectBy : '_numero'">{{ v._numero }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn default" data-dismiss="modal">Cancelar</button>
                    <button class="btn blue" ng-click="guardarMoverRacimosViajesOtraFinca()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="crear-viaje-racimos-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999!important;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        CREAR VIAJE
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                Escriba al viaje que quiere crear
                                <small>({{ selected.blz ? selected.blz : selected.form }} Racimos)</small>
                            </p>
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="numero-crear-viaje">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn default" data-dismiss="modal">Cancelar</button>
                    <button class="btn blue" ng-click="guardarCrearViaje()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cambiar-palanca-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999!important;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        CAMBIAR PALANCA
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                Escriba el número de palanca
                            </p>
                            <div class="col-md-6">
                                <input type="number" class="form-control" id="numero-palanca-viaje">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn default" data-dismiss="modal">Cancelar</button>
                    <button class="btn blue" ng-click="guardarCambiarPalanca()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cambiar-lote-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9999!important;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        CAMBIAR LOTE
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                Escriba el número de lote
                            </p>
                            <div class="col-md-6">
                                <select class="form-control min-width" id="numero-lote-viaje">
                                    <option value="{{l.id}}" ng-repeat="l in lotes">{{ l.nombre }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn default" data-dismiss="modal">Cancelar</button>
                    <button class="btn blue" ng-click="guardarCambiarLote()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>
<script src="assets/global/plugins/FileSaver.min.js"></script>