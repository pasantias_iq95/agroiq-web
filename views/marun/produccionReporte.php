<style>
	td, th  {
		text-align: center;
	}
	.form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
</style>

<script src="assets/global/plugins/FileSaver.min.js"></script>

<div ng-controller="controller" ng-cloak>
	<h3 class="page-title"> 
          Reporte Producción
     </h3>
     <div class="page-bar" ng-init="produccion.nocache()">
         <ul class="page-breadcrumb">
             
         </ul>
         <div class="page-toolbar">
			<label for="">Año</label>
			<select class="input-sm" ng-model="filters.year" ng-change="changeYear()">
				<option value="{{y}}" ng-repeat="y in years" ng-selected="y == filters.year">{{y}}</option>
			</select>
			<label for="">Semana</label>
			<select class="input-sm" ng-model="filters.semana" ng-change="changeWeek()">
				<option value="{{s}}" ng-repeat="s in semanas" ng-selected="s == filters.semana">{{s}}</option>
			</select>
        </div>
    </div>

	<div class="tabbable tabbable-tabdrop">
		<ul class="nav nav-tabs">
			<li ng-repeat="(key, value) in fincas" ng-class="{ 'active' : filters.id_finca == key }">
				<a ng-click="filters.id_finca = key; getData();">{{value}}</a>
			</li>
		</ul>
	</div>
    
	<div id="contenedor" class="div2">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">REPORTE PRODUCCION POR LOTE</span>
						<div class="tools">
							<button class="btn btn-sm dark" ng-click="fnExcelReport('table_2', 'REPORTE ADM')">Excel</button>
							<select style="color : black; height : 36" class="input-md" ng-model="mostrar_calculos">
								<option value="1">Mostrar Calculos</option>
								<option value="0">Ocultar Calculos</option>
							</select>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable" id="div_table_2" style="padding-right:5px">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
										<th></th>
										<th ng-if="edades.length > 0" colspan="{{ edades.length }}">EDAD DE RACIMO</th>
										<th colspan="3">CANT RACIMOS</th>
										<th ng-show="mostrar_calculos == 1" rowspan="2">TOTAL PESO</th>
										<th rowspan="2">PESO PROM<br>RAC KG</th>
										<th ng-show="mostrar_calculos == 1" rowspan="2">PESO KG<br>PROC</th>
										<th ng-show="mostrar_calculos == 1" rowspan="2">PESO KG<br>RECU</th>
										<th ng-show="mostrar_calculos == 1" rowspan="2">CAJAS KG</th>
										<th ng-show="mostrar_calculos == 1" rowspan="2">MERMA KG<br>COSE</th>
										<th ng-show="mostrar_calculos == 1" rowspan="2">MERMA KG<br>PROC</th>
										<th ng-show="mostrar_calculos == 1" rowspan="2">%</th>
										<th rowspan="2">CAJAS<br>CONV</th>
										<th colspan="2">% MERMA</th>
										<th colspan="2">RATIO</th>
										<th rowspan="2">CAJAS<br>POR HA</th>
										<th rowspan="2">PROYECCION<br>CAJAS Ha/año</th>
									</tr>
									<tr>
										<th>LOTE</th>
										<th ng-repeat="e in edades" class="{{ cintas[e] }}">{{e}}</th>
										<th>COSE</th>
										<th>RECU</th>
										<th>PROC</th>

										<th>COSE</th>
										<th>PROC</th>
										<th>COSE</th>
										<th>PROC</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="row in data">
										<td>{{ row.lote }}</td>
										<td ng-repeat="e in edades">{{ row['edad_'+e] }}</td>
										<td>{{ row.cosechados }}</td>
										<td>{{ row.recusados }}</td>
										<td>{{ row.procesados }}</td>
										<td ng-show="mostrar_calculos == 1" class="bg-grey">{{ row.peso_racimos_cosechados | number : 2 }}</td>
										<td>{{ row.peso_prom_racimo | number : 2 }}</td>
										<td ng-show="mostrar_calculos == 1" class="bg-grey">{{ row.peso_racimos_procesados | number : 2 }}</td>
										<td ng-show="mostrar_calculos == 1" class="bg-grey">{{ row.peso_racimos_recusados | number : 2 }}</td>
										<td ng-show="mostrar_calculos == 1" class="bg-grey">{{ row.cajas_lb | number : 2 }}</td>
										<td ng-show="mostrar_calculos == 1" class="bg-grey">{{ row.merma_lb_cosec | number : 2 }}</td>
										<td ng-show="mostrar_calculos == 1" class="bg-grey">{{ row.merma_lb_proc | number : 2 }}</td>
										<td ng-show="mostrar_calculos == 1" class="bg-grey">{{ row.porcentaje_distribucion_merma | number : 2 }}%</td>
										<td>{{ row.convertidas | number : 0 }}</td>
										<td>{{ row.merma_cortada | number : 2 }}%</td>
										<td>{{ row.merma_procesada | number : 2 }}%</td>
										<td>{{ row.ratio_cortado }}</td>
										<td>{{ row.ratio_procesado }}</td>
										<td>{{ row.cajas_ha }}</td>
										<td>{{ row.cajas_ha_proy | number : 2 }}</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th ng-repeat="e in edades">{{ totales['edad_'+e] }}</th>
										<th>{{ totales.total_racimos_cosechados }}</th>
										<th>{{ totales.total_racimos_recusados }}</th>
										<th>{{ totales.total_racimos_procesados }}</th>
										<th ng-show="mostrar_calculos == 1" class="bg-grey">{{ data | sumOfValue : 'peso_racimos_cosechados' | number : 2 }}</th>
										<th>{{ totales_general['PESO RAC PROM (KG)'] | number: 2 }}</th>
										<th ng-show="mostrar_calculos == 1" class="bg-grey">{{ data | sumOfValue : 'peso_racimos_procesados' | number : 2 }}</th>
										<th ng-show="mostrar_calculos == 1" class="bg-grey">{{ data | sumOfValue : 'peso_racimos_recusados' | number : 2 }}</th>
										<th ng-show="mostrar_calculos == 1" class="bg-grey">{{ data | sumOfValue : 'cajas_lb' | number : 2 }}</th>
										<th ng-show="mostrar_calculos == 1" class="bg-grey">{{ data | sumOfValue : 'merma_lb_cosec' | number : 2 }}</th>
										<th ng-show="mostrar_calculos == 1" class="bg-grey">{{ data | sumOfValue : 'merma_lb_proc' | number : 2 }}</th>
										<th ng-show="mostrar_calculos == 1" class="bg-grey">{{ data | sumOfValue : 'porcentaje_distribucion_merma' | number : 2 }}%</th>
										<th>{{ totales_general['CAJAS 41.5'] | number : 0 }}</th>
										<th>{{ totales_general['MERMA CORTADA'] | number: 2  }}%</th>
										<th>{{ totales_general['MERMA PROCESADA'] | number: 2 }}%</th>
										<th>{{ totales_general['RATIO CORTADO'] | number: 2  }}</th>
										<th>{{ totales_general['RATIO PROCESADO'] | number: 2  }}</th>
										<th>{{ totales_general['CAJAS CONV / HA'] | number: 2 }}</th>
										<th>{{ data | avgOfValue : 'cajas_ha_proy' | number: 2 }}</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

    </div>
</div>