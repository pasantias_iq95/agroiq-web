<style>
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	table {
		cursor: pointer !important;
	}
	.anchoTable {
		width: 100px;
	}
    .delete  {
        background-color : #c8d8e8 !important;
    }
    .text-left {
        text-align: left !important;
		padding-left: 10px !important;
    }
    .text-right {
        text-align: right !important;
		padding-right: 10px !important;
    }

    .form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
    .highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>
<script src="assets/global/plugins/FileSaver.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>-->
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Produccion. Resumen Corte
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <input id="datepicker" class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly>
        </div>
    </div>

    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li ng-repeat="(key, value) in fincas" ng-class="{ 'active' : filters.id_finca == value.id }">
                <a ng-click="filters.id_finca = value.id; waitInit()">{{value.nombre}}</a>
            </li>
        </ul>
    </div>

	<div class="row">
		<div class="col-md-5">
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN POR EDAD</span>
				</div>
				<div class="portlet-body" >
					<div class="table-responsive" id="racimos_edad">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
                                    <th class="center-th">EDAD</th>
                                    <th class="center-th">CALIB 2DA</th>
                                    <th class="center-th">CALIB ULT</th>
                                    <th class="center-th">PROC</th>
                                    <th class="center-th">RECU</th>
                                    <th class="center-th">CORT</th>
                                    <th class="center-th">%</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="color in tabla.edades | orderObjectBy : 'edad'" ng-show="color.cosechados > 0">
                                    <td class="{{ color.class }}">{{ (color.edad > 0) ? color.edad : 'S/C' }}</td>
                                    <td>{{ (color.calibracion_segunda > 0) ? (color.calibracion_segunda  | number : 2) : '' }}</td>
                                    <td>{{ (color.calibracion_ultima > 0) ? (color.calibracion_ultima  | number : 2) : '' }}</td>
                                    <td>{{color.procesados}}</td>
                                    <td>{{color.recusados}}</td>
                                    <td>{{color.cosechados}}</td>
                                    <td>{{color.cosechados/totales.total_cosechados*100 | number : 2}}</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
                                    <td>{{ totales.edad | number: 2 }}</td>
                                    <td>{{ tabla.edades | avgOfValue : 'calibracion_segunda' | number: 2 }}</td>
                                    <td>{{ tabla.edades | avgOfValue : 'calibracion_ultima' | number: 2 }}</td>
                                    <td>{{totales.total_procesada}}</td>
                                    <td>{{totales.recusados}}</td>
                                    <td>{{totales.total_cosechados}}</td>
                                    <td></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>

            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN PROCESO</span>
				</div>
				<div class="portlet-body" >
                    <div class="table-responsive" id="resumen_produccion">
                        <table class="table table-bordered">
                            <tbody>
                                <tr ng-repeat="row in resumenProduccion">
                                    <td class="text-left">{{ row.name }}</td>
                                    <td class="text-right">{{ row.value | number : 2 }}</td>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
		</div>
        <div class="col-md-7">
            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">GUIA DE REMISION</span>
				</div>
				<div class="portlet-body" >
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table_4">
                            <thead>
                                <tr>
                                    <th class="center-th">GUIA</th>
                                    <th class="center-th">MARCA</th>
                                    <th class="center-th">UNIDADES</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat-start="row in cuadrarCajas" ng-hide="true">
                                    <td class="center-th">{{ detalle.guia }}</td>
                                    <td class="center-th">{{ row.marca }}</td>
                                    <td class="center-th">{{ detalle.valor | number }}</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="detalle in row.detalle">
                                    <td class="center-th">{{ detalle.guia }}</td>
                                    <td class="center-th">{{ row.marca }}</td>
                                    <td class="center-th">{{ detalle.valor | number }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th class="center-th">TOTAL:</th>
                                    <th></th>
                                    <th class="center-th">{{ cuadrarCajas | sumOfValue : 'balanza' }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>

            <div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption">RESUMEN CAJAS CONVERTIDAS A PRIMERA</span>
				</div>
				<div class="portlet-body" >
                    <div class="table-responsive" id="div_table_2">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="center-th">MARCA</th>
                                    <th class="center-th">CANTIDAD</th>
                                    <th class="center-th">PESO KG</th>
                                    <th class="center-th">PROM</th>
                                    <th class="center-th">MAX</th>
                                    <th class="center-th">MIN</th>
                                    <th class="center-th">PALLETS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="row in resumen">
                                    <td>{{ row.marca }}</td>
                                    <td>{{ row.conv | number }}</td>
                                    <td>{{ row.total_kg | number: 2 }}</td>
                                    <td>{{ row.promedio | number: 2 }}</td>
                                    <td>{{ row.maximo | number: 2 }}</td>
                                    <td>{{ row.minimo | number: 2 }}</td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <th class="center-th">{{ (resumen | sumOfValue:'conv') | number }}</th>
                                    <th class="center-th">{{ (resumen | sumOfValue:'total_kg') | number : 2 }}</th>
                                    <th class="center-th">{{ (resumen | avgOfValue:'promedio') | number : 2 }}</th>
                                    <th class="center-th">{{ (resumen | avgOfValue:'maximo') | number : 2 }}</th>
                                    <th class="center-th">{{ (resumen | avgOfValue:'minimo') | number : 2 }}</th>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
	</div>

</div>

<script src="componentes/FilterableSortableTable.js"></script>