 <!-- BEGIN PAGE HEAD-->
 <div ng-controller="membresias" id="membresias" ng-cloak>
    <div class="page-head">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Membresias</h1>
        </div>
        <!-- END PAGE TITLE -->
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE BREADCRUMB -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li ng-click="changeTab(0)">
            	<i class="icon-home"></i>
                <a >Listado de Usuarios</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li ng-click="changeTab(1)">
                <a class="active">Registro de Usuarios</a>
            </li>
        </ul>
    </div>
    <!-- END PAGE BREADCRUMB -->
    <!-- BEGIN PAGE BASE CONTENT -->
    <ul class="nav nav-tabs">
        <li class="active">
            <a  ng-click="changeTab(0)" data-toggle="tab"> Listado de usuarios </a>
        </li>
        <li>
            <a  ng-click="changeTab(1)" data-toggle="tab"> {{memberships.button[memberships.params.idUser]}} </a>
        </li>
    </ul>
    <!-- <div class="tab-content"> -->
		<div ng-include src="memberships.templatePath[memberships.type]" onload="loadExternal()">

		</div> 
	<!-- </div> -->
 </div>