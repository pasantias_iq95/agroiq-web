    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
    </style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="informe_calidad" ng-cloak>
     <h3 class="page-title"> 
          Merma
     </h3>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Merma</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
            <label ng-if="id_company != 7" class="btn red-thunderbird btn-outline btn-circle btn-sm" ng-click="convertKg()">
                {{labelLbKg}}
            </label>
            <label ng-if="id_company == 7 || id_company == 6" for="">
                Finca : 
            </label>
            <select ng-if="id_company == 7 || id_company == 6" onchange="cambiosFincas()" name="idFinca" id="idFinca" ng-model="calidad.params.idFinca" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in fincas" value="{{key}}">{{value}}</option>
            </select>
             <label ng-if="id_company == 6" for="">
                Tipo de Merma : 
            </label>
            <select ng-if="id_company == 6" onchange="cambiosMerma()" name="idMerma" id="idMerma" ng-model="calidad.params.idMerma" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in mermas" value="{{key}}">{{value}}</option>
            </select>
            <label for="">
                Palanca : 
            </label>
            <select onchange="cambiosPalanca()" name="palanca" id="palanca" ng-model="calidad.params.palanca" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in palancas" value="{{key}}">{{value}}</option>
            </select>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
     </div>

    <div class="page-bar" style="padding: 0; color: #888" ng-if="checkParams()">

            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-2" style="text-align: left">CLIENTE:</div>
                    <div class="col-md-4">{{param_cliente}}</div>
                    <div class="col-md-4" style="text-align: left">PESO REFERENCIAL:</div>
                    <div class="col-md-2">{{param_peso}}</div>
                </div>
                <div class="row" style="margin-top: 12px;">
                    <div class="col-md-2" style="text-align: left">MARCA:</div>
                    <div class="col-md-4">{{param_marca}}</div>
                    <div class="col-md-4" style="text-align: left"># CLÚSTER REF.:</div>
                    <div class="col-md-2">{{param_cluster}}</div>
                </div>
            </div>
            <div class="col-md-3"><img ng-src="{{param_logo}}" height="50" /></div>
    </div>

     <?php include("./views/tags_merma_".$this->session->agent_user.".php");?>           
     <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]" onload="loadExternal()">

     </div>  
</div>