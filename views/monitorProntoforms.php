<style>
	td, th {
		text-align : center;
	}
	.calendar-table td.available {
		background-color: green !important;
    	color: white !important;
	}
	.icalendar-table td.available:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>

<div ng-controller="controller">
	<h3 class="page-title"> 
		Monitor
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Monitor</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
		 	<ng-calendarapp search="search"></ng-calendarapp>
        </div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="portlet box green-haze">
				<div class="portlet-title">
					<span class="caption"> Por Usuario </span>
					<div class="tools">

					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th>Finca</th>
											<th>Usuario</th>
											<th ng-repeat="(name, values) in formularios">{{ name }}</th>
											<th>Comentarios</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="row in data">
											<td>{{ row.finca }}</td>
											<td>{{ row.usuario }}</td>
											<td ng-repeat="(name, values) in formularios">
												<span ng-show="!row[name]" class="font-red-thunderbird">
													<b>0</b>
												</span>
												<b ng-show="row[name]">{{ row[name] }}</b>
											</td>
											<td>
												<input type="text" class="form-control" ng-model="row.comentarios" enter="comentario(row)">
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<script src="componentes/FilterableSortableTable.js"></script>