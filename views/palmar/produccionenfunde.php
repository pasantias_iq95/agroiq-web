<style>
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.alginLeft {
		text-align: left !important;
		padding-top: 20px;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}
	td-hidden {
		padding: 1px;
	}
	
	.subtittleTr {
	    padding: 2px 25px;
	}

	.Titlespan {
		font-size: 21px;
		/* text-align: left; */
		float: left;
	}
	.td-small td {
		max-width : 60px;
	}

	@media print{
		body * { visibility: hidden; }
		.div2 * { visibility: visible; }
		.div2 { position: absolute; top: 40px; left: 30px; }
	}

    #semanal {
        background-color: #fff !important;
    }
	.form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Enfunde
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Enfoque</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label for="year">
                Año : 
            </label>
            <select name="year" id="year" ng-model="filters.year" style="margin: 2px;height: 36px;" ng-change="init()">
                <option ng-repeat="(key, value) in anios" value="{{value.anio}}" ng-selected="key == params.year">{{value.anio}}</option>
            </select>
            <label for="semana">
                Sem : 
            </label>
            <select name="semana" id="semana" ng-model="filters.semana" style="margin: 2px;height: 36px;" ng-change="init()">
                <option value="">TODAS</option>
                <option ng-repeat="(key, value) in semanas" value="{{value.sem}}" ng-selected="value.sem == filters.semana">{{value.sem}}</option>
            </select>
            <label for="variable">
                Var : 
            </label>
            <select name="variable" id="variable" ng-model="filters.variable" style="margin: 2px;height: 36px;" ng-change="changeVariable()">
                <option value="SEM">SEM</option>
                <option value="HA">HA</option>
            </select>
        </div>
    </div>

    <?php include 'views/marcel/tags_produccion_enfunde.php'; ?>
    
	<div id="contenedor" class="div2">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">ENFUNDE</span>
						<div class="tools">
                            <a class="btn green-jungle" style="height: 36px;" href="nuevoEnfunde">CREAR</a>
						</div>
					</div>
					<div class="portlet-body" >
						<div class="table-responsive table-scrollable">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
										<th class="center-th">ID</th>
										<th style="min-width : 150px;" class="center-th">ENFUNDADOR</th>
										<th class="center-th" ng-repeat="dia in dias">{{ dia.nombre }}</th>
										<th class="center-th">TOTAL</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat-start="row in tabla_enfundadores" class="td-small" ng-click="row.expanded = !row.expanded">
                                        <td>{{row.id}}</td>
										<td>{{row.nombre}}</td>
                                        <td ng-repeat="dia in dias">{{ (row[dia.nombre] > 0) ? (row[dia.nombre] | number: 0) : '' }}</td>
                                        <td>{{row.total}}</td>
                                    </tr>
                                    <tr ng-show="row.expanded">
                                        <td></td>
                                        <td>INICIAL</td>
                                        <td ng-repeat="dia in dias">{{ (row.inicial[dia.nombre]) ? (row.inicial[dia.nombre] | number: 0) : '' }}</td>
                                        <td></td>
                                    </tr>
                                    <tr ng-show="row.expanded">
                                        <td></td>
                                        <td>ENTREGADAS</td>
                                        <td ng-repeat="dia in dias">{{ (row.entregadas[dia.nombre]) ? (row.entregadas[dia.nombre] | number: 0) : '' }}</td>
                                        <td></td>
                                    </tr>
                                    <tr ng-show="row.expanded" ng-repeat-end="row">
                                        <td></td>
                                        <td>FINAL</td>
                                        <td ng-repeat="dia in dias">{{ (row.final[dia.nombre]) ? (row.final[dia.nombre] | number: 0) : '' }}</td>
                                        <td></td>
                                    </tr>
								</tbody>
								<tfoot>
									<tr>
                                        <th></th>
                                        <th></th>
                                        <th class="center-th" ng-repeat="dia in dias">{{ tabla_enfundadores | sumOfValue:dia.nombre }}</th>
                                        <th class="center-th">{{ tabla_enfundadores | sumOfValue:'total' }}</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO ENFUNDE POR SEMANA</span>
						<div class="tools pull-right">
							
						</div>
					</div>
					<div class="portlet-body">
						<div style="height: 400px;" id="semanal"></div>
					</div>
				</div>
			</div>
		</div>
        <div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO LOTE POR SEMANA</span>
						<div class="tools pull-right">
							
						</div>
					</div>
					<div class="portlet-body">
						<div style="height: 400px;" id="lote_semanal"></div>
					</div>
				</div>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green-haze">
                    <div class="portlet-title">
                        <span class="caption">HISTORICO</span>
                        <div class="tools pull-right">
                            
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="panel-group accordion" id="accordion3">
                            <!-- TAB 1 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a id="head_3_1" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="loadLotesSemanal(true)" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> LOTE POR SEMANA </a>
                                    </h4>
                                </div>
                                <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="col-md-12">
                                            <div id="lotes_semanal">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- TAB 2 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a id="head_3_2" class="accordion-toggle accordion-toggle-styled collapsed" ng-click="loadEnfundadoresSemanal(true)" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> ENFUNDADOR POR SEMANA </a>
                                    </h4>
                                </div>
                                <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body">
                                        <div style="">
                                            <div id="enfundadores_semanal">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- TAB 3 -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle accordion-toggle-styled collapsed" ng-click="loadEnfundadoresLote(true)" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> REPORTE SEMANAL ENFUNDADOR </a>
                                    </h4>
                                </div>
                                <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false">
                                    <div class="panel-body">
                                        <div class="btn-group pull-right" style="margin-right: 5px;">
                                            <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('table_enfundador_export')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="table-scrollable">
                                            <table class="table table-bordered" id="table_enfundador_export">
                                                <thead>
                                                    <tr>
                                                        <th class="center-th">ID</th>
                                                        <th class="center-th">ENFUNDADOR</th>
                                                        <th class="center-th" ng-repeat="lote in lotes | orderObjectBy : 'lote'">{{ lote.lote }}</th>
                                                        <th class="center-th">TOTAL</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr ng-repeat="row in enfundadores_lote">
                                                        <td>{{ row.id }}</td>
                                                        <td>{{ row.enfundador }}</td>
                                                        <td class="center-th" ng-repeat="lote in lotes | orderObjectBy : 'lote'">
                                                            {{ (row[lote.lote] > 0) 
                                                                    ? (filters.variable == 'SEM')
                                                                        ? (row[lote.lote] | number: 0) 
                                                                        : (row[lote.lote+'_ha'] | number: 2)
                                                                    : '' 
                                                            }}
                                                        </td>
                                                        <td>{{ (filters.variable == 'SEM') ? (row.total | number: 0) : (row.total_ha | number: 2) }}</td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td></td>
                                                        <td class="center-th">TOTAL</td>
                                                        <th class="center-th" ng-repeat="lote in lotes | orderObjectBy : 'lote'">
                                                            {{ (filters.variable == 'SEM') 
                                                                ? (enfundadores_lote | sumOfValue: lote.lote | number: 0)
                                                                : (enfundadores_lote | sumOfValue: (lote.lote+'_ha') | number: 2) }}
                                                        </th>
                                                        <th class="center-th">{{ enfundadores_lote | sumOfValue: 'total' | number: 0 }}</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
		<div class="row hide">
			<div class="col-md-3 col-sm-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">ENFUNDADOR SEMANA</span>
						<div class="tools pull-right">
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_3">
								<thead>
									<tr>
										<th>ENFUNDADOR</th>
										<th>FUNDAS</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat-start="row in enfundador_semana" ng-click="row.expand = !row.expand">
										<td class="alginLeft">{{ row.enfundador }}</td>
										<td>{{ row.usadas | number: 0 }}</td>
									</tr>
									<tr ng-repeat="sem in semanas" ng-show="row.expand" ng-repeat-end="row">
										<td class="{{ sem.class }}">{{ sem.sem }}</td>
										<td>{{ row.detalle[sem.sem] | number: 0 }}</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th class="alginLeft">TOTAL</th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">LOTE SEMANA</span>
						<div class="tools pull-right">
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_3">
								<thead>
									<tr>
										<th>LOTE</th>
										<th>FUNDAS</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="lote in lotes">
										<td class="{{ lote }}">{{ lote }}</td>
										<td></td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th>TOT</th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">SEMANA LOTE</span>
						<div class="tools pull-right">
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_3">
								<thead>
									<tr>
										<th>SEM</th>
										<th>FUNDAS</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="sem in semanas">
										<td class="{{ sem.color }}">{{ sem.sem }}</td>
										<td></td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th class="{{ sem.color }}">TOT</th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">SEMANA ENFUNDADOR</span>
						<div class="tools pull-right">
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_4">
								<thead>
									<tr>
										<th>SEM</th>
										<th>FUNDAS</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="sem in semanas">
										<td class="{{ sem.color }}">{{ sem.sem }}</td>
										<td>{{ sem.enfundador }}</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th class="{{ sem.color }}">TOT</th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>