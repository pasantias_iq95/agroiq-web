<?php
    $factory->path = 'marun/banano';
    $ProduccionRacimos = new $factory->ProduccionRacimos();
    $ProduccionRacimos2 = new $factory->Produccion2();
    $CorteDia = new $factory->ProduccionResumenCorte();

    $factory->path = '';
    $ProduccionCajas = new $factory->ProduccionCajas();
    
    $resumenLotes = $ProduccionRacimos->resumen();
    $resumenEdades = $ProduccionRacimos2->racimosEdad();
    $tags = (object) $ProduccionRacimos->tags()->tags;
    $last = $ProduccionRacimos->lastDay();
    $defectos = $ProduccionRacimos->defectos();
    $resumenCalibre = $ProduccionRacimos2->racimosCalibre();
    $resumenCajas = $ProduccionCajas->resumenMarca();
    $excedente = $ProduccionCajas->tablasDiferecias();
    $resumenCorte = $CorteDia->resumenProceso();
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
<style>
    .chart {
        height : 400px;
        text-align: center;
    }
</style>

<div class="row" id="pdf" ng-controller="produccion" style="width : 1200">

    <!-- RESUMEN POR EDAD -->
    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">RESUMEN POR EDAD</span>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive col-md-12" id="racimos_edad">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <? if($last->enabled['calibre_ultima']) : ?>
                                    <th class="text-center" colspan="2">PROMEDIO</th>
                                    <? else : ?>
                                    <th class="text-center" colspan="1">PROMEDIO</th>
                                    <? endif ; ?>
                                    <th class="text-center" colspan="3">RACIMOS</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th class="text-center">EDAD</th>
                                    <th class="text-center">CALIB 2DA</th>
                                    <? if($last->enabled['calibre_ultima']) : ?>
                                    <th class="text-center">CALIB ULT</th>
                                    <? endif ; ?>
                                    <th class="text-center" title="Procesados">PROC</th>
                                    <th class="text-center" title="Recusados">RECU</th>
                                    <th class="text-center" title="Cortados">CORT</th>
                                    <th class="text-center">%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?  foreach($resumenEdades->data as $row) : 
                                    if($row->procesados > 0 || $row->recusados > 0 || $row->edad == 'N/A'):
                                ?>
                                <tr>
                                    <td class="text-center <?= $row->class ?>"><?= $row->edad > 0 ? $row->edad : 'S/C' ?></td>
                                    <td class="text-center"><?= round($row->calibracion_segunda, 2) ?></td>
                                    <? if($last->enabled['calibre_ultima']) : ?>
                                    <td class="text-center"><?= round($row->calibracion_ultima, 2) ?></td>
                                    <? endif ; ?>
                                    <td class="text-center"><?= $row->procesados ?></td>
                                    <td class="text-center"><?= $row->recusados ?></td>
                                    <td class="text-center"><?= $row->cosechados ?></td>
                                    <td class="text-center"><?= round($row->cosechados/sumOfValue($resumenEdades->data, 'cosechados'), 2) ?></td>
                                </tr>
                                <?  endif ; 
                                    endforeach ; 
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="text-center"><?= round($tags->edad, 2) ?></td>
                                    <td class="text-center"><?= round($resumenLotes->totales['calibre_segunda'], 2) ?></td>
                                    <? if($last->enabled['calibre_ultima']) : ?>
                                    <td class="text-center"><?= round($resumenLotes->totales['calibre_ultima'], 2) ?></td>
                                    <? endif ; ?>
                                    <td class="text-center"><?= round(sumOfValue($resumenEdades->data, 'procesados'), 2) ?></td>
                                    <td class="text-center"><?= round(sumOfValue($resumenEdades->data, 'recusados'), 2) ?></td>
                                    <td class="text-center"><?= round(sumOfValue($resumenEdades->data, 'cosechados'), 2) ?></td>
                                    <td class="text-center">100</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- RESUMEN POR EDAD POR AGRUPACION CALIBRE -->
    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">CANTIDAD DE RACIMOS COSECHADOS</span>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive col-md-12" id="racimos_edad">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th class="text-center" colspan="<?= count($resumenCalibre->calibres) ?>">AGRUPACION POR CALIBRE</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th class="text-center">EDAD</th>
                                    <? foreach($resumenCalibre->calibres as $c): ?>
                                    <th class="text-center"><?= $c ?></th>
                                    <? endforeach ; ?>
                                    <th class="text-center">TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?  foreach($resumenCalibre->data as $row) : 
                                    if($row->procesados > 0 || $row->recusados > 0 || $row->edad == 'N/A'):
                                ?>
                                <tr>
                                    <td class="text-center <?= $row->class ?>"><?= $row->edad > 0 ? $row->edad : 'S/C' ?></td>
                                    <? foreach($resumenCalibre->calibres as $c): ?>
                                    <td class="text-center"><?= $row->calibres[$c] ? $row->calibres[$c] : '' ?></td>
                                    <? endforeach ; ?>
                                    <td class="text-center"><?= $row->cosechados ?></td>
                                </tr>
                                <?  endif ; 
                                    endforeach ; 
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td class="text-center"><?= round($tags->edad, 2) ?></td>
                                    <? foreach($resumenCalibre->calibres as $c): ?>
                                    <td class="text-center"><?= $resumenCalibre->totales[$c] ?></td>
                                    <? endforeach ; ?>
                                    <td class="text-center"><?= round(sumOfValue($resumenEdades->data, 'cosechados'), 2) ?></td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- RESUMEN POR LOTE -->
    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">RESUMEN POR LOTE</span>
            </div>
            <div class="portlet-body" >
                <div class="table-responsive" id="promedios_lotes">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center"></th>
                                <th class="text-center" colspan="<?= count($resumenLotes->edades) ?>" ng-if="edades.length > 0">EDAD</th>
                                <th class="text-center" colspan="4">RACIMOS</th>
                                <th class="text-center" colspan="4">PROMEDIO</th>
                            </tr>
                            <tr>
                                <th class="text-center">LOTE</th>
                                <? foreach($resumenLotes->edades as $e) : ?>
                                <td class="text-center <?= $e->class ?>"><?= $e->edad > 0 ? $e->edad : 'S/C' ?></td>
                                <? endforeach ; ?>
                                <th class="text-center">COSE</th>
                                <th class="text-center">PROC</th>
                                <th class="text-center">RECU</th>
                                <th class="text-center">PESO PROM 
                                    <a ng-click="toggle_peso_prom_bruto = !toggle_peso_prom_bruto">
                                        <small>Bruto</small>
                                    </a> 
                                </th>
                                <th class="text-center">CALIB 2DA</th>
                                <th class="text-center" ng-show="enabled.calibre_ultima">CALIB ULT</th>
                                <th class="text-center">MANOS</th>
                                <th class="text-center" ng-show="enabled.dedos">DEDOS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? foreach($resumenLotes->data as $row) : ?>
                            <tr>
                                <td class="text-center"><?= $row->lote ?></td>
                                <? foreach($resumenLotes->edades as $e) : ?>
                                <td class="text-center"><?= $row->{"edad_".($e->edad > 0 ? $e->edad : 'S/C')} ?></td>
                                <? endforeach ; ?>
                                <td class="text-center"><?= $row->cosechados = ($row->procesados + $row->recusados ) ?></td>
                                <td class="text-center"><?= $row->procesados ?></td>
                                <td class="text-center"><?= $row->recusados ?></td>
                                <td class="text-center"><?= $row->peso ?></td>
                                <td class="text-center"><?= $row->calibre_segunda > 0 ? $row->calibre_segunda : '' ?></td>
                                <td class="text-center" ng-show="enabled.calibre_ultima"><?= $row->calibre_ultima > 0 ? $row->calibre_ultima : '' ?></td>
                                <td class="text-center"><?= $row->manos > 0 ? $row->manos : '' ?></td>
                                <td class="text-center" ng-show="enabled.dedos"><?= $row->dedos > 0 ? $row->dedos : '' ?></td>
                            </tr>
                            <? endforeach ; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>TOTAL</th>
                                <? foreach($resumenLotes->edades as $e) : ?>
                                <th class="text-center"><?= sumOfValue($resumenLotes->data, 'edad_'.($e->edad > 0 ? $e->edad : 'S/C')) ?></th>
                                <? endforeach ; ?>
                                <th class="text-center"><?= sumOfValue($resumenLotes->data, 'cosechados') ?></th>
                                <th class="text-center"><?= sumOfValue($resumenLotes->data, 'procesados') ?></th>
                                <th class="text-center"><?= sumOfValue($resumenLotes->data, 'recusados') ?></th>
                                <th class="text-center"><?= round($resumenLotes->totales['peso_prom'], 2) ?></th>
                                <th class="text-center"><?= round($resumenLotes->totales['calibre_segunda'], 2) ?></th>
                                <th class="text-center" ng-show="enabled.calibre_ultima"><?= round($resumenLotes->totales['calibre_ultima'], 2) ?></th>
                                <th class="text-center"><?= round($resumenLotes->totales['manos_prom'], 2) ?></th>
                                <th class="text-center" ng-show="enabled.dedos"><?= round($resumenLotes->totales['dedos_prom'], 2) ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">PESO RACIMOS VS MANOS POR RACIMO (lb)</span>
            </div>
            <div class="portlet-body">
                <div class="chart" id="grafica-manos-peso"></div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">PESO POR RACIMO</span>
            </div>
            <div class="portlet-body">
                <div class="chart" id="grafica-peso-racimo"></div>
            </div>
        </div>
    </div>

    <!-- RESUMEN RECUSADOS -->
    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">RESUMEN DE RECUSADOS</span>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="table-responsive" id="racimos_recusados">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>CAUSA</th>
                                    <th class="text-center">CANTIDAD</th>
                                    <th class="text-center">PORCENTAJE</th>
                                </tr>
                            </thead>
                            <tbody>
                                <? foreach($defectos->data as $row): ?>
                                <tr>
                                    <td><?= $row->causa ?></td>
                                    <td class="text-center"><?= $row->cantidad ?></td>
                                    <td class="text-center"><?= $row->porcentaje ?></td>
                                </tr>
                                <? endforeach ; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>RECUSADOS</th>
                                    <th class="text-center"><?= sumOfValue($defectos->data, 'cantidad') ?></th>
                                    <th class="text-center">100</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-6 hide">
                        <div id="grafica-defectos" class="chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">RACIMOS POR NUMERO DE MANOS</span>
            </div>
            <div class="portlet-body">
                <div class="chart" id="grafica-peso-mano"></div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">RESUMEN CAJAS</span>
                <div class="tools">
                    
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive" id="div_table_2">
                    <table class="table table-striped table-bordered table-hover" id="table_2">
                        <thead>
                            <tr>
                                <th class="text-center">TIPO</th>
                                <th class="text-center">MARCA</th>
                                <th class="text-center">CANTIDAD</th>
                                <th class="text-center">
                                    <small>(<?= $resumenCajas->convertidas ?> lb)</small>
                                    <br>
                                    CONV
                                    <a title="Mostrar decimales" ng-click="mostrarDecimalesConv = !mostrarDecimalesConv">...</a>
                                </th>
                                <th class="text-center">TOTAL (lb)</th>
                                <th class="text-center">PROM</th>
                                <th class="text-center">MAX</th>
                                <th class="text-center">MIN</th>
                                <th class="text-center">DESV</th>
                                <th class="text-center">CANT.<br>BAJO PESO</th>
                                <th class="text-center">%<br>BAJO PESO</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <? foreach($resumenCajas->data as $row) : ?>
                            <tr>
                                <td><?= $row->tipo ?></td>
                                <td>
                                    <? if($row->id_marca > 0) : ?>
                                    <span><?= $row->marca ?></span>
                                    <? else : ?>
                                    <span class="font-red-thunderbird" 
                                        title="Favor de registrar esta marca en la base de datos" >

                                        <u><?= $row->marca ?></u>
                                    </span>
                                    <? endif ; ?>
                                </td>
                                <td class="text-center"><?= $row->cantidad ?></td>
                                <td class="text-center"><?= round($row->conv, 0) ?></td>
                                <td class="text-center"><?= $row->total_kg ?></td>
                                <td class="text-center">
                                    <? if(!$row->umbral_minimo > 0) : ?>
                                    <span>
                                        <?= round($row->promedio, 2) ?> <span class="badge badge-info" title="Este promedio no tiene una restriccion de peso minimo, por lo cual puede ser mas bajo de lo normal"><i class="fa fa-info"></i></span>
                                    </span>
                                    <? else : ?>
                                    <span>
                                        <?= round($row->promedio, 2) ?>
                                    </span>
                                    <? endif ; ?>
                                </td>
                                <td class="text-center"><?= $row->maximo ?></td>
                                <td class="text-center"><?= $row->minimo ?></td>
                                <td class="text-center"><?= $row->desviacion ?></td>
                                <td class="text-center">
                                    <? if($row->umbral_minimo > 0 && $row->minimo < $row->umbral_minimo) : ?>
                                    <span>
                                        <span title="Cajas por debajo del umbral : <?= $row->umbral_minimo ?>"><?= $row->cajas_bajo_peso ?></span>
                                    </span>
                                    <? endif ; ?>
                                </td>
                                <td class="text-center">
                                    <? if($row->umbral_minimo > 0 && $row->minimo < $row->umbral_minimo) : ?>
                                    <span>
                                        <?= round($row->cajas_bajo_peso / $row->cantidad * 100, 2) ?> %
                                    </span>
                                    <? endif ; ?>
                                </td>
                                <td class="text-center">
                                    <? if($row->umbral_minimo > 0 && $row->minimo < $row->umbral_minimo) : ?>
                                    <span>
                                        <button class="btn btn-primary" ng-click="procesarMarca(row.id_marca)">
                                            Procesar
                                        </button>
                                    </span>
                                    <? endif ; ?>
                                </td>
                            </tr>
                            <? endforeach ; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>TOTAL</th>
                                <th></th>
                                <th class="text-center"><?= sumOfValue($resumenCajas->data, 'cantidad') ?></th>
                                <th class="text-center"><?= round(sumOfValue($resumenCajas->data, 'conv'), 2) ?></th>
                                <th class="text-center"><?= sumOfValue($resumenCajas->data, 'total_kg') ?></th>
                                <th class="text-center"><?= round(avgOfValue($resumenCajas->data, 'promedio'), 2) ?></th>
                                <th class="text-center"><?= round(avgOfValue($resumenCajas->data, 'maximo'), 2) ?></th>
                                <th class="text-center"><?= round(avgOfValue($resumenCajas->data, 'minimo'), 2) ?></th>
                                <th class="text-center"><?= round(avgOfValue($resumenCajas->data, 'desviacion'), 2) ?></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">EXCEDENTE <span class="badge badge-info" title="Para visualizar es necesario configurar marcas con Máximo y Mínimo"><i class="fa fa-info"></i></span> </span>
                <div class="actions">
                    
                </div>
            </div>
            <div class="portlet-body" id="tablas">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">MARCA</th>
                                <th class="text-center">TOTAL (lb) </th>
                                <th class="text-center">CAJAS CONV</th>
                                <th class="text-center">DOLARES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <? foreach($excedente->tablas as $row): ?>
                            <tr>
                                <td><?= $row->marca ?></td>
                                <td><?= $row->kg_diff ?></td>
                                <td><?= $row->cajas ?></td>
                                <td><?= $row->dolares ?></td>
                            </tr>
                            <? endforeach ; ?>
                            <tr>
                                <th></th>
                                <th class="text-center"><?= round(sumOfValue($excedente->tablas, 'kg_diff'), 2) ?></th>
                                <th class="text-center"><?= round(sumOfValue($excedente->tablas, 'cajas'), 2) ?></th>
                                <th class="text-center"><?= round(sumOfValue($excedente->tablas, 'dolares'), 2) ?></th>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption"></span>
                <div class="actions">
                    
                </div>
            </div>
            <div class="portlet-body">
                <div class="row" id="barras">
                    <div ng-repeat="marca in marcasBarras" class="col-md-12" style="padding-bottom: 50px;">
                        <div class="col-md-12">
                            <input type="text" class="input-sm" name="{{ marca.marca }}" ng-model="rangos[marca.marca]"> Rango
                        </div>
                        <div class="col-md-12 font-red" ng-if="!marca.id_marca || !marca.maximo || !marca.minimo">
                            <label class="control-label">Encontramos estos problemas</label>
                            <ol>
                                <li ng-if="!marca.id_marca">La marca no esta registrada</li>
                                <li ng-if="!marca.maximo">No existe el peso máximo</li>
                                <li ng-if="!marca.minimo">No existe el peso mínimo</li>
                            </ol>
                        </div>
                        <div class="col-md-6">
                            <div id="barras_{{$index}}" class="chart"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="pastel_{{$index}}" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <span class="caption">RESUMEN PROCESO</span>
                <div class="actions">
                    
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <? foreach($resumenCorte->data as $row): ?>
                            <tr>
                                <td class="text-left"><?= $row['name'] ?></td>
                                <td class="text-right"><?= round($row['value'], 2) ?></td>
                            </tr>
                            <? endforeach ; ?>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
