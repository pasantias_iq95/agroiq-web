<?php
    $factory->path = 'marun/banano';
    $ProduccionRacimos = new $factory->ProduccionRacimos();
    $ProduccionRacimos2 = new $factory->Produccion2();
    $CorteDia = new $factory->ProduccionResumenCorte();

    $factory->path = '';
    $ProduccionCajas = new $factory->ProduccionCajas();
    
    $resumenLotes = $ProduccionRacimos->resumen();
    $resumenEdades = $ProduccionRacimos2->racimosEdad();
    $tags = (object) $ProduccionRacimos->tags()->tags;
    $last = $ProduccionRacimos->lastDay();
    $defectos = $ProduccionRacimos->defectos();
    $resumenCalibre = $ProduccionRacimos2->racimosCalibre();
    $resumenCajas = $ProduccionCajas->resumenMarca();
    $excedente = $ProduccionCajas->tablasDiferecias();
    $resumenCorte = $CorteDia->resumenProceso();
    $graficasPasteles = $ProduccionCajas->graficasBarras();


    # CONTENIDO RESUMEN EDAD
    $contentResumenEdad = "";
    foreach($resumenEdades->data as $row) : 
        if($row->procesados > 0 || $row->recusados > 0 || $row->edad == 'N/A'):
            $contentResumenEdad .= "
                <tr>
                    <td class=\"text-center {$row->class}\">". ($row->edad > 0 ? $row->edad : 'S/C') ."</td>
                    <td class=\"text-center\">". round($row->calibracion_segunda, 2) ."</td>".
                    ( ($last->enabled['calibre_ultima']) 
                        ? "<td class=\"text-center\">". round($row->calibracion_ultima, 2) ."</td>"
                        : ""
                    )."
                    <td class=\"text-center\">{$row->procesados}</td>
                    <td class=\"text-center\">{$row->recusados}</td>
                    <td class=\"text-center\">{$row->cosechados}</td>
                    <td class=\"text-center\">". round($row->cosechados/sumOfValue($resumenEdades->data, 'cosechados'), 2) ."</td>
                </tr>
            ";
        endif ; 
    endforeach ;
    
    $columnasEdades = "";
    foreach($resumenLotes->edades as $e) :
        $columnasEdades .= "<td class=\"text-center {$e->class}\">". ($e->edad > 0 ? $e->edad : 'S/C') ."</td>";
    endforeach ;

    # CONTENIDO RESUMEN POR CALIBRE
    $contentResumenCalibre = "";
    foreach($resumenCalibre->data as $row) : 
        if($row->procesados > 0 || $row->recusados > 0 || $row->edad == 'N/A'):
            $contentResumenCalibre .= "
                <tr>
                    <td class=\"text-center {$row->class}\">". ($row->edad > 0 ? $row->edad : 'S/C') ."</td>
            ";

            foreach($resumenCalibre->calibres as $c):
                $contentResumenCalibre .= "
                    <td class=\"text-center\">". ($row->calibres[$c] ? $row->calibres[$c] : '') ."</td>
                ";
            endforeach ;
            
            $contentResumenCalibre .= "
                    <td class=\"text-center\">{$row->cosechados}</td>
                </tr>
            ";
        endif ; 
    endforeach ; 

    $columnasCalibre = "";
    foreach($resumenCalibre->calibres as $c):
        $columnasCalibre .= "<th class=\"text-center\">{$c}</th>";
    endforeach ; 

    $footerCalibre = "";
    foreach($resumenCalibre->calibres as $c):
        $footerCalibre .= "<td class=\"text-center\">{$resumenCalibre->totales[$c]}</td>";
    endforeach ;

    # CONTENIDO RESUMEN PROCESO
    $contentResumenProceso = "";
    foreach($resumenCorte->data as $row): 
        $contentResumenProceso .= "
            <tr>
                <td class=\"text-left\">{$row['name']}</td>
                <td class=\"text-right\">". round($row['value'], 2) ."</td>
            </tr>
        ";
    endforeach ;

    # CONTENIDO RESUMEN LOTE
    $contentResumenLote = "";
    foreach($resumenLotes->data as $row) :
        $contentResumenLote .= "
            <tr>
                <td class=\"text-center\">{$row->lote}</td>
        ";

        foreach($resumenLotes->edades as $e) :
            $contentResumenLote .= "
                <td class=\"text-center\">". ($row->{"edad_".($e->edad > 0 ? $e->edad : 'S/C')}) ."</td>
            ";
        endforeach ;
            
        $contentResumenLote .= "
                <td class=\"text-center\">".($row->cosechados = ($row->procesados + $row->recusados ))."</td>
                <td class=\"text-center\">{$row->procesados}</td>
                <td class=\"text-center\">{$row->recusados}</td>
                <td class=\"text-center\">{$row->peso}</td>
                <td class=\"text-center\">". ($row->calibre_segunda > 0 ? $row->calibre_segunda : '') ."</td>
                <td class=\"text-center\" ng-show=\"enabled.calibre_ultima\">". ($row->calibre_ultima > 0 ? $row->calibre_ultima : '') ."</td>
                <td class=\"text-center\">". ($row->manos > 0 ? $row->manos : '') ."</td>
                <td class=\"text-center\" ng-show=\"enabled.dedos\">". ($row->dedos > 0 ? $row->dedos : '') ."</td>
            </tr>
        ";
    endforeach ;

    $footerEdades = "";
    foreach($resumenLotes->edades as $e) :
        $footerEdades .= "<th class=\"text-center\">". sumOfValue($resumenLotes->data, 'edad_'.($e->edad > 0 ? $e->edad : 'S/C')) ."</th>";
    endforeach ;

    # CONTENIDO CAJAS

    $contentCajas = "";
    foreach($resumenCajas->data as $row) :
        $contentCajas .= "
            <tr>
                <td>{$row->tipo}</td>
                <td>
                    ".
                    ( ($row->id_marca > 0) 
                        ? "<span>{$row->marca}</span>"
                        : "
                            <span class=\"font-red-thunderbird\" 
                                title=\"Favor de registrar esta marca en la base de datos\" >

                                <u>{$row->marca}</u>
                            </span>
                        "
                    )."
                </td>
                <td class=\"text-center\">{$row->cantidad}</td>
                <td class=\"text-center\">". round($row->conv, 0) ."</td>
                <td class=\"text-center\">{$row->total_kg}</td>
                <td class=\"text-center\">
                    ".
                    ((!$row->umbral_minimo > 0) 
                        ? "
                            <span>
                                ". round($row->promedio, 2) ." <span class=\"badge badge-info\" title=\"Este promedio no tiene una restriccion de peso minimo, por lo cual puede ser mas bajo de lo normal\"><i class=\"fa fa-info\"></i></span>
                            </span>
                        "
                        : "
                            <span>
                                ". round($row->promedio, 2) ."
                            </span>
                        "
                    )."
                </td>
                <td class=\"text-center\">{$row->maximo}</td>
                <td class=\"text-center\">{$row->minimo}</td>
                <td class=\"text-center\">{$row->desviacion}</td>
                <td class=\"text-center\">
                    ".
                    ( ($row->umbral_minimo > 0 && $row->minimo < $row->umbral_minimo) 
                        ? "
                            <span>
                                <span title=\"Cajas por debajo del umbral : {$row->umbral_minimo}\">{$row->cajas_bajo_peso}</span>
                            </span>
                        "
                        : ""
                    )."
                </td>
                <td class=\"text-center\">
                    ".
                    ( ($row->umbral_minimo > 0 && $row->minimo < $row->umbral_minimo) 
                        ? "
                            <span>
                                ". round($row->cajas_bajo_peso / $row->cantidad * 100, 2) ." %
                            </span>
                        "
                        : ""
                    )."
                </td>
                <td class=\"text-center\">
                    ". 
                    ( ($row->umbral_minimo > 0 && $row->minimo < $row->umbral_minimo) 
                        ? "
                            <span>
                                <button class=\"btn btn-primary\">
                                    Procesar
                                </button>
                            </span>
                        "
                        : ""
                    )."
                </td>
            </tr>
        ";
    endforeach ;

    # CONTENIDO EXCEDENTE
    
    $contentExcedente = "";
    foreach($excedente->tablas as $row): 
        $contentExcedente .= "
            <tr>
                <td>{$row->marca}</td>
                <td>{$row->kg_diff}</td>
                <td>{$row->cajas}</td>
                <td>{$row->dolares}</td>
            </tr>
        ";
    endforeach ;

    # CONTENIDO RECUSADOS

    $contentRecusados = "";
    foreach($defectos->data as $row):
        $contentRecusados .= "
            <tr>
                <td>{$row->causa}</td>
                <td class=\"text-center\">{$row->cantidad}</td>
                <td class=\"text-center\">{$row->porcentaje}</td>
            </tr>
        ";
    endforeach ;

    # URL

    $url1 = "pdf/produccion-dia/{$_GET['finca']}_{$_GET['fecha_inicial']}/grafica-manos-peso.jpg";
    $url2 = "pdf/produccion-dia/{$_GET['finca']}_{$_GET['fecha_inicial']}/grafica-peso-racimo.jpg";
    $url3 = "pdf/produccion-dia/{$_GET['finca']}_{$_GET['fecha_inicial']}/grafica-peso-mano.jpg";

    # CONTENIDO PASTELES

    $contentPasteles = "";
    foreach($graficasPasteles->marcas as $index => $row) :
        $url_barras = "pdf/produccion-dia/{$_GET['finca']}_{$_GET['fecha_inicial']}/barras_{$index}.jpg";
        $url_pastel = "pdf/produccion-dia/{$_GET['finca']}_{$_GET['fecha_inicial']}/pastel_{$index}.jpg";

        $contentPasteles .= "
            <div class=\"col-md-12 grid-container\" style=\"padding-bottom: 50px;\">
                ". ( (!$row->id_marca || !$row->maximo || !$row->minimo) 
                    ? "
                        <div class=\"col-md-12 font-red\">
                            <label class=\"control-label\">Encontramos estos problemas</label>
                            <ol>
                                ". ( (!$row->id_marca) 
                                    ? "<li>La marca no esta registrada</li>"
                                    : ""
                                ). ( (!$row->maximo) 
                                    ? "<li>No existe el peso máximo</li>"
                                    : ""
                                ). ( (!$row->minimo) 
                                    ? "<li>No existe el peso mínimo</li>"
                                    : ""
                                )."
                            </ol>
                        </div>
                    "
                    : ""
                )."
                <div class=\"col-md-6\">
                    <div id=\"barras_{$index}\" class=\"chart\">
                        <img src=\"{$url_barras}\" alt=\"\" srcset=\"\">
                    </div>
                </div>
                <div class=\"col-md-6\">
                    <div id=\"pastel_{$index}\" class=\"chart\">
                        <img src=\"{$url_pastel}\" alt=\"\" srcset=\"\">
                    </div>
                </div>
            </div>
        ";
    endforeach ;

    $html = "
        <!DOCTYPE html>
        <head>
            <style>
                .chart, .chart img {
                    height : 250px;
                    width : 330px;
                }
                .text-center {
                    text-align : center;
                }
                .grid-container, .row {
                    display: grid;
                    grid-template-columns : repeat(12, 1fr);
                    margin-left : 0 !important;
                    margin-right : 0 !important;
                }
                .col-md-12 {
                    grid-column : span 12;
                    width : unset !important;
                    padding-left: 15px;
                    padding-right: 15px;
                }
                .col-md-6 {
                    grid-column : span 6;
                    width : unset !important;
                    padding-left: 15px;
                    padding-right: 15px;
                }
                .col-md-4 {
                    grid-column : span 4;
                    width : unset !important;
                    padding-left: 15px;
                    padding-right: 15px;
                }
                @media print {
                    .unbreakable {
                        page-break-after : avoid !important;
                    }
                }
                .break {
                    page-break-after: always;
                }
                .hoja {
                    min-height: 930px;
                    min-width: 738px;
                }
                .table {
                    width : 100%;
                }
                .table-responsive {
                    overflow-x: auto;
                    min-height: .01%;
                }
                .portlet.box>.portlet-title {
                    border-bottom: 0;
                    padding: 0 10px;
                    margin-bottom: 0;
                    color: #fff;
                }
                .portlet.box.green-haze>.portlet-title, .portlet.green-haze, .portlet>.portlet-body.green-haze {
                    background-color: #44b6ae;
                }
                .portlet.box>.portlet-body {
                    background-color: #fff;
                    padding: 15px;
                }
                .portlet.box.green-haze {
                    border: 1px solid #67c6bf;
                    border-top: 0;
                }
                .portlet.box.green-haze>.portlet-title, .portlet.green-haze, .portlet>.portlet-body.green-haze {
                    background-color: #44b6ae;
                }
                .portlet.box {
                    padding: 0!important;
                }
                .portlet {
                    margin-top: 0;
                    margin-bottom: 25px;
                    padding: 0;
                    border-radius: 4px;
                }
                .portlet.box.green-haze>.portlet-title>.caption, .portlet.box.green-haze>.portlet-title>.caption>i {
                    color: #FFF;
                }
                .portlet.box>.portlet-title>.caption {
                    padding: 11px 0 9px;
                }
                .portlet>.portlet-title>.caption {
                    
                    display: inline-block;
                    font-size: 18px;
                    line-height: 18px;
                    padding: 10px 0;
                }
                .break {
                    page-break-after: always;
                }
                @media print {
                    .break {
                        page-break-after: always;
                    }
                }
                .table-bordered, .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
                    border: 1px solid #e7ecf1;
                }
            </style>
        </head>
        <body>
            <div class=\"row pdf grid-container break\">
                <!-- RESUMEN POR EDAD -->
                <div class=\"col-md-6\" style=\"grid-column-start : 1; grid-column-end : 7;\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">RESUMEN POR EDAD</span>
                        </div>
                        <div class=\"portlet-body\">
                            <table class=\"table table-striped table-bordered table-hover\">
                                <thead>
                                    <tr>
                                        <th></th> ".
                                        ( ($last->enabled['calibre_ultima']) 
                                            ? "<th class=\"text-center\" colspan=\"2\">PROMEDIO</th>"
                                            : "<th class=\"text-center\" colspan=\"1\">PROMEDIO</th>"
                                        )."
                                        <th class=\"text-center\" colspan=\"3\">RACIMOS</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th class=\"text-center\">EDAD</th>
                                        <th class=\"text-center\">CALIB 2DA</th>".
                                        ( ($last->enabled['calibre_ultima'])
                                            ? "<th class=\"text-center\">CALIB ULT</th>"
                                            : ""
                                        )."
                                        <th class=\"text-center\" title=\"Procesados\">PROC</th>
                                        <th class=\"text-center\" title=\"Recusados\">RECU</th>
                                        <th class=\"text-center\" title=\"Cortados\">CORT</th>
                                        <th class=\"text-center\">%</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {$contentResumenEdad}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class=\"text-center\">". round($tags->edad, 2) ."</td>
                                        <td class=\"text-center\">". round($resumenLotes->totales['calibre_segunda'], 2) ."</td>".
                                        ( ($last->enabled['calibre_ultima']) 
                                            ? "<td class=\"text-center\">". round($resumenLotes->totales['calibre_ultima'], 2) ."</td>"
                                            : ""
                                        )."
                                        <td class=\"text-center\">". round(sumOfValue($resumenEdades->data, 'procesados'), 2) ."</td>
                                        <td class=\"text-center\">".round(sumOfValue($resumenEdades->data, 'recusados'), 2) ."</td>
                                        <td class=\"text-center\">". round(sumOfValue($resumenEdades->data, 'cosechados'), 2) ."</td>
                                        <td class=\"text-center\">100</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- RESUMEN POR EDAD POR AGRUPACION CALIBRE -->
                <div class=\"col-md-6\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">CANTIDAD DE RACIMOS COSECHADOS</span>
                        </div>
                        <div class=\"portlet-body\">
                            <table class=\"table table-striped table-bordered table-hover\">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th class=\"text-center\" colspan=\"". count($resumenCalibre->calibres) ."\">AGRUPACION POR CALIBRE</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th class=\"text-center\">EDAD</th>
                                        {$columnasCalibre}
                                        <th class=\"text-center\">TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {$contentResumenCalibre}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class=\"text-center\">". round($tags->edad, 2) ."</td>
                                        {$footerCalibre}
                                        <td class=\"text-center\">". round(sumOfValue($resumenEdades->data, 'cosechados'), 2) ."</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- RESUMEN POR LOTE -->
                <div class=\"col-md-12\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">RESUMEN POR LOTE</span>
                        </div>
                        <div class=\"portlet-body\" >
                            <div class=\"table-responsive\" id=\"promedios_lotes\">
                                <table class=\"table table-striped table-bordered table-hover\">
                                    <thead>
                                        <tr>
                                            <th class=\"text-center\"></th>
                                            <th class=\"text-center\" colspan=\"". count($resumenLotes->edades) ."\">EDAD</th>
                                            <th class=\"text-center\" colspan=\"4\">RACIMOS</th>
                                            <th class=\"text-center\" colspan=\"4\">PROMEDIO</th>
                                        </tr>
                                        <tr>
                                            <th class=\"text-center\">LOTE</th>
                                            {$columnasEdades}
                                            <th class=\"text-center\">COSE</th>
                                            <th class=\"text-center\">PROC</th>
                                            <th class=\"text-center\">RECU</th>
                                            <th class=\"text-center\">PESO PROM 
                                                <a ng-click=\"toggle_peso_prom_bruto = !toggle_peso_prom_bruto\">
                                                    <small>Bruto</small>
                                                </a> 
                                            </th>
                                            <th class=\"text-center\">CALIB 2DA</th>
                                            <th class=\"text-center\" ng-show=\"enabled.calibre_ultima\">CALIB ULT</th>
                                            <th class=\"text-center\">MANOS</th>
                                            <th class=\"text-center\" ng-show=\"enabled.dedos\">DEDOS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {$contentResumenLote}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>TOTAL</th>
                                            {$footerEdades}
                                            <th class=\"text-center\">". sumOfValue($resumenLotes->data, 'cosechados') ."</th>
                                            <th class=\"text-center\">". sumOfValue($resumenLotes->data, 'procesados') ."</th>
                                            <th class=\"text-center\">". sumOfValue($resumenLotes->data, 'recusados') ."</th>
                                            <th class=\"text-center\">". round($resumenLotes->totales['peso_prom'], 2) ."</th>
                                            <th class=\"text-center\">". round($resumenLotes->totales['calibre_segunda'], 2) ."</th>
                                            <th class=\"text-center\" ng-show=\"enabled.calibre_ultima\">". round($resumenLotes->totales['calibre_ultima'], 2) ."</th>
                                            <th class=\"text-center\">". round($resumenLotes->totales['manos_prom'], 2) ."</th>
                                            <th class=\"text-center\" ng-show=\"enabled.dedos\">". round($resumenLotes->totales['dedos_prom'], 2) ."</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-6\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">PESO RACIMOS VS MANOS POR RACIMO (lb)</span>
                        </div>
                        <div class=\"portlet-body\">
                            <div class=\"chart\" id=\"grafica-manos-peso\">
                                <img src=\"{$url1}\" alt=\"\" srcset=\"\">
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-6\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">PESO POR RACIMO</span>
                        </div>
                        <div class=\"portlet-body\">
                            <div class=\"chart\" id=\"grafica-peso-racimo\">
                                <img src=\"{$url2}\" alt=\"\" srcset=\"\">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"row pdf grid-container break\">
                <!-- RESUMEN RECUSADOS -->
                <div class=\"col-md-6\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">RESUMEN DE RECUSADOS</span>
                        </div>
                        <div class=\"portlet-body\">
                            <div class=\"table-responsive\" id=\"racimos_recusados\">
                                <table class=\"table table-bordered\">
                                    <thead>
                                        <tr>
                                            <th>CAUSA</th>
                                            <th class=\"text-center\">CANTIDAD</th>
                                            <th class=\"text-center\">PORCENTAJE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {$contentRecusados}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>RECUSADOS</th>
                                            <th class=\"text-center\">". sumOfValue($defectos->data, 'cantidad') ."</th>
                                            <th class=\"text-center\">100</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-6\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">RACIMOS POR NUMERO DE MANOS</span>
                        </div>
                        <div class=\"portlet-body\">
                            <div class=\"chart\" id=\"grafica-peso-mano\">
                                <img src=\"{$url3}\" alt=\"\" srcset=\"\">
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-12\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">RESUMEN CAJAS</span>
                            <div class=\"tools\">
                                
                            </div>
                        </div>
                        <div class=\"portlet-body\">
                            <div class=\"table-responsive\" id=\"div_table_2\">
                                <table class=\"table table-striped table-bordered table-hover\" id=\"table_2\">
                                    <thead>
                                        <tr>
                                            <th class=\"text-center\">TIPO</th>
                                            <th class=\"text-center\">MARCA</th>
                                            <th class=\"text-center\">CANTIDAD</th>
                                            <th class=\"text-center\">
                                                <small>({$resumenCajas->convertidas} lb)</small>
                                                <br>
                                                CONV
                                                <a title=\"Mostrar decimales\" ng-click=\"mostrarDecimalesConv = !mostrarDecimalesConv\">...</a>
                                            </th>
                                            <th class=\"text-center\">TOTAL (lb)</th>
                                            <th class=\"text-center\">PROM</th>
                                            <th class=\"text-center\">MAX</th>
                                            <th class=\"text-center\">MIN</th>
                                            <th class=\"text-center\">DESV</th>
                                            <th class=\"text-center\">CANT.<br>BAJO PESO</th>
                                            <th class=\"text-center\">%<br>BAJO PESO</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {$contentCajas}
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>TOTAL</th>
                                            <th></th>
                                            <th class=\"text-center\">". sumOfValue($resumenCajas->data, 'cantidad') ."</th>
                                            <th class=\"text-center\">". round(sumOfValue($resumenCajas->data, 'conv'), 2) ."</th>
                                            <th class=\"text-center\">". sumOfValue($resumenCajas->data, 'total_kg') ."</th>
                                            <th class=\"text-center\">". round(avgOfValue($resumenCajas->data, 'promedio'), 2) ."</th>
                                            <th class=\"text-center\">". round(avgOfValue($resumenCajas->data, 'maximo'), 2) ."</th>
                                            <th class=\"text-center\">". round(avgOfValue($resumenCajas->data, 'minimo'), 2) ."</th>
                                            <th class=\"text-center\">". round(avgOfValue($resumenCajas->data, 'desviacion'), 2) ."</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-12\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">EXCEDENTE <span class=\"badge badge-info\" title=\"Para visualizar es necesario configurar marcas con Máximo y Mínimo\"><i class=\"fa fa-info\"></i></span> </span>
                            <div class=\"actions\">
                                
                            </div>
                        </div>
                        <div class=\"portlet-body\" id=\"tablas\">
                            <div class=\"table-responsive\">
                                <table class=\"table table-bordered\">
                                    <thead>
                                        <tr>
                                            <th class=\"text-center\">MARCA</th>
                                            <th class=\"text-center\">TOTAL (lb) </th>
                                            <th class=\"text-center\">CAJAS CONV</th>
                                            <th class=\"text-center\">DOLARES</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {$contentExcedente}
                                        <tr>
                                            <th></th>
                                            <th class=\"text-center\">". round(sumOfValue($excedente->tablas, 'kg_diff'), 2) ."</th>
                                            <th class=\"text-center\">". round(sumOfValue($excedente->tablas, 'cajas'), 2) ."</th>
                                            <th class=\"text-center\">". round(sumOfValue($excedente->tablas, 'dolares'), 2) ."</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-12\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\"></span>
                            <div class=\"actions\">
                                
                            </div>
                        </div>
                        <div class=\"portlet-body\">
                            <div class=\"row grid-container\" id=\"barras\">
                                {$contentPasteles}
                            </div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-6\">
                    <div class=\"portlet box green-haze\">
                        <div class=\"portlet-title\">
                            <span class=\"caption\">RESUMEN PROCESO</span>
                            <div class=\"actions\">
                                
                            </div>
                        </div>
                        <div class=\"portlet-body\">
                            <div class=\"table-responsive\">
                                <table class=\"table table-bordered\">
                                    <tbody>
                                        {$contentResumenProceso}
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>
    ";
    
    echo $html;
    set_time_limit(0);
    ini_set("memory_limit","128M");
    require_once("phrapi/libs/utilities/dompdf/dompdf_config.inc.php");
    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
    $dompdf->render();
    $output = $dompdf->output();
    file_put_contents("pdf/produccion-dia/{$_GET['finca']}_{$_GET['fecha_inicial']}/informe.pdf", $output);
?>