<style>
    .chart {
        height : 400px;
    }
    td, th {
        text-align : center;
    }
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }

    .main-row {
        border: 2px solid #d6d6d6;
        background-color: #dfdfdf87;
    }
    .pointer {
        cursor : pointer;
    }

    .calendar-table td.available {
		background-color: green !important;
    	color: white !important;
	}
	.icalendar-table td.available:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>

<div ng-controller="controller" ng-cloak>
    <div style="width: 100%;margin-bottom: 10px;display:table;">
        <h3 class="page-title" style="display: table-cell !important;"> 
            Reporte de Comparación
        </h3>
    </div>

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Calidad</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Comparación</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li ng-class="{ 'active' : mode == 'general' }">
                            <a ng-click="mode = 'general'">General</a>
                        </li>
                        <li ng-class="{ 'active' : mode == 'defectos' }">
                            <a ng-click="mode = 'defectos'">Defectos</a>
                        </li>
                        <li ng-class="{ 'active' : mode == 'temperatura' }">
                            <a ng-click="mode = 'temperatura'">Temperatura</a>
                        </li>
                        <li ng-class="{ 'active' : mode == 'cluster' }">
                            <a ng-click="mode = 'cluster'">Cluster por Grado</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="principal">

                    <div class="table-responsive" ng-class="{ 'hide' : mode != 'general' }">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th class="pointer" ng-click="toggleOrder('general', 'cliente')">Detalle</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'recibidas')"># Gavetas Recibidas en local</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'kilos_totales')">Kilos Totales</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'kilos_destruidos')">Kilos Destruidos</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'porc_lancofruit')">% Lancofruit</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'porc_otros')">% Otros</th>
                                    <th class="pointer" ng-click="toggleOrder('general', 'porc_vacia')">% Vacia</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="main-row" ng-repeat-start="row in data.general | orderObjectBy : orderBy.general.key : orderBy.general.direction" ng-click="row.expanded = !row.expanded">
                                    <td>{{ row.cliente }}</td>
                                    <td>{{ row.recibidas }}</td>
                                    <td>{{ row.kilos_totales | number }}</td>
                                    <td>{{ row.kilos_destruidos | number }}</td>
                                    <td>{{ row.porc_lancofruit | number : 2 }}</td>
                                    <td>{{ row.porc_otros | number : 2 }}</td>
                                    <td>{{ row.porc_vacia | number : 2 }}</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="suc in row.detalle" ng-show="row.expanded">
                                    <td>{{ suc.cliente }}</td>
                                    <td>{{ suc.recibidas }}</td>
                                    <td>{{ suc.kilos_totales | number }}</td>
                                    <td>{{ suc.kilos_destruidos | number }}</td>
                                    <td>{{ suc.porc_lancofruit | number : 2 }}</td>
                                    <td>{{ suc.porc_otros | number : 2 }}</td>
                                    <td>{{ suc.porc_vacia | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>{{ data.general | sumOfValue : 'recibidas' }}</th>
                                    <th>{{ data.general | sumOfValue : 'kilos_totales' | number }}</th>
                                    <th>{{ data.general | sumOfValue : 'kilos_destruidos' | number }}</th>
                                    <th>{{ data.general | avgOfValue : 'porc_lancofruit' | number : 2 }}</th>
                                    <th>{{ data.general | avgOfValue : 'porc_otros' | number : 2 }}</th>
                                    <th>{{ data.general | avgOfValue : 'porc_vacia' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="table-responsive" ng-class="{ 'hide' : mode != 'defectos' }">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="{{ categoria.defectos.length }}" ng-repeat="categoria in data.defectos.categorias">{{ categoria.type }}</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th class="pointer" ng-click="toggleOrder('defectos', 'zona')">Detalle</th>
                                    <!-- DEFECTOS -->
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td class="pointer" ng-click="toggleOrder('defectos', categoria.type+'_'+d.siglas)" ng-repeat-end ng-repeat="d in categoria.defectos" title="{{ d.descripcion }}">{{ d.siglas }}</td>
                                    <th class="pointer" ng-click="toggleOrder('defectos', 'total_defectos')">Total</th>
                                    <th class="pointer" ng-click="toggleOrder('defectos', 'total_defectos_porc')">%</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="main-row" ng-repeat-start="row in data.defectos.data | orderObjectBy : orderBy.defectos.key : orderBy.defectos.direction" ng-click="row.expanded = !row.expanded">
                                    <td>{{ row.cliente }}</td>
                                    <!-- DEFECTOS -->
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="d in categoria.defectos">{{ row[categoria.type+'_'+d.siglas] }}</td>
                                    <td>{{ row.total_defectos }}</td>
                                    <td>{{ row.total_defectos_porc = (row.total_defectos/(data.defectos.data | sumOfValue : 'total_defectos')*100) | number : 2 }}</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="finca in row.detalle | orderObjectBy : orderBy.defectos.key : orderBy.defectos.direction" ng-show="row.expanded">
                                    <td>{{ finca.cliente }}</td>
                                    <!-- DEFECTOS -->
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="d in categoria.defectos">{{ finca[categoria.type+'_'+d.siglas] }}</td>
                                    <td>{{ finca.total_defectos }}</td>
                                    <td>{{ finca.total_defectos/(data.defectos.data | sumOfValue : 'total_defectos')*100 | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr style="border-top : 1px solid black;">
                                    <th>TODOS</th>
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="d in categoria.defectos">{{ data.defectos.data | sumOfValue : categoria.type+'_'+d.siglas }}</td>
                                    <th>{{ data.defectos.data | sumOfValue : 'total_defectos' }}</th>
                                    <th>100%</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td ng-repeat-start="categoria in data.defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="d in categoria.defectos"><span class="{{ getUmbralDefectos(totales_defectos[categoria.type+'_'+d.siglas]) }}">{{ totales_defectos[categoria.type+'_'+d.siglas] = ((data.defectos.data | sumOfValue : categoria.type+'_'+d.siglas)/(data.defectos.data | sumOfValue : 'total_defectos')*100) | number:2 }}%</span></td>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="table-responsive" ng-class="{ 'hide' : mode != 'temperatura' }">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th class="pointer" ng-click="toggleOrder('temperatura', 'cliente')">Detalle</th>
                                    <th class="pointer" ng-click="toggleOrder('temperatura', 'temp_bodega')">Temp. Prom. Bodega</th>
                                    <th class="pointer" ng-click="toggleOrder('temperatura', 'temp_percha_1')">Temp. Prom. Percha 1</th>
                                    <th class="pointer" ng-click="toggleOrder('temperatura', 'temp_percha_2')">Temp. Prom. Percha 2</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="main-row" ng-repeat-start="row in data.temperatura | orderObjectBy : orderBy.temperatura.key : orderBy.temperatura.direction" ng-click="row.expanded = !row.expanded">
                                    <td>{{ row.cliente }}</td>
                                    <td>{{ row.temp_bodega }}</td>
                                    <td>{{ row.temp_percha_1 }}</td>
                                    <td>{{ row.temp_percha_2 }}</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="suc in row.detalle" ng-show="row.expanded">
                                    <td>{{ suc.cliente }}</td>
                                    <td>{{ suc.temp_bodega }}</td>
                                    <td>{{ suc.temp_percha_1 }}</td>
                                    <td>{{ suc.temp_percha_2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>{{ data.temperatura | avgOfValue : 'temp_bodega' | number : 2 }}</th>
                                    <th>{{ data.temperatura | avgOfValue : 'temp_percha_1' | number : 2 }}</th>
                                    <th>{{ data.temperatura | avgOfValue : 'temp_percha_2' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div class="table-responsive" ng-class="{ 'hide' : mode != 'cluster' }">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th class="pointer" ng-click="toggleOrder('cluster', 'cliente')">Detalle</th>
                                    <th class="pointer" ng-click="toggleOrder('cluster', 'grado_1')">Grado 1</th>
                                    <th class="pointer" ng-click="toggleOrder('cluster', 'grado_2')">Grado 2</th>
                                    <th class="pointer" ng-click="toggleOrder('cluster', 'grado_3')">Grado 3</th>
                                    <th class="pointer" ng-click="toggleOrder('cluster', 'grado_4')">Grado 4</th>
                                    <th class="pointer" ng-click="toggleOrder('cluster', 'grado_5')">Grado 5</th>
                                    <th class="pointer" ng-click="toggleOrder('cluster', 'grado_6')">Grado 6</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="main-row" ng-repeat-start="row in data.cluster | orderObjectBy : orderBy.cluster.key : orderBy.cluster.direction" ng-click="row.expanded = !row.expanded">
                                    <td>{{ row.cliente }}</td>
                                    <td>{{ row.grado_1 | number : 2 }}</td>
                                    <td>{{ row.grado_2 | number : 2 }}</td>
                                    <td>{{ row.grado_3 | number : 2 }}</td>
                                    <td>{{ row.grado_4 | number : 2 }}</td>
                                    <td>{{ row.grado_5 | number : 2 }}</td>
                                    <td>{{ row.grado_6 | number : 2 }}</td>
                                </tr>
                                <tr ng-repeat-end ng-repeat="suc in row.detalle" ng-show="row.expanded">
                                    <td>{{ suc.cliente }}</td>
                                    <td>{{ suc.grado_1 | number : 2 }}</td>
                                    <td>{{ suc.grado_2 | number : 2 }}</td>
                                    <td>{{ suc.grado_3 | number : 2 }}</td>
                                    <td>{{ suc.grado_4 | number : 2 }}</td>
                                    <td>{{ suc.grado_5 | number : 2 }}</td>
                                    <td>{{ suc.grado_6 | number : 2 }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>{{ data.cluster | sumOfValue : 'grado_1' | number : 2 }}</th>
                                    <th>{{ data.cluster | sumOfValue : 'grado_2' | number : 2 }}</th>
                                    <th>{{ data.cluster | sumOfValue : 'grado_3' | number : 2 }}</th>
                                    <th>{{ data.cluster | sumOfValue : 'grado_4' | number : 2 }}</th>
                                    <th>{{ data.cluster | sumOfValue : 'grado_5' | number : 2 }}</th>
                                    <th>{{ data.cluster | sumOfValue : 'grado_6' | number : 2 }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="tabbable tabbable-tabdrop">
                <ul class="nav nav-tabs">
                    <li ng-class="{ 'active' : mode_graficas == 'defectos' }">
                        <a ng-click="mode_graficas = 'defectos'; moda_graficas_desc = '(# Daños)'; renderGraficaZona(modeCategoriaGraficaZona); renderGraficaFinca(2, modeCategoriaGraficaZona)">Defectos</a>
                    </li>
                    <li ng-class="{ 'active' : mode_graficas == 'cluster' }">
                        <a ng-click="mode_graficas = 'cluster'; moda_graficas_desc = '(# Cluster)'; renderGraficaZona(); renderGraficaFinca()">Cluster Por Grado</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Grafica Comp. Zona. {{moda_graficas_desc}}</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li ng-repeat="categoria in dataGraficaZona[mode_graficas].categorias" ng-class="{ 'active' : modeCategoriaGraficaZona == categoria }">
                            <a ng-click="renderGraficaZona(categoria); renderGraficaFinca(categoria)">{{ categoria }}</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="graficas-zona">
                    <h3 style="position : absolute" ng-show="dataGraficaZona[mode_graficas].categorias.length == 0">SIN DEFECTOS</h3>
                    <div id="grafica-zona-chart" class="chart">
                    
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Grafica Por Sucursal {{ moda_graficas_desc }}</span>
                    </div>
                    <div class="tools">
                        Cliente 
                        <select class="input-sm" style="color : black;" ng-model="modeZonaGraficaFinca" ng-change="renderGraficaFinca(modeCategoriaGraficaZona)">
                            <option value="{{cliente}}" ng-repeat="cliente in dataGraficaFinca[mode_graficas].clientes" ng-show="dataGraficaFinca[mode_graficas].data[cliente][modeCategoriaGraficaZona]">{{cliente}}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body" id="graficas-finca">
                    <div id="grafica-finca-chart" class="chart">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>