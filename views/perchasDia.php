<style>
    .chart {
        height : 400px;
    }
    td, th {
        text-align : center;
    }
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }

    #columns {
        column-width: 320px;
        column-gap: 15px;
        width: 90%;
        margin: 50px auto;
    }

    div#columns figure {
        background: #fefefe;
        border: 2px solid #fcfcfc;
        box-shadow: 0 1px 2px rgba(34, 25, 25, 0.4);
        margin: 0 2px 15px;
        padding: 15px;
        padding-bottom: 10px;
        transition: opacity .4s ease-in-out;
        display: inline-block;
        column-break-inside: avoid;
    }

    div#columns figure img {
        width: 100%; height: auto;
        border-bottom: 1px solid #ccc;
        padding-bottom: 15px;
        margin-bottom: 5px;
    }

    div#columns figure figcaption {
        font-size: 1rem;
        color: #444;
        line-height: 1.5;
    }

    div#columns small { 
        font-size: 1rem;
        float: right; 
        text-transform: uppercase;
        color: #aaa;
    } 

    div#columns small a { 
        color: #666; 
        text-decoration: none; 
        transition: .4s color;
    }

    @media screen and (max-width: 750px) { 
        #columns { column-gap: 0px; }
        #columns figure { width: 100%; }
    }

    .calendar-table td.available {
		background-color: green !important;
    	color: white !important;
	}
	.icalendar-table td.available:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>

<div ng-controller="controller" ng-cloak>
    <div style="width: 100%;margin-bottom: 10px;display:table;">
        <h3 class="page-title" style="display: table-cell !important;"> 
            Reporte Día
        </h3>
    </div>

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Perchas - Día</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            Sucursales
            <select class="input-sm" ng-model="filters.sucursal" ng-change="init()" style="height: 40px;">
                <option value="">TODOS</option>
                <option value="{{s}}" ng-repeat="s in sucursales">{{ s }}</option>
            </select>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
    </div>
     
    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li ng-repeat="c in clientes" class="{{ filters.cliente == c ? 'active' : '' }}">
                <a ng-click="filters.cliente = c; variables()">{{c}}</a>
            </li>
        </ul>
    </div>

    <div id="tags">
        <?php include("./views/tags_perchas_dia.php");?>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Defectos</span>
                    </div>
                </div>
                <div class="portlet-body" id="defectos">
                    <div class="table-responsive" ng-show="defectos.data.length > 0">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="{{ categoria.defectos.length }}" ng-repeat="categoria in defectos.categorias">{{ categoria.type }}</th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>N</th>
                                    <th ng-repeat-start="categoria in defectos.categorias" class="hide"></th>
                                    <th ng-repeat-end ng-repeat="defecto in categoria.defectos" title="{{ defecto.descripcion }}">{{ defecto.siglas }}</th>
                                    <th>Total Daños</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="calidad in defectos.data">
                                    <td>{{$index+1}}</td>
                                    <td ng-repeat-start="categoria in defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="defecto in categoria.defectos">{{ calidad.defectos[categoria.type+'_'+defecto.siglas] | number }}</td>
                                    <td>{{ calidad.total_defectos }}</td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>P/T</td>
                                    <td ng-repeat-start="categoria in defectos.categorias" class="hide"></td>
                                    <td ng-repeat-end ng-repeat="defecto in categoria.defectos">{{ defectos.data | sumOfValueDouble : 'defectos' : (categoria.type+'_'+defecto.siglas) }}</td>
                                    <td>{{ defectos.data | sumOfValue : 'total_defectos' | number }}</td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    <div ng-show="defectos.data.length == 0">
                        <h4>SIN DEFECTOS</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Cluster por Grado</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="">
                            <a href="#empaque_tab_tabla" data-toggle="tab" aria-expanded="true"> Tabla </a>
                        </li>
                        <li class="active">
                            <a ng-click="reRenderClusterGrado()" href="#empaque_tab_grafica" data-toggle="tab" aria-expanded="false"> Gráfica </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="clusterGrado">
                    <div class="tab-content">
                        <div class="tab-pane" id="empaque_tab_tabla">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Detalle</th>
                                            <th>Cantidad Cluster</th>
                                            <th>%</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in clusterGrado.data">
                                            <td>{{ row.label }}</td>
                                            <td>{{ row.value }}</td>
                                            <td>{{ row.porc = (row.value/(clusterGrado.data | sumOfValue : 'value')*100) | number : 2 }}</td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th>{{ total_empaque.cantidad = (clusterGrado.data | sumOfValue : 'value') | number : 2 }}</th>
                                            <th>100%</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane active" id="empaque_tab_grafica">
                            <div id="clusterGrado-pie" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Daños por Categoria</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li ng-class="{ 'active' : categoria == filters.porCategoria }" ng-repeat="categoria in porCategoria.categorias">
                            <a ng-click="filters.porCategoria = categoria; reRenderPorCategoria();" data-toggle="tab" aria-expanded="false"> {{ categoria }} </a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="porCategoria">
                    <div class="tab-content">
                        <div class="tab-pane active" id="cluster_tab_grafica">
                            <div ng-show="porCategoria.categorias.length == 0" style="position : absolute;">
                                <h4>SIN DEFECTOS</h4>
                            </div>
                            <div id="porCategoria-pie" class="chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" ng-show="filters.cliente == 'Mi Comisariato'">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Participación de mercado</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="participacion-pie" style="height : 400px"></div>
                        </div>

                        <div class="col-md-6 table-responsive">
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>PARTICIPANTE</th>
                                        <th>PORCENTAJE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in participacionMercado.data">
                                        <td>{{ row.participante }}</td>
                                        <td>{{ row.porcentaje | number }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">Fotos</span>
                    </div>
                    <div class="tools">
                        <h4 style="display : inline;">{{ (fotos.data | filter : filters.foto).length }} FOTOS</h4>
                        <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                    </div>
                </div>
                <div class="portlet-body" id="fotos" style="display : none;">

                    <!-- TIPOS -->
                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-tabs">
                            <li ng-class="{ 'active' : !filters.fotos.type }">
                                <a ng-click="deleteFilter('type')">TODOS</a>
                            </li>
                            <li ng-repeat="type in (fotos.data | filter : { id_calidad : filters.fotos.id_calidad } | getNotRepeat : 'type')" class="{{ filters.fotos.type == type ? 'active' : '' }}">
                                <a ng-click="filters.fotos.type = type;">{{ type }}</a>
                            </li>
                        </ul>
                    </div>

                    <div class="row" id="columns">
                        <figure ng-repeat="image in fotos.data | filter : filters.fotos">
                            <a href="{{image.path}}" target="_black"><img src="{{ image.path }}" alt="Imagen no disponible"></a>
                            <figcaption>
                                Fecha : {{ image.fecha }}
                                <br>
                                Tipo : {{ image.type }}
                                <br>
                                Observaciones : 
                                <p>{{ image.observaciones }}</p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="portlet box green-haze">
        <div class="portlet-title">
            <div class="caption">
                <span class="caption-subject bold uppercase">Base de datos</span>
            </div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <div id="tabla_venta_dia" class="table-responsive table-scrollable">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">FECHA</th>
                                    <th class="text-center">HORA</th>
                                    <th class="text-center">CLIENTE</th>
                                    <th class="text-center">LOCAL</th>
                                    <th class="text-center">RESPONSABLE</th>
                                    <th class="text-center">ACCIONES</th>
                                </tr>
                                <tr>
                                    <td><input type = "text" ng-model = "search.fecha" class = "color-td-prac form-control form-filter"/></td>
                                    <td><input type = "text" ng-model = "search.hora" class = "color-td-prac form-control form-filter"/></td>
                                    <td><input type = "text" ng-model = "search.cliente" class = "color-td-pract form-control form-filter"/></td>
                                    <td><input type = "text" ng-model = "search.local" class = "color-td-prac form-control form-filter"/></td>
                                    <td><input type = "text" ng-model = "search.responsable" class = "color-td-prac form-control form-filter"/></td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat = "row in listado">
                                    <td>{{ row.fecha }}</td>
                                    <td>{{ row.hora }}</td>
                                    <td>{{ row.cliente }}</td>
                                    <td>{{ row.local }}</td>
                                    <td>{{ row.responsable }}</td>
                                    <td class="text-center">
                                        <a href="https://s3.amazonaws.com/json-publicos/{{row.url}}" target="_blank" class="btn green-haze" title="VER PDF"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>