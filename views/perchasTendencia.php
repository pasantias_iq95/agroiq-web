<div ng-controller="controller" ng-cloak>
    <div style="width: 100%;margin-bottom: 10px;display:table;">
        <h3 class="page-title" style="display: table-cell !important;"> 
            Reporte de Tendencia
        </h3>
    </div>

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Calidad</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="col-md-6">
                <ul class="nav nav-tabs">
                    <li ng-class="{ 'active' : filters.period == 'semana' }">
                        <a ng-click="filters.period = 'semana'; index()">Sem</a>
                    </li>
                    <li ng-class="{ 'active' : filters.period == 'periodo' }">
                        <a ng-click="filters.period = 'periodo'; index()">Per</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                Cliente : 
                <select class="form-control" ng-model="filters.cliente" ng-change="changeCliente()">
                    <option value="">Seleccione</option>
                    <option value="{{cliente}}" ng-repeat="cliente in clientes">{{cliente}}</option>
                </select>
            </div>
            <div class="col-md-12">
                <ul class="nav nav-tabs">
                    <li ng-class="{ 'active' : filters.mode == 'general' }">
                        <a ng-click="filters.mode = 'general'; index()">General</a>
                    </li>
                    <li ng-class="{ 'active' : filters.mode == 'temperatura' }">
                        <a ng-click="filters.mode = 'temperatura'; index()">Temperatura</a>
                    </li>
                    <li ng-class="{ 'active' : filters.mode == 'cluster' }">
                        <a ng-click="filters.mode = 'cluster'; index()">Cluster por Grado</a>
                    </li>
                    <li ng-class="{ 'active' : filters.mode == 'defectos' }">
                        <a ng-click="filters.mode = 'defectos'; index()">Defectos</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-6">
            Sucursales : 
            <select id="sucursales" class="form-control" ng-model="sucursales_md" ng-change="changeSucursales()" multiple>
                <option value="{{suc}}" ng-repeat="suc in sucursales" ng-disabled="sucursales_available.indexOf(suc) == -1">{{suc}}</option>
            </select>
        </div>
    </div>

    <div class="row hide" id="contenedor">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">{{ filters.mode | uppercase }}</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <!-- BEGIN GENERAL -->
                        <li ng-show="filters.mode == 'general'" ng-class="{ 'active' : filters.var == 'gavetas' }">
                            <a ng-click="filters.var = 'gavetas'; index()"># Gavetas Recibidas en local</a>
                        </li>
                        <li ng-show="filters.mode == 'general'" ng-class="{ 'active' : filters.var == 'kilos_totales' }">
                            <a ng-click="filters.var = 'kilos_totales'; index()">Kilos Totales</a>
                        </li>
                        <li ng-show="filters.mode == 'general'" ng-class="{ 'active' : filters.var == 'kilos_destruidos' }">
                            <a ng-click="filters.var = 'kilos_destruidos'; index()">Kilos Destruidos</a>
                        </li>
                        <li ng-show="filters.mode == 'general'" ng-class="{ 'active' : filters.var == 'porc_fruta_lancofruit' }">
                            <a ng-click="filters.var = 'porc_fruta_lancofruit'; index()">% Lancofruit</a>
                        </li>
                        <li ng-show="filters.mode == 'general'" ng-class="{ 'active' : filters.var == 'porc_fruta_terceros' }">
                            <a ng-click="filters.var = 'porc_fruta_terceros'; index()">% Otros</a>
                        </li>
                        <li ng-show="filters.mode == 'general'" ng-class="{ 'active' : filters.var == 'porc_percha_vacia' }">
                            <a ng-click="filters.var = 'porc_percha_vacia'; index()">% Vacia</a>
                        </li>
                        <!-- BEGIN TEMPERATURA -->
                        <li ng-show="filters.mode == 'temperatura'" ng-class="{ 'active' : filters.var == 'temp_bodega' }">
                            <a ng-click="filters.var = 'temp_bodega'; index()">Temp. Prom. Bodega</a>
                        </li>
                        <li ng-show="filters.mode == 'temperatura'" ng-class="{ 'active' : filters.var == 'temp_percha_1' }">
                            <a ng-click="filters.var = 'temp_percha_1'; index()">Temp. Prom. Percha 1</a>
                        </li>
                        <li ng-show="filters.mode == 'temperatura'" ng-class="{ 'active' : filters.var == 'temp_percha_2' }">
                            <a ng-click="filters.var = 'temp_percha_2'; index()">Temp. Prom. Percha 2</a>
                        </li>
                        <!-- BEGIN CLUSTER POR GRADO -->
                        <li ng-show="filters.mode == 'cluster'" ng-class="{ 'active' : filters.var == 'grado_2_cantidad_cluster' }">
                            <a ng-click="filters.var = 'grado_2_cantidad_cluster'; index()">Grado 2</a>
                        </li>
                        <li ng-show="filters.mode == 'cluster'" ng-class="{ 'active' : filters.var == 'grado_3_cantidad_cluster' }">
                            <a ng-click="filters.var = 'grado_3_cantidad_cluster'; index()">Grado 3</a>
                        </li>
                        <li ng-show="filters.mode == 'cluster'" ng-class="{ 'active' : filters.var == 'grado_4_cantidad_cluster' }">
                            <a ng-click="filters.var = 'grado_4_cantidad_cluster'; index()">Grado 4</a>
                        </li>
                        <li ng-show="filters.mode == 'cluster'" ng-class="{ 'active' : filters.var == 'grado_5_cantidad_cluster' }">
                            <a ng-click="filters.var = 'grado_5_cantidad_cluster'; index()">Grado 5</a>
                        </li>
                        <li ng-show="filters.mode == 'cluster'" ng-class="{ 'active' : filters.var == 'grado_6_cantidad_cluster' }">
                            <a ng-click="filters.var = 'grado_6_cantidad_cluster'; index()">Grado 6</a>
                        </li>
                        <!-- DEFECTOS -->
                        <li ng-show="filters.mode == 'defectos'" ng-class="{ 'active' : filters.var == 'total_defectos' }">
                            <a ng-click="filters.var = 'total_defectos'; index()">Total defectos</a>
                        </li>
                        <li ng-show="filters.mode == 'defectos'" ng-class="{ 'active' : filters.defecto.defecto == c.defecto && filters.defecto.categoria == c.categoria }" ng-repeat="c in defectos" title="{{ c.categoria }} - {{ c.defecto }}">
                            <a ng-click="filters.var = 'defecto'; filters.defecto = c; index()">{{ c.categoria.substring(0, 1).trim() }} - {{ c.defecto.substring(0, 2).trim() }}</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body" id="principal">
                    <div>
                        <div id="chart" class="chart"></div>
                    </div>

                    <div class="table table-hover table-bordered" id="tabla-general">
                        
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>