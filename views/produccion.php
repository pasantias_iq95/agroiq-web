<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción
     </h3>
     <div class="page-bar" ng-init="produccion.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <ng-calendarapp search="search"></ng-calendarapp>
        </div>
    </div>
	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Racimos por Palanca</span>
		    <div class="tools">
	            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
	        </div>
		</div>
		<div class="portlet-body" style="display: none;">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
			                <tr role="row" class="heading">
			                    <th width="8.33%" class="center-th"> Palanca  </th>
			                    <th width="8.33%" class="center-th"> Lote  </th>
			                    <th ng-if="id_company != 4" width="8.33%" class="center-th"> Cable  </th>
			                    <th width="8.33%" class="center-th"> Racimos <br> Cosechados  </th>
			                    <th width="8.33%" class="center-th"> Racimos <br> Recusados </th>   
			                    <th width="8.33%" class="center-th"> Racimos <br> Procesados </th>                                                    
			                    <th width="8.33%" class="center-th"> Racimos <br> Muestrados </th>                                                    
			                    <th width="8.33%" class="center-th"> % </th>                                                  
			                    <th width="8.33%" class="center-th"> Peso <br> KG </th>                                                    
			                    <th width="8.33%" class="center-th"> # <br> Manos </th>
			                    <th width="8.33%" class="center-th"> Calibre </th>                                                  
			                    <th width="8.33%" class="center-th">  Largo <br> Dedos  </th>                                                  
			                </tr>
					<tbody>
						<tr ng-repeat-start="palanca in palancas" ng-click="openDetallePalanca(palanca)">
							<td class="left-td">{{palanca.palanca}}</td>
							<td ></td>
							<td ng-if="id_company != 4"></td>
							<td>{{palanca.total_cosechados}}</td>
							<td>{{palanca.total_recusados}}</td>
							<td>{{palanca.total_procesada}}</td>
							<td ng-if="palanca.muestrados > 0">{{palanca.muestrados  | number : 2}}</td>
							<td ng-if="palanca.muestrados == 0"></td>
							<td ng-if="((palanca.muestrados / palanca.total_cosechados) * 100) > 0">{{(palanca.muestrados / palanca.total_cosechados) * 100 | number : 2}}</td>
							<td ng-if="((palanca.muestrados / palanca.total_cosechados) * 100) <= 0"></td>
							<td ng-if="palanca.peso > 0">{{palanca.peso  | number : 2}}</td>
							<td ng-if="palanca.peso == 0"></td>
							<td ng-if="palanca.manos > 0">{{palanca.manos  | number : 2}}</td>
							<td ng-if="palanca.manos == 0"></td>
							<td ng-if="palanca.calibracion > 0">{{palanca.calibracion  | number : 2}}</td>
							<td ng-if="palanca.calibracion == 0"></td>
							<td ng-if="palanca.largo_dedos > 0">{{palanca.largo_dedos  | number : 2}}</td>
							<td ng-if="palanca.largo_dedos == 0"></td>
						</tr>	
						<tr ng-show="palanca.expanded" ng-repeat-end="">
							<td colspan="12">
								<table class="table table-striped table-bordered table-hover">
									<tr ng-repeat-start="lote in palanca.lotes" ng-click="openDetalleLote(lote)">
										<td  width="8.33%"></td>
										<td  width="8.33%">{{lote.lote}}</td>
										<td ng-if="id_company != 4" width="8.33%"></td>
										<td  width="8.33%">{{lote.total_cosechados}}</td>
										<td  width="8.33%">{{lote.total_recusados}}</td>
										<td  width="8.33%">{{lote.total_procesada}}</td>
										<td ng-if="lote.muestrados > 0">{{lote.muestrados  | number : 2}}</td>
										<td ng-if="lote.muestrados == 0"></td>
										<td ng-if="((lote.muestrados / lote.total_cosechados) * 100) > 0">{{(lote.muestrados / lote.total_cosechados) * 100 | number : 2}}</td>
										<td ng-if="((lote.muestrados / lote.total_cosechados) * 100) <= 0"></td>
										<td ng-if="lote.peso > 0">{{lote.peso  | number : 2}}</td>
										<td ng-if="lote.peso == 0"></td>
										<td ng-if="lote.manos > 0">{{lote.manos  | number : 2}}</td>
										<td ng-if="lote.manos == 0"></td>
										<td ng-if="lote.calibracion > 0">{{lote.calibracion  | number : 2}}</td>
										<td ng-if="lote.calibracion == 0"></td>
										<td ng-if="lote.largo_dedos > 0">{{lote.largo_dedos  | number : 2}}</td>
										<td ng-if="lote.largo_dedos == 0"></td>
									</tr>
									<tr ng-show="lote.expanded" ng-repeat-end="">
										<td colspan="12">
											<table class="table table-striped table-bordered table-hover">
												<tr ng-repeat="cable in lote.cables">
													<td  width="8.33%"></td>
													<td  width="8.33%"></td>
													<td ng-if="id_company != 4" width="8.33%">{{cable.cable}}</td>
													<td  width="8.33%">{{cable.total_cosechados}}</td>
													<td  width="8.33%">{{cable.total_recusados}}</td>
													<td  width="8.33%">{{cable.total_procesada}}</td>
													<td ng-if="cable.muestrados > 0">{{cable.muestrados  | number : 2}}</td>
													<td ng-if="cable.muestrados == 0"></td>
													<td ng-if="((cable.muestrados / cable.total_cosechados) * 100) > 0">
														{{(cable.muestrados / cable.total_cosechados) * 100 | number : 2}}
													</td>
													<td ng-if="((cable.muestrados / cable.total_cosechados) * 100) <= 0"></td>
													<td ng-if="cable.peso > 0">{{cable.peso  | number : 2}}</td>
													<td ng-if="cable.peso == 0"></td>
													<td ng-if="cable.manos > 0">{{cable.manos  | number : 2}}</td>
													<td ng-if="cable.manos == 0"></td>
													<td ng-if="cable.calibracion > 0">{{cable.calibracion  | number : 2}}</td>
													<td ng-if="cable.calibracion == 0"></td>
													<td ng-if="cable.largo_dedos > 0">{{cable.largo_dedos  | number : 2}}</td>
													<td ng-if="cable.largo_dedos == 0"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr ng-repeat="total in totales">
							<td>Total : </td>
							<td></td>
							<td ng-if="id_company != 4"></td>
							<td>{{total.total_cosechados}} </td>
							<td>{{total.total_recusados}}</td>
							<td>{{total.total_procesada}}</td>
							<td>{{total.muestrados}}</td>
							<td>{{total.porcen | number : 2}}</td>
							<td>{{total.peso  | number : 2}}</td>
							<td>{{total.manos  | number : 2}}</td>
							<td>{{total.calibracion  | number : 2}}</td>
							<td>{{total.largo_dedos  | number : 2}}</td>
				</table>
			</div>
		</div>
	</div>

	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Racimos por Lote</span>
		    <div class="tools">
	            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
	        </div>
		</div>
		<div class="portlet-body" style="display: none;">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
		                <tr role="row" class="heading">
		                  	<th width="8.33%" class="center-th"> Lote  </th>
		                    <th ng-if="id_company != 4" width="8.33%" class="center-th"> Cable </th>
		                    <th width="8.33%" class="center-th"> Cinta  </th>
		                    <th width="8.33%" class="center-th"> Racimos <br> Cosechados  </th>
		                    <th width="8.33%" class="center-th"> Racimos <br> Recusados </th>                                                    
		                    <th width="8.33%" class="center-th"> Racimos <br> Procesados </th>                                                    
		                    <th width="8.33%" class="center-th"> Racimos <br> Muestrados </th>                                                    
		                    <th width="8.33%" class="center-th"> % </th>
		                    <th width="8.33%" class="center-th"> Peso <br> KG </th>                                                  
		                    <th width="8.33%" class="center-th"> # <br> Manos </th>
		                    <th width="8.33%" class="center-th"> Calibre </th>
		                    <th width="8.33%" class="center-th">  Largo <br> Dedos  </th>                                                
		                </tr>
					<thead>
					<tbody>
						<tr ng-repeat-start="detalle in tabla.produccion" ng-click="openDetallePalanca(detalle)">
							<td class="center-th">{{detalle.lote}}</td>
							<td ng-if="id_company != 4" ></td>
							<td ></td>
							<td>{{detalle.total_cosechados}}</td>
							<td>{{detalle.total_recusados}}</td>
							<td>{{detalle.total_procesada}}</td>
							<td ng-if="detalle.muestrados > 0">{{detalle.muestrados  | number : 2}}</td>
							<td ng-if="detalle.muestrados == 0"></td>
							<td ng-if="((detalle.muestrados / detalle.total_cosechados) * 100) > 0">{{(detalle.muestrados / detalle.total_cosechados) * 100 | number : 2}}</td>
							<td ng-if="((detalle.muestrados / detalle.total_cosechados) * 100) <= 0"></td>
							<td ng-if="detalle.peso > 0">{{detalle.peso  | number : 2}}</td>
							<td ng-if="detalle.peso == 0"></td>
							<td ng-if="detalle.manos > 0">{{detalle.manos  | number : 2}}</td>
							<td ng-if="detalle.manos == 0"></td>
							<td ng-if="detalle.calibracion > 0">{{detalle.calibracion  | number : 2}}</td>
							<td ng-if="detalle.calibracion == 0"></td>
							<td ng-if="detalle.largo_dedos > 0">{{detalle.largo_dedos  | number : 2}}</td>
							<td ng-if="detalle.largo_dedos == 0"></td>
						</tr>	
						<tr ng-show="detalle.expanded" ng-repeat-end="">
							<td colspan="12">
								<table class="table table-striped table-bordered table-hover">
									<tr ng-repeat-start="details in detalle.cables" ng-click="openDetalleLote(details)">
										<td width="8.33%"></td>
										<td ng-if="id_company != 4" width="8.33%">{{details.cable}}</td>
										<td width="8.33%"></td>
										<td width="8.33%">{{details.total_cosechados}}</td>
										<td width="8.33%">{{details.total_recusados}}</td>
										<td width="8.33%">{{details.total_procesada}}</td>
										<td ng-if="details.muestrados > 0">{{details.muestrados  | number : 2}}</td>
										<td ng-if="details.muestrados == 0"></td>
										<td ng-if="((details.muestrados / details.total_cosechados) * 100) > 0">{{(details.muestrados / details.total_cosechados) * 100 | number : 2}}</td>
										<td ng-if="((details.muestrados / details.total_cosechados) * 100) <= 0"></td>
										<td ng-if="details.peso > 0">{{details.peso  | number : 2}}</td>
										<td ng-if="details.peso == 0"></td>
										<td ng-if="details.manos > 0">{{details.manos  | number : 2}}</td>
										<td ng-if="details.manos == 0"></td>
										<td ng-if="details.calibracion > 0">{{details.calibracion  | number : 2}}</td>
										<td ng-if="details.calibracion == 0"></td>
										<td ng-if="details.largo_dedos > 0">{{details.largo_dedos  | number : 2}}</td>
										<td ng-if="details.largo_dedos == 0"></td>
									</tr>
									<tr ng-show="details.expanded" ng-repeat-end="">
										<td colspan="12">
											<table class="table table-striped table-bordered table-hover">
												<tr ng-repeat="cinta in details.cintas">
													<td width="8.33%"></td>
													<td ng-if="id_company != 4" width="8.33%"></td>
													<td width="8.33%" class="{{cinta.color}}">{{cinta.nombre | uppercase}}</td>
													<td width="8.33%">{{cinta.cosechados}}</td>
													<td width="8.33%">{{cinta.recusados}}</td>
													<td width="8.33%">{{cinta.procesados}}</td>
													<td ng-if="cinta.muestrados > 0">{{cinta.muestrados  | number : 2}}</td>
													<td ng-if="cinta.muestrados == 0"></td>
													<td ng-if="((cinta.muestrados / cinta.total_cosechados) * 100) > 0">
														{{(cinta.muestrados / cinta.total_cosechados) * 100 | number : 2}}
													</td>
													<td ng-if="((cinta.muestrados / cinta.total_cosechados) * 100) <= 0"></td>
													<td ng-if="cinta.peso > 0">{{cinta.peso  | number : 2}}</td>
													<td ng-if="cinta.peso == 0"></td>
													<td ng-if="cinta.manos > 0">{{cinta.manos  | number : 2}}</td>
													<td ng-if="cinta.manos == 0"></td>
													<td ng-if="cinta.calibracion > 0">{{cinta.calibracion  | number : 2}}</td>
													<td ng-if="cinta.calibracion == 0"></td>
													<td ng-if="cinta.largo_dedos > 0">{{cinta.largo_dedos  | number : 2}}</td>
													<td ng-if="cinta.largo_dedos == 0"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr ng-repeat="total in totales">
							<td>Total : </td>
							<td ng-if="id_company != 4"></td>
							<td></td>
							<td>{{total.total_cosechados}} </td>
							<td>{{total.total_recusados}}</td>
							<td>{{total.total_procesada}}</td>
							<td>{{total.muestrados}}</td>
							<td>{{total.porcen | number : 2}}</td>
							<td>{{total.peso  | number : 2}}</td>
							<td>{{total.manos  | number : 2}}</td>
							<td>{{total.calibracion  | number : 2}}</td>
							<td>{{total.largo_dedos  | number : 2}}</td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>

	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Racimos por Edad</span>
		    <div class="tools">
	            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
	        </div>
		</div>
		<div class="portlet-body" style="display: none;">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
			                <tr role="row" class="heading">
			                    <th width="8.33%" class="center-th"> Cinta  </th>
			                    <th width="8.33%" class="center-th"> Lote  </th>
			                    <th ng-if="id_company != 4" width="8.33%" class="center-th"> Cable </th>
			                    <th width="8.33%" class="center-th"> Racimos <br> Cosechados  </th>
			                    <th width="8.33%" class="center-th"> Racimos <br> Recusados </th>    
			                    <th width="8.33%" class="center-th"> Racimos <br> Procesados </th>                                                
			                    <th width="8.33%" class="center-th"> Racimos <br> Muestrados </th>                                                    
			                    <th width="8.33%" class="center-th"> % </th>                                               
			                    <th width="8.33%" class="center-th"> Peso <br> KG </th>                                                    
			                    <th width="8.33%" class="center-th"> # <br> Manos </th>
			                    <th width="8.33%" class="center-th"> Calibre </th>                                               
			                    <th width="8.33%" class="center-th"> Largo <br> Dedos </th>                                                  
			                </tr>
					</thead>
					<tbody>
						<tr ng-repeat-start="color in colores" ng-click="openDetallePalanca(color)">
							<td class="{{color.color}}">{{color.nombre | uppercase}}</td>
							<td></td>
							<td ng-if="id_company != 4"></td>
							<td>{{color.cosechados}}</td>
							<td>{{color.recusados}}</td>
							<td>{{color.procesados}}</td>
							<td ng-if="color.muestrados > 0">{{color.muestrados  | number : 2}}</td>
							<td ng-if="color.muestrados == 0"></td>
							<td ng-if="((color.muestrados / color.cosechados) * 100) > 0">{{(color.muestrados / color.cosechados) * 100 | number : 2}}</td>
							<td ng-if="((color.muestrados / color.cosechados) * 100) <= 0"></td>
							<td ng-if="color.peso > 0">{{color.peso  | number : 2}}</td>
							<td ng-if="color.peso == 0"></td>
							<td ng-if="color.manos > 0">{{color.manos  | number : 2}}</td>
							<td ng-if="color.manos == 0"></td>
							<td ng-if="color.calibracion > 0">{{color.calibracion  | number : 2}}</td>
							<td ng-if="color.calibracion == 0"></td>
							<td ng-if="color.largo_dedos > 0">{{color.largo_dedos  | number : 2}}</td>
							<td ng-if="color.largo_dedos == 0"></td>
						</tr>	
						<tr ng-show="color.expanded" ng-repeat-end="">
							<td colspan="12">
								<table class="table table-striped table-bordered table-hover">
									<tr ng-repeat-start="lote in color.lotes" ng-click="openDetalleLote(lote)">
										<td width="8.33%"></td>
										<td width="8.33%">{{lote.lote}}</td>
										<td ng-if="id_company != 4" width="8.33%"></td>
										<td width="8.33%">{{lote.cosechados}}</td>
										<td width="8.33%">{{lote.recusados}}</td>
										<td width="8.33%">{{lote.procesados}}</td>
										<td ng-if="lote.muestrados > 0">{{lote.muestrados  | number : 2}}</td>
										<td ng-if="lote.muestrados == 0"></td>
										<td ng-if="((lote.muestrados / lote.cosechados) * 100) > 0">{{(lote.muestrados / lote.cosechados) * 100 | number : 2}}</td>
										<td ng-if="((lote.muestrados / lote.cosechados) * 100) <= 0"></td>
										<td ng-if="lote.peso > 0">{{lote.peso  | number : 2}}</td>
										<td ng-if="lote.peso == 0"></td>
										<td ng-if="lote.manos > 0">{{lote.manos  | number : 2}}</td>
										<td ng-if="lote.manos == 0"></td>
										<td ng-if="lote.calibracion > 0">{{lote.calibracion  | number : 2}}</td>
										<td ng-if="lote.calibracion == 0"></td>
										<td ng-if="lote.largo_dedos > 0">{{lote.largo_dedos  | number : 2}}</td>
										<td ng-if="lote.largo_dedos == 0"></td>
									</tr>
									<tr ng-show="lote.expanded" ng-repeat-end="">
										<td colspan="12">
											<table class="table table-striped table-bordered table-hover">
												<tr ng-repeat="cable in lote.cables">
													<td width="8.33%"></td>
													<td width="8.33%"></td>
													<td ng-if="id_company != 4" width="8.33%">{{cable.cable}}</td>
													<td width="8.33%">{{cable.cosechados}}</td>
													<td width="8.33%">{{cable.recusados}}</td>
													<td width="8.33%">{{cable.procesados}}</td>
													<td ng-if="cable.muestrados > 0">{{cable.muestrados  | number : 2}}</td>
													<td ng-if="cable.muestrados == 0"></td>
													<td ng-if="((cable.muestrados / cable.cosechados) * 100) > 0">{{(cable.muestrados / cable.cosechados) * 100 | number : 2}}</td>
													<td ng-if="((cable.muestrados / cable.cosechados) * 100) <= 0"></td>
													<td ng-if="cable.peso > 0">{{cable.peso  | number : 2}}</td>
													<td ng-if="cable.peso == 0"></td>
													<td ng-if="cable.manos > 0">{{cable.manos  | number : 2}}</td>
													<td ng-if="cable.manos == 0"></td>
													<td ng-if="cable.calibracion > 0">{{cable.calibracion  | number : 2}}</td>
													<td ng-if="cable.calibracion == 0"></td>
													<td ng-if="cable.largo_dedos > 0">{{cable.largo_dedos  | number : 2}}</td>
													<td ng-if="cable.largo_dedos == 0"></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr ng-repeat="total in totales">
							<td>Total : </td>
							<td></td>
							<td ng-if="id_company != 4"></td>
							<td>{{total.total_cosechados}} </td>
							<td>{{total.total_recusados}}</td>
							<td>{{total.total_procesada}}</td>
							<td>{{total.muestrados}}</td>
							<td>{{total.porcen | number : 2}}</td>
							<td>{{total.peso  | number : 2}}</td>
							<td>{{total.manos  | number : 2}}</td>
							<td>{{total.calibracion  | number : 2}}</td>
							<td>{{total.largo_dedos  | number : 2}}</td>
						</tr>	
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>