<style>
	.chart {
		height : 400px;
	}
</style>
<div ng-controller="control">
	<h3 class="page-title"> 
        Producción Comparación Anual
    </h3>
    <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Producción</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            
        </div>
    </div>
	<div id="contenedor">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
					    <span class="caption">Resumen</span>
					    <div class="tools">
				            <select ng-model="filters.variable" class="form-control" ng-change="changeGrafica()">
								<option value="{{ variable }}" ng-repeat="variable in variables">{{ variable }}</option>
							</select>
				        </div> 
					</div>
					<div class="portlet-body">
						<div id="grafica" class="chart"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>