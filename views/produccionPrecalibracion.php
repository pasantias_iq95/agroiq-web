<style>
	td, th {
        text-align : center;
    }
    .black {
        color : black;
    }
    .no-margin {
        margin: 0px;
    }
    .form-control {
        border-radius : 3px !important;
    }
    .btn {
        border-radius : 3px !important;
    }
    .portlet {
        border-radius : 3px !important;
    }
    .pointer {
        cursor : pointer;
    }
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Producción. Precalibración
     </h3>
     <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Precalibración</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar" style="display: flex;">
            <label for="semana" style="margin: auto;">
                Sem : 
            </label>
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-fit-height dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="2000" data-close-others="true" aria-expanded="false"> 
                    {{ filters.year }} - {{ filters.week }}
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu" style="max-height: 300px; overflow-y: scroll;">
                    <li ng-repeat="sem in semanas" ng-click="filters.week = sem.semana; filters.year = sem.anio; init()">
                        <a href="javascript:;">
                            {{ sem.anio }} -  {{ sem.semana }}
                        </a>
                    </li>
                </ul>
            </div>
            <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Barrer</button>
        </div>
    </div>

    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li ng-repeat="f in fincas" class="{{ filters.id_finca == f.id ? 'active' : '' }}">
                <a ng-click="filters.id_finca = f.id; init()">{{f.nombre}}</a>
            </li>
        </ul>
    </div>

	<div id="contenedor" class="div2">
        <div class="row">
            <div class="col-md-6">
                <div class="portlet box green-haze">
                    <div class="portlet-title">
                        <span class="caption">RACIMOS POR PRECALIBRAR</span>
                        <div class="tools">
                            <button class="btn bg-dark bg-font-dark" ng-click="exportExcel('por_precalibrar')">
                                Exportar
                            </button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive" id="por_precalibrar">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>LOTE</th>
                                        <th class="{{e.class}}" ng-repeat="e in por_precalibrar_edades">{{ e.edad }}</th>
                                        <th>TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in por_precalibrar">
                                        <td>{{ row.lote }}</td>
                                        <td ng-repeat="e in por_precalibrar_edades">
                                            <span 
                                                class="pointer" 
                                                data-toggle="tooltip" 
                                                data-placement="top"
                                                data-html="true"
                                                title="Enfundados : {{ row['sem_'+e.edad+'_enfundados'] }}<br> Precalibrados : {{ row['sem_'+e.edad+'_precalibrados'] }}<br> Caidos : {{ row['sem_'+e.edad+'_caidos'] }}">
                                                {{ row['sem_'+e.edad] }}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="pointer" data-toggle="tooltip" data-html="true" data-placement="top" title="Enfundados : {{ row.total_enfundados }}<br> Precalibrados : {{ row.total_precalibrados }}<br> Caidos : {{ row.total_caidos }}">
                                                {{ row.total }}
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th ng-repeat="e in por_precalibrar_edades">
                                            <span 
                                                class="pointer" 
                                                data-toggle="tooltip" 
                                                data-placement="top" 
                                                data-html="true"
                                                title="Enfundados : {{ por_precalibrar | sumOfValue : 'sem_'+e.edad+'_enfundados' }}<br> Precalibrados : {{ por_precalibrar | sumOfValue : 'sem_'+e.edad+'_precalibrados' }}<br> Caidos : {{ por_precalibrar  | sumOfValue : 'sem_'+e.edad+'_caidos' }}"
                                            >
                                                {{ por_precalibrar | sumOfValue: 'sem_'+e.edad }}
                                            </span>
                                        </th>
                                        <th>
                                            <span 
                                                class="pointer" 
                                                data-toggle="tooltip" 
                                                data-placement="top" 
                                                data-html="true"
                                                title="Enfundados : {{ por_precalibrar | sumOfValue : 'total_enfundados' }}<br> Precalibrados : {{ por_precalibrar | sumOfValue : 'total_precalibrados' }}<br> Caidos : {{ por_precalibrar | sumOfValue : 'total_caidos' }}"
                                            >
                                                {{ por_precalibrar | sumOfValue : 'total' }}
                                            </span>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box green-haze">
                    <div class="portlet-title">
                        <span class="caption">RACIMOS LISTOS PARA COSECHAR</span>
                        <div class="tools">
                            <button class="btn bg-dark bg-font-dark" ng-click="exportExcel('por_cosechar')">
                                Exportar
                            </button>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive" id="por_cosechar">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>LOTE</th>
                                        <th class="{{e.class}}" ng-repeat="e in por_precalibrar_edades">{{ e.edad }}</th>
                                        <th>TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in por_cosechar">
                                        <td>{{ row.lote }}</td>
                                        <td ng-repeat="e in por_precalibrar_edades">
                                            <span class="pointer" data-toggle="tooltip" data-html="true" data-placement="top" title="Precalibrados : {{ row['sem_'+e.edad+'_precalibrados'] }}<br> Cosechados {{ row['sem_'+e.edad+'_cosechados'] }}<br> Caidos : {{ row['sem_'+e.edad+'_caidos'] }}">
                                                {{ row['sem_'+e.edad] }}
                                            </span>
                                        </td>
                                        <td>
                                            <span class="pointer" data-toggle="tooltip" data-html="true" data-placement="top" title="Precalibrados : {{ row.total_precalibrados }}<br> Cosechados {{ row.total_cosechados }}<br> Caidos : {{ row.total_caidos }}">
                                                {{ row.total }}
                                            </span>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th ng-repeat="e in por_precalibrar_edades">
                                            <span 
                                                class="pointer" 
                                                data-toggle="tooltip" 
                                                data-placement="top" 
                                                data-html="true"
                                                title="Precalibrados : {{ por_cosechar | sumOfValue : 'sem_'+e.edad+'_precalibrados' }}<br>Cosechados : {{ por_cosechar | sumOfValue : 'sem_'+e.edad+'_cosechados' }}<br> Caidos : {{ por_cosechar  | sumOfValue : 'sem_'+e.edad+'_caidos' }}"
                                            >
                                                {{ por_cosechar | sumOfValue: 'sem_'+e.edad }}
                                            </span>
                                        </th>
                                        <th>
                                            <span 
                                                class="pointer" 
                                                data-toggle="tooltip" 
                                                data-placement="top" 
                                                data-html="true"
                                                title="Precalibrados : {{ por_cosechar | sumOfValue : 'sem_'+e.edad+'_precalibrados' }}<br>Cosechados : {{ por_cosechar | sumOfValue : 'total_cosechados' }}<br> Caidos : {{ por_cosechar  | sumOfValue : 'sem_'+e.edad+'_caidos' }}"
                                            >
                                                {{ por_cosechar | sumOfValue : 'total' }}
                                            </span>
                                        </th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="portlet box green-haze">
                    <div class="portlet-title">
                        <span class="caption">RACIMOS PRECALIBRADOS</span>
                        <div class="tools">
                            <button class="btn bg-dark bg-font-dark" ng-click="exportExcel('precalibrado')">
                                Exportar
                            </button>
                            <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body" style="display:none">
                        <div class="table-responsive" id="precalibrado">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>LOTE</th>
                                        <th class="{{e.class}}" ng-repeat="e in precalibrado_edades">{{ e.edad }}</th>
                                        <th>TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in precalibrado">
                                        <td>{{ row.lote }}</td>
                                        <td ng-repeat="e in precalibrado_edades">{{ row['sem_'+e.edad] }}</td>
                                        <td>{{ row.total }}</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th ng-repeat="e in precalibrado_edades">{{ precalibrado | sumOfValue: 'sem_'+e.edad }}</th>
                                        <th>{{ precalibrado | sumOfValue : 'total' }}</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet box green-haze">
                    <div class="portlet-title">
                        <span class="caption">RACIMOS PRECALIBRADOS <small>(BASE DE DATOS)</small></span>
                        <div class="tools">
                            <button class="btn bg-dark bg-font-dark" ng-click="exportExcel('database')">
                                Exportar
                            </button>
                            <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body" style="display:none">
                        <div class="table-responsive" id="database">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th width="50">LOTE</th>
                                        <th>CALIBRADOR</th>
                                        <th>EDAD</th>
                                        <th>CANTIDAD</th>
                                        <th>REF.</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in database">
                                        <td>{{ row.lote }}</td>
                                        <td>{{ row.calibrador }}</td>
                                        <td class="{{ row.class }}">{{ row.edad }}</td>
                                        <td>{{ row.cantidad }}</td>
                                        <td>
                                            <a href="https://s3.amazonaws.com/json-publicos/banano/marcel/precalibracion/json/20170919-1866471371.pdf" target="_blank">
                                                {{ row.json }}
                                            </a>
                                        </td>
                                        <td>
                                            <button class="btn btn-primary" title="Editar" ng-click="editar(row)">
                                                <i class="fa fa-pencil"></i>
                                            </button>
                                            <button class="btn btn-danger" title="Eliminar" ng-click="borrar(row)">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="row hide">
            <div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">ENFUNDE - PRECALIBRACION</span>
						<div class="tools">
                            <a href="javascript:;" class="expand" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body" style="display:none">
                        <div class="table-responsive" id="precalibracion">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th rowspan="2">LOTE</th>
                                        <th rowspan="2">RAC<br>ENF</th>
                                        <th ng-if="edades.length > 0" class="text-center sm" colspan="{{edades.length}}">RACIMOS COSECHADOS POR EDAD (SEM)</th>
                                        <th rowspan="2">TOTAL<br>PREC</th>
                                        <th rowspan="2">SALDO<br>PREC</th>
                                        <th rowspan="2">%</th>
                                    </tr>
                                    <tr>
                                        <th ng-repeat="e in edades | orderObjectBy : 'edad'" class="sm">{{ e.edad }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in precalibracion">
                                        <td>{{row.lote}}</td>
                                        <td class="{{ colorClass }}">
                                            {{ row.racimos_enfunde > 0 ? (row.racimos_enfunde | number: 0) : '' }}
                                        </td>
                                        <td ng-if="edades.length > 0" class="sm" ng-repeat="e in edades | orderObjectBy : 'edad'">
                                            {{ row['edad_'+e.edad] > 0 ? row['edad_'+e.edad] : '' }}
                                        </td>
                                        <td>{{row.cosechados}}</td>
                                        <td>{{row.saldo = (row.racimos_enfunde - row.cosechados)}}</td>
                                        <td class="{{ getUmbral(row.enfunde) }}">{{ row.enfunde }}%</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>TOTAL</th>
                                        <th class="{{ colorClass }}">{{ totales.racimos_enfunde | number }}</th>
                                        <th ng-if="edades.length > 0" class="sm" ng-repeat="e in edades | orderObjectBy : 'edad'">
                                            {{ (precalibracion | sumOfValue : 'edad_'+e.edad) > 0 ? (precalibracion | sumOfValue : 'edad_'+e.edad | number) : '' }}
                                        </th>
                                        <th>{{ totales.cosechados = (precalibracion | sumOfValue : 'cosechados') | number }}</th>
                                        <th>{{ totales.saldo = (precalibracion | sumOfValue : 'saldo') | number }}</th>
                                        <th class="{{ getUmbral(totales.recobro) }}">{{ totales.recobro | number : 2 }}%</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
					</div>
				</div>
            </div>
		</div>
    </div>
    
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title inline" id="exampleModalLabel">Esta funcionalidad permite eliminar el enfunde que no se a recobrado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                Semana de enfunde
                            </div>
                            <div class="col-md-4">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-fit-height dropdown-toggle {{ enfunde.class }}" data-toggle="dropdown" data-hover="dropdown" data-delay="2000" data-close-others="true" aria-expanded="false"> 
                                        {{ enfunde.anio }} - {{ enfunde.semana }}
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu" style="max-height: 300px; overflow-y: scroll;">
                                        <li ng-repeat="sem in semanas_enfunde" ng-click="select(sem)">
                                            <a href="javascript:;" class="{{sem.class}}">
                                                {{ sem.anio }} -  {{ sem.semana }}
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <table class="table table-responsive">
                                <tbody>
                                    <tr>
                                        <th class="text-left">Racimos Faltantes</th>
                                        <td>{{ enfunde.enfundados - enfunde.caidos - enfunde.cosechados }}</th>
                                    </tr>
                                    <tr>
                                        <th class="text-left">Recobro</th>
                                        <td>{{ enfunde.recobro = ((enfunde.caidos+enfunde.cosechados)/enfunde.enfundados*100) | number : 2 }} %</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-{{ getUmbral(enfunde.recobro) }}" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: {{ enfunde.recobro }}%">
                                    <span class="sr-only"> {{ enfunde.recobro | number : 2 }}% </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" ng-click="barrer()">Barrer Cosecha</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title inline" id="exampleModalLabel">Esta funcionalidad permite modificar información</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                Lote
                            </div>
                            <div class="col-md-4">
                                <select class="form-control" ng-model="rowSelected.lote">
                                    <option value="{{ lote }}" ng-repeat="lote in lotes">{{ lote }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                Calibrador
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control" ng-model="rowSelected.calibrador">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                Edad
                            </div>
                            <div class="col-md-4">
                                <input type="number" class="form-control" ng-model="rowSelected.edad">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-4">
                                Cantidad
                            </div>
                            <div class="col-md-4">
                                <input type="number" class="form-control" ng-model="rowSelected.cantidad">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" ng-click="guardarEditar()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>