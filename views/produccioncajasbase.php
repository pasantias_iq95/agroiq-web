<style>
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	table {
		cursor: pointer !important;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}	
    .delete  {
        background-color : #c8d8e8 !important;
    }

	.form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
	.highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>

<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
        Base de datos
    </h3>
    <div class="page-bar">
         <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Cajas</a>
                <i class="fa fa-angle-right"></i>
			</li>
        </ul>
        <div class="page-toolbar">
            <label>Fecha</label>
            <input id="datepicker" class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly>
        </div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-md btn-default" href="produccioncajas<?= isset($_GET['fecha']) ? '?fecha='.$_GET['fecha'] : '' ?>">
				<i class="fa fa-arrow-left"></i> Regresar a Balanza de Cajas
			</a>
		</div>
	</div>

	<br>
	<br>
    
	<div id="contenedor" class="div2">

		<div class="tabbable tabbable-tabdrop">
			<ul class="nav nav-tabs">
				<li ng-repeat="(key, value) in fincas" class="{{ table.finca == key ? 'active' : '' }}">
					<a ng-click="table.finca = key; getRegistros();">{{value}}</a>
				</li>
			</ul>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">BASE DE DATOS</span>
						
					</div>
					<div class="portlet-body">
						<div class="row" id="tabla_base">
							<button class="btn" ng-click="seleccionarTodo()" title="Seleccionar todo">Seleccionar Todo <i class="fa fa-check-square"></i></button>
							<button class="btn" ng-click="deseleccionarTodo()" title="Deseleccionar todo">Deseleccionar Todo <i class="fa fa-minus-square-o"></i></button>

							<div class="tools pull-right">
								<div id="actions-lista">
									<button class="btn green" ng-click="getRegistrosBase()"><i class="fa fa-refresh"></i></button>
									<select class="input-sm" style="color: #555;" ng-model="searchTable.limit" ng-change="searchTable.changePagination()">
										<option value="{{ registros.length }}">TODOS</option>
										<option value="10">10</option>
										<option value="50">50</option>
										<option value="100">100</option>
									</select>
									<div class="btn-group">
										<a class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
											Exportar <i class="fa fa-angle-down"></i>
										</a>
										<ul class="dropdown-menu pull-right">
											<li>
												<a href="javascript:;" ng-click="exportPrint('table_1')"> Imprimir </a>
											</li>
											<li>
												<a href="javascript:;" ng-click="exportExcel('table_1')">Excel</a>
											</li>
										</ul>
									</div>
									<button class="btn red-thunderbird" ng-show="seleccionados.length > 0" ng-click="eliminar()">Eliminar</button>
									<a class="btn green-jungle" ng-show="seleccionados.length > 0" data-toggle="modal" href="#editar-modal">Editar</a>
								</div>
							</div>
							<ul class="nav nav-tabs tabs-reversed hide">
								<li class="dropdown">
									<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Configuración
										<i class="fa fa-angle-down"></i>
									</a>
									<ul class="dropdown-menu" role="menu">
										<li class="">
											<a href="#configuracion" ng-click="setTipo('Tipo')" tabindex="-1" data-toggle="modal" aria-expanded="false"> Tipo </a>
										</li>
										<li class="">
											<a href="#configuracion" ng-click="showRango()" tabindex="-1" data-toggle="modal" aria-expanded="false"> Rangos (Marca) </a>
										</li>
										<li class="">
											<a href="#configuracion" ng-click="setTipo('Exportadora')" tabindex="-1" data-toggle="modal" aria-expanded="false"> Exportadora </a>
										</li>
									</ul>
								</li>
								<li class="" ng-click="hideActions()">
									<a href="#createproductos" data-toggle="tab" aria-expanded="true"> Configurar </a>
								</li>
								<li class="active" ng-click="showActions()">
									<a href="#listado" data-toggle="tab" aria-expanded="false"> Listado </a>
								</li>

							</ul>
							<div class="tab-content">
								<div class="tab-pane fade hide" id="createproductos" ng-include="'./views/templetes/produccionCajas/configurar.html?i=2'"></div>
								<div class="tab-pane fade active in" id="listado" ng-include="'./views/templetes/produccionCajas/listado.html?i=9'"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="modal fade" id="rangos" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Configurar Rangos</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Marca: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="config.rangos.newConfig.marca">
								<option ng-repeat="(key, value) in config.rangos.marcas" value="{{ value }}">{{ key }}</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Max: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="config.rangos.newConfig.max"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Min: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="config.rangos.newConfig.min"/>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="configuracion.save()">Guardar</button>
			</div>
		</div>
	</div>

	<div class="modal fade" id="editar-modal" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Editar Registros</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<h3>{{ seleccionados.length }} Registros seleccionados para editar</h3>
					</div>
					<br>
					<br>
					<div class="col-md-12">
						<div class="col-md-4">
							Marca:
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="editar.id_marca">
								<option value="">Sin Cambios</option>
								<option value="{{m.id}}" ng-repeat="m in marcas">{{m.nombre}}</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							Peso ({{ table.unidad }}):
						</div>
						<div class="col-md-8">
							<input type="number" class="form-control" ng-model="editar.peso">
						</div>
					</div>
					<div class="col-md-12">
						<div ng-if="editar.id_marca || editar.peso" class="font-green-haze">
							<span>Cambios</span>
							<ol>
								<li ng-if="editar.id_marca">Todas las cajas seleccionadas cambiaran de marca</li>
								<li ng-if="editar.peso">Todas las cajas seleccionadas cambiaran de peso</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="guardar()" ng-disabled="!editar.id_marca && !editar.peso">Guardar</button>
			</div>
		</div>
	</div>

</div>

<script src="assets/global/plugins/FileSaver.min.js"></script>