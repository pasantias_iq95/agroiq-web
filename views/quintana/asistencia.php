    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }

        .search {
            margin-right: 5px;
            margin-top: 4px;
            padding: 5px;
        }
    </style>

      <!-- BEGIN CONTENT BODY -->
<div id="informe_tthh" ng-controller="informe_tthh" ng-cloak>
     <h3 class="page-title"> 
          Asistencia
     </h3>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Asistencia</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
            <!--<input type="text" ng-model="search.nombre" class="search" placeholder="Buscar">-->
            <label for="limit">Mostrar</label>
            <select class="input-sm" ng-model="search.limit" name="limit">
                <option value="10">10</option>
                <option value="50">50</option>
                <option value="100">100</option>
                <option value="{{personal.length}}">TODOS</option>
            </select>
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
     </div>
     <div class="page-bar" style="padding: 0; color: #888">
        <div class="row">
            <!--<div class="col-md-4">
                <div class="col-md-8" style="text-align: left">TIPO DE TRABAJADOR:</div>
                <div class="col-md-4"><select name="" id="">
                    <option value="">AFILIADO</option>
                </select></div>
            </div>-->
            <div class="col-md-4">
                <div class="col-md-4" style="text-align: left">HACIENDA:</div>
                <div class="col-md-4"><select name="finca" id="finca" ng-model="calidad.finca" onchange="changeFinca()">
                    <option value="">TODAS</option>
                    <option ng-repeat="(key , value) in fincas" value="{{value}}">{{value}}</option>
                </select></div>
            </div>
            <!--<div class="col-md-4">
                <div class="col-md-8" style="text-align: left">TIPO DE LABOR:</div>
                <div class="col-md-4"><select name="" id="">
                    <option value="">TODAS</option>
                </select></div>
            </div>-->
        </div>
    </div>

     <?php #include("./views/tags_tthh.php"); ?>
     <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]" onload="loadExternal()">

     </div>  
</div>
