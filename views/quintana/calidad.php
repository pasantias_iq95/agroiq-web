    
    <style>
        .label_chart {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .legendLabel{
            padding: 3px !important;
        }
        .lenguage {
            padding-bottom: 9px;
            padding-top: 9px;
        }
    </style>
      <!-- BEGIN CONTENT BODY -->
<?php
    if(Session::getInstance()->type_users == 'ADMIN' || ((Session::getInstance()->type_users != 'ADMIN' && Session::getInstance()->agent_user == 'marcel') && (isset($_GET['c']) && isset($_GET['m'])))):
?>
<div ng-controller="informe_calidad" id="informe_calidad" ng-cloak>
    <div style="width: 100%;margin-bottom: 10px;display:table;">
         <h3 class="page-title" style="display: table-cell !important;"> 
              Inspección de Calidad
         </h3>
        
    </div>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Calidad</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <div class="inline-block" ng-show="<?= Session::getInstance()->id_company ?> == 6">
                <span>Contenedor</span>
                <select class="input-sm" ng-model="calidad.params.contenedor" ng-change="changeContenedor()">                    
                    <option ng-repeat="(key, value) in contenedores" value="{{ key }}">{{ value }}</option>
                </select>
            </div>
            <div class="inline-block" ng-show="<?= Session::getInstance()->id_company ?> == 6">
                <span>Finca</span>
                <select class="input-sm" ng-model="calidad.params.finca" ng-change="changeContenedor()">
                    <option ng-repeat="(key, value) in fincas" value="{{ key }}">{{ value }}</option>
                </select>
            </div>
            <a href="quality?c=<?=$_GET['c']?>&m=<?=$_GET['m']?>" ng-hide="<?= Session::getInstance()->id_company ?> == 6" class="btn btn-primary lenguage">EN</a>
            <a href="qualitat?c=<?=$_GET['c']?>&m=<?=$_GET['m']?>" ng-hide="<?= Session::getInstance()->id_company ?> == 6" class="btn btn-primary lenguage">Puerto</a>
            <ng-calendarapp  search="search"></ng-calendarapp>
         </div>
     </div>

    <div class="page-bar" style="padding: 0; color: #888" ng-if="checkParams()">

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-2" style="text-align: left">CLIENTE:</div>
                    <div class="col-md-4">{{param_cliente}}</div>
                    <div class="col-md-4" style="text-align: left">PESO REFERENCIAL:</div>
                    <div class="col-md-2">{{param_peso}}</div>
                </div>
                <div class="row" style="margin-top: 12px;">
                    <div class="col-md-2" style="text-align: left">MARCA:</div>
                    <div class="col-md-4">{{param_marca}}</div>
                    <div class="col-md-2" style="text-align: left"> BUQUE:</div>
                    <div class="col-md-4">{{barco.nombre}}</div>
                </div>
            </div>
            <div class="col-md-2"><img ng-src="{{param_logo}}" height="50" /></div>
            <div class="col-md-2" style="display:none;">
					<select name="marcar" class="form-control">
					  <option ng-repeat="option in param_marcas" value="{{option.marca}}">{{option.marca}}</option>
					</select>
			</div>
    </div>

     <?php include("./views/tags_calidad.php");?>           
     <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]"></div>  
</div>
<?php
    endif;
?>