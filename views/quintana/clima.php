<?php
    $cdn = "http://cdn.procesos-iq.com/";
?>
<style>
    .center-th {
        text-align: center;
    }
    .left-th {
        text-align: left !important;
    }
    .charts {
        height:500px;
    }
    .cursor td, .cursor th{
        cursor: pointer;
    }
    .asc {
        background-position: right;
        background-repeat: no-repeat;
        padding-right: 30px;
        background-image : url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAZ0lEQVQ4y2NgGLKgquEuFxBPAGI2ahhWCsS/gDibUoO0gPgxEP8H4ttArEyuQYxAPBdqEAxPBImTY5gjEL9DM+wTENuQahAvEO9DMwiGdwAxOymGJQLxTyD+jgWDxCMZRsEoGAVoAADeemwtPcZI2wAAAABJRU5ErkJggg==');
    }
    .desc {
        background-position: right;
        background-repeat: no-repeat;
        padding-right: 30px;
        background-image : url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAZUlEQVQ4y2NgGAWjYBSggaqGu5FA/BOIv2PBIPFEUgxjB+IdQPwfC94HxLykus4GiD+hGfQOiB3J8SojEE9EM2wuSJzcsFMG4ttQgx4DsRalkZENxL+AuJQaMcsGxBOAmGvopk8AVz1sLZgg0bsAAAAASUVORK5CYII=');
    }
</style>

<script src="componentes/FilterableSortableTable.js"></script>

<div ng-controller="controller">
    <div class="page-head" ng-init="init()">
        <!-- BEGIN PAGE TITLE -->
        <div class="page-title">
            <h1>Reporte Clima</h1>
        </div>
        <!-- END PAGE TITLE -->
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-fit-height blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="false"> 
                    {{filters.year}}
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li ng-repeat="(key , value) in years" ng-click="setFilterYears(value)">
                        <a href="javascript:;">
                            {{value}}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <ul class="page-breadcrumb breadcrumb">
        <li>
            <span class="active">Resumen</span>
        </li>
    </ul>

    <div class="row">
        <div class="col-md-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-dark sbold uppercase">TEMP. MIN</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="grafica_temp_min" class="charts"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-dark sbold uppercase">LLUVIA</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="grafica_lluvia" class="charts"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-dark sbold uppercase">TEMP. MAX</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="grafica_temp_max" class="charts"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-dark sbold uppercase">HORAS LUZ</span>
                    </div>
                    <div class="actions">
                        <select class="form-control input-sm" ng-model="filters.luz" ng-change="changeLuz()">
                            <option value="100" ng-selected="100 == filters.luz">100 (W/M2)</option>
                            <option value="150">150 (W/M2)</option>
                            <option value="200">200 (W/M2)</option>
                            <option value="400">400 (W/M2)</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="grafica_horas_luz" class="charts"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-dark sbold uppercase">HUMEDAD</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="grafica_humedad" class="charts"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-dark sbold uppercase">RAD SOLAR (w/m2)</span>
                    </div>
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <!--<a class="btn newFinca sbold uppercase btn-outline blue-ebonyclay">Nueva Finca</a>-->
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="grafica_rad_solar" class="charts"></div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject font-dark sbold uppercase">RAD SOLAR POR HORA (W/M2)</span>
                    </div>
                    <div class="actions">
                        <ng-calendarapp search="search"></ng-calendarapp>
                    </div>
                </div>
                <div class="portlet-body">
                    <div id="grafica_horas_dia" class="charts"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Datos </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel-group accordion" id="accordion3">

                    <div class="panel panel-default" ng-repeat="(key, data) in tablas">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_{{$index}}" aria-expanded="false"> {{ key }} </a>
                            </h4>
                        </div>
                        <div id="collapse_3_{{$index}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <div id="table_{{$index}}">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>