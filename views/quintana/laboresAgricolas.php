<style>
    .card {
        /* Add shadows to create the "card" effect */
        box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        transition: 0.3s;
        margin-bottom: 15px;
    }

    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    /* Add some padding inside the card container */
    .container {
        padding: 2px 16px;
    }
</style>
<script src="assets/global/plugins/FileSaver.min.js"></script>

<div ng-controller="controller">
     <h3 class="page-title"> 
          LABORES AGRICOLAS
     </h3>
     <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Labores</a>
                <i class="fa fa-angle-right"></i>
             </li>
        </ul>
        <div class="page-toolbar">
          
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
				<div class="portlet-title">
					<span class="caption">TENDENCIA POR PERIODO</span>
				</div>
				<div class="portlet-body">
                    <div id="grafica_tendencia" class="charts-500"></div>
                </div>
            </div>

            <div class="portlet box green">
				<div class="portlet-title">
                    <span class="caption">TENDENCIA POR PERIODO</span>
                    <div class="tools">
                        <!--<div class="btn-group pull-right" style="margin-right: 5px;">
                            <button class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                Exportar <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:;" ng-click="fnExcelReport('table', 'Calidad')">Excel</a>
                                </li>
                            </ul>
                        </div>-->
                    </div>
				</div>
				<div class="portlet-body">
                    <div class="table-scrollable hide" id="tablaHistorica">
                        <table class="table table-bordered" id="table">
                            <thead>
                                <tr>
                                    <th colspan="4"></th>
                                    <th ng-repeat="periodo in periodos | orderObjectBy : 'semana'" style="min-width: 50px;">{{ periodo.periodo }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat-start="row in tablaHistorica" ng-click="row.expanded = !row.expanded">
                                    <td colspan="4" style="min-width: 150px;"><i class="fa fa-chevron-{{ row.expanded ? 'down' : 'right' }}" style="margin-right: 5px;"></i>{{ row.finca }}</td>
                                    <td ng-repeat="periodo in periodos | orderObjectBy : 'semana'" class="{{ checks(row, periodo.periodo) }}">{{ row['PERIODO '+periodo.periodo] }}</td>
                                </tr>
                                <tr ng-repeat-start="lotes in row.detalle" ng-show="row.expanded" ng-click="lotes.expanded = !lotes.expanded" style="padding-left: 16px;">
                                    <td style="min-width: 20px;"></td>
                                    <td colspan="3">{{lotes.lote}}</td>
                                    <td ng-repeat="periodo in periodos | orderObjectBy : 'semana'" class="{{ checks(lotes, periodo.periodo) }}">{{ (lotes['PERIODO '+periodo.periodo] >= 0) ? lotes['PERIODO '+periodo.periodo] : '' }}</td>
                                </tr>
                                <tr ng-repeat-start="labores in lotes.detalle" ng-show="lotes.expanded" ng-click="labores.expanded = !labores.expanded" style="padding-left: 16px;">
                                    <td colspan="2" style="min-width: 40px;"></td>
                                    <td colspan="2">{{labores.labor}}</td>
                                    <td ng-repeat="periodo in periodos | orderObjectBy : 'semana'" class="{{ checks(labores, periodo.periodo) }}">{{ (labores['PERIODO '+periodo.periodo] >= 0) ? labores['PERIODO '+periodo.periodo] : '' }}</td>
                                </tr>
                                <tr ng-repeat="causas in labores.detalle" ng-show="labores.expanded" style="padding-left: 16px;">
                                    <td colspan="3"></td>
                                    <td>{{causas.causa}}</td>
                                    <td ng-repeat="periodo in periodos | orderObjectBy : 'semana'">{{ (causas['PERIODO '+periodo.periodo] > 0) ? causas['PERIODO '+periodo.periodo] : '' }}</td>
                                </tr>

                                <tr ng-repeat-end ng-hide="true">
                                </tr>
                                <tr ng-repeat-end ng-hide="true">
                                </tr>
                                <tr ng-repeat-end ng-hide="true">
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div id="tablaHistorica-react">
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <span class="caption">LABORES</span>
                        </div>
                        <div class="portlet-body">
                            <div id="grafica_labores" class="charts-400"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="portlet box green">
                        <div class="portlet-title">
                            <span class="caption">CAUSAS</span>
                            <div class="tools">
                                <select class="form-control" style="color:black" ng-model="filters.labor" ng-change="changeLaborGrafica()">
                                    <option ng-repeat="(key, values) in labores" value="{{key}}" ng-selected="key == filters.labor ">{{key}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div id="grafica_causas" class="charts-400"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet box green">
                <div class="portlet-title">
                    <span class="caption">FOTOS</span>
                </div>
                <div class="portlet-body">
                    <div class="panel-group accordion" id="accordion1">

                        <div class="panel panel-default" ng-repeat="periodo in fotos">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1_{{periodo.periodo}}" aria-expanded="false"> PERIODO {{ periodo.periodo }} </a>
                                </h4>
                            </div>
                            <div id="collapse_1_{{periodo.periodo}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <!-- NIVEL 2 -->
                                    <div class="panel-group accordion" id="accordion2_{{periodo.periodo}}">
                                        <div class="panel panel-default" ng-repeat="finca in periodo.fincas">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion2_{{periodo.periodo}}" href="#collapse_2_{{periodo.periodo}}_{{finca.idFinca}}" aria-expanded="false"> {{ finca.finca }} </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_2_{{periodo.periodo}}_{{finca.idFinca}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                <div class="panel-body">
                                                    <!-- NIVEL 3 -->
                                                    <div class="panel-group accordion" id="accordion2_{{periodo.periodo}}_{{finca.idFinca}}">
                                                        <div class="panel panel-default" ng-repeat="lote in finca.lotes">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion2_{{finca.idFinca}}_{{lote.idLote}}" href="#collapse_2_{{periodo.periodo}}_{{finca.idFinca}}_{{lote.idLote}}" aria-expanded="false"> LOTE {{ lote.lote }} </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapse_2_{{periodo.periodo}}_{{finca.idFinca}}_{{lote.idLote}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                                <div class="panel-body">
                                                                    <!-- NIVEL 4 -->
                                                                    <div class="panel-group accordion" id="accordion2_{{periodo.periodo}}_{{finca.idFinca}}_{{lote.idLote}}">
                                                                        <div class="panel panel-default" ng-repeat="labor in lote.labores">
                                                                            <div class="panel-heading">
                                                                                <h4 class="panel-title">
                                                                                    <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion2_{{periodo.periodo}}_{{finca.idFinca}}_{{lote.idLote}}" href="#collapse_2_{{periodo.periodo}}_{{finca.idFinca}}_{{lote.idLote}}_{{labor.idLabor}}" aria-expanded="false"> {{ labor.labor }} </a>
                                                                                </h4>
                                                                            </div>
                                                                            <div id="collapse_2_{{periodo.periodo}}_{{finca.idFinca}}_{{lote.idLote}}_{{labor.idLabor}}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                                                                <div class="panel-body">
                                                                                    <div class="col-md-3" ng-repeat="imagen in labor.imagenes">
                                                                                        <div class="card">
                                                                                            <img src="{{ imagen.url }}" alt="Imagen no disponible" style="width:100%; height: 170px;">
                                                                                            <div class="container">
                                                                                                <h4><b>Calidad : {{ imagen.promedio }} %</b></h4> 
                                                                                                <p>{{ imagen.fecha }}</p> 
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- NIVEL 4 -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- NIVEL 3 -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- NIVEL 2 -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
<script src="componentes/FilterableSortableTable.js"></script>