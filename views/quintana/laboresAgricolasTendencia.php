<style>
    .pointer {
        cursor : pointer;
    }
    .main-row {
        border: 2px solid #d6d6d6;
        background-color: #dfdfdf87;
    }
    .chart {
        height : 400px;
    }
    select.input-sm {
        color : black;
    }
    td,th {
        text-align : center;
    }
</style>

<div ng-controller="controller">
    <h3 class="page-title"> 
        Reporte Tendencia
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Labores</a>
                <i class="fa fa-angle-right"></i>
             </li>
        </ul>
        <div class="page-toolbar">
            
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">% CALIDAD PERIODAL</span>
                    </div>
                    <ul class="nav nav-tabs">
                        <li ng-class="{ 'active' : filters.calidadPeriodo.mode == 'fincas' }">
                            <a ng-click="filters.calidadPeriodo.mode = 'fincas'; filters.calidadPeriodo.finca = ''; calidadPeriodo()">POR FINCA</a>
                        </li>
                        <li ng-class="{ 'active' : filters.calidadPeriodo.mode == 'labores' }">
                            <a ng-click="filters.calidadPeriodo.mode = 'labores'; filters.calidadPeriodo.labor = ''; calidadPeriodo()">POR LABOR</a>
                        </li>
                    </ul>
                </div>
                <div class="portlet-body text-right">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <select class="input-sm" ng-show="filters.calidadPeriodo.mode == 'fincas'" ng-model="filters.calidadPeriodo.labor" ng-change="calidadPeriodo()">
                                <option value="">Labor</option>
                                <option value="{{ l.idLabor }}" ng-repeat="l in labores">{{ l.nombre }}</option>
                            </select>

                            <select class="input-sm" ng-show="filters.calidadPeriodo.mode == 'labores'" ng-model="filters.calidadPeriodo.finca" ng-change="calidadPeriodo()">
                                <option value="">Finca</option>
                                <option value="{{ l.idFinca }}" ng-repeat="l in fincas">{{ l.nombre }}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="grafica">
                                <div id="chart-calidad-periodal" class="chart"></div>
                            </div>
                            <div id="table-calidad-periodal"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <span class="caption-subject bold uppercase">CAUSAS PERIODAL</span>
                    </div>
                    <div class="actions">
                        <select class="input-sm" ng-model="filters.causasPeriodo.finca" ng-change="changeFilterCausa()">
                            <option value="{{f.idFinca}}" ng-repeat="f in fincas" ng-selected="filters.causasPeriodo.finca == f.idFinca">{{f.nombre}}</option>
                        </select>

                        <select class="input-sm" ng-model="filters.causasPeriodo.labor" ng-change="changeFilterCausa()">
                            <option value="{{f.idLabor}}" ng-repeat="f in labores" ng-selected="filters.causasPeriodo.labor == f.idLabor">{{f.nombre}}</option>
                        </select>
                    </div>
                </div>
                <div class="portlet-body text-right">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="grafica">
                                <div id="chart-causas-periodal" class="chart"></div>
                            </div>
                            <div id="table-causas-periodal"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="componentes/FilterableSortableTable.js"></script>