<style>
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	table {
		cursor: pointer !important;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}	
    .delete  {
        background-color : #c8d8e8 !important;
    }

	.form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
	.highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>
<script src="assets/global/plugins/FileSaver.min.js"></script>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Cajas
     </h3>
     <div class="page-bar" ng-init="produccion.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Cajas</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label class="btn red-thunderbird btn-outline btn-circle btn-sm" ng-click="convertKg()">
                {{ table.unidad }}
            </label>
            <!--<label for="">
                Año : 
            </label>
            <select name="year" id="year" ng-model="table.year" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in anios" value="{{key}}" ng-selected="key == table.year">{{value}}</option>
            </select>-->
            <label>Fecha</label>
            <input id="datepicker" class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly>
        </div>
    </div>
    
	<div id="contenedor" class="div2">

		<div class="tabbable tabbable-tabdrop">
			<ul class="nav nav-tabs">
				<li ng-repeat="(key, value) in fincas" class="{{ table.finca == key ? 'active' : '' }}">
					<a ng-click="table.finca = key; getRegistros(); getCuadrar();">{{value}}</a>
				</li>
			</ul>
		</div>

        <?php include('./views/marun/tags_produccion_cajas.php') ?>
		
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">RESUMEN CAJAS</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_2')"> Imprimir </a>
									</li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_2')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive" id="div_table_2">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
										<th class="center-th">TIPO</th>
										<th class="center-th">MARCA</th>
										<th class="center-th">CANTIDAD</th>
                                        <th class="center-th">CONV</th>
										<th class="center-th">TOTAL {{ table.unidad | uppercase }}</th>
										<th class="center-th">PROM</th>
										<th class="center-th">MAX</th>
										<th class="center-th">MIN</th>
										<th class="center-th">DESV</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="row in resumen">
										<td>{{ row.tipo }}</td>
										<td>{{ row.marca }}</td>
										<td>{{ row.cantidad }}</td>
                                        <td>{{ row.conv }}</td>
										<td>{{ row.total_kg | number: 2 }}</td>
										<td>{{ row.promedio | number: 2 }}</td>
										<td>{{ row.maximo | number: 2 }}</td>
										<td>{{ row.minimo | number: 2 }}</td>
										<td>{{ row.desviacion | number: 2 }}</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th>TOTAL</th>
										<th></th>
										<th class="center-th">{{ (resumen | sumOfValue:'cantidad') }}</th>
                                        <th class="center-th">{{ tags.cajas40 }}</th>
										<th class="center-th">{{ (resumen | sumOfValue:'total_kg') | number : 2 }}</th>
										<th class="center-th">{{ (registros | avgOfValue:'peso') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'maximo') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'minimo') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'desviacion') | number : 2 }}</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption"> EXCEDENTE </span>
						<div class="tools">
						
						</div>
					</div>
					<div class="portlet-body" id="tablas">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">MARCA</th>
                                        <th class="text-center">TOTAL {{ table.unidad | uppercase }} </th>
                                        <th class="text-center">CAJAS</th>
                                        <th class="text-center">DOLARES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in tablasDiferencias">
                                        <td>{{ row.marca }}</td>
                                        <td>{{ row.kg_diff }}</td>
                                        <td>{{ row.cajas }}</td>
                                        <td>{{ row.dolares }}</td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">{{ tablasDiferencias | sumOfValue: 'kg_diff' | number: 2 }}</th>
                                        <th class="text-center">{{ tablasDiferencias | sumOfValue: 'cajas' | number: 2 }}</th>
                                        <th class="text-center">{{ tablasDiferencias | sumOfValue: 'dolares' | number: 2 }}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>
		</div>

        <div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">  </span>
						<div class="tools">
						
						</div>
					</div>
					<div class="portlet-body">
                        <div class="row" id="barras">
                            <div ng-repeat="marca in marcasBarras | orderObjectBy : 'sec'" class="col-md-12">
                                <div class="col-md-6">
                                    <div id="barras_{{$index}}" class="chart"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="pastel_{{$index}}" class="chart"></div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO CAJAS SEMANAL</span>
						<div class="tools pull-right">
							<select class="form-control" ng-model="table.var" ng-change="loadSemanal()">
								<option value="CONV">CONV</option>
								<option value="CONV/HA">CONV/HA</option>
							</select>
						</div>
					</div>
					<div class="portlet-body">
						<div id="cajas_semanal" style="height: 400px;">

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row hide">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO PESO CAJAS</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row hide">
			<div class="col-md-6">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">ANÁLISIS DE RANGOS %</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">CAJAS POR MARCA</span>
						<div class="tools pull-right">
							
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row hide">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">% SEMANAL DE CAJAS POR DEBAJO DEL PESO</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row hide">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">RESUMEN POR SEMANA</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_3')"> Imprimir </a>
									</li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_3')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_3">
								<thead>
									<tr>
										<th class="center-th">SEM</th>
										<th class="center-th">CAJAS 1ra</th>
										<th class="center-th">CAJAS 2da</th>
										<th class="center-th">T CAJAS</th>
										<th class="center-th">CONV</th>
										<th class="center-th">CAJAS/HA</th>
										<th class="center-th">PESO PROM</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th>TOTAL</th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    Cajas
				</div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel-group accordion" id="accordion3">

                    <div class="panel panel-default hide">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> HISTORICO EXCEDENTE </a>
                            </h4>
                        </div>
                        <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <div class="pull-right col-md-2">
                                    <select class="form-control input-sm" ng-model="filters.var" ng-change="changeExcedente()">
                                        <option value="exce">{{ table.unidad | uppercase }}</option>
                                        <option value="cajas">CAJAS</option>
                                        <option value="dolares">DLL</option>
                                    </select>
                                </div>
                                <div id="table-historico-excedente"></div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> BASE DE DATOS CAJAS </a>
                            </h4>
                        </div>
                        <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <div class="tools pull-right">
                                    <div id="actions-lista">
                                        <button class="btn green" ng-click="getRegistrosBase()"><i class="fa fa-refresh"></i></button>
                                        <select class="input-sm" style="color: #555;" ng-model="searchTable.limit" ng-change="searchTable.changePagination()">
                                            <option value="{{ registros.length }}">TODOS</option>
                                            <option ng-repeat="opt in searchTable.optionsPagination" value="{{ opt > 0 ? opt : '' }}" ng-selected="opt == searchTable.limit">{{ opt > 0 ? opt : 'TODOS' }}</option>
                                        </select>
                                        <div class="btn-group">
                                            <a class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportPrint('table_1')"> Imprimir </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('table_1')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <button class="btn red-thunderbird" ng-click="eliminar()">Eliminar</button>
                                        <!--<button class="btn green-haze" ng-show="editando" ng-click="guardar()">Guardar</button>
                                        <button class="btn default" ng-show="editando" ng-click="cancelar()">Cancelar</button>-->
                                    </div>
                                </div>
                                <ul class="nav nav-tabs tabs-reversed">
                                    <li class="dropdown">
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Configuración
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li class="">
                                                <a href="#configuracion" ng-click="setTipo('Tipo')" tabindex="-1" data-toggle="modal" aria-expanded="false"> Tipo </a>
                                            </li>
                                            <li class="">
                                                <a href="#configuracion" ng-click="showRango()" tabindex="-1" data-toggle="modal" aria-expanded="false"> Rangos (Marca) </a>
                                            </li>
                                            <li class="">
                                                <a href="#configuracion" ng-click="setTipo('Exportadora')" tabindex="-1" data-toggle="modal" aria-expanded="false"> Exportadora </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="" ng-click="hideActions()">
                                        <a href="#createproductos" data-toggle="tab" aria-expanded="true"> Configurar </a>
                                    </li>
                                    <li class="active" ng-click="showActions()">
                                        <a href="#listado" data-toggle="tab" aria-expanded="false"> Listado </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade" id="createproductos" ng-include="'./views/marun/templetes/produccionCajas/configurar.html?i=<?=rand(10,100)?>'"></div>
                                    <div class="tab-pane fade active in" id="listado" ng-include="'./views/marun/templetes/produccionCajas/listado.html?i=<?=rand(10,100)?>'"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="expand_cuadrar" ng-click="getCuadrar()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> CUADRE DE CAJAS </a>
                            </h4>
                        </div>
                        <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-primary pull-right" ng-click="modalCuadrar()"> GUIA </a>                                    
                                    <a ng-show="isValidGuia()" class="btn red pull-right" ng-click="procesarDia()">Procesar</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="table_4">
                                            <thead>
                                                <tr>
													<th class="center-th">TIPO</th>
                                                    <th class="center-th">MARCA</th>
                                                    <th class="center-th">BALANZA</th>
                                                    <th class="center-th">GUIA</th>
													<th class="center-th">PEND</th>
													<th class="center-th">ASIG</th>
													<th class="center-th">REAL</th>
                                                    <th class="center-th">%</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in cuadrarCajas" ng-click="row.expanded = !row.expanded">
													<td class="center-th">{{ row.tipo }}</td>
                                                    <td class="center-th">{{ row.marca }}</td>
                                                    <td class="center-th">{{ row.balanza }}</td>
                                                    <td class="center-th">{{ row.suma_real }}</td>
													<td ng-dblclick="edit(row, 'pendiente')" escape="cancel(row)" enter="save(row)">
														<span  ng-if="row.editing != 'pendiente'">{{ row.pendiente > 0 ? row.pendiente : '' }}</span>
														<input ng-if="row.editing == 'pendiente'" type="text" class="form-control" ng-model="row.pendiente">
													</td>
													<td>
														{{ row.asignada }}
													</td>
													<td>
														<span ng-hide="true">{{ row.real = sum(row.suma_real, row.pendiente, row.asignada*-1) }}</span>
														{{ row.real ? row.real : '' }}
													</td>
                                                    <td class="center-th">
														<span ng-hide="true">{{ row.porcentaje = (row.real/row.balanza*100) }}</span>
														{{ row.porcentaje ? (row.porcentaje | number : 2) : '' }}
													</td>
                                                </tr>
                                                <tr ng-repeat-end="row" ng-repeat="d in row.detalle" ng-show="row.expanded">
													<td></td>
                                                    <td>{{d.guia}}</td>
                                                    <td></td>
                                                    <td>{{d.valor}}</td>
													<td></td>
													<td></td>
													<td></td>
                                                    <td>{{row.balanza/d.valor*100 | number: 2}}</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <th class="center-th">TOTAL:</th>
                                                    <th class="center-th">{{ cuadrarCajas | sumOfValue : 'balanza' }}</th>
                                                    <th class="center-th">{{ cuadrarCajas | sumOfValue : 'suma_real' }}</th>
													<th class="center-th">{{ cuadrarCajas | sumOfValue : 'pendiente' }}</th>
													<th class="center-th">{{ cuadrarCajas | sumOfValue : 'asignada' }}</th>
													<th class="center-th">{{ cuadrarCajas | sumOfValue : 'real' }}</th>
                                                    <th class="center-th">{{ cuadrarCajas | avgOfValue : 'porcentaje' | number: 2 }}</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="table_5">
                                            <thead>
                                                <tr>
                                                    <th class="center-th">GUIA</th>
                                                    <th class="center-th">CAJAS</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in guias" ng-click="row.expanded = !row.expanded">
                                                    <td>{{ row.guia }}</td>
                                                    <td>{{ row.cajas }}</td>
                                                    <td><button class="btn" ng-click="editarGuia(row)">Editar</button></td>
                                                </tr>
                                                <tr ng-repeat-end="" ng-repeat="d in row.detalle" ng-show="row.expanded">
                                                    <td>{{d.marca}}</td>
                                                    <td>{{d.valor}}</td>
                                                    <td>
														<button class="btn btn-sm btn-danger" ng-click="borrarGuiaMarca(row, d)">
															<i class="fa fa-trash"></i>
														</button>
													</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

							<div class="row">
								<div class="col-md-8">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center">ORIGEN</th>
													<th class="text-center">TIPO</th>
													<th class="text-center">MARCA</th>
													<th class="text-center">SALDO</th>
													<th class="text-center">ESTATUS</th>
													<th class="text-center">FINCA</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="row in saldos">
													<td class="text-center">{{ row.finca }}</td>
													<td class="text-center">CAJA</td>
													<td class="text-center">{{ row.marca }}</td>
													<td class="text-center">{{ row.pendiente-row.asignada }}</td>
													<td ng-click="modalAsignar(row)" class="text-center {{ row.pendiente-row.asignada > 0 ? 'bg-red-thunderbird bg-font-red-thunderbird' : 'bg-green-haze bg-font-green-haze' }}">{{ row.pendiente-row.asignada > 0 ? 'POR ASIGNAR' : 'ASIGNADO'  }}</td>
													<td class="text-center"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
                        </div>
                    </div>

					<div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="expand_cuadrar" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_8" aria-expanded="false"> MARCAS DE CAJAS </a>
                            </h4>
                        </div>
                        <div id="collapse_3_8" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
							<div class="row">
								<div class="col-md-12 text-right">
									<button class="btn btn-sm btn-success" style="margin: 10" ng-click="editarMarca({ status : 'Activo', tipo : 'CAJA' })">+ NUEVA</button>
								</div>
							</div>
                            <div class="row" id="marcas">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th></th>
													<th></th>
													<th class="text-center" colspan="3">PESO</th>
													<th></th>
													<th></th>
												</tr>
												<tr>
													<th class="text-center">CODIGO</th>
													<th class="text-center">MARCA</th>
													<th class="text-center">REC</th>
													<th class="text-center">MAX</th>
													<th class="text-center">MIN</th>
													<th class="text-center">ESTATUS</th>
													<th></th>
												</tr>
												<tr>
													<th><input type="text" class="form-control" ng-model="filtersMarca.codigo" ng-change="deleteProp(filtersMarca, 'codigo')"></th>
													<th><input type="text" class="form-control" ng-model="filtersMarca.nombre"></th>
													<th></th>
													<th></th>
													<th></th>
													<th><input type="text" class="form-control" ng-model="filtersMarca.status"></th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<!-- nombre : like , codigo : = -->
												<tr ng-repeat="row in marcas | filter : { nombre : filtersMarca.nombre } | filter : { codigo : filtersMarca.codigo } : true">
													<td>{{ row.codigo }}</td>
													<td>{{ row.nombre }}</td>
													<td>{{ row.requerimiento }}</td>
													<td>{{ row.max }}</td>
													<td>{{ row.min }}</td>
													<td>{{ row.status }}</td>
													<td>
														<button class="btn btn-sm" ng-click="editarMarca(row)">
															Editar <i class="fa fa-pencil"></i>
														</button>
														<button class="btn btn-sm btn-danger" ng-click="deleteMarca(row, $index)">
															Eliminar <i class="fa fa-trash"></i>
														</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

	<div class="modal fade" id="rangos" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Configurar Rangos</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Marca: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="config.rangos.newConfig.marca">
								<option ng-repeat="(key, value) in config.rangos.marcas" value="{{ value }}">{{ key }}</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Max: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="config.rangos.newConfig.max"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Min: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="config.rangos.newConfig.min"/>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="configuracion.save()">Guardar</button>
			</div>
		</div>
	</div>

    <div class="modal fade" id="cuadrar" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">ASIGNAR GUIA DE REMISION</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label"># GUIA DE REMISIÓN <span class="text-danger">*</span></label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.guia" ng-blur="giasDia(guia.fecha, guia.guia)" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">CODIGO PRODUCTOR </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.productor"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">CODIGO MAGAP </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.magap"/>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">SELLO SEGURIDAD </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.sello_seguridad"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">FINCA <span class="text-danger">*</span></label>
						</div>
						<div class="col-md-8">
							<select ng-model="guia.finca" class="form-control">
								<option value="">Seleccione</option>
								<option ng-selected="guia.finca == 1" value="1">Carolina</option>
								<option ng-selected="guia.finca == 4" value="4">Viviana</option>
							</select>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">FECHA <span class="text-danger">*</span></label>
						</div>
						<div class="col-md-8">
							<input class="form-control" 
								data-provide="datepicker" 
								data-date-format="yyyy-mm-dd" 
								ng-model="guia.fecha" 
								ng-change="giasDia(guia.fecha, guia.guia)" readonly>
						</div>
					</div>
				</div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table_5">
                            <thead>
                                <tr>
                                    <th class="center-th">CAJAS</th>
                                    <th class="center-th">CANTIDAD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[0]">
                                            <option value="{{marca.nombre}}" ng-repeat="marca in marcas | filter : { status : 'Activo' }">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[0]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[0] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[1]">
                                            <option value="{{marca.nombre}}" ng-repeat="marca in marcas | filter : { status : 'Activo' }">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[1]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[1] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[2]">
                                            <option value="{{marca.nombre}}" ng-repeat="marca in marcas | filter : { status : 'Activo' }">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[2]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[2] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[3]">
                                            <option value="{{marca.nombre}}" ng-repeat="marca in marcas | filter : { status : 'Activo' }">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[3]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[3] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[4]">
                                            <option value="{{marca.nombre}}" ng-repeat="marca in marcas | filter : { status : 'Activo' }">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[4]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[4] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[5]">
                                            <option value="{{marca.nombre}}" ng-repeat="marca in marcas | filter : { status : 'Activo' }">{{marca.nombre}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[5]]"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>          
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="guardarCuadrar()">Guardar</button>
			</div>
		</div>
	</div>

	<div class="modal fade" id="asignar" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Asignar Cajas</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Marca: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="asignar_form.marca">
								<option ng-repeat="(key, value) in saldos" value="{{ value.marca }}">{{ value.marca }}</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Cantidad: </label>
						</div>
						<div class="col-md-8">
							<input type="number" class="form-control" ng-model="asignar_form.cantidad">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Finca: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="asignar_form.finca">
								<option value="{{key}}" ng-repeat="(key, value) in available_fincas">{{value}}</option>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="asignarGuia()">Guardar</button>
			</div>
		</div>
	</div>

	<div class="modal fade" id="marca-modal" role="basic" aria-hidden="true" style="z-index: 9999!important;">
		<div class="modal-dialog modal-lg" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Marca</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Nombre: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.nombre">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Código: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.codigo">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Alias: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.nombre_alias">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Cliente: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.cliente">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Exportador: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="marcaSelected.exportador">
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Tipo de caja: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="marcaSelected.tipo">
								<option value="CAJA">CAJA</option>
								<option value="GAVETA">GAVETA</option>
							</select>
						</div>
					</div>
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Estatus: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="marcaSelected.status">
								<option value="Activo">Activo</option>
								<option value="Inactivo">Inactivo</option>
							</select>
						</div>
					</div>

					<div class="col-md-12" style="margin-top : 10px">
						<div class="col-md-6">
							<h3 class="inline-block no-margin">NOMBRE ALTERNATIVO</h3>
							<a class="btn btn-sm btn-success pull-right" style="border-radius : 50% !important; height : 30px; width : 30px;" ng-click="addAlias()">
								+
							</a>

							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th></th>
													<th>ACCIONES</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="t in marcaSelected.alias">
													<td>
														<input type="text" class="form-control" ng-model="t.alias">
													</td>
													<td>
														<button class="btn btn-sm btn-success" title="Guardar" ng-click="saveAlias(t)" ng-show="t.alias != ''">
															<i class="fa fa-save"></i>
														</button>
														<button class="btn btn-sm btn-danger" title="Eliminar" ng-click="deleteAlias(t, $index)">
															<i class="fa fa-trash"></i>
														</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<h3 class="inline-block no-margin">RANGOS</h3>
							<a class="btn btn-sm btn-success pull-right" style="border-radius : 50% !important; height : 30px; width : 30px;" ng-click="addRango()">
								+
							</a>

							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table">
											<thead>
												<tr>
													<th class="text-center" style="min-width: 100px;">FECHA</th>
													<th class="text-center">REC</th>
													<th class="text-center">MAX</th>
													<th class="text-center">MIN</th>
													<th class="text-center">ACCIONES</th>
												</tr>
											</thead>
											<tbody>
												<tr ng-repeat="r in marcaSelected.rangos">
													<td><input class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly ng-model="r.fecha"></td>
													<td><input type="number" class="form-control" ng-model="r.requerimiento"></td>
													<td><input type="number" class="form-control" ng-model="r.max"></td>
													<td><input type="number" class="form-control" ng-model="r.min"></td>
													<td>
														<button class="btn btn-sm btn-success" title="Guardar" ng-click="saveRango(r)">
															<i class="fa fa-save"></i>
														</button>
														<button class="btn btn-sm btn-danger" title="Eliminar" ng-click="deleteRango(r, $index)">
															<i class="fa fa-trash"></i>
														</button>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="saveMarca()">Guardar</button>
			</div>
		</div>
	</div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>