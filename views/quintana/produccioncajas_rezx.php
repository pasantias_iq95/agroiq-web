<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.alginLeft {
		text-align: left !important;
		padding-top: 20px;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}
	td-hidden {
		padding: 1px;
	}
	
	.subtittleTr {
	    padding: 2px 25px;
	}

	.Titlespan {
		font-size: 21px;
		/* text-align: left; */
		float: left;
	}

	@media print
		{
		body * { visibility: hidden; }
		.div2 * { visibility: visible; }
		.div2 { position: absolute; top: 40px; left: 30px; }
		}
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Cajas
     </h3>
     <div class="page-bar" ng-init="produccion.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Cajas</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label for="">
                Finca : 
            </label>
            <select oname="finca" id="finca" ng-model="params.finca" style="margin: 2px;height: 36px;">
            	<option value="0">Todos</option>
                <option ng-repeat="(key, value) in fincas" value="{{key}}">{{value}}</option>
            </select>
            <label for="">
                Año : 
            </label>
            <select name="year" id="year" ng-model="params.year" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in anios" value="{{key}}" ng-selected="key == params.year">{{value}}</option>
            </select>
			<label for="">
                Sem : 
            </label>
            <select name="semana" id="semana" ng-model="params.semana" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in semanas" value="{{key}}" ng-selected="key == params.semana">{{value}}</option>
            </select>
			<ng-calendarapp></ng-calendarapp>
        </div>
    </div>
    <?php include("./views/tags_produccion_cajas_".$this->session->agent_user.".php");?>  
	<div id="contenedor" class="div2">
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">BASE DE DATOS CAJAS</span>
						<div class="tools">
							<a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
						</div>
					</div>
					<div class="portlet-body" style="display: none;">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
										<th class="center-th">ID</th>
										<th class="center-th">FECHA</th>
										<th class="center-th">MES</th>
										<th class="center-th">PERIODO</th>
										<th class="center-th">SEMANA</th>
										<th class="center-th">PESO</th>
										<th class="center-th">CAJA</th>
									</tr>
									<tr>
										<th><input type="text" class="form-control form-filter" ng-model="search.id"/></th>
										<th><input type="text" class="form-control form-filter" ng-model="search.fecha"/></th>
										<th><input type="text" class="form-control form-filter" ng-model="search.mes"/></th>
										<th><input type="text" class="form-control form-filter" ng-model="search.periodo"/></th>
										<th><input type="text" class="form-control form-filter" ng-model="search.semana"/></th>
										<th><input type="text" class="form-control form-filter" ng-model="search.peso"/></th>
										<th><input type="text" class="form-control form-filter" ng-model="search.caja"/></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">RESUMEN CAJAS</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_2')"> Imprimir </a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_2')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
										<th class="center-th">ID</th>
										<th class="center-th">CAJAS</th>
										<th class="center-th">CANTIDAD</th>
										<th class="center-th">PROM</th>
										<th class="center-th">MAX</th>
										<th class="center-th">MIN</th>
										<th class="center-th">DESV</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th>TOTAL</th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO CAJAS SEMANAL</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO PESO CAJAS</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">ANÁLISIS DE RANGOS %</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">CAJAS POR MARCA</span>
						<div class="tools pull-right">
							
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">% SEMANAL DE CAJAS POR DEBAJO DEL PESO</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">RESUMEN POR SEMANA</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_3')"> Imprimir </a>
									</li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_3')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_3">
								<thead>
									<tr>
										<th class="center-th">SEM</th>
										<th class="center-th">CAJAS 1ra</th>
										<th class="center-th">CAJAS 2da</th>
										<th class="center-th">T CAJAS</th>
										<th class="center-th">CONV</th>
										<th class="center-th">CAJAS/HA</th>
										<th class="center-th">PESO PROM</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th>TOTAL</th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>