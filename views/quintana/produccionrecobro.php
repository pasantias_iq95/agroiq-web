<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.alginLeft {
		text-align: left !important;
		padding-top: 20px;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}
	td-hidden {
		padding: 1px;
	}
	
	.subtittleTr {
	    padding: 2px 25px;
	}

	.Titlespan {
		font-size: 21px;
		/* text-align: left; */
		float: left;
	}

	@media print
		{
		body * { visibility: hidden; }
		.div2 * { visibility: visible; }
		.div2 { position: absolute; top: 40px; left: 30px; }
		}
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Recobro
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Recobro</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            
        </div>
    </div>
	<div id="contenedor" class="div2">
	
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">% RECOBRO POR LOTE</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_2')"> Imprimir </a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_2')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
										<th class="center-th">LOTE</th>
										<th class="center-th">RACIMOS<br>ENFUNDADOS</th>
										<th class="center-th">RACIMOS<br>CAÍDOS</th>
										<th class="center-th">RACIMOS<br>COSECHADOS</th>
										<th class="center-th">% RECOBRO</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th>TOTAL</th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">% RECOBRO POR SEMANA</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_3')"> Imprimir </a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_3')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_3">
								<thead>
									<tr>
										<th class="center-th">SEMANA</th>
										<th class="center-th">RACIMOS<br>ENFUNDADOS</th>
										<th class="center-th">RACIMOS<br>CAÍDOS</th>
										<th class="center-th">RACIMOS<br>COSECHADOS</th>
										<th class="center-th">% RECOBRO</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th>TOTAL</th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">% RECOBRO POR ENFUNDADOR</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_3')"> Imprimir </a>
									</li>
									<li class="divider"> </li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_3')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_3">
								<thead>
									<tr>
										<th class="center-th">ENFUNDADOR</th>
										<th class="center-th">RACIMOS<br>ENFUNDADOS</th>
										<th class="center-th">RACIMOS<br>CAÍDOS</th>
										<th class="center-th">RACIMOS<br>COSECHADOS</th>
										<th class="center-th">% RECOBRO</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th>TOTAL</th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO % RECOBRO POR LOTE</span>
						<div class="tools pull-right">
							
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>