<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	/*table > th,td{
		text-align: center !important;
	}*/
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.portlet.box .dataTables_wrapper .dt-buttons{
        margin-top: 0px !important; 
        padding-bottom: 5px !important;
    }
    .fixedHeader-floating{top:60px!important;}
</style>
<div>
	<h3 class="page-title"> 
          Asistencia
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Asistencia</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <!-- <ng-calendarapp  search="search"></ng-calendarapp> -->
        </div>
    </div>
	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Personal</span>
		    <div class="tools">
                 <div class="btn-group">
                    <a id="addData" href="agregarAsistencia" class="btn blue"> Agregar </a>
                </div>
				 <div class="btn-group">
                    <button id="editData" class="btn blue"> Editar </button>
                </div>
	        </div>
		</div>
		<div class="portlet-body">
			<div class="table-container">
                <div class="table-actions-wrapper">
                </div>
				<table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer" id="asistencia">
                    <thead>
                        <tr >
                            <th class="center-th" width="1%"></th>
                            <th class="center-th" width="5%"> ID</th>
                            <th class="center-th" width="20%"> Fecha </th>
                            <th class="center-th" width="10%"> Personal </th>
                            <th class="center-th" width="10%"> Centro de<br>Costo </th>
                            <th class="center-th" width="20%"> Codigo </th>
                            <th class="center-th" width="20%"> Labor </th>
                            <th class="center-th" width="5%"> Lotes </th>
                            <th class="center-th" width="10%"> Hectareas </th>
                            <th class="center-th" width="35%"> Horas </th>
                            <th class="center-th" width="15%"> Observaciones </th>
                            <th class="center-th" width="5%"> Acciones </th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td><input type="text" class="form-control form-filter input-sm" name="id" id="id"> </td>
                            <td>
	                            <div class="input-group date date-picker margin-bottom-5" data-date-format="yyyy-mm-dd">
	                                <input type="text" class="form-control form-filter input-sm" readonly="" name="order_date_from" placeholder="From">
	                                <span class="input-group-btn">
	                                    <button class="btn btn-sm default" type="button">
	                                        <i class="fa fa-calendar"></i>
	                                    </button>
	                                </span>
	                            </div>
	                            <div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
	                                <input type="text" class="form-control form-filter input-sm" readonly="" name="order_date_to" placeholder="To">
	                                <span class="input-group-btn">
	                                    <button class="btn btn-sm default" type="button">
	                                        <i class="fa fa-calendar"></i>
	                                    </button>
	                                </span>
	                            </div>
                                <input type="hidden" id="EnableEdit" name="EnableEdit" class="form-filter" value="0">
                            </td>
                            <td><input type="text" class="form-control form-filter input-sm" name="personal" id="personal"> </td>
                            <td><input type="text" class="form-control form-filter input-sm" name="centro_costo" id="centro_costo"> </td>
                            <td>
                            	<input type="text" class="form-control form-filter input-sm" name="codigo" id="codigo"> 
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="labor" id="labor"> 
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="lotes" id="lotes"> 
                            </td>
                            <td>
                            	<input type="text" class="form-control form-filter input-sm" name="hectareas" id="hectareas"> 
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="horas_ordinarias" id="horas_ordinarias"> 
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="observaciones" id="observaciones"> 
                            </td>
                            <td>
                                <div class="margin-bottom-5">
                                    <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                        <i class="fa fa-search"></i> </button>
                                </div>
                                <button class="btn btn-sm red btn-outline filter-cancel" >
                                    <i class="fa fa-times"></i> </button>
                            </td>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>
			</div>
		</div>
	</div>
</div>