<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}
</style>
<div ng-controller="rpersonal" ng-cloak>
	<h3 class="page-title"> 
        Resultados Personal
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Personal</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
		 	Finca :
            <select class="input-sm" ng-model="filters.finca" ng-change="init()" style="width: 150px">
				<option value="1">Maria Maria</option>
				<option value="2">Domenica</option>
			</select>
        </div>
    </div>
	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">PERSONAL POR PERSONAL</span>
		    <div class="tools">
	            <a href="javascript:;" class="collapse" data-original-title="Expandir/Contraer" title=""> </a>
	        </div>
		</div>
		<div class="portlet-body">
			<!--<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
			                <tr role="row" class="heading">
			                    <th width="{{withTable}}" class="center-th"> COD  </th>
			                    <th width="{{withTable}}" class="center-th"> LOTERO  </th>
			                    <th width="{{withTable}}" class="center-th"> LOTE  </th>
			                    <th width="{{withTable}}" class="center-th"> LABOR  </th>
			                    <th width="{{withTable}}" class="center-th" ng-repeat="(key, value) in dataFilters">{{value}}</th>
			                </tr>
					<tbody>
						<tr ng-repeat-start="lotero in loteros_aereo | filter : search" ng-click="openDetalle(lotero)">
							<td width="{{withTable}}" class="center-th">{{$index + 1}}</td>
							<td width="{{withTable}}">{{lotero.lotero}}</td>
							<td width="{{withTable}}">{{lotero.lote}}</td>
							<td width="{{withTable}}"></td>
							<td width="{{withTable}}" ng-repeat="(key, value) in dataFilters">
								<i class="{{lotero.umbral[value]}}"></i>	
								{{lotero.campo[value] | number : 2}}
							</td>
						</tr>	
						<tr ng-show="lotero.expanded" ng-repeat-end="">
							<td colspan="{{colspan}}">
								<table class="table table-striped table-bordered table-hover">
									<tr ng-repeat="labor in lotero.labores.aereo" ng-click="openDetalle(labor)">
										<td width="{{withTable}}">-</td>
										<td width="{{withTable}}">-</td>
										<td width="{{withTable}}">-</td>
										<td width="{{withTable}}" class="left-td">{{labor.labor}}</td>
										<td width="{{withTable}}" ng-repeat="(key, value) in dataFilters">
											<i class="{{labor.umbral[value]}}"></i>
											{{labor.campo[value] | number : 2}}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr ng-repeat="(llave , valor) in totales">
							<td>Total : </td>
							<td></td>
							<td></td>
							<td> </td>
							<td width="{{withTable}}" ng-repeat="(key, value) in dataFilters">
								<i class="{{umbral[value]}}"></i> {{valor[value]}}
							</td>
						</tr>
				</table>
			</div>-->

			<div id="table-react"></div>
		</div>
	</div>
</div>
<script src="componentes/FilterableSortableTable.js"></script>