
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-jungle">
                            <span class="counter_tags" data-value="">{{tags.temp_minima.value}}</span>
                            <small class="font-green-jungle"></small>
                        </h3>
                        <small>TEMP. MINIMA</small>
                    </div>
                    <div class="icon">
                        <i class="icon-pie-chart"></i>
                    </div>
                </div>
                <div class="progress-info">
                    <div class="progress">
                        <span style="width: 0%;" class="progress-bar progress-bar-success">
                            <span class="sr-only"></span>
                        </span>
                    </div>
                    <div class="status">
                        <div class="status-title"></div>
                        <div class="status-number">{{tags.temp_minima.acumulado}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-jungle">
                            <span class="counter_tags" data-value="">{{tags.temp_maxima.value}}</span>
                            <small class="font-green-jungle"></small>
                        </h3>
                        <small>TEMP. MAXIMA</small>
                    </div>
                    <div class="icon">
                        <i class="icon-pie-chart"></i>
                    </div>
                </div>
                <div class="progress-info">
                    <div class="progress">
                        <span style="width: 0%;" class="progress-bar progress-bar-success">
                            <span class="sr-only"></span>
                        </span>
                    </div>
                    <div class="status">
                        <div class="status-title"></div>
                        <div class="status-number">{{tags.temp_maxima.acumulado}}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="dashboard-stat2 ">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-jungle">
                            <span class="counter_tags" data-value="">{{tags.lluvia.value}}</span>
                            <small class="font-green-jungle"></small>
                        </h3>
                        <small>LLUVIA (MM)</small>
                    </div>
                    <div class="icon">
                        <i class="icon-pie-chart"></i>
                    </div>
                </div>
                <div class="progress-info">
                    <div class="progress">
                        <span style="width: 0%;" class="progress-bar progress-bar-success">
                            <span class="sr-only"></span>
                        </span>
                    </div>
                    <div class="status">
                        <div class="status-title"></div>
                        <div class="status-number">LLUVIA ACUMULADA {{tags.lluvia.acumulado}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>                  