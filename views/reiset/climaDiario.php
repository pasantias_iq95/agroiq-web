<?php
    $cdn = "http://cdn.procesos-iq.com/";
?>
<style>
    .center-th {
        text-align: center;
    }
    .left-th {
        text-align: left !important;
    }
    .charts {
        height:500px;
    }
    .charts-200 {
        height:200px;
    }
    .charts-300 {
        height : 300px;
    }
    .cursor td, .cursor th{
        cursor: pointer;
    }
    .asc {
        background-position: right;
        background-repeat: no-repeat;
        padding-right: 30px;
        background-image : url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAZ0lEQVQ4y2NgGLKgquEuFxBPAGI2ahhWCsS/gDibUoO0gPgxEP8H4ttArEyuQYxAPBdqEAxPBImTY5gjEL9DM+wTENuQahAvEO9DMwiGdwAxOymGJQLxTyD+jgWDxCMZRsEoGAVoAADeemwtPcZI2wAAAABJRU5ErkJggg==');
    }
    .desc {
        background-position: right;
        background-repeat: no-repeat;
        padding-right: 30px;
        background-image : url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAZUlEQVQ4y2NgGAWjYBSggaqGu5FA/BOIv2PBIPFEUgxjB+IdQPwfC94HxLykus4GiD+hGfQOiB3J8SojEE9EM2wuSJzcsFMG4ttQgx4DsRalkZENxL+AuJQaMcsGxBOAmGvopk8AVz1sLZgg0bsAAAAASUVORK5CYII=');
    }
    .verticalcenter{
        position: relative;
        top: 50%;
        -webkit-transform: translateY(20%);
        -o-transform: translateY(20%);
        transform: translateY(20%);
    }
    .no-margin {
        margin : 0;
    }
</style>

<div ng-controller="controller">
    <div class="page-head">
        <div class="page-title">
            <h1>Reporte Diario</h1>
        </div>
    </div>

    <div class="page-bar no-margin">
        <div class="page-toolbar">
            <ng-calendarapp search="search"></ng-calendarapp>
            <div class="col-md-4 pull-right">
                <select class="form-control">
                    <option value="GUARUMAL" selected>GUARUMAL</option>
                </select>
            </div>
        </div>
    </div>

    <div class="page-bar" ng-hide="difDias">
        <div class="page-toolbar">
            <div class="col-md-2 pull-right">
                <div class="input-group">
                    <input id="timepicker2" type="text" class="form-control timepicker timepicker-24">
                    <span class="input-group-btn">
                        <button class="btn default" type="button">
                            <i class="fa fa-clock-o"></i>
                        </button>
                    </span>
                </div>
            </div>

            <div class="col-md-2 pull-right">
                <div class="input-group">
                    <input id="timepicker" type="text" class="form-control timepicker timepicker-24">
                    <span class="input-group-btn">
                        <button class="btn default" type="button">
                            <i class="fa fa-clock-o"></i>
                        </button>
                    </span>
                </div>
            </div>

            <button class="btn pull-right" ng-show="isChangeHour" ng-click="init()">Aplicar</button>
        </div>
    </div>

    <ul class="page-breadcrumb breadcrumb">
        <li>
            <span class="active"></span>
        </li>
    </ul>

    <div id="indicadores">
        <?php include('views/quintana/tags_clima_diario.php') ?>
    </div>

    <div class="row">
        <div class="portlet box green">
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6" id="datatable">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>DETALLE</th>
                                        <th>ACUM</th>
                                        <th>AVG</th>
                                        <th>MAX</th>
                                        <th>MIN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in datatable">
                                        <td>{{ row.detalle }}</td>
                                        <td>{{ row.acumulado > 0 ? (row.acumulado | number: 2) : '' }}</td>
                                        <td>{{ row.avg > 0 ? (row.avg | number: 2) : '' }}</td>
                                        <td>{{ row.max }}</td>
                                        <td>{{ row.min }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <h3>Dirección Viento</h3>
                        <div id="grafica-direccion-viento" class="charts-200 verticalcenter">

                        </div>
                    </div>
                    <div class="col-md-3">
                        <h3>Velocidad Viento</h3>
                        <div id="grafica-velocidad-viento" class="charts-300">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="portlet box green">
            <div class="portlet-title">
                <span class="caption">RAD SOLAR POR HORA (W/M2)</span>
                <div class="tools">

                </div>
            </div>
            <div class="portlet-body">
                <div id="grafica_horas_luz" class="charts"></div>
            </div>
        </div>
    </div>
</div>