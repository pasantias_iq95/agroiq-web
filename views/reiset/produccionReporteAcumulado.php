<style>
	td, th  {
		text-align: center;
	}
	.edad {
		padding: 3px !important;
	}
	.form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
</style>
<div ng-controller="controller">
	<h3 class="page-title"> 
          Reporte Producción
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             
         </ul>
         <div class="page-toolbar">
		 	<label for="">Sector</label>	
            <select style="margin: 2px;height: 36px;" class="input-sm" ng-model="filters.sector" ng-change="changeSector()">
                <option value="">TODOS</option>
                <option ng-repeat="s in sectores" value="{{s}}" ng-selected="s == filters.sector">{{s}}</option>
            </select>
			Desde : <select class="input-sm" ng-model="filters.desde_concated" ng-change="changeRangeDate()"> 
						<option value="{{sem.concated}}" ng-repeat="sem in semanas" ng-selected="sem.concated == filters.desde_concated">{{sem.concated}}</option> 
					</select>
			Hasta : <select class="input-sm" ng-model="filters.hasta_concated" ng-change="changeRangeDate()"> 
						<option value="{{sem.concated}}" ng-repeat="sem in semanas" ng-selected="sem.concated == filters.hasta_concated">{{sem.concated}}</option> 
					</select>
        </div>
    </div>
    
	<div id="contenedor" class="div2">	
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption"></span>
						<div class="tools">
							
						</div>
					</div>
					<div class="portlet-body">
						<div id="table-reporte"></div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>