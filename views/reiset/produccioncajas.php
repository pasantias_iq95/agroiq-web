<style>
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
    .right-th {
		text-align: right !important;
		padding-right: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	.alginLeft {
		text-align: left !important;
		padding-top: 20px;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}
	td-hidden {
		padding: 1px;
	}
	
	.subtittleTr {
	    padding: 2px 25px;
	}

	.Titlespan {
		font-size: 21px;
		/* text-align: left; */
		float: left;
	}
    .delete  {
        background-color : #c8d8e8 !important;
    }

	.highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
	.form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
</style>
<div ng-controller="produccion" ng-cloak>
	<h3 class="page-title"> 
          Cajas
     </h3>
     <div class="page-bar" ng-init="produccion.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Cajas</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <label class="btn red-thunderbird btn-outline btn-circle btn-sm" ng-click="convertKg()">
                {{ table.unidad }}
            </label>
            <label for="">
                Finca : 
            </label>
            <select oname="finca" id="finca" ng-model="table.finca" style="margin: 2px;height: 36px;">
            	<option value="">GUARUMAL</option>
            </select>
            <label for="">
                Año : 
            </label>
            <select name="year" id="year" ng-model="table.year" style="margin: 2px;height: 36px;">
                <option ng-repeat="(key, value) in anios" value="{{key}}" ng-selected="key == table.year">{{value}}</option>
            </select>
            <label>Fecha</label>
            <input class="input-sm" sytle="height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" ng-model="table.fecha_inicial" ng-change="changeRangeDate()" readonly>
        </div>
    </div>

    <div class="page-bar">
        <div class="page-toolbar">
            <div>
                <label for="requerimiento">Umbral ({{ table.unidad | uppercase }}): </label>
                <input type="number" step="0.01" name="requerimiento" id="requerimiento" class="input-sm" ng-model="table.requerimiento" ng-change="editRequerimiento = true"/>
                <button class="btn btn-primary" ng-show="editRequerimiento" ng-click="guardarRequerimiento()">Guardar <i class="fa fa-check green-jungle"></i></button>
            </div>
        </div>
    </div>
    
	<div id="contenedor" class="div2">

        <?php include('./views/reiset/tags_produccion_cajas.php') ?>
		
		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">RESUMEN CAJAS</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_2')"> Imprimir </a>
									</li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_2')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive" id="div_table_2">
							<table class="table table-striped table-bordered table-hover" id="table_2">
								<thead>
									<tr>
										<th class="center-th">TIPO</th>
										<th class="center-th">MARCA</th>
										<th class="center-th">CANTIDAD</th>
                                        <th class="center-th">CONV</th>
										<th class="center-th">TOTAL ({{ table.unidad | uppercase }})</th>
										<th class="center-th">PROM</th>
										<th class="center-th">MAX</th>
										<th class="center-th">MIN</th>
										<th class="center-th">DESV</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="row in resumen">
										<td>{{ row.tipo }}</td>
										<td>{{ row.marca }}</td>
										<td>{{ row.cantidad }}</td>
                                        <td>{{ row.conv }}</td>
										<td>{{ row.total_kg | number: 2 }}</td>
										<td>{{ row.promedio | number: 2 }}</td>
										<td>{{ row.maximo | number: 2 }}</td>
										<td>{{ row.minimo | number: 2 }}</td>
										<td>{{ row.desviacion | number: 2 }}</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<th>TOTAL</th>
										<th></th>
										<th class="center-th">{{ (resumen | sumOfValue:'cantidad') }}</th>
                                        <th class="center-th">{{ tags.cajas40 }}</th>
										<th class="center-th">{{ (resumen | sumOfValue:'total_kg') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'promedio') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'maximo') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'minimo') | number : 2 }}</th>
										<th class="center-th">{{ (resumen | avgOfValue:'desviacion') | number : 2 }}</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO CAJAS SEMANAL</span>
						<div class="tools pull-right">
							<select class="form-control" ng-model="table.var" ng-change="loadSemanal()">
								<option value="CONV">CONV</option>
								<option value="CONV/HA">CONV/HA</option>
							</select>
						</div>
					</div>
					<div class="portlet-body">
						<div id="cajas_semanal" style="height: 400px;">

						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption"> EXCEDENTE </span>
						<div class="tools">
						
						</div>
					</div>
					<div class="portlet-body" id="tablas">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">MARCA</th>
                                        <th class="text-center">TOTAL ({{ table.unidad | uppercase }}) </th>
                                        <th class="text-center">CAJAS</th>
                                        <th class="text-center">DOLARES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="row in tablasDiferencias">
                                        <td>{{ row.marca }}</td>
                                        <td>{{ row.kg_diff }}</td>
                                        <td>{{ row.cajas }}</td>
                                        <td>{{ row.dolares }}</td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th class="text-center">{{ tablasDiferencias | sumOfValue: 'kg_diff' | number: 2 }}</th>
                                        <th class="text-center">{{ tablasDiferencias | sumOfValue: 'cajas' | number: 2 }}</th>
                                        <th class="text-center">{{ tablasDiferencias | sumOfValue: 'dolares' | number: 2 }}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
					</div>
				</div>
			</div>
		</div>

        <div class="row">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">  </span>
						<div class="tools">
						
						</div>
					</div>
					<div class="portlet-body">
                        <div class="row" id="barras">
                            <div ng-repeat="marca in marcasBarras | orderObjectBy : 'sec'" class="col-md-12">
                                <div class="col-md-6">
                                    <div id="barras_{{marca.marca}}" class="chart"></div>
                                </div>
                                <div class="col-md-6">
                                    <div id="pastel_{{marca.marca}}" class="chart"></div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>

		<div class="row hide">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">HISTÓRICO PESO CAJAS</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row hide">
			<div class="col-md-6">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">ANÁLISIS DE RANGOS %</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">CAJAS POR MARCA</span>
						<div class="tools pull-right">
							
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row hide">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">% SEMANAL DE CAJAS POR DEBAJO DEL PESO</span>
						<div class="tools pull-right">
							<select class="form-control">
								<option>CAJAS</option>
							</select>
						</div>
					</div>
					<div class="portlet-body" style="height: 400px;">
						
					</div>
				</div>
			</div>
		</div>

		<div class="row hide">
			<div class="col-md-12">
				<div class="portlet box green-haze">
					<div class="portlet-title">
						<span class="caption">RESUMEN POR SEMANA</span>
						<div class="tools">
							<div class="btn-group">
								<a class="btn dark btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
									Exportar <i class="fa fa-angle-down"></i>
								</a>
								<ul class="dropdown-menu pull-right">
									<li>
										<a href="javascript:;" ng-click="exportPrint('table_3')"> Imprimir </a>
									</li>
									<li>
										<a href="javascript:;" ng-click="exportExcel('table_3')">Excel</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-hover" id="table_3">
								<thead>
									<tr>
										<th class="center-th">SEM</th>
										<th class="center-th">CAJAS 1ra</th>
										<th class="center-th">CAJAS 2da</th>
										<th class="center-th">T CAJAS</th>
										<th class="center-th">CONV</th>
										<th class="center-th">CAJAS/HA</th>
										<th class="center-th">PESO PROM</th>
									</tr>
								</thead>
								<tbody>

								</tbody>
								<tfoot>
									<tr>
										<th></th>
										<th>TOTAL</th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i> Cajas </div>
                <div class="tools">
                </div>
            </div>
            <div class="portlet-body">
                <div class="panel-group accordion" id="accordion3">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3" aria-expanded="false"> HISTORICO EXCEDENTE </a>
                            </h4>
                        </div>
                        <div id="collapse_3_3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <div class="pull-right col-md-2">
                                    <select class="form-control input-sm" ng-model="filters.var" ng-change="changeExcedente()">
                                        <option value="exce">LB</option>
                                        <option value="cajas">CAJAS</option>
                                        <option value="dolares">DLL</option>
                                    </select>
                                </div>
                                <div id="table-historico-excedente"></div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1" aria-expanded="false"> BASE DE DATOS CAJAS </a>
                            </h4>
                        </div>
                        <div id="collapse_3_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                                <div class="tools pull-right">
                                    <div id="actions-lista">
                                        <button class="btn green" ng-click="getRegistrosBase()"><i class="fa fa-refresh"></i></button>
                                        <select class="input-sm" style="color: #555;" ng-model="searchTable.limit" ng-change="searchTable.changePagination()">
                                            <option value="{{ registros.length }}">TODOS</option>
                                            <option ng-repeat="opt in searchTable.optionsPagination" value="{{ opt > 0 ? opt : '' }}" ng-selected="opt == searchTable.limit">{{ opt > 0 ? opt : 'TODOS' }}</option>
                                        </select>
                                        <div class="btn-group">
                                            <a class="btn dark btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> 
                                                Exportar <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <li>
                                                    <a href="javascript:;" ng-click="exportPrint('table_1')"> Imprimir </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;" ng-click="exportExcel('table_1')">Excel</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <button class="btn red-thunderbird" ng-click="eliminar()">Eliminar</button>
                                    </div>
                                </div>
                                <ul class="nav nav-tabs tabs-reversed">
                                    <li class="dropdown">
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Configuración
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li class="">
                                                <a href="#configuracion" ng-click="setTipo('Tipo')" tabindex="-1" data-toggle="modal" aria-expanded="false"> Tipo </a>
                                            </li>
                                            <li class="">
                                                <a href="#configuracion" ng-click="showRango()" tabindex="-1" data-toggle="modal" aria-expanded="false"> Rangos (Marca) </a>
                                            </li>
                                            <li class="">
                                                <a href="#configuracion" ng-click="setTipo('Exportadora')" tabindex="-1" data-toggle="modal" aria-expanded="false"> Exportadora </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="" ng-click="hideActions()">
                                        <a href="#createproductos" data-toggle="tab" aria-expanded="true"> Configurar </a>
                                    </li>
                                    <li class="active" ng-click="showActions()">
                                        <a href="#listado" data-toggle="tab" aria-expanded="false"> Listado </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade" id="createproductos" ng-include="'./views/reiset/templetes/produccionCajas/configurar.html?i=<?=rand(10,100)?>'"></div>
                                    <div class="tab-pane fade active in" id="listado" ng-include="'./views/reiset/templetes/produccionCajas/listado.html?i=<?=rand(10,100)?>'"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a id="expand_cuadrar" ng-click="getCuadrar()" class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2" aria-expanded="false"> CUADRE DE CAJAS </a>
                            </h4>
                        </div>
                        <div id="collapse_3_2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-primary pull-right" ng-click="modalCuadrar()"> GUIA </a>                                    
                                    <a ng-show="isValidGuia()" class="btn red pull-right" ng-click="procesarDia()">Procesar</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="table_4">
                                            <thead>
                                                <tr>
                                                    <th class="center-th">TIPO</th>
                                                    <th class="center-th">MARCA</th>
                                                    <th class="center-th">BALANZA</th>
                                                    <th class="center-th">REAL</th>
                                                    <th class="center-th">%</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in cuadrarCajas" ng-click="row.expanded = !row.expanded">
                                                    <td class="center-th">{{ row.tipo }}</td>
                                                    <td class="center-th">{{ row.marca }}</td>
                                                    <td class="center-th">{{ row.balanza }}</td>
                                                    <td class="center-th">{{ row.suma_real }}</td>
                                                    <td class="center-th">{{ row.porcentaje | number: 2 }}</td>
                                                </tr>
                                                <tr ng-repeat-end="row" ng-repeat="d in row.detalle" ng-show="row.expanded">
                                                    <td></td>
                                                    <td>{{d.guia}}</td>
                                                    <td></td>
                                                    <td>{{d.valor}}</td>
                                                    <td>{{row.balanza/d.valor*100 | number: 2}}</td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <th class="center-th">TOTAL:</th>
                                                    <th class="center-th">{{ cuadrarCajas | sumOfValue : 'balanza' }}</th>
                                                    <th class="center-th">{{ cuadrarCajas | sumOfValue : 'suma_real' }}</th>
                                                    <th class="center-th">{{ cuadrarCajas | avgOfValue : 'porcentaje' | number: 2 }}</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="table_5">
                                            <thead>
                                                <tr>
                                                    <th class="center-th">GUIA</th>
                                                    <th class="center-th">CAJAS</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat-start="row in guias" ng-click="row.expanded = !row.expanded">
                                                    <td>{{ row.guia }}</td>
                                                    <td>{{ row.cajas }}</td>
                                                    <td><button class="btn" ng-click="editarGuia(row)">Editar</button></td>
                                                </tr>
                                                <tr ng-repeat-end="" ng-repeat="d in row.detalle" ng-show="row.expanded">
                                                    <td>{{d.marca}}</td>
                                                    <td>{{d.valor}}</td>
                                                    <td><button class="btn input-sm btn-danger" ng-click="borrarGuiaMarca(row, d)"><i class="fa fa-trash"></i></button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

	<div class="modal fade" id="rangos" role="basic" aria-hidden="true">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Configurar Rangos</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Marca: </label>
						</div>
						<div class="col-md-8">
							<select class="form-control" ng-model="config.rangos.newConfig.marca">
								<option ng-repeat="(key, value) in config.rangos.marcas" value="{{ value }}">{{ key }}</option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Max: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="config.rangos.newConfig.max"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">Min: </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="config.rangos.newConfig.min"/>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="configuracion.save()">Guardar</button>
			</div>
		</div>
	</div>

    <div class="modal fade" id="cuadrar" role="basic" aria-hidden="true">
		<div class="modal-dialog" style="background: white;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">ASIGNAR GUIA DE REMISION</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label"># GUIA DE REMISIÓN </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.guia" ng-blur="giasDia(guia.fecha, guia.guia)" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">CODIGO PRODUCTOR </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.productor"/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">CODIGO MAGAP </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.magap"/>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">SELLO SEGURIDAD </label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" ng-model="guia.sello_seguridad"/>
						</div>
					</div>
				</div>
                <div class="row">
					<div class="col-md-12">
						<div class="col-md-4">
							<label class="control-label">FECHA </label>
						</div>
						<div class="col-md-8">
							<input class="form-control" data-provide="datepicker" data-date-format="yyyy-mm-dd" ng-model="guia.fecha" ng-change="giasDia(guia.fecha, guia.guia)" readonly>
						</div>
					</div>
				</div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="table_5">
                            <thead>
                                <tr>
                                    <th class="center-th">CAJAS</th>
                                    <th class="center-th">CANTIDAD</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[0]">
                                            <option value="{{marca}}" ng-repeat="marca in marcas">{{marca}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[0]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[0] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[1]">
                                            <option value="{{marca}}" ng-repeat="marca in marcas">{{marca}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[1]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[1] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[2]">
                                            <option value="{{marca}}" ng-repeat="marca in marcas">{{marca}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[2]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[2] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[3]">
                                            <option value="{{marca}}" ng-repeat="marca in marcas">{{marca}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[3]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[3] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[4]">
                                            <option value="{{marca}}" ng-repeat="marca in marcas">{{marca}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[4]]"/></td>
                                </tr>
                                <tr ng-if="marcasSeleccionadas[4] != ''">
                                    <td>
                                        <select class="form-control" ng-model="marcasSeleccionadas[5]">
                                            <option value="{{marca}}" ng-repeat="marca in marcas">{{marca}}</option>
                                        </select>
                                    </td>
                                    <td><input type="number" ng-model="guia.marca[marcasSeleccionadas[5]]"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>          
                </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn blue" ng-click="guardarCuadrar()">Guardar</button>
			</div>
		</div>
	</div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>