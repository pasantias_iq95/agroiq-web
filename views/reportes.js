var myChartPrincial = {};
function loadScript(step , options , reload , optionsPrincipal){
	    // ECHARTS
    require.config({
        paths: {
            echarts: '/assets/global/plugins/echarts/'
        }
    });

    // DEMOS
    require(
        [
            'echarts',
            'echarts/chart/bar',
            'echarts/chart/chord',
            'echarts/chart/eventRiver',
            'echarts/chart/force',
            'echarts/chart/funnel',
            'echarts/chart/gauge',
            'echarts/chart/heatmap',
            'echarts/chart/k',
            'echarts/chart/line',
            'echarts/chart/map',
            'echarts/chart/pie',
            'echarts/chart/radar',
            'echarts/chart/scatter',
            'echarts/chart/tree',
            'echarts/chart/treemap',
            'echarts/chart/venn',
            'echarts/chart/wordCloud'
        ],
        function(ec) {
            //--- BAR ---
	       	if(step == 0){
	       		if(options.main){
					var label_principal = [] ,categories = [], series_principal = [] , lote_principal = "" , count_p = [];
		            var promedio = [] , count = 0 , tpromedio = 0;
        			series_principal.push({
        				name : "lotes", 
        				type : "line",
        				data : []
        			});
        			label_principal.push("lotes");
        			
		            for (var key in options.main) {
		        		categories.push(key);
	            		if(options.main.hasOwnProperty(key)) {
	            			if(options.main[key].hasOwnProperty("lotes")){
		        				promedio[count] = [];
		        				count_p = options.main[key].lotes.map(function(num) { promedio[count].push(num.sum/num.avg); return num.sum/num.avg; });
	        					tpromedio = promedio[count].reduce(function(a, b) { return parseFloat(a) + parseFloat(b); });
	        					
		        				series_principal[0].data.push(
		        					Math.round(tpromedio / promedio[count].length)
		        				);
		        				count++;
		        				// series_principal[1].data.push(
		        				// 	Math.round(options.main[key].lote.sum / options.main[key].lote.avg)
		        				// );
	            			}else{
	            				series_principal[0].data.push(0);
	            				// series_principal[1].data.push(0);
	            			}
        				}
	        		}
	        		console.log(categories)
	        		console.log(label_principal)
	        		console.log(series_principal)
					myChartPrincial = ec.init(document.getElementById('echarts_bar') , 'infographic');
		            myChartPrincial.setOption({
		                tooltip: {
		                    trigger: 'axis'
		                },
		                legend: {
		                    data: label_principal
		                },
		                toolbox: {
		                    show: true,
		                    feature: {
		                        mark: {
		                            show: false
		                        },
		                        dataView: {
		                            show: false,
		                            readOnly: true
		                        },
		                        magicType: {
		                            show: true,
		                            type: ['line', 'bar']
		                        },
		                        restore: {
		                            show: true
		                        },
		                        saveAsImage: {
		                            show: true
		                        }
		                    }
		                },
		                calculable: true,
		                xAxis: [{
		                    type: 'category',
		                    data: categories
		                }],
		                yAxis: [{
		                    type: 'value',
		                    splitArea: {
		                        show: true
		                    }
		                }],
		                series: series_principal
		            });
		            myChartPrincial.setTheme('infographic');
	            }

	       		var graphic = {
	       			labor : false,
	       			lote : false
	       		};

	        	if(options.data_labores){
		        	var label = [] , series = [] , labor = "" , count = 0;
	        		for (var p in options.data_labores) {
  						if (options.data_labores.hasOwnProperty(p)) {
		        			label.push(options.data_labores[p].labor);
		        			if(labor != options.data_labores[p].labor){
		        				labor = options.data_labores[p].labor;
			        			series[count] = {
			        				name : options.data_labores[p].labor , 
			        				type : "bar",
			        				data : []
			        			};
			        			count++;
		        			}
		        			series[(count-1)].data.push({value : Math.round((options.data_labores[p].promedio / options.data_labores[p].avg))});
	        			}
	        		}
	        		/*=========================================
	        		=            GRAFICA PRINCIPAL            =
	        		=========================================*/
	        		
	        		
	        		
	        		/*=====  End of GRAFICA PRINCIPAL  ======*/
	        		
		            var myChartLabor = ec.init(document.getElementById('echarts_bar_labores') , 'green');
		            myChartLabor.setOption({
		            	title : {
					        text: 'Labores',
					        subtext: 'Promedios Labores',
					        x:'center'
					    },
		                tooltip: {
		                    trigger: 'axis'
		                },
		                legend: {
		                    data: label,
		                    y : 'bottom'
		                },
		                toolbox: {
		                    show: true,
		                    feature: {
		                        mark: {
		                            show: false
		                        },
		                        dataView: {
		                            show: false,
		                            readOnly: false
		                        },
		                        magicType: {
		                            show: false,
		                            type: ['line', 'bar']
		                        },
		                        restore: {
		                            show: true
		                        },
		                        saveAsImage: {
		                            show: true
		                        }
		                    }
		                },
		                calculable: true,
		                xAxis: [{
		                    type: 'category',
		                    data: [""]
		                }],
		                yAxis: [{
		                    type: 'value',
		                    splitArea: {
		                        show: true
		                    }
		                }],
		                series: series
		            });

			        myChartLabor.setTheme('infographic');
			        graphic.labor = true;
	        	}
	        	if(options.lote){
	        		var label = [] , series = [] , labor = "" , count = 0 , promedio = [] , label_promedio = [] , tpromedio = 0;
	        		for (var key in options.lote) {
	        			if (options.lote.hasOwnProperty(key)) {
	        				label.push(key);
	        				promedio[count] = [];
	        				label_promedio = options.lote[key].labores.map(function(num) { promedio[count].push(num.promedio); return num.promedio; });
	        				tpromedio = promedio[count].reduce(function(a, b) { return parseFloat(a) + parseFloat(b); });
	        				series[count] = {
	        					name : key,
	        					type : "bar",
	        					data :  [Math.round((tpromedio / promedio[count].length))]
	        				}
	        				count++;
	        			}
	        		}

					var myChartLotes = ec.init(document.getElementById('echarts_bar_lotes') , 'green');
					myChartLotes.setOption({
							title : {
						        text: 'Lotes',
						        subtext: 'Promedios Lotes',
						        x:'center'
						    },
			                tooltip: {
			                    trigger: 'axis'
			                },
			                legend: {
			                    data: label,
			                    y : 'bottom'
			                },
			                toolbox: {
			                    show: true,
			                    feature: {
			                        mark: {
			                            show: false
			                        },
			                        dataView: {
			                            show: false,
			                            readOnly: false
			                        },
			                        magicType: {
			                            show: false,
			                            type: ['line', 'bar']
			                        },
			                        restore: {
			                            show: true
			                        },
			                        saveAsImage: {
			                            show: true
			                        }
			                    }
			                },
			                calculable: true,
			                xAxis: [{
			                    type: 'category',
			                    data: [""]
			                }],
			                yAxis: [{
			                    type: 'value',
			                    splitArea: {
			                        show: true
			                    }
			                }],
			                series: series
					});
			        myChartLotes.setTheme('infographic');
			        graphic.lote = true;
	        	}
	            window.onresize = function(){
	            	myChartPrincial.resize();
	            	if(graphic.labor)
	            		myChartLabor.resize();
	            	if(graphic.lote)
	            		myChartLotes.resize();
	            }
            }else if(step == 1){
        		var info = options.lote.labores;
	            var label = [] , series = [] , labor = "" , count = 0;
	            for (var key in info) {
	            	if (info.hasOwnProperty(key)) {
        				label.push(key);
        				if(labor != key){
        					labor = key;
		        			series[count] = {
		        				name : key, 
		        				type : "bar",
		        				data : []
		        			};
		        			count++;
        				}
        				series[(count-1)].data.push({value : Math.round(info[key].promedio)});
        			}
        		}
        		// console.log(label)
	            var myChartLaborLote = ec.init(document.getElementById('echarts_bar_labores_lotes') , 'green');
	            myChartLaborLote.setOption({
	                tooltip: {
	                    trigger: 'axis'
	                },
	                legend: {
	                    data: label,
	                    y : 'bottom'
	                },
	                toolbox: {
	                    show: true,
	                    feature: {
	                        mark: {
	                            show: false
	                        },
	                        dataView: {
	                            show: false,
	                            readOnly: false
	                        },
	                        magicType: {
	                            show: false,
	                            type: ['line', 'bar']
	                        },
	                        restore: {
	                            show: true
	                        },
	                        saveAsImage: {
	                            show: true
	                        }
	                    }
	                },
	                calculable: true,
	                xAxis: [{
	                    type: 'category',
	                    data: [""]
	                }],
	                yAxis: [{
	                    type: 'value',
	                    splitArea: {
	                        show: true
	                    }
	                }],
	                series: series
	            });
	            if(options.main){
					var label_principal = [] ,categories = [], series_principal = [] , lote_principal = "" , count_p = [];
		            var promedio = [] , count = 0 , tpromedio = 0;
        			series_principal.push({
        				name : "lotes", 
        				type : "line",
        				data : []
        			});
        			series_principal.push({
        				name : options.lote_name, 
        				type : "line",
        				data : []
        			});
		            for (var key in options.main) {
		        		categories.push(key);
	            		if(options.main.hasOwnProperty(key)) {
	            			if(options.main[key].hasOwnProperty("lote")){
		        				label_principal = ["lotes" , options.main[key].lote.name];
		        				promedio[count] = [];
		        				count_p = options.main[key].lotes.map(function(num) { promedio[count].push(num.sum/num.avg); return num.sum/num.avg; });
	        					tpromedio = promedio[count].reduce(function(a, b) { return parseFloat(a) + parseFloat(b); });
	        					
		        				series_principal[0].data.push(
		        					Math.round(tpromedio / promedio[count].length)
		        				);

		        				series_principal[1].data.push(
		        					Math.round(options.main[key].lote.sum / options.main[key].lote.avg)
		        				);
	            			}else{
	            				series_principal[0].data.push(0);
	            				series_principal[1].data.push(0);
	            			}
        				}
	        		}
					myChartPrincial = ec.init(document.getElementById('echarts_bar_pricipal') , 'infographic');
		            myChartPrincial.setOption({
		                tooltip: {
		                    trigger: 'axis'
		                },
		                legend: {
		                    data: label_principal
		                },
		                toolbox: {
		                    show: true,
		                    feature: {
		                        mark: {
		                            show: false
		                        },
		                        dataView: {
		                            show: false,
		                            readOnly: true
		                        },
		                        magicType: {
		                            show: true,
		                            type: ['line', 'bar']
		                        },
		                        restore: {
		                            show: true
		                        },
		                        saveAsImage: {
		                            show: true
		                        }
		                    }
		                },
		                calculable: true,
		                xAxis: [{
		                    type: 'category',
		                    data: categories
		                }],
		                yAxis: [{
		                    type: 'value',
		                    splitArea: {
		                        show: true
		                    }
		                }],
		                series: series_principal
		            });
		            myChartPrincial.setTheme('infographic');
	            }

				// // myChartPrincial.setTheme('infographic');
				myChartLaborLote.setTheme('infographic');
				// myChartOperador.setTheme('infographic');
	            window.onresize = function(){
	            	myChartPrincial.resize();
	            	myChartLaborLote.resize();
	            	// myChartOperador.resize();
	            }
        	}else if(step == 2){
        		var data = [] , label = [];
        		for (var i = 0; i < options.length; i++) {
        			label.push(options[i].causa);
        			data.push({value : Math.round(options[i].porcentaje) , name : options[i].causa});
        		}
	            var myChartLoteLabor = ec.init(document.getElementById('echarts_bar_lotes_labor') , 'infographic');
	            myChartLoteLabor.setOption({

				    tooltip : {
				        trigger: 'item',
				        formatter: "{a} <br/>{b} : {c} ({d}%)"
				    },
				    legend: {
				        orient : 'vertical',
				        x : 'left',
				        data:label
				    },
				    calculable : true,
				    toolbox: {
				        show : true,
				        feature : {
				            mark : {show: true},
				            dataView : {show: true, readOnly: false},
				            magicType : {
				                show: true, 
				                type: ['pie', 'funnel'],
				                option: {
				                    funnel: {
				                        x: '25%',
				                        width: '50%',
				                        funnelAlign: 'left',
				                        max: 1548
				                    }
				                }
				            },
				            restore : {show: true},
				            saveAsImage : {show: true}
				        }
				    },
				    calculable : true,
				    series : [
				        {
				            name:'Causas',
				            type:'pie',
				            radius : '55%',
				            center: ['50%', '60%'],
				            data:data
				        }
				    ]
				});

	            if(options.main){
					var label_principal = [] ,categories = [], series_principal = [] , lote_principal = "" , count_p = [];
		            var promedio = [] , count = 0 , tpromedio = 0;
        			series_principal.push({
        				name : "lotes", 
        				type : "line",
        				data : []
        			});
        			series_principal.push({
        				name : options.lote_name, 
        				type : "line",
        				data : []
        			});
		            for (var key in options.main) {
		        		categories.push(key);
	            		if(options.main.hasOwnProperty(key)) {
	            			if(options.main[key].hasOwnProperty("lote")){
		        				label_principal = ["lotes" , options.main[key].lote.name];
		        				promedio[count] = [];
		        				count_p = options.main[key].lotes.map(function(num) { promedio[count].push(num.sum/num.avg); return num.sum/num.avg; });
	        					tpromedio = promedio[count].reduce(function(a, b) { return parseFloat(a) + parseFloat(b); });
	        					
		        				series_principal[0].data.push(
		        					Math.round(tpromedio / promedio[count].length)
		        				);

		        				series_principal[1].data.push(
		        					Math.round(options.main[key].lote.sum / options.main[key].lote.avg)
		        				);
	            			}else{
	            				series_principal[0].data.push(0);
	            				series_principal[1].data.push(0);
	            			}
        				}
	        		}
					myChartPrincial = ec.init(document.getElementById('echarts_bar_causa_principal') , 'infographic');
		            myChartPrincial.setOption({
		                tooltip: {
		                    trigger: 'axis'
		                },
		                legend: {
		                    data: label_principal
		                },
		                toolbox: {
		                    show: true,
		                    feature: {
		                        mark: {
		                            show: false
		                        },
		                        dataView: {
		                            show: false,
		                            readOnly: true
		                        },
		                        magicType: {
		                            show: true,
		                            type: ['line', 'bar']
		                        },
		                        restore: {
		                            show: true
		                        },
		                        saveAsImage: {
		                            show: true
		                        }
		                    }
		                },
		                calculable: true,
		                xAxis: [{
		                    type: 'category',
		                    data: categories
		                }],
		                yAxis: [{
		                    type: 'value',
		                    splitArea: {
		                        show: true
		                    }
		                }],
		                series: series_principal
		            });
		            myChartPrincial.setTheme('infographic');
	            }

				myChartLoteLabor.setTheme('infographic');
				 window.onresize = function(){
	            	myChartPrincial.resize();
	            	myChartLoteLabor.resize();
	            }
        	}
        }
    );
}

app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
	  //alert(field);
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
		//alert(a[field]);
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.filter('orderObjectBy2', function() {
  return function(items, field, reverse) {
	  //alert(field);
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
	if(!reverse){
		if(a[field]==null){a[field]='900.00000'}
		if(b[field]==null){b[field]='900.00000'}
		if(a[field]=='0.00000'){a[field]='900.00000'}
		if(b[field]=='0.00000'){b[field]='900.00000'}
	}
	else{
		if(a[field]==null){a[field]='0.00000'}
		if(b[field]==null){b[field]='0.00000'}
		if(a[field]=='900.00000'){a[field]='0.00000'}
		if(b[field]=='900.00000'){b[field]='0.00000'}
	}
		//alert(b[field]);
      return (a[field] - b[field]);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});


app.controller('reportes', ['$scope','$http','$interval','client','$controller', function($scope,$http,$interval,client, $controller){
	$scope.wizardStep = {
		params : {
			idFinca : 1,
			idLote : 0,
			idLabor : 0,
			fecha_inicial : '2016-06-01',
			fecha_final : '2016-06-30',
		},
		step : 0,
		path : ['phrapi/Reportes/lotes', 'phrapi/Reportes/lotes' , 'phrapi/Reportes/labores' , 'phrapi/Reportes/causas'],
		templatePath : ['/views/step4.html', '/views/step3.html' , '/views/step2.html' , '/views/step1.html']
	}

	$scope.tags = {
		lote : {
			valor : 0,
			label : ""
		},
		labor : {
			valor : 0,
			label : ""
		},
		auditoria : {
			valor : 0
		}
	}

	$scope.interval = 140000;

	$scope.wizardData = {
		data : [],
		data_labores : [],
		data_labores_lotes : [],
		labores : [],
		main : [],
		lote_name : [],
		labor_name : [],
	}

	$scope.tittles = {
		lote : "Todos",
		labor : "Todo"
	}

	$scope.init = function(){
		$interval($scope.loadExternal, $scope.interval);
	}

	$scope.changeStep = function(step ,idLote , idLabor , lote , labor){
		console.log(step)
		$scope.wizardStep.step = step;
		$scope.wizardStep.params.idLote = idLote || 0;
		$scope.wizardStep.params.idLabor = idLabor || 0;
		$scope.tittles.lote = lote || 'Todos';
		$scope.tittles.labor = labor || 'Todos';
		$scope.wizardStep.templatePath[step] += "?"+Math.random();
	}
	$scope.loadExternal = function(){
		// console.log($scope.wizardStep.path[$scope.wizardStep.step]);
		if($scope.wizardStep.path[$scope.wizardStep.step] != ""){
			var data = $scope.wizardStep.params;
			client.post($scope.wizardStep.path[$scope.wizardStep.step] , $scope.getDetails , data);
		}
	}

	$scope.getDetails = function(r , b){
		b();
		if(r){
			var options = r.causas || {};
			var OptionsPrincipal = r.principal || {};
			$scope.wizardData.data = r.causas || [];
			$scope.wizardData.main = r.main || [];
			$scope.wizardData.lote_name = r.lote_name || [];
			$scope.wizardData.data_labores = r.data_labores || [];
			$scope.wizardData.data_labores_lotes = r.data_labores_lotes || [];
			if(options){
				options.data_labores = []
				options.data_labores = $scope.wizardData.data_labores;
				options.main = $scope.wizardData.main;
				options.lote_name = $scope.wizardData.lote_name;
			}
			if(r.labores){
				var labores = r.labores.filter(function(item, pos) {
				    return  r.labores.indexOf(item) == pos;
				});

				$scope.wizardData.labores = labores;
			}

			if($scope.wizardStep.step == 0 && r.data_labores){
				/*----------  LABOR  ----------*/
				var labor = [] , label = [] , value = [];
				labor = r.data_labores.map(function(item){
					label.push(item.labor);
					value.push((item.promedio / item.avg));
					return (item.promedio / item.avg);
				});
				$scope.tags.labor.valor = value.min().value;
				$scope.tags.labor.label = label[value.min().label];
				/*----------  LABOR  ----------*/

				/*----------  LOTE  ----------*/
				var lote = [] , lote_label = [] , lote_value = [];
				lote = r.causas.lote.map(function(item){
					lote_value.push($scope.getPromedio(item));
					return Object.keys(item);
				});
				lote_label = Object.keys(r.causas.lote);
				$scope.tags.lote.valor = lote_value.min().value;
				$scope.tags.lote.label = lote_label[lote_value.min().label];
				/*----------  LOTE  ----------*/
				/*----------  AUDITORIA  ----------*/
				$scope.tags.auditoria.valor = lote_value.avg();

				setTimeout(function(){
					$(".counter_tags").counterUp({
						delay: 10,
						time: 1000
					});	
				} , 1000);
			}

			loadScript($scope.wizardStep.step , options , true, OptionsPrincipal);
		}
	}

	$scope.getTotals = function(type){
		type = type || 1;
		var data = $scope.wizardData.data;
		var total_1 = 0;
		var total_2 = 0;
		for (var i = 0; i < data.length; i++) {
			total_1 += parseFloat(data[i].porcentaje);	
			if(!isNaN(parseFloat(data[i].porcentaje2)))
				total_2 += parseFloat(data[i].porcentaje2);
		}
		return (type == 1) ? total_1 : total_2;
	}
	
	$scope.checks = function(porcentaje){
		//alert(porcentaje);
		if(porcentaje >= '90.00')
			return 'fa fa-check font-green-jungle';
		else if (porcentaje >= '88.00' &&porcentaje <='89.99')
			return 'fa fa-exclamation font-yellow-lemon';
		else 
			return 'fa fa-close font-red-thunderbird';	
	}
	
	$scope.arrayToString = function(string){
    return string;
	};

	$scope.getPromedio = function(data){
		var count = 0;
		var promedio = 0;
		var total = 0;
		for (var key in data.labores) {
			count++;
			promedio += parseFloat(data.labores[key].promedio);
		}
		total = (promedio / count);
		return total;
	}
}]);








