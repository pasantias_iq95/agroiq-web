<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.page-bar .page-toolbar{
		display: inline-flex !important;
	}
	.subContent {
		font-style: italic !important;
	}
	.dt-buttons{
		display : none;
	}
</style>
<div>
	<h3 class="page-title"> 
          Revisión Racimos
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Revisión Racimos</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">   
        </div>
    </div>
	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Datos</span>
		    <div class="tools">
				 <div class="btn-group">
                    <button id="editData" class="btn blue"> Editar </button>
                </div>
	        </div>
		</div>
		<div class="portlet-body">
			<div class="table-container">
                <div class="table-actions-wrapper">
                </div>
				<table class="table table-striped table-bordered table-hover table-header-fixed dataTable no-footer" id="datatable_ajax">
                    <thead>
                        <tr >
                            <th class="center-th" width="1%"></th>
                            <th class="center-th" width="10%"> ID</th>
                            <th class="center-th" width="5%"> Bloque </th>
                            <th class="center-th" width="10%"> Finca </th>
                            <th class="center-th" width="20%"> Palanca </th>
                            <th class="center-th" width="10%"> Racimo </th>
                            <th class="center-th" width="10%"> Tallo </th>
                            <th class="center-th" width="10%"> Fecha </th>
                            <th class="center-th" width="10%"> Acciones </th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td><input type="text" class="form-control form-filter input-sm" name="id" id="id"> </td>
                            <td><input type="text" class="form-control form-filter input-sm" name="bloque" id="bloque"> </td>
                            <td>
                            	<input type="text" class="form-control form-filter input-sm" name="finca" id="finca">
                            	<input type="hidden" id="EnableEdit" name="EnableEdit" class="form-filter" value="0">
                            </td>
                            <td><input type="text" class="form-control form-filter input-sm" name="palanca" id="palanca"></td>
                            <td><input type="text" class="form-control form-filter input-sm" name="racimo" id="racimo"> </td>
                            <td><input type="text" class="form-control form-filter input-sm" name="tallo" id="tallo"> </td>
                            <td><input type="text" class="form-control form-filter input-sm" name="fecha" id="fecha"> </td>
                            <td>
                            	<div class="margin-bottom-5">
                                    <button class="btn btn-sm green btn-outline filter-submit margin-bottom">
                                        <i class="fa fa-search"></i> 
                                    </button>
                                </div>
                                <button class="btn btn-sm red btn-outline filter-cancel">
                                    <i class="fa fa-times"></i> 
                                </button>
                            </td>
                        </tr>
                    </thead>
                    <tbody> </tbody>
                </table>
			</div>
		</div>
	</div>
</div>