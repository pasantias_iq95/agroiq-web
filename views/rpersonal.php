<style>
	.accordion .panel .panel-title .accordion-toggle {
		display: block !important;
    	padding: 0px 14px !important;
	}
	.margin-5 {
		margin-bottom: 5px !important;
	}
	table > th,td{
		text-align: center !important;
	}
	.center-th {
		text-align: center !important;
	}
	.left-td {
		text-align: left !important;
		padding-left: 10px !important;
	}
	table {
		cursor: pointer !important;
	}
	.alginCenter {
		text-align: center !important;
		padding-top: 20px;
	}
	tfoot > tr > td {
		font-size: 13px !important;
		padding: 0px !important;
	}
</style>
<div ng-controller="rpersonal" ng-cloak>
	<h3 class="page-title"> 
          Producción
     </h3>
     <div class="page-bar" ng-init="rpersonal.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Producción</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
         <div class="page-toolbar">
            <!-- <ng-calendarapp  search="search"></ng-calendarapp> -->
        </div>
    </div>
    <div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Filtros</span>
		    <div class="tools">
	            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
	        </div>
		</div>
		<div class="portlet-body" style="display: none;">
			<form action="#" class="horizontal-form">
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Fincas : </label>
                                    <select class="form-control" id="filter" name="filter" ng-model="rpersonal.params.idFinca" onchange="changeFilter()">
                                        <option ng-repeat="(key, value) in filters" value="{{key}}">{{value}}</option>
                                    </select>
                                </div>
                            </div>
                             <!-- <div class="col-md-4">
                                <div class="form-group">
                                    <label>Personal : </label>
                                    <input type="text" ng-model="search" ng-blur="viewModel()" id="personal" name="personal" class="form-control">
                                </div>
                            </div> -->
                        </div>
                    </div>
				</div>
			</form>
		</div>
	</div>
	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Loteros Aéreos</span>
		    <div class="tools">
	            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
	        </div>
		</div>
		<div class="portlet-body" style="display: none;">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
			                <tr role="row" class="heading">
			                    <th width="{{withTable}}" class="center-th"> COD  </th>
			                    <th width="{{withTable}}" class="center-th"> LOTERO  </th>
			                    <th width="{{withTable}}" class="center-th"> LOTE  </th>
			                    <th width="{{withTable}}" class="center-th"> LABOR  </th>
			                    <th width="{{withTable}}" class="center-th" ng-repeat="(key, value) in dataFilters">{{value}}</th>
			                </tr>
					<tbody>
						<tr ng-repeat-start="lotero in loteros_aereo | filter : search" ng-click="openDetalle(lotero)">
							<td width="{{withTable}}" class="center-th">{{$index + 1}}</td>
							<td width="{{withTable}}">{{lotero.lotero}}</td>
							<td width="{{withTable}}">{{lotero.lote}}</td>
							<td width="{{withTable}}"></td>
							<td width="{{withTable}}" ng-repeat="(key, value) in dataFilters">
								<i class="{{lotero.umbral[value]}}"></i>	
								{{lotero.campo[value] | number : 2}}
							</td>
						</tr>	
						<tr ng-show="lotero.expanded" ng-repeat-end="">
							<td colspan="{{colspan}}">
								<table class="table table-striped table-bordered table-hover">
									<tr ng-repeat="labor in lotero.labores.aereo" ng-click="openDetalle(labor)">
										<td width="{{withTable}}">-</td>
										<td width="{{withTable}}">-</td>
										<td width="{{withTable}}">-</td>
										<td width="{{withTable}}" class="left-td">{{labor.labor}}</td>
										<td width="{{withTable}}" ng-repeat="(key, value) in dataFilters">
											<i class="{{labor.umbral[value]}}"></i>
											{{labor.campo[value] | number : 2}}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr ng-repeat="(llave , valor) in totales">
							<td>Total : </td>
							<td></td>
							<td></td>
							<td> </td>
							<td width="{{withTable}}" ng-repeat="(key, value) in dataFilters">
								<i class="{{umbral[value]}}"></i> {{valor[value]}}
							</td>
						</tr>
				</table>
			</div>
		</div>
	</div>

	<div class="portlet box green">
		<div class="portlet-title">
		    <span class="caption">Loteros Terrestres</span>
		    <div class="tools">
	            <a href="javascript:;" class="expand" data-original-title="Expandir/Contraer" title=""> </a>
	        </div>
		</div>
		<div class="portlet-body" style="display: none;">
			<div class="table-responsive">
				<table class="table table-striped table-bordered table-hover">
					<thead>
			                <tr role="row" class="heading">
			                    <th width="{{withTable}}" class="center-th"> COD  </th>
			                    <th width="{{withTable}}" class="center-th"> LOTERO  </th>
			                    <th width="{{withTable}}" class="center-th"> LOTE  </th>
			                    <th width="{{withTable}}" class="center-th"> LABOR  </th>
			                    <th width="{{withTable}}" class="center-th" ng-repeat="(key, value) in dataFilters">{{value}}</th>
			                </tr>
					<tbody>
						<tr ng-repeat-start="lotero in loteros_terrestre" ng-click="openDetalle(lotero)">
							<td width="{{withTable}}" class="center-th">{{$index + 1}}</td>
							<td width="{{withTable}}">{{lotero.lotero}}</td>
							<td width="{{withTable}}">{{lotero.lote}}</td>
							<td width="{{withTable}}"></td>
							<td width="{{withTable}}" ng-repeat="(key, value) in dataFilters">
								<i class="{{lotero.umbral[value]}}"></i>	
								{{lotero.campo[value] | number : 2}}
							</td>
						</tr>	
						<tr ng-show="lotero.expanded" ng-repeat-end="">
							<td colspan="{{colspan}}">
								<table class="table table-striped table-bordered table-hover">
									<tr ng-repeat="labor in lotero.labores.terrestre" ng-click="openDetalle(labor)">
										<td width="{{withTable}}"></td>
										<td width="{{withTable}}"></td>
										<td width="{{withTable}}"></td>
										<td width="{{withTable}}" class="left-td">{{labor.labor}}</td>
										<td width="{{withTable}}" ng-repeat="(key, value) in dataFilters">
											<i class="{{labor.umbral[value]}}"></i>
											{{labor.campo[value] | number : 2}}
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr ng-repeat="(llave , valor) in totales">
							<td>Total : </td>
							<td></td>
							<td></td>
							<td> </td>
							<td width="{{withTable}}" ng-repeat="(key, value) in dataFilters">
								<i class="{{umbral[value]}}"></i> {{valor[value]}}
							</td>
						</tr>
				</table>
			</div>
		</div>
	</div>
</div>