<style>
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
    
    .calendar-table td.available {
		background-color: green !important;
    	color: white !important;
	}
	.icalendar-table td.available:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>
      <!-- BEGIN CONTENT BODY -->
<div ng-controller="informe_calidad" ng-cloak>
     <h3 class="page-title"> 
          Merma
     </h3>
     <div class="page-bar" ng-init="calidad.nocache()">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Merma</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
            <ng-calendarapp  search="search"></ng-calendarapp>
        </div>
     </div>

     <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li ng-repeat="(key, value) in fincas" class="{{ calidad.params.idFinca == key ? 'active' : '' }}">
                <a ng-click="calidad.params.idFinca = key; cambiosFincas()">{{value}}</a>
            </li>
        </ul>
    </div>

     <?php include("./views/tags_merma_".$this->session->agent_user.".php");?>           
     <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]" onload="load()">

     </div>  
</div>

<script src="//unpkg.com/react@15/dist/react.js"></script>
<script src="//unpkg.com/react-dom@15/dist/react-dom.js"></script>
<script src="componentes/FilterableSortableTable.js"></script>