<div ng-controller="productos">
    <h3 class="page-title"> 
        Módulo de Productos
        <small>Registro de Producto</small>
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Inicio</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <span><a>Registro de Producto</a></span>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
    </div> 
    <div class="row" ng-controller="productos">
        <div class="col-md-12" ng-init="index()">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-purple-soft"></i>
                        <span class="caption-subject font-purple-soft bold uppercase">PRODUCTOS</span>
                    </div>
                    <div class="actions">
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-cloud-upload"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-wrench"></i>
                        </a>
                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                            <i class="icon-trash"></i>
                        </a>
                    </div>
                </div>
                <div class="portlet-body">
                    <ul class="nav nav-tabs tabs-reversed">
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> Configuración
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li class="">
                                    <a href="#configuracion" ng-click="setTipo('Formuladora')" tabindex="-1" data-toggle="modal" aria-expanded="false"> Formuladora </a>
                                </li>
                                <li class="">
                                    <a href="#configuracion" ng-click="setTipo('Proveedor')" tabindex="-1" data-toggle="modal" aria-expanded="false"> Proveedor </a>
                                </li>
                                <li class="">
                                    <a href="#configuracion" ng-click="setTipo('Tipo de Producto')" tabindex="-1" data-toggle="modal" aria-expanded="false"> Tipo de Producto </a>
                                </li>
                                <!--<li class="">
                                    <a href="#precio" tabindex="-1" data-toggle="tab" aria-expanded="false"> Precio </a>
                                </li>-->
                            </ul>
                        </li>
                        <li class="">
                            <a href="#createproductos" ng-click="getProduct()" data-toggle="tab" aria-expanded="true"> Crear Producto </a>
                        </li>
                        <li class="active" ng-click="cancel()">
                            <a href="#listado" data-toggle="tab" aria-expanded="false"> Listado </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade" id="createproductos" ng-include="'./views/sigat/<?=$this->session->agent_user?>/productos/producto.html?i=<?=rand(10,100)?>'"></div>
                        <div class="tab-pane fade active in" id="listado" ng-include="'./views/sigat/<?=$this->session->agent_user?>/productos/listado.html?i=<?=rand(10,100)?>'"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- MODAL -->
        <div class="modal fade" id="configuracion" role="basic" aria-hidden="true">
            <div class="modal-dialog" style="background: white;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Registrar</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>{{configuracion.nombre}}</h4>
                            <p>
                                <input type="text" class="col-md-6 form-control" ng-model="configuracion.campo"> </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn blue" ng-click="configuracion.save()">Guardar</button>
                </div>
            </div>
        </div>
    </div>
</div>