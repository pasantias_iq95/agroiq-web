<style>
    .label_chart {
        background-color: #fff;
        padding: 2px;
        margin-bottom: 8px;
        border-radius: 3px 3px 3px 3px;
        border: 1px solid #E6E6E6;
        display: inline-block;
        margin: 0 auto;
    }
    .legendLabel{
        padding: 3px !important;
    }
    .portlet.box.green>.portlet-title, .portlet.green, .portlet>.portlet-body.green {
        background-color: #009739 !important;
    }
    .form-control, .input-sm, select, .btn, .portlet {
        border-radius : 3px !important;
    }
    .highlight {
		background-color: green;
    	color: white !important;
	}
	.highlight:hover {
		background-color: #285e8e !important;
    	color: white !important;
	}
</style>
      
<div ng-controller="informe_merma_dia"  ng-cloak>
     <h3 class="page-title" ng-init="calidad.nocache()">
          Merma Día
     </h3>
     <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a>Merma</a>
                 <i class="fa fa-angle-right"></i>
             </li>
         </ul>
        <div class="page-toolbar">
            <label ng-if="id_company != 4" for="">
                Categoría : 
            </label>
            <select onchange="cambiosCategoria()" name="categoria" id="categoria" ng-model="calidad.params.categoria" style="margin: 2px;height: 36px;">
                <option value="COSECHA">COSECHA</option>
            </select>
            <!--<ng-calendarapp  search="search"></ng-calendarapp>-->
            <input id="datepicker" class="input-sm" sytle="margin: 2px;height: 36px;" data-provide="datepicker" data-date-format="yyyy-mm-dd" readonly>
        </div>
     </div>

    <div class="tabbable tabbable-tabdrop">
        <ul class="nav nav-tabs">
            <li ng-repeat="(key, value) in fincas" class="{{ calidad.params.idFinca == key ? 'active' : '' }}">
                <a ng-click="calidad.params.idFinca = key; cambiosFincas()">{{value}}</a>
            </li>
        </ul>
    </div>

    <div id="reportes_all" ng-include="calidad.templatePath[calidad.step]" onload="last()">
        
    </div>  
</div>