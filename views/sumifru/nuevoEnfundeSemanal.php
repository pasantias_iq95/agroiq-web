<style>
    .form-group {
        margin-bottom : 5px;
    }
</style>

<div ng-controller="produccion">
    <h3 class="page-title"> 
          Nuevo Enfunde
    </h3>
    <div class="page-bar">
         <ul class="page-breadcrumb">
             <li>
                 <i class="icon-home"></i>
                 <a href="produccionenfunde">Enfunde</a>
                 <i class="fa fa-angle-right"></i>
             </li>
             <li>
                 <a>Nuevo</a>
             </li>
         </ul>
        <div class="page-toolbar" style="display: flex;">
            <label for="" style="margin: auto">Sem</label>
            <div class="btn-group pull-right">
                <button type="button" class="btn btn-fit-height dropdown-toggle {{color}}" data-toggle="dropdown" data-hover="dropdown" data-delay="2000" data-close-others="true" aria-expanded="false"> 
                    {{ filters.anio }} - {{ filters.semana }}
                    <i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu" style="max-height: 300px; overflow-y: scroll;">
                    <li ng-repeat="sem in semanas" class="{{sem.class}}" ng-click="filters.semana = sem.semana; filters.anio = sem.anio; init()">
                        <a href="javascript:;" class="{{sem.class}}">
                            {{ sem.anio }} -  {{ sem.semana }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="portlet box green">
        <div class="portlet-title">
            <div class="caption">
                DATOS DEL ENFUNDE
            </div>
            <div class="tools">
                
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12">
                    <label for="" class="label font-red-thunderbird">Presionar <b>Enter</b> para guardar</label><br>
                    <label for="" class="label font-red-thunderbird">Las casillas con fondo gris no estan habilitadas para guardar</label><br>
                    <div class="table-container">
                        <div id="table-react"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="componentes/FilterableSortableTable.js"></script>