<style>
    .charts {
        height : 400px;
    }
</style>

<div ng-controller="control">
	<h3 class="page-title"> 
        Producción. Comparación
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Producción</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
            Año: 
            <select class="input-sm" style="color: black" ng-model="filters.anio" ng-change="init(); changeGrafica();">
                <option value="">Seleccione</option>
                <option value="{{y}}" ng-repeat="y in anios">{{y}}</option>
            </select>

            Sem: 
            <select class="input-sm" style="color: black" ng-model="filters.semana" ng-change="init()">
                <option value="">Seleccione</option>
                <option value="{{s}}" ng-selected="s == filters.semana" ng-repeat="s in semanas">{{s}}</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                </div>
                <div class="portlet-body">
                    <div id="react-table"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                </div>
                <div class="portlet-body">
                    <div id="react-table-2"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet box green">
                <div class="portlet-title">
                </div>
                <div class="portlet-body">
                    <div class="pull-right">
                        <select class="form-control" ng-model="filters.variable" ng-change="changeGrafica()">
                            <option value="{{variable}}" ng-repeat="variable in variables" ng-selected="variable == filters.variable">{{ variable }}</option>
                        </select>
                    </div>
                    <div id="grafica" class="charts" style="margin-top: 40px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>