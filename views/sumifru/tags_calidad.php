<div class="row">
    <div class="tag col-lg-{{tag_md}} col-md-{{tag_md}} col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.calidad_cluster}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>% Calidad Cluster</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: {{tags.calidad_cluster}}%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-{{tag_md}} col-md-{{tag_md}} col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.calidad_dedos}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>% Calidad Dedos</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: {{tags.calidad_dedos}}%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-{{tag_md}} col-md-{{tag_md}} col-sm-6 col-xs-12" ng-if="config.calidad_empaque">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.calidad_empaque | number : 2}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>% Calidad Empaque</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: {{tags.calidad_empaque}}%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-{{tag_md}} col-md-{{tag_md}} col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.cluster_promedio}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small># Cluster Prom Por Caja</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-{{tag_md}} col-md-{{tag_md}} col-sm-6 col-xs-12" ng-if="config.peso_prom_cluster">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.peso_prom_cluster}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>Peso Prom Cluster (g)</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-{{tag_md}} col-md-{{tag_md}} col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.dedos_promedio}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small># Dedos Prom Por Caja</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>