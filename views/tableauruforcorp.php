<?php
    /*=============================================================
    =            Insertar aqui validacion  de usuarios            =
    =============================================================*/

    $url = 'http://tableau.procesos-iq.com/trusted';
    $username = 'adminiq';
    $site = 'ruforcorp';

    $data = array('username' => $username, 'target_site' => $site);
    $options = array(
        'http' => array(
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $tiket = file_get_contents($url, false, $context);
    //var_dump($tiket);
    //if ($tiket === "-1") { echo "error\n"; }
    
    
    /*=====  End of Insertar aqui validacion  de usuarios  ======*/
?>
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="assets/pages/css/error.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <div class="page-head">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Error 404
                    <small>La pagina no existe</small>
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE BREADCRUMB -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/">Inicio</a>
            </li>
            <li>
                <span class="active">Error 404</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMB -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class='tableauPlaceholder' style='width: 1000px; height: 827px;'>
                <object class='tableauViz' width='1000' height='827' style='display:none;'>
                    <param name='host_url' value='http%3A%2F%2Ftableau.procesos-iq.com%2F' /> 
                    <param name='embed_code_version' value='3' /> 
                    <param name='site_root' value='&#47;t&#47;ruforcorp' />
                    <param name='name' value='Fase1&#47;Tabla1' />
                    <param name='tabs' value='no' />
                    <param name='toolbar' value='yes' />
                    <param name='showAppBanner' value='false' />
                    <?php
                    if ($tiket !== "-1") {
                        echo "<param name=\"ticket\" value=\"$tiket\" />";
                    }
                    ?>
                </object>
            </div>
        </div>

<script type='text/javascript' src='http://tableau.procesos-iq.com/javascripts/api/viz_v1.js'></script>