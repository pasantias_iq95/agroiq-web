
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.merma.value) }}">
                                            <span class="counter_tags" data-value="{{tags.merma.value}}">0</span>
                                            <small class="font-{{ revision(tags.merma.value) }}">%</small>
                                        </h3>
                                        <small>% MERMA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.merma.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.merma.value) }}">
                                            <span class="sr-only">{{tags.merma.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.merma.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.enfunde.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.enfunde.value}}">0</span>
                                            <small class="font-{{ revision(tags.enfunde.value) }}">%</small>
                                        </h3>
                                        <small>ENFUNDE Y PROTECCIÓN</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.enfunde.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.enfYPro.value) }}">
                                            <span class="sr-only"> {{tags.enfunde.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.enfunde.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.campo.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.campo.value}}">0</span>
                                            <small class="font-{{ revision(tags.campo.value) }}">%</small>
                                        </h3>
                                        <small>CAMPO</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.campo.value}}%;" class="progress-bar progress-bar-success  {{ revision(tags.campo.value) }}">
                                            <span class="sr-only">0% grow</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.campo.value}}% </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cosecha.value) }}">
                                            <span class="counter_tags" data-value="{{tags.cosecha.value}}">0</span>
                                            <small class="font-{{ revision(tags.cosecha.value) }}">%</small>
                                        </h3>
                                        <small>COSECHA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:{{tags.cosecha.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cosecha.value) }}">
                                            <span class="sr-only">{{tags.cosecha.value}}%</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.animales.value) }}">
                                            <span class="counter_tags" data-value="{{tags.animales.value}}">0</span>
                                            <small class="font-{{ revision(tags.animales.value) }}">%</small>
                                        </h3>
                                        <small>ANIMALES</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.animales.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.animales.value) }}">
                                            <span class="sr-only">{{tags.animales.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.hongos.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.hongos.value}}">0</span>
                                            <small class="font-{{ revision(tags.hongos.value) }}">%</small>
                                        </h3>
                                        <small>HONGOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.hongos.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.hongos.value) }}">
                                            <span class="sr-only"> {{tags.hongos.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.empacadora.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.empacadora.value}}">0</span>
                                            <small class="font-{{ revision(tags.empacadora.value) }}">%</small>
                                        </h3>
                                        <small>EMPACADORA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.empacadora.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.empacadora.value) }}">
                                            <span class="sr-only"> {{tags.empacadora.value}}% progress</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.fisiologicos.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.fisiologicos.value}}">0</span>
                                            <small class="font-{{ revision(tags.fisiologicos.value) }}">%</small>
                                        </h3>
                                        <small>FISIOLÓGICOS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.fisiologicos.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.fisiologicos.value) }}">
                                            <span class="sr-only"> {{tags.fisiologicos.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>