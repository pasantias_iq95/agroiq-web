<div class="row">
    <div class="tag col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.gavetas_recibidas}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small># Gavetas recibidas en el local</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: {{tags.gavetas_recibidas}}%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.kilos_totales}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>Kilos totales</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: {{tags.kilos_totales}}%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.kilos_destruidos | number : 2}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>Kilos destruidos</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: {{tags.kilos_destruidos}}%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.temperatura_bodega}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>TEMPERATURA PROM BODEGA</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.temperatura_percha_1}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>TEMPERATURA PROM PERCHA 1</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="tag col-lg-4 col-md-4 col-sm-6 col-xs-12" ng-show="tags.temperatura_percha_2 > 0">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-jungle">
                        <span class="counter_tags" data-value="">{{tags.temperatura_percha_2}}</span>
                        <small class="font-green-jungle"></small>
                    </h3>
                    <small>TEMPERATURA PROM PERCHA 2</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>
            <div class="progress-info">
                <div class="progress">
                    <span style="width: 0%;" class="progress-bar progress-bar-success">
                        <span class="sr-only"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>