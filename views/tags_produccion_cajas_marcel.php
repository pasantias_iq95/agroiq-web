
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.cajas.value) }}">
                                            <span class="counter_tags" data-value="{{tags.cajas.value}}">0</span>
                                            <small class="font-{{ revision(tags.cajas.value) }}"></small>
                                        </h3>
                                        <small>CAJAS DE PRIMERA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.cajas.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.cajas.value) }}">
                                            <span class="sr-only">{{tags.cajas.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.cajas.value}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.otras_cajas.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.otras_cajas.value}}">0</span>
                                            <small class="font-{{ revision(tags.otras_cajas.value) }}"></small>
                                        </h3>
                                        <small>CAJAS DE SEGUNDA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.otras_cajas.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.otras_cajas.value) }}">
                                            <span class="sr-only"> {{tags.otras_cajas.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.calibracion.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.calibracion.value}}">0</span>
                                            <small class="font-{{ revision(tags.calibracion.value) }}"></small>
                                        </h3>
                                        <small>TOTAL CAJAS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.calibracion.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.calibracion.value) }}">
                                            <span class="sr-only"> {{tags.calibracion.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number">  {{tags.calibracion.value}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.racimo.value) }}">
                                            <span class="counter_tags" data-value="{{tags.racimo.value}}">0</span>
                                            <small class="font-{{ revision(tags.racimo.value) }}"></small>
                                        </h3>
                                        <small>CONVERSIÓN CAJAS</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width: {{tags.racimo.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.racimo.value) }}">
                                            <span class="sr-only">{{tags.racimo.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"> {{tags.racimo.value}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.edad.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.edad.value}}">0</span>
                                            <small class="font-{{ revision(tags.edad.value) }}"></small>
                                        </h3>
                                        <small>CAJAS /HA</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.edad.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.edad.value) }}">
                                            <span class="sr-only"> {{tags.edad.value}}</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title"></div>
                                        <div class="status-number"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 ">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-{{ revision(tags.ratio_cortado.value) }}">
                                            <span class="counter_tags" data-value=" {{tags.ratio_cortado.value}}">0</span>
                                            <small class="font-{{ revision(tags.ratio_cortado.value) }}"></small>
                                        </h3>
                                        <small>DÓLARES TOTAL</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-bar-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                    <div class="progress">
                                        <span style="width:  {{tags.ratio_cortado.value}}%;" class="progress-bar progress-bar-success {{ revision(tags.ratio_cortado.value) }}">
                                            <span class="sr-only"> {{tags.ratio_cortado.value}}% change</span>
                                        </span>
                                    </div>
                                    <div class="status">
                                        <div class="status-title">RATIO CORTADO 2da</div>
                                        <div class="status-number"> {{tags.ratio_cortado_dos}} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>