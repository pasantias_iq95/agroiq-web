<style>
	.card {
		/* Add shadows to create the "card" effect */
		box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
		transition: 0.3s;
	}

	/* On mouse-over, add a deeper shadow */
	.card:hover {
		box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
	}

	/* Add some padding inside the card container */
	.container {
		padding: 2px 16px;
	}
</style>
<div ng-controller="controller">
	<h3 class="page-title"> 
		Tutoriales
    </h3>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a>Tutoriales</a>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
        <div class="page-toolbar">
		 	
        </div>
	</div>

	<div class="portlet box green-haze">
		<div class="portlet-title">
		    <span class="caption"></span>
		    <div class="tools">

	        </div>
		</div>
		<div class="portlet-body">
			<div class="row">
				<div class="video col-md-6" ng-repeat="t in tutoriales">
					<div class="video-code" ng-bind-html="t.code | bypass"></div>
					<div class="video-title">
						<h3>{{ t.title }}</h3>
					</div>
					<div class="video-description">
						{{ t.description }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="componentes/FilterableSortableTable.js"></script>